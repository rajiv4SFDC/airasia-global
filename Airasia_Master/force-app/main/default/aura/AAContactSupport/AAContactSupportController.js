({
    doInit : function(component, event, helper) {
		
        // Yugesh Sharma 2-4-2019 currently allowing Japanese Chatbot
        //if($A.get("$Locale.langLocale") == "ja"){
            //adaBot.destroy();
        //}
        
	},
    
    navigateToChatBot : function(component, event, helper){
        var event = new Event('toggleADA');
		window.dispatchEvent(event);
        //adaBot.show();      
    },
    
	navigateToCreateCase : function(component, event, helper) {
		var address = "/customcontactsupport";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :false
        });
        urlEvent.fire();
	},
    navigateToFacebook : function(component, event, helper) {
		var address = "https://www.facebook.com/messages/t/AirAsiaSupport";
        if ($A.get("$Locale.langLocale") == "th") {
            address = "https://www.facebook.com/messages/t/AirAsiaThailand";
        }
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :true
        });
        urlEvent.fire();
	},
    navigateToWeibo : function(component, event, helper) {
		var address = "https://weibo.com/";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :true
        });
        urlEvent.fire();
	},
    navigateToTwitter : function(component, event, helper) {
		var address = "https://twitter.com/AirAsiaSupport";
        if ($A.get("$Locale.langLocale") == "th") {
            address = "https://twitter.com/airasiasupport?lang=th";
        }
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :true
        });
        urlEvent.fire();
	},
    navigateToAllPhoneNos : function(component, event, helper) {
        window.open('/s/call-center');
	},  
    
    
    navigateToWeChat : function(component, event, helper){
    
        window.open($A.get("$Resource.AAIcons") + '/wechat_QRCode.png','_blank');
	},
    
    navigateToWhatsApp : function(component, event, helper){
        
        window.open('https://api.whatsapp.com/send?phone=601135165078');
    }
    
    
})