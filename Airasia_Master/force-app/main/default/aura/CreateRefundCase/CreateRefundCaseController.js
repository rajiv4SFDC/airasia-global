({
    doInit : function(component, event, helper){
        helper.findUserType(component, event, helper);
        helper.fetchPicklistValues(component, 'Type', 'Sub_Category_1__c');
        helper.fetchCountryCode(component);
        helper.fetchDynamicCasePageLayout(component);
        helper.fetchSalutation(component);
        helper.fillCategoryData(component);
        /*component.set("v.valueListener", {
            add : function(fileId){
                var ListOfFileIds = component.get("v.ListOfFileIds") || [];
                ListOfFileIds.push(fileId);
                
                component.set("v.ListOfFileIds", ListOfFileIds);
                console.log('********* '+component.get("v.ListOfFileIds"));
            }
        });*/
    },
    escapeKeyPressed: function(component, event, helper) {
        //console.log(event.keyCode);
        if(event.keyCode == 27){
            helper.closeModal(component);
        }
    },
    showCaseTypes : function(component, event, helper) {
        //component.set("v.showCSButton", false);
    },
    navigateToCreateCase : function(component, event, helper) {
        var selVal = event.getSource().get("v.value");
        console.log('Navigate called: '+selVal);
        var address = "/contactsupport";
        if(selVal == "refund"){
            address = "/createrefundcase";
        }
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :true
        });
        urlEvent.fire();
    },
    handleSaveCase : function(component, event, helper) {
        $A.util.removeClass(component.find('spinnerAA'), "slds-hide");
        
        var formErrorMsg = component.find("formErrorMsg");
        $A.util.addClass(formErrorMsg, "hideErrorMsg");
        component.set("v.isValidForm",true);
        
        var valid = helper.validateFields(component);
        console.log('Valid : '+valid);

        if(valid){
            var button =component.find("Submit");
            button.set("v.disabled",true);
            var btn =component.find("SaveFilesSubmit");
            btn.set("v.disabled",true);
            helper.saveCase(component, event, helper, false); 
        }else{
            var formErrorMsg = component.find("formErrorMsg");
            $A.util.removeClass(formErrorMsg, "hideErrorMsg");
            $A.util.addClass(component.find('spinnerAA'), "slds-hide");
        }
    },
    
    handleSaveCaseUploadFiles: function(component, event, helper) {
        $A.util.removeClass(component.find('spinnerAA'), "slds-hide"); 
        
        var formErrorMsg = component.find("formErrorMsg");
        $A.util.addClass(formErrorMsg, "hideErrorMsg");
        component.set("v.isValidForm",true);
        var valid = helper.validateFields(component);
        console.log('Valid : '+valid);
        if(valid){
            var button =component.find("Submit");
            button.set("v.disabled",true);
            var btn =component.find("SaveFilesSubmit");
            btn.set("v.disabled",true);
            helper.saveCase(component, event, helper, true);
        }else{
            var formErrorMsg = component.find("formErrorMsg");
            $A.util.removeClass(formErrorMsg, "hideErrorMsg");
            $A.util.addClass(component.find('spinnerAA'), "slds-hide");
        }
    },
    display : function(component, event, helper) {
        helper.toggleHelper(component, event);
    },
    
    displayOut : function(component, event, helper) {
        helper.toggleHelper(component, event);
    },
    validateGivenName : function(component,event,helper){
        helper.validateGivenName(component);
    },
    validateFamilyName : function(component,event,helper){
        helper.validateFamilyName(component);
    },
    validateEmail : function(component,event,helper){
        helper.validateEmail(component);
    },
    validateContactMobile : function(component,event,helper){
        helper.validateContactMobile(component);
    },
    validateAlternateNumber : function(component,event,helper){
        helper.validateAlternateNumber(component); 
    },
    validatesubcat1 : function(component,event,helper){
        helper.validatesubcat1(component);
    },
    validatesubcat2 : function(component,event,helper){
        helper.validatesubcat2(component);
    },
    validatesubcat3 : function(component,event,helper){
        helper.validatesubcat3(component);
    },
    validatesubject : function(component,event,helper){
        helper.validatesubject(component);
    },
    validateBookingNumber : function(component,event,helper){
        helper.validateBookingNumber(component);  
    },
    validateAirlineCode : function(component,event,helper){
        helper.validateAirlineCode(component);
    },
    validateFlightNumber : function(component,event,helper){
        helper.validateFlightNumber(component);
    },
    validatedeptDateTime : function(component,event,helper){
        helper.validatedeptDateTime(component);
    },
    validateBookingPayment : function(component,event,helper){
        helper.validateBookingPayment(component);
    },
    validateCurrency : function(component,event,helper){
        helper.validateCurrency(component);
    },
    validateBankName : function(component,event,helper){
        helper.validateBankName(component);
    },
    validateBankNameinC : function(component,event,helper){
        helper.validateBankNameinC(component);
    },
    validateBankAccountHolderName : function(component,event,helper){
        helper.validateBankAccountHolderName(component);
    },
    validateBankAccountNumber : function(component,event,helper){
        helper.validateBankAccountNumber(component);
    },
    validatebankAccountHHAddress : function(component,event,helper){
        helper.validatebankAccountHHAddress(component);
    },
    validateBAHHANonE : function(component,event,helper){
        helper.validateBAHHANonE(component);
    },
    validateBankBranch : function(component,event,helper){
        helper.validateBankBranch(component);  
    },
    validateBankBranchinC : function(component,event,helper){
        helper.validateBankBranchinC(component);  
    },
    validateBankCountry : function(component,event,helper){
        helper.validateBankCountry(component);
    },
    validateBankAddress : function(component,event,helper){
        helper.validateBankAddress(component);  
    },
    validateBAinNonE : function(component,event,helper){
        helper.validateBAinNonE(component);  
    },
    validateProvince : function(component,event,helper){
        helper.validateProvince(component);  
    },
    validateProvinceinC : function(component,event,helper){
        helper.validateProvinceinC(component);
    },
    validateCity : function(component,event,helper){
        helper.validateCity(component);  
    },
    validateCityinC : function(component,event,helper){
        helper.validateCityinC(component);  
    },
    validateBankBCode : function(component,event,helper){
        helper.validateBankBCode(component);  
    },
    validateBankCode : function(component,event,helper){
        helper.validateBankCode(component);  
    },
    validateGender : function(component,event,helper){
        helper.validateGender(component);  
    },
    validatetelephone : function(component,event,helper){
        helper.validatetelephone(component);  
    },
    validateBankAHIdentity : function(component,event,helper){
        helper.validateBankAHIdentity(component);  
    },
    validateAccountType : function(component,event,helper){
        helper.validateAccountType(component);  
    },
    validateIBCode : function(component,event,helper){
        helper.validateIBCode(component);  
    },
    validateIBCodeText : function(component,event,helper){
        helper.validateIBCodeText(component);  
    },
    validateFlightSectorToBeRefunded : function(component,event,helper){
        helper.validateFlightSectorToBeRefunded(component);
    },
    validatePassengerToBeRefunded : function(component,event,helper){
        helper.validatePassengerToBeRefunded(component);
    },
    validateFlightDetail: function(component,event,helper){
        var fieldName = event.getSource().getLocalId();
        helper.validateFlightDetail(component, fieldName);
    },
    validateName : function(component,event,helper){
        var fieldName = event.getSource().getLocalId();
        helper.validateNameField(component, fieldName);
    },
    validateComment : function(component,event,helper){
        helper.validateComment(component);  
    },
    // function call on change tha controller field  
    showRelatedComponent: function(component, event, helper) {
        console.log('showRelatedComponent start');
        $A.util.removeClass(component.find('spinnerAA'), "slds-hide");
                
        var controllerValueKey = event.getSource().get("v.value");
        console.log('controllerValueKey is ' + controllerValueKey)
        var formErrorMsg = component.find("formErrorMsg");
        console.log('formErrorMsg: '+formErrorMsg);
        $A.util.addClass(formErrorMsg, "hideErrorMsg");
        console.log('controllerValueKey: '+controllerValueKey);
        if(controllerValueKey.toLowerCase() == "refund"){
            component.set("v.showRefundCaseType", true);
            component.set("v.isSubCat", true);
            component.set("v.showpersonald",false);
            component.set("v.showPhone",false);
            component.set("v.showSubject", true);
            component.set("v.showComment", false);
            component.set("v.showBooking", true);
            component.set("v.showSubmitAttach", true);
            helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode');
            // Defect 149
            helper.fetchPicklistValsforWellnet(component, 'Booking_Payment__c', 'bookingPayment', false);
        }else if(controllerValueKey.toLowerCase() == ""){
            component.set("v.showRefundCaseType", false);
            helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode');
            helper.resetBankDetailsDisplayProp(component);
            component.set("v.showPaymentRelatedFields", false);            
            component.set("v.isSubCat", false);
            component.set("v.isSubCat1Others", false);
            component.set("v.showPhone",false);
            component.set("v.showComment", false);
            component.set("v.showSubject", false);
            component.set("v.showpersonald",false);
            component.set("v.showBooking", false);
            component.set("v.showSubmitAttach", false);
            component.set("v.showCurrency", false);
            component.set("v.showPassengerRefundOpt", false);
            component.set("v.showFlightRefundOpt", false);
            component.set("v.showFlightRefund1", false);
            component.set("v.showPassengerRefund1", false);
            var newCase = component.get("v.newCase");
            newCase.Case_Currency__c = null;
            component.set("v.newCase",newCase);
        }else{
            component.set("v.showRefundCaseType", false);
            helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode');
            helper.resetBankDetailsDisplayProp(component);
            component.set("v.showPaymentRelatedFields", false);            
            component.set("v.isSubCat", true);
            component.set("v.isSubCat1Others", false);
            component.set("v.isSubCat2Others", false);
            component.set("v.showPhone",true);
            component.set("v.showComment", true);
            component.set("v.showSubject", true);
            component.set("v.showpersonald",true);
            component.set("v.showBooking", false);
            component.set("v.showSubmitAttach", false);
            component.set("v.showCurrency", false);
            component.set("v.showPassengerRefundOpt", false);
            component.set("v.showFlightRefundOpt", false);
            component.set("v.showFlightRefund1", false);
            component.set("v.showPassengerRefund1", false);
            component.set("v.showSubmitAttach", true);
            var newCase = component.get("v.newCase");
            newCase.Case_Currency__c = null;
            component.set("v.newCase",newCase);
        }
        helper.loadCategories(component, event);
    },
    
    // function call on change tha Dependent field    
    onDependentFieldChange: function(component, event, helper) {
        console.log('onDependentFieldChange start');
        var cat1Val = event.getSource().get("v.value");
        var cat2Val = component.find('subCat2').get('v.value');
        var typeValue = component.find('type').get('v.value');
        //Reset the Sub Category 2 & 3 to not to show
        component.set("v.isSubCat1Others", false);
        component.set("v.isSubCat2Others", false);
        component.set("v.showPassengerRefundOpt", false);
        component.set("v.showFlightRefundOpt", false);
        component.set("v.showFlightRefund1", false);
        component.set("v.showPassengerRefund1", false);
        
        if(typeValue.toLowerCase() === 'refund') {
            if(cat1Val.toLowerCase() === 'other'){
                component.set("v.showComment", true);
                helper.loadCategories(component, event);
            } else {
                var categoriesData = component.get("v.categoriesData");
                var selectedCategory = cat1Val.toLowerCase();
                
                if(selectedCategory === 'flight reschedule' || selectedCategory === 'flight cancellation') {
                    
                    //Maaz Khan Changes: 4 Feb 2019 Begin
                    
                    component.set("v.showPassengerRefundOpt", true);
            		component.set("v.showFlightRefundOpt", true);
                    component.set("v.isSubCat1Others", false);
                    component.set("v.isSubCat2Others", false);
                    component.set("v.newCase.Sub_Category_2__c",'Other');
                    component.set("v.newCase.Sub_Category_3__c",'Others');
            		helper.fetchPicklistVals(component, 'Passenger_to_be_Refunded__c', 'passengerToBeRefunded');            
           		    helper.fetchPicklistVals(component, 'Flight_Sector_to_be_Refunded__c', 'flightSectorToBeRefunded');
                    
                    ////Maaz Khan Changes End
                    
                	//component.set("v.showComment", true);    
                } else {
                    component.set("v.showComment", false);
                    
                }
                helper.loadModel(component, selectedCategory);
            }
        }
        else {
            helper.loadCategories(component, event);
        }
    },
    onSubCat2Change: function(component, event, helper) {
        console.log('onSubCat2Change start');
        var cat2Val = event.getSource().get("v.value");
        
        //Reset the Sub Category 2 to not to show
        component.set("v.isSubCat2Others", false);
        component.set("v.showFlightRefundOpt", false);
        component.set("v.showPassengerRefundOpt", false);
        component.set("v.showFlightRefund1", false);
        component.set("v.showPassengerRefund1", false);
        
        if(cat2Val.toLowerCase() == 'voluntary cancellation'){
            component.set("v.isSubCat2Others", true);
            component.set("v.showComment", false); 
            helper.fetchPicklistVals(component, 'Sub_Category_3__c', 'subCat3');
        }
        else{
            component.set("v.showComment", true); 
        }
        
        // Refund Pop Up
        var typeValue = component.find('type').get('v.value');
        if(typeValue.toLowerCase() === 'refund') {
            
            if(cat2Val.toLowerCase() == 'serious illness / injury') {
            	component.set("v.showBooking", false);
                component.set("v.showPaymentRelatedFields", false);
                component.set("v.showCreditCardInfo", false);
                component.find('bookingPayment').set("v.value", component.get('v.defaultCategoryValue'));
            } else {
                component.set("v.showBooking", true);
            }
            
            helper.loadModel(component, cat2Val);
        }
    },
    onSubCat3Change: function(component, event, helper) {
        console.log('onSubCat3Change start');
        var cat3Val = event.getSource().get("v.value").toLowerCase();
      if(cat3Val === 'india domestic (i5)' || cat3Val === 'korea flight ticket' || cat3Val === 'indonesia domestic'){
            component.set("v.showPassengerRefundOpt", true);
            component.set("v.showFlightRefundOpt", true); 
            helper.fetchPicklistVals(component, 'Passenger_to_be_Refunded__c', 'passengerToBeRefunded');            
            helper.fetchPicklistVals(component, 'Flight_Sector_to_be_Refunded__c', 'flightSectorToBeRefunded');
        }
        else{
            component.set("v.showFlightRefundOpt", false);
            if(cat3Val === 'united states flight booking') {
                component.set("v.showPassengerRefundOpt", true);
                helper.fetchPicklistVals(component, 'Passenger_to_be_Refunded__c', 'passengerToBeRefunded'); 
            }
             if(cat3Val === 'other' ){
                component.set("v.showPassengerRefundOpt", false);
            }
        }
        
        // Refund Pop Up
        var typeValue = component.find('type').get('v.value');
        if(typeValue.toLowerCase() === 'refund') {
            var selectedCategory = event.getSource().get("v.value");
            helper.loadModel(component, selectedCategory);
        }
    },
    onPassengerToBeRefundedChange: function(component, event, helper){
    	var passengerRefundOptValue = event.getSource().get("v.value").toLowerCase();
        if(passengerRefundOptValue === 'selected'){
            component.set("v.showPassengerRefund1", true);
            component.set("v.passengerNumberValue", '1');
        }
        else{
            component.set("v.showPassengerRefund1", false);
            component.set("v.showPassengerRefund2", false);
            component.set("v.showPassengerRefund3", false);
            component.set("v.showPassengerRefund4", false);
            component.set("v.showPassengerRefund5", false);
            component.set("v.showPassengerRefund6", false);
            component.set("v.showPassengerRefund7", false);
            component.set("v.showPassengerRefund8", false);
            component.set("v.passengerNumberValue", '0');
        }
	},
    onFlightSectorToBeRefundedChange: function(component, event, helper){
    	var flightSectorRefundOptValue = event.getSource().get("v.value").toLowerCase();
        if(flightSectorRefundOptValue === 'selected'){
            component.set("v.showFlightRefund1", true);
            component.set("v.sectorNumberValue", '1');
            helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode1');
        }
        else{
            component.set("v.showFlightRefund1", false);
            component.set("v.showFlightRefund2", false);
            component.set("v.showFlightRefund3", false);
            component.set("v.showFlightRefund4", false);
            component.set("v.showFlightRefund5", false);
            component.set("v.showFlightRefund6", false);
        }
	}, 
    addPassenger: function(component, event, helper){
        var passengerNumberValue = component.get("v.passengerNumberValue");
        //console.log('PassengerNumber', passengerNumberValue);
        if(passengerNumberValue < 9){
            if(!component.get("v.showPassengerRefund2")){
               component.set("v.showPassengerRefund2", true);
               passengerNumberValue++; 
        	}else if(!component.get("v.showPassengerRefund3")){
            	component.set("v.showPassengerRefund3", true);
                passengerNumberValue++;
            	}else if(!component.get("v.showPassengerRefund4")){
                	component.set("v.showPassengerRefund4", true);
                    passengerNumberValue++;
                	}else if(!component.get("v.showPassengerRefund5")){
                    	component.set("v.showPassengerRefund5", true);
                        passengerNumberValue++;
                		}else if(!component.get("v.showPassengerRefund6")){
                    		component.set("v.showPassengerRefund6", true);
                            passengerNumberValue++;
                			}else if(!component.get("v.showPassengerRefund7")){
                    			component.set("v.showPassengerRefund7", true);
                                passengerNumberValue++;
                				}else if(!component.get("v.showPassengerRefund8")){
                    				component.set("v.showPassengerRefund8", true);
                                    passengerNumberValue++;
                					}
        }
        component.set("v.passengerNumberValue", passengerNumberValue);
     	//console.log('PassengerNumber', passengerNumberValue);
	},
    removePassenger: function(component, event, helper){
        var removePassengerValue = event.getSource().getLocalId().toLowerCase();
        var passengerNumberValue = component.get("v.passengerNumberValue");
        switch(removePassengerValue){
            case "removepassenger2":
                component.set("v.showPassengerRefund2", false);
                passengerNumberValue--;
                break;
            case "removepassenger3":
                component.set("v.showPassengerRefund3", false);
                passengerNumberValue--;
                break;
            case "removepassenger4":
                component.set("v.showPassengerRefund4", false);
                passengerNumberValue--;
                break;
            case "removepassenger5":
                component.set("v.showPassengerRefund5", false);
                passengerNumberValue--;
                break;
            case "removepassenger6":
                component.set("v.showPassengerRefund6", false);
                passengerNumberValue--;
                break; 
            case "removepassenger7":
                component.set("v.showPassengerRefund7", false);
                passengerNumberValue--;
                break;
            case "removepassenger8":
                component.set("v.showPassengerRefund8", false);
                passengerNumberValue--;
                break;
        }
        component.set("v.passengerNumberValue", passengerNumberValue);
    },
    addSector: function(component, event, helper){
    	var sectorNumberValue = component.get("v.sectorNumberValue");
        if(sectorNumberValue < 7){
            if(!component.get("v.showFlightRefund2")){
                component.set("v.showFlightRefund2", true);
                helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode2');
                sectorNumberValue++;
            }else if(!component.get("v.showFlightRefund3")){
                component.set("v.showFlightRefund3", true);
                helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode3');
                sectorNumberValue++;
            	}else if(!component.get("v.showFlightRefund4")){
                	component.set("v.showFlightRefund4", true);
                    helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode4');
                	sectorNumberValue++;
            		}else if(!component.get("v.showFlightRefund5")){
                		component.set("v.showFlightRefund5", true);
                        helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode5');
                		sectorNumberValue++;
            			}else if(!component.get("v.showFlightRefund6")){
                			component.set("v.showFlightRefund6", true);
                            helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode6');
                			sectorNumberValue++;
            				}
        }
        component.set("v.sectorNumberValue", sectorNumberValue);
	},
    removeSector: function(component, event, helper){
    	var removeSectorValue = event.getSource().getLocalId().toLowerCase();
        var sectorNumberValue = component.get("v.sectorNumberValue");
        switch(removeSectorValue){
            case "removesector2":
                component.set("v.showFlightRefund2", false);
                sectorNumberValue--;
                break;
            case "removesector3":
                component.set("v.showFlightRefund3", false);
                sectorNumberValue--;
                break;
            case "removesector4":
                component.set("v.showFlightRefund4", false);
                sectorNumberValue--;
                break;
            case "removesector5":
                component.set("v.showFlightRefund5", false);
                sectorNumberValue--;
                break;
            case "removesector6":
                component.set("v.showFlightRefund6", false);
                sectorNumberValue--;
                break;
        }        
    	component.set("v.sectorNumberValue", sectorNumberValue);
	},
    closeModal: function(component, event, helper) {
        helper.closeModal(component);
    },
    searchArticles: function(component, event, helper) {
        var isBlur = false;
        if(event.vf === 'blur') {
            helper.validatesubject(component);
            isBlur = true
        }
        var srchStr = component.find("SearchText").get("v.value");
        var SearchKAEvent = $A.get("e.c:SearchKAEvent");
        SearchKAEvent.setParams({ "searchString" : srchStr, "isBlur" : isBlur });
        SearchKAEvent.fire();
    },
    onBookingPaymentChange: function(component, event, helper) {
        var bookingPayment = event.getSource().get("v.value");
        console.log('Booking Payment: '+bookingPayment.toLowerCase());
        // Defect 149
        component.find('bankCurrency').set("v.disabled", 'false');
        if(bookingPayment.toLowerCase() == 'cash'){
            component.set("v.showPaymentRelatedFields", true);
            component.set("v.showCurrency", true);
            component.set("v.showCreditCardInfo",false);
            helper.fetchPicklistVals(component, 'Case_Currency__c', 'bankCurrency');
            //helper.fetchPicklistVals(component, 'swift_iban_Code__c', 'intenationalBankCode');
        }else if(bookingPayment.toLowerCase() == 'direct debit'){
            component.set("v.showPaymentRelatedFields", true);
            component.set("v.showCurrency", true);
            component.set("v.showCreditCardInfo",false);
            helper.fetchPicklistVals(component, 'Case_Currency__c', 'bankCurrency');
        }else if(bookingPayment.toLowerCase() == 'wellnet'){
            component.set("v.showPaymentRelatedFields", true);
            component.set("v.showCreditCardInfo",false);
            // Defect 149
	    component.set("v.showCurrency", true);
            component.find('bankCurrency').set("v.disabled", 'true');
            helper.fetchPicklistValsforWellnet(component, 'Case_Currency__c', 'bankCurrency', false);
            helper.bankDetailsForWellnet(component, event, helper);
        }else if(bookingPayment.toLowerCase() == 'credit card'){
            component.set("v.showCreditCardInfo", true);
            component.set("v.showCurrency", false);
            component.set("v.showPaymentRelatedFields", false);
        }else{
            component.set("v.showPaymentRelatedFields", false);
        }
    },
    BankDetailsValuation: function(component, event, helper) {
        var caseCurrency = component.find("bankCurrency").get("v.value");
        var airlineCode = component.find("airlineCode").get("v.value");
        console.log('Bank Currency: '+caseCurrency);
        console.log('Airline Code: '+airlineCode);
        
        // Defect 149
		component.find('bankCurrency').set("v.disabled", 'false');
        var typeValue = component.find('type').get('v.value');
        if(typeValue.toLowerCase() === 'refund') {
        	helper.fetchPicklistValsforWellnet(component, 'Booking_Payment__c', 'bookingPayment', (airlineCode === 'DJ'));
        }
        
        helper.resetBankDetailsDisplayProp(component);
        helper.loadElements(component);
        if(caseCurrency != undefined || caseCurrency!= null || caseCurrency!= ''){
            var currencyList = ['EUR','INR','AUD','USD','BND','CNY','GBP','THB','MYR','IDR','SGD','PHP','NZD','HKD','TWD','JPY','MUR'];
            var airlineCodeList = ['AK','D7','FD','XJ','QZ','XT','PQ','Z2','I5'];
            var KRWAirlineCodeList = ['D7','XJ','XT'];
            if(currencyList.indexOf(caseCurrency) != -1 && airlineCodeList.indexOf(airlineCode) != -1){
                component.set("v.showBHN", true);
                component.set("v.showBN", true);
                component.set("v.showBAN", true);
                component.set("v.showBB", true);
                component.set("v.showBoC", true);
                if(caseCurrency == 'MOP' || caseCurrency == 'KRW' ){
            //        component.set("v.showBSIIBS", false);
                }else{
              //      component.set("v.showBSIIBS", true);
                //    helper.fetchPicklistVals(component, 'International_Bank_Code__c', 'internationalBankCode');
                }
                if((caseCurrency == 'INR' || caseCurrency == 'IDR' ) && airlineCode == 'XJ'){
                    component.set("v.showBA", true);
                    component.set("v.showBAHHA", true);
                }else if(caseCurrency == 'USD' && airlineCode == 'FD'){
                    component.set("v.showBAHHA", true);
                    component.set("v.showBAHG", true);
                    component.set("v.showBAHTN", true);
                    helper.fetchPicklistVals(component, 'Bank_Account_Holder_Gender__c', 'gender');
                }else if(caseCurrency == 'CNY' || caseCurrency == 'TWD'){
                    component.set("v.showProvince", true);
                    component.set("v.showCity", true);
                    component.set("v.showBHNinC", true);
                    component.set("v.showBNinC", true);
                    component.set("v.showBBinC", true);
                    component.set("v.showProvinceinC", true);
                    component.set("v.showCityinC", true);
                    if(caseCurrency == 'CNY' && (airlineCode == 'XJ' || airlineCode == 'XT')){
                        component.set("v.showBA", true);
                        component.set("v.showBAinNonE", true);
                        component.set("v.showBAHHA", true);
                        component.set("v.showBAHHANonE", true);
                        if(airlineCode == 'XT'){
                            component.set("v.showBAHINP", true);
                        }
                    }
                }else if(caseCurrency == 'MYR' && (airlineCode == 'XJ' || airlineCode == 'XT')){
                    component.set("v.showBA", true);
                    component.set("v.showBAHHA", true);
                }else if(caseCurrency == 'JPY'){
                    component.set("v.showBBC", true);
                    component.set("v.showBC", true);
                    if(airlineCode == 'D7'){
                        var JInfo = component.get("v.JPYD7ArticleId");
                        if($A.get("$Locale.langLocale") === 'ja') {
                        	JInfo = component.get("v.JPYD7ArticleIdJPY");
                        }
                        var SearchKAEvent = $A.get("e.c:SearchKAEvent");
                        SearchKAEvent.setParams({ "JapanId" : JInfo });
                        SearchKAEvent.fire();
                        component.set("v.showJPYD7info", true);
                    }
                }else if(caseCurrency == 'MUR' && airlineCode == 'D7'){
                  //  component.set("v.showBAHG", true);
                    component.set("v.showBAHTN", true);
                    component.set("v.showBAHHA", true);
                  //  helper.fetchPicklistVals(component, 'Bank_Account_Holder_Gender__c', 'gender');
                }
            }else if(caseCurrency == 'INR' && airlineCode == 'I5'){
                component.set("v.showBHN", true);
                component.set("v.showBN", true);
                component.set("v.showBAN", true);
                component.set("v.showBB", true);
                component.set("v.showBoC", true);
            }else if(caseCurrency == 'MOP' && airlineCode == 'FD'){
                component.set("v.showBHN", true);
                component.set("v.showBN", true);
                component.set("v.showBAN", true);
                component.set("v.showBB", true);
                component.set("v.showBoC", true);
              //  component.set("v.showBSIIBS", true);
            //    helper.fetchPicklistVals(component, 'International_Bank_Code__c', 'internationalBankCode');
                
            }else if(airlineCode == 'DJ' && caseCurrency != undefined){
                component.set("v.showBHN", true);
                component.set("v.showBN", true);
                component.set("v.showBAN", true);
                if(caseCurrency == 'JPY') {
                    component.set("v.showBBC", true);
                    component.set("v.showBC", true);
                    component.set("v.showAcctType", true);
                    helper.fetchPicklistVals(component, 'Account_Type__c', 'acctType');
                } else {
                    component.set("v.showBB", true);
                    component.set("v.showBoC", true);
                    if(caseCurrency == 'CNY' || caseCurrency == 'TWD'){
                        component.set("v.showProvince", true);
                        component.set("v.showCity", true);
                        component.set("v.showBHNinC", true);
                        component.set("v.showBNinC", true);
                        component.set("v.showBBinC", true);
                        component.set("v.showProvinceinC", true);
                        component.set("v.showCityinC", true);
					}
                }
            }else if(caseCurrency == 'KRW' && KRWAirlineCodeList.indexOf(airlineCode) != -1){
                component.set("v.showBHN", true);
                component.set("v.showBN", true);
                component.set("v.showBAN", true);
                component.set("v.showBB", true);
                component.set("v.showBoC", true);
          //      component.set("v.showBSIIBS", true);
                //helper.fetchPicklistVals(component, 'International_Bank_Code__c', 'internationalBankCode');
            }else if((caseCurrency == 'KRW' || caseCurrency == 'MOP') && airlineCode == 'I5'){
                component.set("v.showBHN", true);
                component.set("v.showBN", true);
                component.set("v.showBAN", true);
                component.set("v.showBB", true);
                component.set("v.showBoC", true);
            }
        }
    },
    
    showSpinner: function(component, event, helper) {
        console.log('showSpinner start');
        $A.util.removeClass(component.find('spinnerAA'), "slds-hide");
    },
    
    hideSpinner: function(component, event, helper) {
        console.log('hideSpinner start');
        $A.util.addClass(component.find('spinnerAA'), "slds-hide");
    },
    done: function(component, event, helper){
        console.log('done start');
        helper.navigateToCaseDetails(component);
    },
    closeErrorMessage: function(component,event,helper){
        var formErrorMsg = component.find("formErrorMsg");
        $A.util.addClass(formErrorMsg, "hideErrorMsg");
    },
    showToolTipHelp: function(component,event,helper){
        var myId = event.currentTarget.getAttribute('data-idevalue');
        helper.showToolTip(component, myId);
    },
    hideTooltipHelp: function(component,event,helper){
        var myId = event.currentTarget.getAttribute('data-idevalue');
        helper.hideToolTip(component, myId);
    },
    countryCodeChange : function(component, event, helper){
        helper.countryCodeChange(component, event);
    }
})