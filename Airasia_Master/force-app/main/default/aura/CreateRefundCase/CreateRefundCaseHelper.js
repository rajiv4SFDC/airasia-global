({
    findUserType: function(component, event, helper){
        var action = component.get("c.getUserType");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                var userType = a.getReturnValue();
                component.set("v.userType", userType);
            }
        });
        
        $A.enqueueAction(action)
    },
    saveCase : function(component, event, helper, uploadFiles) {
        var i;
        var passengerListValue = "";
        var sectorListValue = "";
        var tempStr1;
        var tempStr2;
        var tempCond;
        var newCase = component.get("v.newCase");
        var mobilephoneValue = this.generateCountryTelephoneInfo(component.get("v.mobileCountryCodeValue"), component.get("v.mobilephoneValue"));
        var alternatephoneValue = this.generateCountryTelephoneInfo(component.get("v.alternateCountryCodeValue"), component.get("v.alternatephoneValue"));
        for(i=1; i<9; i++){
            //tempInt = eval("i + 1") ; 
            tempCond = "component.get(\"v.showPassengerRefund" + i + "\")";
            tempStr1 = "component.get(\"v.givenName" + i + "Value\")";
            tempStr2 = "component.get(\"v.familyName" + i + "Value\")";
            if(passengerListValue.length < 1){
                if(eval(tempCond)){
                    if(eval(tempStr1)){
                        passengerListValue = eval(tempStr1);
                    }
                    passengerListValue = passengerListValue + ":";
                    if(eval(tempStr2)){
                    	passengerListValue = passengerListValue + eval(tempStr2);
                	}
                }
            }
            else{
                if(eval(tempCond)){
                    passengerListValue = passengerListValue + ",";
                    if(eval(tempStr1)){
                        passengerListValue = passengerListValue + eval(tempStr1);
                    }
                    passengerListValue = passengerListValue + ":";
                    if(eval(tempStr2)){
                    	passengerListValue = passengerListValue + eval(tempStr2);
                	}
                }
            }
        }
        for(i=1; i<7; i++){
            tempCond = "component.get(\"v.showFlightRefund" + i + "\")";
            tempStr1 = "component.get(\"v.airlineCode" + i + "Value\")";
            tempStr2 = "component.get(\"v.flightNumber" + i + "Value\")";
            if(sectorListValue.length < 1){
                if(eval(tempCond)){
                    if(eval(tempStr1)){
                        sectorListValue = eval(tempStr1);
                    }
                    sectorListValue = sectorListValue + ":";
                    if(eval(tempStr2)){
                    	sectorListValue = sectorListValue + eval(tempStr2);
                	}
                }
            }
            else{
                if(eval(tempCond)){
                    sectorListValue = sectorListValue + ",";
                    if(eval(tempStr1)){
                        sectorListValue = sectorListValue + eval(tempStr1);
                    }
                    sectorListValue = sectorListValue + ":";
                    if(eval(tempStr2)){
                    	sectorListValue = sectorListValue + eval(tempStr2);
                	}
                }
            }
        }
        console.log('passengerListValue', passengerListValue);
        console.log('sectorListValue', sectorListValue);
        this.fetchCaseElements(component);
        
        newCase.Case_Language__c = $A.get("$Locale.langLocale");
        
        if(component.get("v.userType") == 'Guest') { 
        	newCase.SuppliedPhone = component.get("v.mobilephoneValue");    
        }

      //  console.log('Case' , JSON.stringify(newCase));
        var action = component.get("c.saveCase");
        /*
        action.setParams({ 
            "newCase": newCase,
            "caseComments": newCase.Comments,
            "mobilephoneValue": mobilephoneValue,
            "alternatephoneValue": alternatephoneValue
        });
        */
        action.setParams({ 
            "newCase": newCase,
            "caseComments": newCase.Comments,
            "mobilephoneValue": mobilephoneValue,
            "alternatephoneValue": alternatephoneValue,
            "flightRefundList": passengerListValue,
            "passengerRefundList": sectorListValue
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                component.set("v.showError",false);
                var caseDetails = a.getReturnValue();
                
                component.set("v.caseId", caseDetails.Id);
                component.set("v.refNo", caseDetails.CaseNumber);
                if(uploadFiles){ 
                    component.set("v.uploadAttachments", true);
                }else{
                    this.navigateToCaseDetails(component);
                }
            }else{
                var errors = action.getError();
                console.log('Error - ', errors[0].message);
                var btn = component.find("Submit");
                btn.set("v.disabled",false);
                var button =component.find("SaveFilesSubmit");
                button.set("v.disabled",false);
                
                if('NON_COMMUNITY_USER' === errors[0].message) {
                    component.set("v.showErrorMsg", 'MESSAGE - Only Community User can save case.');
                }
                component.set("v.showError",true);
            }
        });
        
        $A.enqueueAction(action)
    },
    toggleHelper : function(component,event) {
        var toggleText = component.find("tooltip");
        $A.util.toggleClass(toggleText, "toggle");
    },
    navigateToCaseDetails: function(component,caseDetails){
        var userType = component.get("v.userType");
        if(userType == 'Guest'){
            component.set("v.recordCreated", true);
        }else{
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId":component.get("v.caseId"),
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
    },
    fetchPicklistValues: function(component, controllerField, dependentField) {
        var action = component.get("c.getDependentOptionsImpl");
        console.log('fetchPicklistValues action: '+action);
        action.setParams({
            'objApiName': component.get("v.objInfo"),
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField
        });
        //set callback   
        action.setCallback(this, function(response) {
            if (response.getState() === 'ERROR'){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error('ERROR --> ', errors[0].message);
                    }
                }
            } else if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                var StoreResponse = response.getReturnValue();
                
                if(controllerField == 'Type'){
                    component.set("v.depnedentFieldMap", StoreResponse);
                    console.log('Type component: '+component);
                    var listOfkeys = []; // for store all map keys (controller picklist values)
                    var ControllerField = []; // for store controller picklist value to set on ui field. 
                    for (var singlekey in StoreResponse) {
                        //AT (23 April 2019) - Temp Fix to remove "Refund" from the E-Form picklist
                        if(StoreResponse[singlekey].value!=="Refund") {
                        	listOfkeys.push(StoreResponse[singlekey]);
                    	}
                    }
                    this.fetchDepValues(component, listOfkeys, 'type');
                }else if(controllerField == 'Sub_Category_1__c'){
                    component.set("v.depnedentCat2FieldMap", StoreResponse);
                }else if(controllerField == 'Sub_Category_2__c'){
                    component.set("v.depnedentCat3FieldMap", StoreResponse);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields, fieldId) {
        // create a empty array var for store dependent picklist values for controller field)  
        var dependentFields = [];
        //console.log('fetchDepValues component: '+component);
        //console.log('fetchDepValues ListOfDependentFields: '+ListOfDependentFields);
        //console.log('fetchDepValues fieldId: '+fieldId);
        if (ListOfDependentFields != undefined && ListOfDependentFields.length > 0) {
            dependentFields.push({
                class: "optionClass",
                label: component.get("v.defaultCategoryValue"),
                value: ""
            });
            
        }
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            dependentFields.push({
                class: "optionClass",
                label: ListOfDependentFields[i].label,
                value: ListOfDependentFields[i].value
            });
        }
        // set the dependentFields variable values to State(dependent picklist field) on ui:inputselect    
        component.find(fieldId).set("v.options", dependentFields);
        // make disable false for ui:inputselect field 
        component.set("v.isDependentDisable", false);
    },
    
    fetchPicklistVals: function(component, fieldApiName, fieldId) {
        var action = component.get("c.getPicklistVals");
        action.setParams({
            'objApiName': component.get("v.objInfo"),
            'fieldApiName': fieldApiName
        });
        //console.log('fieldApiName' + fieldApiName);
        //console.log('component.get("v.objInfo")' + component.get("v.objInfo"));
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                this.fetchDepValues(component, StoreResponse, fieldId);
            }
        });
        $A.enqueueAction(action);
    },
    // Defect 149
    fetchPicklistValsforWellnet: function(component, fieldApiName, fieldId, isIncludeWellnet) {
        var bookingPaymentValue = component.find('bookingPayment').get('v.value');
        var action = component.get("c.getPicklistVals");
        action.setParams({
            'objApiName': component.get("v.objInfo"),
            'fieldApiName': fieldApiName
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                this.fetchDepValues(component, StoreResponse, fieldId);
                if(fieldId === 'bankCurrency') {
                	component.find(fieldId).set("v.value", 'JPY');
                } else {
                    if(isIncludeWellnet === false) {
                        var bookingOptions = component.find('bookingPayment').get("v.options");
                        var newBookingOptions = [];
                        for(var i=0; i < bookingOptions.length; i++) {
                            if(bookingOptions[i].label !== 'Wellnet') {
                            	newBookingOptions.push(bookingOptions[i]);
                            }
                        }
                        component.find('bookingPayment').set("v.options", newBookingOptions);
                    }
                    if(typeof bookingPaymentValue != 'undefined' && bookingPaymentValue) {
                        component.find('bookingPayment').set("v.value", bookingPaymentValue);
                    }                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    bankDetailsForWellnet: function(component, event, helper) {
        this.resetBankDetailsDisplayProp(component);
        
        component.set("v.showBHN", true);
        component.set("v.showBN", true);
        component.set("v.showBAN", true);
        component.set("v.showBBC", true);
        component.set("v.showBC", true);
        this.fetchPicklistVals(component, 'Account_Type__c', 'acctType');
        component.set("v.showAcctType", true);
    },
    closeModal: function(component) {
        component.set("v.isModelOpen", false);
        component.set("v.showJPYD7info",false);
    },
    resetBankDetailsDisplayProp: function(component) {
        component.set("v.showBHN", false);
        component.set("v.showBN", false);
        component.set("v.showBAN", false);
        component.set("v.showBB", false);
        component.set("v.showBoC", false);
        component.set("v.showProvince", false);
        component.set("v.showCity", false);
        component.set("v.showBHNinC", false);
        component.set("v.showBNinC", false);
        component.set("v.showBBinC", false);
        component.set("v.showProvinceinC", false);
        component.set("v.showCityinC", false);
        component.set("v.showBBC", false);
        component.set("v.showBC", false);
        component.set("v.showBA", false);
        component.set("v.showBAinNonE", false);
        component.set("v.showBAHHA", false);
        component.set("v.showBAHHANonE", false);
        component.set("v.showBAHG", false);
        component.set("v.showBAHTN", false);
        component.set("v.showBAHINP", false);
        component.set("v.showAcctType", false);
        component.set("v.showBSIIBS", false);
    },
    validateGivenName: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        console.log('WHAT IS HERE' + isValidForm)
        var reg1 = /^[a-zA-Zaâáéíîó????ã?????ñcdeínrš?t?úužd ]*$/;
        
        if(component.get("v.userType") === 'Guest'){
            var flag1=0;
            var GivenNameVal = component.get("v.GivenName");
            var GivenName = component.find("GivenName");
            if (GivenNameVal == '' || typeof GivenNameVal == 'undefined'){
                GivenName.set("v.errors", [{message: $A.get("$Label.c.AA_Case_First_Name_EM")}]);
                isValidForm = false;
            } else if(reg1.test(GivenNameVal) == false) {
                GivenName.set("v.errors", [{message: $A.get("$Label.c.AA_Case_First_Name_ValidEM")}]);
                isValidForm = false;
            }else {
                GivenName.set("v.errors", null);
                flag1 = 1;
            }
        }else{
            newCase.SuppliedName = null;
        }
        component.set("v.isValidForm",isValidForm);
        component.set("v.newCase",newCase);                   
        return flag1;       
    },
    validateFamilyName: function(component){
        //console.log('B')
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
                    var GivenNameVal = component.get("v.GivenName");
        var reg1 = /^[a-zA-Zaâáéíîó????ã?????ñcdeínrš?t?úužd ]*$/;
        var flag1 = this.validateGivenName(component);
        console.log('flag1 is ' + flag1);
        var flag2 = 0;
         if(component.get("v.userType") === 'Guest'){
            var  FamilyNameVal = component.get("v.FamilyName");
            var FamilyName = component.find("FamilyName");
            if (FamilyNameVal == '' || typeof FamilyNameVal == 'undefined'){
                FamilyName.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Surname_EM")}]);
                isValidForm = false;
            } else if(reg1.test(FamilyNameVal) == false) {
                FamilyName.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Surname_Valid_EM")}]);
                isValidForm = false;
            }else {
                FamilyName.set("v.errors", null);
                flag2 = 1;
            }
            
            if(flag1===1 && flag2===1){
                var fullName = GivenNameVal.trim()+' '+FamilyNameVal.trim();
                component.set("v.newCase.SuppliedName",fullName);
                component.set("v.newCase.Manual_Contact_First_Name__c", GivenNameVal.trim());
                component.set("v.newCase.Manual_Contact_Last_Name__c", FamilyNameVal.trim());
            }
         }else{
                          newCase.SuppliedName = null;  
         }
                //console.log('B')

                component.set("v.newCase",newCase);
                component.set("v.isValidForm",isValidForm);

    },
    validateEmail: function(component){
                //console.log('C')
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.userType") === 'Guest'){
            var  emailId = component.find("emailId");
            if(typeof emailId != 'undefined'){
                var emailIdVal = newCase.SuppliedEmail;
                var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                //var reg3 = (/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/);
                if (emailIdVal == '' || typeof emailIdVal == 'undefined' || emailIdVal == null) {
                    emailId.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Email_EM")}]);
                    isValidForm = false;
                } else if (!reg.test(emailIdVal)){
                    emailId.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Email_ValidEM")}]);
                    isValidForm = false;
                }else {
                    emailId.set("v.errors", null);
                }
            }
        }else{
            newCase.SuppliedEmail = null;
        }
        //console.log('C')
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
                
    },
    validateContactMobile: function(component){
                //console.log('D')
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
      //  var reg1 = (/^[a-zA-Z ]*$/);
        
        if(component.get("v.userType") === 'Guest'){
            
            var mobilephone = component.find("mobilephone");
            mobilephone.showHelpMessageIfInvalid();
            if(!mobilephone.get('v.validity').valid) {
/***********************************************************************
                // mobilephone is a <lightning:input> which does not accept the following line
                mobilephone.set("v.errors", [{message: $A.get("$Label.c.AA_Case_TelephoneValid_EM")}]);
                // instead, set one of the "messageWhen" attributes
***********************************************************************/
                mobilephone.set("v.messageWhenBadInput", $A.get("$Label.c.AA_Case_TelephoneValid_EM"));
                isValidForm = false;
            } else {
/***********************************************************************
                // mobilephone is a <lightning:input> which does not accept the following line
                mobilephone.set("v.errors", null);
                // do nothing
***********************************************************************/
            }
			
            /***********************************
			var mobilephoneVal = newCase.ContactMobile;
            if(mobilephoneVal == '' || typeof mobilephoneVal == 'undefined' || mobilephoneVal == null){
                mobilephone.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Telephone_EM")}]);
                isValidForm = false;
            }else if((mobilephoneVal.length) > 10 ||reg1.test(mobilephoneVal) == true || /[^0-9-]/.test( mobilephoneVal )) {
                mobilephone.set("v.errors", [{message: $A.get("$Label.c.AA_Case_TelephoneValid_EM")}]);
                isValidForm = false;
            }else{
                mobilephone.set("v.errors", null);
            }*********************/
        }else{
            newCase.ContactMobile = null;
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateAlternateNumber: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");

        var alternatephone = component.find("alternatephone");

        alternatephone.showHelpMessageIfInvalid();
        if(!alternatephone.get('v.validity').valid) {
/***********************************************************************
            // alternatephone is a <lightning:input> which does not accept the following line
            alternatephone.set("v.errors", [{message: $A.get("$Label.c.AA_Case_TelephoneValid_EM")}]);
            // instead, set one of the "messageWhen" attributes
***********************************************************************/
            alternatephone.set("v.messageWhenBadInput", $A.get("$Label.c.AA_Case_TelephoneValid_EM"));
            isValidForm = false;
        } else {
/***********************************************************************
            // alternatephone is a <lightning:input> which does not accept the following line
            alternatephone.set("v.errors", null);
            // do nothing
***********************************************************************/
        }
        
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validatesubcat1: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");

        if(component.get("v.isSubCat") === true){
            var subCat1 = component.find("subCat1");
            if(typeof subCat1 != 'undefined'){
                var subCat1Val = newCase.Sub_Category_1__c;
                if(subCat1Val == '' || typeof subCat1Val == 'undefined'){
                    subCat1.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Subcat_EM")}]);
                    isValidForm = false;
                }else{
                    subCat1.set("v.errors", null);
                }
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validatesubcat2: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.isSubCat1Others") === true){
            var subCat2 = component.find("subCat2");
            if(typeof subCat2 != 'undefined'){
                var subCat2Val = newCase.Sub_Category_2__c;
                if(subCat2Val == '' || typeof subCat2Val == 'undefined'){
                    subCat2.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Subcat_EM")}]);
                    isValidForm = false;
                }else{
                    subCat2.set("v.errors", null);
                }
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validatesubcat3: function(component){
                //console.log('H')
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.isSubCat2Others") === true){
            var subCat3 = component.find("subCat3");
            if(typeof subCat3 != 'undefined'){
                var subCat3Val = newCase.Sub_Category_3__c;
                if(subCat3Val == '' || typeof subCat3Val == 'undefined'){
                    subCat3.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Subcat_EM")}]);
                    isValidForm = false;
                }else{
                    subCat3.set("v.errors", null);
                }
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validatesubject: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        var SearchText = component.find("SearchText");
        if(typeof SearchText != 'undefined'){
            var SearchTextVal = newCase.Subject;
            if (SearchTextVal == '' || typeof SearchTextVal == 'undefined' || SearchTextVal.length < 4) {
                SearchText.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Subject_EM")}]);
                isValidForm = false;
            } else {
                SearchText.set("v.errors", null);
            }
        }
                //console.log('I')
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBookingNumber: function(component){
                //console.log('J')
     	var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");  
        
        var BookingNumber = component.find("BookingNumber");
        if(typeof BookingNumber != 'undefined'){
            var BookingNumberVal = newCase.Booking_Number__c;
            var type = component.find("type").get("v.value");
            if(type == 'Refund'){
                if( BookingNumberVal == undefined || BookingNumberVal == null || BookingNumberVal == ''  ){
                    BookingNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BookingNumber_EM")}]);
                    isValidForm = false;
                }else if(typeof BookingNumberVal != 'undefined' && BookingNumberVal.length != 6){
                    BookingNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BookingNumber_EM")}]);
                    isValidForm = false;
                }else{
                    BookingNumber.set("v.errors", null);
                }
            }
            else {
                if(typeof BookingNumberVal != 'undefined' && BookingNumberVal.length != 6 && BookingNumberVal.length != 0){
                    BookingNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BookingNumber_EM")}]);
                    isValidForm = false;
                }else{
                    BookingNumber.set("v.errors", null);
                }
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateAirlineCode: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        var type = component.find("type").get("v.value");
        if(type == 'Refund'){           
            var airlineCode = component.find("airlineCode");
            if(typeof airlineCode != 'undefined'){
                var airlineCodeVal = newCase.Airline_Code__c;
                if(airlineCodeVal == '' || airlineCodeVal == component.get("v.defaultCategoryValue")){
                    airlineCode.set("v.errors", [{message: $A.get("$Label.c.AA_Case_AirlineCode_EM")}]);
                    isValidForm = false;
                }else{
                    airlineCode.set("v.errors", null);
                }
            }
        }else{
            var airlineCode = component.find("airlineCode");
             airlineCode.set("v.errors", null);
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateFlightNumber: function(component){

        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        var type = component.find("type").get("v.value");
        var flightNumber = component.find("FlightNumber");
        flightNumber.set("v.errors", null);
        var flightRegex = /^([0-9]{2,4})?$/;
        if(typeof flightNumber != 'undefined'){
            var flightNumberVal = newCase.Flight_Number__c;
            if(type == 'Refund'){
                if(flightNumberVal == '' || typeof flightNumberVal == 'undefined' || flightNumberVal.length < 2 || flightRegex.test(flightNumberVal) === false){
                    flightNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_FlightNumber_EM")}]);
                    isValidForm = false;
                }
            } else if(typeof flightNumberVal != 'undefined' && flightNumberVal != '' && flightRegex.test(flightNumberVal) === false) {
                flightNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_FlightNumber_EM")}]);
                isValidForm = false;
            }
        }

        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validatedeptDateTime: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        var type = component.find("type").get("v.value");
        if(type == 'Refund'){
            var deptDateTime = component.find("deptDateTime");
            if(typeof deptDateTime != 'undefined'){
                var deptDateTimeVal = newCase.Departure_Date_and_Time__c;
                if(typeof deptDateTimeVal == 'undefined'){
                    deptDateTime.set("v.errors", [{message: $A.get("$Label.c.AA_Case_DeptDateTime_EM")}]);
                    isValidForm = false;
                }
                else{
                    deptDateTime.set("v.errors", null);
                }
            }
        }else{
            var deptDateTime = component.find("deptDateTime");
            deptDateTime.set("v.errors", null);
        }
                //console.log('M')
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBookingPayment: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showBooking") === true){
            var bookingPayment = component.find("bookingPayment");
            if(typeof bookingPayment != 'undefined'){
                var bookingPaymentVal = newCase.Booking_Payment__c;
                if(bookingPaymentVal == ''){
                    bookingPayment.set("v.errors", [{message: $A.get("$Label.c.AA_Case_PaymentMode_EM")}]);
                    isValidForm = false;
                }else{
                    bookingPayment.set("v.errors", null);
                }
            }
        }else{
            newCase.Booking_Payment__c = null;
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);

    },
    validateCurrency: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        
        if(component.get("v.showCurrency") === true){
            var bankCurrency = component.find("bankCurrency");
            var bankCurrencyVal = newCase.Case_Currency__c;
            console.log('Entered Bank Currency')
            if(bankCurrencyVal == '' || bankCurrencyVal == 'undefined' || bankCurrencyVal == null){
                bankCurrency.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Currency_EM")}]);
                isValidForm = false;
            }else{
                bankCurrency.set("v.errors", null);
            }
        }else{
            newCase.Case_Currency__c = null;
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankName: function(component){
        var reg1 = /^[a-zA-Z0-9 ]*$/;
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBN") === true){
                var bankName = component.find("bankName");
                var bankNameVal = newCase.Bank_Name__c;
                if(bankNameVal == '' || bankNameVal == 'undefined' || bankNameVal == null){
                    bankName.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Bank_Name_EM")}]);
                    isValidForm = false;
                }else if(reg1.test(bankNameVal) == false){
                    bankName.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Bank_Name_Valid_EM")}]);
                    isValidForm = false;
                }else{
                    bankName.set("v.errors", null);   
                }
            }else{
                newCase.Bank_Name__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankNameinC: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBNinC") === true){
                var BankNameinC = component.find("BankNameinC");
                //var BankNameinCVal = newCase.Bank_Name_in_Chinese_language__c;
                //if (BankNameinCVal == '' || typeof BankNameinCVal == 'undefined' || BankNameinCVal == null) {
                if(this.validateLightningInputElement(component, 'BankNameinC') === false) {
/**************************************************************************
                    // bankNameinC is a <lightning:input> so the following line is invalid
                    BankNameinC.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankNameInChinese_EM")}]);
                    // instead, set one of the "messageWhen..." attributes
**************************************************************************/
                    BankNameinC.set("v.messageWhenInvalidInput", $A.get("$Label.c.AA_Case_BankNameInChinese_EM"));
                    isValidForm = false;
                }else {
/**************************************************************************
                    // bankNameinC is a <lightning:input> so the following line is invalid
                    BankNameinC.set("v.errors", null);
                    // do nothing
**************************************************************************/
                }
            }else{
                newCase.Bank_Name_in_Chinese_language__c = null;
            } 
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateLightningInputElement: function(component, auraElementId){
        var isValidForm = false;
        try {
            var inputCmp = component.find(auraElementId);
            inputCmp.showHelpMessageIfInvalid();
            isValidForm = inputCmp.get('v.validity').valid;
        } catch(err) {
            console.log('Error while validating components - ', err);
        }
        return isValidForm;
    },
    validateBankAccountHolderinChinese: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBHNinC") === true){
                var BankNameinC = component.find("BankAHNinC");
                if(this.validateLightningInputElement(component, 'BankAHNinC') === false) {
 /**************************************************************************
                    // BankAHNinC is a <lightning:input> so the following line is invalid
                    BankNameinC.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Bank_Account_Holder_Name_in_ChineseEM")}]);
                    // instead, set one of the "messageWhen..." attributes
**************************************************************************/
                    BankNameinC.set("v.messageWhenInvalidInput", $A.get("$Label.c.AA_Case_Bank_Account_Holder_Name_in_ChineseEM"));
                    isValidForm = false;
                }else {
/**************************************************************************
                    // BankAHNinC is a <lightning:input> so the following line is invalid
                    BankNameinC.set("v.errors", null);
                    // do nothing
**************************************************************************/
                }
            }else{
                newCase.Bank_Holder_Name_in_Chinese_language__c = null;
            } 
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankAccountHolderName: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        
        var regJapPattern = /^[\uFF00-\uFFEF ]*$/;
        var caseCurrency = component.find("bankCurrency").get("v.value");
        var airlineCode = component.find("airlineCode").get("v.value");

        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBHN") === true){
                var bankAccountHolderName = component.find("bankAccountHolderName");
                var bankAccountHolderNameval = newCase.Bank_Holder_Name__c;
                
                var isRegValid = regPattern.test(bankAccountHolderNameval);
                if((airlineCode === 'DJ' && caseCurrency === 'JPY') && !isRegValid) {
                	isRegValid = regJapPattern.test(bankAccountHolderNameval);
                }
                
                if (bankAccountHolderNameval == '' || typeof bankAccountHolderNameval == 'undefined' || bankAccountHolderNameval == null) {
                    bankAccountHolderName.set("v.errors", [{message: $A.get("$Label.c.AA_Case_AccountHolderName_EM")}]);
                    isValidForm = false;
                } else if(!isRegValid){
                    bankAccountHolderName.set("v.errors", [{message: $A.get("$Label.c.AA_Case_AccountHolderName_EM")}]);
                    isValidForm = false;
                } else {
                    bankAccountHolderName.set("v.errors", null);
                }
            }else{
                newCase.Bank_Holder_Name__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankAccountNumber: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[0-9]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBAN") === true){
                var bankAccountNumber = component.find("bankAccountNumber");
                var bankAccountNumberVal = newCase.Bank_Account_Number__c;
                console.log('bankAccountNumberVal  '+ bankAccountNumberVal)
                if (bankAccountNumberVal == '' || typeof bankAccountNumberVal == 'undefined' || bankAccountNumberVal == null) {
                    bankAccountNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Bank_Account_Number_EM")}]);
                    isValidForm = false;
                } else if(isNaN(bankAccountNumberVal) || bankAccountNumberVal.length>19){
                    bankAccountNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankAccountNumber_Valid_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(bankAccountNumberVal) == false){
                    bankAccountNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankAccountNumber_Valid_EM")}]);
                    isValidForm = false;
                } else{
                    bankAccountNumber.set("v.errors", null);
                }
            }else{
                newCase.Bank_Account_Number__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validatebankAccountHHAddress: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBAHHA") === true){
                var bankAccountHHAddress = component.find("bankAccountHHAddress");
                var bankAccountHHAddressVal = newCase.Bank_Account_Holder_Home_Address__c;
                if (bankAccountHHAddressVal == '' || typeof bankAccountHHAddressVal == 'undefined' || bankAccountHHAddressVal == null) {
                    bankAccountHHAddress.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankAccountHolderHomeAddresss_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(bankAccountHHAddressVal) == false){
                    bankAccountHHAddress.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankAccountHolderHomeAddresss_EM")}]);
                    isValidForm = false;
                } else {
                    bankAccountHHAddress.set("v.errors", null);
                }
            }else{
                newCase.Bank_Account_Holder_Home_Address__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBAHHANonE: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
        	if(component.get("v.showBAHHANonE") === true){
                var BAHHANonE = component.find("BAHHANonE");
                //var BAHHANonEVal = newCase.Bank_Account_Holder_Home_Address_Non_En__c;
               // if (BAHHANonEVal == '' || typeof BAHHANonEVal == 'undefined' || BAHHANonEVal == null) {
                if(this.validateLightningInputElement(component, 'BAHHANonE') === false) {
                   /**************************************************************************
                    // BAHHANonE is a <lightning:input> so the following line is invalid
                    BAHHANonE.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankAccountHolderAddress_EM")}]);
                    // instead, set one of the "messageWhen..." attributes
**************************************************************************/
                    BAHHANonE.set("v.messageWhenBadInput", $A.get("$Label.c.AA_Case_BankAccountHolderAddress_EM"));
                    isValidForm = false;
                } else {
/**************************************************************************
                    // BAHHANonE is a <lightning:input> so the following line is invalid
                    BAHHANonE.set("v.errors", null);
                    // do nothing
**************************************************************************/
                }
            }else{
                newCase.Bank_Account_Holder_Home_Address_Non_En__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
        
    },
    validateBankBranch: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBB") === true){
                var bankBranch = component.find("bankBranch");
                var bankBranchVal = newCase.Bank_Branch__c;
                if (bankBranchVal == '' || typeof bankBranchVal == 'undefined' || bankBranchVal == null) {
                    bankBranch.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankBranch_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(bankBranchVal) == false){
                    bankBranch.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankBranch_EM")}]);
                    isValidForm = false;
                } else {
                    bankBranch.set("v.errors", null);
                }
            }else{
                newCase.Bank_Branch__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankBranchinC: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBBinC") === true){
                var BankBranchinC = component.find("BankBranchinC");
                //var BankBranchinCVal = newCase.Bank_Branch_in_Chinese_language__c;
                if(this.validateLightningInputElement(component, 'BankBranchinC') === false) {
                //if (BankBranchinCVal == '' || typeof BankBranchinCVal == 'undefined' || BankBranchinCVal == null) {
                    /**************************************************************************
                    // BankBranchinC is a <lightning:input> so the following line is invalid
                    BankBranchinC.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankBranchinChinese_EM")}]);
                    // instead, set one of the "messageWhen..." attributes
**************************************************************************/
                    BankBranchinC.set("v.messageWhenBadInput", $A.get("$Label.c.AA_Case_BankBranchinChinese_EM"));
                    isValidForm = false;
                } else {
/**************************************************************************
                    // BAHHANonE is a <lightning:input> so the following line is invalid
                    BankBranchinC.set("v.errors", null);
                    // do nothing
**************************************************************************/
                }
            }else{
                newCase.Bank_Branch_in_Chinese_language__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
        
    },
    validateBankCountry: function(component){
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBoC") === true){
                var BankCountry = component.find("BankCountry");
                var BankCountryVal = newCase.Country_of_Bank__c;
                
                if (BankCountryVal == '' || typeof BankCountryVal == 'undefined' || BankCountryVal == null ) {
                    BankCountry.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Country_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(BankCountryVal) == false) {
                    BankCountry.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Country_Valid_EM")}]);
                    isValidForm = false;
                }else {
                    BankCountry.set("v.errors", null);
                }
            }else{
                newCase.Country_of_Bank__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankAddress: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBA") === true){
                var BankAddress = component.find("BankAddress");
                var BankAddressVal = newCase.Bank_Address__c;
                if (BankAddressVal == '' || typeof BankAddressVal == 'undefined' || BankAddressVal == null) {
                    BankAddress.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankAddress_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(BankAddressVal) == false){
                    BankAddress.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankAddress_EM")}]);
                    isValidForm = false;
                } else {
                    BankAddress.set("v.errors", null);
                }
            }else{
                newCase.Bank_Address__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBAinNonE: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBAinNonE") === true){
                var BAinNonE = component.find("BAinNonE");
               // var BAinNonEVal = newCase.Bank_Address_Non_English__c;
                //if (BAinNonEVal == '' || typeof BAinNonEVal == 'undefined' || BAinNonEVal == null) {
                if(this.validateLightningInputElement(component, 'BAinNonE') === false) {
                    /**************************************************************************
                    // BAinNonE is a <lightning:input> so the following line is invalid
                    BAinNonE.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankAddressinNonE_EM")}]);
                    // instead, set one of the "messageWhen..." attributes
**************************************************************************/
                    BAinNonE.set("v.messageWhenBadInput", $A.get("$Label.c.AA_Case_BankAddressinNonE_EM"));
                    isValidForm = false;
                } else {
/**************************************************************************
                    // BAinNonE is a <lightning:input> so the following line is invalid
                    BAinNonE.set("v.errors", null);
                    // do nothing
**************************************************************************/
                }
            }else{
                newCase.Bank_Address_Non_English__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateProvince: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
        if(component.get("v.showProvince") === true){
                var Province = component.find("Province");
                var ProvinceVal = newCase.Province__c;
                if (ProvinceVal == '' || typeof ProvinceVal == 'undefined' || ProvinceVal == null ) {
                    Province.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Province_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(ProvinceVal) == false){
                    Province.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Province_EM")}]);
                    isValidForm = false;
                } else {
                    Province.set("v.errors", null);
                }
            }else{
                newCase.Province__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
        validateProvinceinC: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showProvinceinC") === true){
                var ProvinceinC = component.find("ProvinceinC");
                if(this.validateLightningInputElement(component, 'ProvinceinC') === false) {
                    // var ProvinceinCVal = newCase.Province_in_Chinese_language__c;
                    //if (ProvinceinCVal == '' || typeof ProvinceinCVal == 'undefined' || ProvinceinCVal == null) {
/**************************************************************************
                    // ProvinceinC is a <lightning:input> so the following line is invalid
                    ProvinceinC.set("v.errors", [{message: $A.get("$Label.c.AA_Case_ProvinceinChinese_EM")}]);
                    // instead, set one of the "messageWhen..." attributes
**************************************************************************/
                    ProvinceinC.set("v.messageWhenBadInput", $A.get("$Label.c.AA_Case_ProvinceinChinese_EM"));
                    isValidForm = false;
                } else {
/**************************************************************************
                    // ProvinceinC is a <lightning:input> so the following line is invalid
                    ProvinceinC.set("v.errors", null);
                    // do nothing
**************************************************************************/
                }
            } else {
                newCase.Province_in_Chinese_language__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateCity: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showCity") === true){
                var City = component.find("City");
                var CityVal = newCase.City__c;
                if (CityVal == '' || typeof CityVal == 'undefined' || CityVal == null) {
                    City.set("v.errors", [{message: $A.get("$Label.c.AA_Case_City_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(CityVal) == false){
                    City.set("v.errors", [{message: $A.get("$Label.c.AA_Case_City_EM")}]);
                    isValidForm = false;
                } else {
                    City.set("v.errors", null);
                }
            }else{
                newCase.City__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
      validateCityinC: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showCityinC") === true){
                var CityinC = component.find("CityinC");
                if(this.validateLightningInputElement(component, 'CityinC') === false) {
                    // var CityinCVal = newCase.City_in_Chinese_language__c;
                    //if (CityinCVal == '' || typeof CityinCVal == 'undefined' || CityinCVal == null) {
/**************************************************************************
                    // CityinC is a <lightning:input> so the following line is invalid
                    CityinC.set("v.errors", [{message: $A.get("$Label.c.AA_Case_CityinChinese_EM")}]);
                    // instead, set one of the "messageWhen..." attributes
**************************************************************************/
                    CityinC.set("v.messageWhenBadInput", $A.get("$Label.c.AA_Case_CityinChinese_EM"));
                    isValidForm = false;
                } else {
/**************************************************************************
                    // CityinC is a <lightning:input> so the following line is invalid
                    CityinC.set("v.errors", null);
                    // do nothing
**************************************************************************/
                }
            }else{
                newCase.City_in_Chinese_language__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankBCode: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBBC ") === true){
                var BankBCode = component.find("BankBCode");
                var BankBCodeVal = newCase.Bank_Branch_Code__c;
                if (BankBCodeVal == '' || typeof BankBCodeVal == 'undefined' || BankBCodeVal == null) {
                    BankBCode.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankBranchCode_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(BankBCodeVal) == false){
                    BankBCode.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankBranchCode_EM")}]);
                    isValidForm = false;
                } else {
                    BankBCode.set("v.errors", null);
                }
            }else{
                newCase.Bank_Branch_Code__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankCode: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBC") === true){
                var BankCode = component.find("BankCode");
                var BankCodeVal = newCase.Bank_Code__c;
                if (BankCodeVal == '' || typeof BankCodeVal == 'undefined' || BankCodeVal == null) {
                    BankCode.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankCode_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(BankCodeVal) == false){
                    BankCode.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BankCode_EM")}]);
                    isValidForm = false;
                } else {
                    BankCode.set("v.errors", null);
                }
            }else{
                newCase.Bank_Code__c = null;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateGender: function(component){
         //console.log('7');        
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBAHG") === true){
                var gender = component.find("gender");
                var genderVal = newCase.Bank_Account_Holder_Gender__c;
                if(genderVal == '' || genderVal == null){
                    gender.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Gender_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(genderVal) == false){
                    gender.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Gender_EM")}]);
                    isValidForm = false;
                } else{
                    gender.set("v.errors", null);
                }
            }else{
                newCase.Bank_Account_Holder_Gender__c = null;
            }
        }
        //console.log('7');
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validatetelephone: function(component){
        //console.log('8');     
        var reg1 = (/^[a-zA-Z ]*$/);
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBAHTN") === true){
                var telephone = component.find("telephone");
                var telephoneVal = newCase.Bank_Account_Holder_Telephone_Number__c;
                var phoneno = /^\d{10}$/;
                if(telephoneVal == '' || typeof telephoneVal == 'undefined' || telephoneVal == null){
                    telephone.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Telephone_EM")}]);
                    isValidForm = false;
                }else if(telephoneVal.length > 10 ||reg1.test(telephoneVal) == true || /[^0-9-/]/.test( telephoneVal )) {
                    telephone.set("v.errors", [{message: $A.get("$Label.c.AA_Case_TelephoneValid_EM")}]);
                    isValidForm = false;
                }else{
                    telephone.set("v.errors", null);
                }
            }else{
                newCase.Bank_Account_Holder_Telephone_Number__c = null;
            }
            
        }
        //console.log('8');
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateBankAHIdentity: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var regPattern = /^[a-zA-Z0-9 ]*$/;
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBAHINP") === true){
                var BankAHIdentity = component.find("BankAHIdentity");
                var BankAHIdentityVal = newCase.Bank_Account_Holder_Identity_Number_Pass__c;
                if(BankAHIdentityVal == '' || typeof BankAHIdentityVal == 'undefined' || BankAHIdentityVal == null){
                    BankAHIdentity.set("v.errors", [{message: $A.get("$Label.c.AA_Case_AccountHolderIdentityNumber_EM")}]);
                    isValidForm = false;
                } else if(regPattern.test(BankAHIdentityVal) == false){
                    BankAHIdentity.set("v.errors", [{message: $A.get("$Label.c.AA_Case_AccountHolderIdentityNumber_EM")}]);
                    isValidForm = false;
                } else{
                    BankAHIdentity.set("v.errors", null);
                }
            }else{
                newCase.Bank_Account_Holder_Identity_Number_Pass__c = null;           
            }
        }
        //console.log('8');
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateAccountType: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showAcctType") === true){
                var acctType = component.find("acctType");
                var acctTypeVal = newCase.Account_Type__c;
                if(acctTypeVal == '' || acctTypeVal == null){
                    acctType.set("v.errors", [{message: $A.get("$Label.c.AA_Case_AccountType_EM")}]);
                    isValidForm = false;
                }else{
                    acctType.set("v.errors", null);
                }
            }else{
                newCase.Account_Type__c = null;           
            }
        }
        //console.log('9');
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateIBCode: function(component){
        //console.log('10');     
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            
            if(component.get("v.showBSIIBS") === true){
                var internationalBankCode = component.find("internationalBankCode");
                if(typeof internationalBankCode != 'undefined'){
                    var internationalBankCodeVal = newCase.International_Bank_Code__c;
                    if(internationalBankCodeVal == '' || internationalBankCodeVal == component.get("v.defaultCategoryValue") || internationalBankCodeVal == null){
                        internationalBankCode.set("v.errors", [{message: $A.get("$Label.c.AA_Case_InternationalBankCode_EM")}]);
                        isValidForm = false;
                    }else{
                        internationalBankCode.set("v.errors", null);
                    }
                } 
            }else{
                newCase.International_Bank_Code__c = null;
                
            }
            
        }
        //console.log('10');
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
        
    },
    validateIBCodeText: function(component){
        //console.log('11');     
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showBSIIBS") === true){
                var internationalBankNumber = component.find("internationalBankNumber");
                var internationalBankNumberVal = newCase.International_Bank_Number__c;
                if(internationalBankNumberVal == '' || typeof internationalBankNumberVal == 'undefined' || internationalBankNumberVal == null){
                    internationalBankNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_InternationalBankNumber_EM")}]);
                    isValidForm = false;
                }else{
                    internationalBankNumber.set("v.errors", null);
                } 
            }else{
                newCase.International_Bank_Number__c = null;
            }
        }
        //console.log('11');
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
        
    },
    validateComponents: function(component, cmpName){
        var isValidForm = component.get("v.isValidForm");
        try {
            var inputCmp = component.find(cmpName);
            inputCmp.showHelpMessageIfInvalid();
            if(inputCmp.get('v.validity').valid === false) {
                isValidForm = false;    
            }
        } catch(err) {
            console.log('Error while validating components - ', err);
        }
        component.set("v.isValidForm",isValidForm);
    },
    validateElements: function(component){
        
        var isValidForm = component.get("v.isValidForm");
        try {
            var caseElementsVisible = component.get("v.caseElementsVisible");
            var dynamicComponents = component.find("caseElementAuraId");
            
            for(elementKey in dynamicComponents) {
                var inputCmp = dynamicComponents[elementKey];
                if(caseElementsVisible.indexOf(inputCmp.get("v.name")) > -1) {
                    inputCmp.showHelpMessageIfInvalid();
                    if(inputCmp.get('v.validity').valid === false) {
                    	isValidForm = false;    
                    }
                }
            }
            
        } catch(err) {
            console.log('Error while validating components - ', err);
        }
        component.set("v.isValidForm",isValidForm);
    },
    validateFlightSectorToBeRefunded: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showFlightRefundOpt") === true){
            var flightRefundOpt = component.find("flightSectorToBeRefunded");
            if(typeof flightRefundOpt != 'undefined'){
                var flightRefundOptVal = newCase.Flight_Sector_to_be_Refunded__c;
                if(!flightRefundOptVal){
                    flightRefundOpt.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Flight_Sector_to_be_Refunded_EM")}]);
                    isValidForm = false;
                }else{
                    flightRefundOpt.set("v.errors", null);
                }
            }
        }else{
            newCase.Flight_Sector_to_be_Refunded__c = null;
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validatePassengerToBeRefunded: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPassengerRefundOpt") === true){
            var passengerRefundOpt = component.find("passengerToBeRefunded");
            if(typeof passengerRefundOpt != 'undefined'){
                var passengerRefundOptVal = newCase.Passenger_to_be_Refunded__c;
                if(!passengerRefundOptVal){
                    passengerRefundOpt.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Passenger_to_be_Refunded_EM")}]);
                    isValidForm = false;
                }else{
                    passengerRefundOpt.set("v.errors", null);
                }
            }
        }else{
            newCase.Passenger_to_be_Refunded__c = null;
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateFlightDetail: function(component, fieldName){
    	var tempStr = "component.get(\"v." + fieldName + "Value\")";
        var flightValue = eval(tempStr);
        var isValidForm = component.get("v.isValidForm");
		var reg1 = /^([0-9]{2,4})?$/;
        var flag1=0;
        tempStr = "component.find(\"" + fieldName + "\")";
        var flightField = eval(tempStr);
        if (!flightValue){
            if(fieldName.includes("airline"))
            	flightField.set("v.errors", [{message: $A.get("$Label.c.AA_Case_AirlineCode_EM")}]);
            else
            	flightField.set("v.errors", [{message: $A.get("$Label.c.AA_Case_FlightNumber_EM")}]);
            isValidForm = false;
        } else if(fieldName.includes("flight") && reg1.test(flightValue) == false) {
            flightField.set("v.errors", [{message: $A.get("$Label.c.AA_Case_FlightNumber_EM")}]);
            isValidForm = false;
        }else {
            flightField.set("v.errors", null);
            flag1 = 1;
        }
        component.set("v.isValidForm",isValidForm);                  
        return flag1; 
	},
    validateNameField: function(component, fieldName){
        var tempStr = "component.get(\"v." + fieldName + "Value\")";
        var nameValue = eval(tempStr);
        var isValidForm = component.get("v.isValidForm");
        console.log('WHAT IS HERE' + isValidForm)
        var reg1 = /^[a-zA-Zaâáéíîó????ã?????ñcdeínrš?t?úužd ]*$/;
		var flag1=0;
        tempStr = "component.find(\"" + fieldName + "\")";
        var nameField = eval(tempStr);
        if (nameValue == '' || typeof nameValue == 'undefined'){
            if(fieldName.includes("given"))
            	nameField.set("v.errors", [{message: $A.get("$Label.c.AA_Case_First_Name_EM")}]);
            else
            	nameField.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Surname_EM")}]);
            isValidForm = false;
        } else if(reg1.test(nameValue) == false) {
            if(fieldName.includes("given"))
            	nameField.set("v.errors", [{message: $A.get("$Label.c.AA_Case_First_Name_ValidEM")}]);
            else
            	nameField.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Surname_Valid_EM")}]);
            isValidForm = false;
        }else {
            nameField.set("v.errors", null);
            flag1 = 1;
        }
        
        component.set("v.isValidForm",isValidForm);                  
        return flag1; 
    },
    validateComment: function(component){
        //console.log('12');     
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.showPaymentRelatedFields") === true || component.get("v.showpersonald") === true){
            if(component.get("v.showComment") === true){
                var commentbox = component.find("commentbox");
                var commentboxVal = newCase.Comments;
                if(commentboxVal == '' || typeof commentboxVal == 'undefined' || commentboxVal == null){
                    commentbox.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Comment_EM")}]);
                    isValidForm = false;
                }else{
                    commentbox.set("v.errors", null);
                }
            }else{
                newCase.Comments = null;           
            }
        }
        //console.log('12');
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    validateFields: function(component){
        
        if(component.get("v.userType") === 'Guest'){
            // Validate Salutation
            this.validateComponents(component, 'Salutation');
        }
        
        this.validateGivenName(component);
        //console.log('1:' + component.get("v.isValidForm",isValidForm));
        this.validateFamilyName(component);
        //console.log('2:' + component.get("v.isValidForm",isValidForm));
        this.validateEmail(component);
        //console.log('3:' + component.get("v.isValidForm",isValidForm));
        this.validateContactMobile(component);
        //console.log('4:' + component.get("v.isValidForm",isValidForm));
        this.validateAlternateNumber(component);
        //console.log('5:' + component.get("v.isValidForm",isValidForm));
        this.validatesubcat1(component);
        //console.log('6:' + component.get("v.isValidForm",isValidForm));
        this.validatesubcat2(component);
        //console.log('7:' + component.get("v.isValidForm",isValidForm));
        this.validatesubcat3(component);
        //console.log('8:' + component.get("v.isValidForm",isValidForm));
        this.validatesubject(component);
        //console.log('9:' + component.get("v.isValidForm",isValidForm));
        this.validateBookingNumber(component);
        //console.log('10:' + component.get("v.isValidForm",isValidForm));
        this.validateAirlineCode(component);
        //console.log('11:' + component.get("v.isValidForm",isValidForm));
        this.validateFlightNumber(component);
        //console.log('12:' + component.get("v.isValidForm",isValidForm));
        this.validatedeptDateTime(component);
        //console.log('13:' + component.get("v.isValidForm",isValidForm));
        this.validateBookingPayment(component);
        //console.log('14:' + component.get("v.isValidForm",isValidForm));
    	this.validateCurrency(component);
        //console.log('15:' + component.get("v.isValidForm",isValidForm));
        this.validateBankName(component);
        //console.log('16:' + component.get("v.isValidForm",isValidForm));
        this.validateBankNameinC(component);
        //console.log('17:' + component.get("v.isValidForm",isValidForm));
        this.validateBankAccountHolderName(component);
        this.validateBankAccountHolderinChinese(component);
        //console.log('18:' + component.get("v.isValidForm",isValidForm));
        this.validateBankAccountNumber(component);
        //console.log('19:' + component.get("v.isValidForm",isValidForm));
        this.validatebankAccountHHAddress(component);
        //console.log('20:' + component.get("v.isValidForm",isValidForm));
        this.validateBAHHANonE(component);
        //console.log('21:' + component.get("v.isValidForm",isValidForm));
        this.validateBankBranch(component);
        //console.log('22:' + component.get("v.isValidForm",isValidForm));
        this.validateBankBranchinC(component);
        //console.log('23:' + component.get("v.isValidForm",isValidForm));
        this.validateBankCountry(component);
        //console.log('24:' + component.get("v.isValidForm",isValidForm));
        this.validateBankAddress(component);
        //console.log('25:' + component.get("v.isValidForm",isValidForm));
        this.validateBAinNonE(component);
        //console.log('26:' + component.get("v.isValidForm",isValidForm));
        this.validateProvince(component);
        //console.log('27:' + component.get("v.isValidForm",isValidForm));
        this.validateProvinceinC(component);
        //console.log('28:' + component.get("v.isValidForm",isValidForm));
        this.validateCity(component);
        //console.log('29:' + component.get("v.isValidForm",isValidForm));
        this.validateCityinC(component);
        //console.log('30:' + component.get("v.isValidForm",isValidForm));
        this.validateBankBCode(component);
        //console.log('31:' + component.get("v.isValidForm",isValidForm));
        this.validateBankCode(component);
        //console.log('32:' + component.get("v.isValidForm",isValidForm));
        this.validateGender(component);
        //console.log('33:' + component.get("v.isValidForm",isValidForm));
        this.validatetelephone(component);
        //console.log('34:' + component.get("v.isValidForm",isValidForm));
        this.validateBankAHIdentity(component);
        //console.log('35:' + component.get("v.isValidForm",isValidForm));
        this.validateAccountType(component);
        //console.log('36:' + component.get("v.isValidForm",isValidForm));
    	//this.validateIBCode(component);
        //console.log('37:' + component.get("v.isValidForm",isValidForm));
    	//this.validateIBCodeText(component);
        //console.log('38:' + component.get("v.isValidForm",isValidForm));
        this.validatePassengerToBeRefunded(component);
        this.validateFlightSectorToBeRefunded(component);
        if(component.get("v.showFlightRefund1")){
            var tempStr = "airlineCode1";
            this.validateFlightDetail(component, tempStr);
            tempStr = "flightNumber1";
            this.validateFlightDetail(component, tempStr);
        }
        if(component.get("v.showFlightRefund2")){
            var tempStr = "airlineCode2";
            this.validateFlightDetail(component, tempStr);
            tempStr = "flightNumber2";
            this.validateFlightDetail(component, tempStr);
        }
        if(component.get("v.showFlightRefund3")){
            var tempStr = "airlineCode3";
            this.validateFlightDetail(component, tempStr);
            tempStr = "flightNumber3";
            this.validateFlightDetail(component, tempStr);
        }
        if(component.get("v.showFlightRefund4")){
            var tempStr = "airlineCode4";
            this.validateFlightDetail(component, tempStr);
            tempStr = "flightNumber4";
            this.validateFlightDetail(component, tempStr);
        }
        if(component.get("v.showFlightRefund5")){
            var tempStr = "airlineCode5";
            this.validateFlightDetail(component, tempStr);
            tempStr = "flightNumber5";
            this.validateFlightDetail(component, tempStr);
        }
        if(component.get("v.showFlightRefund6")){
            var tempStr = "airlineCode6";
            this.validateFlightDetail(component, tempStr);
            tempStr = "flightNumber6";
            this.validateFlightDetail(component, tempStr);
        }
        if(component.get("v.showPassengerRefund1")){
            var tempStr = "givenName1";
            this.validateNameField(component, tempStr);
            tempStr = "familyName1";
            this.validateNameField(component, tempStr);
        }
        if(component.get("v.showPassengerRefund2")){
            var tempStr = "givenName2";
            this.validateNameField(component, tempStr);
            tempStr = "familyName2";
            this.validateNameField(component, tempStr);
        }
        if(component.get("v.showPassengerRefund3")){
            var tempStr = "givenName3";
            this.validateNameField(component, tempStr);
            tempStr = "familyName3";
            this.validateNameField(component, tempStr);
        }
        if(component.get("v.showPassengerRefund4")){
            var tempStr = "givenName4";
            this.validateNameField(component, tempStr);
            tempStr = "familyName4";
            this.validateNameField(component, tempStr);
        }
        if(component.get("v.showPassengerRefund5")){
            var tempStr = "givenName5";
            this.validateNameField(component, tempStr);
            tempStr = "familyName5";
            this.validateNameField(component, tempStr);
        }
        if(component.get("v.showPassengerRefund6")){
            var tempStr = "givenName6";
            this.validateNameField(component, tempStr);
            tempStr = "familyName6";
            this.validateNameField(component, tempStr);
        }
        if(component.get("v.showPassengerRefund7")){
            var tempStr = "givenName7";
            this.validateNameField(component, tempStr);
            tempStr = "familyName7";
            this.validateNameField(component, tempStr);
        }
        if(component.get("v.showPassengerRefund8")){
            var tempStr = "givenName8";
            this.validateNameField(component, tempStr);
            tempStr = "familyName8";
            this.validateNameField(component, tempStr);
        }
        this.validateComment(component);
        //console.log('39:' + component.get("v.isValidForm",isValidForm));
        this.validateElements(component);

        
        var isValidForm = component.get("v.isValidForm");                             
        return isValidForm;
    },
    showToolTip: function(component, tooltipId){
        var tooltipElm = component.find(tooltipId);
        $A.util.removeClass(tooltipElm, "slds-fall-into-ground");
        $A.util.addClass(tooltipElm, "slds-rise-from-ground");
    },
    hideToolTip: function(component, tooltipId){
        var tooltipElm = component.find(tooltipId);
        $A.util.addClass(tooltipElm, "slds-fall-into-ground");
        $A.util.removeClass(tooltipElm, "slds-rise-from-ground");
    },
    fetchCountryCode: function(component) {
        var action = component.get("c.getCountryCode");
        
        //set callback   
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.countryCodeOptions", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    countryCodeChange: function(component, event) {

        var countryCodeElement = null;
        var oldCountryCodeElement = null;
        var elementName = null;

        if(event.getSource().get("v.name") === 'mobileCountryCode' || event.getSource().get("v.name") === 'mobilephone') {
            countryCodeElement = "mobileCountryCodeValue";
            oldCountryCodeElement = "oldMobileCountryCodeValue";
            elementName = "mobilephone";
        } else if(event.getSource().get("v.name") === 'alternateCountryCode' || event.getSource().get("v.name") === 'alternatephone') {
            countryCodeElement = "alternateCountryCodeValue";
            oldCountryCodeElement = "oldAlternateCountryCodeValue";
            elementName = "alternatephone";
        }
        var elementValue = !component.find(elementName).get("v.value") ? '' : component.find(elementName).get("v.value");
        var countryCodeValue = component.get("v." + countryCodeElement);
        var oldCountryCodeValue = component.get("v." + oldCountryCodeElement);

        var countryCodeData = this.getCountryCode(component, countryCodeValue, oldCountryCodeValue);
        countryCodeValue = countryCodeData.countryName + ' ';
        oldCountryCodeValue = countryCodeData.oldCountryName + ' ';

        if(elementValue.substr(0, oldCountryCodeValue.length) != oldCountryCodeValue){
            if(elementValue.length < oldCountryCodeValue.length) {
                elementValue = oldCountryCodeValue;
                component.set("v." + countryCodeElement, component.get("v." + oldCountryCodeElement));
            } else {
                if(countryCodeValue != oldCountryCodeValue) {
                    elementValue = elementValue.substr(countryCodeValue.length).trim();
                    component.set("v." + countryCodeElement, component.get("v." + oldCountryCodeElement));
                } else {
                    elementValue = '';
                }
            	elementValue = oldCountryCodeValue +  elementValue;    
            }
            component.find(elementName).set("v.value", elementValue);
        }
    },
    getCountryCode: function(component, countryName, oldCountryName) {
        var countryMap = {'countryName':null,
                          'oldCountryName': null};
  		var countryData = component.get("v.countryCodeOptions");
        
        for ( key in countryData ) {
            if(countryData[key].label === countryName) {
                countryMap['countryName'] = countryData[key].value;
            }
            if(countryData[key].label === oldCountryName) {
                countryMap['oldCountryName'] = countryData[key].value;     
            }
        }
        return countryMap;
    },
    generateCountryTelephoneInfo: function(countryName, telephoneNumber) {
        var data = '';
        try {
            var regPattern = /^\+?([0-9-]{1,6})?[ ]?([0-9]{8,15})$/;
            
            if(regPattern.test(telephoneNumber) === true) {
                data = countryName + ' ' + telephoneNumber.split(' ', 2)[1];
            }   
        } catch(err) {
            data = '';
        }
        return data;
    },
    fetchDynamicCasePageLayout: function(component){
        var action = component.get("c.getCaseLayout");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                var caseLayoutObjects = a.getReturnValue();
                component.set("v.caseLayoutObjects", caseLayoutObjects.eformLayout);
                this.transformElements(component, caseLayoutObjects.eformElement);
            }
        });
        
        $A.enqueueAction(action);
    },
    transformElements: function(component, caseElements) {
        try {
            if(caseElements) {
                for(elementKey in caseElements) {
                    if(caseElements[elementKey].Name__c) {
                        caseElements[elementKey].Name__c = $A.getReference("$Label.c." + caseElements[elementKey].Name__c);
                    }
                    if(caseElements[elementKey].Error_Value_Missing__c) {
                        caseElements[elementKey].Error_Value_Missing__c = $A.getReference("$Label.c." + caseElements[elementKey].Error_Value_Missing__c);
                    }
                    if(caseElements[elementKey].Error_Pattern_Mismatch__c) {
                        caseElements[elementKey].Error_Pattern_Mismatch__c = $A.getReference("$Label.c." + caseElements[elementKey].Error_Pattern_Mismatch__c);
                    }
                }
            }
        }catch(err) {
            console.log('Error while transforming element - ' , err)
        }
        component.set("v.caseElements", caseElements);
    },
    loadElements: function(component){
        var caseCurrency = component.find("bankCurrency").get("v.value");
        var airlineCode = component.find("airlineCode").get("v.value");
        var caseElements = component.get("v.caseElements");
        var caseLayoutObjects = component.get("v.caseLayoutObjects");
        var caseElementsVisible = [];
        component.set("v.showBSIIBS", false);
        for(elementKey in caseElements) {
            for(layoutKey in caseLayoutObjects) {
                if(caseLayoutObjects[layoutKey].Airline_Code__c === airlineCode && 
                   caseLayoutObjects[layoutKey].Case_Currency__c === caseCurrency) {
                   caseElements[elementKey].Visible__c = caseLayoutObjects[layoutKey][caseElements[elementKey].Match_Field__c];
                   caseElements[elementKey].Value = '';
                    if(caseElements[elementKey].Visible__c === true) {
                        caseElementsVisible.push(caseElements[elementKey].Match_Field__c);
                    }
                   component.set("v.showBSIIBS", true);
                   break;
                }
            }
        }
        component.set("v.caseElementsVisible", caseElementsVisible);
        component.set("v.caseElements", caseElements);
    },
    fetchCaseElements: function(component) {
        try {
            var caseElements = component.get("v.caseElements");
            var newCase = component.get("v.newCase");
            
            if(caseElements) {
                for(elementKey in caseElements) {
                    if(caseElements[elementKey].Visible__c === true) {
                        var matchField = caseElements[elementKey].Match_Field__c;
                        newCase[matchField] = caseElements[elementKey].Value;
                    }
                }
                component.set("v.newCase", newCase);
            }
        }catch(err) {
            console.log('Error while fetching case element - ' , err)
        }
    },
    loadCategories: function(component, event) {
        var controllerValueKey = event.getSource().get("v.value");
        var controllerId = event.getSource().getLocalId();
        var nextCmp = null;
        var dependentMap = null;
        // Find Next Cmp
        if(controllerId === 'type') {
            nextCmp = 'subCat1';
            dependentMap = 'depnedentFieldMap';
        } else {
            nextCmp = 'subCat2';
            dependentMap = 'depnedentCat2FieldMap';
        }
        
        var dependentMapData = component.get("v."+dependentMap);
        if (controllerValueKey != component.get("v.defaultCategoryValue")) {
            for (var singlekey in dependentMapData) {
                if(dependentMapData[singlekey].value === controllerValueKey) {
                    var listOfDependentFields = dependentMapData[singlekey].dependentList;
                    
                    this.fetchDepValues(component, listOfDependentFields, nextCmp);
                    
                    if(controllerId === 'type') {
                        this.fetchPicklistValues(component, 'Sub_Category_1__c', 'Sub_Category_2__c');
                    } else {
                        if(listOfDependentFields.length > 0) {
                            component.set("v.isSubCat1Others", true);
                        }
                    }
                }
            }
        } else {
            var defaultVal = [{
                class: "optionClass",
                label: component.get("v.defaultCategoryValue"),
                value: component.get("v.defaultCategoryValue")
            }];
            component.find(nextCmp).set("v.options", defaultVal);
        }
    },
    fetchSalutation: function(component)  {
        try {
            var action = component.get("c.fetchSalutation");
            action.setCallback(this, function(result) {
                var state = result.getState();
                if (state === "SUCCESS") {
                    var salutationList = [];
                    salutationList.push({label: component.get("v.defaultCategoryValue"),value: ""});
                    salutationList = salutationList.concat(result.getReturnValue());
                    component.set("v.SalutationList", salutationList);
                }
            });
        }catch(err) {
            console.log('Error while loading salutation -', err);
        }
        $A.enqueueAction(action);
    },
    fillCategoryData: function(component)  {
        var categoriesData = [{
                                  name : "airport tax",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_Airport_Tax"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_Airport_Tax_Data_1"), 
                                                $A.getReference("$Label.c.AA_Case_Category_Airport_Tax_Data_2")]
                              }, {
                                  name : "double payment",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_Double_Payment"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_Double_Payment_Data")]
                              }, {
                                  name : "duplicate booking",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_Duplicate_Booking"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_Duplicate_Booking_Data")]
                              }, {
                                  name : "flight cancellation",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_Flight_Cancellation"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_Flight_Cancellation_Data")]
                              }, {
                                  name : "flight reschedule",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_Flight_Reschedule"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_Flight_Reschedule_Data")]
                              }, {
                                  name : "deceased guest",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_Deceased_Guest"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_Deceased_Guest_Data")]
                              }, {
                                  name : "korea flight ticket",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_Korea_Flight_Ticket"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_Korea_Flight_Ticket_Data_1"), $A.getReference("$Label.c.AA_Case_Category_Korea_Flight_Ticket_Data_2")]
                              }, {
                                  name : "indonesia domestic",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_Indonesia_Domestic"),
                                  dataLabels : [$A.get("$Label.c.AA_Case_Category_Indonesia_Domestic_Data")],
                                  dataReplacer : [$A.get('$Resource.CC_Resources') + '/images/category_indonesia_domestic.png']
                              }, {
                                  name : "india domestic (i5)",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_India_Domestic_I5"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_India_Domestic_I5_Data")]
                              }, {
                                  name : "united states flight booking",
                                  headerLabel : $A.getReference("$Label.c.AA_Case_Category_United_States_Flight_Booking"),
                                  dataLabels : [$A.getReference("$Label.c.AA_Case_Category_United_States_Flight_Booking_Data")]
                              }];
        component.set("v.categoriesData", categoriesData);
    },
    loadModel: function(component, selectedCategory)  {
        var categoriesData = component.get("v.categoriesData");
        selectedCategory = selectedCategory.toLowerCase();
        for(index in categoriesData) {
            if(categoriesData[index].name === selectedCategory) {
                var modelData = [];
                component.set("v.Model_Header", categoriesData[index].headerLabel);
                for (dataIndex in categoriesData[index].dataLabels) {
                    var value = this.stringFormat(categoriesData[index].dataLabels[dataIndex], categoriesData[index].dataReplacer);
                    modelData.push({value: value});
                }
                component.set("v.Model_Data", modelData);
                component.set("v.isModelOpen", true);
                break;
            }
        } 
    },
    stringFormat: function(data, arguments){
        if(arguments) {
            for (var i = 0; i < arguments.length; i++) {
                var regexp = new RegExp('\\{'+i+'\\}', 'gi');
                data = data.replace(regexp, arguments[i]);
            }
        }
        return data;
    }
})