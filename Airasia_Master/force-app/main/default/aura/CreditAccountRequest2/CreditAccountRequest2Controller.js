({
    submitform : function(component, event, helper) {
        
        //console.log('coming here 1');
        var validSoFar = helper.validateFields(component, event, helper);
        console.log('validSoFar='+validSoFar);
        if(validSoFar){

            helper.createCase(component,false);
        }
    },

    submitWithAttachment : function(component, event, helper){

        var validSoFar = helper.validateFields(component, event, helper);
        if(validSoFar){

            helper.createCase(component,true);
        }

    },

    handleUploadFinished : function(component, event, helper){

        component.set('v.mode',3);
    }

    
})