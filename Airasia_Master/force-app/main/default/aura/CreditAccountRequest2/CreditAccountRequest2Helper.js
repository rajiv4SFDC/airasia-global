({
    validateFields : function(component, event, helper){

        var componentFields = component.find('caform');
        var validSoFar = true;
        //console.log('coming here validateField 1');
        componentFields.forEach(function (com, index){

            //console.log('coming here 1a');
            if(!com.get('v.validity').valid){
                
                //console.log('coming here 1b');
                com.showHelpMessageIfInvalid();
                validSoFar = false;
            }    

        });
        //console.log('coming here 1c');
        //console.log('validSoFar='+validSoFar);
        return validSoFar;
    },

    createCase : function(component,attachfile) {
        var newCase = component.get('v.newCase');
        var action = component.get("c.saveCase");
        action.setParams({
            "newCase": newCase
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log('Case Response='+JSON.stringify(response.getReturnValue()));
                component.set('v.returnedCase',response.getReturnValue());
                if(!attachfile){
                    component.set('v.mode',3);
                }
                else{
                    //attach file
                    component.set('v.mode',2);
                }
                
                
                /*
                var expenses = component.get("v.expenses");
                expenses.push(response.getReturnValue());
                component.set("v.expenses", expenses);
                */
            }
            else{
                
                console.log('Fail Case Response='+JSON.stringify(response.getReturnValue()));
                console.log(JSON.stringify(response));
            }
        });
        $A.enqueueAction(action);
        component.set('v.mode',1);
    },    
})