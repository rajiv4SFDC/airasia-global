({
    doInit : function(component, event, helper){
        helper.findUserType(component, event, helper);
        helper.fetchPicklistValues(component, "Type", "Sub_Category_1__c");
        var typeInput = component.find("type");
        typeInput.set("v.value","Complaint");
        console.log("doInit: end");
    },
    
    escapeKeyPressed: function(component, event, helper) {
        if(event.keyCode == 27){
            helper.closeModal(component);
        }
    },
    
    hideSpinner: function(component, event, helper) {
        $A.util.addClass(component.find('spinnerAA'), "slds-hide");
    },

    closeErrorMessage: function(component,event,helper){
        var formErrorMsg = component.find("formErrorMsg");
        $A.util.addClass(formErrorMsg, "hideErrorMsg");
    },
	
    showRelatedComponent: function (component, event, helper) {
        var controllerValueKey = event.getSource().get("v.value");
        var controllerId = event.getSource().getLocalId();
        helper.showRelatedComponentHelper(component, controllerValueKey, controllerId, helper);
    },
    
    // function call on change tha Dependent field    
    onDependentFieldChange: function(component, event, helper) {
        console.log("onDependentFieldChange: start");
        var cat1Val = event.getSource().get("v.value");
        var cat2Val = component.find("subCat2").get("v.value");
        var typeValue = component.find("type").get("v.value");

        //Reset the Sub Category 2 & 3 to not to show
        component.set("v.isSubCat1Others", false);
        component.set("v.isSubCat2Others", false);

        var controllerValueKey = event.getSource().get("v.value");
        var controllerId = event.getSource().getLocalId();
        
        helper.loadCategories(component, controllerValueKey, controllerId);
    },

    onSubCat2Change: function(component, event, helper) {
        var cat2Val = event.getSource().get("v.value");
        
        //Reset the Sub Category 2 to not to show
        component.set("v.isSubCat2Others", false);
        
        if (cat2Val.toLowerCase() == "voluntary cancellation"){
            component.set("v.isSubCat2Others", true);
            component.set("v.showComment", false); 
            helper.fetchPicklistVals(component, "Sub_Category_3__c", "subCat3");
        }
        else{
            component.set("v.showComment", true); 
        }
        
        // Refund Pop Up
        var typeValue = component.find('type').get('v.value');
        if(typeValue.toLowerCase() === 'refund') {
            
            if(cat2Val.toLowerCase() == 'serious illness / injury') {
            	component.set("v.showBooking", false);
                component.set("v.showPaymentRelatedFields", false);
                component.set("v.showCreditCardInfo", false);
                component.find('bookingPayment').set("v.value", component.get('v.defaultCategoryValue'));
            } else {
                component.set("v.showBooking", true);
            }
            
            helper.loadModel(component, cat2Val);
        }
    },
    
    showToolTipHelp: function(component,event,helper){
        var myId = event.currentTarget.getAttribute('data-idevalue');
        helper.showToolTip(component, myId);
    },
    
    hideTooltipHelp: function(component,event,helper){
        var myId = event.currentTarget.getAttribute('data-idevalue');
        helper.hideToolTip(component, myId);
    },

    done: function(component, event, helper){
        helper.navigateToCaseDetails(component);
    },
    
    handleSaveCase : function(component, event, helper) {
        console.log("handleSaveCase:");
        $A.util.removeClass(component.find('spinnerAA'), "slds-hide");
        
        var formErrorMsg = component.find("formErrorMsg");
        $A.util.addClass(formErrorMsg, "hideErrorMsg");
        component.set("v.isValidForm", true);
        
        var valid = helper.validateFields(component);
        console.log("Valid : " + valid);

        if (valid){
            console.log("handleSaveCase: valid");
            var button = component.find("Submit");
            button.set("v.disabled",true);
            button = component.find("SaveFilesSubmit");
            button.set("v.disabled",true);
            helper.saveCase(component, event, helper, false); 
        } else {
            console.log("handleSaveCase: NOT valid");
            var formErrorMsg = component.find("formErrorMsg");
            $A.util.removeClass(formErrorMsg, "hideErrorMsg");
            $A.util.addClass(component.find('spinnerAA'), "slds-hide");
        }
    },
    
    handleSaveCaseUploadFiles: function(component, event, helper) {
        $A.util.removeClass(component.find('spinnerAA'), "slds-hide"); 
        
        var formErrorMsg = component.find("formErrorMsg");
        $A.util.addClass(formErrorMsg, "hideErrorMsg");
        component.set("v.isValidForm",true);
        var valid = helper.validateFields(component);
        console.log('Valid : '+valid);
        if(valid){
            var button =component.find("Submit");
            button.set("v.disabled",true);
            var btn =component.find("SaveFilesSubmit");
            btn.set("v.disabled",true);
            helper.saveCase(component, event, helper, true);
        }else{
            var formErrorMsg = component.find("formErrorMsg");
            $A.util.removeClass(formErrorMsg, "hideErrorMsg");
            $A.util.addClass(component.find('spinnerAA'), "slds-hide");
        }
    },

    display : function(component, event, helper) {
        helper.toggleHelper(component, event);
    },
    
    displayOut : function(component, event, helper) {
        helper.toggleHelper(component, event);
    },

    validateFullName : function(component,event,helper){
        console.log("controller.validateFullName: start");
        helper.validateFullName(component);
    },
    validateEmail : function(component,event,helper){
        helper.validateEmail(component);
    },
    validatesubcat1 : function(component,event,helper){
        helper.validatesubcat1(component);
    },
    validatesubcat2 : function(component,event,helper){
        helper.validatesubcat2(component);
    },
    validatesubcat3 : function(component,event,helper){
        helper.validatesubcat3(component);
    },
    validatesubject : function(component,event,helper){
        helper.validatesubject(component);
    },
    validateBookingNumber : function(component,event,helper){
        helper.validateBookingNumber(component);  
    },
    validateAirlineCode : function(component,event,helper){
        helper.validateAirlineCode(component);
    },
    validateFlightNumber : function(component,event,helper){
        helper.validateFlightNumber(component);
    },
    validateComment : function(component,event,helper){
        helper.validateComment(component);  
    },
    validateStaffId : function(component,event,helper){
        helper.validateStaffId(component);  
    },
    validateStaffName : function(component,event,helper){
        helper.validateStaffName(component);  
    },
    validateStaffEmail : function(component,event,helper){
        helper.validateStaffEmail(component);  
    },

})