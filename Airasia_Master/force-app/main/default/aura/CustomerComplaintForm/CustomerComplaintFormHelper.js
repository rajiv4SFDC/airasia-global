({
    findUserType: function(component, event, helper){
        var action = component.get("c.getUserType");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                var userType = a.getReturnValue();
                component.set("v.userType", userType);
            }
        });
        
        $A.enqueueAction(action)
    },

    fetchPicklistValues: function(component, controllerField, dependentField) {
        var action = component.get("c.getDependentOptionsImpl");
        action.setParams({
            'objApiName': component.get("v.objInfo"),
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField
        });  
        action.setCallback(this, function(response) {
            if (response.getState() === 'ERROR'){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error('ERROR --> ', errors[0].message);
                    }
                }
            } else if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                var storeResponse = response.getReturnValue();
                if (controllerField == "Type"){
                    component.set("v.dependentFieldMap", storeResponse);
                    var listOfkeys = []; // for store all map keys (controller picklist values)
                    for (var singlekey in storeResponse) {
                        //AT (23 April 2019) - Temp Fix to remove "Refund" from the E-Form picklist
                        if (storeResponse[singlekey].value !== "Refund") {
                        	listOfkeys.push(storeResponse[singlekey]);
                    	}
                    }
                    this.fetchDepValues(component, listOfkeys, "type");
                    this.showRelatedComponentHelper(component, 'Complaint', 'type', this);
                    
                } else if (controllerField == "Sub_Category_1__c"){
                    component.set("v.dependentCat2FieldMap", storeResponse);
                } else if (controllerField == "Sub_Category_2__c"){
                    component.set("v.dependentCat3FieldMap", storeResponse);
                }
            }
        });
        $A.enqueueAction(action);
    }, // end fetchPicklistValues
    
    // Note that this function is similar - but different - to the one above
    fetchPicklistVals: function(component, fieldApiName, fieldId) {
        var action = component.get("c.getPicklistVals");
        action.setParams({
            'objApiName': component.get("v.objInfo"),
            'fieldApiName': fieldApiName
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var storeResponse = response.getReturnValue();
                this.fetchDepValues(component, storeResponse, fieldId);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields, fieldId) {
        // create a empty array var for store dependent picklist values for controller field)  
        var dependentFields = [];
        if ((ListOfDependentFields != undefined) && 
            (ListOfDependentFields.length > 0)) {
            dependentFields.push({
                class: "optionClass",
                label: component.get("v.defaultCategoryValue"),
                value: ""
            });
        }
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            dependentFields.push({
                class: "optionClass",
                label: ListOfDependentFields[i].label,
                value: ListOfDependentFields[i].value
            });
        }
        // set the dependentFields variable values to State(dependent picklist field) on ui:inputselect    
        component.find(fieldId).set("v.options", dependentFields);
        // make disable false for ui:inputselect field 
        component.set("v.isDependentDisabled", false);
    },
    
    loadCategories: function(component, controllerValueKey, controllerId) {
        //var controllerValueKey = event.getSource().get("v.value");
        //var controllerId = event.getSource().getLocalId();
        var nextCmp = null;
        var dependentMap = null;
        // Find Next Cmp
        if (controllerId === "type") {
            nextCmp = "subCat1";
            dependentMap = "dependentFieldMap";
        } else {
            nextCmp = "subCat2";
            dependentMap = "dependentCat2FieldMap";
        }
        
        var dependentMapData = component.get("v." + dependentMap);
        if (controllerValueKey != component.get("v.defaultCategoryValue")) {
            for (var singlekey in dependentMapData) {
                if (dependentMapData[singlekey].value === controllerValueKey) {
                    var listOfDependentFields = dependentMapData[singlekey].dependentList;
                    
                    this.fetchDepValues(component, listOfDependentFields, nextCmp);
                    
                    if (controllerId === "type") {
                        this.fetchPicklistValues(component, "Sub_Category_1__c", "Sub_Category_2__c");
                    } else {
                        if (listOfDependentFields.length > 0) {
                            component.set("v.isSubCat1Others", true);
                        }
                    }
                }
            }
        } else {
            var defaultVal = [{
                class: "optionClass",
                label: component.get("v.defaultCategoryValue"),
                value: component.get("v.defaultCategoryValue")
            }];
            component.find(nextCmp).set("v.options", defaultVal);
        }
    },
    
    showToolTip: function(component, tooltipId){
        var tooltipElm = component.find(tooltipId);
        $A.util.removeClass(tooltipElm, "slds-fall-into-ground");
        $A.util.addClass(tooltipElm, "slds-rise-from-ground");
    },
    
    hideToolTip: function(component, tooltipId){
        var tooltipElm = component.find(tooltipId);
        $A.util.addClass(tooltipElm, "slds-fall-into-ground");
        $A.util.removeClass(tooltipElm, "slds-rise-from-ground");
    },
    
    toggleHelper : function(component,event) {
        var toggleText = component.find("tooltip");
        $A.util.toggleClass(toggleText, "toggle");
    },
    
    navigateToCaseDetails: function(component,caseDetails){
        var userType = component.get("v.userType");
        if (userType == 'Guest'){
            component.set("v.recordCreated", true);
        } else {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId":component.get("v.caseId"),
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
    },

    validateFields: function(component){
        this.validateFullName(component);
        this.validateEmail(component);
        this.validateSubject(component);
        this.validateBookingNumber(component);
        this.validateAirlineCode(component);
        this.validateFlightNumber(component);
        this.validateComment(component);
        this.validateStaffId(component);
        this.validateStaffName(component);
        this.validateStaffEmail(component);
        var isValidForm = component.get("v.isValidForm");                             
        return isValidForm;
    },
    
    validateFullName: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        var reg1 = /^[a-zA-Zaâáéíîó????ã?????ñcdeínrš?t?úužd ]*$/;
        
        if (component.get("v.userType") === 'Guest'){
            var flag1 = 0;
            var FullNameVal = component.get("v.FullName");
            var FullName = component.find("FullName");
            if (FullNameVal == '' || typeof FullNameVal == 'undefined'){
                FullName.set("v.errors", 
                             [{message: $A.get("$Label.c.AA_Case_First_Name_EM")}]);
                isValidForm = false;
            } else if(reg1.test(FullNameVal) == false) {
                FullName.set("v.errors", 
                             [{message: $A.get("$Label.c.AA_Case_First_Name_ValidEM")}]);
                isValidForm = false;
            } else {
                FullName.set("v.errors", null);
                flag1 = 1;
                newCase.SuppliedName = FullNameVal;
            }
        } else {
            newCase.SuppliedName = null;
        }
        component.set("v.isValidForm",isValidForm);
        component.set("v.newCase",newCase);                   
        return flag1;       
    },

    validateEmail: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.userType") === 'Guest'){
            var  emailId = component.find("emailId");
            if(typeof emailId != 'undefined'){
                var emailIdVal = newCase.SuppliedEmail;
                var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (emailIdVal == '' || typeof emailIdVal == 'undefined' || 
                    emailIdVal == null) {
                    emailId.set("v.errors", 
                                [{message: $A.get("$Label.c.AA_Case_Email_EM")}]);
                    isValidForm = false;
                } else if (!reg.test(emailIdVal)){
                    emailId.set("v.errors", 
                                [{message: $A.get("$Label.c.AA_Case_Email_ValidEM")}]);
                    isValidForm = false;
                }else {
                    emailId.set("v.errors", null);
                }
            }
        }else{
            newCase.SuppliedEmail = null;
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    
    validateSubject: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        var SearchText = component.find("SearchText");
        if (typeof SearchText != 'undefined'){
            var SearchTextVal = newCase.Subject;
            if ((SearchTextVal == "") || 
                (typeof SearchTextVal == "undefined") || 
                (SearchTextVal.length < 4)) {
                SearchText.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Subject_EM")}]);
                isValidForm = false;
            } else {
                SearchText.set("v.errors", null);
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },

    validateBookingNumber: function(component){
     	var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");  
        var BookingNumber = component.find("BookingNumber");
        if (typeof BookingNumber != 'undefined'){
            var BookingNumberVal = newCase.Booking_Number__c;
            var type = component.find("type").get("v.value");
            if(type == 'Refund'){
                if ((BookingNumberVal == undefined) || 
                    (BookingNumberVal == null) || 
                    (BookingNumberVal == "")){
                    BookingNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BookingNumber_EM")}]);
                    isValidForm = false;
                } else if ((typeof BookingNumberVal != "undefined") && 
                           (BookingNumberVal.length != 6)){
                    BookingNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BookingNumber_EM")}]);
                    isValidForm = false;
                } else {
                    BookingNumber.set("v.errors", null);
                }
            } else {
                if ((typeof BookingNumberVal != "undefined") && 
                    (BookingNumberVal.length != 6) && 
                    (BookingNumberVal.length != 0)){
                    BookingNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_BookingNumber_EM")}]);
                    isValidForm = false;
                } else {
                    BookingNumber.set("v.errors", null);
                }
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },

    validateAirlineCode: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        var type = component.find("type").get("v.value");
        if (type == "Refund"){           
            var airlineCode = component.find("airlineCode");
            if (typeof airlineCode != "undefined"){
                var airlineCodeVal = newCase.Airline_Code__c;
                if ((airlineCodeVal == "") || 
                    (airlineCodeVal == component.get("v.defaultCategoryValue"))){
                    airlineCode.set("v.errors", [{message: $A.get("$Label.c.AA_Case_AirlineCode_EM")}]);
                    isValidForm = false;
                } else {
                    airlineCode.set("v.errors", null);
                }
            }
        } else {
            var airlineCode = component.find("airlineCode");
            airlineCode.set("v.errors", null);
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },

    validateFlightNumber: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        var type = component.find("type").get("v.value");
        var flightNumber = component.find("FlightNumber");
        flightNumber.set("v.errors", null);
        var flightRegex = /^([0-9]{2,4})?$/;
        if (typeof flightNumber != 'undefined'){
            var flightNumberVal = newCase.Flight_Number__c;
            if (type == 'Refund'){
                if ((flightNumberVal == "") || 
                    (typeof flightNumberVal == "undefined") || 
                    (flightNumberVal.length < 2) || 
                    (flightRegex.test(flightNumberVal) === false)){
                    flightNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_FlightNumber_EM")}]);
                    isValidForm = false;
                }
            } else if ((typeof flightNumberVal != "undefined") && 
                       (flightNumberVal != "") && 
                       (flightRegex.test(flightNumberVal) === false)) {
                flightNumber.set("v.errors", [{message: $A.get("$Label.c.AA_Case_FlightNumber_EM")}]);
                isValidForm = false;
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    
    validateComment: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        if ((component.get("v.showPaymentRelatedFields") === true) || 
            (component.get("v.showpersonald") === true)){
            if (component.get("v.showComment") === true){
                var commentbox = component.find("commentbox");
                var commentboxVal = newCase.Comments;
                if ((commentboxVal == "") || 
                    (typeof commentboxVal == "undefined") || 
                    (commentboxVal == null)){
                    commentbox.set("v.errors", [{message: $A.get("$Label.c.AA_Case_Comment_EM")}]);
                    isValidForm = false;
                } else {
                    commentbox.set("v.errors", null);
                }
            } else {
                newCase.Comments = null;           
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },

    validateStaffId: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var staffId = component.find("staffId");
        var staffIdVal = newCase.Staff_ID__c;
        if(staffIdVal == '' || typeof staffIdVal == 'undefined' || staffIdVal == null){
            staffId.set("v.errors", [{message: $A.get("Invalid Staff ID")}]);
            isValidForm = false;
        } else {
            staffId.set("v.errors", null);
        }
        component.set("v.newCase", newCase);
        component.set("v.isValidForm",isValidForm);
    },
    
    validateStaffName: function(component){
        var newCase = component.get("v.newCase");                
        var isValidForm = component.get("v.isValidForm");
        var staffName = component.find("staffName");
        var staffNameVal = newCase.Staff_Name__c;
        if(staffNameVal == '' || typeof staffNameVal == 'undefined' || 
           staffNameVal == null){
            staffName.set("v.errors", [{message: $A.get("Invalid Staff Name")}]);
            isValidForm = false;
        } else {
            staffName.set("v.errors", null);
        }
        component.set("v.newCase", newCase);
        component.set("v.isValidForm",isValidForm);
    },

    validateStaffEmail: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if (component.get("v.userType") === 'Guest'){
            var  emailId = component.find("staffEmailId");
            if (typeof emailId != 'undefined'){
                var emailIdVal = newCase.Staff_Email__c;
                var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if ((emailIdVal == '') || 
                    (typeof emailIdVal == 'undefined') || 
                    (emailIdVal == null)) {
                    emailId.set("v.errors", 
                                [{message: $A.get("$Label.c.AA_Case_Email_EM")}]);
                    isValidForm = false;
                } else if (!reg.test(emailIdVal)){
                    emailId.set("v.errors", 
                                [{message: $A.get("$Label.c.AA_Case_Email_ValidEM")}]);
                    isValidForm = false;
                } else {
                    emailId.set("v.errors", null);
                }
            }
        } else {
            newCase.Staff_Email__c = null;
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    
    saveCase : function(component, event, helper, uploadFiles) {
        var i;
        var tempStr1;
        var tempStr2;
        var tempCond;
        var newCase = component.get("v.newCase");
        
        newCase.Case_Language__c = $A.get("$Locale.langLocale");
        
        var action = component.get("c.saveCase");
        action.setParams({ 
            "newCase": newCase,
            "caseComments": newCase.Comments,
            "mobilephoneValue": null,
            "alternatephoneValue": null,
            "flightRefundList": null,
            "passengerRefundList": null
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            console.log("saveCase: " + state);
            if (state === "SUCCESS") {
                component.set("v.showError",false);
                var caseDetails = a.getReturnValue();
                
                component.set("v.caseId", caseDetails.Id);
                component.set("v.refNo", caseDetails.CaseNumber);
                if(uploadFiles){ 
                    component.set("v.uploadAttachments", true);
                }else{
                    this.navigateToCaseDetails(component);
                }
            } else {
                var errors = action.getError();
                console.log('Error - ', errors[0].message);
                var btn = component.find("Submit");
                btn.set("v.disabled",false);
                var button =component.find("SaveFilesSubmit");
                button.set("v.disabled",false);
                
                if('NON_COMMUNITY_USER' === errors[0].message) {
                    component.set("v.showErrorMsg", 
                                  'MESSAGE - Only Community User can save case.');
                }
                component.set("v.showError",true);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    // function called when the controlling field's value changes
    showRelatedComponentHelper: function(component, controllerValueKey, controllerId, helper) {
        $A.util.removeClass(component.find('spinnerAA'), "slds-hide");
        
        // default to complaint
        if (controllerValueKey.toLowerCase() == ""){
            controllerValueKey = "Complaint";
        }
        console.log("ControllerValueKey = " + controllerValueKey);
        
        var formErrorMsg = component.find("formErrorMsg");
        $A.util.addClass(formErrorMsg, "hideErrorMsg");
        
        if (controllerValueKey.toLowerCase() == "refund"){
            component.set("v.showRefundCaseType", true);
            component.set("v.isSubCat", true);
            component.set("v.showPersonalD",false);
            component.set("v.showSubject", true);
            component.set("v.showComment", false);
            component.set("v.showBooking", true);
            component.set("v.showSubmitAttach", true);
            
        } else if (controllerValueKey.toLowerCase() == ""){
            component.set("v.showRefundCaseType", false);
            component.set("v.isSubCat", false);
            component.set("v.isSubCat1Others", false);
            component.set("v.showComment", false);
            component.set("v.showSubject", false);
            component.set("v.showPersonalD",false);
            component.set("v.showBooking", false);
            component.set("v.showSubmitAttach", false);
            var newCase = component.get("v.newCase");
            newCase.Case_Currency__c = null;
            component.set("v.newCase",newCase);
            
        } else { 
            // controller value is something valid other than refund
            component.set("v.showRefundCaseType", false);
            component.set("v.isSubCat", true);
            component.set("v.isSubCat1Others", false);
            component.set("v.isSubCat2Others", false);
            component.set("v.showComment", true);
            component.set("v.showSubject", true);
            component.set("v.showPersonalD",true);
            component.set("v.showBooking", false);
            component.set("v.showSubmitAttach", true);
            var newCase = component.get("v.newCase");
            newCase.Case_Currency__c = null;
            component.set("v.newCase",newCase);
        }
        helper.loadCategories(component, controllerValueKey, controllerId);
        helper.fetchPicklistVals(component, 'Airline_Code__c', 'airlineCode');
    },
    
    validatesubcat1: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");

        if(component.get("v.isSubCat") === true){
            var subCat1 = component.find("subCat1");
            if(typeof subCat1 != 'undefined'){
                var subCat1Val = newCase.Sub_Category_1__c;
                if(subCat1Val == '' || typeof subCat1Val == 'undefined'){
                    subCat1.set("v.errors", 
                                [{message: $A.get("$Label.c.AA_Case_Subcat_EM")}]);
                    isValidForm = false;
                }else{
                    subCat1.set("v.errors", null);
                }
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    
    validatesubcat2: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.isSubCat1Others") === true){
            var subCat2 = component.find("subCat2");
            if(typeof subCat2 != 'undefined'){
                var subCat2Val = newCase.Sub_Category_2__c;
                if(subCat2Val == '' || typeof subCat2Val == 'undefined'){
                    subCat2.set("v.errors", 
                                [{message: $A.get("$Label.c.AA_Case_Subcat_EM")}]);
                    isValidForm = false;
                } else {
                    subCat2.set("v.errors", null);
                }
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    
    validatesubcat3: function(component){
        var newCase = component.get("v.newCase");
        var isValidForm = component.get("v.isValidForm");
        if(component.get("v.isSubCat2Others") === true){
            var subCat3 = component.find("subCat3");
            if(typeof subCat3 != 'undefined'){
                var subCat3Val = newCase.Sub_Category_3__c;
                if(subCat3Val == '' || typeof subCat3Val == 'undefined'){
                    subCat3.set("v.errors", 
                                [{message: $A.get("$Label.c.AA_Case_Subcat_EM")}]);
                    isValidForm = false;
                }else{
                    subCat3.set("v.errors", null);
                }
            }
        }
        component.set("v.newCase",newCase);
        component.set("v.isValidForm",isValidForm);
    },
    
})