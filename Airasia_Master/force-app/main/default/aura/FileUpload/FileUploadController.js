({  
    fileSelected : function(component, event, helper) {
        helper.validateFiles(component);
    },
    toggleSection : function(component, event, helper) {
        var buttonName = event.target.name;
        if(buttonName === 'selectedFileButton') {
            $A.util.toggleClass(component.find("selectedFileSection"), "slds-is-open");
        }
    },
    removeFile : function(component, event, helper) {
        helper.removeFile(component, event);
    },
    uploadFiles : function(component, event, helper) {
        helper.uploadFiles(component);
    }
})