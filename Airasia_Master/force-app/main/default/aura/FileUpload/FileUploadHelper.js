({
    MAX_FILE_SIZE: 4150000, /* File Size : 3.96 MB Because heap size doesn't allow more than this size*/
    CHUNK_SIZE: 614400, /* Chunk Size : 600 KB*/
    MAX_FILES: 5,
    validateFiles : function(component) {
        try {
            var fileSelector = document.getElementById("fileSelector");
            var selectedFiles = fileSelector.files;
            var selectedFileList = component.get("v.selectedFileList");
            var uploadedFiles = component.get("v.uploadedFiles");
            var errorMessage = component.find("errorMessage");
            $A.util.addClass(errorMessage, "hideElement");
            var count = selectedFileList.length + selectedFiles.length + uploadedFiles.length;

            if(count < this.MAX_FILES) {
                if(selectedFiles.length > 0) {
                    for (var i = 0; i < selectedFiles.length; i++) {
                        //if(selectedFiles.item(i).type)
                        var fileObj = selectedFiles.item(i);
                        if(this.validateUniqueName(component, fileObj)) {
                            if(this.validateFile(fileObj)) {
                                selectedFileList.push(fileObj);
                            } else {
                                $A.util.removeClass(errorMessage, "hideElement");
                            }
                        } 
                    }
                    component.set("v.isFileSelected", true);
                    component.set("v.selectedFileList", selectedFileList);
                }
            } else {
                $A.util.removeClass(errorMessage, "hideElement");
            }
            document.getElementById("uploadForm").reset();
        } catch(err) {
            console.log('Error while validating files - ', err);
        }
    },
    validateFile : function(file) {
        return (file == null || file.size > this.MAX_FILE_SIZE) ? false : true;
    },
    validateUniqueName : function(component, file) {
     	var isUnique = true;
        var selectedFileList = component.get("v.selectedFileList");
        if(selectedFileList.length > 0) {
            for (var i = 0; i < selectedFileList.length; i++) {
                if(file.name === selectedFileList[i].name) {
                    isUnique = false;
                    break;
                }
            }
        }
        return isUnique;
    },
    removeFile : function(component, event) {
        try {
            component.set("v.isFileSelected", false);
            var indexValue = event.getSource().get('v.value');
            var selectedFileList = component.get("v.selectedFileList");
            var newFileList = [];
            
            if(selectedFileList.length > 0) {
                for (var i = 0; i < selectedFileList.length; i++) {
                    if(indexValue != i) {
                        newFileList.push(selectedFileList[i]);
                    }
                }
            }
            component.set("v.selectedFileList", newFileList);
            if(newFileList.length > 0) {
                component.set("v.isFileSelected", true);
            } 
            var errorMessage = component.find("errorMessage");
            $A.util.addClass(errorMessage, "hideElement");
        } catch(err) {
            console.log('Error while removing file from list -', err);
        }
        
    },
    uploadFiles : function(component) {
        try {
            var selectedFileList = component.get("v.selectedFileList");
            for(var i=0; i < selectedFileList.length; i++){
                var self = this;
                self.setupReader(component, selectedFileList[i]);
            }
            selectedFileList = []; 
            component.set("v.selectedFileList", selectedFileList);
            component.set("v.isFileSelected", false);
        } catch(err) {
            console.log('Error while uploading files - ', err);
        }
    },
    setupReader : function(component, file) {
        var name = file.name;
        var fr = new FileReader();
        fr._NAME = name;
        var self = this;
        fr.onload = $A.getCallback(function() {
            fileName = fr._NAME;
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            
            fileContents = fileContents.substring(dataStart);
            
            self.upload(component, file, fileContents, fileName);
        });
        
        fr.readAsDataURL(file); 
    },
    upload : function(component, file, fileContents, fileName) {
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);
        this.uploadChunk(component, file, fileContents, fromPos, toPos, '', fileName);
    },
    uploadChunk : function(component, file, fileContents, fromPos, toPos, attachId, fileName) {
        var action = component.get("c.saveTheChunk"); 
        var chunk = fileContents.substring(fromPos, toPos);
        
        action.setParams({
            parentId: component.get("v.parentId"),
            fileName: fileName,
            base64Data: encodeURIComponent(chunk), 
            contentType: file.type,
            fileId: attachId
        });
         
        var self = this;
        action.setCallback(this, function(a) {
            attachId = a.getReturnValue();
            var state = a.getState();
            if (state === "SUCCESS"){ 
                console.log('Files have been Uploaded');
            }
            else if (state === "ERROR"){
                console.log('Files have not been Uploaded');
            }
            fromPos = toPos;
            toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);    
            if (fromPos < toPos) {
                self.uploadChunk(component, file, fileContents, fromPos, toPos, attachId);  
            }else{
                var UploadedFileIds = component.get("v.UploadedFileIds");
                if(UploadedFileIds == undefined){
                    UploadedFileIds = attachId;
                }else{
                    UploadedFileIds = UploadedFileIds+','+attachId;
                }
                component.set("v.UploadedFileIds", UploadedFileIds);
                
                var newUploadedFile = {id:attachId, fileName:fileName};
                var uploadedFiles = component.get("v.uploadedFiles");
                uploadedFiles.push(newUploadedFile);
                component.set("v.uploadedFiles", uploadedFiles);
                
                console.log('Completed upload chunk task.');
            }
        });

        $A.enqueueAction(action); 
    } 
})