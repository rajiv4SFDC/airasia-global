({
	doInit : function(component, event, helper) {
        // Load user language
        helper.init(component);
    },
    updateLanguage : function(component, event, helper) {
        // Update user language
        helper.updateLanguage(component, event);
    },
    displayLanguageData : function(component, event, helper) {
        // Open language menu
        var cmpTarget = component.find('langTab');
        $A.util.toggleClass(cmpTarget, 'visible positioned');
    }
})