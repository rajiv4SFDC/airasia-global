({
    init : function(component) {
        
        var lang = this.getLanguage();
        var action = component.get("c.getLanguageInfo");
        component.set("v.isLangInfo", false);
        action.setParams({ 
            "lang": lang
        });        
        
        action.setCallback(this, function(result) {
            var state = result.getState();
            if (state === "SUCCESS") {
                var languageInfo = result.getReturnValue();
                if(languageInfo != null) {
                    component.set("v.isLangInfo", true);
                    component.set("v.language", languageInfo.userLanguage);
                    component.set("v.langSet" , languageInfo.languageSet);
                } else {
                    var pathValue = '/s/case/Case/Recent';
                    if(pathValue === window.location.pathname) {
                       window.location = "/s/login?language="+lang;
                    }
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    updateLanguage : function(component, event) {
        
        var userLang = component.get("v.language");
        var selectedLang = event.currentTarget.getAttribute('title');
        
        if(userLang != selectedLang) {
             var action = component.get("c.getUserLanguage");
            
            action.setParams({ 
                "selectedLang": selectedLang
            });
            
            action.setCallback(this, function(result) {
                var state = result.getState();
                if (state === "SUCCESS") {
                    var lang = result.getReturnValue();
                    component.set("v.language", selectedLang);
                    var location = window.location.href;
					var newLocation = this.replaceUrlParam(location, 'language', lang);
                    window.location.href = newLocation;
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    getLanguage : function() {
        var lang = '';
        try {
            var sPageURL = decodeURIComponent(window.location.search.substring(1));
            var sURLVariables = sPageURL.split('&');
            var sParameterName;
            var i;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                
                if (sParameterName[0] === 'language') {
                    lang = sParameterName[1] === undefined ? 'en_GB' : sParameterName[1];
                    break;
                }
            }
        } catch(err) {
          console.log('Error while retrieving language - ', err);
        } 
        return lang;
    },
    replaceUrlParam : function (url, paramName, paramValue){
        if(paramValue == null)
            paramValue = '';
        var pattern = new RegExp('\\b('+paramName+'=).*?(&|$)')
        if(url.search(pattern)>=0){
            return url.replace(pattern,'$1' + paramValue + '$2');
        }
        return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue 
    }
})