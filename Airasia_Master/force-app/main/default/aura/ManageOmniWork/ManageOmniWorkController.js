({
    doInit : function(component, event, helper) {
        helper.loadCapacity(component, helper);
        
    },
    onLoginSuccess : function(component, event, helper) { 
        helper.loadCapacity(component, helper);
        helper.reloadWorkItems(component);        
    },
    onWorkClosed : function(component, event, helper) { 
        helper.reloadWorkItems(component);        
    },
    onWorkloadChanged : function(component, event, helper) { 
        //helper.reloadWorkItems(component);        
    },
    onWorkAccepted : function(component, event, helper) { 
        helper.reloadWorkItems(component);        
    },
    onWorkAssigned : function(component, event, helper) {
        
        var omniAPI = component.find("omniToolkit");
        var workId = event.getParam('workId');
        var currentWorkItemId = null;
        var maxLCCount = component.get("v.userInfo.Live_Chat_Capacity__c");
        var maxSocCount = component.get("v.userInfo.Social_Capacity__c");
        
        //Add Default while loading
        if(maxLCCount == "" && maxSocCount == "") {
        	helper.loadCapacity(component, helper);
            maxLCCount = 3;
            maxSocCount = 1;
        }
        
        //Only apply if properties for the user has been populated 
        if(maxLCCount != "" && maxSocCount != "") {
            omniAPI.getAgentWorks().then(function(result) {
                //Get Current Live Chat and Social Case Assigned
                var lcCount = 0;
                var socCount = 0;
                
                var works = JSON.parse(result.works);
                for (var i = 0; i < works.length; i++) {
                    var workItemId = works[i].workItemId;
                    lcCount = (workItemId.substring(0,3)=="570") ? lcCount + 1 : lcCount;
                    socCount = (workItemId.substring(0,3)=="500") ? socCount + 1 : socCount;
                    
                    if(works[i].workId == workId) {
                        currentWorkItemId = workItemId;
                    }
                }
                //End
                
                //Check if Chat has been transferred - Allow bypass of capacity limit - 02 Jan 2020
                var bypassChatCapacity = false;
                if (currentWorkItemId.substring(0,3)=="570") {                    
        			var chatDetails = component.get("c.hasCaseId");
                    chatDetails.setParams({ "recordId" : currentWorkItemId });
                    chatDetails.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            var storeResponse = response.getReturnValue();
                            bypassChatCapacity = (storeResponse != null);
                            console.log('hasCaseId => ' + storeResponse + '/' + bypassChatCapacity);
                            helper.processWorkItem(component, workId, lcCount, maxLCCount, socCount, maxSocCount, currentWorkItemId, bypassChatCapacity);
                        }
                        else {
                            //If unable to complete the request, do not allow chat bypass and calculate by capacity
                            console.log('Unable to retrieve Case from Chat');
                            helper.processWorkItem(component, workId, lcCount, maxLCCount, socCount, maxSocCount, currentWorkItemId, bypassChatCapacity);
                        }
                    });
                    $A.enqueueAction(chatDetails);
                }             
                else {
                    helper.processWorkItem(component, workId, lcCount, maxLCCount, socCount, maxSocCount, currentWorkItemId, false);
                }
                
            }).catch(function(error) {
                //Decline
                omniAPI.declineAgentWork({workId: workId}).then(function(res) {
                    if (res) {
                        console.log("Declined work successfully - " + workId);
                    } else {
                        console.log("Decline work failed - " + workId);
                    }
                }).catch(function(error) {
                    console.log(error);
                });
            });
        }
    }
})