({
    reloadWorkItems : function(component) {        
        var omniAPI = component.find("omniToolkit");
        omniAPI.getAgentWorks().then(function(result) {
            
            //Array for aura:attribute
            var workItemAttr = [];
            
            var works = JSON.parse(result.works);
            for (var i = 0; i < works.length; i++) {
                var workItemId = works[i].workItemId;
                workItemAttr.push(workItemId);
            }
            console.log(workItemAttr);
            
            component.set("v.workitems", workItemAttr);
        });
    },
    loadCapacity : function (component, helper) {
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.userInfo", storeResponse);
            }
            else {
                helper.loadCapacity(component, helper);
            }
            console.log('ManageOmniWork.doInit: ' + state);
        });
        $A.enqueueAction(action);
    },
    processWorkItem : function(component, workId, lcCount, maxLCCount, socCount, maxSocCount, currentWorkItemId, bypassChatCapacity) {
        var omniAPI = component.find("omniToolkit");
        console.log("LC: " + lcCount + "/" + maxLCCount + " = " + (lcCount >= maxLCCount));
        console.log("Social: " + socCount + "/" + maxSocCount + " = " + (socCount >= maxSocCount));
        
        var decline = false;
        if (currentWorkItemId.substring(0,3)=="570") {
            //LC
            decline = (lcCount > maxLCCount) || bypassChatCapacity;
        }
        else if (currentWorkItemId.substring(0,3)=="500") {
            //Case
            decline = (socCount > maxSocCount);
        }
        
        //Check if the maximum allowed has been exceeded then decline
        if(decline)  {
            //Decline
            omniAPI.declineAgentWork({workId: workId}).then(function(res) {
                if (res) {
                    console.log("Declined work successfully - " + workId);
                } else {
                    console.log("Decline work failed - " + workId);
                }
            }).catch(function(error) {
                console.log(error);
            });
        }
        else {
            //Accept
            omniAPI.acceptAgentWork({workId: workId}).then(function(res) {
                if (res) {
                    console.log("Accepted work successfully - " + workId);
                } else {
                    console.log("Accept work failed - " + workId);
                } 
            }).catch(function(error) {
                console.log(error); 
            });
        }
    }
})