({
	doInit : function(component, event, helper) {
        
        var recordId = component.get("v.recordId");
        console.log("recordId => " + recordId);
        var action = component.get("c.fetchCaseName");
        var iconName = "standard:case";
        if (recordId.substring(0,3) == "570") {
            action = component.get("c.fetchChatName");
            iconName = "standard:live_chat";
        }        
        
  		action.setParams({ "recordId" : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.recordName", storeResponse);
                component.set("v.iconName", iconName);
            }
        });
        $A.enqueueAction(action);	
	},
    
    openWork : function(component, event, helper) {
     	var workspaceAPI = component.find("workspace");
        var recId = component.get("v.recordId");
        workspaceAPI.openTab({
            recordId: recId,
            focus: true
        }).then(function(response) {
            workspaceAPI.getTabInfo({
                  tabId: response
            }).then(function(tabInfo) {
            });
        })
        .catch(function(error) {
               console.log(error);
        });   
    }
})