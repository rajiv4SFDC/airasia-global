({
    doInit : function(component, event, helper) {
        var language = helper.getParameterByName('language');
        helper.navigateToLogin(language);
    }
})