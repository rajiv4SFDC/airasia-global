/*Steven M. Giangrasso*/
({
    doInit:function(component, event, helper) {
        console.log('Search Articles');
        helper.findUserType(component, event, helper);
        helper.getDefaultSearchResults(component);
    },
    
    /*Method when the user is Searching the articles  */
    search : function(component, event, helper) {
        /*Helper: extend the getSearchResults function from the Helper*/
        helper.getSearchResults(component, event);
    },
    
    /*Clears the results*/    
    clear : function(component,event,helper){
        
        var searchValue = component.find("SearchText");
        if(searchValue!=""){
            component.find("SearchText").set("v.value","");
        }
        component.set("v.records",[]);
        
    },
    displayPopup:function(component,event, helper){
        var articleid = event.currentTarget.dataset.record ;
        helper.displayArticlePopup(component,articleid);
    },
    
    Hidepopup: function(component,event){
        var displayPopup = component.find("displayPopup");
        var modelDiv = displayPopup.find("modelDiv");
        var backGroundSectionId = displayPopup.find("backGroundSectionId");
        
        
        
        $A.util.removeClass(displayPopup, "slds-fade-in-open");
        $A.util.addClass(displayPopup, "slds-fade-in-hide");
        
        $A.util.removeClass(backGroundSectionId, "slds-backdrop--open");
        $A.util.addClass(backGroundSectionId, "slds-backdrop--close");
        
        
    },
    UpVoteMethod:function(component,event){
        // alert('UpVoteMethod');
        var SelectedRrcord= component.get("v.SelectedRrcord");
        var VoteType="Up";
        //console.log('SelectedRrcord is ' + SelectedRrcord)
        var JSONstr=JSON.stringify(SelectedRrcord);
        
        var action = component.get("c.CreateVote");
        action.setParams({
            "jsonstring": JSONstr,
            "VoteType":VoteType
        });
        
        action.setCallback(this, function(a) {
            //console.log(a);
            component.set("v.SelectedRrcord", a.getReturnValue());
            
        });
        
        $A.enqueueAction(action);
    },
    DownVoteMethod:function(component,event){
        var SelectedRrcord= component.get("v.SelectedRrcord");
        var VoteType="Down";
        var JSONstr=JSON.stringify(SelectedRrcord);
        
        var action = component.get("c.CreateVote");
        
        action.setParams({
            "jsonstring": JSONstr,
            "VoteType":VoteType
        });
        
        action.setCallback(this, function(a) {
            
            //console.log(a);
            component.set("v.SelectedRrcord", a.getReturnValue());
            
        });
        
        $A.enqueueAction(action);
    },
    handleApplicationEvent: function(cmp, event, helper) {
         var JapanId = event.getParam("JapanId");
        console.log('JapanId ---->' +JapanId);
        
        //console.log('JapanId ::::' + JapanId)
        if(JapanId != null || JapanId !=undefined){
            console.log('JapanId--->1' +JapanId);
            console.log('cmp--->1' +cmp);
            helper.displayArticlePopup(cmp,JapanId);
        }

        var searchString = event.getParam("searchString");
        if(searchString != '' ){
            cmp.set("v.searchStr", searchString);
            helper.getSearchResults(cmp, event);
        }
    }
    
})