//Steven M. Giangrasso
({
    findUserType: function(component, event, helper){
        var action = component.get("c.getUserType");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                var userType = a.getReturnValue();
                component.set("v.userType", userType);
            }
        });
        
        $A.enqueueAction(action)
    },
    //Making a call to Salesforce and getting the results of the Knowledge Article Searches
    getSearchResults : function(component, event) {
        var srchStr = component.find("SearchText").get("v.value");
        console.log('Search String: '+srchStr);
        var isWord = false;
        if(srchStr) {
            var length = srchStr.length - 1;
            isWord = srchStr.lastIndexOf(' ') === length;

            //If the length of the text is greater than 2, listen for the Event to Search the Knowledge Articles
            if(isWord === true || event.getParam("isBlur") === true){
                //APEX SearchArticles class method
                var action = component.get("c.SearchArticlesLang");
                //passing the parameters to Salesforce
                action.setParams({
                    "searchKey": srchStr,
                    "language": $A.get("$Locale.langLocale")
                });
                
                /*Results displayed from the method execution*/ 
                action.setCallback(this, function(a) {

                    try {
                        var articles = a.getReturnValue();
						srchStr = srchStr.trim();
                        var regEx = new RegExp(srchStr, "ig");
                        for (var prop in articles) {
                            if (articles.hasOwnProperty(prop)) { 
                                articles[prop].Title = articles[prop].Title.replace(regEx, "<mark>" + srchStr + "</mark>");
                            }
                        }
                    } catch(err) {
                        console.log('Error while retreiving articles - ', err);
                    }
                    component.set("v.records", a.getReturnValue());
                });
                
                //invoke action to Search Articles (line 11)
                $A.enqueueAction(action);
            }
        }
    },
    getDefaultSearchResults: function(component) {
            var action = component.get("c.SearchArticlesLang");
            action.setParams({
                "searchKey": '',
                "language": $A.get("$Locale.langLocale")
            });
            action.setCallback(this, function(a) {
                console.log(a.getReturnValue());
                component.set("v.records", a.getReturnValue());
            });
            $A.enqueueAction(action);
        },
     displayArticlePopup: function(component,articleid){
        var action = component.get("c.getArticleDetails");
        
        action.setParams({
            "articleid": articleid
        });
         console.log('articleid ' + articleid);
        
        action.setCallback(this, function(a) {
            console.log('-----------------');
            console.log(a.getReturnValue());
            console.log('-----------------');
            component.set("v.SelectedRrcord", a.getReturnValue());
            var displayPopup = component.find("displayPopup");
            var modelDiv = displayPopup.find("modelDiv");
            var backGroundSectionId = displayPopup.find("backGroundSectionId");
            
            
            
            $A.util.addClass(displayPopup, "slds-fade-in-open");
            $A.util.removeClass(displayPopup, "slds-fade-in-hide");
            
            $A.util.addClass(backGroundSectionId, "slds-backdrop--open");
            $A.util.removeClass(backGroundSectionId, "slds-backdrop--close");
        });
        
        
        $A.enqueueAction(action);

    }
    
})