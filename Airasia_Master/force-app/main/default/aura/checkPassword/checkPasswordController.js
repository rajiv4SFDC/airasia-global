({
	initialize: function(component, event, helper) {
        helper.loadEmail(component);
        helper.setLanguage(component);
    }
})