({
    loadEmail : function(component) {
        var data = $A.getReference("$Label.c.AA_Case_Check_Password_Message");
        var regEx = /{(\d+)}/;
        component.set('v.message', data);
        var userEmail = '<strong>' + this.getParameterByName('eid') + '</strong>';
		var args = [userEmail];
		data = component.get('v.message');
        for(var i=0; i < args.length; i++) {
            data = data.replace(regEx, args[i]);
        }
        component.set('v.message', data);
    },
	setLanguage : function(component) {
        component.set('v.language', this.getParameterByName('language'));
    },
    getParameterByName: function (name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
})