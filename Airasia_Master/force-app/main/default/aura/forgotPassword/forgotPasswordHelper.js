({
    handleForgotPassword: function (component, event, helpler) {
        component.set("v.showError",false);
        if(this.validateData(component)) {
            var username = component.find("username").get("v.value");
            var action = component.get("c.forgotPassword");
            action.setParams({username:username});
            action.setCallback(this, function(a) {
                var rtnValue = a.getReturnValue();
                if (rtnValue != null) {
                    if(rtnValue == 'success') {
                        var address = "/CheckPasswordResetEmail";
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": address+'?eid=' + username,
                            "isredirect": true
                        });
                        urlEvent.fire();
                    } else {
                        component.set("v.errorMessage",rtnValue);
                        component.set("v.showError",true);
                    }
                } 
            });
            $A.enqueueAction(action);
        }
    },
    setLanguage : function(component) {
        component.set('v.language', this.getParameterByName('language'));
    },
    getParameterByName: function (name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    validateData: function(component) {
        var isValidForm = true;
        var cmpList = ['username'];
        var inputCmp;
        
        for (var i = 0; i < cmpList.length; i++) {
            inputCmp = component.find(cmpList[i]);
            inputCmp.showHelpMessageIfInvalid();
            if(inputCmp.get('v.validity').valid === false) {
                isValidForm = false;
            }
        }
        return isValidForm;
    }
})