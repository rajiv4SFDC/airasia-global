({
    
    qsToEventMap: {
        'startURL'  : 'e.c:setStartUrl'
    },

    handleLogin: function (component, event, helpler) {
        component.set("v.showError",false);
        if(this.validateData(component)) {
            var username = component.find("username").get("v.value");
            var password = component.find("password").get("v.value");
            var action = component.get("c.login");
            var startUrl = component.get("v.startUrl");
            
            startUrl = decodeURIComponent(startUrl);
            
            if(startUrl == 'undefined' || startUrl.indexOf("/s") == -1) {
                startUrl = '/s/case/Case/Recent';
            }

            action.setParams({username:username, password:password, startUrl:startUrl});
            action.setCallback(this, function(a){
                var rtnValue = a.getReturnValue();
                if (rtnValue !== null) {
                    component.set("v.errorMessage",rtnValue);
                    component.set("v.showError",true);
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    getIsUsernamePasswordEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsUsernamePasswordEnabled");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isUsernamePasswordEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getIsSelfRegistrationEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsSelfRegistrationEnabled");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isSelfRegistrationEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCommunityForgotPasswordUrl : function (component, event, helpler) {
        var action = component.get("c.getForgotPasswordUrl");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                rtnValue = rtnValue + '?language=' + component.get('v.language');
                component.set('v.communityForgotPasswordUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCommunitySelfRegisterUrl : function (component, event, helpler) {
        var action = component.get("c.getSelfRegistrationUrl");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                rtnValue = rtnValue + '?language=' + component.get('v.language');
                component.set('v.communitySelfRegisterUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    setLanguage : function(component) {
        component.set('v.language', this.getParameterByName('language'));
    },
    getParameterByName: function (name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    validateData: function(component) {
        var isValidForm = true;
        var cmpList = ['username', 'password'];
        var inputCmp;
        
        for (var i = 0; i < cmpList.length; i++) {
            inputCmp = component.find(cmpList[i]);
            inputCmp.showHelpMessageIfInvalid();
            if(inputCmp.get('v.validity').valid === false) {
                isValidForm = false;
            }
        }
        return isValidForm;
    }
})