@isTest public class ACEBookingService_Element_Test {

	@isTest public static void testCodeCoverage(){
        ACEBookingService.GetBookingStatelessFilteredResponse_element GetBookingStatelessFilteredResponse_elementObj = new ACEBookingService.GetBookingStatelessFilteredResponse_element();
        GetBookingStatelessFilteredResponse_elementObj.GetBookingStatelessFilteredResult_type_info = new String[] {''};
        GetBookingStatelessFilteredResponse_elementObj.apex_schema_type_info = new String[] {''};
        GetBookingStatelessFilteredResponse_elementObj.field_order_type_info = new String[] {''};
            
        ACEBookingService.UpdateContactsResponse_element UpdateContactsResponse_elementObj = new ACEBookingService.UpdateContactsResponse_element();
        UpdateContactsResponse_elementObj.UpdateContactsResult_type_info = new String[]{'UpdateContactsResult','http://tempuri.org/',null,'0','1','true'};
        UpdateContactsResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        UpdateContactsResponse_elementObj.field_order_type_info = new String[]{'UpdateContactsResult'};
        
         ACEBookingService.CommitStateless_element CommitStateless_elementObj = new ACEBookingService.CommitStateless_element();
        CommitStateless_elementObj.apex_schema_type_info = new String[] {''};
        CommitStateless_elementObj.field_order_type_info = new String[] {''};
        CommitStateless_elementObj.strSessionID_type_info = new String[] {''};       
            
       ACEBookingService.OverrideFeeResponse_element OverrideFeeResponse_elementObj = new ACEBookingService.OverrideFeeResponse_element();
       OverrideFeeResponse_elementObj.OverrideFeeResult = new ACEBookingService.Booking();
       OverrideFeeResponse_elementObj.OverrideFeeResult_type_info = new String[]{'OverrideFeeResult','http://tempuri.org/',null,'0','1','true'};
      OverrideFeeResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
      OverrideFeeResponse_elementObj.field_order_type_info = new String[]{'OverrideFeeResult'};
  
            ACEBookingService.UnassignSeats_element UnassignSeats_elementObj = new ACEBookingService.UnassignSeats_element();
        UnassignSeats_elementObj.strSessionID='';
       // public ACEBookingService.SeatSellRequest objSeatSellRequest;
       UnassignSeats_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
      UnassignSeats_elementObj.objSeatSellRequest_type_info = new String[]{'objSeatSellRequest','http://tempuri.org/',null,'0','1','true'};
       UnassignSeats_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
      UnassignSeats_elementObj.field_order_type_info = new String[]{'strSessionID','objSeatSellRequest'};
 
   ACEBookingService.ResellSSRResponse_element ResellSSRResponse_elementObj = new ACEBookingService.ResellSSRResponse_element();
      //  public ACEBookingService.BookingUpdateResponseData ResellSSRResult;
       ResellSSRResponse_elementObj.ResellSSRResult_type_info = new String[]{'ResellSSRResult','http://tempuri.org/',null,'0','1','true'};
       ResellSSRResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       ResellSSRResponse_elementObj.field_order_type_info = new String[]{'ResellSSRResult'};
  
    ACEBookingService.GetBookingHistoryResponse_element GetBookingHistoryResponse_elementObj = new ACEBookingService.GetBookingHistoryResponse_element();
        GetBookingHistoryResponse_elementObj.GetBookingHistoryResult = new ACEBookingService.GetBookingHistoryResponseData();
        GetBookingHistoryResponse_elementObj.GetBookingHistoryResult_type_info = new String[]{'GetBookingHistoryResult','http://tempuri.org/',null,'0','1','true'};
        GetBookingHistoryResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetBookingHistoryResponse_elementObj.field_order_type_info = new String[]{'GetBookingHistoryResult'};
    
  ACEBookingService.FindBookingResponse_element FindBookingResponse_elementObj = new ACEBookingService.FindBookingResponse_element();
      //  public ACEBookingService.FindBookingResponseData FindBookingResult;
        FindBookingResponse_elementObj.FindBookingResult_type_info = new String[]{'FindBookingResult','http://tempuri.org/',null,'0','1','true'};
            FindBookingResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
                FindBookingResponse_elementObj.field_order_type_info = new String[]{'FindBookingResult'};
                    
                    ACEBookingService.Cancel_element Cancel_elementObj = new ACEBookingService.Cancel_element();
       Cancel_elementObj.strSessionID='';
      Cancel_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
         Cancel_elementObj.objCancelRequestData_type_info = new String[]{'objCancelRequestData','http://tempuri.org/',null,'0','1','true'};
         Cancel_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
          Cancel_elementObj.field_order_type_info = new String[]{'strSessionID','objCancelRequestData'};

    ACEBookingService.GetBookingPayments_element GetBookingPayments_elementObj = new ACEBookingService.GetBookingPayments_element();
       GetBookingPayments_elementObj.strSessionID='';
     GetBookingPayments_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
         GetBookingPayments_elementObj.objGetBookingPaymentsRequestData_type_info = new String[]{'objGetBookingPaymentsRequestData','http://tempuri.org/',null,'0','1','true'};
          GetBookingPayments_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
            GetBookingPayments_elementObj.field_order_type_info = new String[]{'strSessionID','objGetBookingPaymentsRequestData'};
    
    ACEBookingService.UpdateSSR_element UpdateSSR_elementObj = new ACEBookingService.UpdateSSR_element();
       UpdateSSR_elementObj.strSessionID='';
      UpdateSSR_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
      UpdateSSR_elementObj.objSSRRequest_type_info = new String[]{'objSSRRequest','http://tempuri.org/',null,'0','1','true'};
        UpdateSSR_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        UpdateSSR_elementObj.field_order_type_info = new String[]{'strSessionID','objSSRRequest'};
    
     ACEBookingService.GetSSRAvailability_element GetSSRAvailability_elementObj = new ACEBookingService.GetSSRAvailability_element();
        GetSSRAvailability_elementObj.strSessionID='';
       GetSSRAvailability_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        GetSSRAvailability_elementObj.objSSRAvailabilityRequest_type_info = new String[]{'objSSRAvailabilityRequest','http://tempuri.org/',null,'0','1','true'};
       GetSSRAvailability_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetSSRAvailability_elementObj.field_order_type_info = new String[]{'strSessionID','objSSRAvailabilityRequest'};
    
		ACEBookingService.GetBookingFromStateFiltered_element GetBookingFromStateFiltered_elementObj = new ACEBookingService.GetBookingFromStateFiltered_element();
       GetBookingFromStateFiltered_elementObj.strSessionID='';
      GetBookingFromStateFiltered_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
       GetBookingFromStateFiltered_elementObj.objGetBookingFromStateFilteredRequestData_type_info = new String[]{'objGetBookingFromStateFilteredRequestData','http://tempuri.org/',null,'0','1','true'};
       GetBookingFromStateFiltered_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetBookingFromStateFiltered_elementObj.field_order_type_info = new String[]{'strSessionID','objGetBookingFromStateFilteredRequestData'};
    
		ACEBookingService.CancelInProcessPaymentResponse_element CancelInProcessPaymentResponse_elementObj = new ACEBookingService.CancelInProcessPaymentResponse_element();
        CancelInProcessPaymentResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        CancelInProcessPaymentResponse_elementObj.field_order_type_info = new String[]{};
    
  ACEBookingService.FareOverride_element FareOverride_elementObj = new ACEBookingService.FareOverride_element();
        FareOverride_elementObj.strSessionID='';
      FareOverride_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        FareOverride_elementObj.objFareOverrideRequestData_type_info = new String[]{'objFareOverrideRequestData','http://tempuri.org/',null,'0','1','true'};
        FareOverride_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       FareOverride_elementObj.field_order_type_info = new String[]{'strSessionID','objFareOverrideRequestData'};
    
    ACEBookingService.ClearResponse_element ClearResponse_elementObj = new ACEBookingService.ClearResponse_element();
        ClearResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        ClearResponse_elementObj.field_order_type_info = new String[]{};
    
              ACEBookingService.SendItinerary_element SendItinerary_elementObj = new ACEBookingService.SendItinerary_element();
        SendItinerary_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
       SendItinerary_elementObj.strRecordLocator_type_info = new String[]{'strRecordLocator','http://tempuri.org/',null,'0','1','true'};
       SendItinerary_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       SendItinerary_elementObj.field_order_type_info = new String[]{'strSessionID','strRecordLocator'};
    
   ACEBookingService.AddInProcessPaymentToBooking_element AddInProcessPaymentToBooking_elementObj = new ACEBookingService.AddInProcessPaymentToBooking_element();
        AddInProcessPaymentToBooking_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
      AddInProcessPaymentToBooking_elementObj.objPayment_type_info = new String[]{'objPayment','http://tempuri.org/',null,'0','1','true'};
     AddInProcessPaymentToBooking_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       AddInProcessPaymentToBooking_elementObj.field_order_type_info = new String[]{'strSessionID','objPayment'};
    
    ACEBookingService.GetBookingHistory_element GetBookingHistory_elementObj = new ACEBookingService.GetBookingHistory_element();
       GetBookingHistory_elementObj.strSessionID='';
       // GetBookingHistory_elementObj.GetBookingHistoryRequestData= new ACEBookingService.GetBookingHistoryRequestData();
       GetBookingHistory_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        GetBookingHistory_elementObj.objGetBookingHistoryRequestData_type_info = new String[]{'objGetBookingHistoryRequestData','http://tempuri.org/',null,'0','1','true'};
        GetBookingHistory_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        GetBookingHistory_elementObj.field_order_type_info = new String[]{'strSessionID','objGetBookingHistoryRequestData'};
    
               ACEBookingService.SendItineraryResponse_element SendItineraryResponse_elementObj = new ACEBookingService.SendItineraryResponse_element();
        SendItineraryResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        SendItineraryResponse_elementObj.field_order_type_info = new String[]{};
    
	
	  ACEBookingService.Clear_element Clear_elementObj = new ACEBookingService.Clear_element();
        Clear_elementObj.strSessionID='';
        Clear_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        Clear_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        Clear_elementObj.field_order_type_info = new String[]{'strSessionID'};
    
	
	
	   ACEBookingService.GetBookingFromStateResponse_element GetBookingFromStateResponse_elementObj = new ACEBookingService.GetBookingFromStateResponse_element();
       // public ACEBookingService.Booking GetBookingFromStateResult;
        GetBookingFromStateResponse_elementObj.GetBookingFromStateResult_type_info = new String[]{'GetBookingFromStateResult','http://tempuri.org/',null,'0','1','true'};
       GetBookingFromStateResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetBookingFromStateResponse_elementObj.field_order_type_info = new String[]{'GetBookingFromStateResult'};
    
	
	    ACEBookingService.CancelInProcessPayment_element CancelInProcessPayment_elementObj = new ACEBookingService.CancelInProcessPayment_element();
        CancelInProcessPayment_elementObj.strSessionID='';
        CancelInProcessPayment_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        CancelInProcessPayment_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        CancelInProcessPayment_elementObj.field_order_type_info = new String[]{'strSessionID'};
    
	
	    ACEBookingService.GetBookingResponse_element GetBookingResponse_elementObj = new ACEBookingService.GetBookingResponse_element();
       // public ACEBookingService.Booking GetBookingResult;
       GetBookingResponse_elementObj.GetBookingResult_type_info = new String[]{'GetBookingResult','http://tempuri.org/',null,'0','1','true'};
       GetBookingResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetBookingResponse_elementObj.field_order_type_info = new String[]{'GetBookingResult'};
    
    ACEBookingService.GetBookingFromState_element GetBookingFromState_elementObj = new ACEBookingService.GetBookingFromState_element();
       GetBookingFromState_elementObj.strSessionID='';
       GetBookingFromState_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
      GetBookingFromState_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
      GetBookingFromState_elementObj.field_order_type_info = new String[]{'strSessionID'};
    ACEBookingService.PassengerInfo PassengerInfoObj = new ACEBookingService.PassengerInfo();
        PassengerInfoObj.State_type_info = new String[]{'State','http://tempuri.org/',null,'1','1','true'};
        PassengerInfoObj.BalanceDue_type_info = new String[]{'BalanceDue','http://tempuri.org/',null,'1','1','true'};
        PassengerInfoObj.Gender_type_info = new String[]{'Gender','http://tempuri.org/',null,'1','1','true'};
        PassengerInfoObj.Nationality_type_info = new String[]{'Nationality','http://tempuri.org/',null,'0','1','false'};
        PassengerInfoObj.ResidentCountry_type_info = new String[]{'ResidentCountry','http://tempuri.org/',null,'0','1','false'};
        PassengerInfoObj.TotalCost_type_info = new String[]{'TotalCost','http://tempuri.org/',null,'1','1','true'};
        PassengerInfoObj.WeightCategory_type_info = new String[]{'WeightCategory','http://tempuri.org/',null,'1','1','true'};
        PassengerInfoObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        PassengerInfoObj.field_order_type_info = new String[]{'State','BalanceDue','Gender','Nationality','ResidentCountry','TotalCost','WeightCategory'};

        ACEBookingService.Fare FareObj = new ACEBookingService.Fare();
        FareObj.State_type_info = new String[]{'State','http://tempuri.org/',null,'1','1','true'};
        FareObj.ClassOfService_type_info = new String[]{'ClassOfService','http://tempuri.org/',null,'0','1','false'};
        FareObj.ClassType_type_info = new String[]{'ClassType','http://tempuri.org/',null,'0','1','false'};
        FareObj.RuleTariff_type_info = new String[]{'RuleTariff','http://tempuri.org/',null,'0','1','false'};
        FareObj.CarrierCode_type_info = new String[]{'CarrierCode','http://tempuri.org/',null,'0','1','false'};
        FareObj.RuleNumber_type_info = new String[]{'RuleNumber','http://tempuri.org/',null,'0','1','false'};
        FareObj.FareBasisCode_type_info = new String[]{'FareBasisCode','http://tempuri.org/',null,'0','1','false'};
        FareObj.FareSequence_type_info = new String[]{'FareSequence','http://tempuri.org/',null,'1','1','true'};
        FareObj.FareClassOfService_type_info = new String[]{'FareClassOfService','http://tempuri.org/',null,'0','1','false'};
        FareObj.FareStatus_type_info = new String[]{'FareStatus','http://tempuri.org/',null,'1','1','true'};
        FareObj.FareApplicationType_type_info = new String[]{'FareApplicationType','http://tempuri.org/',null,'1','1','true'};
        FareObj.OriginalClassOfService_type_info = new String[]{'OriginalClassOfService','http://tempuri.org/',null,'0','1','false'};
        FareObj.XrefClassOfService_type_info = new String[]{'XrefClassOfService','http://tempuri.org/',null,'0','1','false'};
        FareObj.PaxFares_type_info = new String[]{'PaxFares','http://tempuri.org/',null,'0','1','true'};
        FareObj.ProductClass_type_info = new String[]{'ProductClass','http://tempuri.org/',null,'0','1','false'};
        FareObj.IsAllotmentMarketFare_type_info = new String[]{'IsAllotmentMarketFare','http://tempuri.org/',null,'1','1','true'};
        FareObj.TravelClassCode_type_info = new String[]{'TravelClassCode','http://tempuri.org/',null,'0','1','false'};
        FareObj.FareSellKey_type_info = new String[]{'FareSellKey','http://tempuri.org/',null,'0','1','false'};
        FareObj.InboundOutbound_type_info = new String[]{'InboundOutbound','http://tempuri.org/',null,'1','1','true'};
        FareObj.AvailableCount_type_info = new String[]{'AvailableCount','http://tempuri.org/',null,'1','1','true'};
        FareObj.Status_type_info = new String[]{'Status','http://tempuri.org/',null,'1','1','true'};
        FareObj.SSRIndexes_type_info = new String[]{'SSRIndexes','http://tempuri.org/',null,'0','1','true'};
        FareObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        FareObj.field_order_type_info = new String[]{'State','ClassOfService','ClassType','RuleTariff','CarrierCode','RuleNumber','FareBasisCode','FareSequence','FareClassOfService','FareStatus','FareApplicationType','OriginalClassOfService','XrefClassOfService','PaxFares','ProductClass','IsAllotmentMarketFare','TravelClassCode','FareSellKey','InboundOutbound','AvailableCount','Status','SSRIndexes'};
    
    ACEBookingService.Leg LegObj = new ACEBookingService.Leg();
    LegObj.State_type_info = new String[]{'State','http://tempuri.org/',null,'1','1','true'};
        LegObj.ArrivalStation_type_info = new String[]{'ArrivalStation','http://tempuri.org/',null,'0','1','false'};
        LegObj.DepartureStation_type_info = new String[]{'DepartureStation','http://tempuri.org/',null,'0','1','false'};
        LegObj.STA_type_info = new String[]{'STA','http://tempuri.org/',null,'1','1','true'};
        LegObj.STD_type_info = new String[]{'STD','http://tempuri.org/',null,'1','1','true'};
        LegObj.FlightDesignator_type_info = new String[]{'FlightDesignator','http://tempuri.org/',null,'0','1','false'};
        LegObj.LegInfo_type_info = new String[]{'LegInfo','http://tempuri.org/',null,'0','1','false'};
        LegObj.OperationsInfo_type_info = new String[]{'OperationsInfo','http://tempuri.org/',null,'0','1','false'};
        LegObj.InventoryLegID_type_info = new String[]{'InventoryLegID','http://tempuri.org/',null,'1','1','true'};
        LegObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        LegObj.field_order_type_info = new String[]{'State','ArrivalStation','DepartureStation','STA','STD','FlightDesignator','LegInfo','OperationsInfo','InventoryLegID'};

    ACEBookingService.BookingName BookingNameObj = new ACEBookingService.BookingName();
    BookingNameObj.State_type_info = new String[]{'State','http://tempuri.org/',null,'1','1','true'};
        BookingNameObj.FirstName_type_info = new String[]{'FirstName','http://tempuri.org/',null,'0','1','false'};
        BookingNameObj.MiddleName_type_info = new String[]{'MiddleName','http://tempuri.org/',null,'0','1','false'};
        BookingNameObj.LastName_type_info = new String[]{'LastName','http://tempuri.org/',null,'0','1','false'};
        BookingNameObj.Suffix_type_info = new String[]{'Suffix','http://tempuri.org/',null,'0','1','false'};
        BookingNameObj.Title_type_info = new String[]{'Title','http://tempuri.org/',null,'0','1','false'};
        BookingNameObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        BookingNameObj.field_order_type_info = new String[]{'State','FirstName','MiddleName','LastName','Suffix','Title'};

    ACEBookingService.UnassignSeatsResponse_element UnassignSeatsResponse_elementObj = new ACEBookingService.UnassignSeatsResponse_element();
    UnassignSeatsResponse_elementObj.UnassignSeatsResult_type_info = new String[]{'UnassignSeatsResult','http://tempuri.org/',null,'0','1','false'};
    UnassignSeatsResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
    UnassignSeatsResponse_elementObj.field_order_type_info = new String[]{'UnassignSeatsResult'};
	
	 ACEBookingService.GetBooking_element GetBooking_elementObj = new ACEBookingService.GetBooking_element();
       GetBooking_elementObj.strSessionID='';
      //  public ACEBookingService.GetBookingRequestData objGetBookingRequestData;
        GetBooking_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
       GetBooking_elementObj.objGetBookingRequestData_type_info = new String[]{'objGetBookingRequestData','http://tempuri.org/',null,'0','1','true'};
      GetBooking_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
      GetBooking_elementObj.field_order_type_info = new String[]{'strSessionID','objGetBookingRequestData'};
    
    ACEBookingService.GetBookingRequestData GetBookingRequestDataObj = new ACEBookingService.GetBookingRequestData();
    GetBookingRequestDataObj.GetBookingBy_type_info = new String[]{'GetBookingBy','http://tempuri.org/',null,'1','1','true'};
    GetBookingRequestDataObj.GetByRecordLocator_type_info = new String[]{'GetByRecordLocator','http://tempuri.org/',null,'0','1','false'};
    GetBookingRequestDataObj.GetByThirdPartyRecordLocator_type_info = new String[]{'GetByThirdPartyRecordLocator','http://tempuri.org/',null,'0','1','false'};
    GetBookingRequestDataObj.GetByID_type_info = new String[]{'GetByID','http://tempuri.org/',null,'0','1','false'};
    GetBookingRequestDataObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
    GetBookingRequestDataObj.field_order_type_info = new String[]{'GetBookingBy','GetByRecordLocator','GetByThirdPartyRecordLocator','GetByID'};

         ACEBookingService.Segment SegmentObj = new ACEBookingService.Segment();
         SegmentObj.State_type_info = new String[]{'State','http://tempuri.org/',null,'1','1','true'};
       SegmentObj.ActionStatusCode_type_info = new String[]{'ActionStatusCode','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.ArrivalStation_type_info = new String[]{'ArrivalStation','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.CabinOfService_type_info = new String[]{'CabinOfService','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.ChangeReasonCode_type_info = new String[]{'ChangeReasonCode','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.DepartureStation_type_info = new String[]{'DepartureStation','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.PriorityCode_type_info = new String[]{'PriorityCode','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.SegmentType_type_info = new String[]{'SegmentType','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.STA_type_info = new String[]{'STA','http://tempuri.org/',null,'1','1','true'};
        SegmentObj.STD_type_info = new String[]{'STD','http://tempuri.org/',null,'1','1','true'};
        SegmentObj.International_type_info = new String[]{'International','http://tempuri.org/',null,'1','1','true'};
        SegmentObj.FlightDesignator_type_info = new String[]{'FlightDesignator','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.XrefFlightDesignator_type_info = new String[]{'XrefFlightDesignator','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.Fares_type_info = new String[]{'Fares','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.Legs_type_info = new String[]{'Legs','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.PaxBags_type_info = new String[]{'PaxBags','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.PaxSeats_type_info = new String[]{'PaxSeats','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.PaxSSRs_type_info = new String[]{'PaxSSRs','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.PaxSegments_type_info = new String[]{'PaxSegments','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.PaxTickets_type_info = new String[]{'PaxTickets','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.PaxSeatPreferences_type_info = new String[]{'PaxSeatPreferences','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.SalesDate_type_info = new String[]{'SalesDate','http://tempuri.org/',null,'1','1','true'};
        SegmentObj.SegmentSellKey_type_info = new String[]{'SegmentSellKey','http://tempuri.org/',null,'0','1','false'};
        SegmentObj.PaxScores_type_info = new String[]{'PaxScores','http://tempuri.org/',null,'0','1','true'};
        SegmentObj.ChannelType_type_info = new String[]{'ChannelType','http://tempuri.org/',null,'1','1','true'};
        SegmentObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        SegmentObj.field_order_type_info = new String[]{'State','ActionStatusCode','ArrivalStation','CabinOfService','ChangeReasonCode','DepartureStation','PriorityCode','SegmentType','STA','STD','International','FlightDesignator','XrefFlightDesignator','Fares','Legs','PaxBags','PaxSeats','PaxSSRs','PaxSegments','PaxTickets','PaxSeatPreferences','SalesDate','SegmentSellKey','PaxScores','ChannelType'};

        ACEBookingService.RecordLocator RecordLocatorObj = new ACEBookingService.RecordLocator();
        RecordLocatorObj.State_type_info = new String[]{'State','http://tempuri.org/',null,'1','1','true'};
        RecordLocatorObj.SystemDomainCode_type_info = new String[]{'SystemDomainCode','http://tempuri.org/',null,'0','1','false'};
        RecordLocatorObj.SystemCode_type_info = new String[]{'SystemCode','http://tempuri.org/',null,'0','1','false'};
        RecordLocatorObj.RecordCode_type_info = new String[]{'RecordCode','http://tempuri.org/',null,'0','1','false'};
        RecordLocatorObj.InteractionPurpose_type_info = new String[]{'InteractionPurpose','http://tempuri.org/',null,'0','1','false'};
        RecordLocatorObj.HostedCarrierCode_type_info = new String[]{'HostedCarrierCode','http://tempuri.org/',null,'0','1','false'};
        RecordLocatorObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        RecordLocatorObj.field_order_type_info = new String[]{'State','SystemDomainCode','SystemCode','RecordCode','InteractionPurpose','HostedCarrierCode'};

        ACEBookingService.PaxSegment PaxSegmentObj = new ACEBookingService.PaxSegment();
        PaxSegmentObj.State_type_info = new String[]{'State','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.BoardingSequence_type_info = new String[]{'BoardingSequence','http://tempuri.org/',null,'0','1','false'};
        PaxSegmentObj.CreatedDate_type_info = new String[]{'CreatedDate','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.LiftStatus_type_info = new String[]{'LiftStatus','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.OverBookIndicator_type_info = new String[]{'OverBookIndicator','http://tempuri.org/',null,'0','1','false'};
        PaxSegmentObj.PassengerNumber_type_info = new String[]{'PassengerNumber','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.PriorityDate_type_info = new String[]{'PriorityDate','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.TripType_type_info = new String[]{'TripType','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.TimeChanged_type_info = new String[]{'TimeChanged','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.POS_type_info = new String[]{'POS','http://tempuri.org/',null,'0','1','false'};
        PaxSegmentObj.SourcePOS_type_info = new String[]{'SourcePOS','http://tempuri.org/',null,'0','1','false'};
        PaxSegmentObj.VerifiedTravelDocs_type_info = new String[]{'VerifiedTravelDocs','http://tempuri.org/',null,'0','1','false'};
        PaxSegmentObj.ModifiedDate_type_info = new String[]{'ModifiedDate','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.ActivityDate_type_info = new String[]{'ActivityDate','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.BaggageAllowanceWeight_type_info = new String[]{'BaggageAllowanceWeight','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.BaggageAllowanceWeightType_type_info = new String[]{'BaggageAllowanceWeightType','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.BaggageAllowanceUsed_type_info = new String[]{'BaggageAllowanceUsed','http://tempuri.org/',null,'1','1','true'};
        PaxSegmentObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        PaxSegmentObj.field_order_type_info = new String[]{'State','BoardingSequence','CreatedDate','LiftStatus','OverBookIndicator','PassengerNumber','PriorityDate','TripType','TimeChanged','POS','SourcePOS','VerifiedTravelDocs','ModifiedDate','ActivityDate','BaggageAllowanceWeight','BaggageAllowanceWeightType','BaggageAllowanceUsed'};
	}
    
    
      @isTest public static void testGetBookingHistoryMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new ACEBookingHistoryResponseMock());
        Test.startTest();
        ACEBookingService.BasicHttpsBinding_IBookingService basicHttpsObj1 = new ACEBookingService.BasicHttpsBinding_IBookingService();
        basicHttpsObj1.GetBookingHistory('',new ACEBookingService.GetBookingHistoryRequestData());
        Test.stopTest();
        
      
      }
    
    
      @isTest public static void testClearSessionMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new ACEBookingClearSessionMock());
        Test.startTest();
        ACEBookingService.BasicHttpsBinding_IBookingService basicHttpsObj1 = new ACEBookingService.BasicHttpsBinding_IBookingService();
        basicHttpsObj1.Clear('');
        Test.stopTest();
        
      
      }
    
     @isTest public static void testGetBookingMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new ACEBookingBookingMock());
        Test.startTest();
        ACEBookingService.BasicHttpsBinding_IBookingService basicHttpsObj1 = new ACEBookingService.BasicHttpsBinding_IBookingService();
        basicHttpsObj1.GetBooking('',new ACEBookingService.GetBookingRequestData());
        Test.stopTest();
        
      
      }
    
      @isTest public static void testSendItineraryMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new ACEBookingItineraryMock());
        Test.startTest();
        ACEBookingService.BasicHttpsBinding_IBookingService basicHttpsObj1 = new ACEBookingService.BasicHttpsBinding_IBookingService();
        basicHttpsObj1.SendItinerary('','');
        Test.stopTest();
        
      
      }
    
    
    
    
    
}