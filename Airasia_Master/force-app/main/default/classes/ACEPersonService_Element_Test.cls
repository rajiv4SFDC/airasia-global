@isTest
public class ACEPersonService_Element_Test {
    @isTest public static void testCodeCoverage(){
        
        
        ACEPersonService ACEPersonServiceObj = new ACEPersonService();
        ACEPersonService.FindPersonsResponse_element findPersonsResponseElObj= new ACEPersonService.FindPersonsResponse_element();
        findPersonsResponseElObj.FindPersonsResult_type_info = new String[]{'FindPersonsResult','http://tempuri.org/',null,'0','1','true'};
        findPersonsResponseElObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        findPersonsResponseElObj.field_order_type_info = new String[]{'FindPersonsResult'};
            
            
            
        ACEPersonService.FindPersons_element FindPersons_elementObj = new ACEPersonService.FindPersons_element();
        FindPersons_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        FindPersons_elementObj.objFindPersonRequestData_type_info = new String[]{'objFindPersonRequestData','http://tempuri.org/',null,'0','1','true'};
        FindPersons_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
		FindPersons_elementObj.field_order_type_info = new String[]{'strSessionID','objFindPersonRequestData'};
                        
            
            
        ACEPersonService.GetPerson_element GetPerson_elementobj = new ACEPersonService.GetPerson_element();
        GetPerson_elementobj.strSessionID='';
        GetPerson_elementobj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        GetPerson_elementobj.objGetPersonRequestData_type_info = new String[]{'objGetPersonRequestData','http://tempuri.org/',null,'0','1','true'};
        GetPerson_elementobj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        GetPerson_elementobj.field_order_type_info = new String[]{'strSessionID','objGetPersonRequestData'};
    
        ACEPersonService.GetPersonResponse_element GetPersonResponse_elementObj = new ACEPersonService.GetPersonResponse_element();

        GetPersonResponse_elementObj.GetPersonResult=new ACEPersonService.Person();
        //ACEPersonService.Person = new ACEPersonService.Person();
        GetPersonResponse_elementObj.GetPersonResult_type_info = new String[]{'GetPersonResult','http://tempuri.org/',null,'0','1','true'};
        GetPersonResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        GetPersonResponse_elementObj.field_order_type_info = new String[]{'GetPersonResult'};
    
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new ACEPersonMock());
        Test.startTest();
        ACEPersonService.BasicHttpsBinding_IPersonService basicHttpsObj = new ACEPersonService.BasicHttpsBinding_IPersonService();
        basicHttpsObj.GetPerson('',new ACEPersonService.GetPersonRequestData());
        Test.stopTest();
        
        
               
        
    }
    
     @isTest public static void testCodeCoverageForRequest(){
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new ACEPersonFindMock());
        Test.startTest();
        ACEPersonService.BasicHttpsBinding_IPersonService basicHttpsObj1 = new ACEPersonService.BasicHttpsBinding_IPersonService();
        basicHttpsObj1.FindPersons('',new ACEPersonService.FindPersonRequest());
        Test.stopTest();
        
        
    }
}