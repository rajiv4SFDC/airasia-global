public class AVAAttachmentFuture {
    
	@future (callout = true)
    public static void callAVAAttachmentBatch(Set<Id> caseIds) {
        List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();
    	System_Settings__mdt logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('AVA_Attachment_Error'));
        Datetime startTime = DateTime.now();        
        Web_Services_Credentials__mdt avaAttachmentExistsUrl = getAVAAttachmentExistsURL();
        Web_Services_Credentials__mdt avaAttachmentUrl = getAVAAttachmentURL();
        
        List<Case> caseList = [SELECT Id, 
                                   ADA_DocType__c, 
                                   Ada_Chatter_ID__c, 
                                   Booking_Number__c 
                               FROM Case 
                               WHERE Id =: caseIds];
        if(avaAttachmentUrl != null && avaAttachmentUrl.URL__c != '') {
            for(Case c : caseList) {
                //Check if it Exists
                Boolean fileExists = false;
                
                String urlExist = avaAttachmentExistsUrl.URL__c;
                urlExist = urlExist.replace('/chatterId/', '/' + c.Ada_Chatter_ID__c + '/');
                Http hExist = new Http();
                HttpRequest reqExist = new HttpRequest();
                reqExist.setEndpoint(urlExist);
                reqExist.setMethod('POST');
                reqExist.setHeader('Content-Type', 'application/json');
                String reqExistBody = '{' +
                                     '"booking_id": "' + c.Booking_Number__c + '",' +
                                     '"doc_type": "'+ c.ADA_DocType__c + '"' +
                                    '}';
                reqExist.setBody(reqExistBody);
                try {
                    HttpResponse resExist = hExist.send(reqExist);
                    if(resExist.getStatusCode() == 200) {
                        fileExists = true;
                    }
                }
                catch (Exception e) {
                    appWrapperLogList.add(Utilities.createApplicationLog('Error', 'AVAAttachmentFuture', 'execute',
                                                                                            '', 'Exception on Requesting API to Upload File to Case', reqExistBody, reqExistBody, 'Error Log', startTime, logLevelCMD, e));
                    
                }
                
                if (fileExists) {
                //Upload Attachment
                    String urlUpload = avaAttachmentUrl.URL__c;
                    Http hUpload = new Http();
                    HttpRequest reqUpload = new HttpRequest();
                    reqUpload.setTimeout(20000);
                    reqUpload.setEndpoint(urlUpload);
                    reqUpload.setMethod('POST');
                    reqUpload.setHeader('Content-Type', 'application/json');
                    String reqBody = '{' +
                                         '"pnr": "' + c.Booking_Number__c + '",' +
                                         '"chatId": "' + c.Ada_Chatter_ID__c + '",' +
                                         '"docType": "'+ c.ADA_DocType__c + '",' +
                                         '"caseId": "' + c.Id + '"' +
                                        '}';
                    reqUpload.setBody(reqBody);
                    try {
                        HttpResponse resUpload = hUpload.send(reqUpload);
                        if(resUpload.getStatusCode() != 200) {
                            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'AVAAttachmentFuture', 'execute',
                                                                                                '', 'Exception on Requesting API to Upload File to Case', reqBody, resUpload.getBody(), 'Error Log', startTime, logLevelCMD, null));
                        }
                    }
                    catch (Exception e) {
                        appWrapperLogList.add(Utilities.createApplicationLog('Error', 'AVAAttachmentFuture', 'execute',
                                                                                                '', 'Exception on Requesting API to Upload File to Case', reqBody, reqBody, 'Error Log', startTime, logLevelCMD, e));
                        
                    }
                }
            }
        }
        
        if(!appWrapperLogList.isEmpty()) {
            GlobalUtility.logMessage(appWrapperLogList);
        }
    }
    
    private static Web_Services_Credentials__mdt getAVAAttachmentExistsURL() {
        List<Web_Services_Credentials__mdt> credentials = [SELECT Url__c
                                                          FROM Web_Services_Credentials__mdt
                                                          WHERE Web_Service_Name__c = 'WS_AVA_UPLOAD_EXIST'];

        if(credentials.size() > 0 && credentials != null) {
            return credentials[0];
        }
        return null;
    }
    
    private static Web_Services_Credentials__mdt getAVAAttachmentURL() {
        List<Web_Services_Credentials__mdt> credentials = [SELECT Url__c
                                                          FROM Web_Services_Credentials__mdt
                                                          WHERE Web_Service_Name__c = 'WS_AVA_UPLOAD'];

        if(credentials.size() > 0 && credentials != null) {
            return credentials[0];
        }
        return null;
    }
}