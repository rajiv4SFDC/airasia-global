/**
 * @File Name          : AVATranscriptFuture.cls
 * @Description        : Retrieve Transcript from ADA
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 22/11/2019 5:17:41 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/19/2019, 3:18:35 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
 * 2.0    10/30/2019, 3:18:35 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Added WeChatBot
 * 3.0	  22/11/2019 5:17:41 PM		Mark Alvin Tayag (mtayag@salesforce.com)	Changed to use origin to retrieve Ada Url
**/
public class AVATranscriptFuture {     
    
    @future (callout = true)
    public static void retrieveTranscripts(Set<Id> caseIds) {
    	Datetime startTime = DateTime.now();   
    	System_Settings__mdt logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('AVA_Transcript_Error'));
        try {
            List<CaseComment> caseComments = new List<CaseComment>();
            List<Case> caseList = [SELECT Id, 
                                       Ada_Chatter_Token__c,
                                   	   Origin
                                   FROM Case 
                                   WHERE Id =: caseIds];
            
            for(Case c : caseList) {
                String transcript = getTextFileContentAsString(c.Ada_Chatter_Token__c, c.Origin);
                caseComments.addAll(convertToCaseComments(c.Id, transcript));
            }
            
            Database.insert(caseComments, false);
        }
        catch (Exception e) {
            GlobalUtility.logMessage(Utilities.createApplicationLog('Error', 'AVATranscriptFuture', 'retrieveTranscripts',
                                                                 '', 'Error in retrieving transcript for Case', '', 
                                                                 '', 'Error Log', startTime, logLevelCMD, e));
        }
    }
    
    // go to Ada's database to get the chat transcript leading up to the live chat
    public static String getTextFileContentAsString(String chatter_token, String origin) {
    	Web_Services_Credentials__mdt avaTranscriptURL = getAVATranscriptURL(origin);
        if(avaTranscriptURL != null) {
        	String url = avaTranscriptURL.Url__c;
            url = url.replace('chatter_token', chatter_token);
                        
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('GET');
            HttpResponse res = h.send(req);
            
            return res.getBody();
        }
        
        return origin + ' is not registered in the Web Services Custom Metadata. Please contact your Salesforce Administrator for assistance.';
    }
    
    private static List<CaseComment> convertToCaseComments(String caseId, String transcript) {
        List<CaseComment> caseComments = new List<CaseComment>();
        
        for (String transcriptChunk : AdaFileUploadCSVBatchApex.splitTranscript(transcript)) {
            caseComments.add(new CaseComment(ParentId = caseId,CommentBody = transcriptChunk));
        }
        
        return caseComments;
    }
    
    public static Web_Services_Credentials__mdt getAVATranscriptURL(String origin) {
        Web_Services_Credentials__mdt url;
        url = [SELECT Url__c
               FROM Web_Services_Credentials__mdt
               WHERE Web_Service_Name__c = :origin];
        return url;
    }
    
    /*
    public static Web_Services_Credentials__mdt getB2BAVATranscriptURL() {
        Web_Services_Credentials__mdt url;
        url = [SELECT Url__c
               FROM Web_Services_Credentials__mdt
               WHERE Web_Service_Name__c = 'B2B_ADA_Transcript'];
        return url;
    }
    
    public static Web_Services_Credentials__mdt getWeChatAVATranscriptURL() {
        Web_Services_Credentials__mdt url;
        url = [SELECT Url__c
               FROM Web_Services_Credentials__mdt
               WHERE Web_Service_Name__c = 'WeChat_ADA_Transcript'];
        return url;
    }
	*/
}