public class AVATranscriptMock implements HttpCalloutMock {
    public HttpResponse respond(HttpRequest rqst) {
            HttpResponse res = new HttpResponse();
            res.setBody('test');
            res.setStatusCode(200);
            
            return res;
        }
}