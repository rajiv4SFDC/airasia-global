/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/09/2017      Sharan Desai   		Created
*******************************************************************/
public with sharing class AccountTriggerHandler {
    
    
    /**
    * This method performs all the required operation on AfterInsert context
    * 
    **/
    public static void isAfterInsert(Account[] accounts){
        
        //-- STEP 1:- Call method to reparent Lead's Opportunity and Activity records macthing Account orgCode
        AccountTriggerHelper.reparentOpportunityAndActivity(accounts);
        
    }
    
    /**
    * This method performs all the required operation on AfterInsert context
    * 
    **/
    public static void isAfterUpdate(Account[] accounts){
        
        //-- STEP 1:- Call method to reparent Lead's Opportunity and Activity records macthing Account orgCode
        AccountTriggerHelper.reparentOpportunityAndActivity(accounts);
        
    }
    

}