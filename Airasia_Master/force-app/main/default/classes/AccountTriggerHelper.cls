/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/09/2017      Sharan Desai   		Created
*******************************************************************/

public with sharing class AccountTriggerHelper {
    
    //@TestVisible static boolean throwUnitTestExceptions=false;
    @TestVisible static boolean throwMainUnitTestExceptions=false;
    
    /**
    * This method updates Booking Channel and Sub Channel based on SalesChannel
    * 
    **/
    public static void reparentOpportunityAndActivity(Account[] accounts){
        
        DateTime startTime = DateTime.now();
        List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
        System_Settings__mdt logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('AccountTrigger_Log_Level_CMD_NAME'));
       
        try{                      
            Map<String,String> orgCodeAndAccIdMap = new   Map<String,String>(); 
            for(Account eachAccount : accounts){            
                if(String.isNotBlank(eachAccount.Organization_Code__c)){
                    orgCodeAndAccIdMap.put(eachAccount.Organization_Code__c,eachAccount.Id);
                }
            }
            
            if(orgCodeAndAccIdMap.size()>0){     
                
                //-- This block of code is for Unit Testing
                if(Test.isRunningTest() && throwMainUnitTestExceptions){
                    orgCodeAndAccIdMap=null;                    
                }
                List<lead> leadList = new List<lead>();
                 Map<String,Lead> contactExtIdAndLeadObjectMap = new Map<String,Lead>();
                List<String> leadIdList = new List<String>();
                for(Lead eachLead : [SELECT ID,Organization_Code__c FROM LEAD WHERE Organization_Code__c IN:orgCodeAndAccIdMap.keySet()]){
                    String matchingAccountId = orgCodeAndAccIdMap.containsKey(eachLead.Organization_Code__c)?
                        orgCodeAndAccIdMap.get(eachLead.Organization_Code__c):'';
                    
                    //-- Process logic if the Leads OrgCode has a matching Account record
                    if(String.isNotBlank(matchingAccountId)){
                        Database.executeBatch(new LeadConvertBatch(matchingAccountId,eachLead.ID,LeadConvertConstants.OPPORTUNITY_OBJECT_NAME));
                        Database.executeBatch(new LeadConvertBatch(matchingAccountId,eachLead.ID,LeadConvertConstants.TASK_OBJECT_NAME));
                        Database.executeBatch(new LeadConvertBatch(matchingAccountId,eachLead.ID,LeadConvertConstants.EVENT_OBJECT_NAME));                       
                    	eachLead.Status='Closed Converted';
                        
                        leadList.add(eachLead);
                        leadIdList.add(eachLead.Id);
                        
                        String contactExtIdString ='';                                
                        if(String.isNotBlank(eachLead.Organization_Code__c)){
                            contactExtIdString= eachLead.Organization_Code__c;
                        }
                        if(String.isNotBlank(eachLead.Organization_Code__c)){
                            contactExtIdString = contactExtIdString+eachLead.LastName;
                        }
                        if(String.isNotBlank(eachLead.Organization_Code__c)){
                            contactExtIdString = contactExtIdString+eachLead.FirstName;
                        }
                        if(String.isNotBlank(eachLead.Organization_Code__c)){
                            contactExtIdString = contactExtIdString+eachLead.Email;
                        }  
                        
                        if(String.isNotBlank(contactExtIdString)){
                            contactExtIdAndLeadObjectMap.put(contactExtIdString,eachLead);
                        }
                    }
                }
                
                if(leadIdList.size()>0){
                        
                        List<Contact> contactList = [SELECT ID,Contact_ExtId__c,FirstName,MiddleName,LastName,Title,Email,Phone,MobilePhone FROM CONTACT Where Contact_ExtId__c IN :contactExtIdAndLeadObjectMap.keySet() AND Is_Primary__c = false];
                        Map<String,Contact> contactMap = new Map<String,Contact>();
                        for(Contact eachContact : contactList){
                            contactMap.put(eachContact.Contact_ExtId__c, eachContact);
                        }                        
                        
                        List<Contact> upsertContactList = new List<Contact>();
                        for(String eachContExtId : contactExtIdAndLeadObjectMap.keySet()){
                            
                            Lead eachLead = contactExtIdAndLeadObjectMap.get(eachContExtId);    
                            
                            if(eachLead!=null){
                                Contact contactObj = null; 
                                if(contactMap!=null && contactMap.containsKey(eachContExtId)){
                                    contactObj = contactMap.get(eachContExtId);
                                }else{
                                    contactObj = new Contact();
                                    contactObj.AccountId=orgCodeAndAccIdMap.get(eachLead.Organization_Code__c);
                                    contactObj.Contact_ExtId__c=eachContExtId;
                                }
                                contactObj.FirstName=eachLead.FirstName;
                                contactObj.MiddleName=eachLead.MiddleName;
                                contactObj.LastName=eachLead.LastName;
                                contactObj.Title=eachLead.Title;
                                contactObj.Email=eachLead.Email;
                                contactObj.Phone=eachLead.Phone;
                                contactObj.Phone=eachLead.Phone;
                                contactObj.MobilePhone=eachLead.MobilePhone; 
                                upsertContactList.add(contactObj);
                            }
                        }
                        
                        if(upsertContactList.size()>0){
                            upsert upsertContactList;
                        }                                                
                       update leadList;
                    }
            }
        }catch(Exception reparentOpportunityAndActivityException){
            //-- Perform Application logging
            String processLog = 'Account Trigger Failed to initiate LeadConvert Batch for Processing Records: \r\n';
            processLog += 'REASON :: '+reparentOpportunityAndActivityException.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'AccountTriggerHelper', 'reparentOpportunityAndActivity',
                                                                 null, 'AccountTrigger', processLog, null,'Error Log',  startTime, logLevelCMD, reparentOpportunityAndActivityException));
        }finally{      
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }        
    }
    
}