public class ActualSalesTriggerConstants {
    
    public static final String ACCOUNTY_TYPE_CORPORATE_DIRECT = 'Corporate - Direct';
    
    public static final String ACCOUNTY_TYPE_CORPORATE_TMC = 'Corporate - TMC';
    
    public static final String OTA_CHANNEL_MIX = 'OTA';

}