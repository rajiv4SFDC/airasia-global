/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
14/09/2017      Sharan Desai   		Created
*******************************************************************/
public with sharing class ActualSalesTriggerHandler {

    
    //-- class level variables
    public static Map<String, String> ChannelMixMdtMap = new Map<String, String>();
    private static String SEPARATOR ='#@#';
    
    
    static{
        
        //-- Query Channel Mix Custom Metadata to build collection map with key as 'SALESCHANNEL__c' and value as 'Booking_Channel__c:Sub_Channel__c'
        for (Channel_Mix__mdt eachChannelMix: [SELECT SALESCHANNEL__c, Booking_Channel__c, Sub_Channel__c FROM Channel_Mix__mdt ORDER BY SALESCHANNEL__c]){
            if(String.isNotEmpty(eachChannelMix.SALESCHANNEL__c)){
                
                String bookingChannel=eachChannelMix.Booking_Channel__c!=null?eachChannelMix.Booking_Channel__c:'';
                String subChannel=eachChannelMix.Sub_Channel__c!=null?eachChannelMix.Sub_Channel__c:'';                
                ChannelMixMdtMap.put(eachChannelMix.SALESCHANNEL__c,bookingChannel+ SEPARATOR+subChannel);
            }
        }         
    }
    
    
    /**
    * This method performs all the required operation on BeforeInsert context
    * 
    **/

    public static void isBeforeInsert(Actual_Sales__c[] actualSales){
        
        //-- STEP 1:- call method to update Booking Channel and Sub Channel based on SalesChannel
        ActualSalesTriggerHelper.setChannelMix(actualSales,ChannelMixMdtMap);        
                
        //-- STEP 2:- call method to associate ActualSales to the matching ChannelTarget
        ActualSalesTriggerHelper.setAccountTarget(actualSales);
        
        //-- STEP 3:- call method to associate ActualSales to the matching ChannelTarget
        ActualSalesTriggerHelper.setChannelTarget(actualSales);
        
        //-- STEP 4:- call method to associate ActualSales to the matching POSTarget
        ActualSalesTriggerHelper.setPOSTarget(actualSales);
        
        //-- STEP 5:- call method to associate ActualSales to the matching CorporateTarget
        ActualSalesTriggerHelper.setCorporateTarget(actualSales);
        
    }
    
    
     /**
     *  This method performs all the required operation on BeforeUpdate context
     * 
     **/

    public static void isBeforeUpdate(Actual_Sales__c[] actualSales){
        
        //-- STEP 1:- call method to update Booking Channel and Sub Channel based on SalesChannel
        ActualSalesTriggerHelper.setChannelMix(actualSales,ChannelMixMdtMap);
        
        //-- STEP 2:- call method to associate ActualSales to the matching ChannelTarget
        ActualSalesTriggerHelper.setAccountTarget(actualSales);
        
        //-- STEP 3:- call method to associate ActualSales to the matching ChannelTarget
        ActualSalesTriggerHelper.setChannelTarget(actualSales);
        
        //-- STEP 4:- call method to associate ActualSales to the matching POSTarget
        ActualSalesTriggerHelper.setPOSTarget(actualSales);
        
        //-- STEP 5:- call method to associate ActualSales to the matching CorporateTarget
        ActualSalesTriggerHelper.setCorporateTarget(actualSales);
        
    }
    
 
}