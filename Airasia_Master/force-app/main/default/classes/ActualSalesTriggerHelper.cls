/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
14/09/2017      Sharan Desai   		Created
*******************************************************************/
public with sharing class ActualSalesTriggerHelper {
    
    
    private static String SEPARATOR ='#@#';
    static Set<String> globalAccountIdList = new Set<String>();
    static Map<String,String> globalAccountIdAndOrgCodeMap = new  Map<String,String>();
    static Map<String,String> globalAccountOrgCodeAndTypeMap = new  Map<String,String>();
    
    /**
    * This method updates Booking Channel and Sub Channel based on SalesChannel
    * 
    **/
    
    public static void setChannelMix(Actual_Sales__c[] actualSales,Map<String, String> ChannelMixMdtMap){
        
        if(ChannelMixMdtMap.size()>0){
            for(Actual_Sales__c eachActualSalesRecord : actualSales){
                
                if(String.isNotEmpty(eachActualSalesRecord.SalesChannel__c) && ChannelMixMdtMap.containsKey(eachActualSalesRecord.SalesChannel__c) ){
                    
                    String channelPart = ChannelMixMdtMap.get(eachActualSalesRecord.SalesChannel__c);
                    string[] channelPartArray = channelPart.split(SEPARATOR);           
                    
                    if(channelPartArray!=null && channelPartArray.size()>0 && String.isNotBlank(channelPartArray[0]) && channelPartArray[0]!='null'){
                        eachActualSalesRecord.Channel_Mix__c= channelPartArray[0];
                    }
                    
                    if(channelPartArray!=null && channelPartArray.size()>1 && String.isNotBlank(channelPartArray[1]) && channelPartArray[1]!='null'){
                        eachActualSalesRecord.Sub_Channel__c= channelPartArray[1];
                    }
                    
                }            
            }
        }
    }
    
    /**
    * This method associates ActualSales to the matching AccountTarget
    * 
    **/    
    public static void setAccountTarget(Actual_Sales__c[] actualSales){
        
        //-- method level variables
        Set<String> orgCodeList = new Set<String>();
        Set<String> departureYearList = new Set<String>();
        Set<String> monthList = new Set<String>();
        Set<String> bookingYearList = new Set<String>();
        Set<String> bookingMonthList = new Set<String>();
        Map<String,String> accountTargetMap = new  Map<String,String>();
        Map<String,String> accountTargetBDMap = new  Map<String,String>();
        
        
        //-- build the criteria list to query matching Target Channels
        for(Actual_Sales__c eachActualSalesRecord : actualSales){            
            
            if(String.isNotBlank(eachActualSalesRecord.BK_CreatedOrganizationCode__c))
                orgCodeList.add(eachActualSalesRecord.BK_CreatedOrganizationCode__c);
            
            if(eachActualSalesRecord.IL_DepartureDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month())))
                monthList.add(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month()));
            
            if(eachActualSalesRecord.IL_DepartureDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year())))
                departureYearList.add(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year()));
            
            if(eachActualSalesRecord.BK_BookingDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month())))
                bookingMonthList.add(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month()));
            
            if(eachActualSalesRecord.BK_BookingDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year())))
                bookingYearList.add(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year()));
            
        }    
        
        if(orgCodeList.size()>0 && bookingMonthList.size()>0 && bookingYearList.size()>0){
            fetchAssociatedAccount(orgCodeList);
            
            if(globalAccountIdList.size()>0){
                
                //-- build the collection Map to hold AccountChannel records
                for(Account_Target__c eachAccountTargetForBD : [SELECT ID,MONTH__C,Year__c,Account__c From Account_Target__c WHERE MONTH__C IN :bookingMonthList AND Year__c IN:bookingYearList AND Account__c IN:globalAccountIdList]){
                    
                    if(String.isNotBlank(globalAccountIdAndOrgCodeMap.get(eachAccountTargetForBD.Account__c)) && String.isNotBlank(eachAccountTargetForBD.Year__c)){
                        accountTargetBDMap.put(globalAccountIdAndOrgCodeMap.get(eachAccountTargetForBD.Account__c) +SEPARATOR+
                                               eachAccountTargetForBD.MONTH__C+SEPARATOR+
                                               eachAccountTargetForBD.Year__c, eachAccountTargetForBD.Id);
                    }
                }
            }            
        }
        
        if(orgCodeList.size()>0 && monthList.size()>0 && departureYearList.size()>0){
            
            if(globalAccountIdList.size()<=0){
                fetchAssociatedAccount(orgCodeList);
            }
            
            if(globalAccountIdList.size()>0){                
                
                //-- build the collection Map to hold AccountChannel records
                for(Account_Target__c eachAccountTarget : [SELECT ID,MONTH__C,Year__c,Account__c From Account_Target__c WHERE MONTH__C IN :monthList AND Year__c IN:departureYearList AND Account__c IN:globalAccountIdList]){
                    
                    if(String.isNotBlank(globalAccountIdAndOrgCodeMap.get(eachAccountTarget.Account__c)) && String.isNotBlank(eachAccountTarget.Year__c)){
                        accountTargetMap.put(globalAccountIdAndOrgCodeMap.get(eachAccountTarget.Account__c) +SEPARATOR+
                                             eachAccountTarget.MONTH__C+SEPARATOR+
                                             eachAccountTarget.Year__c, eachAccountTarget.Id);
                    }
                }
            }
        }
        
        if(accountTargetMap.size()>0 || accountTargetBDMap.size()>0 ){
            //-- find the matching AccountChannel and associate it to Actual Sales record.
            for(Actual_Sales__c eachActualSalesRecord : actualSales){
                
                //-- build matching criteria string
                String matchingCriteria=eachActualSalesRecord.BK_CreatedOrganizationCode__c+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month()))+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year()));
                
               //-- build matching criteria string
                String matchingCriteriaForBD=eachActualSalesRecord.BK_CreatedOrganizationCode__c+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month()))+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year()));
                
                
                if(eachActualSalesRecord.Channel_Mix__c==ActualSalesTriggerConstants.OTA_CHANNEL_MIX){
                    eachActualSalesRecord.Sub_Channel__c=globalAccountOrgCodeAndTypeMap.get(eachActualSalesRecord.BK_CreatedOrganizationCode__c)!=null?globalAccountOrgCodeAndTypeMap.get(eachActualSalesRecord.BK_CreatedOrganizationCode__c):'';
                }
                
                //-- Target Channel Association
                if(accountTargetMap.containsKey(matchingCriteria)){
                    eachActualSalesRecord.Account_Target__c=accountTargetMap.get(matchingCriteria);
                }  
                
                //-- Target Channel BD Association
                if(accountTargetBDMap.containsKey(matchingCriteriaForBD)){
                    eachActualSalesRecord.Account_Target_BD__c=accountTargetBDMap.get(matchingCriteriaForBD);
                }  
            }
            
        }
        
    }

    /**
    * This method associates ActualSales to the matching ChannelTarget
    * 
    **/    
    public static void setChannelTarget(Actual_Sales__c[] actualSales){
        
        //-- method level variables
        Set<String> salesChannelList = new Set<String>();
        Set<String> departureMonthList = new Set<String>();
        Set<String> departureYearList = new Set<String>();
        Set<String> bookingYearList = new Set<String>();
        Set<String> bookingMonthList = new Set<String>();
        Set<String> carrierCodeList = new Set<String>();
        Map<String,String> channelTargetMap = new  Map<String,String>();
        Map<String,String> channelTargetMapForBD = new  Map<String,String>();
        
        //-- build the criteria list to query matching Target Channels
        for(Actual_Sales__c eachActualSalesRecord : actualSales){            
            
            if(String.isNotBlank(eachActualSalesRecord.SalesChannel__c))
                salesChannelList.add(eachActualSalesRecord.SalesChannel__c);
            
            if(eachActualSalesRecord.IL_DepartureDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month())))
                departureMonthList.add(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month()));
            
            if(eachActualSalesRecord.IL_DepartureDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year())))
                departureYearList.add(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year()));
            
             if(eachActualSalesRecord.BK_BookingDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month())))
                bookingMonthList.add(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month()));
            
            if(eachActualSalesRecord.BK_BookingDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year())))
                bookingYearList.add(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year()));
            
            if(String.isNotBlank(eachActualSalesRecord.IL_CarrierCode__c))
                carrierCodeList.add(eachActualSalesRecord.IL_CarrierCode__c);            
        }
        
        if(salesChannelList.size()>0 && bookingMonthList.size()>0 && bookingYearList.size()>0
           && carrierCodeList.size()>0){
               
               //-- build the collection Map to hold target Channel records FOR BD
               for(Channel_Target__c eachChannelTarget : [SELECT ID, Year__c,Month__c,AOC__c,booking_Channel__c From Channel_Target__c WHERE Year__c IN:bookingYearList AND Month__c IN:bookingMonthList AND AOC__c IN:carrierCodeList AND booking_Channel__c IN:salesChannelList ]){
                   
                   if(String.isNotBlank(eachChannelTarget.booking_Channel__c) && String.isNotBlank(eachChannelTarget.Month__c) &&
                      String.isNotBlank(eachChannelTarget.Year__c) && String.isNotBlank(eachChannelTarget.AOC__c)){
                          channelTargetMapForBD.put(eachChannelTarget.booking_Channel__c+SEPARATOR+
                                                    eachChannelTarget.Month__c+SEPARATOR+
                                                    eachChannelTarget.Year__c+SEPARATOR+
                                                    eachChannelTarget.AOC__c, eachChannelTarget.Id);
                      }
               }
           }
        
        if(salesChannelList.size()>0 && departureMonthList.size()>0 && departureYearList.size()>0
           && carrierCodeList.size()>0){
               
               //-- build the collection Map to hold target Channel records
               for(Channel_Target__c eachChannelTarget : [SELECT ID, Year__c,Month__c,AOC__c,booking_Channel__c From Channel_Target__c WHERE Year__c IN:departureYearList AND Month__c IN:departureMonthList AND AOC__c IN:carrierCodeList AND booking_Channel__c IN:salesChannelList ]){
                   
                   if(String.isNotBlank(eachChannelTarget.booking_Channel__c) && String.isNotBlank(eachChannelTarget.Month__c) &&
                      String.isNotBlank(eachChannelTarget.Year__c) && String.isNotBlank(eachChannelTarget.AOC__c)){
                          channelTargetMap.put(eachChannelTarget.booking_Channel__c+SEPARATOR+
                                               eachChannelTarget.Month__c+SEPARATOR+
                                               eachChannelTarget.Year__c+SEPARATOR+
                                               eachChannelTarget.AOC__c, eachChannelTarget.Id);
                      }
               }
           }
        
        if(channelTargetMap.size()>0 || channelTargetMapForBD.size()>0){
            //-- find the matching Target Channel and associate it to Actual Sales record.
            for(Actual_Sales__c eachActualSalesRecord : actualSales){
                
                //-- build matching criteria string for BD
                String matchingCriteriaForBD=eachActualSalesRecord.SalesChannel__c+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month()))+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year()))+SEPARATOR+
                    eachActualSalesRecord.IL_CarrierCode__c;    
                
                
                //-- build matching criteria string
                String matchingCriteria=eachActualSalesRecord.SalesChannel__c+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month()))+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year()))+SEPARATOR+
                    eachActualSalesRecord.IL_CarrierCode__c;  
                
                //-- Target Channel Association FOr BD
                if(channelTargetMapForBD.containsKey(matchingCriteriaForBD)){
                    eachActualSalesRecord.Channel_Target_BD__c=channelTargetMapForBD.get(matchingCriteriaForBD);
                }    
                
                //-- Target Channel Association
                if(channelTargetMap.containsKey(matchingCriteria)){
                    eachActualSalesRecord.Channel_Target__c=channelTargetMap.get(matchingCriteria);
                }            
            }    
        }               
           
    }
    
    /**
    * This method associates ActualSales to the matching POSTarget
    * 
    **/
    public static void setPOSTarget(Actual_Sales__c[] actualSales){
        
        Set<String> departureYearList = new Set<String>();
        Set<String> departureMonthList = new Set<String>();
        Set<String> bookingYearList = new Set<String>();
        Set<String> bookingMonthList = new Set<String>();
        Set<String> carrierCodeList = new Set<String>();
        Set<String> departureCountryList = new Set<String>();
        Map<String,String> posTargetMap = new  Map<String,String>();
        Map<String,String> posTargetMapForBD = new  Map<String,String>();
        
        //-- build the criteria list to query matching Target Channels
        for(Actual_Sales__c eachActualSalesRecord : actualSales){            
            
            if(String.isNotBlank(eachActualSalesRecord.DepartureCountry__c))
                departureCountryList.add(eachActualSalesRecord.DepartureCountry__c); 
            
            if(eachActualSalesRecord.IL_DepartureDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month())))
                departureMonthList.add(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month()));
            
            if(eachActualSalesRecord.IL_DepartureDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year())))
                departureYearList.add(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year()));
            
            if(eachActualSalesRecord.BK_BookingDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month())))
                bookingMonthList.add(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month()));
            
            if(eachActualSalesRecord.BK_BookingDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year())))
                bookingYearList.add(String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year()));
            
            if(String.isNotBlank(eachActualSalesRecord.IL_CarrierCode__c))
                carrierCodeList.add(eachActualSalesRecord.IL_CarrierCode__c);
        }
        
        if(departureCountryList.size()>0 && bookingMonthList.size()>0 && bookingYearList.size()>0 && carrierCodeList.size()>0){
            //-- build the collection Map to hold POS Channel records For BD
            for(POS_Target__c eachPOSTarget : [SELECT ID,POS__c,Month__c,Year__c,AOC__c From POS_Target__c WHERE POS__c IN: departureCountryList OR Month__c IN :bookingMonthList OR Year__c IN: bookingYearList OR AOC__c IN:carrierCodeList]){
                
                if(String.isNotBlank(eachPOSTarget.POS__c) && String.isNotBlank(eachPOSTarget.Year__c) 
                   && String.isNotBlank(eachPOSTarget.AOC__c)){
                       
                       posTargetMapForBD.put(eachPOSTarget.POS__c+SEPARATOR+
                                             eachPOSTarget.Month__c+SEPARATOR+
                                             eachPOSTarget.Year__c+SEPARATOR+
                                             eachPOSTarget.AOC__c, eachPOSTarget.Id);
                       
                   }
            }
        }
        
        if(departureCountryList.size()>0 && departureMonthList.size()>0 && departureYearList.size()>0 && carrierCodeList.size()>0){
            //-- build the collection Map to hold POS Channel records
            for(POS_Target__c eachPOSTarget : [SELECT ID,POS__c,Month__c,Year__c,AOC__c From POS_Target__c WHERE POS__c IN: departureCountryList OR Month__c IN :departureMonthList OR Year__c IN: departureYearList OR AOC__c IN:carrierCodeList]){
                
                if(String.isNotBlank(eachPOSTarget.POS__c) && String.isNotBlank(eachPOSTarget.Year__c) 
                   && String.isNotBlank(eachPOSTarget.AOC__c)){
                       
                       posTargetMap.put(eachPOSTarget.POS__c+SEPARATOR+
                                        eachPOSTarget.Month__c+SEPARATOR+
                                        eachPOSTarget.Year__c+SEPARATOR+
                                        eachPOSTarget.AOC__c, eachPOSTarget.Id);
                       
                   }
            }            
        } 
            
        if(posTargetMap.size()>0 || posTargetMapForBD.size()>0){
            
            //-- find the matching POS Channel and associate it to Actual Sales record.
            for(Actual_Sales__c eachActualSalesRecord : actualSales){
                
                //-- build matching criteria string
                String matchingCriteriaForBD=eachActualSalesRecord.departurecountry__c+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.BK_BookingDate__c.month()))+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.BK_BookingDate__c.year()))+SEPARATOR+
                    eachActualSalesRecord.IL_CarrierCode__c; 
                
                //-- build matching criteria string
                String matchingCriteria=eachActualSalesRecord.departurecountry__c+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month()))+SEPARATOR+
                    (String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year()))+SEPARATOR+
                    eachActualSalesRecord.IL_CarrierCode__c; 
                
                //-- POS Channel Association
                if(posTargetMapForBD.containsKey(matchingCriteriaForBD)){
                    eachActualSalesRecord.POS_Target_BD__c=posTargetMapForBD.get(matchingCriteriaForBD);                        
                }   
                
                //-- POS Channel Association
                if(posTargetMap.containsKey(matchingCriteria)){
                    eachActualSalesRecord.POS_Target__c=posTargetMap.get(matchingCriteria);                        
                }            
            }    
        }            
               
    }
    
    /**
    * This method associates ActualSales to the matching CorporateTarget
    * 
    **/
    public static void setCorporateTarget(Actual_Sales__c[] actualSales){
                
        //-- method level variables
        Set<String> orgCodeList = new Set<String>();
        Set<String> departureMonthList = new Set<String>();
        Set<String> departureYearList = new Set<String>();
        Map<String,String> corporateTargetMap = new  Map<String,String>();
        
        //-- build the criteria list to query matching Corporate Channels
        for(Actual_Sales__c eachActualSalesRecord : actualSales){            
            
            if(String.isNotBlank(eachActualSalesRecord.BK_CreatedOrganizationCode__c))
                orgCodeList.add(eachActualSalesRecord.BK_CreatedOrganizationCode__c);
            
            if(eachActualSalesRecord.IL_DepartureDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year())))
                departureYearList.add(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year()));
            
            if(eachActualSalesRecord.IL_DepartureDate__c!=null && String.isNotBlank(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month())))
                departureMonthList.add(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month()));
            
        }
        
        if(orgCodeList.size()>0 && departureMonthList.size()>0 && departureYearList.size()>0){
            
            //-- build the collection Map to hold Corporate Channel records
            for(Corporate_Target__c eachCorporateTarget : [SELECT ID,month__c,Year__c From Corporate_Target__c WHERE Month__c IN :departureMonthList OR Year__c IN :departureYearList]){
                
                if(String.isNotBlank(eachCorporateTarget.Month__c) && String.isNotBlank(eachCorporateTarget.Year__c) ){                    
                    corporateTargetMap.put(eachCorporateTarget.Month__c+SEPARATOR+
                                           eachCorporateTarget.Year__c, eachCorporateTarget.Id);                    
                }
            }
            
            System.debug('corporateTargetMap'+corporateTargetMap);
           
            if(corporateTargetMap.size()>0){
                
                fetchAssociatedAccount(orgCodeList);
                
                //-- find the matching Corporate Channel and associate it to Actual Sales record.
                for(Actual_Sales__c eachActualSalesRecord : actualSales){
                    
                    String accountType = globalAccountOrgCodeAndTypeMap.get(eachActualSalesRecord.BK_CreatedOrganizationCode__c);
                    
                    if(String.isNotBlank(accountType) && (accountType.equalsIgnoreCase(ActualSalesTriggerConstants.ACCOUNTY_TYPE_CORPORATE_DIRECT)
                       || accountType.equalsIgnoreCase(ActualSalesTriggerConstants.ACCOUNTY_TYPE_CORPORATE_TMC))){
                           
                          
                           //-- build matching criteria string
                           String matchingCriteria=(String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.month()))+SEPARATOR+
                               (String.valueOf(eachActualSalesRecord.IL_DepartureDate__c.year()));            
                           
                           
                           //-- Corporate Channel Association
                           if(corporateTargetMap.containsKey(matchingCriteria)){
                               eachActualSalesRecord.Corporate_Target__c=corporateTargetMap.get(matchingCriteria);
                           }                           
                       }                    
                }    
            }            
        }        
    }
    
    
    /**
    * This method associates ActualSales to the matching CorporateTarget
    * 
    **/
    public static void fetchAssociatedAccount(Set<String> orgCodeList){
        
        if(globalAccountIdList.size()<=0 && orgCodeList.size()>0){
            //-- Query Account to get AccountIds from OrganizationCode
            for(Account eachAccount :  [SELECT ID,Organization_Code__c,type FROM ACCOUNT WHERE Organization_Code__c IN :orgCodeList AND isPersonAccount=false]){
                globalAccountIdList.add(eachAccount.Id);
                globalAccountIdAndOrgCodeMap.put(eachAccount.ID, eachAccount.Organization_Code__c);
                globalAccountOrgCodeAndTypeMap.put(eachAccount.Organization_Code__c, eachAccount.type);
            }
        }    
    }
    
    
}