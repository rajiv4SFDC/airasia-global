/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
14/09/2017      Sharan Desai   		Created
*******************************************************************/

@IsTest(SeeAllData=false)
public class ActualSalesTrigger_Test {

    private static testMethod void testsetChannelsScenarioForCorporateDirectAccountType() {
                
        Channel_Mix__mdt channelMixObj = [SELECT SALESCHANNEL__c, Booking_Channel__c, Sub_Channel__c FROM Channel_Mix__mdt ORDER BY SALESCHANNEL__c LIMIT 1 ];
        		 
        Map<String,Object> channelTargetFieldValueMap = new Map<String,Object>();
        channelTargetFieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        channelTargetFieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        channelTargetFieldValueMap.put('Booking_Channel__c',channelMixObj.SALESCHANNEL__c);
        channelTargetFieldValueMap.put('AOC__c','AK');
        channelTargetFieldValueMap.put('Base_Fare_Target__c',1);
        TestUtility.createChannelTarget(channelTargetFieldValueMap);       
                
        Map<String,Object> accountFieldValueMap = new Map<String,Object>();
        accountFieldValueMap.put('Name','Test Account');
        accountFieldValueMap.put('Organization_Code__c','12345');
        accountFieldValueMap.put('Type',ActualSalesTriggerConstants.ACCOUNTY_TYPE_CORPORATE_DIRECT);
        TestUtility.createAccount(accountFieldValueMap);       
                
        Map<String,Object> accountTargetfieldValueMap = new Map<String,Object>();
        accountTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        accountTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        accountTargetfieldValueMap.put('Account__c',TestUtility.getAccount().Id);
        accountTargetfieldValueMap.put('Base_Target_Fare__c',1);
        TestUtility.createAccountTarget(accountTargetfieldValueMap);
        
         Map<String,Object> posTargetfieldValueMap = new Map<String,Object>();
        posTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        posTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        posTargetfieldValueMap.put('POS__c','MY');
        posTargetfieldValueMap.put('AOC__c','AK');
        posTargetfieldValueMap.put('Base_Fare_Target__c',1);
        TestUtility.createPOSTarget(posTargetfieldValueMap);
        
        Map<String,Object> corporateTargetfieldValueMap = new Map<String,Object>();
        corporateTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        corporateTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        corporateTargetfieldValueMap.put('Fare_Target__c',1);
        TestUtility.createCorporateTarget(corporateTargetfieldValueMap);
      
        Map<String,Object> actualSalesFieldValueMap = new Map<String,Object>();
 		actualSalesFieldValueMap.put('Account__c',TestUtility.getAccount().Id);
        actualSalesFieldValueMap.put('DEPARTURECOUNTRY__c','MY');
        actualSalesFieldValueMap.put('SalesChannel__c',channelMixObj.SALESCHANNEL__c);
        actualSalesFieldValueMap.put('BK_BookingID__c','1');
        actualSalesFieldValueMap.put('BK_RecordLocator__c','1');
        actualSalesFieldValueMap.put('BK_CurrencyCode__c','1');
        actualSalesFieldValueMap.put('BK_CreatedOrganizationCode__c','12345');
        actualSalesFieldValueMap.put('BK_BookingDate__c', Date.today());
        actualSalesFieldValueMap.put('SeatSold__c',1);
        actualSalesFieldValueMap.put('PaxFlown__c',1);
        actualSalesFieldValueMap.put('OrganizationName__c','1');
        actualSalesFieldValueMap.put('Org_CountryCode__c','1');
        actualSalesFieldValueMap.put('IL_DepartureDate__c',Date.today());
        actualSalesFieldValueMap.put('IL_CarrierCode__c','AK');
        actualSalesFieldValueMap.put('IL_DepartureStation__c','1');
        actualSalesFieldValueMap.put('IL_ArrivalStation__c','1');
        actualSalesFieldValueMap.put('ARRIVALCOUNTRY__c','1');
        actualSalesFieldValueMap.put('PJS_FareJourneyType__c','1');
        actualSalesFieldValueMap.put('PJS_ClassOfService__c','1');
        actualSalesFieldValueMap.put('InternationalDesc__c','1');
        actualSalesFieldValueMap.put('BASE_FARE__c',1);
        actualSalesFieldValueMap.put('FUEL_AMOUNT__c',1);
        actualSalesFieldValueMap.put('CONNECTION_AMOUNT__c',1);
        actualSalesFieldValueMap.put('THRU_AMOUNT__c',1);
        actualSalesFieldValueMap.put('OTHER_FARE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('KLIA2_AMOUNT__c',1);
        actualSalesFieldValueMap.put('BAGGAGE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('TAX_AMOUNT__c',1);
        actualSalesFieldValueMap.put('SEAT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('FNB_AMOUNT__c',1);
        actualSalesFieldValueMap.put('FEE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('SPORTS_EQUIPMENT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_BASE_FARE__c',1);
        actualSalesFieldValueMap.put('MYR_FUEL_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_CONNECTION_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_THRU_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_OTHER_FARE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_KLIA2_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_BAGGAGE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_TAX_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_SEAT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_FNB_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_FEE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_SPORTS_EQUIPMENT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_Total_Cost__c',1);
        actualSalesFieldValueMap.put('PRIMARYPAYMENT__c','1');
        actualSalesFieldValueMap.put('PaymentMethodCode__c','1');
        
        Test.startTest();
        
            TestUtility.createActualSales(actualSalesFieldValueMap);        
            update TestUtility.getActualSales();
                  
            LIST<String> actualSalesQueryFieldList = new List<String>();
            actualSalesQueryFieldList.add('Account_Target__c');
            actualSalesQueryFieldList.add('Channel_Target__c');
            actualSalesQueryFieldList.add('POS_Target__c');
            actualSalesQueryFieldList.add('COrporate_Target__c');
            
            Actual_Sales__c actualSalesObj = (Actual_Sales__c)TestUtility.getActualSales(actualSalesQueryFieldList);
            System.assertEquals(TestUtility.getAccountTarget().Id, actualSalesObj.Account_Target__c);
            System.assertEquals(TestUtility.getChannelTarget().Id,actualSalesObj.Channel_Target__c);
            System.assertEquals(TestUtility.getPOSTarget().Id, actualSalesObj.POS_Target__c);
            System.assertEquals(TestUtility.getCorporateTarget().Id,actualSalesObj.COrporate_Target__c);
        
        Test.stopTest();
    }
    
    private static testMethod void testsetChannelsScenarioForCorporateTMCAccountType() {
                
        Channel_Mix__mdt channelMixObj = [SELECT SALESCHANNEL__c, Booking_Channel__c, Sub_Channel__c FROM Channel_Mix__mdt ORDER BY SALESCHANNEL__c LIMIT 1 ];
        		 
        Map<String,Object> channelTargetFieldValueMap = new Map<String,Object>();
        channelTargetFieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        channelTargetFieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        channelTargetFieldValueMap.put('Booking_Channel__c',channelMixObj.Booking_Channel__c);
        channelTargetFieldValueMap.put('AOC__c','AK');
        channelTargetFieldValueMap.put('Base_Fare_Target__c',1);
        TestUtility.createChannelTarget(channelTargetFieldValueMap);       
                
        Map<String,Object> accountFieldValueMap = new Map<String,Object>();
        accountFieldValueMap.put('Name','Test Account');
        accountFieldValueMap.put('Organization_Code__c','12345');
        accountFieldValueMap.put('Type',ActualSalesTriggerConstants.ACCOUNTY_TYPE_CORPORATE_TMC);
        TestUtility.createAccount(accountFieldValueMap);       
                
        Map<String,Object> accountTargetfieldValueMap = new Map<String,Object>();
        accountTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        accountTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        accountTargetfieldValueMap.put('Account__c',TestUtility.getAccount().Id);
        accountTargetfieldValueMap.put('Base_Target_Fare__c',1);
        TestUtility.createAccountTarget(accountTargetfieldValueMap);
        
         Map<String,Object> posTargetfieldValueMap = new Map<String,Object>();
        posTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        posTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        posTargetfieldValueMap.put('POS__c','MY');
        posTargetfieldValueMap.put('AOC__c','AK');
        posTargetfieldValueMap.put('Base_Fare_Target__c',1);
        TestUtility.createPOSTarget(posTargetfieldValueMap);
        
        Map<String,Object> corporateTargetfieldValueMap = new Map<String,Object>();
        corporateTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        corporateTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        corporateTargetfieldValueMap.put('Fare_Target__c',1);
        TestUtility.createCorporateTarget(corporateTargetfieldValueMap);
      
        Map<String,Object> actualSalesFieldValueMap = new Map<String,Object>();
 		actualSalesFieldValueMap.put('Account__c',TestUtility.getAccount().Id);
        actualSalesFieldValueMap.put('DEPARTURECOUNTRY__c','MY');
        actualSalesFieldValueMap.put('SalesChannel__c',channelMixObj.SALESCHANNEL__c);
        actualSalesFieldValueMap.put('BK_BookingID__c','1');
        actualSalesFieldValueMap.put('BK_RecordLocator__c','1');
        actualSalesFieldValueMap.put('BK_CurrencyCode__c','1');
        actualSalesFieldValueMap.put('BK_CreatedOrganizationCode__c','12345');
        actualSalesFieldValueMap.put('BK_BookingDate__c', Date.today());
        actualSalesFieldValueMap.put('SeatSold__c',1);
        actualSalesFieldValueMap.put('PaxFlown__c',1);
        actualSalesFieldValueMap.put('OrganizationName__c','1');
        actualSalesFieldValueMap.put('Org_CountryCode__c','1');
        actualSalesFieldValueMap.put('IL_DepartureDate__c',Date.today());
        actualSalesFieldValueMap.put('IL_CarrierCode__c','AK');
        actualSalesFieldValueMap.put('IL_DepartureStation__c','1');
        actualSalesFieldValueMap.put('IL_ArrivalStation__c','1');
        actualSalesFieldValueMap.put('ARRIVALCOUNTRY__c','1');
        actualSalesFieldValueMap.put('PJS_FareJourneyType__c','1');
        actualSalesFieldValueMap.put('PJS_ClassOfService__c','1');
        actualSalesFieldValueMap.put('InternationalDesc__c','1');
        actualSalesFieldValueMap.put('BASE_FARE__c',1);
        actualSalesFieldValueMap.put('FUEL_AMOUNT__c',1);
        actualSalesFieldValueMap.put('CONNECTION_AMOUNT__c',1);
        actualSalesFieldValueMap.put('THRU_AMOUNT__c',1);
        actualSalesFieldValueMap.put('OTHER_FARE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('KLIA2_AMOUNT__c',1);
        actualSalesFieldValueMap.put('BAGGAGE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('TAX_AMOUNT__c',1);
        actualSalesFieldValueMap.put('SEAT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('FNB_AMOUNT__c',1);
        actualSalesFieldValueMap.put('FEE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('SPORTS_EQUIPMENT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_BASE_FARE__c',1);
        actualSalesFieldValueMap.put('MYR_FUEL_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_CONNECTION_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_THRU_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_OTHER_FARE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_KLIA2_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_BAGGAGE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_TAX_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_SEAT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_FNB_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_FEE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_SPORTS_EQUIPMENT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_Total_Cost__c',1);
        actualSalesFieldValueMap.put('PRIMARYPAYMENT__c','1');
        actualSalesFieldValueMap.put('PaymentMethodCode__c','1');
        
        Test.startTest();
        
            TestUtility.createActualSales(actualSalesFieldValueMap);        
            update TestUtility.getActualSales();
                  
            LIST<String> actualSalesQueryFieldList = new List<String>();
            actualSalesQueryFieldList.add('Account_Target__c');
            actualSalesQueryFieldList.add('Channel_Target__c');
            actualSalesQueryFieldList.add('POS_Target__c');
            actualSalesQueryFieldList.add('COrporate_Target__c');
            
            Actual_Sales__c actualSalesObj = (Actual_Sales__c)TestUtility.getActualSales(actualSalesQueryFieldList);
            System.assertEquals(TestUtility.getAccountTarget().Id, actualSalesObj.Account_Target__c);
            System.assertEquals(TestUtility.getChannelTarget().Id,actualSalesObj.Channel_Target__c);
            System.assertEquals(TestUtility.getPOSTarget().Id, actualSalesObj.POS_Target__c);
            System.assertEquals(TestUtility.getCorporateTarget().Id,actualSalesObj.COrporate_Target__c);
        
        Test.stopTest();
    }
    
     private static testMethod void testsetSubChannelForOTABookingChannel() {
                
        Channel_Mix__mdt channelMixObj = [SELECT SALESCHANNEL__c, Booking_Channel__c, Sub_Channel__c FROM Channel_Mix__mdt WHERE SalesChannel__c =:ActualSalesTriggerConstants.OTA_CHANNEL_MIX LIMIT 1 ];
     
         Map<String,Object> channelTargetFieldValueMap = new Map<String,Object>();
        channelTargetFieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        channelTargetFieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        channelTargetFieldValueMap.put('Booking_Channel__c',channelMixObj.Booking_Channel__c);
        channelTargetFieldValueMap.put('AOC__c','AK');
        channelTargetFieldValueMap.put('Base_Fare_Target__c',1);
        TestUtility.createChannelTarget(channelTargetFieldValueMap);       
                
        Map<String,Object> accountFieldValueMap = new Map<String,Object>();
        accountFieldValueMap.put('Name','Test Account');
        accountFieldValueMap.put('Organization_Code__c','12345');
        accountFieldValueMap.put('Type',ActualSalesTriggerConstants.ACCOUNTY_TYPE_CORPORATE_DIRECT);
        TestUtility.createAccount(accountFieldValueMap);       
                
        Map<String,Object> accountTargetfieldValueMap = new Map<String,Object>();
        accountTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        accountTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        accountTargetfieldValueMap.put('Account__c',TestUtility.getAccount().Id);
        accountTargetfieldValueMap.put('Base_Target_Fare__c',1);
        TestUtility.createAccountTarget(accountTargetfieldValueMap);
        
         Map<String,Object> posTargetfieldValueMap = new Map<String,Object>();
        posTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        posTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        posTargetfieldValueMap.put('POS__c','MY');
        posTargetfieldValueMap.put('AOC__c','AK');
        posTargetfieldValueMap.put('Base_Fare_Target__c',1);
        TestUtility.createPOSTarget(posTargetfieldValueMap);
        
        Map<String,Object> corporateTargetfieldValueMap = new Map<String,Object>();
        corporateTargetfieldValueMap.put('Year__c',String.valueOf(Date.today().year()));
        corporateTargetfieldValueMap.put('Month__c',String.valueOf(Date.today().month()));
        corporateTargetfieldValueMap.put('Fare_Target__c',1);
        TestUtility.createCorporateTarget(corporateTargetfieldValueMap);
      
        Map<String,Object> actualSalesFieldValueMap = new Map<String,Object>();
 		actualSalesFieldValueMap.put('Account__c',TestUtility.getAccount().Id);
        actualSalesFieldValueMap.put('DEPARTURECOUNTRY__c','MY');
        actualSalesFieldValueMap.put('SalesChannel__c',channelMixObj.SALESCHANNEL__c);
        actualSalesFieldValueMap.put('BK_BookingID__c','1');
        actualSalesFieldValueMap.put('BK_RecordLocator__c','1');
        actualSalesFieldValueMap.put('BK_CurrencyCode__c','1');
        actualSalesFieldValueMap.put('BK_CreatedOrganizationCode__c','12345');
        actualSalesFieldValueMap.put('BK_BookingDate__c', Date.today());
        actualSalesFieldValueMap.put('SeatSold__c',1);
        actualSalesFieldValueMap.put('PaxFlown__c',1);
        actualSalesFieldValueMap.put('OrganizationName__c','1');
        actualSalesFieldValueMap.put('Org_CountryCode__c','1');
        actualSalesFieldValueMap.put('IL_DepartureDate__c',Date.today());
        actualSalesFieldValueMap.put('IL_CarrierCode__c','AK');
        actualSalesFieldValueMap.put('IL_DepartureStation__c','1');
        actualSalesFieldValueMap.put('IL_ArrivalStation__c','1');
        actualSalesFieldValueMap.put('ARRIVALCOUNTRY__c','1');
        actualSalesFieldValueMap.put('PJS_FareJourneyType__c','1');
        actualSalesFieldValueMap.put('PJS_ClassOfService__c','1');
        actualSalesFieldValueMap.put('InternationalDesc__c','1');
        actualSalesFieldValueMap.put('BASE_FARE__c',1);
        actualSalesFieldValueMap.put('FUEL_AMOUNT__c',1);
        actualSalesFieldValueMap.put('CONNECTION_AMOUNT__c',1);
        actualSalesFieldValueMap.put('THRU_AMOUNT__c',1);
        actualSalesFieldValueMap.put('OTHER_FARE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('KLIA2_AMOUNT__c',1);
        actualSalesFieldValueMap.put('BAGGAGE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('TAX_AMOUNT__c',1);
        actualSalesFieldValueMap.put('SEAT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('FNB_AMOUNT__c',1);
        actualSalesFieldValueMap.put('FEE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('SPORTS_EQUIPMENT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_BASE_FARE__c',1);
        actualSalesFieldValueMap.put('MYR_FUEL_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_CONNECTION_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_THRU_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_OTHER_FARE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_KLIA2_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_BAGGAGE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_TAX_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_SEAT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_FNB_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_FEE_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_SPORTS_EQUIPMENT_AMOUNT__c',1);
        actualSalesFieldValueMap.put('MYR_Total_Cost__c',1);
        actualSalesFieldValueMap.put('PRIMARYPAYMENT__c','1');
        actualSalesFieldValueMap.put('PaymentMethodCode__c','1');
        
        Test.startTest();
        
            TestUtility.createActualSales(actualSalesFieldValueMap);        
            update TestUtility.getActualSales();
                  
            LIST<String> actualSalesQueryFieldList = new List<String>();
            actualSalesQueryFieldList.add('Account_Target__c');
            actualSalesQueryFieldList.add('Channel_Target__c');
            actualSalesQueryFieldList.add('POS_Target__c');
            actualSalesQueryFieldList.add('COrporate_Target__c');
            
            Actual_Sales__c actualSalesObj = (Actual_Sales__c)TestUtility.getActualSales(actualSalesQueryFieldList);
            System.assertEquals(TestUtility.getAccountTarget().Id, actualSalesObj.Account_Target__c);
            System.assertEquals(TestUtility.getChannelTarget().Id,actualSalesObj.Channel_Target__c);
            System.assertEquals(TestUtility.getPOSTarget().Id, actualSalesObj.POS_Target__c);
            System.assertEquals(TestUtility.getCorporateTarget().Id,actualSalesObj.COrporate_Target__c);
        
        Test.stopTest();
     
     }

}