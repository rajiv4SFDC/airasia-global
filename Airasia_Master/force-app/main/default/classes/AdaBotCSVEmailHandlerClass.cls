/******************************************************************
 * Handle incoming emails from Ada.support
 * 
 * Each email contains a CSV file as an attachment.  We don't need
 * the email body.
 * 
 * Each row in the CSV file represents a single Ada bot (AVA) chat
 * conversation, and here we create a Case for each conversation.
 * 
 * They represent conversations that are closed by the bot and
 * are not handed off to Live Chat. 
 * 
 * Nov 2018 - Maaz Khan, AirAsia
 * Intial creation and updates to support business improvements
 * 
 * Mar 2019 - Charles Thompson, Salesforce Singapore
 * Updates to support case origin values being provided in the file
 *****************************************************************/
Global class AdaBotCSVEmailHandlerClass implements Messaging.InboundEmailHandler{
    
    Global string nameFile{get;set;}
    Global blob contentFile{get;set;}
    String[] filelines = new String[]{};
    Boolean processSheet = True;
    List<Case> CaseUpload;    
    List <Messaging.SingleEmailMessage> Mail = new List <Messaging.SingleEmailMessage>();
    List <Messaging.SingleEmailMessage> Mails = new List <Messaging.SingleEmailMessage>();
    
    // email - handle inbound email
    Global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                           Messaging.Inboundenvelope envelope){
        
        CaseUpload = new List<Case>();
        
        String a1= '';
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        System.debug(' >email< ' + email);
        String fileAttachments = '';
        
        Messaging.InboundEmail.textAttachment[] tAttachments = email.TextAttachments;        
        Messaging.InboundEmail.BinaryAttachment[] bAttachments = email.BinaryAttachments;       
        String csvbody = '';
        for (Integer i = 0; i < tAttachments.size(); i++){
            System.debug(' attacments... ' + tAttachments[i]);
            fileAttachments = String.valueOf(tAttachments[i]);
        }
        List<Case> caseList = new List<Case>();
        
        List<String> badrows = new List<String>();
        String csvBody1 = '';
        String[] lineByCsvBody = new String[]{};
        List<String> linebycsvdata = new List<String>();
        
        // expect to have only one text attachhment and 
        // ignore any binary attachments
        if (tAttachments != Null && tAttachments.size() > 0){
            for (Messaging.InboundEmail.textAttachment ttt : tAttachments){
                csvBody1 = ttt.body;
                // call AdaFileUploadCSVBatchApex class to convert to Cases
                // csvBody1 will likely be thousands of lines
                Id jobId = Database.executeBatch(new AdaFileUploadCSVBatchApex(csvBody1), 20);  
                System.debug('Batch job Id = ' + jobId);
            }
        }
        return result;
    }
}