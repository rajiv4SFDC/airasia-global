/*********************************************************
 * Batch handler for incoming CSV file from Ada Bot
 * 
 * Called by AdaBotCSVEmailHandlerClass
 * 
 * Nov 2019 - Maaz Khan - AirAsia 
 * Initial release
 * 
 * Mar 2019 - Maaz Khan - AirAsia
 * Added changes to support case_origin field coming from Ada
 * 
 * Mar 2018 - Charles Thompson - Salesforce Singapore
 * Enhancements to the case_origin processing
 *
 * May 2019 - Alvin Tayag - Salesforce Singapore
 * Update to accomodate FB Country, and Transcripts
 * 
 * June 2019 - Alvin Tayag  - Salesforce Singapore
 * Update to add handling for multibyte entries from users
 *
 * August 2019 - Alvin Tayag - Salesforce Singapore
 * Update to accomodate Field Agency Id, Subject and Country of Bank
 *
 * November 2019 - Alvin Tayag - Salesforce Singapore
 * Update to add special handling for WeChat AVA
 * 
 * Feb 2019 - Tushar Arora
 * Update to add hotel mapping
 * 
 * June 2020 - Tushar Arora
 * Update to do CSAT update on cases with disposition != 'Resolved by bot'
 * This is to handle whatsApp scenarios 
 * 
 * July 2020 - Encora-MG
 * Created Custom lable for Hotel AA_Case_Business_Lines_For_Hotel [Label name: HotelValue]
 * Updated RBV_R1_Case_Disposition__c = dispositionLevel1. 
 *
 * ******************************************************/ 
public with sharing class AdaFileUploadCSVBatchApex   
       implements Database.Batchable<String>,
                  Database.Stateful,
                  Database.AllowsCallouts{
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    public String recordsToBeProcessed;
    private static Set<String> subCategory1Values;
    private static Set<String> subCategory2Values;
   
    public List<String> badrows = new List<String>();  
    public Integer totalRows;

    // constructor
    public AdaFileUploadCSVBatchApex(String csvFileValues) {
        recordsToBeProcessed = csvFileValues;
    }

    // batch start
    public Iterable<String> start(Database.BatchableContext context){
        // scope is a single CSV file with many rows delimited by '\n' (new line char)
        // use the utility class to iterate through the CSV rows
        // the batch will be many of these rows
        System.debug('ta:recordstobeprocessed='+recordsToBeProcessed);
        return new AdaFileUploadUtilityRowIterator(this.recordsToBeProcessed, '\n');
    }
 
    // batch execute
    public void execute(Database.BatchableContext bc, List<String> scope) {

        // process a single row from the CSV, as returned by the utility class
        // create a new Case for each row
        // create application log list
        
        // Get Hotel value from Custom label 
        String HotelValue = System.Label.AA_Case_Business_Lines_For_Hotel;
        System.debug('ta:execute1');
        List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();

        //Preload values
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('AVA_CSV_Upload_Error'));
        subCategory1Values = getSubCategory1Values();
        subCategory2Values = getSubCategory2Values();
        String a1 = '';    
        //For Closed Cases
        List<Case> caseUploadList = new List<Case>();
        List<String> chatterIdList = new List<String>();
        List<String> chatterIdWithCasesList = new List<String>();
    
        //For Transferred Cases        
        List<Case> caseUpdateList = new List<Case>();
        List<String> chatterUpdateIdList = new List<String>();
        Map<String, Case> caseUpdateMap = new Map<String, Case>();
        
        //Hold records for getting Case Id
        Map<String, List<String>> transcriptMap = new Map<String, List<String>>();
        Map<String, String> rowChatterId = new Map<String, String>();
        integer rowCount = 0;
        String comments = '';
        String errorLogComment = 'ADA CSV Upload\n';
        System.Debug('Scope Size==>'+scope.size());

        Id hotelRecId = [SELECT Id, 
                                       name 
                                FROM   RecordType 
                                WHERE  sobjecttype = 'Case' 
                                AND    Name = 'Support Case - Hotels'
                               ].Id;
        
        for (String svalue : scope) {

            try {
                System.Debug('ta:SVALUE ==> '+svalue);
                rowCount++;
                
                // split the CSV line into columns based on comma
                String[] splitvalue = svalue.split('","'); 
                System.debug(' size of splitvalue ' + splitvalue.size());
                String caseOriginValue = '';

                // CSV columns match Case fields                
                String statusValue = splitvalue[5];
                System.debug('statusValue---0'+statusValue);
                statusValue = statusValue.replaceAll('"','');
                System.debug('statusValue---1'+statusValue);
                
                System.debug('ta:coming here 1');
                if (statusValue == 'Closed') {
                        Case caseObjectValues = new Case();                               
                        
                        String chatterId = splitvalue[3];
                        chatterId = chatterId.replaceAll('"','');
                        caseObjectValues.Ada_Chatter_ID__c = chatterId;

                        // Case supplied name
                        String suppliedName = splitvalue[0];
                        suppliedName = suppliedName.replaceAll('"','');
                        System.Debug('Supplied Name ==>'+suppliedName+'<==');                       
                        Integer suppliedNameLength = (Case.SuppliedName.getDescribe()).getLength(); 
                        //Protection against multibyte strings
                        if(suppliedName.length() != Blob.valueOf(suppliedName).size()) {
                                suppliedNameLength = suppliedNameLength/2; //Multibyte Unicode takes 2x more space
                        }        
                        if(suppliedNameLength >= suppliedName.length()) {
                            caseObjectValues.SuppliedName = suppliedName;    
                        }
                        else {
                            caseObjectValues.SuppliedName =suppliedName.left(suppliedNameLength);
                            comments += 'Supplied Name Response: ' + suppliedName + '\n';
                            errorLogComment += 'Supplied Name error\n';
                        }
                        
                        if (splitvalue.size() >= 16){
                            //Case Origin
                            caseOriginValue = splitvalue[15];
                            caseOriginValue = caseOriginValue.replaceAll('"','');
                            if (caseOriginValue.equals('Live Bot') ||
                                String.isBlank(caseOriginValue)) {
                                caseOriginValue = 'Live Chat'; // api name for this value
                            }
                            System.debug('caseOriginValue-----'+caseOriginValue);
                            caseObjectValues.Origin = caseOriginValue;
                            if (caseOriginValue.contains(HotelValue)) {  //'Hotel'
                                system.debug('is it hotel---'+caseOriginValue);
                                caseObjectValues.RecordTypeId = hotelRecId;
                            }
                            //End - Case Origin                         
                        } 
                        else {
                            caseObjectValues.Origin = 'Live Chat';
                        }




                        // Case supplied email
                        String suppliedEmail = splitvalue[1];
                        suppliedEmail = suppliedEmail.replaceAll('"','');
                        Integer suppliedEmailLength = Case.SuppliedEmail.getDescribe().getLength();
                        //Protection against multibyte strings - unable to validate if multibyte
                        if(suppliedEmail.length() != Blob.valueOf(suppliedEmail).size()) {
                                caseObjectValues.SuppliedEmail = '';
                                comments += 'Supplied Email Response: ' + suppliedEmail + '\n';
                                errorLogComment += 'Supplied Email error\n';
                        }        
                        else if(suppliedEmailLength >= suppliedEmail.length()) {
                            caseObjectValues.SuppliedEmail = suppliedEmail;
                        }                        
                        
                        // Case booking number
                        String bookingNumber = splitvalue[2];
                        bookingNumber = bookingNumber.replaceAll('"','');    
                        // 24 May 2019 - AT - Additional Logic to handle booking numbers where length is greater than booking number length
                        Integer bookingNumberMaxLength = Case.Booking_Number__c.getDescribe().getLength();
                        //Protection against multibyte strings
                        if(bookingNumber.length() != Blob.valueOf(bookingNumber).size()) {
                                bookingNumberMaxLength = bookingNumberMaxLength/2; //Multibyte Unicode takes 2x more space
                        }        
                        if(bookingNumberMaxLength >= bookingNumber.length()) {
                            caseObjectValues.Booking_Number__c = bookingNumber;
                        }
                        else {
                            caseObjectValues.Booking_Number__c = '';
                            comments += 'Booking Number Response:\n' + bookingNumber + '\n';    
                            errorLogComment += 'Booking Number error\n';                    
                        }
                        
                        
                        String chatterToken = splitvalue[4];
                        chatterToken = chatterToken.replaceAll('"','');
                        caseObjectValues.Ada_Chatter_Token__c = chatterToken;
                        
                        String status = splitvalue[5];
                        status = status.replaceAll('"','');
                        caseObjectValues.Status = status;
                        
                        String type = splitvalue[6];
                        type = type.replaceAll('"','');
                        system.debug('is it hotel---207'+caseOriginValue);
                        if (caseOriginValue.contains(HotelValue)){  //'Hotel'

                            caseObjectValues.RBV_R1_Type__c = type;
                            system.debug('is it type'+caseObjectValues.RBV_R1_Type__c);
                        }
                    
                        else{
                            
                            caseObjectValues.Type = type;
                        }
                        
                        
                        String subCategory1 = splitvalue[7];
                        subCategory1 = subCategory1.replaceAll('"','');
                        
                        
                        if (caseOriginValue.contains(HotelValue)){ //'Hotel'

                            caseObjectValues.RBV_R1_Sub_Category_1__c = subCategory1;
                        }
                        else{

                            //Check if Sub Category 1 value exists
                            if(subCategory1Values.contains(subCategory1)) {
                                caseObjectValues.Sub_Category_1__c = subCategory1;
                            }
                            else {
                                errorLogComment += 'Sub Category 1 error\n';
                            }
                        }
                        
                        String subCategory2 = splitvalue[8];
                        subCategory2 = subCategory2.replaceAll('"','');
                            
                        if (caseOriginValue.contains(HotelValue)){  //'Hotel'

                            caseObjectValues.RBV_R1_Sub_Category_2__c = subCategory2;
                        }
                        else{

                            //Check if Sub Category 2 value exists
                            if(subCategory2Values.contains(subCategory2)) {
                                caseObjectValues.Sub_Category_2__c = subCategory2;
                            }
                            else {
                                errorLogComment += 'Sub Category 2 error\n';
                            }
                        }
                        
                        
                        String dispositionLevel1 = splitvalue[9];
                        dispositionLevel1 = dispositionLevel1.replaceAll('"','');

                        if (caseOriginValue.contains(HotelValue)){ //'Hotel'

                            caseObjectValues.RBV_R1_Case_Disposition__c = dispositionLevel1;
                        }
                        else{
                            caseObjectValues.Disposition_Level_1__c = dispositionLevel1;
                        }

                        String caseLanguage = splitvalue[10];
                        caseLanguage = caseLanguage.replaceAll('"','');
                        
                        switch on caseLanguage{
                            when 'en-GB' {
                                caseObjectValues.Case_Language__c = 'English';
                            }  
                            when 'in' {
                                caseObjectValues.Case_Language__c = 'Indonesian';
                            }             
                            when 'ko' {
                                caseObjectValues.Case_Language__c = 'Korean';
                            }
                            when 'th' {
                                caseObjectValues.Case_Language__c = 'Thai';
                            }
                            when 'vi' {
                                caseObjectValues.Case_Language__c = 'Vietnamese';
                            }
                            when 'ms' {
                                caseObjectValues.Case_Language__c = 'Malay';
                            }
                            when 'zh-CN' {
                                caseObjectValues.Case_Language__c = 'Simplified Chinese';
                            }
                            when 'zh-HANS-CN' {
                                caseObjectValues.Case_Language__c = 'Simplified Chinese';
                            }
                            when 'ja' {
                                caseObjectValues.Case_Language__c = 'Japanese';
                            }
                            when else {
                                caseObjectValues.Case_Language__c = 'Traditional Chinese';
                            }
                        }
                        
                        String createdDate = splitvalue[11];
                        createdDate = createdDate.replaceAll('"','');
                        DateTime createdDateValue = DateTime.valueOf(createdDate.replace('T', ' '));
                        caseObjectValues.CreatedDate = createdDateValue; 
                        
                        String closedDate = splitvalue[12];
                        closedDate = closedDate.replaceAll('"','');
                        DateTime closedDateValue = DateTime.valueOf(closedDate.replace('T', ' '));
                        caseObjectValues.ClosedDate = closedDateValue;               
                        
                        if(splitvalue.size() > 14){
                            String qualityOfService = splitvalue[14];
                            qualityOfService = qualityOfService.replaceAll('"','');
                            
                            if(qualityOfService == 'TRUE'){
                                caseObjectValues.Quality_of_Service__c = 'Yes';
                            }
                            
                            if(qualityOfService == 'FALSE'){
                                caseObjectValues.Quality_of_Service__c = 'No';
                            }
                        }
                        
                        

                        //FB Country                        
                        if (splitvalue.size() >= 17) {
                            String fbCountry = splitvalue[16];
                            fbCountry = fbCountry.replaceAll('"','');
                            caseObjectValues.FB_Country__c = fbCountry;
                        }
                        else {
                            caseObjectValues.FB_Country__c = '';
                        }
                        //End - FB Country

                        //Transcript  
                        String transcript = '';                 
                        if (splitvalue.size() >= 18) {
                            System.debug('ta:transcript incoming ='+splitvalue[17]);
                            List<String> transcriptList = splitTranscript(splitvalue[17]);
                            System.debug('ta:transcript processed ='+transcriptList);
                            transcriptMap.put(chatterId, transcriptList);
                        }
                        //End Transcript

                        //Field Agency ID                      
                        if (splitvalue.size() >= 19) {
                            String fieldAgencyId = splitvalue[18];
                            fieldAgencyId = fieldAgencyId.replaceAll('"','');
                            caseObjectValues.Airline_Refund_Field_Agency_ID__c = fieldAgencyId;
                        }
                        else {
                            caseObjectValues.Airline_Refund_Field_Agency_ID__c = '';
                        }
                        //End - Field Agency ID

                        //Field Agency ID                      
                        if (splitvalue.size() >= 19) {
                            String fieldAgencyId = splitvalue[18];
                            fieldAgencyId = fieldAgencyId.replaceAll('"','');
                            caseObjectValues.Airline_Refund_Field_Agency_ID__c = fieldAgencyId;
                        }
                        else {
                            caseObjectValues.Airline_Refund_Field_Agency_ID__c = '';
                        }
                        //End - Field Agency ID

                        //Subject
                        if (splitvalue.size() >= 20) {
                            String subject = splitvalue[19];
                            subject = subject.replaceAll('"','');
                            caseObjectValues.Subject = subject;
                        }
                        //End - Subject

                        //Subject
                        if (splitvalue.size() >= 20) {
                            String subject = splitvalue[19];
                            subject = subject.replaceAll('"','');
                            caseObjectValues.Subject = subject;
                        }
                        //End - Subject

                        //Country of Bank
                        if (splitvalue.size() >= 21) {
                            String countryOfBank = splitvalue[20];
                            countryOfBank = countryOfBank.replaceAll('"','');
                            caseObjectValues.Country_of_Bank__c = countryOfBank;
                        }
                        //End - Country of Bank
                        
                        //Business Lines
                        if (splitvalue.size() >= 24) {
                            String businessLines = splitvalue[23];
                            businessLines = businessLines.replaceAll('"','');
                            businessLines = businessLines.replaceAll(',',';');
                            caseObjectValues.Other_Topics__c = businessLines;
                        }
                        //End - Country of Bank

                        caseObjectValues.OwnerId = Label.AA_AVA_OwnerID;
                        caseObjectValues.First_Response_Completed__c = True;  
                        
                        if(comments.length() > 0) {
                            caseObjectValues.Comments__c = comments;
                        }
                        
                        //24 May 2019 - AT - ChatterId to be used for matching transcript with Case Id
                        chatterIdList.add(chatterId);
                        rowChatterId.put(chatterId,svalue);
                        caseUploadList.add(CaseObjectValues);
                        System.debug('ta:passed record size ' + caseUploadList.size());
                }
                else if (statusValue == 'Transferred') {
                    //Transferred Cases
                    Case caseObjectValues = new Case();                               
                    
                    String chatterId = splitvalue[3];
                    chatterId = chatterId.replaceAll('"','');
                    caseObjectValues.Ada_Chatter_ID__c = chatterId;
                    
                    //CSAT Rating
                    if (splitvalue.size() >= 22) {
                        String csatRating = splitvalue[21];
                        csatRating = csatRating.replaceAll('"','');
                        if(String.isNotEmpty(csatRating)) {
                          caseObjectValues.Quality_of_Service__c = csatRating == 'TRUE' ? 'Yes' :'No' ;
                        }
                    }
                    //CSAT Resolution
                    if (splitvalue.size() >= 23) {
                        String csatResolution = splitvalue[22];
                        csatResolution = csatResolution.replaceAll('"','');
                        if(String.isNotEmpty(csatResolution)) {
                          caseObjectValues.Service_Recommend__c = csatResolution == 'TRUE' ? 'Yes' :'No' ;
                        }
                    }
                    
                    
                    if (splitvalue.size() >= 23 && 
                         String.isNotEmpty(caseObjectValues.Quality_of_Service__c) &&
                        String.isNotEmpty(caseObjectValues.Service_Recommend__c)) {
                        chatterUpdateIdList.add(chatterId);
                        caseUpdateMap.put(chatterId, caseObjectValues);
                    }
                }
                        
            }
            catch (Exception e) {
                //Parse Error
                System.debug('ta:error='+e.getMessage()+' ;;;stacktrace='+e.getStackTraceString());
                badrows.add(svalue + ',\"'+e.getMessage() +'\"');
            }
        }
        
        Database.SaveResult[] res = new Database.SaveResult[]{};
        System.debug('ta:caseuploadlist='+caseUploadList);

        if (caseUploadList != null && 
            caseUploadList.size() > 0) {
             res = Database.insert(caseUploadList, false);
        } 
        String errorMsg = '';
        for (Integer i = 0; i < res.size(); i++) {
            if(!res[i].isSuccess()) {
                Database.Error[] errors = res[i].getErrors();
                
                for(Database.Error err : errors) {
                    errorMsg = err.getMessage() + ';';
                }
                badrows.add(rowChatterId.get(caseUploadList[i].Ada_Chatter_ID__c) + ',\"' + errorMsg + '\"');
            }
        }

        System.debug('ta:errorMessage='+errorMsg);

        //Retrieve CaseIds based on Ada Chatter Id
        List<CaseComment> ccUploadList = new List<CaseComment>();
        for (Case caseObj : [SELECT id, Ada_Chatter_ID__c
                                FROM Case
                                WHERE Ada_Chatter_ID__c = :chatterIdList]) {
            List<String> transcriptList = transcriptMap.get(caseObj.Ada_Chatter_ID__c);
            if(transcriptList != null && transcriptList.size() > 0) {
                for(String transcript : transcriptList) {
                    ccUploadList.add(new CaseComment(ParentId = caseObj.Id,CommentBody = transcript));
                    chatterIdWithCasesList.add(caseObj.Ada_Chatter_ID__c);
                }
            }
        }
        
        for (Case caseObj : [SELECT id, Ada_Chatter_ID__c
                                FROM Case
                                WHERE Ada_Chatter_ID__c = :chatterUpdateIdList
                                    AND IsClosed=true
                                    AND Disposition_Level_1__c != 'Resolved by bot'
                                    AND RBV_R1_Case_Disposition__c != 'Resolved by bot']) {
            Case caseUpdObj = caseUpdateMap.get(caseObj.Ada_Chatter_ID__c);
            if(caseUpdObj != null) {
                caseUpdObj.Id = caseObj.Id;
                if(!caseUpdateList.contains(caseUpdObj)) {
                caseUpdateList.add(caseUpdObj);
                }
            }
        }

        //Insert Transcripts
        if (ccUploadList != null &&
            ccUploadList.size() > 0) {
            Database.insert(ccUploadList, false);
        }
        
        if (!caseUpdateList.isEmpty()) {
            Database.update(caseUpdateList, false);
        }
        
        if(totalRows != null) {
          totalRows += caseUploadList.size();
        }
        else {
            totalRows = caseUploadList.size();
        }
    }
 
    public void finish(Database.BatchableContext info) {
        //Add Logging Here for complete Import status
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('AVA_CSV_Upload_Error'));
        List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();
        if(badrows.isEmpty()) {
            appWrapperLogList.add(Utilities.createApplicationLog('Info', 'AdaFileUploadCSVBatchApex', 'execute',
                                                                null, 'AdaFileUploadCSVBatchApex', totalRows + ' Cases Imported', '', 'Job Log', startTime, logLevelCMD, null));
        }
        else {
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'AdaFileUploadCSVBatchApex', 'execute',
                                                                null, 'AdaFileUploadCSVBatchApex', 'Error Occured on the following rows', String.join(badrows,'\n'), 'Error Log', startTime, logLevelCMD, null));
        }                     
        if(!appWrapperLogList.isEmpty()) {
            GlobalUtility.logMessage(appWrapperLogList);
        }
    }

    //Get Picklist Values for Sub Category 1 to ensure that the values from ADA will not fail validation
    public Set<String> getSubCategory1Values(){
        Set<String> pickListValuesList= new Set<String>();
    Schema.DescribeFieldResult fieldResult = Case.Sub_Category_1__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    for( Schema.PicklistEntry pickListVal : ple){
      pickListValuesList.add(pickListVal.getLabel());
    }     
    return pickListValuesList;
    }

    //Get Picklist Values for Sub Category 2 to ensure that the values from ADA will not fail validation
    public Set<String> getSubCategory2Values(){
        Set<String> pickListValuesList= new Set<String>();
    Schema.DescribeFieldResult fieldResult = Case.Sub_Category_2__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    for( Schema.PicklistEntry pickListVal : ple){
      pickListValuesList.add(pickListVal.getLabel());
    }     
    return pickListValuesList;
    }

    public static List<String> splitTranscript(String transcript) {
        //Integer transcriptMaxLength = CaseComment.CommentBody.getDescribe().getLength(); 
        Integer transcriptMaxLength = 3000;
        List<String> transcriptList = new List<String>();
        transcript = transcript.replaceAll('"','');
        transcript = transcript.replaceAll('/r/n','\n');
        Boolean isMultiByte = false;
        if(transcript.length() != Blob.valueOf(transcript).size()) {
            //transcriptMaxLength = transcriptMaxLength/2; //Multibyte Unicode takes 2x more space
            isMultiByte = true;
        }
        if(Blob.valueOf(transcript).size() < transcriptMaxLength) {
            transcriptList.add(transcript);
        } 
        else {
            //Loop through and create multiple Case comments
            String substrTranscript = transcript;
            
            while(Blob.valueOf(substrTranscript).size() > transcriptMaxLength) {
                Integer tempSubStrCount = transcriptMaxLength;
                if(isMultiByte) {
                    tempSubStrCount = tempSubStrCount / 2;
                }
                String tempStr = substrTranscript.left(tempSubStrCount);
                
                if(Blob.valueOf(tempStr).size() > transcriptMaxLength) {
                   Integer overLimit = Blob.valueOf(tempStr).size() - transcriptMaxLength;                    
                    tempSubStrCount = transcriptMaxLength - overLimit;
                    if(isMultiByte) {
                        tempSubStrCount = tempSubStrCount / 2;
                    }
                    tempStr = tempStr.left(tempSubStrCount);
                }
                
                /*Make sure the length is below - Trim by steps of 50
                while(Blob.valueOf(tempStr).size() > transcriptMaxLength) {
                    System.debug('Additional Trim --> ' + Blob.valueOf(tempStr).size());
                    tempSubStrCount -= 50;
                    tempStr = tempStr.left(tempSubStrCount);
                }*/
                transcriptList.add(tempStr);
                System.debug('Original Length: ' + Blob.valueOf(substrTranscript).size() + ' CurrentLength:' + Blob.valueOf(tempStr).size());
                substrTranscript = substrTranscript.substring(tempSubStrCount);

                if(Blob.valueOf(substrTranscript).size() <= transcriptMaxLength) {
                    transcriptList.add(substrTranscript);
                    substrTranscript = '';
                }
            }
            
        }
    System.debug('Total Case Comments --> ' + transcriptList.size());
        return transcriptList;
    }
}