/*********************************************************
 * Batch handler for incoming CSV file from Ada Bot
 * 
 * Called by AdaBotCSVEmailHandlerClass
 * 
 * 03 Oct 2019 - Alvin Tayag
 * Created to handle backfilling of 
 * ******************************************************/
public with sharing class AdaFileUploadCSVUpdateBatchApex 
       implements Database.Batchable<String>,
                  Database.Stateful,
                  Database.AllowsCallouts{
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    public String recordsToBeProcessed;
    private static Set<String> subCategory1Values;
    private static Set<String> subCategory2Values;
   
    public List<String> badrows = new List<String>();  
    public Integer totalRows;
    // constructor
    public AdaFileUploadCSVUpdateBatchApex(String csvFileValues) {
        recordsToBeProcessed = csvFileValues;
    }

    // batch start
    public Iterable<String> start(Database.BatchableContext context){
        // scope is a single CSV file with many rows delimited by '\n' (new line char)
        // use the utility class to iterate through the CSV rows
        // the batch will be many of these rows
        return new AdaFileUploadUtilityRowIterator(this.recordsToBeProcessed, '\n');
    }
 
    // batch execute
    public void execute(Database.BatchableContext bc, List<String> scope) {

        // process a single row from the CSV, as returned by the utility class
        // create a new Case for each row
        // create application log list
        List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();

        //Preload values
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('AVA_CSV_Upload_Error'));
        String a1 = '';    
        List<Case> casesToUpdate = new List<Case>();
        List<String> chatterIdList = new List<String>();

        //Hold records for getting Case Id
        Map<String, String> chatterIdLangMap = new Map<String, String>();
        integer rowCount = 0;
        String comments = '';
        String errorLogComment = 'ADA CSV Upload\n';
        System.Debug('Scope Size==>'+scope.size());

        for (String svalue : scope) {

            try {
                System.Debug('SVALUE ==> '+svalue);
                rowCount++;
                
                // split the CSV line into columns based on comma
                String[] splitvalue = svalue.split('","'); 
                System.debug(' size of splitvalue ' + splitvalue.size());

                // CSV columns match Case fields                
                String statusValue = splitvalue[5];
                statusValue = statusValue.replaceAll('"','');

                if (statusValue == 'Closed') {                             
                        
                        String chatterId = splitvalue[3];
                        chatterId = chatterId.replaceAll('"','');
                        
                        String caseLanguage = splitvalue[10];
                        caseLanguage = caseLanguage.replaceAll('"','');
                        
                        switch on caseLanguage{
                            when 'en-GB' {
                                caseLanguage = 'English';
                            }  
                            when 'in' {
                                caseLanguage = 'Indonesian';
                            }             
                            when 'ko' {
                                caseLanguage = 'Korean';
                            }
                            when 'th' {
                                caseLanguage = 'Thai';
                            }
                            when 'vi' {
                                caseLanguage = 'Vietnamese';
                            }
                            when 'ms' {
                                caseLanguage = 'Malay';
                            }
                            when 'zh-CN' {
                                caseLanguage = 'Simplified Chinese';
                            }
                            when 'zh-HANS-CN' {
                                caseLanguage = 'Simplified Chinese';
                            }
                            when 'ja' {
                                caseLanguage = 'Japanese';
                            }
                            when else {
                                caseLanguage = 'Traditional Chinese';
                            }
                        }
                        
                        chatterIdList.add(chatterId);
                        chatterIdLangMap.put(chatterId,caseLanguage);
                }
                else {
                    //Open Cases
                }
                        
            }
            catch (Exception e) {
                //Parse Error
                badrows.add(svalue + ',\"'+e.getMessage() +'\"');
            }
        }
        
        for (Case caseObj : [SELECT id, Ada_Chatter_ID__c
                                FROM Case
                                WHERE Ada_Chatter_ID__c = :chatterIdList]) {
            if(chatterIdLangMap.containsKey(caseObj.Ada_Chatter_ID__c)) {
              	caseObj.Case_Language__c = chatterIdLangMap.get(caseObj.Ada_Chatter_ID__c);
                casesToUpdate.add(caseObj);
            }
        }
        
        if (casesToUpdate != null) {
            update casesToUpdate;
        }
    }
 
    public void finish(Database.BatchableContext info) {
        //Add Logging Here for complete Import status
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('AVA_CSV_Upload_Error'));
        List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();
        if(badrows.isEmpty()) {
            appWrapperLogList.add(Utilities.createApplicationLog('Info', 'AdaFileUploadCSVBatchApex', 'execute',
                                                                null, 'AdaFileUploadCSVBatchApex', totalRows + ' Cases Imported', '', 'Job Log', startTime, logLevelCMD, null));
        }
        else {
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'AdaFileUploadCSVBatchApex', 'execute',
                                                                null, 'AdaFileUploadCSVBatchApex', 'Error Occured on the following rows', String.join(badrows,'\n'), 'Error Log', startTime, logLevelCMD, null));
        }                     
        if(!appWrapperLogList.isEmpty()) {
            GlobalUtility.logMessage(appWrapperLogList);
        }
    }
}