/*********************************************************
 * Utility class for incoming CSV file from Ada Bot
 * 
 * Called by AdaFileUploadCSVBatchApex
 * 
 * Nov 2019 - Maaz Khan - AirAsia
 * Initial release
 * 
 * Mar 2019 - Maaz Khan - AirAsia
 * Added changes to support case_origin field coming from Ada
 * 
 * Mar 2018 - Charles Thompson - Salesforce Singapore
 * Enhancements to the case_origin processing
 * ******************************************************/
public with sharing class AdaFileUploadUtilityRowIterator 
       implements Iterator<String>, Iterable<String>{

    private String m_Data;
    private Integer m_index = 0;
    private String m_rowDelimiter = '\n';

    // constructor - takes a CSV file as a string (no delimiter)
    public AdaFileUploadUtilityRowIterator(String fileData){
        m_Data = fileData; 
    }
    
    // constructor - takes CSV file and delimiter
    public AdaFileUploadUtilityRowIterator(String fileData, String rowDelimiter){
        m_Data = fileData; 
        m_rowDelimiter = rowDelimiter;
    }

    // are there any more lines to iterate?
    public Boolean hasNext(){
        return m_index < m_Data.length() ? true : false;
    }
    
    // get the next line in the file based on row delimiter
    public String next(){
        Integer key = m_Data.indexOf(m_rowDelimiter, m_index);

        // no more delimiters in the file?
        if (key == -1){
            key = m_Data.length();
        }
        
        // get the data up to the next line delimiter
        String row = m_Data.subString(m_index, key);
        
        // set the index to the beginning of the next line
        m_index = key + 1;
        
        return row;
    }

    // complete the iterator
    public Iterator<String> Iterator(){
        return this;   
    }
}