public class AgentWorkTriggerHandler{
    
    public static void UpdateCaseStatus(List<AgentWork> agentWorkList) {
        Set<String> workItemSet = new Set<String>();
        if(agentWorkList != null && !agentWorkList.isEmpty()) {
            for(AgentWork agentWorkObj : agentWorkList) {
                if(agentWorkObj.AcceptDateTime != null || Test.isRunningTest()) {
                    workItemSet.add(agentWorkObj.WorkItemId);
                }
            }
            if(!workItemSet.isEmpty()) {
                List<Case> caseList = [SELECT Id, Status FROM Case WHERE Id IN : workItemSet AND (Status = 'New' OR Status = 'Transfer to Refund')]; // Adding an additional Status value for Ourshop Refund Project
                if(caseList != null && !caseList.isEmpty()) {
                    for(Case caseObject : caseList) {
                        caseObject.Status = 'Assigned';
                    }
                    Database.update(caseList);               
                }
            }
        }
    }    
}