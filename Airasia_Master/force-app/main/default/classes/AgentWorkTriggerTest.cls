@isTest(SeeAllData=false)
public class AgentWorkTriggerTest {
    
    private static final ServiceChannel SChannel= [SELECT Id FROM ServiceChannel WHERE Masterlabel = 'Case' LIMIT 1];    
    
    @testSetup
    private static void setup() {
        UserRole userRoleOfficer = createUserRole('Officer', null);
        createUser('System Administrator', 'Officer', userRoleOfficer.Id);
    }
    
    private static UserRole createUserRole(String name, String parentRoleId) {
        UserRole userRoleObject = new UserRole();
        userRoleObject.DeveloperName = name.replace(' ', '_');
        userRoleObject.Name = name;
        userRoleObject.parentRoleId = parentRoleId;
        insert userRoleObject;
        return userRoleObject;
    }
    
     @future
    private static void createUser(String profileName, String lastName, Id roleId) {    
        String ProfileId = [SELECT Id FROM Profile WHERE Name = : profileName].Id;
        
        User userObject = new User();
        userObject.ProfileId = ProfileId;
        userObject.LastName = lastName;
        userObject.Email = lastName + '@amamama.com';
        userObject.Username = lastName + '@amamama.com' + System.currentTimeMillis();
        userObject.CompanyName = 'TEST';
        userObject.Title = 'title';
        userObject.Alias = 'alias';
        userObject.TimeZoneSidKey = 'America/Los_Angeles';
        userObject.EmailEncodingKey = 'UTF-8';
        userObject.LanguageLocaleKey = 'en_US';
        userObject.LocaleSidKey = 'en_US';
        userObject.UserRoleId = roleId;
        insert userObject;
    }
    
    @isTest
    private static void testAgentWorkTrigger() {
        
        try{
            
            User userObj = [SELECT Id FROM User WHERE LastName = 'Officer' LIMIT 1];
            Case caseObj = null;
            
            //System.runAs(userObj) {
            caseObj = new Case();
            caseObj.Subject = 'Test';
            caseObj.Status = 'New';
            insert caseObj;
            
            List<AgentWork> works = createAgentWorks(new List<Case>{caseObj} , userObj);
             AgentWorkTriggerHandler.UpdateCaseStatus(works);
            insert works;
            
            //update works;// When i update Agent Work It coverd my code coverage 
            caseObj = [SELECT Id, Status FROM Case LIMIT 1];
            System.assert(caseObj.Status == 'Assigned');
            
        }catch(Exception exe){
            System.debug('Exception '+exe);
        }
    }
    
    public static List<AgentWork> createAgentWorks(List<Case> cases, User user) {
        ServiceChannel sc = [SELECT Id FROM ServiceChannel WHERE DeveloperName = 'Case' LIMIT 1];
        PresenceUserConfig puc = [SELECT Id FROM PresenceUserConfig WHERE DeveloperName = 'Cases_Configuration_Beginner' LIMIT 1];
        ServicePresenceStatus sps = [SELECT Id FROM ServicePresenceStatus WHERE DeveloperName = 'Available_Cases' LIMIT 1];

        List<AgentWork> agentWorks = new List<AgentWork>();

        for (Case testCase : cases) {
            agentWorks.add(new AgentWork(ServiceChannelId = sc.Id,
                                         WorkItemId = testCase.Id,
                                         UserId = UserInfo.getUserId()));
        }

        return agentWorks;
    }
}