/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
10/07/2017      Pawan Kumar         Created
*******************************************************************/
global class AggregateResultIterable implements Iterable<AggregateResult> {
    public String query;
    
    global AggregateResultIterable(String soql){
        query = soql;
    }
    
    global Iterator<AggregateResult> Iterator(){
        return new AggregateResultIterator(query);
    }
}