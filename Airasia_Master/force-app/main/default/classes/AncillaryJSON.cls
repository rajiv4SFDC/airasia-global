public class AncillaryJSON {

	public List<Data> data {get;set;} 
	public String type_Z {get;set;} // in json: type

	public AncillaryJSON(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'data') {
						data = arrayOfData(parser);
					} else if (text == 'type') {
						type_Z = parser.getText();
					} else {
						System.debug(LoggingLevel.WARN, 'AncillaryJSON consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class Data {
		public Double baggage_score {get;set;} 
		public Double fnb_score {get;set;} 
		public Double ins_score {get;set;} 
		public Double premium_flatbed_score {get;set;} 
		public Double premium_flex_score {get;set;} 
		public Double seat_score {get;set;} 
		public Double value_pack_score {get;set;} 
		public String fav_meal {get;set;} 
		public String fav_meal_code {get;set;} 

		public Data(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'baggage_score') {
							baggage_score = parser.getDoubleValue();
						} else if (text == 'fnb_score') {
							fnb_score = parser.getDoubleValue();
						} else if (text == 'ins_score') {
							ins_score = parser.getDoubleValue();
						} else if (text == 'premium_flatbed_score') {
							premium_flatbed_score = parser.getDoubleValue();
						} else if (text == 'premium_flex_score') {
							premium_flex_score = parser.getDoubleValue();
						} else if (text == 'seat_score') {
							seat_score = parser.getDoubleValue();
						} else if (text == 'value_pack_score') {
							value_pack_score = parser.getDoubleValue();
						} else if (text == 'fav_meal') {
							fav_meal = parser.getText();
						} else if (text == 'fav_meal_code') {
							fav_meal_code = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static AncillaryJSON parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new AncillaryJSON(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	



    private static List<Data> arrayOfData(System.JSONParser p) {
        List<Data> res = new List<Data>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Data(p));
        }
        return res;
    }
}