@IsTest
public class AncillaryJSON_Test {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{\"data\":[{\"baggage_score\":0.5733333333333334,\"fnb_score\":0.05333333333333334,\"ins_score\":0.08,\"premium_flatbed_score\":0,\"premium_flex_score\":0,\"seat_score\":0,\"value_pack_score\":0.02666666666666667,\"fav_meal\":\"EZPay Meal Discount\",\"fav_meal_code\":\"Not found\"}],\"type\":\"desktop_gaclient_id\"}';
		AncillaryJSON r = AncillaryJSON.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AncillaryJSON objJSON2Apex = new AncillaryJSON(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		System.assert(objJSON2Apex.data == null);
		System.assert(objJSON2Apex.type_Z == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AncillaryJSON.Data objData = new AncillaryJSON.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.baggage_score == null);
		System.assert(objData.fnb_score == null);
		System.assert(objData.ins_score == null);
		System.assert(objData.premium_flatbed_score == null);
		System.assert(objData.premium_flex_score == null);
		System.assert(objData.seat_score == null);
		System.assert(objData.value_pack_score == null);
		System.assert(objData.fav_meal == null);
		System.assert(objData.fav_meal_code == null);
	}
}