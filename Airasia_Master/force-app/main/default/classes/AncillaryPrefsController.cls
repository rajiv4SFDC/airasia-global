/*******************************************************************
Author:        Mark Alvin Tayag
Email:         mtayag@salesforce.com

History:
Date            Author              Comments
-------------------------------------------------------------
16/05/2019      Mark Alvin Tayag         Created
*******************************************************************/
public class AncillaryPrefsController {

    private String returnedContinuationIdAPIKey;
    private String returnedContinuationIdAccessToken;
    private String returnedContinuationIdAncillaryData;
    public String apiKeyResponse {
        get;
        set;
    }
    public String clientIDResponse {
        get;
        set;
    }
    public String accessTokenResponse {
        get;
        set;
    }
    public String ancillaryResponse {
        get;
        set;
    }
	
    public Boolean retrieveSuccess {
        get;
        set;
    }
    
    //Ancilliary Scores
    public Decimal baggageScore {
        get;
        set;
    }

    public Decimal fnbScore {
        get;
        set;
    }

    public Decimal insScore {
        get;
        set;
    }

    public Decimal premiumFlatbedScore {
        get;
        set;
    }

    public Decimal premiumFlexScore {
        get;
        set;
    }

    public Decimal seatScore {
        get;
        set;
    }

    public Decimal valuePackScore {
        get;
        set;
    }

    public String favMeal {
        get;
        set;
    }

    public String favMealCode {
        get;
        set;
    }
    //END - Ancilliary Scores
    
	private final Case caseObj;
    public String caseId{get;set;}
    private String contactEmail = '';
    private static String processLog = '';
    private static String payLoad = '';
    private DateTime startTime = DateTime.now();
    
    public AncillaryPrefsController(ApexPages.StandardController stdController) {
        this.caseObj = (Case)stdController.getRecord();
        if(this.caseObj!=null && this.caseObj.Id!=null){
            caseId = this.caseObj.Id;
            Case caseObj = [SELECT Id,ContactEmail FROM CASE WHERE Id =:caseId];
            contactEmail = caseObj.ContactEmail;
        }
         
    }
    
    //Load Ancilliary Details
    public Object getDetails() {
        return requestAPIKey();
    }
    // Start - GET By-Origin

    //Create an HTTP request object
    public HttpRequest getAPIKeyRequest() {
        //Form the request
        Web_Services_Credentials__mdt ancilliaryCred = (Web_Services_Credentials__mdt)getAncilliaryCredentials();
        HttpRequest request = new HttpRequest();
        String url = ancilliaryCred.Url__c + '/config/v2/clients/by-origin';
        request.setEndpoint(url);
        System.debug('URL--> ' + url);
        request.setMethod('GET');
        request.setHeader('Origin',ancilliaryCred.Origin__c);
        return request;
    }    

    //Create Continuation for Http Request
    public Object requestAPIKey() {
        Continuation con = new Continuation(60);
        con.continuationMethod = 'processAPIKeyResponse';
        HttpRequest httpReq = getAPIKeyRequest();
        returnedContinuationIdAPIKey = con.addHttpRequest(httpReq);
        System.debug('returnedContinuationIdAPIKey--> ' + returnedContinuationIdAPIKey);
        return con;
    }

    //Process Response
    public Object processAPIKeyResponse() {
        HttpResponse httpRes = Continuation.getResponse(returnedContinuationIdAPIKey);
        
        return getAPIKey(httpRes);
    }

    //Store Results in Variable for loading in VFP
    public Object getAPIKey(HttpResponse httpRes) {
        try {
            apiKeyResponse = '';
            payLoad = httpRes.getBody();
             
            if (httpRes.getStatusCode() == 200) {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(payLoad);
                apiKeyResponse = (String)results.get('apiKey'); //API Key
                clientIDResponse = (String)results.get('id');   //Client ID

                //Chained Continuation Call
	       		return requestAccessToken();
            }
            else {
                // any other network error
                apiKeyResponse = 'AncilliaryApiKeyExceptionMsg:' + 'STATUS: ' + httpRes.getStatus() + ' : STATUS_CODE: ' + httpRes.getStatusCode();
                processLog += payLoad + '\r\n';
                System.debug('Error --> ' + apiKeyResponse);
            }
        }
        catch (Exception wsAncilliaryGetApiKey) {
            System.debug('Error!');
            System_Settings__mdt logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Ws_Ancilliary_CMD_NAME'));
            GlobalUtility.logMessage(Utilities.createApplicationLog('Error', 'AncilliaryPrefsController', 'getApiKey',
                                                                    null, 'Ancilliary', processLog, payLoad,'Integration Log', startTime, logLevelCMD, wsAncilliaryGetApiKey));
        }
        
        return null;
    }
     //END - GET By-Origin

    //Start - POST By-Credentials
    //Create an HTTP request object
    public HttpRequest getAccessTokenRequest() {
        //Form the request
        Web_Services_Credentials__mdt ancilliaryCred = (Web_Services_Credentials__mdt)getAncilliaryCredentials();
        HttpRequest request = new HttpRequest();
        String reqParam = 'clientId=' + clientIDResponse;
        String url = ancilliaryCred.Url__c + '/sso/v2/authorization/admin/by-credentials?' + reqParam;
        request.setEndpoint(url);
        System.debug('URL--> ' + url);
        request.setMethod('POST');
        request.setHeader('Origin',ancilliaryCred.Origin__c);
        request.setHeader('x-api-key', apiKeyResponse);
        String reqBody = '{"username":"'+ ancilliaryCred.Username__c +'",' + 
                         '"password":"'+ ancilliaryCred.Password__c +'"}';
        request.setBody(reqBody);
        return request;
    }

    //Create Continuation for Http Request
    public Object requestAccessToken() {
        Continuation con = new Continuation(60);
        con.continuationMethod = 'processAccessTokenResponse';
        HttpRequest httpReq = getAccessTokenRequest();
        returnedContinuationIdAccessToken = con.addHttpRequest(httpReq);
        System.debug('returnedContinuationIdAccessToken--> ' + returnedContinuationIdAccessToken);
        return con;
    }

    //Process Response
    public Object processAccessTokenResponse() {
        HttpResponse httpRes = Continuation.getResponse(returnedContinuationIdAccessToken);
        return getAccessToken(httpRes);
    }

    //Store Results in Variable for loading in VFP
    //public Object getAccessToken(HttpResponse httpRes) {
    public Object getAccessToken(HttpResponse httpRes) {
        try {
            accessTokenResponse = '';
            payLoad = httpRes.getBody();
             
            if (httpRes.getStatusCode() == 200) {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(payLoad);
                accessTokenResponse = (String)results.get('accessToken'); //Access Token

                //Add Continuation here
                return requestAncilliaryData();
            }
            else {
                // any other network error
                accessTokenResponse = 'AncilliaryAccessTokenExceptionMsg:' + 'STATUS: ' + httpRes.getStatus() + ' : STATUS_CODE: ' + httpRes.getStatusCode();
                processLog += payLoad + '\r\n';
                System.debug('Error --> ' + accessTokenResponse);
            }
        }
        catch (Exception wsAncilliaryGetAccessToken) {
/*
            System.debug('Error!');
            System_Settings__mdt logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Ws_Ancilliary_CMD_NAME'));
            GlobalUtility.logMessage(Utilities.createApplicationLog('Error', 'AncilliaryPrefsController', 'getApiKey',
                                                                    null, 'Ancilliary', processLog, payLoad,'Integration Log', startTime, logLevelCMD, wsAncilliaryGetAccessToken));
*/

                accessTokenResponse = 'AncilliaryAccessTokenExceptionMsg:' + 'STATUS: ' + httpRes.getStatus() + ' : STATUS_CODE: ' + httpRes.getStatusCode();
                processLog += payLoad + '\r\n';
                System.debug('Error --> ' + accessTokenResponse);        }
        
        return null;
    }
    //END - POST By-Credentials
    
    //Start - POST Ancilliary Data
    public HttpRequest getAncilliaryDataRequest() {
        //Form the request
        Web_Services_Credentials__mdt ancilliaryCred = (Web_Services_Credentials__mdt)getAncilliaryCredentials();
        HttpRequest request = new HttpRequest();
        String email = EncryptEmail(contactEmail);
        email = EncodingUtil.urlEncode(email, 'utf-8');
        String url = ancilliaryCred.Url__c + '/um/v2/admin/users/'+email+'/ancillary-preference/';
        System.debug('URL--> ' + url);
        request.setEndpoint(url);
        request.setMethod('GET');
        request.setHeader('Origin',ancilliaryCred.Origin__c);
        System.debug('Origin--> ' + ancilliaryCred.Origin__c);
        request.setHeader('x-api-key',apiKeyResponse);
        System.debug('x-api-key--> ' + apiKeyResponse);
        request.setHeader('x-aa-client-id', clientIdResponse);
        System.debug('x-aa-client-id--> ' + clientIdResponse);
        request.setHeader('Authorization', accessTokenResponse);
        System.debug('Authorization--> ' + accessTokenResponse);
        request.setHeader('x-sf-encrypt', 'sf');
        return request;
    }

    //Create Continuation for Http Request
    public Object requestAncilliaryData() {
        Continuation con = new Continuation(60);
        con.continuationMethod = 'processAncillaryDataResponse';
        HttpRequest httpReq = getAncilliaryDataRequest();
        returnedContinuationIdAncillaryData = con.addHttpRequest(httpReq);
        System.debug('returnedContinuationIdAncillaryData--> ' + returnedContinuationIdAncillaryData);
        return con;
    }

    //Process Response
    public Object processAncillaryDataResponse() {
        HttpResponse httpRes = Continuation.getResponse(returnedContinuationIdAncillaryData);
        getAncillaryData(httpRes);
        return null;
    }

    public void getAncillaryData(HttpResponse httpRes) {
        try {
            ancillaryResponse = '';
            payLoad = httpRes.getBody();
             
            if (httpRes.getStatusCode() == 200) {
                AncillaryJSON r = AncillaryJSON.parse(payLoad);
                baggageScore = (Decimal)r.data[0].baggage_score;
                fnbScore = (Decimal)r.data[0].fnb_score;
                insScore = (Decimal)r.data[0].ins_score;
                premiumFlatbedScore = (Decimal)r.data[0].premium_flatbed_score;
                premiumFlexScore = (Decimal)r.data[0].premium_flex_score;
                seatScore = (Decimal)r.data[0].seat_score;
                valuePackScore = (Decimal)r.data[0].value_pack_score;
                favMeal = (String)r.data[0].fav_meal;
                favMealCode = (String)r.data[0].fav_meal_code;
		        retrieveSuccess = true;
                System.debug('Payload--> ' + payLoad);
            }
            else {
                // any other network error
                ancillaryResponse = 'AncilliaryGetAncillaryExceptionMsg:' + 'STATUS: ' + httpRes.getStatus() + ' : STATUS_CODE: ' + httpRes.getStatusCode();
                processLog += payLoad + '\r\n';
                System.debug('Error --> ' + ancillaryResponse);
            }
        }
        catch (Exception wsAncilliaryGetAncillaryResponse) {
            System.debug('Error!');
            System_Settings__mdt logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Ws_Ancilliary_CMD_NAME'));
            GlobalUtility.logMessage(Utilities.createApplicationLog('Error', 'AncilliaryPrefsController', 'getAncillary',
                                                                    null, 'Ancilliary', processLog, payLoad,'Integration Log', startTime, logLevelCMD, wsAncilliaryGetAncillaryResponse));
        }
    }
    //END - POST Ancilliary Data
    public static Object getAncilliaryCredentials() {
        List<Web_Services_Credentials__mdt> ancilliaryCreds = [SELECT Origin__c,
                                                                      URL__c,
                                                                      Username__c,
                                                                      Password__c,
                                                                      Key__c
                                                              FROM Web_Services_Credentials__mdt 
                                                              WHERE Web_Service_Name__c ='ANCILLIARY_WS' LIMIT 1];
        return ancilliaryCreds[0];
    }
	
    public static String EncryptEmail(String email) {
        Blob iv = Crypto.generateAesKey(128);
        Blob clearEmail = Blob.valueOf(email);
        Web_Services_Credentials__mdt ancilliaryCred = (Web_Services_Credentials__mdt)getAncilliaryCredentials();
        Blob key = Blob.valueOf(ancilliaryCred.Key__c);
        Blob encryptedData = Crypto.encrypt('AES256', key, iv, clearEmail);
        
        return EncodingUtil.convertToHex(iv) + '/' + EncodingUtil.convertToHex(encryptedData);
    }
}