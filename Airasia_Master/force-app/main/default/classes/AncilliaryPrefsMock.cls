@isTest
global class AncilliaryPrefsMock {
	
    global class ConfigByOrigin implements HttpCalloutMock {
    	
        global HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"id":"AASFC","origins":["957cc906-495c-4ad0-82a9-467f5988f508"],"apiKey":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6IkFBU0ZDIiwib3JpZ2luIjoiOTU3Y2M5MDYtNDk1Yy00YWQwLTgyYTktNDY3ZjU5ODhmNTA4IiwiaWF0IjoxNTU3OTkyMTQ4LCJleHAiOjE1NTc5OTU3NDh9.zR6QgYmOClVInI8T3tDMuwi8Kwhanr06YuX1_LZh6UQ"}');
            response.setStatusCode(200);
            return response; 
        }
    
    }
    
//    global class 
}