/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
10/07/2017      Pawan Kumar         Created
*******************************************************************/
@isTest(SeeAllData=false)
public class ApplicationLogWrapper_Test {
    
    static testMethod void testApplicationLogWrapper() {
        
        // Create Wrapper Class
        DateTime curDateTime =DateTime.now();
        ApplicationLogWrapper appLogWrapper = new ApplicationLogWrapper();
        appLogWrapper.logLevel = 'Info';
        appLogWrapper.sourceClass = 'TestBatch';
        appLogWrapper.sourceFunction = 'finish(Database.BatchableContext bc)';
        appLogWrapper.referenceId = 'XYZ';
        appLogWrapper.referenceInfo = 'Apex Batch: Job Id: ';
        appLogWrapper.logMessage = 'Test Logss';
        appLogWrapper.applicationName = 'GlobalPurgeBatch';
        appLogWrapper.exceptionThrown = null;
        appLogWrapper.startDate = curDateTime;
        appLogWrapper.endDate = curDateTime;
        appLogWrapper.type = 'Job Log';
        appLogWrapper.payLoad  = 'Int Log';
        
        // Set Log Level
        appLogWrapper.isDebug = true;
        appLogWrapper.isInfo = true;
        appLogWrapper.isError = true;
        appLogWrapper.isWarning = true;
        
        System.assertequals('Info',appLogWrapper.logLevel);
        System.assertequals('TestBatch',appLogWrapper.sourceClass);
        System.assertequals('finish(Database.BatchableContext bc)',appLogWrapper.sourceFunction);
        System.assertequals('XYZ',appLogWrapper.referenceId);
        System.assertequals('Apex Batch: Job Id: ',appLogWrapper.referenceInfo);
        System.assertequals('Test Logss',appLogWrapper.logMessage);
        System.assertequals('GlobalPurgeBatch',appLogWrapper.applicationName);
        System.assertequals(null,appLogWrapper.exceptionThrown);
        System.assertequals(curDateTime,appLogWrapper.startDate);
        System.assertequals(curDateTime,appLogWrapper.endDate);
        System.assertequals('Job Log',appLogWrapper.type);
        System.assertequals('Int Log',appLogWrapper.payLoad);
        
    }
}