/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com
Description:   This class is just for a formal code coverage for WSDLtoApex cass

History:
Date            Author              Comments
-------------------------------------------------------------
31-07-2017      Sharan Desai	    Created
*******************************************************************/

public class AsyncTempuriOrgPersn {
    public class GetPersonResponse_elementFuture extends System.WebServiceCalloutFuture {
        public schemasDatacontractOrg200407AceEntPersn.Person getValue() {
            tempuriOrgPersn.GetPersonResponse_element response = (tempuriOrgPersn.GetPersonResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.GetPersonResult;
        }
    }
    public class FindPersonsResponse_elementFuture extends System.WebServiceCalloutFuture {
        public schemasDatacontractOrg200407AceEntPersn.FindPersonResponseData getValue() {
            tempuriOrgPersn.FindPersonsResponse_element response = (tempuriOrgPersn.FindPersonsResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.FindPersonsResult;
        }
    }
    public class AsyncbasicHttpsPersonService {
       // public String endpoint_x = 'https://stgace.airasia.com/ACE/PersonService.svc';
        //public String endpoint_x = 'https://requestb.in/1f0dp7j1';
        
         Web_Services_Credentials__mdt credentialsStore = [Select URL__c from Web_Services_Credentials__mdt where Web_Service_Name__c = 'ACE_PERSON_WS'];
        public String endpoint_x = credentialsStore.URL__c;
        
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://tempuri.org/', 'tempuriOrgPersn', 'http://schemas.datacontract.org/2004/07/ACE.Entities', 'schemasDatacontractOrg200407AceEntPersn', 'http://schemas.microsoft.com/2003/10/Serialization/', 'schemasMicrosoftCom200310SerializatPersn'};
        public AsyncTempuriOrgPersn.GetPersonResponse_elementFuture beginGetPerson(System.Continuation continuation,String strSessionID,schemasDatacontractOrg200407AceEntPersn.GetPersonRequestData objGetPersonRequestData) {
            tempuriOrgPersn.GetPerson_element request_x = new tempuriOrgPersn.GetPerson_element();
            request_x.strSessionID = strSessionID;
            request_x.objGetPersonRequestData = objGetPersonRequestData;
            return (AsyncTempuriOrgPersn.GetPersonResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncTempuriOrgPersn.GetPersonResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://tempuri.org/IPersonService/GetPerson',
              'http://tempuri.org/',
              'GetPerson',
              'http://tempuri.org/',
              'GetPersonResponse',
              'tempuriOrgPersn.GetPersonResponse_element'}
            );
        }
        public AsyncTempuriOrgPersn.FindPersonsResponse_elementFuture beginFindPersons(System.Continuation continuation,String strSessionID,schemasDatacontractOrg200407AceEntPersn.FindPersonRequest objFindPersonRequestData) {
            tempuriOrgPersn.FindPersons_element request_x = new tempuriOrgPersn.FindPersons_element();
            request_x.strSessionID = strSessionID;
            request_x.objFindPersonRequestData = objFindPersonRequestData;
            return (AsyncTempuriOrgPersn.FindPersonsResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncTempuriOrgPersn.FindPersonsResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://tempuri.org/IPersonService/FindPersons',
              'http://tempuri.org/',
              'FindPersons',
              'http://tempuri.org/',
              'FindPersonsResponse',
              'tempuriOrgPersn.FindPersonsResponse_element'}
            );
        }
    }
}