/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
28/08/2017      Pawan Kumar         Created
*******************************************************************/
public without sharing class B2BClientSupportRouting {
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    private static String processLog = '';
    private static List < ApplicationLogWrapper > appWrapperLogList = null;
    
    @InvocableMethod
    public static void assignCaseToRelevantQueue(List < Id > caseIds) {
        // create application log list
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        
        try {
            // get System Setting details for Logging
            logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Routing_Log_Level_CMD_NAME'));
            
            // Query Case for extra details
            List < Case > updateCaseList;
            List < Case > caseList = [Select Transfer_to_B2B_Client_Support_Team__c from Case where Id IN: caseIds];
            
            if (!caseList.isEmpty()) {
                updateCaseList = new List < Case > ();
                
                Map < String, String > boCaseRoutingMap = new Map < String, String > ();
                List < B2B_Client_Support_Routing__mdt > boCaseRoutingList = [Select Transfer_to_B2B_Client_Support_Team__c, Queue__c from B2B_Client_Support_Routing__mdt];
                
                for (B2B_Client_Support_Routing__mdt eachRow: boCaseRoutingList) {
                    boCaseRoutingMap.put(eachRow.Transfer_to_B2B_Client_Support_Team__c, eachRow.Queue__c);
                }
                //System.debug('boCaseRoutingMap:'+JSON.serializePretty(boCaseRoutingMap));
                
                for (Case caseToBeAssignedToQueue: caseList) {
                    String caseId = caseToBeAssignedToQueue.id;
                    
                    try {
                        String criteriaKey = caseToBeAssignedToQueue.Transfer_to_B2B_Client_Support_Team__c;
                        if (boCaseRoutingMap.containsKey(criteriaKey)) {
                            caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(boCaseRoutingMap.get(criteriaKey));
                        } else {
                            String soqlQuery = 'Select Id, Queue__c from B2B_Client_Support_Routing__mdt' +
                                ' where Transfer_to_B2B_Client_Support_Team__c=' + caseToBeAssignedToQueue.Transfer_to_B2B_Client_Support_Team__c 
                                + '\r\n';
                            throw new QueryException(soqlQuery);
                        }
                        
                    } catch (Exception queueQueryEx) {
                        System.debug(LoggingLevel.ERROR, queueQueryEx);
                        
                        caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(Utilities.getAppSettingsMdtValueByKey('Case_Routing_Error_Queue'));
                        processLog += 'Failed to get QUEUE NAME from routing table: \r\n';
                        processLog += queueQueryEx.getMessage() + '\r\n';
                        
                        appWrapperLogList.add(Utilities.createApplicationLog('Warning', 'B2BClientSupportRouting', 'assignCaseToRelevantQueue',
                                                                             caseId, 'B2B Client Support Routing', processLog, null, 'Error Log', startTime, logLevelCMD, queueQueryEx));
                    }
                    
                    // add case to update list
                    updateCaseList.add(caseToBeAssignedToQueue);
                }
                
                // update Case
                if (updateCaseList != null && !updateCaseList.isEmpty())
                    update updateCaseList;
            }
        } catch (Exception caseAssignFailedEx) {
            System.debug(LoggingLevel.ERROR, caseAssignFailedEx);
            
            String firstCaseId = '';
            if (caseIds != null && !caseIds.isEmpty()) {
                firstCaseId = caseIds[0];
            }
            
            System.debug(LoggingLevel.ERROR, caseAssignFailedEx);
            processLog += 'Failed to assign queue for the following Case Ids: \r\n';
            processLog += JSON.serializePretty(caseIds) + '\r\n';
            processLog += 'Root Cause: ' + caseAssignFailedEx.getMessage() + '\r\n';
            
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'B2BClientSupportRouting', 'assignCaseToRelevantQueue',
                                                                 firstCaseId, 'B2B Client Support Routing', processLog, null, 'Error Log', startTime, logLevelCMD, caseAssignFailedEx));
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
        }
   }
}