/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/06/2017      Pawan Kumar         Created
*******************************************************************/
@isTest(SeeAllData = false)
private class B2BClientSupportRouting_Test{
    
    private static testMethod void testPosBORouting() {
        Test.startTest();
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Transfer_to_B2B_Client_Support_Team__c', 'Global Client Support');
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Case.Owner.Name',
                'Transfer_to_B2B_Client_Support_Team__c'
                };
                    
                    List < Case > casesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Global Client Support', casesList[0].Transfer_to_B2B_Client_Support_Team__c);
        
        // call our routing logic.
        B2BClientSupportRouting.assignCaseToRelevantQueue(new List < String > {
            casesList[0].Id
                });
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Global Client Support', updatedCasesList[0].Transfer_to_B2B_Client_Support_Team__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        String queueId = TestUtility.getQueueIdByName('Global Client Support');
        System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        Test.stopTest();
    }
    
    private static testMethod void testNegRouting() {
        Test.startTest();
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_2');
        caseFieldValueMap.put('Transfer_to_B2B_Client_Support_Team__c', null);
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'OwnerId' , 
                'Transfer_to_B2B_Client_Support_Team__c'
                };
                    
                    List < Case > casesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals(null, casesList[0].Transfer_to_B2B_Client_Support_Team__c);
        
        // call our routing logic.
        B2BClientSupportRouting.assignCaseToRelevantQueue(new List < String > {
            casesList[0].Id
                });
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals(null, updatedCasesList[0].Transfer_to_B2B_Client_Support_Team__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        //String queueId = TestUtility.getQueueIdByName('Error Handling Queue');
        String queueId = TestUtility.getQueueIdByName('Malaysia Client Support'); //25 Sept 2019 - Error Queue is Malaysia Client Support
        System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        //System.assertNotEquals(null,appLogs);
        //System.assertEquals(1,appLogs.size());
        
        Test.stopTest();
    }
    
    
    private static testMethod void testNoCaseIdsRouting() {
        Test.startTest();
        
        // call our routing logic.
        B2BClientSupportRouting.assignCaseToRelevantQueue(null);
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases();
        System.assertNotEquals(null, updatedCasesList);
        System.assertEquals(0, updatedCasesList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
        
        Test.stopTest();
    }
    
}