public class BQDeleteSchedule implements Schedulable{
	
    public void execute(SchedulableContext ctx) {
        
        try{
            
            
            for(BQIntegration__mdt bqIntegrationRecord : [SELECT Batch_Size__c, Dataset_Id__c, Project_Id__c, 
                                                          Query_String__c, TableId__c, Type__c, 
                                                          BQIntegrationUserId__c, 
                                                          Undelete_confirmed_sent_records__c, Hard_delete_records__c, 
                                                          Undelete_unsent_records__c, No_of_days_to_critical_alert__c  
                                                          FROM BQIntegration__mdt WHERE 
                                                          Type__c = :Constants.BQ_INTEGRATION_CONFIG_TYPE_DELETE 
                                                          AND isActive__c = true]){
            
            		if(bqIntegrationRecord.Query_String__c != null && bqIntegrationRecord.Query_String__c != ''){
                                                                  
                    	Database.executeBatch(new BQObjectDelete(bqIntegrationRecord.TableId__c, 
                                                                bqIntegrationRecord.Query_String__c, 
                                                                bqIntegrationRecord.Project_Id__c,
                                                                bqIntegrationRecord.Dataset_Id__c,
                                                                bqIntegrationRecord.BQIntegrationUserId__c,
                                                                bqIntegrationRecord.Undelete_confirmed_sent_records__c, 
                                                                bqIntegrationRecord.Hard_delete_records__c,
                                                                bqIntegrationRecord.Undelete_unsent_records__c,
                                                                (Integer)bqIntegrationRecord.No_of_days_to_critical_alert__c),
                                              					(Integer)bqIntegrationRecord.Batch_Size__c);    
                    }		                                                  
            }
			
        }
        catch(Exception ex){
            
            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_ERROR, 'BQDeleteSchedule', 'execute', '','', 'Exception occured in BQDeleteSchedule', '', 'BQIntegration', ex,  System.now(), System.now(), Constants.LOG_TYPE_ERROR_LOG, true, true,   true, true);
        }
    }    
    
    
}