public virtual class BQMapping {
    
    
    
    public BQMapping returnMappedObject(sObject record){
        
        if(record instanceof LiveChatTranscriptEvent){
            return getChatTranscriptEventRecord(record);
        }
        else if(record instanceof Lead){
            
            return getLeadWrapRecord(record);
        }
        
        return null;
    }
    
    
    
    public class ChatTranscriptEventWrap extends BQMapping{
        
        public String Id;//Id field
        public String AgentId;//AgentId field
        public String AgentName;//Agent.Name field
        public String Detail;//Detail field
        public String LastReferencedDate;//LastReferencedDate field
        public String LastViewedDate;//LastViewedDate field
        public String LiveChatTranscriptId;//LiveChatTranscriptId field
        public String Name;//Name field
        public String TranscriptTime;//Time field
        public String Type;//Type field;
        public String CreatedById;//CreatedById field
        public String CreatedDate;//CreatedDate field
        public String CurrencyIsoCode;//CurrencyIsoCode field
        public String LastModifiedById;//LastModifiedById field
        public String LastModifiedDate;//LastModifiedDate field
        public String SystemModstamp;//SystemModstamp field
        
    }
    
    public class LeadWrap extends BQMapping{
        
        public String Id;
        public String FirstName;
        public String LastName;
        public String Company;
        public String Status;
        public String Datefield;
        public String abcDatetime;
    } 
    
    public LeadWrap getLeadWrapRecord(sObject rec){
        
        Lead leadRec = (Lead)rec;
        LeadWrap leadWrapRec = new LeadWrap();
        leadWrapRec.Id = leadRec.Id;
        leadWrapRec.FirstName = leadRec.FirstName;
        leadWrapRec.LastName = leadRec.LastName;
        leadWrapRec.Company = leadRec.Company;
        leadWrapRec.Status = leadRec.Status;
        leadWrapRec.Datefield = leadRec.CreatedDate !=null ? leadRec.CreatedDate.formatGMT(Constants.dtTimeGMTFormat) : null;
        leadWrapRec.abcDatetime = leadRec.CreatedDate !=null ? leadRec.CreatedDate.formatGMT(Constants.dtTimeGMTFormat) : null;
        return leadWrapRec;
    }
    
    /*
     * Query to be used: 
     	"SELECT Id, AgentId, 
       	Agent.Name, Detail, LastReferencedDate, LastViewedDate, 
		LiveChatTranscriptId, Name,Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, 
		LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent WHERE 
		CreatedDate < Last_n_days:365"
    */
    public ChatTranscriptEventWrap getChatTranscriptEventRecord(sObject rec){
        
        LiveChatTranscriptEvent tranRec = (LiveChatTranscriptEvent)rec;
        ChatTranscriptEventWrap wrap = new ChatTranscriptEventWrap();
        wrap.Id = tranRec.Id;
        wrap.AgentId = tranRec.AgentId;
        wrap.AgentName = tranRec.Agent.Name;
        wrap.Detail = tranRec.Detail;
        wrap.LastReferencedDate= tranRec.LastReferencedDate !=null ? tranRec.LastReferencedDate.formatGMT(Constants.dtTimeGMTFormat) : null;
        wrap.LastViewedDate=tranRec.LastViewedDate !=null ? tranRec.LastViewedDate.formatGMT(Constants.dtTimeGMTFormat) : null;
        wrap.LiveChatTranscriptId=tranRec.LiveChatTranscriptId;
        wrap.Name=tranRec.Name;
        wrap.TranscriptTime=tranRec.Time!=null?tranRec.Time.formatGMT(Constants.dtTimeGMTFormat):null;
        wrap.Type=tranRec.Type;
        wrap.CreatedById=tranRec.CreatedById;
        wrap.CreatedDate=tranRec.CreatedDate!=null?tranRec.CreatedDate.formatGMT(Constants.dtTimeGMTFormat):null;
        wrap.CurrencyIsoCode=tranRec.CurrencyIsoCode;
        wrap.LastModifiedById=tranRec.LastModifiedById;
        wrap.LastModifiedDate=tranRec.LastModifiedDate!=null?tranRec.LastModifiedDate.formatGMT(Constants.dtTimeGMTFormat):null;
        wrap.SystemModstamp=tranRec.SystemModstamp!=null?tranRec.SystemModstamp.formatGMT(Constants.dtTimeGMTFormat):null;
        
        return wrap;
        
    }

}