global class BQObjectBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    public String projectId;
	public String datasetId;
	public String tableId;
    public BQMapping bqMapping = new BQMapping();
    
    //airasia-salesforce-stg:Salesforce_Dataset.leads_test
    public GoogleBigQuery google ;
	public GoogleBigQuery.InsertAll insertAll ;
    public Boolean doNotDelete;
	
	//constructor
    public String queryString = '';
    public BQObjectBatch(String tableId, String queryString, String projectId, String datasetId, Boolean doNotDelete){
        
        this.tableId = tableId;
        this.queryString = queryString;
        this.projectId = projectId;
        this.datasetId = datasetId;
		google = new GoogleBigQuery(projectId, datasetId, tableId);
		insertAll = new GoogleBigQuery.InsertAll();        
        this.doNotDelete = doNotDelete;
    }
    
    
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        return Database.getQueryLocator(queryString);
    }    

    global void execute(Database.BatchableContext bc, List<sObject> records){
        String JSONstring;
        try{
            //process each batch of records
            for(sObject record : records){
            	//add records to send to BQ
                insertAll.addObject(bqMapping.returnMappedObject(record));
            } 
            //ta:debugging
            JSONstring = System.JSON.serialize(insertAll);
            System.debug('ta:Before sending starts');
            
            
            if (/*!Test.isRunningTest() && */!google.add(insertAll)) {
                //throw data send failure exception
                System.debug('ta:Error in sending records: ' + google.getResponse());
                throw new BQUtilities.DataSendFailureException('Error while sending records. Data not sent. Error= '+google.getResponse());
                
            }
            else{
                
                System.debug('ta:Success: in sending records to BQ');
                
                //delete records;
                if(!doNotDelete){
                	
                    delete records;
                    GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_INFO, 'BQObjectBatch', 'Execute', '','', 'Success in sending and deleting records. JSONData sent='+JSONstring, '', Constants.APPLICATION_NAME_BQ_INTEGRATION, null,  System.now(), System.now(), Constants.LOG_TYPE_JOB_LOG, true, true,   true, true);
                }
                else{
                	
                    GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_INFO, 'BQObjectBatch', 'Execute', '','', 'Success in sending records. Deletion has been disabled in settings. JSONData sent='+JSONstring, '', Constants.APPLICATION_NAME_BQ_INTEGRATION, null,  System.now(), System.now(), Constants.LOG_TYPE_JOB_LOG, true, true,   true, true);
                }
                
                
            }
        }
        catch(BQUtilities.DataSendFailureException ex){
            
            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_ERROR, 'BQObjectBatch', 'Execute', '','', 'Error while sending records. Error='+google.getResponse(), 'JSONData='+JSONstring, Constants.APPLICATION_NAME_BQ_INTEGRATION, ex,  System.now(), System.now(), Constants.LOG_TYPE_ERROR_LOG, true, true,   true, true);
        }
        catch(Exception ex){
			
			GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_ERROR, 'BQObjectBatch', 'Execute', '','', 'Exception occured in BQObjectBatch', 'JSONData='+JSONstring, Constants.APPLICATION_NAME_BQ_INTEGRATION, ex,  System.now(), System.now(), Constants.LOG_TYPE_ERROR_LOG, true, true,   true, true);            
        }
        
        System.debug('ta: A batch ends');
    }    
    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        System.debug('ta: Complete batch job ends');
    }    

}