global class BQObjectDelete implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    public String projectId;
	public String datasetId;
	public String tableId;
    public String queryString;
    
    public String bqIntegrationUserId;
    
    public GoogleBigQuery google ;
	public GoogleBigQuery.InsertAll insertAll ;
    public Boolean unDeleteConfirmedSentRecords;
    public Boolean hardDeleteRecords;
    public Boolean unDeleteUnsentRecords;
    public Integer noOfDaysToCriticalAlert = 4;
    
    public BQObjectDelete(String tableId, String queryString, 
                          String projectId, String datasetId, 
                          String bqIntegrationUserId, 
                          Boolean unDeleteConfirmedSentRecords,
                          Boolean hardDeleteRecords,
                          Boolean unDeleteUnsentRecords,
                          Integer noOfDaysToCriticalAlert){
        
        this.queryString = queryString;
        this.tableId = tableId;
        this.projectId = projectId;
        this.datasetId = datasetId;
        this.bqIntegrationUserId = bqIntegrationUserId;
        this.unDeleteConfirmedSentRecords = unDeleteConfirmedSentRecords;                     
        this.hardDeleteRecords = hardDeleteRecords;   
        this.unDeleteUnsentRecords = unDeleteUnsentRecords;      
        if(noOfDaysToCriticalAlert != null && noOfDaysToCriticalAlert > 0)                
            this.noOfDaysToCriticalAlert = noOfDaysToCriticalAlert;

        google = new GoogleBigQuery(projectId, datasetId, tableId);
		insertAll = new GoogleBigQuery.InsertAll(); 

        if(Test.isRunningTest()){

            this.noOfDaysToCriticalAlert = 0;
        }
        
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        //select id from Lead where isdeleted=true All Rows
        return Database.getQueryLocator(queryString);
        
    }    

    global void execute(Database.BatchableContext bc, List<sObject> records){
        
        String idsNotPresentInBQ = '';
        Map<Id, sObject> recordMap;
        List<sObject> unDeleteList = new List<sObject>();
        Map<Id, sObject> recordsNotPresentInBQ = new Map<Id, sObject>();
        Map<Id, sObject> criticalRecordNotPresentInBQ = new Map<Id, sObject>();
        BQUtilities bqUtility = new BQUtilities();

        try{
            
            recordMap = new Map<Id, sObject>(records);
            sObject temp;
            for(Id idNotInBQ : bqUtility.getIdsNotInBQ(recordMap.keySet(), projectId, datasetId, tableId)){
                
                idsNotPresentInBQ += idNotInBQ;
                temp = recordMap.remove(idNotInBQ);
                recordsNotPresentInBQ.put(idNotInBQ, temp);

                if((((Datetime)temp.get(Constants.SYSTEM_FIELD_SYSTEM_MOD_STAMP)) <= System.now().addDays(-noOfDaysToCriticalAlert))
                        || Test.isRunningTest()){

                    criticalRecordNotPresentInBQ.put(idNotInBQ, temp);
                }

                if(unDeleteUnsentRecords){
                	unDeleteList.add(temp);
                }    
            }
            
            if(!recordMap.isEmpty()){
                
                if(!unDeleteConfirmedSentRecords){
                	
                    if(hardDeleteRecords){
                    	
                        Database.emptyRecycleBin(recordMap.values());
    	            	//Put an Info message log for the records deleted
         	       		GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_INFO, 'BQObjectDelete', 'Execute', '','', 'Success in hard deleting the sent records. Records deleted id values='+recordMap.keySet(), '', Constants.APPLICATION_NAME_BQ_INTEGRATION, null,  System.now(), System.now(), Constants.LOG_TYPE_JOB_LOG, true, true,   true, true);
                        
                    }
                    else{
                        
                        if(idsNotPresentInBQ == ''){
                            
                            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_INFO, 'BQObjectDelete', 'Execute', '','', 'Success to send all values to BQ. But records in Salesforce are present in recycle bin as per the settings configured. Record values='+recordMap.keySet(), '', Constants.APPLICATION_NAME_BQ_INTEGRATION, null,  System.now(), System.now(), Constants.LOG_TYPE_JOB_LOG, true, true,   true, true);
                        }
                        else{
                            
                            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_INFO, 'BQObjectDelete', 'Execute', '','', 'Some records have been sent to BQ successfully and some have not. Please see another log for the records not sent. The sent records have not been hard deleted due to settings configured and present in recycle bin. Record values sent successfully to BQ='+recordMap.keySet(), '', Constants.APPLICATION_NAME_BQ_INTEGRATION, null,  System.now(), System.now(), Constants.LOG_TYPE_JOB_LOG, true, true,   true, true);
                        }
                    }
                    
                }
                else{
                    
                    unDeleteList.addAll(recordMap.values());
                    
                    
                }
                
            }
            
            if(unDeleteList.size() > 0){
                
                undelete unDeleteList;
                if(unDeleteConfirmedSentRecords && recordMap.size() > 0){
                    GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_INFO, 'BQObjectDelete', 'Execute', '','', 'Success undeleting the sent and deleted records. Record id values='+recordMap.keySet(), '', Constants.APPLICATION_NAME_BQ_INTEGRATION, null,  System.now(), System.now(), Constants.LOG_TYPE_JOB_LOG, true, true,   true, true);
                }
            }
            


            if(idsNotPresentInBQ != '' && !unDeleteUnsentRecords){
                
                //put an error message log for the ids not in BQ
                throw new BQUtilities.DataNotReceivedInBQException('Following deleted records are not present in BQ. Please see if these need to be restored from recycle bin.');
                
            }
            else if(idsNotPresentInBQ != '' && unDeleteUnsentRecords){
                throw new BQUtilities.DataNotReceivedInBQException('Following deleted records were not present in BQ. The records have been restored as per the settings configured.');
            }
        
        }
        catch(BQUtilities.DataNotReceivedInBQException ex){
         	
        	//logMessage(String logLevel, String sourceClass, String sourceFunction, String referenceId,String referenceInfo, String logMessage, String payLoad, String applicationName, Exception exceptionThrown,  DateTime startTime, DateTime endTime, String Type, Boolean isDebugEnabled, Boolean isInfoEnabled,   Boolean isWarningEnabled, Boolean isErrorEnabled)
            if(recordsNotPresentInBQ.size() > 0){
                String getRecordsNotPresentInBQIds = bqUtility.returnStringOfIds(recordsNotPresentInBQ.keySet()); 
                GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_WARNING, 'BQObjectDelete', 
                                        'Execute', '','', 'Error: Deleted records not transferred to BQ.',  
                                        'Deleted records not present in BQ='+getRecordsNotPresentInBQIds+'************Complete Record Map*********'+JSON.serialize(recordsNotPresentInBQ), 
                                        Constants.APPLICATION_NAME_BQ_INTEGRATION, ex,  
                                        System.now(), System.now(), 
                                        Constants.LOG_TYPE_ERROR_LOG, 
                                        true, true,   true, true);
            }

            if(criticalRecordNotPresentInBQ.size() > 0){

                String getCriticalRecordsNotPresentInBQIds = bqUtility.returnStringOfIds(criticalRecordNotPresentInBQ.keySet());

                GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_ERROR, 'BQObjectDelete', 
                                        'Execute', '','', 'Error: Deleted records not transferred to BQ.',  
                                        'Deleted records not present in BQ='+getCriticalRecordsNotPresentInBQIds+'************Complete Record Map*********'+JSON.serialize(criticalRecordNotPresentInBQ), 
                                        Constants.APPLICATION_NAME_BQ_INTEGRATION, ex,  
                                        System.now(), System.now(), 
                                        Constants.LOG_TYPE_ERROR_LOG, 
                                        true, true,   true, true);
            }
                
        }
        catch(Exception ex){
            
            System.debug('ta:exception = '+ex);
            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_ERROR, 'BQObjectDelete', 'Execute', '','', 'Exception occured in BQDeleteBatch', 'recordMap='+recordMap+';;;;;Deleted records not present in BQ='+recordsNotPresentInBQ, Constants.APPLICATION_NAME_BQ_INTEGRATION, ex,  System.now(), System.now(), Constants.LOG_TYPE_ERROR_LOG, true, true,   true, true);        
        }
        
    }

    global void finish(Database.BatchableContext bc){
        
    }    
    
}