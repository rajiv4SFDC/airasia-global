public class BQSendSchedule implements Schedulable{
    
	public void execute(SchedulableContext ctx) {
        
        //List below all the object send batches
        
        try{
		
            for(BQIntegration__mdt bqIntegrationRecord : [SELECT Batch_Size__c, Dataset_Id__c, 
                                                          Project_Id__c, Query_String__c, 
                                                          TableId__c, Type__c, Do_not_delete__c 
                                                          FROM BQIntegration__mdt 
                                                          WHERE 
                                                          Type__c = :Constants.BQ_INTEGRATION_CONFIG_TYPE_SEND
                                                          AND isActive__c = true]){
            
            		if(bqIntegrationRecord.Query_String__c != null && bqIntegrationRecord.Query_String__c != ''){
                                                                  
                    	Database.executeBatch(new BQObjectBatch(bqIntegrationRecord.TableId__c, 
                                                                bqIntegrationRecord.Query_String__c, 
                                                                bqIntegrationRecord.Project_Id__c,
                                                                bqIntegrationRecord.Dataset_Id__c,
                                                               bqIntegrationRecord.Do_not_delete__c), 
                                              					(Integer)bqIntegrationRecord.Batch_Size__c);    
                    }		                                                  
            }
        }
        catch(Exception ex){
            
            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_ERROR, 'BQSendSchedule', 'execute', '','', 'Exception occured in BQSendSchedule', '', 'BQIntegration', ex,  System.now(), System.now(), Constants.LOG_TYPE_ERROR_LOG, true, true,   true, true); 
        }
    }
}