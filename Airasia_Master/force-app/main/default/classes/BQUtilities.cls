public class BQUtilities {
    
    public Set<Id> getIdsNotInBQ(Set<Id> idsInSalesforce, String projectId, String datasetId, String tableId){
        
        Integer value = 0;
        GoogleBigQuery google = new GoogleBigQuery(projectId);
        GoogleBigQuery.JobsQuery query = new GoogleBigQuery.JobsQuery();
        String stringOfIds = returnStringOfIds(idsInSalesforce);
        
        Set<Id> idsNotPresentInBQ = new Set<Id>();
        Set<Id> idsInBQ = new Set<Id>();
        
        String idTemVar;
        query.query = 'SELECT Id FROM `'+projectId+'.'+datasetId+'.'+tableId+'` where Id in '+stringOfIds;
        if (!google.query(query)) {
            //ta:debugging
            System.debug('Error querying: ='+google.getResponse());
            throw new UnableToQueryFromBQException('Unable to query from google BQ. Error='+google.getResponse());
        }
        else{
            
            // Parse JSON response to get all the totalPrice field values.
            JSONParser parser = JSON.createParser(google.getResponse());
            while (parser.nextToken() != null) {
                
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                    (parser.getText() == 'v')) {
                        // Get the value.
                        parser.nextToken();
                        // Compute the grand total price for all invoices.
                        idTemVar = parser.getText();
                        idsInBQ.add(idTemVar);
                        
                    }
            }
            
            for(Id idInSalesforce : idsInSalesforce){
                
                if(!idsInBQ.contains(idInSalesforce)){
                    
                    idsNotPresentInBQ.add(idInSalesforce);
                }
            }
            
        }
        
        return idsNotPresentInBQ;
	}
    
    //returns ("id1","id2","id3")
    public String returnStringOfIds(Set<Id> records){
        
        String stringOfIds = '(';
        for(Id record : records){
            
            stringOfIds += '"'+record+'"'+',';
        }
        stringOfIds = stringOfIds.substring(0, stringOfIds.length()-1);
        stringOfIds += ')';
        
        return stringOfIds;
    }
    
    
    public class DataSendFailureException extends Exception{
        
    }
    
    public class DataNotReceivedInBQException extends Exception{
        
    }
    
    public class UnableToQueryFromBQException extends Exception{
        
    }
}