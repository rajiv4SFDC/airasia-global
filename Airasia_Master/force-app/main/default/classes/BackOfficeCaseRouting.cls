/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/06/2017      Pawan Kumar         Created
*******************************************************************/
public without sharing class BackOfficeCaseRouting {
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    private static String processLog = '';
    private static List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
    private static final String DELIMITER = '#~#';
    
    @InvocableMethod
    public static void assignCaseToRelevantQueue(List < Id > caseIds) {
        try {
            // get System Setting details for Logging
            logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Routing_Log_Level_CMD_NAME'));
            
            // Query Case for extra details
            List < Case > updateCaseList;
            List < Case > caseList = [Select Id, Priority, Transfer_to_Back_Office_Team__c from Case where Id IN: caseIds];
            
            if (!caseList.isEmpty()) {
                updateCaseList = new List < Case > ();
                
                Map < String, String > boCaseRoutingMap = new Map < String, String > ();
                List < Back_Office_Case_Routing__mdt > boCaseRoutingList = [Select Id, Case_Priority__c, Transfer_to_Back_Office_Team__c, Queue__c from Back_Office_Case_Routing__mdt];
                
                for (Back_Office_Case_Routing__mdt eachRow: boCaseRoutingList) {
                    boCaseRoutingMap.put(eachRow.Case_Priority__c + DELIMITER + eachRow.Transfer_to_Back_Office_Team__c, eachRow.Queue__c);
                }
                //System.debug('boCaseRoutingMap:'+JSON.serializePretty(boCaseRoutingMap));
                
                for (Case caseToBeAssignedToQueue: caseList) {
                    String caseId = caseToBeAssignedToQueue.id;
                    
                    try {
                        String criteriaKey = caseToBeAssignedToQueue.Priority + DELIMITER + caseToBeAssignedToQueue.Transfer_to_Back_Office_Team__c;
                        if (boCaseRoutingMap.containsKey(criteriaKey)) {
                            caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(boCaseRoutingMap.get(criteriaKey));
                        } else {
                            String soqlQuery = 'Select Id, Queue__c from Back_Office_Case_Routing__mdt' +
                                ' where Case_Priority__c=' + caseToBeAssignedToQueue.Priority +
                                ' AND Transfer_to_Back_Office_Team__c =' + caseToBeAssignedToQueue.Transfer_to_Back_Office_Team__c + '\r\n';
                            throw new QueryException(soqlQuery);
                        }
                        
                    } catch (Exception queueQueryEx) {
                        System.debug(LoggingLevel.ERROR, queueQueryEx);
                        
                        caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(Utilities.getAppSettingsMdtValueByKey('Case_Routing_Error_Queue'));
                        processLog += 'Failed to get QUEUE NAME from routing table: \r\n';
                        processLog += queueQueryEx.getMessage() + '\r\n';
                        
                        appWrapperLogList.add(Utilities.createApplicationLog('Warning', 'BackOfficeCaseRouting', 'assignCaseToRelevantQueue',
                                                                             caseId, 'Back Office Case Routing', processLog, null, 'Error Log', startTime, logLevelCMD, queueQueryEx));
                    }
                    
                    updateCaseList.add(caseToBeAssignedToQueue);
                }
                
                // update Case
                if (updateCaseList != null && !updateCaseList.isEmpty())
                    update updateCaseList;
            }
        } catch (Exception caseAssignFailedEx) {
            System.debug(LoggingLevel.ERROR, caseAssignFailedEx);
            
            String firstCaseId = '';
            if (caseIds != null && !caseIds.isEmpty()) {
                firstCaseId = caseIds[0];
            }
            
            System.debug(LoggingLevel.ERROR, caseAssignFailedEx);
            processLog += 'Failed to assign queue for the following Case Ids: \r\n';
            processLog += JSON.serializePretty(caseIds) + '\r\n';
            processLog += 'Root Cause: ' + caseAssignFailedEx.getMessage() + '\r\n';
            
            appWrapperLogList.add(Utilities.createApplicationLog('Info', 'BackOfficeCaseRouting', 'assignCaseToRelevantQueue',
                                                                 firstCaseId, 'Back Office Case Routing', processLog, null, 'Error Log', startTime, logLevelCMD, caseAssignFailedEx));
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
        } // finally
        
    }
}