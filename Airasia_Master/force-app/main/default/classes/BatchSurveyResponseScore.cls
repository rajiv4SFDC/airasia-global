/* Charles Thompson - 30 Jan 2019
 * To be run once only.  Updates the past Survey Responses to include a "Response Score"
 * If the response is Yes or Good, then update the Response Score to 1.
 * This will then be used in reporting to calculate percentages.
 */
global class BatchSurveyResponseScore implements Database.Batchable<sObject> {
    
    global List<SurveyQuestionResponse__c> rsToUpdate = new List<SurveyQuestionResponse__c>();
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
         
        String query = 'SELECT Id, '+
                       '       Response__c, '+
                       '       Response_Score__c '+
                       'FROM   SurveyQuestionResponse__c';
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<SurveyQuestionResponse__c> rs) {
        
        // process each batch of records
        for (SurveyQuestionResponse__c r: rs){
            if (r.Response__c.startsWith('Yes') ||
                r.Response__c.startsWith('Good')){
                    r.Response_Score__c = 1;
                    rsToUpdate.add(r);
                }
        }
        try {
            // Update the Account Record
            update rsToUpdate;
         
        } catch(Exception e) {
            System.debug(e);
        }
         
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
        System.debug('Survey Responses processed: '+ rsToUpdate.size());
  }
}