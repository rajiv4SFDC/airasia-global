@isTest 
public class BatchSurveyResponseScore_Test {

    static testMethod void testMethod1() 
    {

        Survey__c s = new Survey__c();
        s.CurrencyIsoCode = 'MYR';
        s.Name = 'Test Survey';
        s.OwnerId = System.UserInfo.getUserId();
        s.URL__c = 'http://example.com';
        insert s;
        
        Survey_Question__c sq = new Survey_Question__c();
        sq.CurrencyIsoCode = 'MYR';
        sq.OrderNumber__c = 1;
        sq.Question__c = 'Test question';
        sq.Survey__c = s.Id;
        insert sq;

		SurveyTaker__c st = new SurveyTaker__c();
        st.CurrencyIsoCode = 'MYR';
		st.OwnerId = System.UserInfo.getUserId();
        insert st;
        
        List<SurveyQuestionResponse__c> rs = new List<SurveyQuestionResponse__c>();
        for(Integer i = 0; i < 200; i++){
            SurveyQuestionResponse__c r = new SurveyQuestionResponse__c();
            r.CurrencyIsoCode = 'MYR';
            r.Survey_Question__c = sq.Id;
            r.SurveyTaker__c = st.Id;
            if (Math.mod(i, 2) == 0){
	            r.Response__c = 'Yes';
            } else {
                r.Response__c = 'No';
            }
            rs.add(r);
        }
        
        insert rs;
        
        Test.startTest();

            BatchSurveyResponseScore bsrs = new BatchSurveyResponseScore();
            Database.executeBatch(bsrs);            

        Test.stopTest();
    }
}