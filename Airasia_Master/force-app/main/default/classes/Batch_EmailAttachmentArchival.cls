/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created
09/01/2019      Charles Thompson    Fixed mistaken use of DateTime.now() by 
                                    creating prepQuery from code in the Schedulable class
*******************************************************************/
global class Batch_EmailAttachmentArchival 
			 implements Database.Batchable<sObject>, Database.Stateful {
    global String metadata_label_name = 'Email Attachment Archival Batch';
    global String sobj_api_name;
    global String additionalFilter;
    global Boolean isPermDel = false;
    global Integer batchSize = 200;
    global Integer totalRecords = 0, errorRecords = 0, succesfulRecords = 0;
    global DateTime startTime = DateTime.now();
    global String query;
    global String processLog = '';
    global System_Settings__mdt sysSettingMD;
    global List < String > failureRecords;
    global String JobName = 'EmailAttachmentArchivalBatch';
    
    global Batch_EmailAttachmentArchival(String query, System_Settings__mdt sysSettingMD) {
        this.query = query;
        this.sysSettingMD = sysSettingMD;
        
        failureRecords = new List < String > ();
        System.debug(LoggingLevel.INFO, 'query: ' + query);
        processLog += 'query: ' + query + '\r\n';
    }
    
    global void prepQuery(){
        // CT 09 Jan 2019 - copied from Schedular_EmailAttachmentArchival
        
        // find record ages from setting
        List <System_Settings__mdt> sysSettingMDlist = [SELECT Id, 
                                                        Date_Field_API_Name__c,  
                                                        Additional_Filter__c,  
                                                        Batch_Size__c,  
                                                        Log_Purge_Days__c,  
                                                        Is_Permanent_Delete__c,
                                                        SObject_Api_Name__c,  
                                                        Debug__c,  
                                                        Info__c,  
                                                        Warning__c,  
                                                        Error__c 
                                                        FROM   System_Settings__mdt
                                                        WHERE  MasterLabel = :metadata_label_name 
                                                        LIMIT  1
                                                       ];
        String dateTime_field = '';
        String parentQuery = '';
        if (sysSettingMDlist.size() > 0) {
            if (Integer.valueOf(sysSettingMDlist[0].Log_Purge_Days__c) > 0) {
                DateTime closeDateCriteria = 
                    (startTime - Integer.valueof(sysSettingMDlist[0].Log_Purge_Days__c));
                String closeDateCriteriaStr = 
                    closeDateCriteria.formatGMT('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                
                // Replace time with 00:00:00
                String[] closeDateCriteriaStrArr = closeDateCriteriaStr.split('T');
                closeDateCriteriaStr = closeDateCriteriaStrArr[0] + 'T00:00:00Z';
                System.debug(closeDateCriteriaStr);
                
                dateTime_field = sysSettingMDlist[0].Date_Field_API_Name__c;
                isPermDel = sysSettingMDlist[0].Is_Permanent_Delete__c;
                if (sysSettingMDlist[0].Batch_Size__c>0)
                    batchSize = Integer.valueof(sysSettingMDlist[0].Batch_Size__c);
                sobj_api_name = sysSettingMDlist[0].SObject_Api_Name__c;
                
                // build parent query
                parentQuery = 'SELECT Id FROM ' + sobj_api_name + 
                    ' WHERE ' + dateTime_field + ' <= ' + closeDateCriteriaStr;
                
                // add any additional query.
                additionalFilter = sysSettingMDlist[0].Additional_Filter__c;
                if (String.isNotBlank(additionalFilter)) {
                    parentQuery = parentQuery + 
                        ' AND ' + additionalFilter;
                }
            }
        } else {
            parentQuery = '';
        }
        
        System.debug('*** parentQuery = ' + parentQuery);
        
        // finally call batch   
        ID BatchId = Database.executeBatch(
            new Batch_EmailAttachmentArchival(parentQuery, sysSettingMDlist[0]), batchSize);
        System.debug('Batch ID:' + BatchId);
    } // END - prepQuery()
    
    // gets all processing record.
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    } // END- start
    
    global void execute(Database.BatchableContext bc, List < Case > scope) {
        System.debug('execute() - START');
        List < Attachment > attachmentToBeDeletedId = new List < Attachment > ();
        for (Attachment att: [SELECT Id FROM Attachment WHERE Parentid IN(SELECT Id FROM EmailMessage WHERE ParentId IN: scope)]) {
            attachmentToBeDeletedId.add(att);
            totalRecords++;
        }
        
        if (attachmentToBeDeletedId != null && !attachmentToBeDeletedId.isEmpty()) {
            
            // below code just to cover error block - delete
            if (Test.isRunningTest()) {
                Attachment dummyAttach = new Attachment(Id = '00P0l000000FKZ2');
                dummyAttach.Name = 'Already Deleted Record';
                attachmentToBeDeletedId.add(dummyAttach);
            }
            
            // soft delete records
            Database.DeleteResult[] srDeleteList = Database.delete(attachmentToBeDeletedId, false);
            for (Integer i = 0; i < srDeleteList.size(); i++) {
                if (!srDeleteList[i].isSuccess()) {
                    for (Database.Error err: srDeleteList[i].getErrors()) {
                        processLog += 'Unable to Delete Record ID: ' + attachmentToBeDeletedId[i].Id + '\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += 'Record fields that affected this error: ' + err.getFields() + '\r\n';
                        
                        // add to failure List
                        errorRecords++;
                        failureRecords.add(attachmentToBeDeletedId[i].Id);
                    }
                } else {
                    succesfulRecords++;
                }
            }
            
            // below code just to cover error block - delete
            if (Test.isRunningTest()) {
                Attachment dummyAttach = new Attachment(Id = '00P0l000000FKZ2');
                dummyAttach.Name = 'Already Deleted Record from Recycle BIN';
                attachmentToBeDeletedId.add(dummyAttach);
            }
            
            // Hard delete records from recycle bin
            if (sysSettingMD.Is_Permanent_Delete__c) {
                Database.EmptyRecycleBinResult[] srEmptyRecBinList = Database.emptyRecycleBin(attachmentToBeDeletedId);
                
                for (Integer i = 0; i < srEmptyRecBinList.size(); i++) {
                    if (!srEmptyRecBinList[i].isSuccess()) {
                        for (Database.Error err: srEmptyRecBinList[i].getErrors()) {
                            processLog += 'Unable to Permanently Delete Record ID: ' + attachmentToBeDeletedId[i].Id + '\r\n';
                            processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                            processLog += 'Record fields that affected this error: ' + err.getFields() + '\r\n';
                        }
                    } // end if
                } // for outer
            } // end if 
            
        } // end outer if
        System.debug('execute() - END');
    } // END- execute()
    
    global void finish(Database.BatchableContext bc) {
        System.debug('finish() - START');
        
        // query job details
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = : bc.getJobId()];
        processLog = 'Job Id: ' + job.Id + '; Job Status: ' + job.Status + '; Total Job Items: ' + job.TotalJobItems +'\r\n'+
            '------------------------------------------------\r\n' + processLog;
        // to avoid null print
        String failuerIdsStr = (failureRecords != null && failureRecords.size() > 0) ? string.join(failureRecords, ',') : 'NONE';
        
        processLog = '################################################\r\n' +
            'Job Name: ' + JobName + '\r\n' +
            'Start Date Time: ' + string.valueof(startTime) + '\r\n' +
            'End Date Time: ' + string.valueof(DateTime.Now()) + '\r\n' +
            'Total Processed Records: ' + totalRecords + '\r\n' +
            'Total Successful Records: ' + succesfulRecords + '\r\n' +
            'Total Failed Records: ' + errorRecords + '\r\n' +
            'List of Failed Records: ' + failuerIdsStr + '\r\n' +
            '################################################\r\n' + processLog;
        
        // add application log
        boolean isError = false;
        if (errorRecords > 0) {
            isError = true;
        }
        insertApplicationLog(bc.getJobId(), isError);
        
        System.debug('finish() - END');
        
    } // END - finish
    
    private void insertApplicationLog(String jobId, boolean isError) {
        
        ApplicationLogWrapper appLogWrapper = new ApplicationLogWrapper();
        appLogWrapper.logLevel = 'Info';
        
        if (isError)
            appLogWrapper.logLevel = 'Error';
        
        appLogWrapper.sourceClass = 'Batch_EmailAttachmentArchival';
        appLogWrapper.sourceFunction = 'finish(Database.BatchableContext bc)';
        appLogWrapper.referenceInfo = 'Apex Batch: Job Id: ' + jobId;
        appLogWrapper.logMessage = processLog;
        appLogWrapper.applicationName = 'Email Attachment Archival Batch';
        appLogWrapper.exceptionThrown = null;
        appLogWrapper.startDate = startTime;
        appLogWrapper.endDate = DateTime.now();
        appLogWrapper.type = 'Job Log';
        
        // Set Log Level
        appLogWrapper.isDebug = sysSettingMD.Debug__c;
        appLogWrapper.isInfo = sysSettingMD.Info__c;
        appLogWrapper.isError = sysSettingMD.Error__c;
        appLogWrapper.isWarning = sysSettingMD.Warning__c;
        
        GlobalUtility.logMessage(appLogWrapper);
    }
}