/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created

20/08/2018      Charles Thompson    1. Reduced the number of cases created, 
                                       so that SOQL limits are maintained
                                    2. Added case disposition to match validation rule
09/01/2019      Charles Thompson    Updated to reflect updated picklist values
*******************************************************************/
@isTest(SeeAllData = false)
private class Batch_EmailAttachmentArchival_Test {

    private static String moduleName='Batch_EmailAttachmentArchival';
    
    /** when case exist with Email Attachment**/
    static testMethod void testPurgeCaseEmailWithAttachment() {
        
        //case data
        Map<String, Object> caseFieldValueMap = new Map<String, Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Type', 'Complaint');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Origin', 'Twitter');
        TestUtility.createCases(caseFieldValueMap, 3);
        
        List<String> caseQueryFieldsList = new List<String>{
            'Subject',
            'Type',
            'Priority',
            'Origin'
        };
                    
        List<Case> caseCreatedList = (List<Case>) TestUtility.getCases(caseQueryFieldsList);
        System.assertEquals(3, caseCreatedList.size());
        
        // creating email message
        List<Map<String, Object>> emailMsgList = new List<Map<String, Object>> ();
        for (Case eachCase: caseCreatedList) {
            Map<String, Object> emailMsgFieldValueMap = new Map<String, Object>();
            emailMsgFieldValueMap.put('ParentId', eachCase.Id);
            emailMsgList.add(emailMsgFieldValueMap);
        }
        for (Map<String, Object> eachEmailMsgMap: emailMsgList) {
            TestUtility.createEmailMessages(eachEmailMsgMap, 1);
        }
        
        List<String> emQueryFieldsList = new List<String>{
            'ParentId'
        };
                    
        List<EmailMessage> emCreatedList = (List<EmailMessage>) TestUtility.getEmailMessages(emQueryFieldsList);
        System.assertEquals(3, emCreatedList.size());
        
        // attachment added to email
        List <Map<String, Object>> attachmentList = new List<Map<String, Object>>();
        for (EmailMessage eachEmailMsg: emCreatedList) {
            Map<String, Object> attchFieldValueMap = new Map<String, Object>();
            attchFieldValueMap.put('Name', 'test attachment' + eachEmailMsg.id);
            attchFieldValueMap.put('body', blob.valueof('attachment body'));
            attchFieldValueMap.put('ParentId', eachEmailMsg.Id);
            attachmentList.add(attchFieldValueMap);
        }
        for (Map<String, Object> eachAttachMap: attachmentList) {
            TestUtility.createAttachments(eachAttachMap, 1);
        }
        
        List<Attachment> attachCreatedList = (List<Attachment>) TestUtility.getAttachments();
        System.assertEquals(3, attachCreatedList.size());
        
        // now close case
        List <Case> updateCaseList = new List<Case>();
        for (Case eachCase: caseCreatedList) {
            eachCase.Status = 'Closed';
            eachCase.Type = 'Complaint';
            eachCase.Sub_Category_1__c = 'Service';
            eachCase.Sub_Category_2__c = 'Staff';
            eachCase.Disposition_Level_1__c = 'Sales'; // 20 Aug 18 - CThompson
            updateCaseList.add(eachCase);
        }
        update updateCaseList;
        
        // build query
        DateTime StartTime = DateTime.now();
        List<System_Settings__mdt> sysSettingMD = 
            [SELECT Id, 
                    Label, 
                    Date_Field_API_Name__c, 
                    Is_Permanent_Delete__c, 
             		Additional_Filter__c,
                    Batch_Size__c, 
             		SObject_Api_Name__c, 
             		Debug__c, 
             		Info__c, 
             		Warning__c, 
             		Error__c 
             FROM   System_Settings__mdt
             WHERE  MasterLabel = 'Email Attachment Archival Batch'
             LIMIT  1
            ];
        
        // set close date to future to pick up above test data   
        DateTime closeDateCriteria = StartTime + 1;
        String closeDateCriteriaStr = closeDateCriteria.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String dateTime_field = sysSettingMD[0].Date_Field_API_Name__c;
        String sobj_api_name = sysSettingMD[0].SObject_Api_Name__c;
        Boolean isPerDel = sysSettingMD[0].Is_Permanent_Delete__c;
        String parentQuery = 'SELECT Id FROM ' + sobj_api_name + 
                            ' WHERE ' + dateTime_field + ' <= ' + closeDateCriteriaStr;
        
        // call batch
        Test.startTest();
	        Database.executeBatch(new Batch_EmailAttachmentArchival(parentQuery, sysSettingMD[0]), 10);
        Test.stopTest();
        
        // making sure attachment deleted
        List<Attachment> attachAfterPurgeList = (List<Attachment>) TestUtility.getAttachments();
        System.assertNotEquals(null, attachAfterPurgeList);
        System.assertEquals(0, attachAfterPurgeList.size());
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName
                                           ];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
    
    /** when case does not have any Email Attachment **/
    static testMethod void testPurgeCaseEmailWithoutAttachment() {
        
        //create case test data
        Map<String, Object> caseFieldValueMap = new Map<String, Object> ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Type', 'Complaint');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Origin', 'Twitter');
        TestUtility.createCases(caseFieldValueMap, 3);
        
        List<String> caseQueryFieldsList = new List<String> {
            'Subject',
            'Type',
            'Priority',
            'Origin'
        };
                    
        List<Case> caseCreatedList = (List<Case>) TestUtility.getCases(caseQueryFieldsList);
        System.assertEquals(3, caseCreatedList.size());
        
        // creating email message
        List<Map<String, Object>> emailMsgList = new List<Map<String, Object>> ();
        for (Case eachCase: caseCreatedList) {
            Map<String, Object> emailMsgFieldValueMap = new Map<String, Object> ();
            emailMsgFieldValueMap.put('ParentId', eachCase.Id);
            emailMsgList.add(emailMsgFieldValueMap);
        }
        for (Map<String, Object> eachEmailMsgMap: emailMsgList) {
            TestUtility.createEmailMessages(eachEmailMsgMap, 1);
        }
        
        List<String> emQueryFieldsList = new List<String> {
            'ParentId'
        };
                    
        List<EmailMessage> emCreatedList = (List<EmailMessage> ) TestUtility.getEmailMessages(emQueryFieldsList);
        System.assertEquals(3, emCreatedList.size());
        
        // making sure there is no attachment
        List<Attachment> attachCreatedList = (List<Attachment> ) TestUtility.getAttachments();
        System.assertEquals(0, attachCreatedList.size());
        
        // closing case
        List<Case> updateCaseList = new List<Case> ();
        for (Case eachCase: caseCreatedList) {
            eachCase.Status = 'Closed';
            eachCase.Type = 'Complaint';
            eachCase.Sub_Category_1__c = 'Service';
            eachCase.Sub_Category_2__c = 'Staff';
            eachCase.Disposition_Level_1__c = 'Sales'; // 20 Aug 18 - CThompson
            updateCaseList.add(eachCase);
        }
        update updateCaseList;
        
        // No attachment
        List<Attachment> attachList = (List<Attachment>) TestUtility.getAttachments();
        System.assertNotEquals(null, attachList);
        System.assertEquals(0, attachList.size());
        
        // building query
        DateTime StartTime = DateTime.now();
        List<System_Settings__mdt> sysSettingMD = [SELECT Id, 
                                                          Label,  
                                                          Date_Field_API_Name__c,  
                                                          Is_Permanent_Delete__c,  
                                                          Additional_Filter__c,
                                                          Batch_Size__c,  
                                                          SObject_Api_Name__c,  
                                                          Debug__c,  
                                                          Info__c,  
                                                          Warning__c,  
                                                          Error__c  
                                                   FROM   System_Settings__mdt
                                                   WHERE  MasterLabel = :'Email Attachment Archival Batch'
                                                   LIMIT  1
                                                  ];
        
        // set close date to future to pick up above test data   
        DateTime closeDateCriteria = StartTime + 1;
        String closeDateCriteriaStr = closeDateCriteria.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String dateTime_field = sysSettingMD[0].Date_Field_API_Name__c;
        String sobj_api_name = sysSettingMD[0].SObject_Api_Name__c;
        Boolean isPerDel = sysSettingMD[0].Is_Permanent_Delete__c;
        String parentQuery = 'SELECT Id FROM ' + sobj_api_name + 
                            ' WHERE ' + dateTime_field + ' <= ' + closeDateCriteriaStr;

        // calling batch    
        Test.startTest();
        Database.executeBatch(new Batch_EmailAttachmentArchival(parentQuery, sysSettingMD[0]), 10);
        Test.stopTest();
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName
                                           ];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
    
    /** Case when Email Message itself is not there **/
    static testMethod void testPurgeWhenCaseWithoutEmail() {
        
        //create case test data
        Map<String, Object> caseFieldValueMap = new Map<String, Object> ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Type', 'Complaint');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Origin', 'Twitter');
        TestUtility.createCases(caseFieldValueMap, 10);
        
        List<String> caseQueryFieldsList = new List<String> {
            'Subject',
            'Type',
            'Priority',
            'Origin'
        };
                    
        List<Case> caseCreatedList = (List<Case>) TestUtility.getCases(caseQueryFieldsList);
        System.assertEquals(10, caseCreatedList.size());
        
        List<String> emQueryFieldsList = new List<String> {
            'ParentId'
        };
        
        // making sure there is no email    
        List<EmailMessage> emCreatedList = (List<EmailMessage>) TestUtility.getEmailMessages(emQueryFieldsList);
        System.assertNotEquals(null, emCreatedList);
        System.assertEquals(0, emCreatedList.size());
        
        // making sure there is no attachment
        List<Attachment> attachList = (List<Attachment>) TestUtility.getAttachments();
        System.assertNotEquals(null, attachList);
        System.assertEquals(0, attachList.size());
        
        // closing case.
        List<Case> updateCaseList = new List<Case> ();
        for (Case eachCase: caseCreatedList) {
            eachCase.Status = 'Closed';
            eachCase.Type = 'Complaint';
            eachCase.Sub_Category_1__c = 'Service';
            eachCase.Sub_Category_2__c = 'Staff';
            eachCase.Disposition_Level_1__c = 'Sales'; // 20 Aug 18 - CThompson
            updateCaseList.add(eachCase);
        }
        update updateCaseList;
        
        // building query
        DateTime StartTime = DateTime.now();
        List<System_Settings__mdt> sysSettingMD = [SELECT Id,  
                                                          Label,  
                                                          Date_Field_API_Name__c,  
                                                          Is_Permanent_Delete__c,  
                                                          Additional_Filter__c,
                                                          Batch_Size__c,  
                                                          SObject_Api_Name__c,  
                                                          Debug__c,  
                                                          Info__c,  
                                                          Warning__c,  
                                                          Error__c 
                                                   FROM   System_Settings__mdt
                                                   WHERE  MasterLabel = :'Email Attachment Archival Batch'
                                                   LIMIT 1
                                                  ];
        
        // set close date to future to pick up above test data   
        DateTime closeDateCriteria = StartTime + 1;
        String closeDateCriteriaStr = closeDateCriteria.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String dateTime_field = sysSettingMD[0].Date_Field_API_Name__c;
        String sobj_api_name = sysSettingMD[0].SObject_Api_Name__c;
        Boolean isPerDel = sysSettingMD[0].Is_Permanent_Delete__c;
        String parentQuery = 'SELECT Id FROM ' + sobj_api_name + 
                            ' WHERE ' + dateTime_field + ' <= ' + closeDateCriteriaStr;

        // calling batch        
        Test.startTest();
        Database.executeBatch(new Batch_EmailAttachmentArchival(parentQuery, sysSettingMD[0]), 10);
        Test.stopTest();
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [Select Id from Application_Log__c where Module__c=:moduleName];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
    
    /** Making sure without case : batch runs without any exception**/
    static testMethod void testPurgeWithoutCase() {
        
        //making sure there is no case, Email Message, Attachments
        List<Case> caseCreatedList = (List<Case> ) TestUtility.getCases();
        System.assertNotEquals(null, caseCreatedList);
        System.assertEquals(0, caseCreatedList.size());
        
        List<String> emQueryFieldsList = new List<String> {'ParentId'};
        List<EmailMessage> emCreatedList = 
            (List<EmailMessage> ) TestUtility.getEmailMessages(emQueryFieldsList);
        System.assertNotEquals(null, emCreatedList);
        System.assertEquals(0, emCreatedList.size());
        
        List<Attachment> attachList = (List<Attachment> ) TestUtility.getAttachments();
        System.assertNotEquals(null, attachList);
        System.assertEquals(0, attachList.size());
        
        // building query
        DateTime StartTime = DateTime.now();
        List<System_Settings__mdt> sysSettingMD = 
            [SELECT Id, Label, Date_Field_API_Name__c, Is_Permanent_Delete__c, 
             		Additional_Filter__c, Batch_Size__c, SObject_Api_Name__c, 
             		Debug__c, Info__c, Warning__c, Error__c 
             FROM   System_Settings__mdt
             Where  MasterLabel = 'Email Attachment Archival Batch'
             LIMIT  1
            ];
        
        // set close date to future to pick up above test data   
        DateTime closeDateCriteria = StartTime + 1;
        String closeDateCriteriaStr = closeDateCriteria.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String dateTime_field = sysSettingMD[0].Date_Field_API_Name__c;
        String sobj_api_name = sysSettingMD[0].SObject_Api_Name__c;
        Boolean isPerDel = sysSettingMD[0].Is_Permanent_Delete__c;
        String parentQuery = 'SELECT Id FROM ' + sobj_api_name + 
                            ' WHERE ' + dateTime_field + ' <= ' + closeDateCriteriaStr;

        // calling batch
        Test.startTest();        
        Database.executeBatch(new Batch_EmailAttachmentArchival(parentQuery, sysSettingMD[0]), 10);
        Test.stopTest();
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
}