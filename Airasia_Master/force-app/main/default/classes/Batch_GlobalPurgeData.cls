/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created
*******************************************************************/
global class Batch_GlobalPurgeData
implements Database.Batchable<sObject>, Database.Stateful {
    global String query;
    global Integer totalRecords = 0;
    global Integer succesfulRecords = 0;
    global Integer errorRecords = 0;
    global String processLog = '';
    global Datetime StartTime = DateTime.now();
    global System_Settings__mdt sysSettingMD;
    global List<String> failureRecords;
    global String JobName = 'GlobalPurgeDataBatch';
    
    global Batch_GlobalPurgeData(String query,System_Settings__mdt sysSettingMD) {
        this.query = query;
        this.sysSettingMD = sysSettingMD;
        
        failureRecords = new List < String > ();
        System.debug(LoggingLevel.INFO, 'query: ' + query);
        processLog += 'query: ' + query + '\r\n';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List < SObject > records) {
        System.debug('execute() - START');
        
        // total records
        totalRecords += records.size();
        
        // soft delete records
        Database.DeleteResult[] srDeleteList = Database.delete(records, false);
        for (Integer i = 0; i < srDeleteList.size(); i++) {
            if (!srDeleteList[i].isSuccess()) {
                for (Database.Error err: srDeleteList[i].getErrors()) {
                    processLog += 'Unable to Delete Record ID: ' + records[i].Id + '\r\n';
                    processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                    processLog += 'Record fields that affected this error: ' + err.getFields() + '\r\n';
                    errorRecords++;
                    failureRecords.add(records[i].Id);
                }
            } else {
                succesfulRecords++;
            }
        }
        
        // Hard delete records from recycle bin
        if (sysSettingMD.Is_Permanent_Delete__c) {
            Database.EmptyRecycleBinResult[] srEmptyRecBinList = Database.emptyRecycleBin(records);
            
            for (Integer i = 0; i < srEmptyRecBinList.size(); i++) {
                if (!srEmptyRecBinList[i].isSuccess()) {
                    for (Database.Error err: srEmptyRecBinList[i].getErrors()) {
                        processLog += 'Unable to Permanently Delete Record ID: ' + records[i].Id + '\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += 'Record fields that affected this error: ' + err.getFields() + '\r\n';
                    }
                } // end if
            } // for outer
        } // end if 
        
        System.debug('execute() - END');
    }
    
    global void finish(Database.BatchableContext bc) {
        System.debug('finish() - START');
        
        // query job details
        AsyncApexJob job = [SELECT Id, 
                                   Status, 
                                   NumberOfErrors, 
                                   JobItemsProcessed, 
                                   TotalJobItems, 
                                   CreatedBy.Email 
                            FROM   AsyncApexJob 
                            WHERE  Id = :bc.getJobId()
                           ];
        processLog = 'Job Id: ' + job.Id + 
                     '; Job Status: ' + job.Status + 
                     '; Total Job Items: ' + job.TotalJobItems + 
                     '\r\n' + '---\r\n' + processLog;
        
        // to avoid null print
        String failureIdsStr = (failureRecords != null && 
                                failureRecords.size() > 0) ? 
                                    String.join(failureRecords, ',') : 
                                    'NONE';
        
        processLog = 
            'Job Name: ' + JobName + '\r\n' +
            'Start Date Time: ' + string.valueof(StartTime) + '\r\n' +
            'End Date Time: ' + string.valueof(DateTime.Now()) + '\r\n' +
            'Total Processed Records: ' + totalRecords + '\r\n' +
            'Total Successful Records: ' + succesfulRecords + '\r\n' +
            'Total Failed Records: ' + errorRecords + '\r\n' +
            'List of Failed Records: ' + failureIdsStr + '\r\n' +
            processLog;
        
        // add application log
        boolean isError = false;
        if (errorRecords > 0) {
            isError = true;
        }
        insertApplicationLog(bc.getJobId(), isError);
        
        System.debug('finish() - END');
    }
    
    private void insertApplicationLog(String jobId, boolean isError) {
        
        ApplicationLogWrapper appLogWrapper = new ApplicationLogWrapper();
        appLogWrapper.logLevel = 'Info';
        
        if (isError)
            appLogWrapper.logLevel = 'Error';
        
        appLogWrapper.sourceClass = 'Batch_GlobalPurgeData';
        appLogWrapper.sourceFunction = 'finish(Database.BatchableContext bc)';
        appLogWrapper.referenceInfo = 'Apex Batch: Job Id: ' + jobId;
        appLogWrapper.logMessage = processLog;
        appLogWrapper.applicationName = 'GlobalPurgeBatch';
        appLogWrapper.exceptionThrown = null;
        appLogWrapper.startDate = StartTime;
        appLogWrapper.endDate = DateTime.now();
        appLogWrapper.type = 'Job Log';
        
        // Set Log Level
        appLogWrapper.isDebug = sysSettingMD.Debug__c;
        appLogWrapper.isInfo = sysSettingMD.Info__c;
        appLogWrapper.isError = sysSettingMD.Error__c;
        appLogWrapper.isWarning = sysSettingMD.Warning__c;
        
        GlobalUtility.logMessage(appLogWrapper);
    }
}