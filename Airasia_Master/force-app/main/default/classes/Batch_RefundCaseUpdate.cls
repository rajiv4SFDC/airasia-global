/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created
*******************************************************************/
global class Batch_RefundCaseUpdate implements
Database.Batchable < AggregateResult > , Database.Stateful {
    // instance member to retain state across transactions
    global Integer totalRecord = 0, errorRecord = 0, succesfulRecord = 0;
    global DateTime StartTime = DateTime.now();
    public String query;
    global String processLog = '';
    global System_Settings__mdt sysSettingMD;
    global String DELIMITER = ',';
    global List < String > failureRecords;
    global String JobName = 'RefundCaseUpdateBatch';
    global Map < String, String > appSettings;
    global String refundCompletedQueueId = '';        
    
    // constructor
    global Batch_RefundCaseUpdate(String query, System_Settings__mdt sysSettingMD) {
        this.query = query;
        this.sysSettingMD = sysSettingMD;
        
        appSettings = Utilities.getAppSettings();
        refundCompletedQueueId = Utilities.getQueueIdByName(appSettings.get('REFUND_COMPLETED_QUEUE_NAME'));
        
        failureRecords = new List < String > ();
        System.debug(LoggingLevel.INFO, 'query: ' + query);
        processLog += 'query: ' + query + '\r\n';
    }
    
    global Iterable < AggregateResult > start(Database.BatchableContext bc) {
        return new AggregateResultIterable(query);
    } // END- start
    
    
    global void execute(Database.BatchableContext bc, List < AggregateResult > scope) {
        System.debug('execute() - START');
        System.debug('scope:'+scope);        
        
        Map < String, String > caseWithStatusMap = new Map < String, String > ();
        Map < String, String > caseWithCaseIdMap = new Map < String, String > ();
        Map < String, String > caseWithDeclineCodeMap = new Map < String, String > ();
        Map < String, String > parentCaseWithFailedValidationMap = new Map < String, String > ();
        
        List < String > parentCaseList = new List < String > ();
        List < Case > updateParentCaseList = new List < Case > ();
        
        // build parent case id list to query child case
        for (AggregateResult arCase: scope) {
            String caseParentId = ((String) arCase.get('ParentId'));
            if (String.isNotEmpty(caseParentId)){
                parentCaseList.add(caseParentId);
            }
            
        }
        System.debug('parentCaseList:'+parentCaseList);
        
        for (Case eachCase: [Select Id, Status, Refund_Status__c,Declined_Reason_Code__c from Case where Id IN: parentCaseList]) {
            parentCaseWithFailedValidationMap.put(eachCase.Id,eachCase.Declined_Reason_Code__c);    
        }
        
        
        // query Child Case
        for (Case eachCase: [Select Id, Status, ParentId, Refund_Status__c,Declined_Reason_Code__c from Case where ParentId IN: parentCaseList]) {
            if (caseWithStatusMap.containsKey(eachCase.ParentId)) {
                caseWithStatusMap.put(eachCase.ParentId, caseWithStatusMap.get(eachCase.ParentId) + DELIMITER + eachCase.Refund_Status__c);
                caseWithCaseIdMap.put(eachCase.ParentId, caseWithCaseIdMap.get(eachCase.ParentId) + DELIMITER + eachCase.Id);
                caseWithDeclineCodeMap.put(eachCase.ParentId, caseWithDeclineCodeMap.get(eachCase.ParentId) + DELIMITER + eachCase.Declined_Reason_Code__c);
            } else {
                caseWithStatusMap.put(eachCase.ParentId, eachCase.Refund_Status__c);
                caseWithCaseIdMap.put(eachCase.ParentId, eachCase.Id);
                caseWithDeclineCodeMap.put(eachCase.ParentId,eachCase.Declined_Reason_Code__c);
            }
        } // end for
        System.debug('caseWithStatusMap:'+caseWithStatusMap);
        
        // check all child case refund status='Posted'
        for (String key: caseWithStatusMap.keySet()) {
            try{
            String caseStatusConcatValues = caseWithStatusMap.get(key);
            
            if(caseStatusConcatValues!=null){
            String[] caseStatusConcatValuesArr = caseStatusConcatValues.split(DELIMITER);
            
            System.debug('Parent Case ID '+key);
            System.debug('Child Case Status '+caseStatusConcatValues);
            
            Boolean isAllPosted = true;
            Boolean isAllValidated = true;
            Boolean isAllFailedValidation = true;
            Boolean isAllContainsDeclineCodeFG = true;
            Boolean isAllChildHasSameDeclineCodeAsParent = true;
            
            for (String caseStatus: caseStatusConcatValuesArr) {
                if (!caseStatus.equals('Posted')) {
                    isAllPosted = false;
                }
                
                if(!caseStatus.equals('Validated')){
                    isAllValidated = false;
                }
                
                if(!caseStatus.equals('Failed Validation')){
                    isAllFailedValidation = false;
                }
            }
            
            // all child contains decline code 'FG_'
            if(isAllFailedValidation){
                String caseDeclinedCodeConcatValues = caseWithDeclineCodeMap.get(key);
                if(caseDeclinedCodeConcatValues!=null){
                    String[] caseDeclinedCodeConcatValuesArr = caseDeclinedCodeConcatValues.split(DELIMITER);
                    for (String caseDeclineCode: caseDeclinedCodeConcatValuesArr) {
                        if(!caseDeclineCode.startsWithIgnoreCase(appSettings.get('CASE_REFUND_DECLINE_REASON_CODE'))){
                            isAllContainsDeclineCodeFG = false;
                        }
                    }
                }else{
                    isAllContainsDeclineCodeFG = false;
                }
                // END
            }
            
            // is all child has same decline code as Parent?
            String caseDeclinedCodeConcatValues = caseWithDeclineCodeMap.get(key);
            if(caseDeclinedCodeConcatValues!=null && parentCaseWithFailedValidationMap.containsKey(key)){
            String[] caseDeclinedCodeConcatValuesArr = caseDeclinedCodeConcatValues.split(DELIMITER);
            //if(parentCaseWithFailedValidationMap.containsKey(key)){
                for (String caseDeclineCode: caseDeclinedCodeConcatValuesArr) {
                    if(!caseDeclineCode.equalsIgnoreCase(parentCaseWithFailedValidationMap.get(key))){
                        isAllChildHasSameDeclineCodeAsParent = false;
                    }
                }
            //}else{
            //    isAllChildHasSameDeclineCodeAsParent = false;
            //}
            }else{
                isAllChildHasSameDeclineCodeAsParent = false;
            }   
            // END
                
            
            // if all posted then update Parent Case
            if (isAllPosted) {
                
                // add parent case     
//                Case parentCase = new Case(Id = key, Status = 'Closed', Refund_Status__c = 'Posted');
                Case parentCase = new Case(Id = key, Status = 'Closed', Refund_Status__c = 'Posted', Disposition_Level_1__c='Refund - Validated Posted'); //AM
                updateParentCaseList.add(parentCase);
                totalRecord++;
                
                // add all child case
                String caseIdConcatValues = caseWithCaseIdMap.get(key);
                String[] caseIdConcatValuesArr = caseIdConcatValues.split(DELIMITER);
                for (String eachCaseId: caseIdConcatValuesArr) {
//                    Case childCase = new Case(Id = eachCaseId, Status = 'Closed', Refund_Status__c = 'Posted');
                    Case childCase = new Case(Id = eachCaseId, Status = 'Closed', Refund_Status__c = 'Posted', Disposition_Level_1__c='Refund - Validated Posted'); //AM
                    updateParentCaseList.add(childCase);
                    totalRecord++;
                }
            }else if(isAllValidated){
                // add parent case     
                Case parentCase = new Case(Id = key,Refund_Status__c = 'Validated');
                updateParentCaseList.add(parentCase);
                totalRecord++;         
            }else if(isAllFailedValidation){
                // add parent case     
//                Case parentCase = new Case(Id = key,Refund_Status__c = 'Failed Validation');
                Case parentCase = new Case(Id = key,Refund_Status__c = 'Failed Validation', Disposition_Level_1__c='Refund - Failed Validation'); //AM
                if(isAllContainsDeclineCodeFG){
                    parentCase.Status='Closed';
                    parentCase.OwnerId=refundCompletedQueueId;
                    parentCase.Declined_Reason_Code__c=caseDeclinedCodeConcatValues.split(DELIMITER)[0];
                }
                
                updateParentCaseList.add(parentCase);
                totalRecord++;

                // update child case closed
                if(isAllContainsDeclineCodeFG){
                    String caseIdConcatValues = caseWithCaseIdMap.get(key);
                    String[] caseIdConcatValuesArr = caseIdConcatValues.split(DELIMITER);
                    for (String eachCaseId: caseIdConcatValuesArr) {
                        Case childCase = new Case(Id = eachCaseId, Status = 'Closed',OwnerId=refundCompletedQueueId);
                        updateParentCaseList.add(childCase);
                        totalRecord++;
                    }
                }
            }else if(isAllChildHasSameDeclineCodeAsParent){
                Case parentCase = new Case(Id = key,Status = 'Closed');
                updateParentCaseList.add(parentCase);
                totalRecord++;
            }
            }else{
                processLog+='WARNING: Ignored Parent Case ID: '+ key +'\r\n';
            }
        }catch(Exception eachRecordProcessEx){
            processLog+='Unable to process Parent Case ID: '+ key +'\r\n';
            processLog+='ERROR: '+eachRecordProcessEx.getCause()+'\r\n';
            processLog+=eachRecordProcessEx.getMessage()+'\r\n';
            processLog+=eachRecordProcessEx.getStackTraceString()+'\r\n';
        }   
        } // end - for
        
        Database.SaveResult[] srURList = Database.update(updateParentCaseList, false);
        for (Integer i = 0; i < srURList.size(); i++) {
            if (!srURList[i].isSuccess()) {
                for (Database.Error err: srURList[i].getErrors()) {
                    processLog += 'Unable to Update Case ID: ' + updateParentCaseList[i].Id+'\r\n';
                    processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                    processLog += 'Case fields that affected this error: ' + err.getFields() + '\r\n';
                    errorRecord++;
                    failureRecords.add(updateParentCaseList[i].Id);
                }
            } else {
                succesfulRecord++;
            }
        }
        
        System.debug('execute() - END');
    } // END- execute()
    
    global void finish(Database.BatchableContext bc) {
        System.debug('finish() - START');
        
        // query job details
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = : bc.getJobId()];
        processLog = 'Job Id: ' + job.Id + '; Job Status: ' + job.Status + '; Total Job Items: ' + job.TotalJobItems + '\r\n' + '------------------------------------------------\r\n' + processLog;
        
        // to avoid null print
        String failuerIdsStr = (failureRecords != null && failureRecords.size() > 0) ? string.join(failureRecords, ',') : 'NONE';
        
        // summary log on top
        processLog = '################################################\r\n' +
            'Job Name: ' + JobName + '\r\n' +
            'Start Date Time: ' + string.valueof(StartTime) + '\r\n' +
            'End Date Time: ' + string.valueof(DateTime.Now()) + '\r\n' +
            'Total Processed Records: ' + totalRecord + '\r\n' +
            'Total Successful Records: ' + succesfulRecord + '\r\n' +
            'Total Failed Records: ' + errorRecord + '\r\n' +
            'List of Failed Records: ' + failuerIdsStr + '\r\n' +
            '################################################\r\n' + processLog;
        
        // will Last Run Custom Settings
        updateBatchRefundCS();
        
        // add application log
        boolean isError = false;
        if (errorRecord > 0) {
            isError = true;
        }
        insertApplicationLog(bc.getJobId(), isError);
        
        System.debug('finish() - END');
    } // END - finish
    
    private void insertApplicationLog(String jobId, boolean isError) {
        ApplicationLogWrapper appLogWrapper = new ApplicationLogWrapper();
        appLogWrapper.logLevel = 'Info';
        
        if (isError)
            appLogWrapper.logLevel = 'Error';
        
        appLogWrapper.sourceClass = 'Batch_RefundCaseUpdate';
        appLogWrapper.sourceFunction = 'finish(Database.BatchableContext bc)';
        appLogWrapper.referenceInfo = 'Apex Batch: Job Id: ' + jobId;
        appLogWrapper.logMessage = processLog;
        appLogWrapper.applicationName = 'RefundCaseUpdateBatch';
        appLogWrapper.exceptionThrown = null;
        appLogWrapper.startDate = StartTime;
        appLogWrapper.endDate = DateTime.now();
        appLogWrapper.type = 'Job Log';
        
        // Set Log Level
        appLogWrapper.isDebug = sysSettingMD.Debug__c;
        appLogWrapper.isInfo = sysSettingMD.Info__c;
        appLogWrapper.isError = sysSettingMD.Error__c;
        appLogWrapper.isWarning = sysSettingMD.Warning__c;
        
        GlobalUtility.logMessage(appLogWrapper);
    }
    
    // will update batch last run Date Time
    private void updateBatchRefundCS(){
        List<BATCH_REFUND_CASE__c> batchRefundCS = [SELECT id,Last_Run_Date_Time__c FROM BATCH_REFUND_CASE__c WHERE name = 'LAST_RUN_DATE_TIME'];
        if(!batchRefundCS.isEmpty()){
            System.debug('Existing Last_Run_Date_Time__c: '+batchRefundCS[0].Last_Run_Date_Time__c);
            batchRefundCS[0].Last_Run_Date_Time__c=StartTime;
            System.debug('After Update Last_Run_Date_Time__c: '+batchRefundCS[0].Last_Run_Date_Time__c);
            update batchRefundCS;
        }else{
            BATCH_REFUND_CASE__c batchRefundCSInsertFirstTime = new BATCH_REFUND_CASE__c();
            batchRefundCSInsertFirstTime.Name='LAST_RUN_DATE_TIME';
            batchRefundCSInsertFirstTime.Last_Run_Date_Time__c = StartTime;
            System.debug('First Time Last_Run_Date_Time__c: '+batchRefundCSInsertFirstTime.Last_Run_Date_Time__c);
            insert batchRefundCSInsertFirstTime;
        }
    }
    
} // END - Batch_ParentCaseUpdate