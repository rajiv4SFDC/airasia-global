/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author          Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar     Created
25/10/2018		Aik Meng		Updated testAllChildValidated() to accommodate new Validation Rule
29/04/2019		Alvin Tayag		Changed testUpdateCaseEx changed Assert to 2 to 3 (To validate business case on change)
*******************************************************************/
@isTest(SeeAllData = false)
private class Batch_RefundCaseUpdate_Test {
    private static String childRefundCaseRecId = '';
    private static String parentRefundCaseRecId = '';
    private static String moduleName = 'Batch_RefundCaseUpdate';
    
    static{
        childRefundCaseRecId = [SELECT Id, 
                                       Name 
                                FROM   RecordType 
                                WHERE  sobjecttype = 'Case' 
                                AND    Name = 'Child Refund Case'
                               ].Id;
        
        parentRefundCaseRecId = [SELECT Id, 
                                        Name 
                                 FROM   RecordType 
                                 WHERE  sobjecttype = 'Case' 
                                 AND    Name = 'Refund Case'
                                ].Id;   
    }   
    
    // created Custom setting
    @testSetup
    static void createBatchRefundCustomSettings(){
        BATCH_REFUND_CASE__c batchRefundCS = new BATCH_REFUND_CASE__c();
        batchRefundCS.Name = 'LAST_RUN_DATE_TIME';
        batchRefundCS.Last_Run_Date_Time__c = null;
        upsert batchRefundCS;
    }
    
    /** All child case 'Posted'**/
    static testMethod void testAllChildPosted() {
        
        //parent case data
        Map<String,Object> caseFieldValueMap = new Map<String,Object>();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);
        
        TestUtility.createCases(caseFieldValueMap, 2);
        
        List<Case> parentCaseCreatedList = (List<Case>) TestUtility.getCases();
        System.assertEquals(2, parentCaseCreatedList.size());
        
        // child case data
        for (Case eachParentCase: parentCaseCreatedList) {
            caseFieldValueMap = new Map<String,Object>();
            caseFieldValueMap.put('Subject', 'Child_Case');
            caseFieldValueMap.put('Status', 'New');
            caseFieldValueMap.put('Type', 'Refund');
            caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
            caseFieldValueMap.put('ParentId', eachParentCase.Id);
            caseFieldValueMap.put('Refund_Status__c', 'Posted');
            caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
            caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
            TestUtility.createCases(caseFieldValueMap, 2);
        }
        
        List<Case> overallCaseCreatedList = (List<Case>) TestUtility.getCases();
        System.assertEquals(6, overallCaseCreatedList.size());
        
        List<Case> postedChildCaseList = [SELECT Id, 
                                                 Status 
                                          FROM   Case 
                                          WHERE  Refund_Status__c = 'Posted'
                                         ];
        System.assertEquals(4, postedChildCaseList.size());

        // building query   
        DateTime StartTime = DateTime.now();
        List<System_Settings__mdt> sysSettingMD = [SELECT Id, 
                                                          Label, 
                                                          Date_Field_API_Name__c, 
                                                          Additional_Filter__c,
                                                          Batch_Size__c, 
                                                          SObject_Api_Name__c, 
                                                          Debug__c, 
                                                          Info__c, 
                                                          Warning__c, 
                                                          Error__c 
                                                   FROM   System_Settings__mdt
                                                   WHERE  MasterLabel = 'Refund Case Update Batch'
                                                   LIMIT 1
                                                  ];
        DateTime rpsStartDateTime = StartTime;
        String rpsStartDateTimeStr = rpsStartDateTime.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String query = 'SELECT ParentId, '+
                       '       Count(Id) '+
                       'FROM   Case '+
                       'GROUP BY ParentId';
        
        Test.startTest();
	        Database.executeBatch(new Batch_RefundCaseUpdate(query,sysSettingMD[0]), 20);
        Test.stopTest();

        // only 6 must be posted.   
        List<Case> postedCaseList = [SELECT Id, 
                                            Status 
                                     FROM   Case 
                                     WHERE  Refund_Status__c = 'Posted'
                                    ];
        // AACSMDEV:  System.assertEquals(6, postedCaseList.size());
        // PROD:  System.assertEquals(6, postedCaseList.size());
        System.assertEquals(6, postedCaseList.size());
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName
                                           ];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
    
    /** All child case 'Validated' **/
    static testMethod void testAllChildValidated() {

        // CREATE Person Account
        String personAccountRecId = [SELECT Id FROM RecordType WHERE Name = 'Person Account'
                               and SObjectType = 'Account'
                               AND IsPersonType = True
                              ].Id;
        
        Map<String,Object> personAccountMap = new Map<String,Object>();
        personAccountMap.put('FirstName', 'Test PA');
        personAccountMap.put('LastName', '1');
        personAccountMap.put('PersonEmail', 'christytai@airasia.com');
        personAccountMap.put('RecordTypeId', personAccountRecId);
        TestUtility.createAccount(personAccountMap);
        
        // Parent Case data
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);

        // Added: 25Oct2018 - additional fields required due to new Validation Rule
        caseFieldValueMap.put('Airline_Code__c','AK');
        caseFieldValueMap.put('Airline_Code_Navitaire__c','AK');
        caseFieldValueMap.put('Booking_Number__c','ABC123');
        caseFieldValueMap.put('Booking_Payment__c','Credit Card');
        caseFieldValueMap.put('Write_Off_Currency_Code__c','MYR');
        caseFieldValueMap.put('Write_Off_Amount__c',100);
        caseFieldValueMap.put('Refund_Currency_Code__c','MYR');
        caseFieldValueMap.put('Refund_Amount__c',100);
        
        TestUtility.createCases(caseFieldValueMap, 2);
        List < Case > parentCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(2, parentCaseCreatedList.size());
        
        // child case data
        for (Case eachParentCase: parentCaseCreatedList) {
            caseFieldValueMap = new Map < String, Object > ();
            caseFieldValueMap.put('Subject', 'Child_Case');
            caseFieldValueMap.put('Status', 'New');
            caseFieldValueMap.put('Type', 'Refund');
            caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
            caseFieldValueMap.put('ParentId', eachParentCase.Id);
            caseFieldValueMap.put('Refund_Status__c', 'Validated');
            caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
            caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);

            // Added: 25Oct2018 - additional fields required due to new Validation Rule
            caseFieldValueMap.put('Airline_Code__c','AK');
            caseFieldValueMap.put('Airline_Code_Navitaire__c','AK');
            caseFieldValueMap.put('Booking_Number__c','ABC123');
            caseFieldValueMap.put('Booking_Payment__c','Credit Card');
            caseFieldValueMap.put('Write_Off_Currency_Code__c','MYR');
            caseFieldValueMap.put('Write_Off_Amount__c',100);
            caseFieldValueMap.put('Refund_Currency_Code__c','MYR');
            caseFieldValueMap.put('Refund_Amount__c',100);
            
            TestUtility.createCases(caseFieldValueMap, 2);
        }
        
        List < Case > overallCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(6, overallCaseCreatedList.size());
        
        List < Case > postedChildCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Validated'];
        System.assertEquals(4, postedChildCaseList.size());
        
        // building query
        DateTime StartTime = DateTime.now();
        List < System_Settings__mdt > sysSettingMD = [SELECT Id, Label, Date_Field_API_Name__c, Additional_Filter__c,
                                                      Batch_Size__c, SObject_Api_Name__c, Debug__c, Info__c, Warning__c, Error__c FROM System_Settings__mdt
                                                      WHERE MasterLabel = : 'Refund Case Update Batch'
                                                      LIMIT 1
                                                     ];
        DateTime rpsStartDateTime = StartTime;
        String rpsStartDateTimeStr = rpsStartDateTime.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String query = 'SELECT ParentId, Count(Id) FROM Case group by ParentId';
        //String query = 'SELECT ParentId, Count(Id) FROM Case WHERE ' + sysSettingMD[0].Date_Field_API_Name__c + '<=' + rpsStartDateTimeStr + ' group by //ParentId';
        
        Test.startTest();
        Database.executeBatch(new Batch_RefundCaseUpdate(query,sysSettingMD[0]), 20);
        Test.stopTest();
        
        // only 6 must be Validated.
        List < Case > postedCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Validated'];
        System.assertEquals(6, postedCaseList.size());
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName
                                           ];
        System.assertNotEquals(null, appLogs);
        System.assertEquals(1, appLogs.size());
    }
    
    /** when all child 'Failed Validated' **/
    static testMethod void testAllChildValidationFailed() {
        
        //Parent case data
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);
        
        TestUtility.createCases(caseFieldValueMap, 2);
        List < Case > parentCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(2, parentCaseCreatedList.size());

        // child case data        
        for (Case eachParentCase: parentCaseCreatedList) {
            caseFieldValueMap = new Map < String, Object > ();
            caseFieldValueMap.put('Subject', 'Child_Case');
            caseFieldValueMap.put('Status', 'New');
            caseFieldValueMap.put('Type', 'Refund');
            caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
            caseFieldValueMap.put('ParentId', eachParentCase.Id);
            caseFieldValueMap.put('Refund_Status__c', 'Failed Validation');
            caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
            caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
            TestUtility.createCases(caseFieldValueMap, 2);
        }
        
        List < Case > overallCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(6, overallCaseCreatedList.size());
        
        List < Case > postedChildCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Failed Validation'];
        System.assertEquals(4, postedChildCaseList.size());

        // building query   
        DateTime StartTime = DateTime.now();
        List < System_Settings__mdt > sysSettingMD = [SELECT Id, Label, Date_Field_API_Name__c, Additional_Filter__c,
                                                      Batch_Size__c, SObject_Api_Name__c, Debug__c, Info__c, Warning__c, Error__c FROM System_Settings__mdt
                                                      WHERE MasterLabel = : 'Refund Case Update Batch'
                                                      LIMIT 1
                                                     ];
        DateTime rpsStartDateTime = StartTime;
        String rpsStartDateTimeStr = rpsStartDateTime.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String query = 'SELECT ParentId, Count(Id) FROM Case group by ParentId';
        //String query = 'SELECT ParentId, Count(Id) FROM Case WHERE ' + sysSettingMD[0].Date_Field_API_Name__c + '<=' + rpsStartDateTimeStr + ' group by //ParentId';
        
        Test.startTest();
        Database.executeBatch(new Batch_RefundCaseUpdate(query,sysSettingMD[0]), 20);
        Test.stopTest();
        
        // only 6 must be Failed Validation
        List < Case > postedCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Failed Validation'];
        // AACSMDEV: System.assertEquals(6, postedCaseList.size());
        // PROD: System.assertEquals(6, postedCaseList.size());
        System.assertEquals(6, postedCaseList.size());
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName
                                           ];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
    
    /** when all child is not posted**/
    static testMethod void testPartialCases() {
        
        //Parent Case test data
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);
        
        TestUtility.createCases(caseFieldValueMap, 2);
        List < Case > parentCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(2, parentCaseCreatedList.size());
        
        // child case data
        caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Child_Case');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('ParentId', parentCaseCreatedList[0].Id);
        caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
        caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
        TestUtility.createCases(caseFieldValueMap, 2);
        
        caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Child_Case');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Refund_Status__c', 'Posted');
        caseFieldValueMap.put('ParentId', parentCaseCreatedList[1].Id);
        caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
        caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
        TestUtility.createCases(caseFieldValueMap, 2);
        
        List < Case > overallCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(6, overallCaseCreatedList.size());
        
        List < Case > postedChildCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Posted'];
        System.assertEquals(2, postedChildCaseList.size());

        // building query        
        DateTime StartTime = DateTime.now();
        List < System_Settings__mdt > sysSettingMD = [SELECT Id, Label, Date_Field_API_Name__c, Additional_Filter__c,
                                                      Batch_Size__c, SObject_Api_Name__c, Debug__c, Info__c, Warning__c, Error__c FROM System_Settings__mdt
                                                      WHERE MasterLabel = : 'Refund Case Update Batch'
                                                      LIMIT 1
                                                     ];
        DateTime rpsStartDateTime = StartTime;
        String rpsStartDateTimeStr = rpsStartDateTime.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String query = 'SELECT ParentId, Count(Id) FROM Case group by ParentId';
        //String query = 'SELECT ParentId, Count(Id) FROM Case WHERE ' + sysSettingMD[0].Date_Field_API_Name__c + '<=' + rpsStartDateTimeStr + ' group by //ParentId';

        // actual batch called  
        Test.startTest();
        Database.executeBatch(new Batch_RefundCaseUpdate(query,sysSettingMD[0]), 20);
        Test.stopTest();
        
        //only 3 must be Posted
        List < Case > postedCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Posted'];
        // AACSMDEV: System.assertEquals(3, postedCaseList.size());
        // PROD: System.assertEquals(3, postedCaseList.size());
        System.assertEquals(3, postedCaseList.size());
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName
                                           ];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
    
    /** when Case.ParentId is empty/null 
     then batch should not process any case record.**/
    static testMethod void testCasesWithNoParentId() {
        
        // Parent case creation
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);
        
        TestUtility.createCases(caseFieldValueMap, 2);
        List < Case > parentCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(2, parentCaseCreatedList.size());
     
        // child case creation
        caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Child_Case');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Refund_Status__c', 'Posted');
        caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
        caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
        
        TestUtility.createCases(caseFieldValueMap, 2);
        List < Case > postedChildCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Posted'];
        System.assertEquals(2, postedChildCaseList.size());
        
        // overall 4 cases
        List < Case > overallCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(4, overallCaseCreatedList.size());
        
        // building query
        DateTime StartTime = DateTime.now();
        List < System_Settings__mdt > sysSettingMD = [SELECT Id, Label, Date_Field_API_Name__c, Additional_Filter__c,
                                                      Batch_Size__c, SObject_Api_Name__c, Debug__c, Info__c, Warning__c, Error__c FROM System_Settings__mdt
                                                      WHERE MasterLabel = : 'Refund Case Update Batch'
                                                      LIMIT 1
                                                     ];
        
        DateTime rpsStartDateTime = StartTime;
        String rpsStartDateTimeStr = rpsStartDateTime.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String query = 'SELECT ParentId, Count(Id) FROM Case group by ParentId';
        //String query = 'SELECT ParentId, Count(Id) FROM Case WHERE ' + sysSettingMD[0].Date_Field_API_Name__c + '<=' + rpsStartDateTimeStr + ' group by //ParentId';
        
        // test actual batch
        Test.startTest();
        Database.executeBatch(new Batch_RefundCaseUpdate(query,sysSettingMD[0]), 20);
        Test.stopTest();

        // only 2 Posted        
        List < Case > postedCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Posted'];
        System.assertEquals(2, postedCaseList.size());
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName
                                           ];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
    
    /** while closing case if Type and Sub_Category_1__c is not populated then 
     it will throw error to cover exceptional block of code.**/
    static testMethod void testUpdateCaseEx() {
        
        //Parent case data
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);

        TestUtility.createCases(caseFieldValueMap, 2);
        List < Case > parentCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(2, parentCaseCreatedList.size());

        // child case creation  
        caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Child_Case');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('ParentId', parentCaseCreatedList[0].Id);
        caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
        caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
        TestUtility.createCases(caseFieldValueMap, 2);
        
        caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Child_Case');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Refund_Status__c', 'Posted');
        caseFieldValueMap.put('ParentId', parentCaseCreatedList[1].Id);
        caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
        caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
        TestUtility.createCases(caseFieldValueMap, 2);
        
        List < Case > overallCaseCreatedList = (List < Case > ) TestUtility.getCases();
        System.assertEquals(6, overallCaseCreatedList.size());
        
        List < Case > postedChildCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Posted'];
        System.assertEquals(2, postedChildCaseList.size());
        
        // building query
        DateTime StartTime = DateTime.now();
        List < System_Settings__mdt > sysSettingMD = [SELECT Id, Label, Date_Field_API_Name__c, Additional_Filter__c,
                                                      Batch_Size__c, SObject_Api_Name__c, Debug__c, Info__c, Warning__c, Error__c FROM System_Settings__mdt
                                                      WHERE MasterLabel = : 'Refund Case Update Batch'
                                                      LIMIT 1
                                                     ];
        
        DateTime rpsStartDateTime = StartTime;
        String rpsStartDateTimeStr = rpsStartDateTime.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        String query = 'SELECT ParentId, Count(Id) FROM Case group by ParentId';
        //String query = 'SELECT ParentId, Count(Id) FROM Case WHERE ' + sysSettingMD[0].Date_Field_API_Name__c + '<=' + rpsStartDateTimeStr + ' group by //ParentId';

        // actual batch called  
        Test.startTest();
        Database.executeBatch(new Batch_RefundCaseUpdate(query,sysSettingMD[0]), 20);
        Test.stopTest();
        
        // AT - 29 April 2019 - Changed assert to 3 instead of 2 - to validate business case on the change
        // only 2 posted
        List < Case > postedCaseList = [SELECT Id, Status FROM Case WHERE Refund_Status__c = 'Posted'];
        //System.assertEquals(2, postedCaseList.size());
        System.assertEquals(3, postedCaseList.size());
        
        // check only 1 summary log 
        List<Application_Log__c> appLogs = [SELECT Id 
                                            FROM   Application_Log__c 
                                            WHERE  Module__c = :moduleName
                                           ];
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
    }
}