/**
 * @File Name          : CAACHandler.apxc
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 7/9/2019, 12:14:36 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/4/2019, 1:42:47 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
 * 1.1    7/9/2019, 12:14:26 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Reduced Time in between
**/

global class CAACHandler implements Schedulable{
    
    
    global void execute(SchedulableContext sc) {
        //Pull Metadata checkcodes
        CAACHandlerFuture.pullComplaints();
        
		
        //Reschedule after 3 minutes
       	Datetime dtResched = system.now();
        dtResched = dtResched.addMinutes(3); //Changed to 3 Minutes as CAAC doesn't display the case for a long period
        String day = string.valueOf(dtResched.day());
        String month = string.valueOf(dtResched.month());
        String hour = string.valueOf(dtResched.hour());
        String minute = string.valueOf(dtResched.minute());
        String second = string.valueOf(dtResched.second());
        String year = string.valueOf(dtResched.year());

        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
       
        String strJobName = 'CAAC-Retrieve - ' + strSchedule;
        System.schedule(strJobName, strSchedule, new CAACHandler());
        System.abortJob(sc.getTriggerId());
        //End
    }
}