/**
 * @File Name          : CAACHandlerFuture.cls
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 7/9/2019, 12:56:18 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/7/2019, 12:38:53 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
public class CAACHandlerFuture {
    
    public static RecordType personAccountRecordType;
	public static Map<String,CAAC_Case_Type_Mapping__mdt> caseTypeMapping;
    public static String defaultEntitlementId;
    private static System_Settings__mdt logLevelCMD;
    private static Datetime startTime = DateTime.now();
    private static List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();
    private static List<Case> caseToUpload = new List<Case>();
    private static List<Account> accountToUpload = new List<Account>();
    private static Map<String, String> accountEmailMap = new Map<String, String>();
    private static Map<String, String> contactEmailMap = new Map<String, String>();
    private static Map<String, Contact> contactToUpload = new Map<String, Contact>();
    private static List<String> newEmails = new List<String>();

    @future(callout=true)
    public static void pullComplaints() {
        //Pre-load Values
        List<CAAC_CheckCode__mdt> checkCodes = getCheckCodes();
        Web_Services_Credentials__mdt caacDownloadURL = getCAACDownloadURL();
        caseTypeMapping = getCaseTypeMapping();
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('CAAC_Retrieve_Error'));
        personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        List<Entitlement> entitlements = [SELECT Id FROM Entitlement LIMIT 1];
        if(!entitlements.isEmpty()) {
            defaultEntitlementId = entitlements[0].Id;
        }
        
        //Loop through checkcodes Custom Metadata Records
        for(CAAC_CheckCode__mdt checkCode : checkCodes) {
            HttpRequest httpReq = prepareComplaintRequest(checkCode, caacDownloadURL);
            httpReq.setTimeout(30000);
            Http http = new Http();
            processComplaintResponse(http.send(httpReq), checkCode);
        }

        //Insert Accounts
        List<Case> updatedCaseToUpload = new List<Case>();
        if (!accountToUpload.isEmpty()) {
            Database.SaveResult[] res = Database.insert(accountToUpload);
            List<Account> insertedAccounts = [SELECT Id,
                                                     PersonEmail
                                              FROM Account
                                              WHERE PersonEmail =: newEmails];
            List<Contact> updatedContactList = new List<Contact>();

            for(Account insertedAccount: insertedAccounts) {
                accountEmailMap.put(insertedAccount.PersonEmail, insertedAccount.Id);
                Contact contactObj = contactToUpload.get(insertedAccount.PersonEmail);
                if(contactObj != null) {
                    updatedContactList.add(contactObj);
                }
            }

            //Insert Contacts
            if (!updatedContactList.isEmpty()) {
                Database.SaveResult[] resContact = Database.insert(updatedContactList);
            }

            //Refresh updatedContactList with Ids
            List<Contact> contactObjWIds = [SELECT Id,
                                                   Email
                                            FROM Contact
                                            WHERE Email =: newEmails];
            
            for(Case caseObj : caseToUpload) {
                if(caseObj.AccountId != null) {
                    updatedCaseToUpload.add(caseObj);
                }
                else {
                    caseObj.AccountId = accountEmailMap.get(caseObj.SuppliedEmail);
                    caseObj.ContactId = contactEmailMap.get(caseObj.SuppliedEmail);
                    updatedCaseToUpload.add(caseObj);
                }
            }
        }
        else {
            updatedCaseToUpload = caseToUpload;
        }

        //Insert Cases
        if (!caseToUpload.isEmpty()) {
            Database.SaveResult[] resCases = Database.insert(caseToUpload);
        }

        if (!appWrapperLogList.isEmpty()) {
            GlobalUtility.logMessage(appWrapperLogList);
        }
    }

    @TestVisible
    private static HttpRequest prepareComplaintRequest(CAAC_CheckCode__mdt checkCode, Web_Services_Credentials__mdt caacDownloadURL) {
        //Prepare Call Out
        HttpRequest req = new HttpRequest();
        req.setTimeout(30000);
        req.setEndpoint(caacDownloadURL.Url__c+checkCode.Check_Code__c);
		req.setMethod('GET');
        
        return req;
    }

    public static Boolean processComplaintResponse(HttpResponse httpRes, CAAC_CheckCode__mdt checkCode) {

        if(Test.isRunningTest()) {
            //Pre-load Values
            List<CAAC_CheckCode__mdt> checkCodes = getCheckCodes();
            Web_Services_Credentials__mdt caacDownloadURL = getCAACDownloadURL();
            caseTypeMapping = getCaseTypeMapping();
            logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('CAAC_Retrieve_Error'));
            personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
            List<Entitlement> entitlements = [SELECT Id FROM Entitlement LIMIT 1];
            if(!entitlements.isEmpty()) {
                defaultEntitlementId = entitlements[0].Id;
            }
        }

        Boolean result = false;
        String resBody = httpRes.getBody();
        if (httpRes.getStatusCode() == 200) {
            List<Account> accountToUpload = new List<Account>();
            List<String> emails = new List<String>();
            //Override ResBody
            //resBody = '[{\"airCptSCode\":\"\",\"arrPort\":\"KUL\",\"attachment\":\"http://www.caacts.org.cn/admin/jiekou_chuanshuxiazai.action?sid=1&attachment=f53646D9930C-73B8-439F-AE11-B587ADFD6227.png&id=153306\",\"attachment2\":\"http://www.caacts.org.cn/admin/jiekou_chuanshuxiazai.action?sid=2&attachment=2aafF394BE6E-7954-49C1-9684-8A6791E962CA.jpeg&id=153306\",\"attachment3\":\"http://www.caacts.org.cn/admin/jiekou_chuanshuxiazai.action?sid=3&attachment=4dcdDEE06E0C-F2F9-49B2-9CDE-88777F727E78.png&id=153306\",\"attachment4\":\"\",\"attachment5\":\"\",\"attachment6\":\"\",\"attachment7\":\"\",\"attachment8\":\"\",\"attachment9\":\"\",\"beizhu\":\"\",\"chadd\":\"190610496\",\"chuanshubiaozhi\":\"1\",\"cishu\":0,\"code1\":\"\",\"code2\":\"\",\"code3\":\"\",\"complaintTime\":\"\",\"cptContent\":\"5月买了往返飞机票晋江到吉隆坡的，重复支付了两次，于是申请了退款，申请了大概四五次一直显示错误，五六天才申请成功。申请成功后，等了半个月，去问客服和我说要等一个月。一个月了，我又去问客服，才收到一封退款失败的邮件，现在又要重新申请，还不一定成功，真的太慢了。好几千块的票钱，就这样一直拖着不给。\",\"cptId\":\"JS2A190610496\",\"cptIdRepeated\":\"\",\"cptOccurPort\":\"JJN\",\"cptSource\":\"S\",\"cptTarget\":\"2\",\"cptTargetName\":\"D7 \",\"cptTime\":\"2019-06-10 16:02:10.0\",\"cptType\":\"A\",\"cptTypeOther\":\"\",\"cuiban\":\"\",\"defined\":0,\"definedCaption\":\"\",\"depPort\":\"JJN\",\"disposeLimit\":\"\",\"disposeName\":\"用户自己\",\"duanxin\":\"\",\"email\":\"342206053@qq.com\",\"fileState\":2,\"firstLooker\":\"\",\"firstTime\":\"\",\"flightNo\":\"AK270 \",\"flightTime\":\"2019-05-29\",\"id\":153306,\"isFirst\":\"4\",\"isdel\":\"1\",\"isshouci\":\"2\",\"istiaojie\":\"0\",\"istousu\":\"\",\"jieshou\":\"0\",\"jujue\":\"0\",\"loginName\":\"\",\"passengerName\":\"陈祺\",\"passengerTel\":\"18759731797\",\"passengerType\":1,\"place1\":\"\",\"place2\":\"\",\"place3\":\"\",\"place4\":\"\",\"shenHe\":100,\"shenHeInfo\":\"\",\"shenfenId\":\"350425199712110022\",\"shield\":\"\",\"state\":\"\",\"state1\":\"\",\"supplementary\":\"0\",\"suqiuContent\":\"希望亚航能有效率地处理这些重复支付退款问题，并且麻烦尽快处理一下我的问题，不要再拖下去了。\",\"templateSMS\":\"\",\"templateSMSbm\":\"\",\"tiaojiehuifu\":0,\"tiaojietime\":\"\",\"urge\":\"\",\"wuxiaoyy\":\"\",\"zhengjianid\":\"身份证\",\"zhiBanRen\":\"\",\"zhongxinhuifu\":0,\"zhuticptid\":\"\",\"zrname\":\"\"}]';
            List<CAACJSON.Visitor> complaints = new List<CAACJSON.Visitor>();
            
            try {
                if(!resBody.contains('"[]"')) {
                    resBody = resBody.replace('"visitor":"', '"visitor":');
                    resBody = resBody.replace('}]"}', '}]}');
                    resBody = resBody.replace('\\', '');
                    complaints = (CAACJSON.parse(resBody)).Visitor;
                }
            }
            catch (Exception e) {
                //Parse Error
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CAACHandlerFuture', 'processComplaintResponse',
                                                                             null, 'JSON Parsing Error', '-', resBody, 'Error Log', startTime, logLevelCMD, e));
            }
            //System.debug('Response--> ' + resBody);
            //Create Case here based on complaints and checkcode
            for(CAACJSON.Visitor complaint : complaints) {
                //Check if it exists first
                List<Case> casesFound = [SELECT Id
                                            FROM Case
                                            WHERE CAAC_ID__c =: (checkCode.Check_Code__c + '-' + complaint.cptId) 
                                            LIMIT 1];
                if(casesFound.isEmpty()) {
                    Case newCase = new Case();
                    //Define Case Details
                    newCase.Subject = 'CAAC-' + complaint.cptId + ': ' + complaint.suqiuContent;
                    newCase.Flight_Number__c = complaint.flightNo;
                    newCase.Comments__c = complaint.cptContent;
                    newCase.CAAC_ID__c = checkCode.Check_Code__c + '-' + complaint.cptId;
                    newCase.SuppliedName = complaint.passengerName;
                    //newCase.Departure_Station__c = ''; //Possible Mismatch on Value
                    //newCase.Arrival_Station__c = ''; //Possible Mismatch on Value
                    
                    try {
                        newCase.Departure_Date_and_Time__c = Date.parse(complaint.flightTime); //Date/Time Required
                    }
                    catch (Exception e) {
                        newCase.Departure_Date_and_Time__c = null;
                    }

                    //Map Case Type
                    CAAC_Case_Type_Mapping__mdt caseType = (CAAC_Case_Type_Mapping__mdt)caseTypeMapping.get(complaint.cptType);
                    if(caseType != null) {
                        
                        newCase.Type = caseType.Type__c;
                        newCase.Sub_Category_1__c = caseType.SubCategory_1__c;
                        newCase.Sub_Category_2__c = caseType.SubCategory_2__c;
                    }
                    
                    //Other Default Values
                    newCase.SuppliedEmail = complaint.email;
                    newCase.CreatedDate = DateTime.now();
                    newCase.Origin = 'CAAC';
                    newCase.Status = 'New';
                    newCase.EntitlementId = defaultEntitlementId;
                    newCase.Disposition_Level_1__c = 'No Disposition Required';
                    Account caseAccountObj = createMissingAccount(complaint);
                    Contact caseContactObj = createMissingContact(complaint);
                    if(caseAccountObj.Id != null) {
                        newCase.AccountId = caseAccountObj.Id;
                        newEmails.add(complaint.email);
                    }
                    else {
                        accountToUpload.add(caseAccountObj);
                    }
                    if(caseContactObj.Id != null) {
                        newCase.ContactId = caseContactObj.Id;
                    }
                    else {
                        contactToUpload.put(caseContactObj.Email, caseContactObj);
                    }

                    try {
                        newCase.OwnerId = Utilities.getQueueIdByName(checkCode.Queue__c);
                        caseToUpload.add(newCase);
                        result = true;
                    }
                    catch (Exception e) {
                        appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CAACHandlerFuture', 'processComplaintResponse',
                                                                                        null, 'Unable to Find Queue for CAAC', '-', JSON.serialize(complaint), 'Error Log', startTime, logLevelCMD, e));
                    }

                    
                } 
            }
        }
        else {
            //Handle Error on Callout
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CAACHandlerFuture', 'processComplaintResponse',
                                                                            null, 'HTTP Callout Error', '-', resBody, 'Error Log', startTime, logLevelCMD, null));
        }
        return result;
    }

    private static Account createMissingAccount(CAACJSON.Visitor complaint) {
        //Check if Contact Exists
        List<Account> accountsFound = [SELECT Id
                                        FROM Account
                                        WHERE PersonEmail =: complaint.email
                                        AND RecordTypeId =: personAccountRecordType.Id];
            
        if(accountsFound.isEmpty()) {
            Account personAccount = new Account();
            personAccount.PersonEmail = complaint.email;
            System.debug('New Account --> ' + complaint.email);
            //Split Names
            String fullName = complaint.passengerName;
            if (fullName == null) {
                fullName = '';
            }
            String firstName = '';
            String lastName = '';
            if (fullName.contains(' ')) {
                firstName = fullName.substringBefore(' ');
                lastName = fullName.substringAfter(' ');
            } else {
                lastName = fullName;
                firstName = '';
            }
            personAccount.FirstName = firstName;
            personAccount.LastName = lastName;
            personAccount.RecordTypeId = personAccountRecordType.Id;
            
            return personAccount;
            
        }
        else {
            return accountsFound[0];
        }
    }

    private static Contact createMissingContact(CAACJSON.Visitor complaint) {
        //Check if Contact Exists
        List<Contact> contactsFound = [SELECT Id
                                        FROM Contact
                                        WHERE Email =: complaint.email];
            
        if(contactsFound.isEmpty()) {
            Contact personContact = new Contact();
            personContact.Email = complaint.email;
            System.debug('New Contact --> ' + complaint.email);
            //Split Names
            String fullName = complaint.passengerName;
            if (fullName == null) {
                fullName = '';
            }
            String firstName = '';
            String lastName = '';
            if (fullName.contains(' ')) {
                firstName = fullName.substringBefore(' ');
                lastName = fullName.substringAfter(' ');
            } else {
                lastName = fullName;
                firstName = '';
            }
            personContact.FirstName = firstName;
            personContact.LastName = lastName;
            
            return personContact;
            
        }
        else {
            return contactsFound[0];
        }
    }

    public static List<CAAC_CheckCode__mdt> getCheckCodes() {
        List<CAAC_CheckCode__mdt> checkCodes = [SELECT Airline_Code__c,
                                                              Check_Code__c,
                                                              Company_Name__c,
                                                              Queue__c
                                                       FROM CAAC_CheckCode__mdt];

        return checkCodes;
    }

    public static Map<String,CAAC_Case_Type_Mapping__mdt> getCaseTypeMapping() {
        Map<String,CAAC_Case_Type_Mapping__mdt> caseTypeMapping = new Map<String,CAAC_Case_Type_Mapping__mdt>();
        List<CAAC_Case_Type_Mapping__mdt> caseTypeMappingList = [SELECT Complaint_Type__c,
                                                                    Complaint_Type_Value__c,
                                                                    Type__c,
                                                                    SubCategory_1__c,
                                                                    SubCategory_2__c
                                                            FROM CAAC_Case_Type_Mapping__mdt];
        
        for(CAAC_Case_Type_Mapping__mdt caacCaseTypeMapping : caseTypeMappingList) {
            caseTypeMapping.put(caacCaseTypeMapping.Complaint_Type_Value__c, caacCaseTypeMapping);
        }
        return caseTypeMapping;
    }

    public static Web_Services_Credentials__mdt getCAACDownloadURL() {
        List<Web_Services_Credentials__mdt> credentials = [SELECT Url__c
                                                          FROM Web_Services_Credentials__mdt
                                                          WHERE Web_Service_Name__c = 'WS_CAAC_DOWNLOAD'];

        if(credentials.size() > 0 && credentials != null) {
            return credentials[0];
        }
        return null;
    }
}