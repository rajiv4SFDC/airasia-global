/**
 * @File Name          : CAACHandler_Test.cls
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 7/9/2019, 1:51:14 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/6/2019, 11:59:08 AM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
@isTest
public class CAACHandlerFuture_Test {
    @isTest
    public static void testValidParse() {
        HttpResponse httpRes = new HttpResponse();
        //httpRes.setBody('{\"checkcode\":\"HFEHFE14844\",\"visitor\":[{\"airCptSCode\":\"\",\"arrPort\":\"SYX\",\"attachment\":\"\",\"attachment2\":\"\",\"attachment3\":\"\",\"attachment4\":\"\",\"attachment5\":\"\",\"attachment6\":\"\",\"attachment7\":\"\",\"attachment8\":\"\",\"attachment9\":\"\",\"beizhu\":\"\",\"chadd\":\"171230981\",\"chuanshubiaozhi\":\"1\",\"cishu\":0,\"code1\":\"\",\"code2\":\"\",\"code3\":\"\",\"cptContent\":\"徐州三亚航班取消改签至合肥新桥机场，但航空公司给改的是候补舱位，在有空位的情况还拒绝让我们登机，不让我们上飞机.航空公司有问题，既然这样还不如不给改签，耽误我们行程。\",\"cptId\":\"S3A171230981 \",\"cptIdRepeated\":\"\",\"cptOccurPort\":\"HFE\",\"cptSource\":\"S\",\"cptTarget\":\"3\",\"cptTargetName\":\"HFE\",\"cptTime\":\"2018-08-31 15:17:31.0\",\"cptType\":\"A\",\"cptTypeOther\":\"\",\"cuiban\":\"\",\"defined\":0,\"definedCaption\":\"\",\"depPort\":\"HFE\",\"disposeLimit\":\"\",\"disposeName\":\"用户自己\",\"duanxin\":\"\",\"email\":\"332853741@qq.com\",\"fileState\":2,\"firstLooker\":\"\",\"firstTime\":\"\",\"flightNo\":\"JD5570    \",\"flightTime\":\"2017-12-30\",\"id\":93519,\"isFirst\":\"4\",\"isdel\":\"1\",\"isshouci\":\"2\",\"istiaojie\":\"0\",\"istousu\":\"\",\"jieshou\":\"0\",\"jujue\":\"0\",\"loginName\":\"\",\"passengerName\":\"武良益\",\"passengerTel\":\"15351637689\",\"passengerType\":1,\"place1\":\"\",\"place2\":\"\",\"place3\":\"\",\"place4\":\"\",\"shenHe\":100,\"shenHeInfo\":\"\",\"shenfenId\":\"342201196603183817\",\"shield\":\"\",\"state\":\"\",\"state1\":\"\",\"suqiuContent\":\"徐州三亚航班取消改签至合肥新桥机场，但航空公司给改的是候补舱位，在有空位的情况还拒绝让我们登机，不让我们上飞机.航空公司有问题，既然这样还不如不给改签，耽误我们行程。\",\"templateSMS\":\"\",\"templateSMSbm\":\"\",\"tiaojiehuifu\":0,\"tiaojietime\":\"\",\"zhengjianid\":\"身份证\",\"zhiBanRen\":\"\",\"zhongxinhuifu\":0,\"zhuticptid\":\"\",\"zrname\":\"\"}]}');
		httpRes.setBody('{"checkcode":"AKAK13980","visitor":"[{\"airCptSCode\":\"\",\"arrPort\":\"SJW\",\"attachment\":\"\",\"attachment2\":\"\",\"attachment3\":\"\",\"attachment4\":\"\",\"attachment5\":\"\",\"attachment6\":\"\",\"attachment7\":\"\",\"attachment8\":\"\",\"attachment9\":\"\",\"beizhu\":\"\",\"chadd\":\"190709809\",\"chuanshubiaozhi\":\"1\",\"cishu\":0,\"code1\":\"\",\"code2\":\"\",\"code3\":\"\",\"cptContent\":\"\",\"cptId\":\"JS1A190709809\",\"cptIdRepeated\":\"\",\"cptOccurPort\":\"SHA\",\"cptSource\":\"S\",\"cptTarget\":\"1\",\"cptTargetName\":\"AK \",\"cptTime\":\"2019-07-09 12:44:32.0\",\"cptType\":\"A\",\"cptTypeOther\":\"\",\"cuiban\":\"\",\"defined\":0,\"definedCaption\":\"\",\"depPort\":\"SHA\",\"disposeLimit\":\"\",\"disposeName\":\"用户自己\",\"duanxin\":\"\",\"email\":\"mtayag@salesforce.com\",\"fileState\":2,\"firstLooker\":\"\",\"firstTime\":\"\",\"flightNo\":\"AK700     \",\"flightTime\":\"2019-07-09\",\"id\":131032,\"isFirst\":\"4\",\"isdel\":\"1\",\"isshouci\":\"2\",\"istiaojie\":\"0\",\"istousu\":\"\",\"jieshou\":\"0\",\"jujue\":\"0\",\"loginName\":\"\",\"passengerName\":\"Test\",\"passengerTel\":\"11111111\",\"passengerType\":1,\"place1\":\"\",\"place2\":\"\",\"place3\":\"\",\"place4\":\"\",\"shenHe\":100,\"shenHeInfo\":\"\",\"shenfenId\":\"XX1234567\",\"shield\":\"\",\"state\":\"\",\"state1\":\"\",\"supplementary\":\"\",\"suqiuContent\":\"\",\"templateSMS\":\"\",\"templateSMSbm\":\"\",\"tiaojiehuifu\":0,\"tiaojietime\":\"\",\"wuxiaoyy\":\"\",\"zhengjianid\":\"身份证\",\"zhiBanRen\":\"\",\"zhongxinhuifu\":0,\"zhuticptid\":\"\",\"zrname\":\"\"}]"}');
        httpRes.setStatusCode(200);
        httpRes.setStatus('OK');
		
        Test.startTest();
        List<CAAC_CheckCode__mdt> checkCodeMap = CAACHandlerFuture.getCheckCodes();
        System.assert(checkCodeMap != null);
        for (CAAC_CheckCode__mdt checkCode : checkCodeMap) {
            System.assert(CAACHandlerFuture.processComplaintResponse(httpRes, checkCode)); 
            return;
        }
        Test.stopTest();
        List<Case> createdCases = [SELECT Id FROM Case];
        System.assertEquals(1, createdCases.size());
    }
    
    @isTest
    public static void testCreateRequest() {
        List<CAAC_CheckCode__mdt> checkCodeMap = CAACHandlerFuture.getCheckCodes();
        Web_Services_Credentials__mdt caacDownloadURL = CAACHandlerFuture.getCAACDownloadURL();
        System.assert(checkCodeMap != null);
        for (CAAC_CheckCode__mdt checkCode : checkCodeMap) {
            System.assert(CAACHandlerFuture.prepareComplaintRequest(checkCode, caacDownloadURL) != null); 
            return;
        }
    }
}