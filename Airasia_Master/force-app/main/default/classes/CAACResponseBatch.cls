/**
 * @File Name          : CAACResponseBatch.cls
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 6/14/2019, 12:33:39 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/13/2019, 12:47:04 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
public class CAACResponseBatch implements 
                Database.Batchable<Case>,
                Database.AllowsCallouts,
                Database.Stateful {
                    
    private Map<String, CAAC_CheckCode__mdt> checkCodeMap;
    private System_Settings__mdt logLevelCMD;
    private Datetime startTime = DateTime.now();
    private List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();
    private Web_Services_Credentials__mdt caacUploadURL;
    private List<Case> caseList;
    public CAACResponseBatch(List<Case> caseList) {
        this.caseList = caseList;
    }

    public List<Case> start(Database.BatchableContext BC) {
        checkCodeMap = getCheckCodes();
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('CAAC_Response_Error'));
        caacUploadURL = getCAACUploadURL();
        return this.caseList;
    }

    public void execute(Database.BatchableContext BC, List<Case> scope) {
        //Prepare Response to CAAC
        List<Case> responseSent = new List<Case>();
        for(Case caseObj : scope) {
            HttpRequest httpReq = prepareSoapRequest(caseObj);
            Http http = new Http();
            try {
                HttpResponse httpRes = http.send(httpReq);
                System.debug(httpRes);
                if(httpRes.getStatusCode() != 200) {
                    appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CAACResponseBatch', 'execute',
                                                                                        '', 'Exception on Sending Response to CAAC', '-', httpRes.getStatusCode() + ':' + httpRes.getStatus() + '\n' + httpReq.getBody(), 'Error Log', startTime, logLevelCMD, null));
                }
                else {
                    caseObj.CAAC_Response_Sent__c = true;
                    responseSent.add(caseObj);
                }
            }
            catch (Exception e) {
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CAACResponseBatch', 'execute',
                                                                                        '', 'Exception on Sending Response to CAAC', '-', httpReq.getBody(), 'Error Log', startTime, logLevelCMD, e));
            }
        }

        try {
            update responseSent;
        }
        catch (Exception e) {
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CAACResponseBatch', 'execute',
                                                                                    '', 'Exception during update of CAAC_Response_Sent__c', '-', '', 'Error Log', startTime, logLevelCMD, e));
        }
    }

    public void finish(Database.BatchableContext BC) {
        if (!appWrapperLogList.isEmpty()) {
            GlobalUtility.logMessage(appWrapperLogList);
        }
    }

    private HttpRequest prepareSoapRequest(Case caseObj) {
        String checkCodeStr = '';
        String cptId = '';
        CAAC_CheckCode__mdt checkCode = null;
        try {
            //Extract CheckCode
            checkCodeStr = caseObj.CAAC_ID__c.split('-')[0];
            cptId = caseObj.CAAC_ID__c.split('-')[1];
            checkCode = checkCodeMap.get(checkCodeStr);
        }
        catch (Exception e) {
            //Parsing Error
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CAACResponseHandler', 'prepareSoapEnvelope',
                                                                                        null, 'Unable to retrieve CheckCode for Case', '-', caseObj.CAAC_ID__c, 'Error Log', startTime, logLevelCMD, e));
        }
        if(checkCode != null) {
            String soapEnv = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:int="http://interdata.com/">' +
                                '<soapenv:Header/>' +
                                    '<soapenv:Body>' +
                                        '<int:interfaceData>' +
                                            '<reply>' +
                                                '<aircode>' + checkCode.Airline_Code__c + '</aircode>' +
                                                '<checkcode>' + checkCode.Check_Code__c + '</checkcode>' +
                                                '<cptid>' + cptId + '</cptid>' +
                                                '<cptemphasis>' + caseObj.Reason + '</cptemphasis>' +
                                                '<disposeending>' + caseObj.CAAC_Final_Reply__c + '</disposeending>' + //Last Public Case Comment
                                                '<isreconciliation>' + caseObj.CAAC_Responsibility__c + '</isreconciliation>' +
                                                '<surveyprocess>' + caseObj.CAAC_Evidence__c + '</surveyprocess>' + //Last Public Case Comment
                                            '</reply>' +
                                        '</int:interfaceData>' +
                                    '</soapenv:Body>' +
                            '</soapenv:Envelope>';
            HttpRequest httpReq = new HttpRequest();
        	httpReq.setMethod('POST');
            httpReq.setHeader('Accept-Encoding', 'gzip,deflate');
            httpReq.setHeader('content-type', 'text/xml; charset=utf-8');
            httpReq.setHeader('Content-Length', String.valueOf(soapEnv.length()));
            httpReq.setHeader('SOAPAction', caacUploadURL.Url__c);
            httpReq.setBody(soapEnv);
            httpreq.setEndpoint(caacUploadURL.Url__c);

            return httpReq;
        }
        return null;
    }

    private static Map<String, CAAC_CheckCode__mdt> getCheckCodes() {
        Map<String, CAAC_CheckCode__mdt> checkCodeMap = new Map<String, CAAC_CheckCode__mdt>();
        List<CAAC_CheckCode__mdt> checkCodes = [SELECT Airline_Code__c,
                                                              Check_Code__c,
                                                              Company_Name__c,
                                                              Queue__c
                                                       FROM CAAC_CheckCode__mdt];
        for(CAAC_CheckCode__mdt checkCode : checkCodes) {
            checkCodeMap.put(checkCode.Check_Code__c, checkCode);
        }
        return checkCodeMap;
    }

    private static Web_Services_Credentials__mdt getCAACUploadURL() {
        List<Web_Services_Credentials__mdt> credentials = [SELECT Url__c
                                                          FROM Web_Services_Credentials__mdt
                                                          WHERE Web_Service_Name__c = 'WS_CAAC_UPLOAD'];

        if(credentials.size() > 0 && credentials != null) {
            return credentials[0];
        }
        return null;
    }
}