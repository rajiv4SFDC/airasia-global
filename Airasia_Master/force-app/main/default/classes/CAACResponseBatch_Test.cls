/**
 * @File Name          : CAACResponseBatch_Test.cls
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 6/14/2019, 12:41:22 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/14/2019, 12:40:02 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
@isTest
public class CAACResponseBatch_Test {
    @isTest
    public static void testBatch() {
        Test.setMock(HttpCalloutMock.class, new CAACResponseMock());
        
        
        Test.startTest();
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('CAAC_ID__c', 'AKAK13980-ID');
        caseFieldValueMap.put('CAAC_Final_Reply__c', 'Test Reply');
        caseFieldValueMap.put('Manual_Contact_Email__c', 'test@gmail.com');
        caseFieldValueMap.put('Status', 'Closed');
        caseFieldValueMap.put('SuppliedEmail', 'test@gmail.com');      
        caseFieldValueMap.put('Manual_Contact_Last_Name__c', 'TEST'); 
        caseFieldValueMap.put('Manual_Contact_First_Name__c', 'TEST'); 
        caseFieldValueMap.put('SuppliedName', 'TEST'); 
        caseFieldValueMap.put('SuppliedPhone', '9999999999'); 
        caseFieldValueMap.put('Supplied_Title__c', 'Mr');  
        caseFieldValueMap.put('Origin', 'CAAC'); 
        
        testUtility.createCase(caseFieldValueMap);
        Test.stopTest();
        
        List<Case> cases = [SELECT CAAC_Response_Sent__c  FROM Case];
        System.assert(cases[0].CAAC_Response_Sent__c);
    }
}