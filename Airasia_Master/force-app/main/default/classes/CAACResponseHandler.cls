/**
 * @File Name          : CAACHandlerFuture.cls
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 6/14/2019, 12:12:57 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/7/2019, 12:38:53 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
public class CAACResponseHandler {

    public static void sendResponseToCAAC (Case[] caseArr) {
        List<Case> filteredCaseArr = new List<Case>();
        for(Case caseObj : caseArr) {
            if (caseObj.CAAC_Response_Sent__c == false &&
                caseObj.Origin == 'CAAC' &&
                String.isNotEmpty(caseObj.CAAC_Final_Reply__c) &&
                String.isNotEmpty(caseObj.CAAC_ID__c) &&
                caseObj.IsClosed) {
                filteredCaseArr.add(caseObj);
            }
        }

        if(!filteredCaseArr.isEmpty()) {
            Id jobId = Database.executeBatch(new CAACResponseBatch(filteredCaseArr), 10);  
            System.debug('Batch job Id = ' + jobId);
        }
    }
}