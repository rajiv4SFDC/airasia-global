public class CallCenterCaseRouting {
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    private static String processLog = '';
    private static List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
    
    @InvocableMethod
    public static void assignCaseToRelevantQueue(List < Id > caseIds) {
        try {
            // get System Setting details for Logging
            logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Routing_Log_Level_CMD_NAME'));
            
            // Query Case for extra details
            List < Case > updateCaseList;
            List < Case > caseList = [Select Id, Call_Center_Escalation_Team__c from Case where Id IN: caseIds];
            
            if (!caseList.isEmpty()) {
                updateCaseList = new List < Case > ();
                
                Map < String, String > ccCaseRoutingMap = new Map < String, String > ();
                List < Call_Center_Case_Routing__mdt > ccCaseRoutingList = [Select Id, Call_Center_Escalation_Team__c, Queue__c from Call_Center_Case_Routing__mdt];
                for (Call_Center_Case_Routing__mdt eachRow: ccCaseRoutingList) {
                    ccCaseRoutingMap.put(eachRow.Call_Center_Escalation_Team__c, eachRow.Queue__c);
                }
                //System.debug('ccCaseRoutingMap:'+JSON.serializePretty(ccCaseRoutingMap));
                
                for (Case caseToBeAssignedToQueue: caseList) {
                    String caseId = caseToBeAssignedToQueue.id;
                    
                    try {
                        if (ccCaseRoutingMap.containsKey(caseToBeAssignedToQueue.Call_Center_Escalation_Team__c)) {
                            caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(ccCaseRoutingMap.get(caseToBeAssignedToQueue.Call_Center_Escalation_Team__c));
                        } else {
                            String soqlQuery = 'Select Id, Queue__c from Call_Center_Case_Routing__mdt' +
                                ' where Call_Center_Escalation_Team__c =' + caseToBeAssignedToQueue.Call_Center_Escalation_Team__c + '\r\n';
                            throw new QueryException(soqlQuery);
                        }
                        
                    } catch (Exception queueQueryEx) {
                        System.debug(LoggingLevel.ERROR, queueQueryEx);
                        
                        caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(Utilities.getAppSettingsMdtValueByKey('Case_Routing_Error_Queue'));
                        processLog += 'Failed to get QUEUE NAME from routing table: \r\n';
                        processLog += queueQueryEx.getMessage() + '\r\n';
                        
                        appWrapperLogList.add(Utilities.createApplicationLog('Warning', 'CallCenterCaseRouting', 'assignCaseToRelevantQueue',
                                                                             caseId, 'Call Center Case Routing', processLog, null, 'Error Log', startTime, logLevelCMD, queueQueryEx));
                    }
                    
                    updateCaseList.add(caseToBeAssignedToQueue);
                }
                
                // update Case
                if (updateCaseList != null && !updateCaseList.isEmpty()) {
                    update updateCaseList;
                }
            }
        } catch (Exception caseAssignFailedEx) {
            System.debug(LoggingLevel.ERROR, caseAssignFailedEx);
            
            String firstCaseId = '';
            if (caseIds != null && !caseIds.isEmpty()) {
                firstCaseId = caseIds[0];
            }
            
            processLog += 'Failed to assign queue for the following Case Ids: \r\n';
            processLog += JSON.serializePretty(caseIds) + '\r\n';
            processLog += 'Root Cause: ' + caseAssignFailedEx.getMessage() + '\r\n';
            
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CallCenterCaseRouting', 'assignCaseToRelevantQueue',
                                                                 firstCaseId, 'Call Center Case Routing', processLog, null, 'Error Log', startTime, logLevelCMD, caseAssignFailedEx));
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
        }
    }
}