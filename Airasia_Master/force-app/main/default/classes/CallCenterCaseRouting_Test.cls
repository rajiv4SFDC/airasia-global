/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/06/2017      Pawan Kumar	        Created
*******************************************************************/
@isTest(SeeAllData=false)
private class CallCenterCaseRouting_Test{
    private static testMethod void testPosCCRouting() {
        Test.startTest();
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Call_Center_Escalation_Team__c', 'Thailand Call Center Escalation');
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'OwnerId',
                'Call_Center_Escalation_Team__c'
                };
                    
                    List < Case > casesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_1', casesList[0].Subject);
        System.assertEquals('Thailand Call Center Escalation', casesList[0].Call_Center_Escalation_Team__c);
        
        // call our routing logic.
        CallCenterCaseRouting.assignCaseToRelevantQueue(new List < String > {
            casesList[0].Id
                });
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_1', updatedCasesList[0].Subject);
        caseFieldValueMap.put('Call_Center_Escalation_Team__c', 'Thailand Call Center Escalation');
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id (19 Nov 2019 - Changed Thailand Call Center - Escalation to Global Client Support)
        String queueId = TestUtility.getQueueIdByName('Global Client Support');
        System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        Test.stopTest();
    }
    
    private static testMethod void testNegCCRouting() {
        Test.startTest();
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_2');
        caseFieldValueMap.put('Call_Center_Escalation_Team__c', null);
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'OwnerId' , 
                'Call_Center_Escalation_Team__c'
                };
                    
                    List < Case > casesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_2', casesList[0].Subject);
        System.assertEquals(null, casesList[0].Call_Center_Escalation_Team__c);
        
        // call our routing logic.
        CallCenterCaseRouting.assignCaseToRelevantQueue(new List < String > {
            casesList[0].Id
                });
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_2', casesList[0].Subject);
        System.assertEquals(null, casesList[0].Call_Center_Escalation_Team__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        String queueId = TestUtility.getQueueIdByName('Error Handling Queue');
        System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
        
        Test.stopTest();
    }
    
    
    private static testMethod void testNoCaseIdsRouting() {
        Test.startTest();
        
        // call our routing logic.
        CallCenterCaseRouting.assignCaseToRelevantQueue(null);
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases();
        System.assertNotEquals(null, updatedCasesList);
        System.assertEquals(0, updatedCasesList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
        
        Test.stopTest();
    }
}