global class CaseInboundEmailHandler implements Messaging.InboundEmailHandler {
    
    static final String THREAD_REGEX = '.*\\[ ref:(.*):ref \\]$';
    static final Pattern THREAD_PATTERN = Pattern.compile(THREAD_REGEX);
    static Matcher THREAD_MATCHER = null;
    public static final String EMAIL_ID = 'airasiasupport@airasia.com';
    static OrgWideEmailAddress OWA = [SELECT Id FROM OrgWideEmailAddress WHERE Address =: EMAIL_ID LIMIT 1];
    static EmailTemplate ET_RESPONSE = [SELECT Id, Subject, Body, HTMLValue FROM EmailTemplate WHERE Name = 'Auto Response - No Support Reply' LIMIT 1];
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Boolean isEmailThread = false;
        try {            
            if(String.isNotBlank(email.subject)) {
                THREAD_MATCHER = THREAD_PATTERN.matcher(email.subject);
                if(THREAD_MATCHER.matches()) {
                    String threadId = THREAD_MATCHER.group(THREAD_MATCHER.groupCount());
                    try {
                        Id caseId = Cases.getCaseIdFromEmailThreadId(threadId);
                        if(caseId != null) {
                            createEmail(caseId, email);
                            isEmailThread = true;
                        }
                    }catch(Exception exp) {}
                }
            } 
            if(!isEmailThread) {
                sendEmail(email.fromAddress);
            }
            result = null;
        } catch(Exception exp) {
            System.debug('Exception in Web to Case - ' + exp);
            result.message = ET_RESPONSE.Body;
            result.success = false;
        }
        return result;
    }
    
    public static void sendEmail(String toAddress) {
        String htmlBody = ET_RESPONSE.HtmlValue;
        
        if(String.isNotBlank(htmlBody)) {
            htmlBody = String.format(htmlBody, new List<String>{getTodaysValue(), getCommunityURL()});
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {toAddress});
        mail.setReplyTo(EMAIL_ID);
        mail.setSaveAsActivity(false);
        mail.setOrgWideEmailAddressId(OWA.Id);
        mail.setSubject(ET_RESPONSE.Subject);
        mail.setPlainTextBody(ET_RESPONSE.Body);
        mail.setHtmlBody(htmlBody);
        mail.setBccSender(false);
        mail.setUseSignature(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public static void createEmail(Id caseId, Messaging.InboundEmail email) {
        List<Attachment> attList = new List<Attachment>();
        
        EmailMessage msg = new EmailMessage();
        msg.subject = email.subject;
        msg.textbody = email.plainTextBody;
        msg.htmlBody = email.htmlBody;
        msg.fromAddress = email.fromAddress;
        msg.ToAddress = EMAIL_ID;
        msg.ParentId = caseId;
        msg.Status = '0';
        msg.Incoming = true;
        msg.fromName = email.fromName;
        insert msg;
        
        if(email.binaryAttachments != null) {
            for(Messaging.Inboundemail.BinaryAttachment file : email.binaryAttachments) {
                Attachment attachment = new Attachment();
                attachment.Name = file.fileName;
                attachment.Body = file.body;
                attachment.ParentId = msg.Id;
                attList.add(attachment);
            }
        }
        if(email.textAttachments!= null) {
            for(Messaging.Inboundemail.TextAttachment file : email.textAttachments) {
                Attachment attachment = new Attachment();
                attachment.Name = file.fileName;
                attachment.Body = Blob.valueof(file.body);
                attachment.ParentId = msg.Id;
                attList.add(attachment);
            }
        }
        if(attList.size() > 0) {
            insert attList;
        }
    }
    
    public static String getTodaysValue() {
        DateTime d = Date.Today() ;
        String todaysValue = d.format('dd MMMM yyyy');
        return todaysValue;
    }
    
    public static String getCommunityURL() {
        String communityURL = '';
        try {
            Network AirAsiaNetwork = [SELECT Id FROM Network WHERE Name ='AirAsia' ];
            ConnectApi.Community  AirAsiaCommunity = ConnectApi.Communities.getCommunity(AirAsiaNetwork.id);
            communityURL = AirAsiaCommunity.siteUrl + '/s';
        }catch(Exception exp) {
            System.debug('Excepton:'+exp);
        }
        return communityURL;
    }
}