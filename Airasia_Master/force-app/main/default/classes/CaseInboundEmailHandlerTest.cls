@isTest
public class CaseInboundEmailHandlerTest {
    @isTest
    public static void CaseInboundEmailHandlerValid(){
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.subject = 'testSubject';
        email.fromName = 'test';
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        envelope.fromAddress = 'airasiasupport@airasia.com';
        
        CaseInboundEmailHandler handler = new CaseInboundEmailHandler();
        handler.handleInboundEmail(email, envelope);
    }
    
    @isTest
    public static void CaseInboundEmailHandlerValid1(){
        Test.startTest();
        
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        Account newPersonAccount = new Account();
        newPersonAccount.Salutation = 'Mr.';
        newPersonAccount.FirstName = 'testFirstname';
        newPersonAccount.LastName = 'testLastName';
        newPersonAccount.PersonEmail = 'test@test.com';
        newPersonAccount.RecordType = personAccountRecordType;
        insert newPersonAccount;
        
        /*Case c = new Case(Case_Language__c = 'English');
        c.Supplied_Title__c='Mr.';
        c.SuppliedName='Test';
        c.Manual_Contact_First_Name__c='test';
        c.Manual_Contact_Last_Name__c='test';
        c.SuppliedEmail='test@test.com';
        c.SuppliedPhone='123123123';
        c.Status='New';
        c.Origin='Live Chat';
        c.RecordTypeId='0120l0000008V08AAE';
        c.Booking_Number__c='DKFJJK';
        c.Type='Refund';
        c.Airline_Code__c='D7';
        c.Subject='Test';
        c.Flight_Number__c='34';
        c.Mobile_Phone_Number__c='78987897';
        c.Mobile_Phone_Country_Code__c='India';
        c.AccountId = newPersonAccount.Id;
        insert c;
        */
        Case c = new Case(); 
        c.AccountId = newPersonAccount.id; 
        c.Subject = 'test case'; 
        c.reason = 'test@test.com'; 
        insert c;
        
        String caseId = c.Id;
        string Case_ThreadID = getThreadId(caseId);
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        email.subject = 'testSubject '+Case_ThreadID;
        email.fromName = 'test';
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        envelope.fromAddress = 'airasiasupport@airasia.com';
        
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.pdf';
        binaryAttachment.body = blob.valueOf('my attachment text');
        //binaryattachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { binaryattachment };
            
            Messaging.InboundEmail.TextAttachment textAttachment = new Messaging.InboundEmail.TextAttachment();
        textAttachment.Filename = 'test.pdf';
        textAttachment.body = 'my attachment text';
        textAttachment.mimeTypeSubType = 'text/plain';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { textAttachment };
            
        CaseInboundEmailHandler handler = new CaseInboundEmailHandler();
        handler.handleInboundEmail(email, envelope);
        CaseInboundEmailHandler.createEmail(caseId, email);
        Test.stopTest();
    }
    private static String getThreadId(String caseId){
        return '[ ref:_' 
            + UserInfo.getOrganizationId().left(4) 
            + '0' 
            + UserInfo.getOrganizationId().mid(11,4) + '._' 
            + caseId.left(4) + '0' 
            + caseId.mid(10,5) + ':ref ]';
    }
}