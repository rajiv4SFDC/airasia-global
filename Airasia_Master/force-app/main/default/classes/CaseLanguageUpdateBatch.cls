global class CaseLanguageUpdateBatch implements Database.Batchable<sObject> {

	private static final Map<String, String> langMap = new Map<String, String>();
	private static final String query = 'SELECT Id, CaseId, LiveChatButtonId, LiveChatButton.WindowLanguage FROM LiveChatTranscript WHERE CaseId != \'\' AND Case.Case_Language__c = \'\'';

	static {
    		langMap.put('en_GB', 'English');
        langMap.put('ms', 'Malay');
        langMap.put('th', 'Thai');
        langMap.put('zh_TW', 'Traditional Chinese');
        langMap.put('ja', 'Japanese');
        langMap.put('ko', 'Korean');
        langMap.put('vi', 'Vietnamese');
        langMap.put('zh_CN', 'Simplified Chinese');
        langMap.put('in', 'Indonesian');
			}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

 	global void execute(Database.BatchableContext BC, List<LiveChatTranscript> scope) {

		Map<String, String> langCaseMap = new Map<String, String>();

		if(scope != null && !scope.isEmpty()) {
			for(LiveChatTranscript transcript : scope) {
				String lang = transcript.LiveChatButtonId != null ? transcript.LiveChatButton.WindowLanguage : '';
				if(String.isNotBlank(lang) && langMap.containsKey(lang)) {
	            lang = langMap.get(lang);
	        } else {
						lang = 'English';
					}
				langCaseMap.put(transcript.caseId, lang);
			}

			if(!langCaseMap.isEmpty()) {
				List<String> caseIds = new List<String>(langCaseMap.keySet());
				List<case> cList = [SELECT Id, Case_Language__c FROM case WHERE Id IN : caseIds];
				if(cList != null && !cList.isEmpty()) {
					for(Case cobj : cList) {
						cobj.Case_Language__c = langCaseMap.get(cobj.Id);
					}
					Database.upsert(cList, false);
				}
			}
		}
	}

	global void finish(Database.BatchableContext BC) {

	}
}