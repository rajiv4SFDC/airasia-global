global with sharing class CaseProcessBatch implements Database.Batchable<sObject>, Database.Stateful{

    public String errorLog = '';
    public String successLog = '';
    public String autoClosedCaseQueueId = '';

    //auto close compliment cases
    public CaseProcessBatch() {

        //autoClosedCaseQueueId;
        Application_Setting__mdt settingRec = [SELECT Value__c FROM Application_Setting__mdt WHERE DeveloperName = :Constants.CUSTOM_METADATA_AUTO_CLOSE_COMPLIMENT_CASES LIMIT 1];
        autoClosedCaseQueueId = settingRec.Value__c;
        System.debug('ta:autoClosedCaseQueueId='+autoClosedCaseQueueId);
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        
            String typeCompliment = Constants.CASE_TYPE_COMPLIMENT;
            String statusClosed = Constants.CASE_STATUS_CLOSED;
            return Database.getQueryLocator('select id from Case where Type =:typeCompliment and Status != :statusClosed');
    }


    global void execute(Database.BatchableContext bc, List<sObject> records){

        try{

            errorLog = '';
            successLog = '';
    
            String errorMessage;
            String successMessage;

            List<Case> caseToUpdate = new List<Case>();
            for(Case caseRecord : (List<Case>)records){

                caseRecord.status = Constants.CASE_STATUS_CLOSED;
                if(autoClosedCaseQueueId != null && autoClosedCaseQueueId != ''){
                    caseRecord.ownerId = autoClosedCaseQueueId;
                }    
                else {
                    caseRecord.ownerId = null;
                }    
                caseToUpdate.add(caseRecord);
            }

            for(Database.SaveResult sr : Database.update(caseToUpdate, false)){
                if(sr.isSuccess()){

                    successMessage = sr.getId();
                    successLog += successMessage + ';;';
                }
                else{

                    errorMessage = 'Id='+sr.getId();
                    for(Database.Error err : sr.getErrors()){

                        errorMessage += ' : Statuscode='+err.getStatusCode() + ' : ErrorMessage=' + err.getMessage() + ' : ErrorFields=' +err.getFields();
                        errorLog += errorMessage + ';;';
                    }
                }
            }
        }    
        catch(Exception ex){
            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_ERROR, 'CaseProcessBatch', 'Execute', '','', 'Error during CaseProcessBatch', null, Constants.APPLICATION_NAME_CASE_PROCESS_BATCH, ex,  System.now(), System.now(), Constants.LOG_TYPE_ERROR_LOG, true, true,   true, true);
        }
    }
    
    global void finish(Database.BatchableContext bc){
        if(errorLog != null && errorLog != ''){
            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_ERROR, 'CaseProcessBatch', 'Execute', '','', 'Error while updating cases', errorLog, Constants.APPLICATION_NAME_CASE_PROCESS_BATCH, null,  System.now(), System.now(), Constants.LOG_TYPE_ERROR_LOG, true, true,   true, true);
        }

        if(successLog != null && successLog != ''){

            GlobalUtility.logMessage(Constants.LOG_DEBUG_LEVEL_INFO, 'CaseProcessBatch', 'Execute', '','', 'Success in updating records', successLog, Constants.APPLICATION_NAME_CASE_PROCESS_BATCH, null,  System.now(), System.now(), Constants.LOG_TYPE_JOB_LOG, true, true, true, true);
        }
        
    }    


}