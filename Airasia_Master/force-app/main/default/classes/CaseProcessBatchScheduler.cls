public with sharing class CaseProcessBatchScheduler implements Schedulable {
    public CaseProcessBatchScheduler() {

    }

    public void execute(SchedulableContext ctx) {

        CaseProcessBatch caseProcessBatch = new CaseProcessBatch();
        Database.executeBatch(caseProcessBatch, 200);
    }
    
    


}