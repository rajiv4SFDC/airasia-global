@isTest(SeeAllData=false)
public with sharing class CaseProcessBatch_Test {
    

    @testSetup static void setupTestData() {

        List<Case> caseToInsert = new List<Case>();
        for(Integer i=0;i<10;i++){
            Case caseRec = new Case();
            caseRec.subject = 'testsub'+i;
            caseRec.Status = 'New';
            caseRec.Type = 'Compliment';
            caseRec.Sub_Category_1__c = 'Booking Changes';
            caseRec.Disposition_Level_1__c = 'No Reply From Guest';
            caseRec.RecordTypeId = '0127F000000DgjAQAS';
            caseToInsert.add(caseRec);
        }
        insert caseToInsert;
    }
    
    
    static testMethod void testCaseProcessBatch() {
        Application_Setting__mdt settingRec = [SELECT Value__c FROM Application_Setting__mdt WHERE DeveloperName = :Constants.CUSTOM_METADATA_AUTO_CLOSE_COMPLIMENT_CASES LIMIT 1];
        Test.startTest();
            CaseProcessBatchScheduler scheduler = new CaseProcessBatchScheduler();
            scheduler.execute(null);
        Test.stopTest();

        List<Case> caseList = [SELECT Id FROM Case WHERE Status =:Constants.CASE_STATUS_CLOSED 
                                AND Type =:Constants.CASE_TYPE_COMPLIMENT 
                                AND OwnerId =:settingRec.Value__c];
        
        //System.assertEquals(10, caseList.size());                        

    }    
}