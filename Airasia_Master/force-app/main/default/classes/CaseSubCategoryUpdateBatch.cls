/* Class: CaseSubCategoryUpdateBatch
 * Description: Used for Case Tagging Data Patching
 * Version: 2.0
 * Last Modified: 15 January 2020 15:30:00 MYT
 * History
 * ===============================================
 * 1.0		XX March 2019	Maaz
 * 2.0		15 January 2020	Alvin Tayag (mtayag@salesforce.com)
 * 
 */
global class CaseSubCategoryUpdateBatch implements Database.Batchable<sObject>{
    
    
   Public String Query = Label.Data_Update_Batch_Query;
   global CaseSubCategoryUpdateBatch(){
        
   }
       
   // Start Method
   global Database.QueryLocator start(Database.BatchableContext BC){
     return Database.getQueryLocator(query);
   }
      
   // Execute Logic
   global void execute(Database.BatchableContext BC, List<Case> scope){
       
       List<Case> ListOfCasesForUpdate = new List<Case>();
       for(Case item : scope){
           	System.debug('Case Number --> ' + item.CaseNumber);
           //15 Jan 2020 - Updated Backup fields and included Disposition Levels
            //taking backup of old sub category values
            item.Old_Sub_Category_1__c = Label.Data_Update_Batch_New_Sub_Cat_1 != '-' ? item.Sub_Category_1__c : item.Old_Sub_Category_1__c;
          	item.Old_Sub_Category_2__c = Label.Data_Update_Batch_New_Sub_Cat_2 != '-' ? item.Sub_Category_2__c : item.Old_Sub_Category_2__c;
          	item.Old_Sub_Category_3__c = Label.Data_Update_Batch_New_Sub_Cat_3 != '-' ? item.Sub_Category_3__c : item.Old_Sub_Category_3__c;
          	item.Old_Disposition_Level_1__c = Label.Data_Update_Batch_New_Disp_1 != '-' ? item.Disposition_Level_1__c : item.Old_Disposition_Level_1__c;
          	item.Old_Disposition_Level_2__c = Label.Data_Update_Batch_New_Disp_2 != '-' ? item.Disposition_Level_2__c : item.Old_Disposition_Level_2__c;
           
            //assigning new sub category values
            item.Sub_Category_1__c = Label.Data_Update_Batch_New_Sub_Cat_1 != '-'? Label.Data_Update_Batch_New_Sub_Cat_1 : item.Sub_Category_1__c;
            item.Sub_Category_2__c = Label.Data_Update_Batch_New_Sub_Cat_2 != '-'? Label.Data_Update_Batch_New_Sub_Cat_2 : item.Sub_Category_2__c;
            item.Sub_Category_3__c = Label.Data_Update_Batch_New_Sub_Cat_3 != '-' ? Label.Data_Update_Batch_New_Sub_Cat_3 : item.Sub_Category_3__c;
            item.Disposition_Level_1__c = Label.Data_Update_Batch_New_Disp_1 != '-' ? Label.Data_Update_Batch_New_Disp_1 : item.Disposition_Level_1__c;
            item.Disposition_Level_2__c = Label.Data_Update_Batch_New_Disp_2 != '-' ? Label.Data_Update_Batch_New_Disp_2 : item.Disposition_Level_2__c;
                      
            //specific for clearing values
            item.Sub_Category_1__c = Label.Data_Update_Batch_New_Sub_Cat_1 == 'null' ? null : item.Sub_Category_1__c;
            item.Sub_Category_2__c = Label.Data_Update_Batch_New_Sub_Cat_2 == 'null'? null : item.Sub_Category_2__c;
            item.Sub_Category_3__c = Label.Data_Update_Batch_New_Sub_Cat_3 == 'null' ? null : item.Sub_Category_3__c;
            item.Disposition_Level_1__c = Label.Data_Update_Batch_New_Disp_1 == 'null' ? null : item.Disposition_Level_1__c;
            item.Disposition_Level_2__c = Label.Data_Update_Batch_New_Disp_2 == 'null' ? null : item.Disposition_Level_2__c;
            ListOfCasesForUpdate.add(item);
       }
       
       System.debug('Before Insert: ' + ListOfCasesForUpdate.size());
       update ListOfCasesForUpdate;
        
   }
     
   global void finish(Database.BatchableContext BC){
           // Logic to be Executed at finish
   }

}

// for testing
// Id batchJobId = Database.executeBatch(new CaseSubCategoryUpdateBatch(), 1);