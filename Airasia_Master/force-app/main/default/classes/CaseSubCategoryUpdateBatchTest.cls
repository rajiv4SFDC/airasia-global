@isTest
public class CaseSubCategoryUpdateBatchTest{
    
    static testmethod void UnitTest(){
        
        Account accountValue = new Account();
        accountValue.Name = 'Test Record';
        insert accountValue;
        
        Contact contactValue = new Contact();
        contactValue.firstName = 'Test Record';
        contactValue.lastName = 'Test Record';
        insert contactValue;    
        
        Case caseValue = new Case();
        caseValue.Subject = 'Test Record - Data Matching';
        caseValue.Sub_Category_1__c = 'General Q&A';
        caseValue.Type = 'Complaint';
        caseValue.AccountId = accountValue.Id;
        caseValue.ContactId = contactValue.Id;
        caseValue.Origin = 'Live Chat';
        caseValue.Status = 'New';
        caseValue.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Auto Case Creation Enquiry/Compliment/Complain').getRecordTypeId();
        insert caseValue;
        
        Test.startTest();
        CaseSubCategoryUpdateBatch c = new CaseSubCategoryUpdateBatch();
        Database.executeBatch(c);
        Test.stopTest();
    }
}