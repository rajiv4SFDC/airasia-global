/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author          Comments
-------------------------------------------------------------
10/07/2017      Pawan Kumar     Created
12/09/2018		Aik Meng		Added TEST method testCaseTriggerOnUpdateWebCaseRouting()
15/09/2018		Aik Meng		Added TEST method testCloseParentCases()
24/10/2018		Aik Meng		Updated testCaseTriggerOnUpdate() to accommodate new Validation Rule
04/07/2019      Alvin Tayag     Moved CloseParentCase Test to CaseTrigger_Test_P2 due to Limits
13/08/2019		Alvin Tayag		Updated MAA Customer Care - High to MAA Customer Care as per the queue renaming exercise
*******************************************************************/
@isTest(SeeAllData = false)
private class CaseTrigger_Test {
    
    private static testMethod void testCaseTriggerOnInsert() {
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'New');
        caseFieldValueMap.put('Manual_Contact_Email__c', 'test@gmail.com');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('SuppliedEmail', 'test@gmail.com');      
        caseFieldValueMap.put('Manual_Contact_Last_Name__c', 'TEST'); 
        caseFieldValueMap.put('Manual_Contact_First_Name__c', 'TEST'); 
        caseFieldValueMap.put('SuppliedName', 'TEST'); 
        caseFieldValueMap.put('SuppliedPhone', '9999999999'); 
        caseFieldValueMap.put('Supplied_Title__c', 'Mr'); 
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSMockImpl());
        
        // will cover insert part of trigger
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c','Manual_Contact_Email__c','Refund_Status__c','SuppliedEmail','Manual_Contact_Last_Name__c','Manual_Contact_First_Name__c','SuppliedName','SuppliedPhone',
'Supplied_Title__c'
                };
                    
        List < Case > createdCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, createdCaseList);
        System.assertEquals(1, createdCaseList.size());
        System.assertEquals('Test_Case_1', createdCaseList[0].Subject);
        System.assertEquals('New', createdCaseList[0].Send_to_RPS__c);
        System.assertEquals('test@gmail.com', createdCaseList[0].Manual_Contact_Email__c);
        System.assertEquals('New', createdCaseList[0].Refund_Status__c);
        System.assertEquals('New', createdCaseList[0].Refund_Status__c);
        System.assertEquals('test@gmail.com', createdCaseList[0].SuppliedEmail);
        System.assertEquals('TEST', createdCaseList[0].Manual_Contact_Last_Name__c);
        System.assertEquals('TEST', createdCaseList[0].Manual_Contact_First_Name__c);
        System.assertEquals('TEST', createdCaseList[0].SuppliedName);
        System.assertEquals('9999999999', createdCaseList[0].SuppliedPhone);
        System.assertEquals('Mr', createdCaseList[0].Supplied_Title__c);
        CaseTriggerHandler.getPersonAccount(createdCaseList[0]) ;
        
        Test.stopTest();
        
        // after calling stop only future method is completed
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCaseList);
        System.assertEquals(1, updatedCaseList.size());
        System.assertEquals('Test_Case_1', updatedCaseList[0].Subject);
        System.assertEquals('Success', updatedCaseList[0].Send_to_RPS__c);
        System.assertEquals('test@gmail.com', createdCaseList[0].Manual_Contact_Email__c);
        System.assertEquals('New', createdCaseList[0].Refund_Status__c);
        
        // make sure no OB records
        List < Outbound_Message__c > obMsgList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages();
        System.assertnotEquals(null, obMsgList);
        System.assertEquals(0, obMsgList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(0,appLogs.size());
        
    }
    
    private static testMethod void testCaseTriggerOnUpdateRPSOnly() {

        // query record type
        RecordType refund = [Select Id, name From RecordType where sobjecttype = 'Case'
                             And Name = 'Refund Case'
                            ];

        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'In Progress');
        caseFieldValueMap.put('Manual_Contact_Email__c', 'test@gmail.com');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', refund.Id);
        
        // will cover insert part of trigger
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSMockImpl());
        
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject','Send_to_RPS__c', 'Airline_Refund_RPS_Case_Update__c','Manual_Contact_Email__c','Refund_Status__c',
            'Airline_Code__c','Airline_Code_Navitaire__c','Booking_Number__c','Booking_Payment__c',
            'Write_Off_Currency_Code__c','Write_Off_Amount__c','Refund_Amount__c','Refund_Currency_Code__c'
        };
                    
                    // updated case with Send_to_RPS__c='Success' due to insert
                    List < Case > newCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, newCaseList);
        System.assertEquals(1, newCaseList.size());
        System.assertEquals('Test_Case_1', newCaseList[0].Subject);
        System.assertEquals('In Progress', newCaseList[0].Send_to_RPS__c);
        System.assertEquals('test@gmail.com', newCaseList[0].Manual_Contact_Email__c);
        caseFieldValueMap.put('Refund_Status__c', 'New');
        
        // Now update from something else to New for Update part of trigger
        newCaseList[0].Send_to_RPS__c = 'New';
        newCaseList[0].Airline_Refund_RPS_Case_Update__c = true;
        newCaseList[0].Refund_Status__c = 'Validated';

        // Added: 24Oct2018 - additional fields required due to new Validation Rule
        newCaseList[0].Airline_Code__c='AK';
        newCaseList[0].Airline_Code_Navitaire__c = 'AK'; 
        newCaseList[0].Booking_Number__c = 'ABC123'; 
        newCaseList[0].Booking_Payment__c = 'Credit Card';
        newCaseList[0].Write_Off_Currency_Code__c = 'MYR'; 
        newCaseList[0].Write_Off_Amount__c = 100; 
        newCaseList[0].Refund_Currency_Code__c = 'MYR'; 
        newCaseList[0].Refund_Amount__c = 100;
        
        Test.setMock(HttpCalloutMock.class, new RPSRestWSMockImpl());
        update newCaseList[0];
        Test.stopTest();
        
        List < Case > updatedCases = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCases);
        System.assertEquals(1, updatedCases.size());
        System.assertEquals('Test_Case_1', updatedCases[0].Subject);
        System.assertEquals('Success', updatedCases[0].Send_to_RPS__c);
        System.assertEquals(false, updatedCases[0].Airline_Refund_RPS_Case_Update__c);
        System.assertEquals('Validated', updatedCases[0].Refund_Status__c );
        // make sure no OB records
        List < Outbound_Message__c > obMsgList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages();
        System.assertnotEquals(null, obMsgList);
        System.assertEquals(0, obMsgList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(0,appLogs.size());
    }
    
    private static testMethod void testCaseTriggerOnUpdate() {

        // query record type
        RecordType refund = [Select Id, name From RecordType where sobjecttype = 'Case'
                             And Name = 'Refund Case'
                            ];

        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'In Progress');
        caseFieldValueMap.put('Manual_Contact_Email__c', 'test@gmail.com');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', refund.Id);
        
        // will cover insert part of trigger
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSMockImpl());
        
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject','Send_to_RPS__c','Manual_Contact_Email__c','Refund_Status__c',
            'Airline_Code__c','Airline_Code_Navitaire__c','Booking_Number__c','Booking_Payment__c',
            'Write_Off_Currency_Code__c','Write_Off_Amount__c','Refund_Amount__c','Refund_Currency_Code__c'
        };
                    
                    // updated case with Send_to_RPS__c='Success' due to insert
                    List < Case > newCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, newCaseList);
        System.assertEquals(1, newCaseList.size());
        System.assertEquals('Test_Case_1', newCaseList[0].Subject);
        System.assertEquals('In Progress', newCaseList[0].Send_to_RPS__c);
        System.assertEquals('test@gmail.com', newCaseList[0].Manual_Contact_Email__c);
        caseFieldValueMap.put('Refund_Status__c', 'New');
        
        // Now update from something else to New for Update part of trigger
        newCaseList[0].Send_to_RPS__c = 'New';
        newCaseList[0].Refund_Status__c = 'Validated';

        // Added: 24Oct2018 - additional fields required due to new Validation Rule
        newCaseList[0].Airline_Code__c='AK';
        newCaseList[0].Airline_Code_Navitaire__c = 'AK'; 
        newCaseList[0].Booking_Number__c = 'ABC123'; 
        newCaseList[0].Booking_Payment__c = 'Credit Card';
        newCaseList[0].Write_Off_Currency_Code__c = 'MYR'; 
        newCaseList[0].Write_Off_Amount__c = 100; 
        newCaseList[0].Refund_Currency_Code__c = 'MYR'; 
        newCaseList[0].Refund_Amount__c = 100;
        
        Test.setMock(HttpCalloutMock.class, new RPSRestWSMockImpl());
        update newCaseList[0];
        Test.stopTest();
        
        List < Case > updatedCases = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCases);
        System.assertEquals(1, updatedCases.size());
        System.assertEquals('Test_Case_1', updatedCases[0].Subject);
        System.assertEquals('Success', updatedCases[0].Send_to_RPS__c);
        System.assertEquals('Validated', updatedCases[0].Refund_Status__c );
        // make sure no OB records
        List < Outbound_Message__c > obMsgList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages();
        System.assertnotEquals(null, obMsgList);
        System.assertEquals(0, obMsgList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(0,appLogs.size());
    }
    
    private static testMethod void testCaseTriggerOnInsertOtherThenNew() {
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'test');
        caseFieldValueMap.put('Manual_Contact_Email__c', 'test@gmail.com');
         
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSMockImpl());
        
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCase(caseFieldValueMap);
        
        Test.stopTest();
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c','Manual_Contact_Email__c'
                };
                    
        // after calling stop only future method is completed
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCaseList);
        System.assertEquals(1, updatedCaseList.size());
        System.assertEquals('Test_Case_1', updatedCaseList[0].Subject);
        System.assertEquals('test', updatedCaseList[0].Send_to_RPS__c);
         System.assertEquals('test@gmail.com', updatedCaseList[0].Manual_Contact_Email__c);
        
        // make sure no OB records
        List < Outbound_Message__c > obMsgList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages();
        System.assertnotEquals(null, obMsgList);
        System.assertEquals(0, obMsgList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(0,appLogs.size());
    }
    
    private static testMethod void testCaseTriggerOnUpdateOtherThenNew() {
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'test');
        caseFieldValueMap.put('Manual_Contact_Email__c', 'test@gmail.com');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSMockImpl());
        
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c','Manual_Contact_Email__c'
                };
                    
        // after calling stop only future method is completed
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCaseList);
        System.assertEquals(1, updatedCaseList.size());
        System.assertEquals('Test_Case_1', updatedCaseList[0].Subject);
        System.assertEquals('test', updatedCaseList[0].Send_to_RPS__c);
        System.assertEquals('test@gmail.com', updatedCaseList[0].Manual_Contact_Email__c);
        
        updatedCaseList[0].Send_to_RPS__c='xyzstaus';
        updatedCaseList[0].Manual_Contact_Email__c='test1@gmail.com';
        update updatedCaseList[0];
        
        Test.stopTest();
        
        // after calling stop only future method is completed
        List < Case > afterUpdateCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, afterUpdateCaseList);
        System.assertEquals(1, afterUpdateCaseList.size());
        System.assertEquals('Test_Case_1', afterUpdateCaseList[0].Subject);
        System.assertEquals('xyzstaus', afterUpdateCaseList[0].Send_to_RPS__c);
        System.assertEquals('test1@gmail.com', afterUpdateCaseList[0].Manual_Contact_Email__c);
        
        // make sure no OB records
        List < Outbound_Message__c > obMsgList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages();
        System.assertnotEquals(null, obMsgList);
        System.assertEquals(0, obMsgList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(0,appLogs.size());
    }
    
    private static testMethod void testRPSDataSendingFailed() {
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'New');
        caseFieldValueMap.put('Manual_Contact_Email__c', 'test@gmail.com');
        
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSFailureMockImpl());

        // will cover insert part of trigger
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c','Manual_Contact_Email__c'
                };
                    
        List < Case > createdCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, createdCaseList);
        System.assertEquals(1, createdCaseList.size());
        System.assertEquals('Test_Case_1', createdCaseList[0].Subject);
        System.assertEquals('New', createdCaseList[0].Send_to_RPS__c);
        System.assertEquals('test@gmail.com', createdCaseList[0].Manual_Contact_Email__c);
        Test.stopTest();
        
        // after calling stop only future method is completed
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCaseList);
        System.assertEquals(1, updatedCaseList.size());
        System.assertEquals('Test_Case_1', updatedCaseList[0].Subject);
        System.assertEquals('New', updatedCaseList[0].Send_to_RPS__c);
        System.assertEquals('test@gmail.com', createdCaseList[0].Manual_Contact_Email__c);
        // make sure no OB records
        List < Outbound_Message__c > obMsgList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages();
        System.assertnotEquals(null, obMsgList);
        System.assertEquals(1, obMsgList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size()); //Additional
    }
    
// ADDED: 12Sep2018
// TEST Method for CaseTriggerHandler.sendDataToCaseRouting
// NOTE: Test logic borrowed from WebCaseRouting_Test.apxc
    private static testMethod void testCaseTriggerOnUpdateWebCaseRouting() {
        // create test case  
        Map<String,Object> caseFieldValueMap = new Map <String,Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Origin', 'Web');
        caseFieldValueMap.put('Call_Apex_to_set_case_owner__c', FALSE);
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        
        Test.startTest();
        
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List<String> queryFieldList = new List<String> {
            'Subject',
            'Call_Apex_to_set_case_owner__c',
            'OwnerId',
            'Origin',
            'Priority',
            'Case_Language__c',
            'Airline_Code__c',
            'Type',
            'Sub_Category_1__c',
            'Sub_Category_2__c'
		};

        // VERIFY created record
        List<Case> updatedCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertNotEquals(null, updatedCaseList);
        System.assertEquals(1, updatedCaseList.size());
        System.assertEquals('Test_Case_1', updatedCaseList[0].Subject);
        System.assertEquals('Web', updatedCaseList[0].Origin);
        System.assertEquals(FALSE, updatedCaseList[0].Call_Apex_to_set_case_owner__c);
        System.assertEquals('Low', updatedCaseList[0].Priority);
        System.assertEquals('English', updatedCaseList[0].Case_Language__c);
        System.assertEquals(null, updatedCaseList[0].Airline_Code__c);
        System.assertEquals('Refund', updatedCaseList[0].Type);
        System.assertEquals(null, updatedCaseList[0].Sub_Category_1__c);
        System.assertEquals(null, updatedCaseList[0].Sub_Category_2__c);
        String queueId = TestUtility.getQueueIdByName('Refund Pending');
        System.assertEquals(queueId, updatedCaseList[0].OwnerId);  // Assigned to Refund Pending Queue on creation
        
        // UPDATE created record -> This should trigger the method sendDataToWebCaseRouting()
        updatedCaseList[0].Call_Apex_to_set_case_owner__c=TRUE;
        updatedCaseList[0].Priority='High';
        update updatedCaseList[0];
        
        Test.stopTest();
        
        // QUERY & VERIFY Case record updated by routing class.
        List < Case > afterUpdateCaseList = (List<Case>) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, afterUpdateCaseList);
        System.assertEquals(1, afterUpdateCaseList.size());
        System.assertEquals('Test_Case_1', afterUpdateCaseList[0].Subject);
        System.assertEquals('High', afterUpdateCaseList[0].Priority);
        // assert queue with id case owner id
        queueId = TestUtility.getQueueIdByName('MAA Customer Care');
        System.assertEquals(queueId, afterUpdateCaseList[0].OwnerId);  // Should be assigned to High Priority Queue
        
        // make sure no OB records
        List < Outbound_Message__c > obMsgList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages();
        System.assertnotEquals(null, obMsgList);
        System.assertEquals(0, obMsgList.size());      
        
    }  // End - testCaseTriggerOnUpdateWebCaseRouting()
    
}