/**
 * @File Name          : CaseTrigger_Test_P2.cls
 * @Description        : Split off from Original Test Class due to Limits
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 7/4/2019, 5:15:07 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/4/2019, 5:14:15 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/

@isTest
public class CaseTrigger_Test_P2 {
  @testSetup
    private static void testSetup() {

    	String childRefundCaseRecId = [SELECT Id, 
                                       Name 
                                FROM   RecordType 
                                WHERE  sobjecttype = 'Case' 
                                AND    Name = 'Child Refund Case'
                               ].Id;
        
        String parentRefundCaseRecId = [SELECT Id, 
                                        Name 
                                 FROM   RecordType 
                                 WHERE  sobjecttype = 'Case' 
                                 AND    Name = 'Refund Case'
                                ].Id;  
        
        // CREATE Parent Case record
        Map<String,Object> caseFieldValueMap = new Map <String,Object>();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Origin', 'Web');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c','Other');
		caseFieldValueMap.put('Disposition_Level_1__c', null);  // Make sure this field is NULL
        caseFieldValueMap.put('Sub_Category_2__c', null);
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);
        caseFieldValueMap.put('ADA_Chatter_Token__c', 'Test');
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCase(caseFieldValueMap);
        
        List<Case> parentCaseList = [SELECT Id, Subject, ParentId, Status, Sub_Category_1__c, Disposition_Level_1__c FROM Case WHERE Subject='Parent_Case'];
        
		Map<String,Object> childCaseFieldValueMap = new Map <String,Object>();
        childCaseFieldValueMap.put('Subject', 'Child_Case');
        childCaseFieldValueMap.put('Origin', 'Web');
        childCaseFieldValueMap.put('Priority', 'Low');
        childCaseFieldValueMap.put('Case_Language__c', 'English');
        childCaseFieldValueMap.put('Airline_Code__c', null);
        childCaseFieldValueMap.put('Type', 'Refund');
        childCaseFieldValueMap.put('Sub_Category_1__c','Other');
        childCaseFieldValueMap.put('Disposition_Level_1__c','No Disposition Required');  
        childCaseFieldValueMap.put('Sub_Category_2__c', null);
        childCaseFieldValueMap.put('ParentId', parentCaseList[0].Id);  // Assign ID of Parent Case record
        childCaseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCases(childCaseFieldValueMap, 2);  // Create 2 Child Case records
    }
    
// ADDED: 17Sep2018
// TEST Method for CaseTriggerHandler.closeParentCases()
    private static testMethod void testCaseTriggerOnUpdateCloseParentCases() {
    	String childRefundCaseRecId = [SELECT Id, 
                                       Name 
                                FROM   RecordType 
                                WHERE  sobjecttype = 'Case' 
                                AND    Name = 'Child Refund Case'
                               ].Id;
        
        String parentRefundCaseRecId = [SELECT Id, 
                                        Name 
                                 FROM   RecordType 
                                 WHERE  sobjecttype = 'Case' 
                                 AND    Name = 'Refund Case'
                                ].Id;   
         
        Test.startTest();
        
        // VERIFY created Parent record
        List<Case> parentCaseList = [SELECT Id, Subject, ParentId, Status, Sub_Category_1__c, Disposition_Level_1__c FROM Case WHERE Subject='Parent_Case'];
        System.assertNotEquals(null, parentCaseList);
        System.assertEquals(1, parentCaseList.size());
        System.assertEquals('Parent_Case', parentCaseList[0].Subject);
        System.assertEquals(null, parentCaseList[0].ParentId);
        System.assertEquals('New', parentCaseList[0].Status);  // Parent Case status is 'New' by default
        System.assertEquals('Other', parentCaseList[0].Sub_Category_1__c);
		System.assertEquals(null, parentCaseList[0].Disposition_Level_1__c);

        // VERIFY created Child records
        List<Case> childCaseList = [SELECT Id, Subject, ParentId, Status, Disposition_Level_1__c FROM Case WHERE Subject='Child_Case'];
        System.assertNotEquals(null, childCaseList);
        System.assertEquals(2, childCaseList.size());
        System.assertEquals('Child_Case', childCaseList[0].Subject);
        System.assertEquals(parentCaseList[0].Id, childCaseList[0].ParentId);
        System.assertEquals('New', childCaseList[0].Status);  // Child Case status is 'New' by default
         
        // UPDATE 1st Child Case status to 'Closed' -> This should trigger the method CloseParentCases()
        childCaseList[0].Status='Closed';
        // NOTE: Validation Rule enforces following 2 fields to be filled when closing case
        // 1. Sub_Category_1__c
        // 2. Disposition_Level_1__c
        update childCaseList[0];

        // VERIFY Parent Case record remains NOT closed.
        List<Case> afterUpdateCaseList = [SELECT Id, Status FROM Case WHERE Subject='Parent_Case'];
        System.assertNotEquals(null, afterUpdateCaseList);
        System.assertEquals(1, afterUpdateCaseList.size());
        System.assertNotEquals('Closed', afterUpdateCaseList[0].Status);  // Parent Case status should NOT be 'Closed'

        // UPDATE 2nd Child Case status to 'Closed' -> This should trigger the method CloseParentCases()
        childCaseList[1].Status='Closed';
        update childCaseList[1];
        
        // VERIFY Parent Case record has been closed (as BOTH Child Cases are closed)
        afterUpdateCaseList = [SELECT Id, Status, Disposition_Level_1__c FROM Case WHERE Subject='Parent_Case'];
        System.assertNotEquals(null, afterUpdateCaseList);
        System.assertEquals(1, afterUpdateCaseList.size());
        System.assertEquals('Closed', afterUpdateCaseList[0].Status);  // Parent Case status should be 'Closed'
		System.assertEquals('No Disposition Required', afterUpdateCaseList[0].Disposition_Level_1__c);  // Empty field was SET by trigger 
        
        // CREATE a 3rd Child Case record & assign to Parent case
        Map<String,Object> childCaseFieldValueMap = new Map <String,Object>();
        childCaseFieldValueMap.put('Subject', 'Child_Case_3');
        childCaseFieldValueMap.put('Origin', 'Web');
        childCaseFieldValueMap.put('Priority', 'Low');
        childCaseFieldValueMap.put('Case_Language__c', 'English');
        childCaseFieldValueMap.put('Airline_Code__c', null);
        childCaseFieldValueMap.put('Type', 'Refund');
        childCaseFieldValueMap.put('Sub_Category_1__c','Other');
        childCaseFieldValueMap.put('Disposition_Level_1__c','No Disposition Required');  
        childCaseFieldValueMap.put('Sub_Category_2__c', null);
        childCaseFieldValueMap.put('ParentId', parentCaseList[0].Id);  // Assign ID of Parent Case record
        childCaseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCases(childCaseFieldValueMap, 1);  // Create 1 Child Case record
        
        // VERIFY created 3rd Child record
        List<Case> newChildCaseList = [SELECT Id, Subject, ParentId, Status, Disposition_Level_1__c FROM Case WHERE Subject='Child_Case_3'];
        System.assertNotEquals(null, newChildCaseList);
        System.assertEquals(1, newChildCaseList.size());
        System.assertEquals('Child_Case_3', newChildCaseList[0].Subject);
        System.assertEquals(parentCaseList[0].Id, newChildCaseList[0].ParentId);
        System.assertEquals('New', newChildCaseList[0].Status);  // Child Case status is 'New' by default
        
        // UPDATE 3rd Child Case status to 'Closed' -> This should trigger the method CloseParentCases()
        // Should NOT cause System.FinalException: Cannot modify a collection while it is being iterated.
        newChildCaseList[0].Status='Closed';
        update newChildCaseList[0];
        
        // VERIFY 3rd Child record's Status
        newChildCaseList = [SELECT Id, Subject, ParentId, Status FROM Case WHERE Subject='Child_Case_3'];
        System.assertEquals('Closed', newChildCaseList[0].Status);  // Child Case status shoud be 'Closed'
        
        Test.stopTest();

    } // End - testCaseTriggerOnUpdateCloseParentCases()
    
    public static testMethod void testLiveChatTranscript() {
        
		Test.setMock(HttpCalloutMock.class, new AVATranscriptMock());
        // create test case  
        Map<String,Object> caseFieldValueMap = new Map <String,Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Origin', 'Live Chat');
        caseFieldValueMap.put('Call_Apex_to_set_case_owner__c', FALSE);
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        caseFieldValueMap.put('ADA_Chatter_Id__c', 'ABC');
        caseFieldValueMap.put('ADA_Chatter_Token__c', 'ABC');
        
        Test.startTest();
        
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCase(caseFieldValueMap);
        Test.stopTest();
        
        
    }
    
    public static testMethod void testB2BBotTranscript() {
        
		Test.setMock(HttpCalloutMock.class, new AVATranscriptMock());
        // create test case  
        Map<String,Object> caseFieldValueMap = new Map <String,Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Origin', 'B2B Bot');
        caseFieldValueMap.put('Call_Apex_to_set_case_owner__c', FALSE);
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        caseFieldValueMap.put('ADA_Chatter_Id__c', 'ABC');
        caseFieldValueMap.put('ADA_Chatter_Token__c', 'ABC');
        
        Test.startTest();
        
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCase(caseFieldValueMap);
        Test.stopTest();
        
        
    }
    
    public static testMethod void testWeChatBotTranscript() {
        
		Test.setMock(HttpCalloutMock.class, new AVATranscriptMock());
        // create test case  
        Map<String,Object> caseFieldValueMap = new Map <String,Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Origin', 'WeChat Bot');
        caseFieldValueMap.put('Call_Apex_to_set_case_owner__c', FALSE);
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        caseFieldValueMap.put('ADA_Chatter_Id__c', 'ABC');
        caseFieldValueMap.put('ADA_Chatter_Token__c', 'ABC');
        
        Test.startTest();
        
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCase(caseFieldValueMap);
        Test.stopTest();
        
        
    }
    
    public static testMethod void testAdaBotParse() {
        String description = 'full_name: alvin\r\n' +
                                'customer_email: mtayag@salesforce.com\r\n' +
                                'booking_id: none\r\n' +
                                'Chatter ID: 5dd2082ef2ca27e61806c462\r\n' +
                                'Chatter Token: 87309862-e7ba-416d-b56d-4354ac8883cd\r\n' +
                                'Category: GeneralQA\r\n' +
                                'Case_Language: English\r\n' +
                                'Origin: Live Chat\r\n' +
                                'ButtonId: 5737F000000GzCj\r\n' +
                                'introshown: False\r\n' +
                                'test_user: 1\r\n' +
                                'browser: chrome\r\n' +
                                'browser_version: 78.0.3904.97\r\n' +
                                'device: macos\r\n' +
                                'language: en\r\n' +
                                'user_agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36\r\n' +
                                'chatter_id: 5dd2082ef2ca27e61806c462\r\n' +
                                'chatter_token: 87309862-e7ba-416d-b56d-4354ac8883cd\r\n';
        // create test case  
        Map<String,Object> caseFieldValueMap = new Map <String,Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Origin', 'AdaBot');
        caseFieldValueMap.put('Description', description);
        Test.startTest();
        
        TestUtility.createCase(caseFieldValueMap);
        
        Test.stopTest();
        Case newCase = [SELECT Origin,
                        		Ada_Chatter_Id__c,
                        		Ada_Chatter_Token__c,
                        		Type
                        FROM Case 
                        WHERE Ada_Chatter_Id__c='5dd2082ef2ca27e61806c462'][0];        
        System.assertEquals('Live Chat', newCase.Origin);      
        System.assertEquals('5dd2082ef2ca27e61806c462', newCase.Ada_Chatter_Id__c);      
        System.assertEquals('87309862-e7ba-416d-b56d-4354ac8883cd', newCase.Ada_Chatter_Token__c);  
        System.assertEquals('Enquiry/Request', newCase.Type);      
        
    }
    
    public static testMethod void testClosedCase() {
        
		Test.setMock(HttpCalloutMock.class, new AVATranscriptMock());
        // create test case  
        Map<String,Object> caseFieldValueMap = new Map <String,Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Origin', 'Web');
        caseFieldValueMap.put('Call_Apex_to_set_case_owner__c', FALSE);
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        caseFieldValueMap.put('Status', 'New');
        
        Test.startTest();
        
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        TestUtility.createCase(caseFieldValueMap);
        Case newCase = [SELECT Id, Subject, ParentId, Status, Disposition_Level_1__c FROM Case WHERE Subject='Test_Case_1'][0];
        //Test Backdate Close
            List<CaseMilestone> caseMilestones = [SELECT Id,
                                                         CaseId,
                                                         CompletionDate
                                                  FROM   CaseMilestone 
                                                  WHERE  IsCompleted = FALSE 
                                                  	AND	 CaseId =: newCase.Id
                                                 ];
        
		System.assertEquals(1, caseMilestones.size());
        
        newCase.Status = 'Closed';
        //newCase.ClosedDate = DateTime.now().addHours(-1);
        
        update newCase;
        caseMilestones = [SELECT Id,
                                 CaseId,
                                 CompletionDate
                          FROM   CaseMilestone 
                          WHERE  IsCompleted = FALSE 
                            AND	 CaseId =: newCase.Id
                         ];
        
		System.assertEquals(0, caseMilestones.size());
        Test.stopTest();
    }
}