/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
10/07/2017      Pawan Kumar         Created
*******************************************************************/
public without sharing class CaseUpdateWithPAC {
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    public static String processLog = '';
    private static Map < String, String > userLanguageCodes;
    private static String USER_DEFAULT_LANGUAGE;
    
    public static String recordTypeId {
        get;
        set;
    }
    
    public static String profileId {
        get;
        set;
    }
    
    public static String personAcctOwnerRowId {
        get;
        set;
    }
    
    
    public static Map < String, Account > personAccountMap;
    public static Map < String, String > caseEmailNIdMap;
    
    static {
        // query Record Type Id for 'Person Account'
        recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Person Account'
                        and SObjectType = 'Account'
                        AND IsPersonType = True
                       ].Id;
        
        // query community profile ID
        profileId = [select Name from Profile WHERE
                     Name = : Utilities.getAppSettingsMdtValueByKey('COMMUNITY_LOGIN_USER_PROFLE_NAME')
                    ].Id;
        
        // user language code
        userLanguageCodes = new Map < String, String > ();
        for (PicklistEntry entry: User.LanguageLocaleKey.getDescribe().getPicklistValues()) {
            userLanguageCodes.put(entry.getLabel(), entry.getValue());
        }
        
        // to user default language when case language empty
        USER_DEFAULT_LANGUAGE = Utilities.getAppSettingsMdtValueByKey('COMMUNITY_USER_DEFAULT_LANGUAGE');
        
        // assigning PA owner
        personAcctOwnerRowId = [Select Id from User where
                                username = : Utilities.getAppSettingsMdtValueByKey('PERSON_ACCOUNT_OWNER_USERNAME')
                               ].Id;
    }
    
    @InvocableMethod
    public static void updateCases(List < ID > caseIds) {
        callFutureUpdateCases(caseIds);
    }
    
    @future(callout = true)
    public static void callFutureUpdateCases(List < ID > caseIds) {
        List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
        List < Case > updateCaseList = new List < Case > ();
        Set < String > invalidCaseSet = new Set < String > ();
        Map < String, String > caseWithEmptyEmailMap = new Map < String, String > ();
        Map < String, String > caseEmailLangMap = new Map < String, String > ();
        
        personAccountMap = new Map < String, Account > ();
        caseEmailNIdMap = new Map < String, String > ();
        
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('CaseUpdatePAC_Log_Level_CMD_NAME'));
        
        // get System Setting details for Logging
        String b2bCaseOriginVals = Utilities.getAppSettingsMdtValueByKey('B2B_CASE_ORIGIN');
        List < String > b2bOrginList = new List < String > ();
        if (String.isNotEmpty(b2bCaseOriginVals)) {
            b2bOrginList = b2bCaseOriginVals.split(',');
        }
        
        try {
            
            List < Case > caseList = [Select Id, Origin, ContactEmail, SuppliedEmail, SuppliedName, Customer_Number__c, FindPersonDetails__c,
                                      AccountId, ContactId, Case_Language__c, Manual_Contact_First_Name__c, Manual_Contact_Last_Name__c, Manual_Contact_Email__c, Account.PersonEmail,SuppliedPhone, Supplied_Title__c
                                      from Case where Id IN: caseIds AND Origin Not IN: b2bOrginList
                                     ];
            System.debug('Very First :caseList: ' + caseList);
            
            // filter invalid case where email id not there
            for (Case eachCase: caseList) {
                if (String.isNotEmpty(eachCase.SuppliedEmail)) {
                    if (caseEmailNIdMap.containsKey(eachCase.SuppliedEmail)) {
                        caseEmailNIdMap.put(eachCase.SuppliedEmail, caseEmailNIdMap.get(eachCase.SuppliedEmail) + ',' + eachCase.Id);
                    } else {
                        caseEmailNIdMap.put(eachCase.SuppliedEmail, eachCase.Id);
                        caseEmailLangMap.put(eachCase.SuppliedEmail, eachCase.Case_Language__c);
                    }
                } else {
                    // check customer number then get JSON string and try to get Email
                    String caseEmailId = getEmailIdFromPersonJsonString(eachCase.Customer_Number__c, eachCase.FindPersonDetails__c);
                    if (String.isNotEmpty(caseEmailId)) {
                        if (caseEmailNIdMap.containsKey(caseEmailId)) {
                            caseEmailNIdMap.put(caseEmailId, caseEmailNIdMap.get(caseEmailId) + ',' + eachCase.Id);
                        } else {
                            caseEmailNIdMap.put(caseEmailId, eachCase.Id);
                            caseEmailLangMap.put(caseEmailId, eachCase.Case_Language__c);
                        }
                        caseWithEmptyEmailMap.put(eachCase.Id, caseEmailId);
                    } else {
                        
                        // check manual email is there - START
                        if (String.isNotEmpty(eachCase.Manual_Contact_Email__c) 
                            && String.isNotEmpty(eachCase.Manual_Contact_Last_Name__c)) {
                            if (caseEmailNIdMap.containsKey(eachCase.Manual_Contact_Email__c)) {
                                caseEmailNIdMap.put(eachCase.Manual_Contact_Email__c, caseEmailNIdMap.get(eachCase.Manual_Contact_Email__c) + ',' + eachCase.Id);
                            } else {
                                caseEmailNIdMap.put(eachCase.Manual_Contact_Email__c, eachCase.Id);
                                caseEmailLangMap.put(eachCase.Manual_Contact_Email__c, eachCase.Case_Language__c);
                            }
                            caseWithEmptyEmailMap.put(eachCase.Id, eachCase.Manual_Contact_Email__c);
                        } else {
                            
                            // check Person Account has email.
                            if (String.isNotEmpty(eachCase.Account.PersonEmail)) {
                                if (caseEmailNIdMap.containsKey(eachCase.Account.PersonEmail)) {
                                    caseEmailNIdMap.put(eachCase.Account.PersonEmail, caseEmailNIdMap.get(eachCase.Account.PersonEmail) + ',' + eachCase.Id);
                                } else {
                                    caseEmailNIdMap.put(eachCase.Account.PersonEmail, eachCase.Id);
                                    caseEmailLangMap.put(eachCase.Account.PersonEmail, eachCase.Case_Language__c);
                                }
                                caseWithEmptyEmailMap.put(eachCase.Id, eachCase.Account.PersonEmail);
                            } else {
                                // for Case where email Id,is not there   
                                invalidCaseSet.add(eachCase.Id);
                            }
                        }
                    }
                }
                
                // if any invalid case?
                if (String.isNotEmpty(processLog)) {
                    processLog = 'The cases which do not have email populated :\r\n' + JSON.serializePretty(invalidCaseSet) + '\r\n';
                }
            }
            System.debug('Invalid Case List : ' + invalidCaseSet);
            System.debug('Final Case Email N Case Lang Map : ' + caseEmailLangMap);
            // filter invalid case where email id not there - END
            
            // collects all existing Account Map with unique email
            for (List < Account > personAccountList: [Select Id, PersonContactId, Salutation, FirstName, LastName, Nationality__pc,
              Date_of_Birth__pc, Big_Member_ID__pc, Customer_ID__pc, PersonMobilePhone, BillingStreet, BillingCity,
              BillingState, BillingPostalCode, BillingCountry, PersonEmail, OwnerId From Account
              WHERE IsPersonAccount = true AND PersonEmail IN: caseEmailNIdMap.keyset()
             ]) {
                 for (Account eachPersonAccount: personAccountList) {
                     if (String.isNotEmpty(eachPersonAccount.PersonEmail)) {
                         personAccountMap.put(eachPersonAccount.PersonEmail, eachPersonAccount);
                     }
                 }
             }
            System.debug('Existing Person Account: ' + personAccountMap);
            
            // actual processing
            for (Case eachCase: caseList) {
                try {
                    if (!invalidCaseSet.contains(eachCase.id)) {
                        String caseOrigin = eachCase.Origin;
                        String customerNumber = eachCase.Customer_Number__c;
                        
                        // is PA associated to case?
                        if (String.isEmpty(eachCase.AccountId)) {
                            System.debug('Person Account is NOT associated with Case: ' + eachCase.id);
                            executeUpdateCaseProcess(customerNumber, eachCase);
                        } else {
                            System.debug('Person Account is associated with Case: ' + eachCase.id);
                            if (isCaseOriginInScope(caseOrigin)) {
                                executeUpdateCaseProcess(customerNumber, eachCase);
                            }
                        }
                    } // end if 
                    
                } catch (Exception caseProcessingEx) {
                    System.debug(LoggingLevel.error, 'caseProcessingEx:' + caseProcessingEx.getStackTraceString());
                    
                    processLog += 'Unable to process Case ID: ' + eachCase.Id + '\r\n';
                    processLog += 'Root Cause: ' + caseProcessingEx.getStackTraceString() + '\r\n';
                    
                    appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CaseUpdateWithPersonAccountAndContact', 'updateCases',
                                                                         caseIds[0], 'Update Case Person Account', processLog, null, 'Error Log', startTime, logLevelCMD, caseProcessingEx));
                }
            }
            System.debug('Existing Plus New Person Account: ' + personAccountMap);
            // actual processing - END
            
            List < Account > eistingPlusNewPAList = personAccountMap.values();
            System.debug('FINAL PA List for UPSERT:' + eistingPlusNewPAList);
            
            
            // below code just to cover error block
            if (Test.isRunningTest()) {
                Account dummyPA = new Account();
                eistingPlusNewPAList.add(dummyPA);
            }
            
            Database.UpsertResult[] upserRList = Database.upsert(eistingPlusNewPAList, false);
            for (Integer i = 0; i < upserRList.size(); i++) {
                if (!upserRList[i].isSuccess()) {
                    for (Database.Error err: upserRList[i].getErrors()) {
                        processLog += 'Unable to Upsert Person Account ID: ' + eistingPlusNewPAList[i].Id +
                            ' :PerssonEmail: ' + eistingPlusNewPAList[i].PersonEmail + '\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += 'Person Account fields that affected this error: ' + err.getFields() + '\r\n';
                    }
                }
            }
            
            // query all existing user related to case email id
            Set < String > existingUserSet = new Set < String > ();
            for (List < User > existingUserList: [Select Id, Email, ContactId from User where Email IN: caseEmailNIdMap.keyset()]) {
                for (User eachUser: existingUserList) {
                    if (!existingUserSet.contains(eachUser.Email)) {
                        existingUserSet.add(eachUser.Email);
                    }
                }
            }
         
            // Query Person Account for Contact Id
            String caseLanguage = '';
            List < User > newCCUserList = new List < User > ();
            for (List < Account > perAccList: [Select Id, PersonContactId, PersonEmail,FirstName,LastName,PersonMobilePhone from Account where PersonEmail IN: caseEmailNIdMap.keyset()]) {
                for (Account eachAccount: perAccList) {
                    // check user already created with this email.    
                    caseLanguage = '';
                    if (!existingUserSet.contains(eachAccount.PersonEmail)) {
                        if (personAccountMap.containsKey(eachAccount.PersonEmail)) {
                            Account personAccount = personAccountMap.get(eachAccount.PersonEmail);
                            //personAccount.PersonContactId = eachAccount.PersonContactId;
                            caseLanguage = caseEmailLangMap.get(eachAccount.PersonEmail);
                            //AT - 9 May 2019 - Disable Community User Creation
                            //newCCUserList.add(createCommunityUser(personAccount, eachAccount.PersonContactId, caseLanguage));
                            //personAccountMap.put(eachAccount.PersonEmail, eachAccount);
                        }
                    }
                    personAccountMap.put(eachAccount.PersonEmail, eachAccount);
                }
            }
            
            System.debug('Final personAccountMap List: ' + personAccountMap);
            System.debug('Final New CCUser List: ' + newCCUserList);
            
            
            // below code just to cover error block - delete
            if (Test.isRunningTest()) {
                User dummyUser = new User();
                newCCUserList.add(dummyUser);
            }

            // stop sending email.  
            Database.DMLOptions dmlOptions = new Database.DMLOptions();
            dmlOptions.Emailheader.triggerUserEmail = false;
            dmlOptions.Emailheader.triggerOtherEmail = false;
            dmlOptions.Emailheader.triggerAutoResponseEmail = false;
            dmlOptions.optAllOrNone =false;
            
            Database.SaveResult[] insertURList = Database.insert(newCCUserList, false);
            for (Integer i = 0; i < insertURList.size(); i++) {
                if (!insertURList[i].isSuccess()) {
                    for (Database.Error err: insertURList[i].getErrors()) {
                        processLog += 'Unable to insert USER FOR PersonEmail: ' + newCCUserList[i].Username + '\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += 'USER fields that affected this error: ' + err.getFields() + '\r\n';
                    }
                }
            }
            System.debug('processLog:' + processLog);
            
            // take all the Case
            for (List < Case > similarCaseList: [Select Id, SuppliedEmail, ContactId, AccountId from Case where Id IN: caseIds]) {
                for (Case eachSimCase: similarCaseList) {
                    if (String.isNotEmpty(eachSimCase.SuppliedEmail)) {
                        if (personAccountMap.containsKey(eachSimCase.SuppliedEmail)) {
                            Account personAccount = personAccountMap.get(eachSimCase.SuppliedEmail);
                            eachSimCase.ContactId = personAccount.PersonContactId;
                            eachSimCase.AccountId = personAccount.Id;
                        }
                    } else {
                        if (caseWithEmptyEmailMap.containsKey(eachSimCase.Id)) {
                            String caseEmailId = caseWithEmptyEmailMap.get(eachSimCase.Id);
                            if (personAccountMap.containsKey(caseEmailId)) {
                                Account personAccount = personAccountMap.get(caseEmailId);
                                eachSimCase.ContactId = personAccount.PersonContactId;
                                eachSimCase.AccountId = personAccount.Id;
                                eachSimCase.SuppliedEmail = caseEmailId;
                            }
                        }
                    }
                    updateCaseList.add(eachSimCase);
                }
            }
            System.debug('Final Update Case List: ' + updateCaseList);
            
            // below code just to cover error block
            if (Test.isRunningTest()) {
                Case dummyCase = new Case();
                updateCaseList.add(dummyCase);
            }
            
            //update updateCaseList;
            Database.SaveResult[] srURList = Database.update(updateCaseList, false);
            for (Integer i = 0; i < srURList.size(); i++) {
                if (!srURList[i].isSuccess()) {
                    for (Database.Error err: srURList[i].getErrors()) {
                        processLog += 'Unable to Update Case ID: ' + updateCaseList[i].Id +
                            'Email:' + updateCaseList[i].SuppliedEmail + '\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += 'Case fields that affected this error: ' + err.getFields() + '\r\n';
                    }
                }
            }
            
            System.debug('Final ProcessLog:'+processLog);
        } catch (Exception caseUpdateEx) {
            System.debug(LoggingLevel.error, 'caseUpdateEx:' + caseUpdateEx.getStackTraceString());
            
            String firstCaseId = '';
            if (caseIds != null && !caseIds.isEmpty()) {
                firstCaseId = caseIds[0];
            }
            
            processLog += 'Failed to update case Person Account and Contact for the following Case Ids: \r\n';
            processLog += JSON.serializePretty(caseIds) + '\r\n';
            processLog += 'Root Cause: ' + caseUpdateEx.getMessage() + '\r\n';
            
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'CaseUpdateWithPersonAccountAndContact', 'updateCases',
                                                                 firstCaseId, 'Update Case Person Account', processLog, null, 'Error Log', startTime, logLevelCMD, caseUpdateEx));
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
        //} // END - for
    } // END - updateCases
    
    // When PA is associated, then Social type Case should not be posted. 
    public static boolean isCaseOriginInScope(String caseOrigin) {
        if (String.isNotEmpty(caseOrigin)) {
            String caseOriginValues = Utilities.getAppSettingsMdtValueByKey('Case_Origin_Values');
            
            if (String.isNotEmpty(caseOriginValues)) {
                String[] caseOriginValuesArr = caseOriginValues.split(',');
                
                for (String eachCaseOrgin: caseOriginValuesArr) {
                    if (eachCaseOrgin.equals(caseOrigin)) {
                        return false;
                    }
                }
            }
        } else {
            System.debug('Case Origin is empty/null');
        }
        
        return true;
    } // END - isCaseOriginInScope
    
    
    public static Account upsertPersonAccount(WS_PersonDetailsDto personDetailDto, String accountId) {
        
        if (!personDetailDto.isErrorOccured) {
            if (!personDetailDto.isContactExist) {
                Account personAccountRecord = new Account();
                personAccountRecord.RecordTypeId = recordTypeId;
                //if (String.isNotEmpty(personDetailDto.webName)) {
                if (String.isNotEmpty(personDetailDto.lastName)) {
                    //personAccountRecord.FirstName = personDetailDto.webName;
                    //personAccountRecord.LastName = personDetailDto.webName;
                    personAccountRecord.LastName = personDetailDto.lastName;
                    if (String.isNotEmpty(personDetailDto.firstName))
                        personAccountRecord.FirstName = personDetailDto.firstName;
                } else {
                    // create custom exception and throw instead of calloutEx
                    throw new CalloutException('Case.SuppliedName OR Case.Manual_Contact_Last_Name__c is empty/null ' +
                                               'so Person Account / User can not be created.');
                }
                
                if (String.isNotEmpty(personDetailDto.personEmailAddress)) {
                    personAccountRecord.PersonEmail = personDetailDto.personEmailAddress;
                } else {
                    // create custom exception and throw instead of calloutEx
                    throw new CalloutException('Case.SuppliedEmail is empty/null ' +
                                               'so Person Account / User can not be created.');
                }
                
                if(String.isNotEmpty(personDetailDto.phoneNumber)){
                    personAccountRecord.PersonMobilePhone = personDetailDto.phoneNumber;
                }
                
                // add always latest data
                personAccountRecord.OwnerId = personAcctOwnerRowId;
                //processLog += 'New PA Owner Set to :' + personAcctOwnerRowId + '\r\n';
                if (!personAccountMap.containsKey(personDetailDto.personEmailAddress)) {
                    personAccountMap.put(personDetailDto.personEmailAddress, personAccountRecord);
                } else {
                    Account pAcct = personAccountMap.get(personDetailDto.personEmailAddress);
                    pAcct.OwnerId = personAcctOwnerRowId;
                    //processLog += 'Existing PA Owner Set to :' + personAcctOwnerRowId + '\r\n';
                    personAccountMap.put(personDetailDto.personEmailAddress, pAcct);
                }
                return personAccountRecord;
            }
            
            Account personAccountRecord;
            String key = '';
            if (personAccountMap.containsKey(accountId)) {
                key = accountId;
            } else {
                key = personDetailDto.personEmailAddress;
            }
            
            if (personAccountMap.containsKey(key)) {
                personAccountRecord = personAccountMap.get(key);
            } else {
                personAccountRecord = new Account();
                personAccountRecord.RecordTypeId = recordTypeId;
            }
            
            // Added PA Owner to avoid Guest User issue.
            personAccountRecord.OwnerId = personAcctOwnerRowId;
            processLog += 'PA Owner Set to :' + personAcctOwnerRowId + '\r\n';
            
            // for person accounts we can not update the Name field
            // instead we have to update the FirstName and LastName individually                 
            personAccountRecord.FirstName = personDetailDto.firstName;
            personAccountRecord.LastName = personDetailDto.lastName;
            personAccountRecord.Salutation = personDetailDto.title;
            personAccountRecord.Nationality__pc = personDetailDto.nationality;
            personAccountRecord.Date_of_Birth__pc = Date.parse(personDetailDto.dOB);
            
            personAccountRecord.Customer_ID__pc = personDetailDto.customerNumber;
            personAccountRecord.PersonMobilePhone = personDetailDto.phoneNumber;
            personAccountRecord.BillingStreet = personDetailDto.addressLine1;
            personAccountRecord.BillingCity = personDetailDto.city;
            personAccountRecord.BillingState = personDetailDto.provinceState;
            personAccountRecord.BillingPostalCode = personDetailDto.postalCode;
            personAccountRecord.BillingCountry = personDetailDto.countryCode;
            personAccountRecord.PersonEmail = personDetailDto.personEmailAddress;
            
            // add always latest data
            personAccountMap.put(key, personAccountRecord);
            
            return personAccountRecord;
        }
        
        return null;
    } // END - upsertPersonAccount
    
    public static User createCommunityUser(Account personAccount, String personContactId, String caseLanguage) {
        User user = new User();
        user.FirstName = personAccount.FirstName;
        user.LastName = personAccount.LastName;
        user.MobilePhone = personAccount.PersonMobilePhone;
        user.ContactId = personContactId;
        user.Username = personAccount.PersonEmail;
        user.Email = personAccount.PersonEmail;
        user.Alias = getUserAlias(personAccount.FirstName, personAccount.LastName);
        //user.CommunityNickname = String.valueof(System.currentTimeMillis());
        user.CommunityNickname = randomWithLimit((personAccount.PersonEmail).length()) + getUniqueUserNickname();
        
        user.TimeZoneSidKey = 'Asia/Kuala_Lumpur';
        user.LocaleSidKey = 'en_MY';
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profileId;
        user.CurrencyIsoCode = 'MYR';
        user.LanguageLocaleKey = getUserLangCode(caseLanguage);
        
        return user;
    } // END - createCommunityUser
    
    public static WS_PersonDetailsDto getWSFindPersonDetails(String personJosnStrAtCase, String personEmail) {
        
        Map < String, Object > obj;
        if (String.isEmpty(personJosnStrAtCase)) {
            personJosnStrAtCase = Utilities.aceFindPerson(personEmail);
        }
        processLog += 'personJosnStrAtCase:' + personJosnStrAtCase;
        System.debug('personJosnStrAtCase:' + personJosnStrAtCase);
        
        WS_PersonDetailsDto personDetailsDto;
        if (String.isNotEmpty(personJosnStrAtCase)) {
            obj = (Map < String, Object > ) JSON.deserializeUntyped(personJosnStrAtCase);
            Boolean isErrorOccured = (boolean) obj.get(WS_PersonConstants.IS_ERROR_OCCURED);
            
            if (!isErrorOccured) {
                personDetailsDto = new WS_PersonDetailsDto();
                personDetailsDto.isContactExist = (boolean) obj.get(WS_PersonConstants.IS_CONTACT_EXIST);
                personDetailsDto.customerNumber = (String) obj.get(WS_PersonConstants.CUSTOMER_NUMBER);
                personDetailsDto.phoneNumber = (String) obj.get(WS_PersonConstants.PHONE_NUMBER);
                personDetailsDto.addressLine1 = (String) obj.get(WS_PersonConstants.ADDRESS_LINE_1);
                personDetailsDto.city = (String) obj.get(WS_PersonConstants.CITY);
                personDetailsDto.provinceState = (String) obj.get(WS_PersonConstants.PROVINCE_STATE);
                personDetailsDto.postalCode = (String) obj.get(WS_PersonConstants.POSTAL_CODE);
                personDetailsDto.countryCode = (String) obj.get(WS_PersonConstants.COUNTRY_CODE);
                //personDetailsDto.personEmailAddress = (String) obj.get(WS_PersonConstants.PERSON_EMAIL_REQUEST);
                personDetailsDto.personEmailAddress = (String) obj.get(WS_PersonConstants.EMAIL);
                if (String.isNotEmpty(personEmail)) {
                    personDetailsDto.personEmailAddress = personEmail;
                }
            } else {
                throw new CalloutException((String) obj.get(WS_PersonConstants.ERROR_MESSAGE));
            }
        }
        
        return personDetailsDto;
    } // END - getWSFindPersonDetails
    
    public static WS_PersonDetailsDto getWSGetPersonDetails(WS_PersonDetailsDto personDetailsDto) {
        
        Map < String, Object > obj;
        processLog += 'CustomerNuber: ' + personDetailsDto.customerNumber;
        String personJsonStr = Utilities.aceGetPerson(personDetailsDto.customerNumber);
        processLog += 'personJsonStr: ' + personJsonStr;
        System.debug('personJsonStr:' + personJsonStr);
        obj = (Map < String, Object > ) JSON.deserializeUntyped(personJsonStr);
        Boolean isErrorOccured = (boolean) obj.get(WS_PersonConstants.IS_ERROR_OCCURED);
        if (String.isNotEmpty(personJsonStr)) {
            
            if (!isErrorOccured) {
                personDetailsDto.title = (String) obj.get(WS_PersonConstants.TITLE);
                personDetailsDto.firstName = (String) obj.get(WS_PersonConstants.FIRST_NAME);
                personDetailsDto.lastName = (String) obj.get(WS_PersonConstants.LAST_NAME);
                personDetailsDto.nationality = (String) obj.get(WS_PersonConstants.NATIONALITY);
                personDetailsDto.dOB = (String) obj.get(WS_PersonConstants.DOB);
            } else {
                throw new CalloutException((String) obj.get(WS_PersonConstants.ERROR_MESSAGE));
            }
            
        }
        
        return personDetailsDto;
    } // END getWSGetPersonDetails
    
    public static String getEmailIdFromPersonJsonString(String customerNumber, String personJsonString) {
        if (String.isNotEmpty(customerNumber)) {
            if (String.isNotEmpty(personJsonString)) {
                Map < String, Object > obj = (Map < String, Object > ) JSON.deserializeUntyped(personJsonString);
                return ((String) obj.get(WS_PersonConstants.EMAIL));
            }
        }
        
        return null;
    }
    
    public static void executeUpdateCaseProcess(String customerNumber, Case eachCase) {
        WS_PersonDetailsDto personDetailDto;
        if (String.isNotEmpty(customerNumber)) {
            personDetailDto = getWSFindPersonDetails(eachCase.FindPersonDetails__c, null);
            personDetailDto.customerNumber = customerNumber;
            personDetailDto.isContactExist = true;
        } else {
            
            // check Manual Email exist    
            if (String.isNotEmpty(eachCase.Manual_Contact_Email__c)) {
                //personDetailDto = getWSFindPersonDetails(null, eachCase.SuppliedEmail);
                personDetailDto = new WS_PersonDetailsDto();
                personDetailDto.personEmailAddress = eachCase.Manual_Contact_Email__c;
                personDetailDto.firstName = eachCase.Manual_Contact_First_Name__c;
                personDetailDto.lastName = eachCase.Manual_Contact_Last_Name__c;
                //-- added on 04/OCT/2017
				personDetailDto.personTitle = eachCase.Supplied_Title__c;
                personDetailDto.isErrorOccured = false;
                personDetailDto.isContactExist = false;
                System.debug('Manual_Contact_Email__c: ' + eachCase.Manual_Contact_Email__c);
                processLog += 'Case.Manual_Contact_Email__c: ' + eachCase.Manual_Contact_Email__c + '\r\n';
            } else {
                if (String.isNotEmpty(eachCase.Account.PersonEmail)) {
                    //personDetailDto = getWSFindPersonDetails(null, eachCase.SuppliedEmail);
                    personDetailDto = new WS_PersonDetailsDto();
                    personDetailDto.personEmailAddress = eachCase.Account.PersonEmail;
                    personDetailDto.firstName = personAccountMap.get(eachCase.Account.PersonEmail).FirstName;
                    personDetailDto.lastName = personAccountMap.get(eachCase.Account.PersonEmail).LastName;
                    personDetailDto.isErrorOccured = false;
                    personDetailDto.isContactExist = false;
                    System.debug('Account.PersonEmail: ' + eachCase.Account.PersonEmail);
                    processLog += 'Account.PersonEmail: ' + eachCase.Account.PersonEmail + '\r\n';
                } else {
                    personDetailDto = getWSFindPersonDetails(null, eachCase.SuppliedEmail);
                }
            }
        }
        System.debug('personDetailDto.personEmailAddress:' + personDetailDto.personEmailAddress);
        
        // set web name
        //personDetailDto.webName = eachCase.SuppliedName;
        
        // Commented By Pawan -18/SEP/2017
        //if (String.isEmpty(personDetailDto.lastName))
        //    personDetailDto.lastName = eachCase.SuppliedName;
        
        if (String.isEmpty(personDetailDto.lastName))
            personDetailDto.lastName = eachCase.Manual_Contact_Last_Name__c;
        if (String.isEmpty(personDetailDto.firstName))
            personDetailDto.firstName = eachCase.Manual_Contact_First_Name__c;
        
        if (personDetailDto.isContactExist) {
            System.debug('getWSGetPersonDetails:' + personDetailDto);
            personDetailDto = getWSGetPersonDetails(personDetailDto);
        }else{
            if(String.isNotEmpty(eachCase.SuppliedPhone))
            personDetailDto.phoneNumber=eachCase.SuppliedPhone;
        }
        
        Account personAccount = upsertPersonAccount(personDetailDto, eachCase.AccountId);
    }
    
    private static String getUniqueUserNickname() {
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        //return hexDigest.substring(0, 20);
        return hexDigest;
    }
    
    private static String getUserAlias(String firtname, String lastname) {
        String alias = '';
        if (String.isNotEmpty(lastname)) {
            alias += lastname;
        }
        
        if (String.isNotEmpty(firtname)) {
            alias += firtname;
        }
        
        if (String.isNotEmpty(alias) && alias.length() >= 8) {
            return alias.substring(0, 8);
        }
        return alias;
        //return (!string.IsBlank(firtname)?firtname.left(1):' ') + lastname.left(5);
    }
    
    private static Integer randomWithLimit(Integer upperLimit) {
        Integer rand = Math.round(Math.random() * 1000);
        return Math.mod(rand, upperLimit);
    }
    
    private static String getUserLangCode(String caseLanguage) {
        System.debug('userLanguageCodes: ' + userLanguageCodes);
        System.debug('caseLanguage: ' + caseLanguage);
        
        if (String.isNotEmpty(caseLanguage)) {
            if (userLanguageCodes.containsKey(caseLanguage)) {
                return userLanguageCodes.get(caseLanguage);
            }
        }
        return USER_DEFAULT_LANGUAGE;
    }
}