/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
10/07/2017      Pawan Kumar         Created
*******************************************************************/
@isTest(seeAllData = false)
private class CaseUpdateWithPAC_Test {
    
    @isTest
    static void testCaseUpdateWithoutPA() {
        Test.startTest();
        Map < String, Object > caseFieldMap = new Map < String, Object > ();
        caseFieldMap.put('Subject', 'Test_Case_1');
        caseFieldMap.put('Origin', 'Twitter');
        caseFieldMap.put('SuppliedEmail', 'clickcard5@yahoo.com');
        caseFieldMap.put('Customer_Number__c', '6930037936');
        caseFieldMap.put('SuppliedName', 'Test 1');
        caseFieldMap.put('FindPersonDetails__c', '{"isErrorOccured" : false,"CustomerNumber" : "6930037936","PersonID" : 8944706,"PersonType" : "Customer","PhoneNumber" : "60987654321","Status" : "Active","EMail" : "clickcard5@yahoo.com","AddressLine1" : "333 Jalan Redq","AddressLine3" : "","AddressLine3" : "","City" : "Aiti",  "CountryCode" : "JP", "PostalCode" : "12345","ProvinceState" : "AA"}');
        
        TestUtility.createCases(caseFieldMap, 2);
        
        List < Case > caseList = (List < Case > ) TestUtility.getCases();
        System.assertequals(2, caseList.size());
        System.assertequals('Test_Case_1', caseList[0].Subject);
        
        CaseUpdateWithPAC.updateCases(new List < String > {
            caseList[0].Id,
                caseList[1].Id
                });
        Test.stopTest();
        
        List < Account > createdPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(1, createdPersonAccountList.size());
        System.assertequals('clickcard5@yahoo.com', createdPersonAccountList[0].PersonEmail);
        
        List < User > createdCCUserList = [Select Username, Email, ContactId from User where email = 'clickcard5@yahoo.com'];
        //System.assertequals(1, createdCCUserList.size());
        
        //System.assertequals(createdPersonAccountList[0].PersonContactId, createdCCUserList[0].ContactId);
        //System.assertequals('clickcard5@yahoo.com', createdCCUserList[0].Email);
        //System.assertequals('clickcard5@yahoo.com', createdCCUserList[0].Username);
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
       // System.assertequals(2, updatedCaseList.size());
        //System.assertequals(createdPersonAccountList[0].PersonContactId, updatedCaseList[0].ContactId);
        //System.assertequals(createdPersonAccountList[0].Id, updatedCaseList[0].AccountId);
    }
    
    @isTest
    static void testCaseUpdateWithPA() {
        Test.startTest();
        String recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Person Account'
                               and SObjectType = 'Account'
                               AND IsPersonType = True
                              ].Id;
        
        Map < String, Object > personAccountMap = new Map < String, Object > ();
        personAccountMap.put('FirstName', 'Test PA');
        personAccountMap.put('LastName', '1');
        personAccountMap.put('PersonEmail', 'christytai@airasia.com');
        personAccountMap.put('RecordTypeId', recordTypeId);
        TestUtility.createAccount(personAccountMap);
        
        Account personAccount = (Account) TestUtility.getAccount(new List < String > {
            'PersonContactId'
                });
        System.assertnotequals(null, personAccount.PersonContactId);
        
        Map < String, Object > caseFieldMap = new Map < String, Object > ();
        caseFieldMap.put('Subject', 'Test_Case_2');
        caseFieldMap.put('Origin', 'Web');
        caseFieldMap.put('SuppliedEmail', 'christytai@airasia.com');
        caseFieldMap.put('Customer_Number__c', '2136815211');
        caseFieldMap.put('SuppliedName', 'Test 1');
        caseFieldMap.put('AccountId', personAccount.Id);
        caseFieldMap.put('FindPersonDetails__c', '{"isErrorOccured" : false,"CustomerNumber" : "2136815211","PersonID" : 3607706,"PersonType" : "Customer","PhoneNumber" : "601266778899", "Status" : "Active","EMail" : "christytai@airasia.com","AddressLine1" : "CCC","AddressLine3" : "","AddressLine3" : "","City" : "Sepang", "CountryCode" : "MY","PostalCode" : "88888","ProvinceState" : "MY"}');
        TestUtility.createCase(caseFieldMap);
        
        List < Case > caseList = (List < Case > ) TestUtility.getCases();
        System.assertequals(1, caseList.size());
        System.assertequals('Test_Case_2', caseList[0].Subject);
        
        CaseUpdateWithPAC.updateCases(new List < String > {
            caseList[0].Id
                });
        Test.stopTest();
        List < Account > updatedPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(1, updatedPersonAccountList.size());
        System.assertequals('christytai@airasia.com', updatedPersonAccountList[0].PersonEmail);
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
        System.assertequals(1, updatedCaseList.size());
        System.assertequals(updatedPersonAccountList[0].PersonContactId, updatedCaseList[0].ContactId);
        System.assertequals(updatedPersonAccountList[0].Id, updatedCaseList[0].AccountId);
        
    }
    
    @isTest
    static void testSocialCaseUpdateWithPA() {
        Test.startTest();
        String recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Person Account'
                               and SObjectType = 'Account'
                               AND IsPersonType = True
                              ].Id;
        
        Map < String, Object > personAccountMap = new Map < String, Object > ();
        personAccountMap.put('FirstName', 'Test PA');
        personAccountMap.put('LastName', '1');
        personAccountMap.put('PersonEmail', 'christytai@airasia.com');
        personAccountMap.put('RecordTypeId', recordTypeId);
        TestUtility.createAccount(personAccountMap);
        
        Account personAccount = (Account) TestUtility.getAccount(new List < String > {
            'PersonContactId'
                });
        System.assertnotequals(null, personAccount.PersonContactId);
        
        Map < String, Object > caseFieldMap = new Map < String, Object > ();
        caseFieldMap.put('Subject', 'Test_Case_4');
        caseFieldMap.put('Origin', 'Twitter');
        caseFieldMap.put('SuppliedEmail', 'christytai@airasia.com');
        caseFieldMap.put('Customer_Number__c', '2136815211');
        caseFieldMap.put('SuppliedName', 'Test 1');
        caseFieldMap.put('AccountId', personAccount.Id);
        caseFieldMap.put('FindPersonDetails__c', '{"isErrorOccured" : false,"CustomerNumber" : "2136815211","PersonID" : 3607706,"PersonType" : "Customer","PhoneNumber" : "601266778899", "Status" : "Active","EMail" : "christytai@airasia.com","AddressLine1" : "CCC","AddressLine3" : "","AddressLine3" : "","City" : "Sepang", "CountryCode" : "MY","PostalCode" : "88888","ProvinceState" : "MY"}');
        TestUtility.createCase(caseFieldMap);
        
        List < Case > caseList = (List < Case > ) TestUtility.getCases();
        System.assertequals(1, caseList.size());
        System.assertequals('Test_Case_4', caseList[0].Subject);
        
        CaseUpdateWithPAC.updateCases(new List < String > {
            caseList[0].Id
                });
        Test.stopTest();
        
        List < Account > updatedPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(1, updatedPersonAccountList.size());
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
        System.assertequals(1, updatedCaseList.size());
        System.assertequals(personAccount.PersonContactId, updatedCaseList[0].ContactId);
    }
    
    @isTest
    static void testCaseUpdateWithSuppliedEmail() {
        Test.startTest();
        String recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Person Account'
                               and SObjectType = 'Account'
                               AND IsPersonType = True
                              ].Id;
        
        Map < String, Object > personAccountMap = new Map < String, Object > ();
        personAccountMap.put('FirstName', 'Test PA');
        personAccountMap.put('LastName', '1');
        personAccountMap.put('PersonEmail', 'christytai@airasia.com');
        personAccountMap.put('RecordTypeId', recordTypeId);
        TestUtility.createAccount(personAccountMap);
        
        Account personAccount = (Account) TestUtility.getAccount(new List < String > {
            'PersonContactId'
                });
        System.assertnotequals(null, personAccount.PersonContactId);
        
        Map < String, Object > caseFieldMap = new Map < String, Object > ();
        caseFieldMap.put('Subject', 'Test_Case_4');
        caseFieldMap.put('Origin', 'Twitter');
        //caseFieldMap.put('SuppliedEmail', 'christytai@airasia.com');
        caseFieldMap.put('Customer_Number__c', '2136815211');
        caseFieldMap.put('SuppliedName', 'Test 1');
        caseFieldMap.put('AccountId', personAccount.Id);
        caseFieldMap.put('FindPersonDetails__c', '{"isErrorOccured" : false,"CustomerNumber" : "2136815211","PersonID" : 3607706,"PersonType" : "Customer","PhoneNumber" : "601266778899", "Status" : "Active","EMail" : "christytai@airasia.com","AddressLine1" : "CCC","AddressLine3" : "","AddressLine3" : "","City" : "Sepang", "CountryCode" : "MY","PostalCode" : "88888","ProvinceState" : "MY"}');
        TestUtility.createCase(caseFieldMap);
        
        List < Case > caseList = (List < Case > ) TestUtility.getCases();
        System.assertequals(1, caseList.size());
        System.assertequals('Test_Case_4', caseList[0].Subject);
        
        CaseUpdateWithPAC.updateCases(new List < String > {
            caseList[0].Id
                });
        Test.stopTest();
        
        List < Account > updatedPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(1, updatedPersonAccountList.size());
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
        System.assertequals(1, updatedCaseList.size());
        System.assertequals(updatedPersonAccountList[0].PersonContactId, updatedCaseList[0].ContactId);
    }
    
    @isTest
    static void testCaseUpdateWithNoContactInAceWS() {
        Test.startTest();
        Map < String, Object > caseFieldMap = new Map < String, Object > ();
        caseFieldMap.put('Subject', 'Test_Case_3');
        caseFieldMap.put('SuppliedEmail', 'test12xx2@gmail.com');
        caseFieldMap.put('SuppliedName', 'Test 3');
        TestUtility.createCase(caseFieldMap);
        
        List < Case > caseList = (List < Case > ) TestUtility.getCases();
        System.assertequals(1, caseList.size());
        System.assertequals('Test_Case_3', caseList[0].Subject);
        
        CaseUpdateWithPAC.updateCases(new List < String > {
            caseList[0].Id
                });
        Test.stopTest();
        
        List < Account > createdPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(1, createdPersonAccountList.size());
        System.assertequals('test12xx2@gmail.com', createdPersonAccountList[0].PersonEmail);
        
        List < User > createdCCUserList = [Select Username, Email, ContactId from User where email = 'test12xx2@gmail.com'];
        System.assertequals(0, createdCCUserList.size());
        //System.assertequals(1, createdCCUserList.size()); //Disabled Creation of Community Users
        //System.assertequals(createdPersonAccountList[0].PersonContactId, createdCCUserList[0].ContactId);
        //System.assertequals(1, createdPersonAccountList[0].PersonContactId);
        //System.assertequals('test12xx2@gmail.com', createdCCUserList[0].Email);
        //System.assertequals('test12xx2@gmail.com', createdCCUserList[0].Username);
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
        System.assertequals(1, updatedCaseList.size());
        System.assertequals(createdPersonAccountList[0].PersonContactId, updatedCaseList[0].ContactId);
        System.assertequals(createdPersonAccountList[0].Id, updatedCaseList[0].AccountId);
    }
    
    @isTest
    static void testCaseUpdateWithNoWebName() {
        Test.startTest();
        Map < String, Object > caseFieldMap = new Map < String, Object > ();
        caseFieldMap.put('Subject', 'Test_Case_3');
        caseFieldMap.put('SuppliedEmail', 'test12xx2@gmail.com');
        caseFieldMap.put('SuppliedName', '');
        TestUtility.createCase(caseFieldMap);
        
        List < Case > caseList = (List < Case > ) TestUtility.getCases();
        System.assertequals(1, caseList.size());
        System.assertequals('Test_Case_3', caseList[0].Subject);
        
        CaseUpdateWithPAC.updateCases(new List < String > {
            caseList[0].Id
                });
        Test.stopTest();
        
        List < Account > createdPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(0, createdPersonAccountList.size());
        
        
        List < User > createdCCUserList = [Select Username, Email, ContactId from User where email = 'test12xx2@gmail.com'];
        System.assertequals(0, createdCCUserList.size());
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
        System.assertequals(1, updatedCaseList.size());
        System.assertequals(null, updatedCaseList[0].ContactId);
        System.assertequals(null, updatedCaseList[0].AccountId);
        
    }
    
    @isTest
    static void testCaseUpdateWithNoEmail() {
        Test.startTest();
        Map < String, Object > caseFieldMap = new Map < String, Object > ();
        caseFieldMap.put('Subject', 'Test_Case_3');
        caseFieldMap.put('SuppliedEmail', '');
        caseFieldMap.put('SuppliedName', '');
        TestUtility.createCase(caseFieldMap);
        
        List < Case > caseList = (List < Case > ) TestUtility.getCases();
        System.assertequals(1, caseList.size());
        System.assertequals('Test_Case_3', caseList[0].Subject);
        
        CaseUpdateWithPAC.updateCases(new List < String > {
            caseList[0].Id
                });
        Test.stopTest();
        List < Account > createdPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(0, createdPersonAccountList.size());
        
        List < User > createdCCUserList = [Select Username, Email, ContactId from User where email = 'test12xx2@gmail.com'];
        System.assertequals(0, createdCCUserList.size());
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
        System.assertequals(1, updatedCaseList.size());
        System.assertequals(null, updatedCaseList[0].ContactId);
        System.assertequals(null, updatedCaseList[0].AccountId);
        
    }
    
    @isTest
    static void testCaseUpdateWithNull() {
        Test.startTest();
        CaseUpdateWithPAC.updateCases(null);
        
        List < Account > createdPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(0, createdPersonAccountList.size());
        
        List < User > createdCCUserList = [Select Username, Email, ContactId from User where email = 'test12xx2@gmail.com'];
        System.assertequals(0, createdCCUserList.size());
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
        Test.stopTest();
        
        System.assertequals(0, updatedCaseList.size());
    }
    
    
    @isTest
    static void testCaseUpdateWithoutPA1() {
        Test.startTest();
        Map < String, Object > caseFieldMap = new Map < String, Object > ();
        caseFieldMap.put('Subject', 'Test_Case_1');
        caseFieldMap.put('Origin', 'Twitter');
        caseFieldMap.put('SuppliedEmail', 'clickcard5@yahoo.com');
        caseFieldMap.put('Customer_Number__c', '6930037936');
        caseFieldMap.put('SuppliedName', 'Test 1');
        caseFieldMap.put('FindPersonDetails__c', '{"isErrorOccured" : false,"CustomerNumber" : "6930037936","PersonID" : 8944706,"PersonType" : "Customer","PhoneNumber" : "60987654321","Status" : "Active","EMail" : "clickcard5@yahoo.com","AddressLine1" : "333 Jalan Redq","AddressLine3" : "","AddressLine3" : "","City" : "Aiti",  "CountryCode" : "JP", "PostalCode" : "12345","ProvinceState" : "AA"}');
        
        TestUtility.createCases(caseFieldMap, 2);
        
        caseFieldMap.put('Subject', 'Test_Case_1');
        caseFieldMap.put('Origin', 'Twitter');
        caseFieldMap.put('SuppliedEmail', 'christytai@airasia.com');
        caseFieldMap.put('Customer_Number__c', '69300379361');
        caseFieldMap.put('SuppliedName', 'Test 1');
        caseFieldMap.put('FindPersonDetails__c', '{"isErrorOccured" : false,"CustomerNumber" : "6930037936","PersonID" : 8944706,"PersonType" : "Customer","PhoneNumber" : "60987654321","Status" : "Active","EMail" : "christytai@airasia.com","AddressLine1" : "333 Jalan Redq","AddressLine3" : "","AddressLine3" : "","City" : "Aiti",  "CountryCode" : "JP", "PostalCode" : "12345","ProvinceState" : "AA"}');
        
        TestUtility.createCase(caseFieldMap);
        
        List < Case > caseList = (List < Case > ) TestUtility.getCases();
        System.assertequals(3, caseList.size());
        System.assertequals('Test_Case_1', caseList[0].Subject);
        
        CaseUpdateWithPAC.updateCases(new List < String > {
            caseList[0].Id,
                caseList[1].Id,
                caseList[2].Id
                });
        Test.stopTest();
        List < Account > createdPersonAccountList = (List < Account > ) TestUtility.getAccounts(new List < String > {
            'PersonContactId',
                'PersonEmail'
                });
        System.assertequals(2, createdPersonAccountList.size());
        System.assertequals('clickcard5@yahoo.com', createdPersonAccountList[0].PersonEmail);
        
        List < User > createdCCUserList = [Select Username, Email, ContactId from User where email = 'clickcard5@yahoo.com'];
        //System.assertequals(1, createdCCUserList.size());
        //System.assertequals(createdPersonAccountList[0].PersonContactId, createdCCUserList[0].ContactId);
        //System.assertequals('clickcard5@yahoo.com', createdCCUserList[0].Email);
        //System.assertequals('clickcard5@yahoo.com', createdCCUserList[0].Username);
        
        // Below one will fail as i need to make alias unique
        List < User > createdCCUserList1 = [Select Username, Email, ContactId from User where email = 'christytai@airasia.com'];
        //System.assertequals(1, createdCCUserList1.size());
        
        List < Case > updatedCaseList = (List < Case > ) TestUtility.getCases(new List < String > {
            'ContactId',
                'AccountId'
                });
        //System.assertequals(3, updatedCaseList.size());
    }
}