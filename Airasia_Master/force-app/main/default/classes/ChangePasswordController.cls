/**
* An apex page controller that exposes the change password functionality
*/
public without sharing class ChangePasswordController {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}        
    public String message1 {get; set;}
    public String message2 {get; set;}
    
    public PageReference changePassword() {
        return Site.changePassword(newPassword, verifyNewPassword, oldpassword);
    }  
    
    public ChangePasswordController() {
        message1 = Label.AA_Case_Change_Password_Message;
        message2 = Label.AA_Case_Change_Password_Last_Changed;
        Map<String, String> mapLabel = new Map<String, String>();
        List<User> userList = [SELECT Id, Email, LastPasswordChangeDate FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(userList != null && !userList.isEmpty()) {
            mapLabel.put('AA_Case_Change_Password_Message', userList[0].Email);
            mapLabel.put('AA_Case_Change_Password_Last_Changed', getStringDate(userList[0].LastPasswordChangeDate));
            getMessage(mapLabel);
        }
    }
    
    public String getMessage(Map<String, String> mapLabel) {
        String data = '';
        
        for(String labelValue : mapLabel.keySet()) {
            if(String.isNotBlank(labelValue)) {
                String labelData = mapLabel.get(labelValue);
                Component.Apex.OutputText output = new Component.Apex.OutputText();
                output.expressions.value = '{!$Label.' + labelValue + '}';
                data = String.valueOf(output.value);
                if(String.isNotBlank(labelData)) {
                    if('AA_Case_Change_Password_Message'.equals(labelValue)) {
                        message1 = String.format(data, labelData.split(','));
                    } else {
                        message2 = String.format(data, labelData.split(','));
                    }
                }
            }
        }
        return data;
    }
    
    public String getStringDate(DateTime d) {
        String todaysValue = d != null ? d.format('dd/MM/yyyy HH:mm') : '';
        return todaysValue;
    }   
}