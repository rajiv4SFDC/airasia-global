@isTest
public class CharketTestFixture
{
    @TestSetup
    static void init()
    {
        Charket__WeChatAccount__c wechatAccount = new Charket__WeChatAccount__c(
                Name = 'Test', Charket__Type__c = 'Service Account',
                Charket__WeChatOriginId__c = 'gh_74140ee3367b',
                Charket__AppId__c = 'wx5df0b68f95bdfb48');

        insert wechatAccount;
    }
    
    @isTest
    static void saveAccessTokenTest()
    {
        Test.startTest();

        RestRequest request = new RestRequest();

        request.requestURI = '/save-access-token';
        String requestBody = '{"wechatOriginId":"gh_74140ee3367b","accessToken":"11_RlQk5bCBgyyC0YNVXnDW5LIuC3HeXqBID6PN6bJAqB5v6rJlOHYCRNcxjSeSIkv3ae1naJa5-H31cdQ0_JxZCXbXmhyIbRPiH4bsvIQlG0pPSZW0uMOLkgGEjEyejDShP8JC3JQYGyRaQn5AKDNcAGAPIS","expiresIn":7200}';
        request.requestBody = Blob.valueOf(requestBody);

        RestContext.request = request;
        RestContext.response = new RestResponse();

        WeChatAccessTokenRestResource.doPost();

        Test.stopTest();
        System.assertEquals(RestContext.response.responseBody.toString(), 'success');
    }
    
    @isTest
    static void saveAccessTokenNotMatchTest()
    {
        Test.startTest();

        RestRequest request = new RestRequest();

        request.requestURI = '/save-token';
        String requestBody = '{"wechatOriginId":"gh_74140ee3367b","accessToken":"11_RlQk5bCBgyyC0YNVXnDW5LIuC3HeXqBID6PN6bJAqB5v6rJlOHYCRNcxjSeSIkv3ae1naJa5-H31cdQ0_JxZCXbXmhyIbRPiH4bsvIQlG0pPSZW0uMOLkgGEjEyejDShP8JC3JQYGyRaQn5AKDNcAGAPIS","expiresIn":7200}';
        request.requestBody = Blob.valueOf(requestBody);

        RestContext.request = request;
        RestContext.response = new RestResponse();

        WeChatAccessTokenRestResource.doPost();

        Test.stopTest();
        System.assertEquals(RestContext.response.responseBody.toString(), 'no match');
    }
    
    @isTest
    static void generateAccessTokenTest()
    {
        Charket__WeChatAccount__c wechatAccount = [select Id, Charket__WeChatOriginId__c 
                from Charket__WeChatAccount__c limit 1];
                
        Charket.WeChatAccessToken.setAccessToken(wechatAccount.Charket__WeChatOriginId__c, 'TestAccessToken', 7200);
        
        Test.startTest();
        
        Charket.WeChatAccessTokenGeneratorContext context = new Charket.WeChatAccessTokenGeneratorContext(
                        wechatAccount.Id, wechatAccount.Charket__WeChatOriginId__c);
        
        Type t = Type.forName('', 'CustomWeChatAccessTokenGenerator');
        Charket.WeChatAccessTokenGenerator tokenGenerator = (Charket.WeChatAccessTokenGenerator)t.newInstance();
        
        String token = tokenGenerator.getAccessToken(context);
        
        Test.stopTest();
    }
}