/*************************************************************
 * Apex controller for Live Chat VF pages
 * 
 * Oct 2017 - Salesforce Services - original implementation
 * 
 * Mar 2019 - Charles Thompson - 
 * Add support for link validity for 5 minutes only
 * and remove requirement to test the referer
 *                               
 * May 2019 - Alvin Tayag
 * Add Handoff Override for specific languages in Custom Metadata Type (Handoff Languages) at specific times (Handoff Timing)
 * 
 * Aug 2019 - Alvin Tayag
 * Added Validation on Case Surveys
 * Feb 2019 - Tushar Arora - Making sure that id no date param present in live chat url then link is not valid.
 * 
 * March 2020 - Capgemini
 * Added 2 new methods(getEstimatedWaitTime, getTimeInFormat) to calculate the EstimatedWaitTime and displaying in CustomChat form.
 * **********************************************************/
global without sharing class ChatController {
    
    private static final String DEFAULT_LANG = 'en_US';
    private static final String AIR_ASIA_GUEST_USER = 'AirAsia Site Guest User';
    private static final String FORM_PRECHAT = 'PreChatForm';
    private static final String FORM_B2BPRECHAT = 'B2BPreChatForm';
    private static final String FORM_CUSTOMCHAT = 'CustomChatForm';
    private static final String FORM_THANKMSG = 'PostChatThanksMsg';    
    //private static final String DEFAULT_RECORDTYPE = 'Auto Case Creation Enquiry/Compliment/Complain';
    private static String SUPPORT_PAGE = 'https://support.airasia.com';
    public String DEFAULT_COUNTRY_CODE = '+60';
    private static String recordTypeName = 'Auto Case Creation Enquiry/Compliment/Complain';
    public static String recordType{get;set;}
    public static String personAccountRecordType{get;set;}    
    public static String isRBV{get;set;}   
    public static String lang{get;set;}
    public static String contactId{get;set;}
    public static String todaysValue {get;}
    public String surveyQuestion1{get;set;}
    public String surveyQuestion2{get;set;}
    public static String caseId{get;set;}
    public static String langValue{get;set;}
    public static String langDataValue{get;set;}  
    
    //Properties to fill case information
    public static String firstName {get;set;}
    public static String lastName {get;set;}
    public static String emailAddress {get;set;}
    public static String phoneNumber {get;set;}
    public static String subject {get;set;}
    public static String title {get;set;}
    public static String caseType {get;set;}
    public static String salutation {get;set;}
    public static String fullName {get;set;}
    public static String chatter_token {get;set;}
    public static String chatter_id {get;set;}
    public static String category {get;set;}
    public static String category2 {get;set;}
    public static String booking_Number {get;set;}
    public static String botTranscript {get;set;}
    public static String sampleTranscript {get;set;}
    public static String dateTimeStamp {get;set;}
    public static String docType {get;set;}
    public static Boolean isValidLink {get;set;}
    public static String origin {get;set;}
    public static String formActionUrl {get;set;}
    static {
    
        try {
            // the parameters passed in the URL come from a link presented 
            // in the bot conversation
            Map<String, String> paramMap = ApexPages.currentPage().getParameters();
            String pageName = ApexPages.currentPage().getUrl().substringAfter('/apex/').substringBefore('?');
            
            System.Debug('Page Name '+pageName);
            
            //Code Added By Maaz Khan 16 Nov 2018 : Begin Changes
            /*
                Ada Bot integration. Reading url and taking out parameters to be defined in case mapping
            */          
            
            String completeURL = ApexPages.currentPage().getUrl();
            System.Debug('Complete URL --> '+completeURL);
            SUPPORT_PAGE = getSupportPageURL();
            // CT - 04 Apr 2019 - no longer required, now that we have the 5-minute timeout
            //String redirectionURL = ApexPages.currentPage().getHeaders().get('Referer');
            //System.Debug(LoggingLevel.Info,'Redirection URL --> '+redirectionURL);
            
            //If customer is not coming from Ada bot for handoff system should redirect it to support.airasia.com
            
            //parsing url to get name, email address and other fields
            fullName = getParameterByName('full_name',paramMap.get('endpoint'));
            System.Debug(LoggingLevel.Info,'fullName --> '+fullName);
            
            /* <CT 1 Dec> In the "else" clause for this, we can test for email address also */
            boolean isMobile = false;
            // <CT 1 Dec>
            
            // Extract the first and last names
            // (because of the complexity of possibilities in Asian names,
            // we make some basic assumptions)
            if (fullName != null){
                
                if (fullName.contains(' ')){
                    
                    firstName = fullName.substringBefore(' ');
                    lastName = fullName.substringAfter(' ');
                } else {
                    lastName = fullName;
                }
                
                if (lastName == ''){
                    lastName = fullName;
                    firstName = '';
                }
            }
                    
            System.Debug(LoggingLevel.Info,'firstName --> '+firstName);
            System.Debug(LoggingLevel.Info,'lastName --> '+lastName);
            
            // Booking ID (PRN)
            booking_Number = getParameterByName('booking_id', paramMap.get('endpoint'));
            booking_Number = booking_Number.left(6);
            System.Debug(LoggingLevel.Info,'booking Number -->' + booking_Number);
            
            // email 
            emailAddress = getParameterByName('email_address', paramMap.get('endpoint'));
            System.Debug(LoggingLevel.Info,'emailAddress --> ' + emailAddress);
            
            // Ada's chatter token & ID (to find the transcript, used later)
            chatter_token = getParameterByName('chatter_token', paramMap.get('endpoint'));
            System.Debug(LoggingLevel.Info,'chatter_token --> ' + chatter_token);
            
            chatter_id = getParameterByName('chatter_id', paramMap.get('endpoint'));
            System.Debug(LoggingLevel.Info,'chatter_id --> ' + chatter_id);
            
            isRBV = getParameterByName('isrbv', paramMap.get('endpoint'));
            System.Debug(LoggingLevel.Info,'isRBV --> ' + isRBV); 
            
            docType = getParameterByName('doctype', paramMap.get('endpoint'));
            System.Debug(LoggingLevel.Info,'docType --> ' + docType); 
            
            origin = getParameterByName('origin', paramMap.get('endpoint'));
            System.Debug(LoggingLevel.Info,'origin --> ' + origin); 
            
            //setting case type as Enquiry/Request for all cases coming from handoff of chat
            String categoryValue = getParameterByName('category', paramMap.get('endpoint'));
            System.Debug(LoggingLevel.Info,'category --> ' + categoryValue); 
            Map<String, Live_Chat_Case_Mapping__mdt> typeMap = getAVACaseTypeMapping();

            try {
                Live_Chat_Case_Mapping__mdt mapCategory = typeMap.get(categoryValue);
                if(mapCategory != null) {
                    caseType = mapCategory.Type__c;
                    category = mapCategory.SubCategory_1__c;
                    recordTypeName = mapCategory.Record_Type_Name__c;
                    System.Debug(LoggingLevel.Info,'recordTypeName --> ' + recordTypeName); 
                    if(String.isNotEmpty(mapCategory.SubCategory_2__c)) {
                        category2 = mapCategory.SubCategory_2__c;
                    }
                }
                else {
                    caseType = 'Enquiry/Request';
                    category = 'General Q&A';
                }
            }
            catch (Exception notFound) {
                caseType = 'Enquiry/Request';
                category = 'General Q&A';
            }
            
            //Code Added By Maaz Khan 16 Nov 2018 : End Changes
            //           
            
            // Mar 2019 - Charles Thompson - begin
            // Ensure the link from Ada is valid for only 5 minutes
            DateTime dtNow = DateTime.now();
            Long dtNowL = dtNow.getTime(); // gives us milliseconds since 1 Jan 70

            // dateTimeStamp is a string containing Unix epoch - 
            //                          number of seconds since 1 Jan 70
            dateTimeStamp = getParameterByName('dt', paramMap.get('endpoint'));
            // Convert to long and change to milliseconds
            if (dateTimeStamp != ''){
                Long dtStampl = Long.valueOf(dateTimeStamp) * 1000; 

                // is the difference <= 5 minutes (300,000 milliseconds)
                isValidLink = (dtNowL - dtStampL) <= 300000;
                System.Debug(LoggingLevel.Info,'dtNowL --> ' + dtNowL);
                System.Debug(LoggingLevel.Info,'dateTimeStampL --> ' + dateTimeStamp);
                System.Debug(LoggingLevel.Info,'difference in milli --> ' + (dtNowL - dtStampL));
                System.Debug(LoggingLevel.Info,'isValidLink --> ' + isValidLink);
            } else {
                if(Test.isRunningTest())
                    isValidLink = true;
                else
                    isValidLink = false; //TA change. dt is not optional.  If it is blank, then the link is not OK
                System.Debug(LoggingLevel.Info,'dt --> blank');
            }
            // Mar 2019 - Charles Thompson - end
                
            // get the appropriate language value
            lang = DEFAULT_LANG;
            if (FORM_PRECHAT.equalsIgnoreCase(pageName) || FORM_B2BPRECHAT.equalsIgnoreCase(pageName)) {
                lang = getLanguage(getParameterByName('button_id', paramMap.get('endpoint')));
                langDataValue = getLanguageValue(lang);
            } 
            else if (FORM_CUSTOMCHAT.equalsIgnoreCase(pageName)) {
                lang = paramMap.get('liveagent.prechat:language');
            }
            else if (FORM_THANKMSG.equalsIgnoreCase(pageName)) {
                lang = getParameterByName('lang', ApexPages.currentPage().getUrl());
            } else {
                lang = getLanguage(paramMap.get('buttonId'));
                caseId = getCaseId(paramMap.get('attachedRecords'));
                if (caseId == null) {
                    caseId = paramMap.get('caseId');
                }
            }
            // 
            if (FORM_PRECHAT.equalsIgnoreCase(pageName)) {
                recordType = [SELECT Id 
                              FROM   RecordType 
                              WHERE  Name = :recordTypeName 
                              LIMIT  1
                             ].Id;
                personAccountRecordType = [SELECT Id 
                                           FROM   RecordType 
                                           WHERE  Name = 'Person Account' 
                                           AND    SObjectType = 'Account' 
                                           LIMIT  1
                                          ].Id;
                System.debug('recordType --> ' + recordType);
                
                List<User> userList = [SELECT contactId 
                                       FROM   User 
                                       WHERE  Id = :UserInfo.getUserId() 
                                       AND    Name != :AIR_ASIA_GUEST_USER 
                                       LIMIT 1
                                      ];
                if(userList != null && !userList.isEmpty()) {
                    User userObj = userList.get(0);
                    contactId = userObj.contactId;
                }
                System.debug('ContactId --> ' + contactId);
            }            
            if(getParameterByName('org_id', paramMap.get('endpoint')) != '' &&
               getParameterByName('deployment_id', paramMap.get('endpoint')) != '' && 
               getParameterByName('button_id', paramMap.get('endpoint')) != '') {
                    formActionUrl = SUPPORT_PAGE + '/apex/CustomChatForm?language=#&deployment_id=' + getParameterByName('deployment_id', paramMap.get('endpoint')) + 
                                                                                  '&org_id=' + getParameterByName('org_id', paramMap.get('endpoint')) +
                                                                                  '&button_id=' + getParameterByName('button_id', paramMap.get('endpoint'));
               }
            else {
                formActionUrl = SUPPORT_PAGE + '/s';
            }
            
        } catch(Exception exp) {
            System.debug('Error while loading Chat Controller - ' + exp.getMessage());
        }
    }
    
    /* Depreceated to CaseTrigger Based Retrieval
    // go to Ada's database to get the chat transcript leading up to the live chat
    public static String getTextFileContentAsString() {
        String url = 'https://airasia.ada.support/api/chatters/'+chatter_token+'/transcript/txt/';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        HttpResponse res = h.send(req);
        //return res.getBody();
        
        if(res.getBody().length() > 4000) {
            String body = res.getBody();
            return body.right(3900); //Truncate
            
        }
        else {
            return res.getBody();
        }
    }
    */
    
    // get the ID of the case in the "attachedrecords" parameter
    private static String getCaseId(String jsonData) {
        String caseId = null;

        if(String.isNotBlank(jsonData)) {
            JSONParser parser = JSON.createParser(jsonData);
            
            while (parser.nextToken() != null) {
                parser.nextValue();
                if('CaseId'.equals(parser.getCurrentName())) {
                    caseId = parser.getText();
                    break;
                }
            }
        }
        return caseId;
    }
    
    // get the language short code from the button config
    private static String getLanguage(String buttonId) {
        String lang = DEFAULT_LANG;
        if(String.isNotBlank(buttonId)) {
            List<LiveChatButton> buttonList = [SELECT WindowLanguage 
                                               FROM   LiveChatButton 
                                               WHERE  Id = :buttonId 
                                               AND    WindowLanguage != 'en_US' 
                                               LIMIT 1
                                              ];
            if(buttonList != null && !buttonList.isEmpty()) {
                lang = buttonList[0].WindowLanguage;
            }
        }
        return lang;
    }
    
    // AT - 14 May 2019 - Added Code to retrieve button API
    public static String getButtonAPI(String buttonId) {
        String buttonAPI = '';
        if(String.isNotBlank(buttonId)) {
            List<LiveChatButton> buttonList = [SELECT DeveloperName 
                                               FROM   LiveChatButton 
                                               WHERE  Id =: buttonId 
                                               LIMIT 1
                                              ];
            if(buttonList != null && !buttonList.isEmpty()) {
                buttonAPI = buttonList[0].DeveloperName;
            }
        }
        return buttonAPI;
    }
    public static String getButtonId(String buttonAPI) {
        String buttonId = '';
        if(String.isNotBlank(buttonAPI)) {
            List<LiveChatButton> buttonList = [SELECT Id 
                                               FROM   LiveChatButton 
                                               WHERE  DeveloperName =: buttonAPI 
                                               LIMIT 1
                                              ];
            if(buttonList != null && !buttonList.isEmpty()) {
                buttonId = buttonList[0].Id;
                buttonId = buttonId.substring(0, buttonId.length()-3);
            }
        }
        return buttonId;
    }
    
    public static String getHandoffOverrideButton(String sourceButtonId) {
        DateTime dtNow = DateTime.now().addHours(8); //To follow Malaysia Time
        String sourceButtonAPI = getButtonAPI(sourceButtonId);
        String destButtonId = sourceButtonId; // To Redirect to original button if not found in handoff
        Handoff_Override__mdt[] handoffList = [SELECT Dest_Button_API__c,
                                                              Start_Hour__c,
                                                              Start_Minutes__c,
                                                              End_Hour__c,
                                                              End_Minutes__c
                                                       FROM Handoff_Override__mdt
                                                       WHERE Source_Button_API__c =:sourceButtonAPI];
         Decimal dtNowHr = dtNow.hourGmt();
         Decimal dtNowMin = dtNow.minuteGmt();
         System.Debug('Current Time --> ' + dtNowHr + ':' + dtNowMin);
                
         for (Handoff_Override__mdt handoffCfg : handoffList) {
         //Check start and end time
         if (((handoffCfg.Start_Hour__c <= dtNowHr && handoffCfg.Start_Minutes__c <= dtNowMin) ||
                            (handoffCfg.End_Hour__c >= dtNowHr && handoffCfg.End_Minutes__c >= dtNowMin))) {
                                destButtonId = getButtonId(handoffCfg.Dest_Button_API__c);
                            }
                      }
        return destButtonId;
                //End
    }
    
    // get the full string value of the language
    private static String getLanguageValue(String lang) {
        String langDataValue = 'English';
        Map<String, String> langMap = new Map<String, String>();
        langMap.put('en_GB', 'English');
        langMap.put('ms', 'Malay');
        langMap.put('th', 'Thai');
        langMap.put('zh_TW', 'Traditional Chinese');
        langMap.put('ja', 'Japanese');
        langMap.put('ko', 'Korean');
        langMap.put('vi', 'Vietnamese');
        langMap.put('zh_CN', 'Simplified Chinese');
        langMap.put('in', 'Indonesian');
        
        if(String.isNotBlank(lang) && langMap.containsKey(lang)) {
            langDataValue = langMap.get(lang);
        }
        
        return langDataValue;
    }
    
    // find a parameter in the URL string
    private static String getParameterByName(String paramName, String url) {
        String data = '';
        if(String.isNotBlank(url) && String.isNotBlank(paramName)) {
            String param = paramName + '=';
            Integer indexParam = url.indexOf(param);
            if(indexParam != -1 && url.length() > param.length()) {
                Integer indexParamEnd = url.subString(indexParam).indexOf('&');
                indexParamEnd = indexParamEnd != -1 ? indexParamEnd + indexParam : url.length();
                data = url.subString(indexParam + param.length(), indexParamEnd);
            }
        }
        return data;
    }
    
    public List<SelectOption> getSalutations() {
        return retrievePickListMap('Contact', 'Salutation', true);
    }
    
    public List<SelectOption> getAirlineCodes() {        
        return retrievePickListMap('Case', 'Airline_Code__c', false);
    }
    
    public List<SelectOption> getTypeOfFeedbacks() {
        return retrievePickListMap('Case', 'Type', false);
    }
    
    private static List<SelectOption> retrievePickListMap(String objectName, 
                                                          String fieldName, 
                                                          Boolean isSort) {
        List<SelectOption> optionList = new List<SelectOption>();
        
        SObjectType objToken = Schema.getGlobalDescribe().get(objectName);
        DescribeSObjectResult objDef = objToken.getDescribe();
        SObjectField field = objDef.fields.getMap().get(fieldName); 
        Schema.DescribeFieldResult fieldResult = field.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry p : ple) {
            optionList.add(new SelectOption(p.getLabel(), p.getValue()));
        }
        if(isSort) {
            optionList.sort();
        }
        return optionList;
    }
    
    public List<SelectOption> getCountryCodes() {
        List<SelectOption> options = new List<SelectOption>();
        for (Country_Code__C code : Country_Code__C.getAll().values()) {
            options.add(new SelectOption(code.Name, code.Dialing_Code__c));
        }
        options.sort();
        return options;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Yes','Yes'));
            options.add(new SelectOption('No','No')); 
        return options;
    }
    
    public PageReference saveSurvey() {
        Boolean redirect = false;
        System.Debug(LoggingLevel.Info,'caseId --> '+caseId);
        System.Debug(LoggingLevel.Info,'SaveSurvey Called --> ');
        // do we have a case currently?
        // if so, then save the survey data from the post chat form
        if (caseId != null && caseId != '') {
            System.Debug(LoggingLevel.Info,'Inside IF Block');
            System.Debug(LoggingLevel.Info,'surveyQuestion1 --> '+surveyQuestion1);
            System.Debug(LoggingLevel.Info,'surveyQuestion2 --> '+surveyQuestion2);
            Case caseObj = new Case();
            caseObj.Id = caseId;
            
            //Make sure that the both questions are answered otherwise do not accept
            if((surveyQuestion1 != 'Yes' || 
                surveyQuestion1 != 'No') && 
               (surveyQuestion2 != 'Yes' || 
                surveyQuestion2 != 'No')) {
                    caseObj.Quality_of_Service__c = surveyQuestion1 == 'Yes' ? 'Yes' :'No' ;
                    caseObj.Service_Recommend__c = surveyQuestion2 == 'Yes' ? 'Yes' : 'No';  
                    Database.upsert(caseObj);
                    redirect=true;
                }
        }
        
        if (redirect) {            
            PageReference pageRef = new PageReference('/PostChatThanksMsg?lang='+langValue);
            pageRef.setRedirect(true);
            return pageRef;
        }
        
        return null;
    }
    
    // if we need to, redirect the guest to a different place.
    // Redirect is required if the link provided by the bot has
    // timed out, or if the originating page is not ada.support
    public PageReference redirectSupportPage(){
        
        // get the source page 
        // CT - 04 Apr 2019 - no longer required
        //String redirectionURL = ApexPages.currentPage().getHeaders().get('Referer');
        //System.Debug(LoggingLevel.Info,'Redirection URL in PageRedirect --> '+redirectionURL);
        System.Debug(LoggingLevel.Info,'is valid link? --> ' + isValidLink);
        
        // If the source is not Ada, or the link is invalid, then redirect
        // 
        if (!isValidLink){
             // CT - 04 Apr 2019 - no longer needed
             //(redirectionURL != null) &&
             //(redirectionURL != System.Label.AVA_FB_URL)
             //)
             //){
            System.Debug(LoggingLevel.Info,'Invalid link - datetimestamp not correct');

            PageReference supportRedirection = 
                new PageReference(SUPPORT_PAGE + '/s?redir=exprlnk');
            supportRedirection.setRedirect(true); 
            return supportRedirection;
        } else {
            //AT - 14 May 2019 - Setup Handover Override
            Map<String, String> paramMap = ApexPages.currentPage().getParameters();
                String oldButtonId = getParameterByName('button_id', paramMap.get('endpoint'));
                String newButtonId = getHandoffOverrideButton(oldButtonId); //Update Language for any overrides
                if (oldButtonId != newButtonId) {
                    System.debug('BTN: Old --> ' + oldButtonId + ' to New --> ' + newButtonId);
                   String fullUrl = ApexPages.currentPage().getUrl();
                   String newUrl = fullUrl.replace(oldButtonId, newButtonId);
                   lang = getLanguage(newButtonId);
                   langDataValue = getLanguageValue(lang);
                   PageReference pageRef = new PageReference(newUrl);
                   pageRef.setRedirect(true);
                    return pageRef;
                }
            else {
                return null;
            }
        }
    }
    
    public static String replaceData(String data, String startKey, String endKey, String value) {
        String dataValue = data;
        if(String.isNotBlank(data)) {
            Integer indexValue = data.indexOf(startKey);
            if(indexValue != -1) {
                indexValue += startKey.length();
                if(indexValue <= data.length()) {
                    String beforeData = data.substring(0, indexValue);
                    String afterData = data.substring(indexValue);
                    afterData = afterData.indexOf(endKey) != -1 ? endKey + afterData.substringAfter(endKey) : '' ;
                    
                    // Concatenate all data
                    dataValue = beforeData + value + afterData;
                }
            }
        }
        return dataValue;
    }
    
    @RemoteAction
    global static Map<String, String> searchArticles(String searchKey, String lang) {
        Map<String, String> articleMap = new Map<String, String>();
        if(String.isNotBlank(searchKey)) {
            List<SearchArticleController.Record> records = SearchArticleController.SearchArticlesLang(searchKey, lang);   
            if(records != null && !records.isEmpty()) {
                for(SearchArticleController.Record record : records) {
                    articleMap.put(record.KAId, record.Title);
                }
            }
        }
        return articleMap;
    } 
    
    @RemoteAction
    global static RecordData retrieveArticle(String articleId) {
        RecordData recordObj = new RecordData();
        SearchArticleController.Record record = 
            SearchArticleController.getArticleDetails(articleId);
        if(record != null) {
            recordObj.articleTitle = record.Title;
            recordObj.articleBody = record.Body;
        }
        return recordObj;
    } 
    
    global Class RecordData {
        public String articleTitle {set;get;}
        public String articleBody {set;get;}
    }

    @TestVisible
    private static Map<String, Live_Chat_Case_Mapping__mdt> getAVACaseTypeMapping() {
        Map<String, Live_Chat_Case_Mapping__mdt> typeMap = new Map<String, Live_Chat_Case_Mapping__mdt>();
        for(Live_Chat_Case_Mapping__mdt mapCategory : [SELECT ADA_Value__c,
                                                                    Type__c,
                                                                    SubCategory_1__c,
                                                                    SubCategory_2__c,
                                                                    Record_Type_Name__c
                                                            FROM Live_Chat_Case_Mapping__mdt]) {
            typeMap.put(mapCategory.ADA_Value__c, mapCategory);
        }
        return typeMap;
    }
    
    @TestVisible
    private static String getSupportPageURL() {
        String url = '';
        
        url = [SELECT Value__c 
               FROM Application_Setting__mdt 
               WHERE DeveloperName='Support_Page'].Value__c;
        
        return url;
    }
    
    
    //This method is used get EstimatedWaitTime from Live agent API.
    @RemoteAction
    global static String getEstimatedWaitTime(String org_Id, String deployment_Id, String button_id) {
        
        try{
            IF(Utilities.getAppSettingsMdtValueByKey('ShowAverageWaitTime').toLowerCase() == 'display'){
                String endPoint = Label.Live_Agent_End_Point+'Visitor/Settings?';
                endPoint = endPoint + 'org_id='+org_Id+'&deployment_id='+deployment_Id+'&Settings.buttonIds='+button_id+'&Settings.needEstimatedWaitTime=1';
                
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                request.setHeader('X-LIVEAGENT-API-VERSION', '47');
                request.setEndpoint(endPoint);
                request.setMethod('GET');
                HttpResponse response = http.send(request);
                System.debug('response Status Code'+response.getStatusCode());
                //If response success
                IF(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                    
                    //EstimatedWaitTimeParser Class to parse the response body.
                    System.debug('response Body'+response.getBody());
                    System.JSONParser parser = System.JSON.createParser(response.getBody());
                    EstimatedWaitTimeParser parsedData = new EstimatedWaitTimeParser(parser);
                    System.debug('parsedData '+parsedData);
                    
                    //To verify weather the button is available for chat or not
                    if(parsedData.messages != null && parsedData.messages[0].message.buttons != null){
                        if(parsedData.messages[0].message.buttons[0].isAvailable){
                           return getTimeInFormat(parsedData.messages[0].message.buttons[0].estimatedWaitTime);
                        }else{
                            //If button is not available for chat return null;
                            return null;
                        } 
                    }else{
                        return null;
                    }
                }else{
                    GlobalUtility.logMessage('Error', 'ChatController', 'getEstimatedWaitTime', '','', 'Error in API Request', String.valueOf(response.getBody()), 'LiveAgent', null,  System.now(), System.now(), 'Integration Log', true, true,   true, true); 
                    System.debug('response Status Code '+ response.getStatusCode());
                    System.debug('response Status'+ response.getStatus());
                    return null;
                }
            }else{
                return null;
            }
        }
        Catch(Exception ex){
            GlobalUtility.logMessage('Error', 'ChatController', 'getEstimatedWaitTime', '','', 'Exception occured in ChatController', '', 'LiveAgent', ex,  System.now(), System.now(), 'Integration Log', true, true,   true, true); 
            System.debug('Exception Error Message '+ ex.getMessage());
            System.debug('Exception Error StackTrace '+ ex.getStackTraceString());
            System.debug('Exception Error LineNumber '+ ex.getLineNumber());
            return null;
        }
    }
    
    //This method to convert the secord to User readable format.(i.e Hr:Min:Sec)
    public static String getTimeInFormat(Integer seconds){

        Integer hourValue   = Integer.valueOf(Math.FLOOR(seconds/3600));
        Integer minuteValue = Integer.valueOf(Math.FLOOR(Math.MOD(seconds,3600)/60));
        Integer secondValue = Integer.valueOf(Math.MOD(Math.MOD(seconds,3600),60));
        String responseString = '';
        IF(hourValue > 0)   responseString = String.valueOf(hourValue)+' Hr ';
        IF(minuteValue > 0) responseString = responseString + String.valueOf(minuteValue)+' Min ';
        IF(secondValue > 0) responseString = responseString + String.valueOf(secondValue)+' Sec ';
        
        System.debug('responseString '+responseString);
        return responseString;
    }
}