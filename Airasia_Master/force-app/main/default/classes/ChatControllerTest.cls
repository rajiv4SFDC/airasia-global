@isTest
public class ChatControllerTest {
    
    @isTest(SeeAllData=true)
    private static void testChatControllerPrechatForm() {
        
        LiveChatButton button = [SELECT Id FROM LiveChatButton LIMIT 1];
        
        Test.startTest();
        
        PageReference pageRef = Page.PreChatForm;
        Test.setCurrentPage(pageRef);
        
        String full_name = 'Test Name';
        String booking_id = 'BR1234';
        String email_address = 'test@testmail.com';
        String chatter_token = '22322MNTEST';
        String chatter_id = 'test';
        String category = 'GeneralQA';
        
        ApexPages.currentPage().getParameters().put('endpoint','=test?button_id='+button.Id+'&full_name='+full_name+'&booking_id='+booking_id+'&email_address='+email_address+'&chatter_token='+chatter_token+'&chatter_id='+chatter_id+'&category='+category);
        ApexPages.currentPage().getHeaders().put('Referer', 'https://airasia.ada.support/chat/');

        ChatController controller = new ChatController();

        System.assert(!controller.getSalutations().isEmpty());
        System.assert(!controller.getAirlineCodes().isEmpty());
        System.assert(!controller.getTypeOfFeedbacks().isEmpty());
        System.assert(!controller.getItems().isEmpty());
        String data = ChatController.replaceData('KEY#DATA&VALUE', 'KEY#', '&', 'TEST');
        System.assert(data == 'KEY#TEST&VALUE');
        
        Test.stopTest();
    }
    
    @isTest(SeeAllData=false)
    private static void testChatControllerCustomForm() {
        
        Test.startTest();
        
        PageReference pageRef = Page.CustomChatForm;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('liveagent.prechat:language', 'en_GB');
        ApexPages.currentPage().getHeaders().put('Referer', 'https://airasia.ada.support/chat/');
        
        ChatController controller = new ChatController();
        

        System.assert(ChatController.lang == 'en_GB' || ChatController.lang == 'en_US');
        
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        System.assert(!controller.getCountryCodes().isEmpty());
        
        Test.stopTest();
    }
    
    @isTest(SeeAllData=false)
    private static void testChatControllerPostForm() {
        
        Test.startTest();
        
        PageReference pageRef = Page.PostChatForm;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('attachedRecords', '{"test":"test"}');
        ApexPages.currentPage().getHeaders().put('Referer', 'https://airasia.ada.support/chat/');
        
        ChatController controller = new ChatController();

        System.assert(ChatController.caseId == null);
        
        Test.stopTest();
    }
    
    @isTest(SeeAllData=false)
    private static void testChatControllerPostForm1() {
        
        Case caseObj = new Case();
        caseObj.Subject = 'Test';
        insert caseObj;
        
        Test.startTest();
        
        PageReference pageRef = Page.PostChatForm;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('Referer', 'https://airasia.ada.support/chat/');
        ApexPages.currentPage().getParameters().put('attachedRecords', '{"CaseId":"'+caseObj.Id+'"}');
        ChatController controller = new ChatController();
        System.assert(ChatController.caseId == caseObj.Id);
        
        controller.surveyQuestion1 = 'Yes';
        controller.surveyQuestion2 = 'Yes';
        controller.saveSurvey();
        
        controller.redirectSupportPage();
        caseObj = [SELECT Quality_of_Service__c, Service_Recommend__c FROM Case WHERE Id =:caseObj.Id LIMIT 1];
        System.assert(caseObj != null);
        System.assertEquals(caseObj.Quality_of_Service__c,'Yes');
        System.assertEquals(caseObj.Service_Recommend__c,'Yes');
        
        Test.stopTest();
    }
    
    @isTest(SeeAllData=false)
    private static void testChatControllerException() {       
        Test.startTest();
        ChatController controller = new ChatController();
        System.assert(ChatController.caseId == null);
        Test.stopTest();
    }
    
    @isTest(SeeAllData=false)
    private static void testChatControllerArticleSearch() {  
        
        User u = [select Id,UserPermissionsKnowledgeUser,UserPermissionsSupportUser from User where Id =: UserInfo.getUserId() LIMIT 1];
            u.UserPermissionsKnowledgeUser = true;
            u.UserPermissionsSupportUser = true;
        update u;
        
        System.runAs(u) {
            FAQ__kav KAVersion = new FAQ__kav();
            KAVersion.Title = 'XYZ';
            KAVersion.UrlName = 'XYZ';
            KAVersion.Language='en_US';
            KAVersion.Answer__c = 'TEST DATA';
            insert KAVersion;
            KAVersion = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Id = :KAVersion.Id];
            KbManagement.PublishingService.publishArticle(KAVersion.KnowledgeArticleId, true);
            
            Test.startTest();
            PageReference pageRef = Page.PreChatForm;
            Test.setCurrentPage(pageRef);
            ChatController controller = new ChatController();
            ChatController.searchArticles('TEST DATA', 'en_GB');
            ChatController.RecordData record = ChatController.retrieveArticle(KAVersion.Id);
            System.assert(record != null);
            System.assert(record.articleTitle == 'XYZ');
            System.assert(record.articleBody == 'TEST DATA');
            Test.stopTest();
        }
    }
    
    @isTest 
    private static void testCallout() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        String EstimatedWaitTime = ChatController.getEstimatedWaitTime('OrgId','DeploymentId','buttonId');
        System.assertEquals('1 Min 54 Sec ', EstimatedWaitTime);
    }
}