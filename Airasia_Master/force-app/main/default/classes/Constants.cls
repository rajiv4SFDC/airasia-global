//This apex class will be used to store all constants
@isTest(seealldata=false)
public class Constants {

    //Constants values for application log fields
    //Debug level, log level
    public static final String LOG_DEBUG_LEVEL_DEBUG = 'Debug';
    public static  final String LOG_DEBUG_LEVEL_ERROR = 'Error';
    public static  final String LOG_DEBUG_LEVEL_INFO = 'Info';
    public static  final String LOG_DEBUG_LEVEL_WARNING = 'Warning';
    
    public static  final String LOG_TYPE_ERROR_LOG = 'Error Log';
    public static  final String LOG_TYPE_INTEGRATION_LOG = 'Integration Log';
    public static  final String LOG_TYPE_JOB_LOG = 'Job Log';
    
    //Datetime format for BQ integration. Please do not change.
    public static  final String dtTimeGMTFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
    
    //Picklist values for BQIntegration__mdt.Type field
    public static  final String BQ_INTEGRATION_CONFIG_TYPE_SEND = 'Send';
    public static  final String BQ_INTEGRATION_CONFIG_TYPE_DELETE = 'Delete';
    
    //BQ Integration Application name in Application Logs
    public static  final String APPLICATION_NAME_BQ_INTEGRATION = 'BQIntegration';

    //Field API name for dynamic usage
    public static final String SYSTEM_FIELD_LAST_MODIFIED_DATE = 'LastModifiedDate'; 
    public static final String SYSTEM_FIELD_SYSTEM_MOD_STAMP = 'SystemModstamp'; 

    //Case status values
    public static final String CASE_STATUS_CLOSED = 'Closed';

    //Case type values
    public static final String CASE_TYPE_COMPLIMENT = 'Compliment';

    //Auto-close compliment case Application name in Application Logs
    public static final String APPLICATION_NAME_CASE_PROCESS_BATCH = 'CaseProcessBatch';

    //Auto-close compliment cases custom metadata name
    public static final String CUSTOM_METADATA_AUTO_CLOSE_COMPLIMENT_CASES = 'Auto_Closed_Cases_Queue'; 


}