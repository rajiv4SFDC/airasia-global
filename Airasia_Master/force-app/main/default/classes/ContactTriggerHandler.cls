/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
09/OCT/2017      Sharan Desai   		Created
*******************************************************************/
public class ContactTriggerHandler {
    
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    
    public static void CheckPrimaryContact(Contact[] contacts,boolean isInsert){
        
        DateTime startTime = DateTime.now();
        List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
        System_Settings__mdt logLevelCMD = null;
        
        try{
            
            logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('ContactTrigger_Log_Level_CMD_NAME'));
            Map<String,String> contactExtIdAndAccountIdMap = new Map<String,String>();
            
            //STEP 1 :- Build a map with Contact External ID as key and associated AccountId as Value
            for(Contact eachContact : contacts){
                if(String.isNotBlank(eachContact.Contact_ExtId__c)){
                    contactExtIdAndAccountIdMap.put(eachContact.Contact_ExtId__c, eachContact.AccountId);
                }
            }
            
            if(contactExtIdAndAccountIdMap!=null && !contactExtIdAndAccountIdMap.isEmpty()){
            
                //STEP 2:- Query Existing contacts which are needs to de-associated as Primary Contact from an Account
                List<Contact> updateContactList = new List<Contact>();                
                for(Contact existingContact : [SELECT AccountId,Contact_ExtId__c,Is_Primary__c FROM Contact WHERE AccountId IN: contactExtIdAndAccountIdMap.values() AND Is_Primary__c=true]){
                    
                    if(!contactExtIdAndAccountIdMap.containsKey(existingContact.Contact_ExtId__c)){
                        existingContact.is_primary__c = false;
                        updateContactList.add(existingContact);
                    }                    
                }    
                
                //STEP 3:- update contacts to de-associate as Primary 
                update updateContactList;
                
                
                //-- This block is used to handle recurrsive call for update context
                if(isInsert){
                    run=true;    
                }                
            }
            
        }catch(DMLException CheckPrimaryContactExe){
            
            System.debug('CheckPrimaryContactExe'+CheckPrimaryContactExe);
            
            //-- Perform Application logging
            String processLog = 'Contact Trigger Failed to de-associate existing Primary Contact \r\n';
            processLog += 'REASON :: '+CheckPrimaryContactExe.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'ContactTriggerHandler', 'CheckPrimaryContact',
                                                                 null, 'ContactTrigger', processLog, null,'Error Log',  startTime, logLevelCMD, CheckPrimaryContactExe));
            
        }finally{      
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }  
        
    }
    
}