/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
12/OCt/2017      Sharan Desai   		Created
*******************************************************************/
@IsTest(SeeAllData=false)
public class ContactTrigger_Test {
    
    private static testMethod void testExistingPrimaryContactDeAssociation() {
        
        Profile profileObj = [SELECT ID FROM Profile Where Name='Integration User'];
        
        User user = new User();
        user.FirstName = 'User FN';
        user.LastName = 'User MN';
        user.MobilePhone = '12321321';
        user.Username = 'testUserSD@gmail.com';
        user.Email = 'testUserSD@gmail.com';
        user.Alias = 'testSD';        
        user.TimeZoneSidKey = 'Asia/Kuala_Lumpur';
        user.LocaleSidKey = 'en_MY';
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profileObj.Id;
        user.CurrencyIsoCode = 'MYR';
        user.LanguageLocaleKey = 'en_US';
        insert user;
        
        Account newAccount = new Account();
        newAccount.Name='Test Name';
        insert newAccount;
        
        System.runAs(user){
            
            Contact newContact = new Contact();
            newContact.FirstName='FN 1';
            newContact.MiddleName='MN 1';
            newContact.LastName='LN 1';
            newContact.Phone='312312';
            newContact.AccountId=newAccount.Id;
            newContact.Is_Primary__c=true;
            newContact.Contact_ExtId__c='Sharan 123';
            insert newContact;
            
            Test.startTest();
                Contact newContact1 = new Contact();
                newContact1.FirstName='FN 2';
                newContact1.MiddleName='MN 2';
                newContact1.LastName='LN 2';
                newContact1.Phone='3123121';
                newContact1.AccountId=newAccount.Id;
                newContact1.Is_Primary__c=true;
                newContact1.Contact_ExtId__c='Sharan 1234';
            insert newContact1;
            Test.stopTest();
            
            
            System.assertEquals(false, [SELECT Is_Primary__c FROM CONTACT WHERE Id=:newContact.Id].Is_Primary__c);
            
            
        }
        
    }

}