/*******************************************************************
Author:        Venkatesh Budi
Company:       Salesforce.com
Description:   Create case
Test Class:    CreateCase_Test

History:
Date            Author              Comments
-------------------------------------------------------------
07-06-2017      Venkatesh Budi        Created
*******************************************************************/
public without sharing class CreateCase {
    @AuraEnabled
    public static String getUserType() {
        return UserInfo.getUserType();
    }
    
    private static Set<String> refundExceptionSet = 
        new Set<String>{'Serious Illness / Injury', 'Deceased Guest'};
        
    @AuraEnabled
    public static Case saveCase (Case newCase, String caseComments, String mobilephoneValue, String alternatephoneValue, String flightRefundList, String passengerRefundList) {
        // User should be Community User
        if(checkCommunityUser()) {
            newCase.Comments__c = caseComments;
            if(newCase.Type == 'Refund'){
                RecordType caseRec = [select Id, SobjectType from RecordType where developerName =: 'Refund_Case' and SobjectType =: 'Case'];
                newCase.RecordTypeId = caseRec.Id;
                // Added Sent to RPS as New for Refund type except sub category 2
                newCase.Send_to_RPS__c = !refundExceptionSet.contains(newCase.Sub_Category_2__c) ? 'New' : null;            
            }else{
                RecordType caseRec = [select Id, SobjectType from RecordType where developerName =: 'Auto_Case_Creation_Enquiry_Compliment_Complain' and SobjectType =: 'Case'];
                newCase.RecordTypeId = caseRec.Id;
            }
            if(UserInfo.getUserType() != 'Guest'){
                User usr = [select AccountId, contact.email from User where Id =: UserInfo.getUserId() LIMIT 1];
                //User usr = [select AccountId,ContactId from User where Id =: UserInfo.getUserId()];
                newCase.AccountId = usr.AccountId;
                newCase.SuppliedEmail = usr.contact.email;
            }else{
                try{        
                    Account acct = [select Id, PersonEmail, PersonContactId from Account where PersonEmail =: newCase.SuppliedEmail LIMIT 1];
                    newCase.ContactId = acct.PersonContactId;
                    newCase.AccountId = acct.Id;
                }catch(Exception e){
                    /*
                    if(newCase.SuppliedName != null){
                    RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
                    Account newPersonAccount = new Account();
                    
                    String SuppliedName = newCase.SuppliedName;
                    system.debug('SuppliedName: '+SuppliedName);
                    String[] nameList = SuppliedName.split('\\s|&nbsp;');
                    system.debug('nameList: '+nameList);
                    
                    newPersonAccount.FirstName = nameList[0];
                    newPersonAccount.LastName = nameList[1];
                    newPersonAccount.PersonEmail = newCase.SuppliedEmail;
                    newPersonAccount.RecordType = personAccountRecordType;
                    
                    insert newPersonAccount;
                    Account pAcct = [select Id, PersonContactId  from Account where Id =: newPersonAccount.Id];
                    //system.debug('------- '+pAcct.PersonContactId );
                    newCase.AccountId = newPersonAccount.Id;
                    newCase.ContactId = pAcct.PersonContactId;
                    
                    if(String.isNotBlank(mobilephoneValue)) {                    
                    Contact newPersonContact = new Contact();
                    PhoneData data = getPhoneData(mobilephoneValue);
                    newPersonContact.Id = pAcct.PersonContactId;
                    newPersonContact.MobilePhone = data.phone;
                    newPersonContact.Mobile_Country_Code__c = data.countryCode;
                    Database.upsert(newPersonContact);
                    }
                    }
                    */
                }
            }
            if(newCase.Case_Language__c == 'en_US'){
                newCase.Case_Language__c ='English';
            }else if(newCase.Case_Language__c == 'in'){
                newCase.Case_Language__c ='Indonesian';
            }else if(newCase.Case_Language__c == 'ja'){
                newCase.Case_Language__c ='Japanese';
            }else if(newCase.Case_Language__c == 'ms'){
                newCase.Case_Language__c ='Malay';
            }else if(newCase.Case_Language__c == 'th'){
                newCase.Case_Language__c ='Thai';
            }else if(newCase.Case_Language__c == 'vi'){
                newCase.Case_Language__c ='Vietnamese';
            }else if(newCase.Case_Language__c == 'zh_CN'){
                newCase.Case_Language__c ='Simplified Chinese';
            }else if(newCase.Case_Language__c == 'zh_HK' || newCase.Case_Language__c == 'zh_TW'){
                newCase.Case_Language__c =' Traditional Chinese';
            }else if(newCase.Case_Language__c == 'ko'){
                newCase.Case_Language__c ='Korean';
            }else{
                newCase.Case_Language__c ='English';
            }
            system.debug('Case Language' + newCase.Case_Language__c);
            
            if(String.isNotBlank(mobilephoneValue)) {
                PhoneData data = getPhoneData(mobilephoneValue);
                newCase.Mobile_Phone_Number__c = data.phone;
                newCase.Mobile_Phone_Country_Code__c = data.countryCode;
            }
            if(String.isNotBlank(alternatephoneValue)) {
                PhoneData data = getPhoneData(alternatephoneValue);
                newCase.Alternate_Phone_Number__c = data.phone;
                newCase.Alternate_Phone_Country_Code__c = data.countryCode;
            }
            if(String.isNotBlank(flightRefundList)) {
                newCase.Passenger_List_to_be_Refunded__c = flightRefundList;
            }
            if(String.isNotBlank(passengerRefundList)) {
                newCase.Sector_List_to_be_Refunded__c = passengerRefundList;
            }
            
            newCase.Reference_Number__c = 'AIRASIA'+Math.round(Math.random()*100000);
            insert newCase;
            
            /*  if(caseComment != null){
            CaseComment cc = new CaseComment(ParentId = newCase.Id,CommentBody = caseComment);
            insert cc;
            }*/
            
            // Sprint 3 Changes
            newCase = [SELECT Id, CaseNumber FROM Case WHERE Id =:newCase.Id LIMIT 1];
        } else {
            throw new AuraHandledException('NON_COMMUNITY_USER');
        }
        
        return newCase;
    }
    
    /*@AuraEnabled
        public static Case saveCaseOld (Case newCase, List<String> attachmentIds) {
        insert newCase;
        
        List<String> wrapperStr = new List<String>();
        for(String key : attachmentIds){
        wrapperStr.add(key);
        }
        List<Attachment> attachments = [select Id, name, body from Attachment where Id IN: wrapperStr];
        List<Attachment> newAttachments = new List<Attachment>();
        for(Attachment a : attachments){
        Attachment attach = New Attachment(Name = a.name, body = a.body);
        attach.parentId = newCase.Id;
        newAttachments.add(attach);
        }
        insert newAttachments;
        delete attachments;
        
        return newCase;
        }*/
    
    @AuraEnabled
    public static List<PicklistFieldController.PicklistWrapper> getDependentOptionsImpl(
            string objApiName, 
            string contrfieldApiName, 
            string depfieldApiName){
        return PicklistFieldController.getDependentOptionsImpl(objApiName, 
                                                               contrfieldApiName, 
                                                               depfieldApiName);
    }
    
    @AuraEnabled
    public static List<PicklistFieldController.PicklistWrapper> getPicklistVals(string objApiName, string fieldApiName){
        return PicklistFieldController.getPicklistVals(objApiName, fieldApiName);
    }
    
    @AuraEnabled
    public static List<CountryData> getCountryCode() {
        CountryData cData = null;
        List<CountryData> cDataList = new List<CountryData>();
        for (Country_Code__C code : Country_Code__C.getAll().values()) {
            cData = new CountryData();
            cData.label = code.Name.trim();
            cData.value = code.Dialing_Code__c.trim();
            cDataList.add(cData);    
        }
        cDataList.sort();
        return cDataList;
    }
    
    public class CountryData implements Comparable {
        @AuraEnabled
        public String label{set;get;}
        @AuraEnabled
        public String value{set;get;}
        
        public Integer compareTo(Object compareTo) {
            CountryData cData = (CountryData) compareTo;
            if (label == cData.label) return 0;
            if (label > cData.label) return 1;
            return -1;        
        }
    }
    
    private static PhoneData getPhoneData(String phoneValue) {
        PhoneData data = new PhoneData();
        
        if(String.isNotBlank(phoneValue)) {
            data.countryCode = phoneValue.substringBefore(' ');
            data.phone = phoneValue.substringAfter(' ');
        }
        
        return data;
    }
    
    public class PhoneData {
        public String countryCode {set;get;}
        public String phone {set;get;}
    }
    
    @AuraEnabled
    public static eformCaseData getCaseLayout(){
        eformCaseData eformData = new eformCaseData();
        eformData.eformLayout = [SELECT Label, Airline_Code__c, Case_Currency__c, BSB_Code__c, Iban_Code__c, IFSC_Code__c, Sort_Code__c, Swift_Code__c FROM Eform_Case_Layout__mdt];
        eformData.eformElement = [SELECT Label, Match_Field__c, Visible__c, Class__c, Error_Value_Missing__c, Error_Pattern_Mismatch__c, Name__c, Pattern__c, Max_Length__c, Min_Length__c, Required__c, Type__c, Order__c FROM Eform_Case_Element__mdt ORDER BY Order__c];
        return eformData;
    }
    
    public class eformCaseData {
        @AuraEnabled
        public List<Eform_Case_Layout__mdt> eformLayout {set;get;}
        @AuraEnabled
        public List<Eform_Case_Element__mdt> eformElement {set;get;}
    }
    
    public class CustomSelectOption implements Comparable {
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public String value { get;set; }
        public CustomSelectOption(String label, String value) {
            this.label = label;
            this.value = value;
        }
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            CustomSelectOption compareToOption = (CustomSelectOption) compareTo;
            if (label == compareToOption.label) return 0;
            if (label > compareToOption.label) return 1;
            return -1;        
        }
    }
    
    @AuraEnabled
    public static List<CustomSelectOption> fetchSalutation() {
        return retrievePickListMap('Contact', 'Salutation', true);
    }
    
    public static List<CustomSelectOption> retrievePickListMap(String objectName, String fieldName, Boolean isSort) {
        List<CustomSelectOption> optionList = new List<CustomSelectOption>();
        try {
            SObjectType objToken = Schema.getGlobalDescribe().get(objectName);
            DescribeSObjectResult objDef = objToken.getDescribe();
            SObjectField field = objDef.fields.getMap().get(fieldName); 
            Schema.DescribeFieldResult fieldResult = field.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            for(Schema.PicklistEntry p : ple) {
                optionList.add(new CustomSelectOption(p.getLabel(), p.getValue()));
            }
            if(isSort) {
                optionList.sort();
            }
        }catch(Exception exp) {
            System.debug('Sal EXP - ' + exp);
        }
        return optionList;
    }
    
    private static boolean checkCommunityUser(){
        List<User> ccUser = [SELECT Id, Profile.Name FROM User WHERE Id =: UserInfo.getUserId() AND 
                             Profile.Name IN ('AirAsia Profile', 'AirAsia Community Login User') LIMIT 1];
        return ccUser != null && !ccUser.isEmpty();
    }
}