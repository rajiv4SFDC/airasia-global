@isTest
public class CreateCase_Test {
    public static testmethod void createCaseTestMethoden() {
        CreateCase.getUserType();
       
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'AirAsia Profile' limit 1];
        System.runAs(u){
            
            Case c1 = new Case(Type = 'Refund' , Sub_Category_1__c = 'Airport Tax' , Case_Language__c = 'in');        
            CreateCase.saveCase(c1, 'Test Comment','+60 1231231231', '+60 1231231231', '', '');
            CreateCase.getDependentOptionsImpl('case', 'Case_Currency__c','bankCurrency');
            CreateCase.getPicklistVals('case','Case_Currency__c');
			Case c9 = new Case(Type = 'Refund' , Sub_Category_1__c = 'Airport Tax' , Case_Language__c = 'en_US');        
            CreateCase.saveCase(c9, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');            
        }        
        System.assert(!CreateCase.getCountryCode().isEmpty());
        System.assert(CreateCase.getCaseLayout() != null);
        System.assert(!CreateCase.fetchSalutation().isEmpty());
        try {
            Case c10= new Case(Type = 'Refund' , Sub_Category_1__c = 'Airport Tax');        
            CreateCase.saveCase(c10, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
        } catch(Exception exp) {
            System.assert(exp instanceOf AuraHandledException);
        }
    }
    
    public static testmethod void createCaseTestMethodja() {
        CreateCase.getUserType();
       
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'AirAsia Profile' limit 1];
        System.runAs(u){
            
            Case c2 = new Case(Case_Language__c = 'ja');        
            CreateCase.saveCase(c2, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            //Case gsc = new Case(Type = 'Refund' , Sub_Category_1__c = 'Airport Tax' , Case_Language__c = 'in' , SuppliedEmail = 'rachelgreen@gmail.com' , SuppliedName = 'Rachel Green'); 
        }        
        System.assert(!CreateCase.getCountryCode().isEmpty());
        System.assert(CreateCase.getCaseLayout() != null);
        System.assert(!CreateCase.fetchSalutation().isEmpty());
    }
    public static testmethod void createCaseTestMethodms() {
        CreateCase.getUserType();
       
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'AirAsia Profile' limit 1];
        System.runAs(u){
            
			Case c3 = new Case(Case_Language__c = 'ms');        
            CreateCase.saveCase(c3, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
        }        
        System.assert(!CreateCase.getCountryCode().isEmpty());
        System.assert(CreateCase.getCaseLayout() != null);
        System.assert(!CreateCase.fetchSalutation().isEmpty());
    }
    public static testmethod void createCaseTestMethodth() {
        CreateCase.getUserType();
       
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'AirAsia Profile' limit 1];
        System.runAs(u){
            
			Case c4 = new Case(Case_Language__c = 'th');        
            CreateCase.saveCase(c4, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
        }        
        System.assert(!CreateCase.getCountryCode().isEmpty());
        System.assert(CreateCase.getCaseLayout() != null);
        System.assert(!CreateCase.fetchSalutation().isEmpty());
    }
    public static testmethod void createCaseTestMethodvi() {
        CreateCase.getUserType();
       
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'AirAsia Profile' limit 1];
        System.runAs(u){
            
			Case c5 = new Case(Case_Language__c = 'vi');        
            CreateCase.saveCase(c5, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            
            //Case c6 = new Case(Case_Language__c = 'zh_CN');        
            //CreateCase.saveCase(c6, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            
            //Case c7 = new Case(Case_Language__c = 'zh_TW');        
            //CreateCase.saveCase(c7, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            
            //Case c8 = new Case(Case_Language__c = 'ko');        
            //CreateCase.saveCase(c8, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            
            
            
            //Case c10= new Case(Type = 'Refund' , Sub_Category_1__c = 'Airport Tax');        
            //CreateCase.saveCase(c10, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
        }        
        System.assert(!CreateCase.getCountryCode().isEmpty());
        System.assert(CreateCase.getCaseLayout() != null);
        System.assert(!CreateCase.fetchSalutation().isEmpty());
    }
    public static testmethod void createCaseTestMethodzhcn() {
        CreateCase.getUserType();
       
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'AirAsia Profile' limit 1];
        System.runAs(u){
            
			Case c6 = new Case(Case_Language__c = 'zh_CN');        
            CreateCase.saveCase(c6, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            
        }        
        System.assert(!CreateCase.getCountryCode().isEmpty());
        System.assert(CreateCase.getCaseLayout() != null);
        System.assert(!CreateCase.fetchSalutation().isEmpty());
    }
    public static testmethod void createCaseTestMethodzhtw() {
        CreateCase.getUserType();
       
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'AirAsia Profile' limit 1];
        System.runAs(u){
            
			Case c7 = new Case(Case_Language__c = 'zh_TW');        
            CreateCase.saveCase(c7, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            
        }        
        System.assert(!CreateCase.getCountryCode().isEmpty());
        System.assert(CreateCase.getCaseLayout() != null);
        System.assert(!CreateCase.fetchSalutation().isEmpty());
    }
    public static testmethod void createCaseTestMethodko() {
        CreateCase.getUserType();
       
        Country_Code__C c = new Country_Code__C(name='Malaysia',Country_Code__c='MY',Dialing_Code__c='+60');
        insert c;
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'AirAsia Profile' limit 1];
        System.runAs(u){
            
			Case c8 = new Case(Case_Language__c = 'ko');        
            CreateCase.saveCase(c8, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            
        }        
        System.assert(!CreateCase.getCountryCode().isEmpty());
        System.assert(CreateCase.getCaseLayout() != null);
        System.assert(!CreateCase.fetchSalutation().isEmpty());
    }
    public static testMethod void createCaseTestMethodWithCommunityUser() {
        
        Id p = [select id from profile where name='AirAsia Community Login User'].id;
 
        Account ac = new Account(name ='Grazitti',OwnerId= UserInfo.getUserId());
        insert ac; 
        
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con;  

        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user) {
            Case c1= new Case(Type = 'Refund' , Sub_Category_1__c = 'Other', Sub_Category_2__c = 'Voluntary Cancellation', 
                              Sub_Category_3__c = 'Indonesia Domestic', Subject = 'TestCase 001', SuppliedEmail = 'abctest@airasiatest.com');        
            try{
            CreateCase.saveCase(c1, 'Test Comment', '+60 1231231231', '+60 1231231231', 'AK:700,AK:10', 'GName:LName');
            }Catch(Exception e){
            }
        }
    }
}