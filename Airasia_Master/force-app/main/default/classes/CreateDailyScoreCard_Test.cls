/**
 * @File Name          : CreateDailyScoreCard_Test.cls
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 8/8/2019, 4:19:02 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/6/2019, 5:37:07 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
@isTest (SeeAllData=false)
public class CreateDailyScoreCard_Test {
    
    public static testMethod void testScorecardHistory() {
        setupData(true, true);
        //// BUILD cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // INVOKE scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        CreateDailyScorecard scheduledJob = new CreateDailyScorecard();
        System.schedule('Scheduled Scorecard ' + String.valueOf(currentDateTime), nextFireTime, scheduledJob);
        Database.executeBatch(new CreateDailyScorecardBatch(Date.today().addDays(-2)), 50);
        Test.stopTest();
        
        List<User_Scorecard__c> ucList = [SELECT FCR__c, CSAT__c, Meal_Amount__c, Seat_Amount__c, Baggage_Amount__c, Base_Fare_Amount__c FROM User_Scorecard__c LIMIT 1]; 
        User_Scorecard__c uc = ucList[0];
        System.assertEquals(1, uc.FCR__c);
        System.assertEquals(0, uc.CSAT__c);
        System.assertEquals(50, uc.Meal_Amount__c);
        System.assertEquals(0, uc.Seat_Amount__c);
        System.assertEquals(100, uc.Baggage_Amount__c);
        System.assertEquals(200, uc.Base_Fare_Amount__c);
    }
    
    public static testMethod void testScorecard() {
        setupData(true, true);
        //// BUILD cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // INVOKE scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        CreateDailyScorecard scheduledJob = new CreateDailyScorecard();
        System.schedule('Scheduled Scorecard ' + String.valueOf(currentDateTime), nextFireTime, scheduledJob);
        Database.executeBatch(new CreateDailyScorecardBatch(Date.today().addDays(-1)), 50);
        Test.stopTest();
        
        List<User_Scorecard__c> ucList = [SELECT FCR__c, CSAT__c, Meal_Amount__c, Seat_Amount__c, Baggage_Amount__c, Base_Fare_Amount__c FROM User_Scorecard__c LIMIT 1]; 
        User_Scorecard__c uc = ucList[0];
        System.assertEquals(1, uc.FCR__c);
        System.assertEquals(1, uc.CSAT__c);
        System.assertEquals(150, uc.Meal_Amount__c);
        System.assertEquals(100, uc.Seat_Amount__c);
        System.assertEquals(200, uc.Baggage_Amount__c);
        System.assertEquals(300, uc.Base_Fare_Amount__c);
    }

    public static testMethod void testScorecardWOKPI() {
        setupData(false, true);
        //Execute Batch here
        //// BUILD cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // INVOKE scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        CreateDailyScorecard scheduledJob = new CreateDailyScorecard();
        System.schedule('Scheduled Scorecard ' + String.valueOf(currentDateTime), nextFireTime, scheduledJob);
        Database.executeBatch(new CreateDailyScorecardBatch(Date.today().addDays(-1)), 50);
        Test.stopTest();
        
        List<User_Scorecard__c> ucList = [SELECT FCR__c, CSAT__c, Meal_Amount__c, Seat_Amount__c, Baggage_Amount__c, Base_Fare_Amount__c FROM User_Scorecard__c LIMIT 1]; 
        User_Scorecard__c uc = ucList[0];
        System.assertEquals(0, uc.FCR__c);
        System.assertEquals(0, uc.CSAT__c);
        System.assertEquals(150, uc.Meal_Amount__c);
        System.assertEquals(100, uc.Seat_Amount__c);
        System.assertEquals(200, uc.Baggage_Amount__c);
        System.assertEquals(300, uc.Base_Fare_Amount__c);
    }

    public static testMethod void testScorecardWOCHSales() {
        setupData(true, false);
        //Execute Batch here
        //// BUILD cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // INVOKE scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        CreateDailyScorecard scheduledJob = new CreateDailyScorecard();
        System.schedule('Scheduled Scorecard ' + String.valueOf(currentDateTime), nextFireTime, scheduledJob);
        Database.executeBatch(new CreateDailyScorecardBatch(Date.today().addDays(-1)), 50);
        Test.stopTest();
        
        List<User_Scorecard__c> ucList = [SELECT FCR__c, CSAT__c, Meal_Amount__c, Seat_Amount__c, Baggage_Amount__c, Base_Fare_Amount__c FROM User_Scorecard__c LIMIT 1]; 
        User_Scorecard__c uc = ucList[0];
        System.assertEquals(1, uc.FCR__c);
        System.assertEquals(1, uc.CSAT__c);
        System.assertEquals(0, uc.Meal_Amount__c);
        System.assertEquals(0, uc.Seat_Amount__c);
        System.assertEquals(0, uc.Baggage_Amount__c);
        System.assertEquals(0, uc.Base_Fare_Amount__c);
    }
    
    static void setupData(Boolean createKPI, Boolean createCHSales){
        
        Profile profileObj = [SELECT ID FROM Profile Where Name='Live Chat Officer'];
        
        User user = new User();
        user.FirstName = 'User FN';
        user.LastName = 'User MN';
        user.MobilePhone = '12321321';
        user.Username = 'testUserSD@gmail.com';
        user.Email = 'testUserSD@gmail.com';
        user.Alias = 'testSD';        
        user.TimeZoneSidKey = 'Asia/Kuala_Lumpur';
        user.LocaleSidKey = 'en_MY';
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profileObj.Id;
        user.CurrencyIsoCode = 'MYR';
        user.LanguageLocaleKey = 'en_US';
        user.Employee_Id__c = 'SFTESTUSER0001';
        insert user;
        
        user = [SELECT Id, Employee_Id__c FROM User WHERE Employee_Id__c='SFTESTUSER0001'];

        //Create User KPI
        List<User_KPI__c> kpiList = new List<User_KPI__c>();
        User_KPI__c ukpi = new User_KPI__c();
        ukpi.Channel__c = 'Web';
        ukpi.User__c = user.Id;
        ukpi.Score__c = 1;
        ukpi.Survey_Type__c = 'CSAT';
        ukpi.Response_DateTime__c = Datetime.now().addDays(-1);
        kpiList.add(ukpi);

        User_KPI__c ukpi2 = new User_KPI__c();
        ukpi2.Channel__c = 'Web';
        ukpi2.User__c = user.Id;
        ukpi2.Score__c = 1;
        ukpi2.Survey_Type__c = 'FCR';
        ukpi2.Response_DateTime__c = Datetime.now().addDays(-1);
        kpiList.add(ukpi2);

        //Should not be included
        User_KPI__c ukpi3 = new User_KPI__c();
        ukpi3.Channel__c = 'Web';
        ukpi3.User__c = user.Id;
        ukpi3.Score__c = 1;
        ukpi3.Survey_Type__c = 'FCR';
        ukpi3.Response_DateTime__c = Datetime.now().addDays(-2);
        kpiList.add(ukpi3);
		
        if(createKPI) {
            insert kpiList;
        }
        
        //Create CH Sales
        List<CH_Sales__c> chList = new List<CH_Sales__c>();
        CH_Sales__c chSales = new CH_Sales__c();
        chSales.UserID__c = user.Id;
        chSales.Meal_Amount__c = 100;
        chSales.Seat_Amount__c = 100;
        chSales.Baggage_Amount__c = 100;
        chSales.Base_Fare_Amount__c = 100;
        chSales.Employee_Id__c = user.Employee_Id__c;
        chSales.Sales_Date__c = Date.today().addDays(-1);
        chList.add(chSales);

        CH_Sales__c chSales2 = new CH_Sales__c();
        chSales2.UserID__c = user.Id;
        chSales2.Meal_Amount__c = 50;
        chSales2.Seat_Amount__c = 0;
        chSales2.Baggage_Amount__c = 100;
        chSales2.Base_Fare_Amount__c = 200;
        chSales2.Employee_Id__c = user.Employee_Id__c;
        chSales2.Sales_Date__c = Date.today().addDays(-1);
        chList.add(chSales2);
        
        //Should not be included
        CH_Sales__c chSales3 = new CH_Sales__c();
        chSales3.UserID__c = user.Id;
        chSales3.Meal_Amount__c = 50;
        chSales3.Seat_Amount__c = 0;
        chSales3.Baggage_Amount__c = 100;
        chSales3.Base_Fare_Amount__c = 200;
        chSales3.Employee_Id__c = user.Employee_Id__c;
        chSales3.Sales_Date__c = Date.today().addDays(-2);
        chList.add(chSales3);
        
        if(createCHSales) {
            insert chList;
        }
    }
}