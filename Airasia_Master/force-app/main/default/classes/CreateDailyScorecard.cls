public class CreateDailyScorecard implements Schedulable {    
    public static void execute(SchedulableContext ctx) {
   		Database.executeBatch(new CreateDailyScorecardBatch(Date.today().addDays(-1)), 50);
    }
}