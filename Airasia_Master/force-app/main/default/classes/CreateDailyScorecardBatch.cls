/**
 * @File Name          : CreateDailyScorecard.cls
 * @Description        : This will be used to create a daily scorecard per user and be executed at 12am via Scheduled Apex
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 8/8/2019, 4:10:39 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/6/2019, 4:53:53 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
public class CreateDailyScorecardBatch implements 
                        Database.Batchable<sObject> {
	
	public Date scorecardDate;
    public CreateDailyScorecardBatch(Date scorecardDate) {
        this.scorecardDate = scorecardDate;
    }
                            
    public Database.QueryLocator start(Database.BatchableContext bc) {        
        if(Test.isRunningTest()) {
            return Database.getQueryLocator('SELECT Id, Employee_Id__c FROM User WHERE IsActive=True AND Employee_Id__c = \'SFTESTUSER0001\'');
        }
        else {
			return Database.getQueryLocator('SELECT Id, Employee_Id__c FROM User WHERE IsActive=True AND Employee_Id__c != \'\'');
        }
    }

    public void execute(Database.BatchableContext bc, List<User> userList) {
        Set<Id> userIds = new Set<Id>();
        for(User u : userList) {
			userIds.add(u.Id);
        }

        Map<Id, Object> fcrMap = new Map<Id, Object>();
        Map<Id, Object> fcrTotalMap = new Map<Id, Object>();
        Map<Id, Object> caseMap = new Map<Id, Object>();
        Map<Id, Object> csatMap = new Map<Id, Object>();
        Map<Id, Object> csatTotalMap = new Map<Id, Object>();
        Map<Id, Object> mealMap = new Map<Id, Object>();
        Map<Id, Object> seatMap = new Map<Id, Object>();
        Map<Id, Object> baggageMap = new Map<Id, Object>();
        Map<Id, Object> baseFareMap = new Map<Id, Object>();

        for (AggregateResult ar : [SELECT User__c, 
                                                   SUM(Score__c) score,
                                   				   COUNT(Id) num
                                            FROM User_KPI__c 
                                            WHERE User__c =: userIds
                                                AND Survey_Type__c = 'FCR'
                                                AND DAY_ONLY(Response_DateTime__c) =: scorecardDate
                                            GROUP BY User__c]) {
            fcrMap.put((Id)ar.get('User__c'), ar.get('score'));
            fcrTotalMap.put((Id)ar.get('User__c'), ar.get('num'));
			System.debug('FCR => ' + ar.get('User__c') + ' | ' + ar.get('score'));
        }
		
        for (AggregateResult ar : [SELECT OwnerId, 
                                          COUNT(Id) num
                                            FROM Case 
                                            WHERE OwnerId =: userIds
                                   				AND ((IsClosed = false AND DAY_ONLY(CreatedDate) >=: scorecardDate)
                                                     OR ClosedDate =: scorecardDate)
                                            GROUP BY OwnerId]) {
            caseMap.put((Id)ar.get('OwnerId'), ar.get('num'));
			System.debug('Cases Owned => ' + ar.get('OwnerId') + ' | ' + ar.get('num'));
        }
        
        for (AggregateResult ar : [SELECT User__c, 
                                                   SUM(Score__c) score,
                                   				   COUNT(Id) num
                                            FROM User_KPI__c 
                                            WHERE User__c =: userIds
                                                AND Survey_Type__c = 'CSAT'
                                                AND DAY_ONLY(Response_DateTime__c) =: scorecardDate
                                            GROUP BY User__c]) {
            csatMap.put((Id)ar.get('User__c'), ar.get('score'));
            csatTotalMap.put((Id)ar.get('User__c'), ar.get('num'));
			System.debug('CSAT => ' + ar.get('User__c') + ' | ' + ar.get('score'));

        }

        for (AggregateResult ar : [SELECT UserID__c, 
                                                   SUM(Meal_Amount__c) meal,
                                                   SUM(Seat_Amount__c) seat,
                                                   SUM(Baggage_Amount__c) baggage,
                                                   SUM(Base_Fare_Amount__c) basefare
                                            FROM CH_Sales__c 
                                            WHERE UserID__c =: userIds 
                                                AND Sales_Date__c =: scorecardDate
                                            GROUP BY UserID__c]) {
            mealMap.put((Id)ar.get('UserID__c'), ar.get('meal'));
            seatMap.put((Id)ar.get('UserID__c'), ar.get('seat'));
            baggageMap.put((Id)ar.get('UserID__c'), ar.get('baggage'));
            baseFareMap.put((Id)ar.get('UserID__c'), ar.get('basefare'));
        }

        List<User_Scorecard__c> uscList = new List<User_Scorecard__c>();
        for (User u : userList) {
            User_Scorecard__c usc = new User_Scorecard__c();
            usc.User__c = u.Id;
            usc.Employee_Id__c = u.Employee_Id__c;
			usc.Scorecard_Date__c = scorecardDate;
            
            //FCR
            if (fcrMap.get(u.Id) != null) {
                usc.FCR__c = (Decimal)fcrMap.get(u.Id);
                usc.FCR_Total__c = (Decimal)fcrTotalMap.get(u.Id);
            }
            else {
                usc.FCR__c = 0;
                usc.FCR_Total__c = 0;
            }
			System.debug('FCR => ' + fcrMap.get(u.Id));
            
            //CSAT
            if (csatMap.get(u.Id) != null) {
                usc.CSAT__c = (Decimal)csatMap.get(u.Id);
                usc.CSAT_Total__c = (Decimal)csatTotalMap.get(u.Id);
            }
            else {
                usc.CSAT__c = 0;
                usc.CSAT_Total__c = 0;
            }
			System.debug('CSAT => ' + csatMap.get(u.Id));
            
            //Case Count
            if (caseMap.get(u.Id) != null) {
                usc.Cases_Owned__c = (Decimal)caseMap.get(u.Id);
            }
            else {
                usc.Cases_Owned__c = 0;
            }
			System.debug('CSAT => ' + csatMap.get(u.Id));
            
            //Meal Amount
            if (mealMap.get(u.Id) != null) {
                usc.Meal_Amount__c = (Decimal)mealMap.get(u.Id);
            }
            else {
                usc.Meal_Amount__c = 0;
            }
            
            //Seat Amount
            if (seatMap.get(u.Id) != null) {
                usc.Seat_Amount__c = (Decimal)seatMap.get(u.Id);
            }
            else {
                usc.Seat_Amount__c = 0;
            }
            
            //Baggage Amount
            if (baggageMap.get(u.Id) != null) {
                usc.Baggage_Amount__c = (Decimal)baggageMap.get(u.Id);
            }
            else {
                usc.Baggage_Amount__c = 0;
            }
            
            //Base Fare Amount
            if (baseFareMap.get(u.Id) != null) {
                usc.Base_Fare_Amount__c = (Decimal)baseFareMap.get(u.Id);
            }
            else {
                usc.Base_Fare_Amount__c = 0;
            }
            
            uscList.add(usc);
        }

        if (!uscList.isEmpty()) {
            Database.insert(uscList, false);
        }
    }
                            
    public void finish(Database.BatchableContext bc) {
        
    }

    /* HELPER Methods */
    
}