public without sharing class CreditAccountRequestController {
    
    
    @AuraEnabled
    public static Case saveCase(Case newCase) {
        // Perform isUpdateable() checking first, then
        insert newCase;

        return [SELECT Id, CaseNumber FROM Case WHERE Id =:newCase.Id];
    }


}