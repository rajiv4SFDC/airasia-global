@isTest
public with sharing class CreditAccountRequestController_isTest {
    public CreditAccountRequestController_isTest() {

    }

    @TestSetup
    static void makeData(){
        
    //     //Create Case
    //     default="{'sobjecttype':'Case',
    // 'Subject':'AAI Credit Request','Manual_Contact_First_Name__c':'',
    // 'Manual_Contact_Last_Name__c':'','Manual_Contact_Email__c':'',
    // 'SuppliedEmail':'','Booking_Number__c':'','Airline_Code__c':'I5',
    // 'Flight_Number__c':'','RBV_R1_BIG_Member_ID_Number__c':'',
    // 'SuppliedPhone':'','Departure_Date_and_Time__c':'','Type':'Enquiry/Request',
    // 'Sub_Category_1__c':'Credit Account Request', 'Origin':'Web',
    // 'Risk__c':'Minor','Case_Language__c':'English','Priority':'Low'}"/>

        
    }

    public static testMethod void testSaveCase(){

        Case caseRec = new Case();
        caseRec.subject = 'AAI CreditRequest';
        caseRec.Booking_Number__c = 'ABCDEF';
        caseRec.Airline_Code__c = 'I5';
        caseRec.Flight_Number__c = '12';
        caseRec.Type = 'Enquiry/Request';
        caseRec.Sub_Category_1__c = 'Credit Account Request';
        caseRec.Origin = 'Web';

        Test.startTest();

            CreditAccountRequestController.saveCase(caseRec);

        Test.stopTest();

        System.assertNotEquals(null, caseRec.Id);

    }
}