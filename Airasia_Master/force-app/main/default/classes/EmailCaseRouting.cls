/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/06/2017      Pawan Kumar         Created
*******************************************************************/
public without sharing class EmailCaseRouting {
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    private static String processLog = '';
    private static List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
    private static final String DELIMITER = '#~#';
    
    @InvocableMethod
    public static void assignCaseToRelevantQueue(List < Id > caseIds) {
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Routing_Log_Level_CMD_NAME'));
        
        try {
            List < Case > updateCaseList;
            List < Case > caseList = [Select Id, Origin from Case where Id IN: caseIds];
            
            if (!caseList.isEmpty()) {
                
                updateCaseList = new List < Case > ();
                
                Map < String, String > emailCaseRoutingMap = new Map < String, String > ();
                List < Email_Case_Routing__mdt > emailCaseRoutingList = [Select Id, Case_Origin__c, Queue__c from Email_Case_Routing__mdt];
                
                for (Email_Case_Routing__mdt eachRow: emailCaseRoutingList) {
                    emailCaseRoutingMap.put(eachRow.Case_Origin__c, eachRow.Queue__c);
                }
                //System.debug('emailCaseRoutingMap:'+JSON.serializePretty(emailCaseRoutingMap));
                
                for (Case caseToBeAssignedToQueue: caseList) {
                    String caseId = caseToBeAssignedToQueue.id;
                    
                    try {
                        if (emailCaseRoutingMap.containsKey(caseToBeAssignedToQueue.Origin)) {
                            caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(emailCaseRoutingMap.get(caseToBeAssignedToQueue.Origin));
                        } else {
                            String soqlQuery = 'Select Id, Queue__c from Email_Case_Routing__mdt' +
                                ' where Case_Origin__c =' + caseToBeAssignedToQueue.Origin + '\r\n';
                            throw new QueryException(soqlQuery);
                        }
                        
                    } catch (Exception queueQueryEx) {
                        System.debug(LoggingLevel.ERROR, queueQueryEx);
                        
                        caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(Utilities.getAppSettingsMdtValueByKey('Case_Routing_Error_Queue'));
                        processLog += 'Failed to get QUEUE NAME from routing table: \r\n';
                        processLog += queueQueryEx.getMessage() + '\r\n';
                        
                        appWrapperLogList.add(Utilities.createApplicationLog('Warning', 'EmailCaseRouting', 'assignCaseToRelevantQueue',
                                                                             caseId, 'Email Case Routing', processLog, null, 'Error Log', startTime, logLevelCMD, queueQueryEx));
                    }
                    
                    updateCaseList.add(caseToBeAssignedToQueue);
                }
                
                // update Case
                if (updateCaseList != null && !updateCaseList.isEmpty()) {
                    update updateCaseList;
                }
            }
        } catch (Exception caseAssignFailedEx) {
            System.debug(LoggingLevel.ERROR, caseAssignFailedEx);
            
            String firstCaseId = '';
            if (caseIds != null && !caseIds.isEmpty()) {
                firstCaseId = caseIds[0];
            }
            
            processLog += 'Failed to assign queue for the following Case Ids: \r\n';
            processLog += JSON.serializePretty(caseIds) + '\r\n';
            processLog += 'Root Cause: ' + caseAssignFailedEx.getMessage() + '\r\n';
            
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'EmailCaseRouting', 'assignCaseToRelevantQueue',
                                                                 firstCaseId, 'Email Case Routing', processLog, null, 'Error Log', startTime, logLevelCMD, caseAssignFailedEx));
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
        }
    }
}