global class EmailPublisherLoader implements QuickAction.QuickActionDefaultsHandler {

    //private static final String DEFAULT_EMAIL_TEMPLATE = 'ARR Default Template';
    private String DEFAULT_EMAIL_ADDRESS = 'airasiasupport@airasia.com';
    private static Set<String> caseOriginExclusionSet = new Set<String>();
    private static Map<String, String> caseOriginMap = new Map<String, String>();

    static {
      List<Application_Setting__mdt> appSettingList = [SELECT Key__c, Value__c FROM Application_Setting__mdt WHERE DeveloperName LIKE 'SUPPORT_EMAIL_CASE_ORIGIN_%'];
      if(appSettingList != null && !appSettingList.isEmpty()) {
        for(Application_Setting__mdt appSet : appSettingList) {
          if(appSet.Key__c.equals('SUPPORT_EMAIL_CASE_ORIGIN_EXCLUSION_LIST')) {
              caseOriginExclusionSet = new Set<String>(appSet.Value__c.split(','));
          } else {
              caseOriginMap.put(appSet.Key__c, appSet.Value__c);
          }
        }
      }
    }

    // Empty constructor
    global EmailPublisherLoader() {
    }

    // The main interface method
    global void onInitDefaults(QuickAction.QuickActionDefaults[] defaults) {

        QuickAction.SendEmailQuickActionDefaults quickActionDefaults = null;
        EmailMessage emailMessageObj = null;
        List<String> fromAddressList = null;

        try {
            if(defaults != null) {

                for (Integer j = 0; j < defaults.size(); j++) {

                    quickActionDefaults = (QuickAction.SendEmailQuickActionDefaults) defaults.get(j);
                    String caseId = quickActionDefaults.getContextId();
                    Case caseObj = getCase(caseId);
                    // Get From Address List
                    fromAddressList = quickActionDefaults.getFromAddressList();

                    fromAddressList.clear();

                    // Only Add Email Message From Address into Email From Address List
                    if(quickActionDefaults.getTargetSObject().getSObjectType() == EmailMessage.sObjectType) {
                        emailMessageObj = (EmailMessage) quickActionDefaults.getTargetSObject();
                        if(caseOriginExclusionSet.contains(caseObj.Origin) && caseOriginMap.containsKey(caseObj.Origin)){
                            fromAddressList.add(caseOriginMap.get(caseObj.Origin));
                        } else {
                            fromAddressList.add(DEFAULT_EMAIL_ADDRESS);
                        }
                        // Remove BCC Content
                        emailMessageObj.BccAddress = '';
                        emailMessageObj.Subject = caseObj.CaseNumber + ' - ' + emailMessageObj.Subject;
                    }
                }
            }
        } catch(Exception exp) {
            System.debug('Exception in Email Publisher Loader - ' + exp);
        }
    }

    private Case getCase(String caseId) {
        Case caseObj = null;
        List<Case> caseList = [SELECT Id, Origin, CaseNumber FROM Case WHERE Id=:caseId LIMIT 1];
        if(caseList != null && !caseList.isEmpty()) {
            caseObj = caseList[0];
        }
        return caseObj;
    }
}