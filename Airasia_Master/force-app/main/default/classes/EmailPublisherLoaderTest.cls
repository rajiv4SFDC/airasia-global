@isTest
public class EmailPublisherLoaderTest {
    
    // Method to create test cases gTechPartnersConstants Class 
    Static testMethod void testEmailPublisherLoader() { 
        
        // Insert Account 
        Account account = new Account(); 
        account.name = 'test account'; 
        account.Organization_Code__c = 'Test';
        insert account; 
        
        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = account.id; 
        contact.Email = 'Test@test.com'; 
        contact.LastName = 'TestLastName'; 
        insert contact; 
        
        Case case1 = new Case(); 
        case1.AccountId = account.id; 
        case1.Subject = 'test case'; 
        case1.reason = 'test@test.com'; 
        case1.Origin = 'Phone';
        insert case1; 
        
        System.Debug('-->' + case1.Id);
        // Test Case for the Constant Statis Constant Variables 
        EmailPublisherLoader emailPublisher = new EmailPublisherLoader(); 
        
        QuickAction.SendEmailQuickActionDefaults sdf ; 
        QuickAction.QuickActionDefaultsHandler qdf ; 
        QuickAction.QuickActionDefaults[] defaults = new QuickAction.QuickActionDefaults[]{}; 
            
            for (QuickAction.QuickActionDefaults dfv :defaults) { 
                dfv = sdf ; 
            } 
        emailPublisher.onInitDefaults(defaults); 
    } 
    
    static testmethod void testEmailPublisherLoaderwithJson()
    {
        //Create test data here
        Exception failureDuringExecution = null;

// Insert Account 
        Account account = new Account(); 
        account.name = 'test account'; 
        account.Organization_Code__c = 'Test';
        insert account; 
        
        // Insert Contact 
        Contact contact = new Contact(); 
        contact.AccountId = account.id; 
        contact.Email = 'Test@test.com'; 
        contact.LastName = 'TestLastName'; 
        insert contact; 
        
        Case case1 = new Case(); 
        case1.AccountId = account.id; 
        case1.Subject = 'test case'; 
        case1.reason = 'test@test.com'; 
        case1.Origin = 'Phone';
        insert case1;
        
        String defaultsAsJSON = '[{"targetSObject":{"attributes":{"type":"EmailMessage"},"TextBody":"",'
            + '"FromName":"Test","FromAddress":"test@example.com","HtmlBody":"<html><body></body></html>","BccAddress":"test@example.com",'
            + '"CcAddress":"","ToAddress":"test@example.com","Subject":"Testing"},"contextId":"'+case1.Id+'","actionType":"Email",'
            + '"actionName":"Case.Email","fromAddressList":["salesforce@test.com"]}]';
        List<QuickAction.SendEmailQuickActionDefaults> defaultsSettings = 
            (List<QuickAction.SendEmailQuickActionDefaults>)JSON.deserialize(defaultsAsJSON, List<QuickAction.SendEmailQuickActionDefaults>.class);
        
        Test.startTest();
        try { (new EmailPublisherLoader()).onInitDefaults(defaultsSettings); }
        catch(Exception failure) { failureDuringExecution = failure; }
        
        Test.stopTest();
        
        System.assertEquals(null, failureDuringExecution, 'There was an exception thrown during the test!');
        //Make other assertions here
    }
}