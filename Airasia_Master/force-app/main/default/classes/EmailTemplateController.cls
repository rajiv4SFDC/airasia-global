public without sharing class EmailTemplateController {
    
    public String labelValue {set;get;}
    public String labelData {set;get;}
    public static String communityURL{set;get;}
    
    static {
        try {
            Network AirAsiaNetwork = [SELECT Id FROM Network WHERE Name ='AirAsia' ];
            ConnectApi.Community  AirAsiaCommunity = ConnectApi.Communities.getCommunity(AirAsiaNetwork.id);
            communityURL = AirAsiaCommunity.siteUrl + '/s';
        }catch(Exception exp) {
        System.debug('Excepton:'+exp);
        }
    }
    
    public String getFormattedLabel() {
        String data = '';
        if(String.isNotBlank(this.labelValue)) {
            
            Component.Apex.OutputText output = new Component.Apex.OutputText();
            output.expressions.value = '{!$Label.' + this.labelValue + '}';
            data = String.valueOf(output.value);
            if(String.isNotBlank(this.labelData)) {
                data = String.format(data, this.labelData.split(','));
                if(String.isNotBlank(communityURL)) {
                    data = data.replace('#COMMUNITY_URL', communityURL);    
                }
            }
        }
        return data;
    }
    
    public String getTodaysValue() {
        DateTime d = Date.Today() ;
        String todaysValue = d.format('dd MMMM yyyy');
        return todaysValue;
    }    
}