@isTest
public class EmailTemplateControllerTest {

    @isTest
    private static void testEmailTemplate() {
        
        Test.startTest();
        
        EmailTemplateController controller = new EmailTemplateController();
        controller.labelValue = 'ET_Case_Survey';
        controller.labelData = 'testValue1, testValue2';
        
        String data = controller.getFormattedLabel();
        System.debug(data.contains('testValue1'));
        System.debug(data.contains('testValue2'));
        System.assert(controller.getTodaysValue() != null);
        
        Test.stopTest();
    }
}