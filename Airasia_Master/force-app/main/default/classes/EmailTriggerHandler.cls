/**
 * @File Name          : EmailTriggerHandler.cls
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 9/19/2019, 11:20:18 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    Sept/19/2019   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
 * 1.1    Sept/30/2019	 Mark Alvin Tayag (mtayag@salesforce.com)	  Update to Handle New and Pending Guest Replies Only.
 * 																	  Handle Empty ToAddress
**/
public class EmailTriggerHandler {
    
    // Look for incoming emails to an email address in the Language Based Routing Metadata
    public static void detectEmailLanguage(List<EmailMessage> emailList) {
        Set<Id> emailIds = new Set<Id>();
        List<Email_Language_Based_Routing__mdt> elbList = getRoutingMDT();
        Set<String> supportedEmailAddresses = new Set<String>();
        
        for (Email_Language_Based_Routing__mdt elb : elbList) {
            supportedEmailAddresses.add(elb.Email_Address__c);
        }
        
        Map<String, String> caseEmailIds = new Map<String, String>();
        for (EmailMessage email : emailList) {
            if (email.Incoming == true && 
                email.ParentId != null &&
                String.isNotBlank(email.ToAddress)) {
                //Check for multiple receipients
                for (String toAddress : email.toAddress.split(';')) {
                    if (supportedEmailAddresses.contains(toAddress)) {
                        caseEmailIds.put(email.ParentId, email.Id);
                        break;
                    }
                }
            }
        }
        
        //25 Sept 2019 - Prevent Closed Cases from performing language detection
        //26 Sept 2019 - Limit to New and Pending Guest Reply
        for (Case caseId : [SELECT Id 
                            FROM Case
                            WHERE IsClosed = false
                            AND (Status = 'New' OR Status = 'Pending Guest Reply')
                            AND Id =: caseEmailIds.keySet()]) {
            emailIds.add(caseEmailIds.get(caseId.Id));
       }
        
        if (!(emailIds.isEmpty())) {
            LanguageDetection.detectEmailLanguage(emailIds);
        }
        
        
    }
    
    /* HELPER METHODS */
    public static List<Email_Language_Based_Routing__mdt> getRoutingMDT() {
        List<Email_Language_Based_Routing__mdt> elbList = [SELECT Email_Language__c, 
                                                           			Queue__c, 
                                                           			Email_Address__c 
                                                           FROM Email_Language_Based_Routing__mdt];       
        
        return elbList;
    }
}