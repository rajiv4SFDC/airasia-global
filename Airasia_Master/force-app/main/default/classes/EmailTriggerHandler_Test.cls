@isTest
public class EmailTriggerHandler_Test {
        
    @testSetup
    public static void testSetup() {
        
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Manual_Contact_Email__c', 'test@gmail.com');
        caseFieldValueMap.put('SuppliedEmail', 'test@gmail.com');      
        caseFieldValueMap.put('Manual_Contact_Last_Name__c', 'TEST'); 
        caseFieldValueMap.put('Manual_Contact_First_Name__c', 'TEST'); 
        caseFieldValueMap.put('SuppliedName', 'TEST'); 
        caseFieldValueMap.put('SuppliedPhone', '9999999999'); 
        caseFieldValueMap.put('Supplied_Title__c', 'Mr'); 
        TestUtility.createCase(caseFieldValueMap);
    }
    
    public static testMethod void testLanguageDetection() {
        Test.setMock(HttpCalloutMock.class, new LanguageDetectionMock('en'));

        Case c = [SELECT Id FROM Case LIMIT 1];
        List<Email_Language_Based_Routing__mdt> elbList = EmailTriggerHandler.getRoutingMDT();
        EmailMessage email = new EmailMessage();
        email.ToAddress = elbList[0].Email_Address__c;
        email.Subject = 'Subject';
        email.TextBody = 'Test Email English';
        email.Incoming = true;
        email.ParentId = c.Id;
        
        Test.startTest();
        insert email;
        Test.stopTest();
        
        email = [SELECT Language__c FROM EmailMessage LIMIT 1];
        System.assertEquals('English', email.Language__c);
    }
    
    public static testMethod void testJapaneseLanguageDetection() {
        Test.setMock(HttpCalloutMock.class, new LanguageDetectionMock('ja'));
        
        Case c = [SELECT Id FROM Case LIMIT 1];
        List<Email_Language_Based_Routing__mdt> elbList = EmailTriggerHandler.getRoutingMDT();
        
        EmailMessage email = new EmailMessage();
        email.ToAddress = elbList[0].Email_Address__c;
        email.Subject = 'Subject - Japanese';
        email.TextBody = 'テストメール英語';
        email.Incoming = true;
        email.ParentId = c.Id;
        
        Test.startTest();
        insert email;
        Test.stopTest();
        
        email = [SELECT Language__c FROM EmailMessage LIMIT 1];
        System.assertEquals('Japanese', email.Language__c);
    }
    
    public static testMethod void testUnsupportedLanguageDetection() {
        Test.setMock(HttpCalloutMock.class, new LanguageDetectionMock('iw'));
        
        Case c = [SELECT Id FROM Case LIMIT 1];
        List<Email_Language_Based_Routing__mdt> elbList = EmailTriggerHandler.getRoutingMDT();
        
        EmailMessage email = new EmailMessage();
        email.ToAddress = elbList[0].Email_Address__c;
        email.Subject = 'Subject - Hebrew';
        email.TextBody = 'בדוק דוא"ל באנגלית';
        email.Incoming = true;

        email.ParentId = c.Id;
        Test.startTest();
        insert email;
        Test.stopTest();
        
        email = [SELECT Language__c FROM EmailMessage LIMIT 1];
        System.assertEquals('English', email.Language__c);
    }
}