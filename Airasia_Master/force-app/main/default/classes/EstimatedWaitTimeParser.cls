public class EstimatedWaitTimeParser {

	public class Messages {
		public String type_Z {get;set;} // in json: type
		public Message message {get;set;} 

		public Messages(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'type') {
							type_Z = parser.getText();
						} else if (text == 'message') {
							message = new Message(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Messages consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public List<Messages> messages {get;set;} 

    //Constructor
	public EstimatedWaitTimeParser(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'messages') {
						messages = arrayOfMessages(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
    
    private static List<Messages> arrayOfMessages(System.JSONParser p) {
        List<Messages> res = new List<Messages>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Messages(p));
        }
        return res;
    }
    
    
	public class Message {
		public String prefixKey {get;set;} 
		public String contentServerUrl {get;set;} 
		public Double pingRate {get;set;} 
		public List<Buttons> buttons {get;set;} 

		public Message(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'prefixKey') {
							prefixKey = parser.getText();
						} else if (text == 'contentServerUrl') {
							contentServerUrl = parser.getText();
						} else if (text == 'pingRate') {
							pingRate = parser.getDoubleValue();
						} else if (text == 'buttons') {
							buttons = arrayOfButtons(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Message consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
    
    private static List<Buttons> arrayOfButtons(System.JSONParser p) {
        List<Buttons> res = new List<Buttons>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Buttons(p));
        }
        return res;
    }
    
    
	public class Buttons {
		public Integer estimatedWaitTime {get;set;} 
		public String language {get;set;} 
		public String type_Z {get;set;} // in json: type
		public String id {get;set;} 
		public Boolean isAvailable {get;set;} 

		public Buttons(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'estimatedWaitTime') {
							estimatedWaitTime = parser.getIntegerValue();
						} else if (text == 'language') {
							language = parser.getText();
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else if (text == 'id') {
							id = parser.getText();
						} else if (text == 'isAvailable') {
							isAvailable = parser.getBooleanValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Buttons consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
		
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
}