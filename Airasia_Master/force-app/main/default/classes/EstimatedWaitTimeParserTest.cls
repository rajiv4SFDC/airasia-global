@isTest
public class EstimatedWaitTimeParserTest {
    
    @isTest
    public static void testParse() {
        String json = '{'+
        '   \"messages\":['+
        '      {'+
        '         \"type\":\"Settings\",'+
        '         \"message\":{'+
        '            \"prefixKey\":\"5a9b4cfd00f1e628a238db2a197f2ba0e6d9e380\",'+
        '            \"contentServerUrl\":\"https://278e.la1-c1cs-hnd.salesforceliveagent.com/content\",'+
        '            \"pingRate\":50000.0,'+
        '            \"buttons\":['+
        '               {'+
        '                  \"estimatedWaitTime\":114,'+
        '                  \"language\":\"en_US\",'+
        '                  \"type\":\"Standard\",'+
        '                  \"id\":\"5739D00000000Hv\",'+
        '                  \"isAvailable\":true'+
        '               }'+
        '            ]'+
        '         }'+
        '      }'+
        '   ]'+
        '}';
        System.JSONParser parser = System.JSON.createParser(json);
        EstimatedWaitTimeParser r = new EstimatedWaitTimeParser(parser);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        EstimatedWaitTimeParser.Messages objMessages = new EstimatedWaitTimeParser.Messages(System.JSON.createParser(json));
        System.assert(objMessages != null);
        System.assert(objMessages.type_Z == null);
        System.assert(objMessages.message == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        EstimatedWaitTimeParser objJSON2Apex = new EstimatedWaitTimeParser(System.JSON.createParser(json));
        System.assert(objJSON2Apex != null);
        System.assert(objJSON2Apex.messages == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        EstimatedWaitTimeParser.Message objMessage = new EstimatedWaitTimeParser.Message(System.JSON.createParser(json));
        System.assert(objMessage != null);
        System.assert(objMessage.prefixKey == null);
        System.assert(objMessage.contentServerUrl == null);
        System.assert(objMessage.pingRate == null);
        System.assert(objMessage.buttons == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        EstimatedWaitTimeParser.Buttons objButtons = new EstimatedWaitTimeParser.Buttons(System.JSON.createParser(json));
        System.assert(objButtons != null);
        System.assert(objButtons.estimatedWaitTime == null);
        System.assert(objButtons.language == null);
        System.assert(objButtons.type_Z == null);
        System.assert(objButtons.id == null);
        System.assert(objButtons.isAvailable == null);
    }
}