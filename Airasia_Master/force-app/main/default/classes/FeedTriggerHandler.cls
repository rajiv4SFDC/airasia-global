public class FeedTriggerHandler extends TriggerHandler {
    
    private static final String FEED_ITEM_TYPE = 'TextPost';
    private static final String COMMUNITY_PROFILE = 'AirAsia Community Login User';
    public Boolean isFeedItem{set;get;}
    private static final FeedTriggerHandler handler = new FeedTriggerHandler();
    
    public override void afterInsert() {
        Map<String, Set<String>> userIdMap = new map<String, Set<String>>();
        
        for(SObject objectData : DATA_LIST_NEW) {
            String createdById = String.valueOf(objectData.get('CreatedById'));
            String caseId = String.valueOf(objectData.get('ParentId'));
            
            if((isFeedItem && FEED_ITEM_TYPE.equals(String.valueOf(objectData.get('Type')))) || !isFeedItem) {
                if(userIdMap.containsKey(createdById)) {
                    userIdMap.get(createdById).add(caseId);
                } else {
                    userIdMap.put(createdById, new Set<String>{caseId});    
                }
            }
        }
        
        if(!userIdMap.isEmpty()) {
            List<User> userList = [SELECT Id FROM User WHERE Profile.Name = :COMMUNITY_PROFILE AND Id IN :userIdMap.keySet()];
            if(userList != null && !userList.isEmpty()) {
                List<Case> caseList = new List<Case>();
                for(User userObj : userList) {
                    for(String caseId : userIdMap.get(userObj.Id)) {   
                        Case caseObj = new Case();
                        caseObj.Id = caseId;
                        caseObj.Start_Subsequent_Milestone__c = true;
                        caseObj.New_Customer_Post_or_Comment__c = true;
                        caseList.add(caseObj);
                    }
                }
                
                if(!caseList.isEmpty()) {
                    Database.upsert(caseList);
                }
            }
        }
    }
    
    public override void beforeInsert() {
        Map<String, Set<SObject>> caseMap = new Map<String, Set<SObject>>();
        for(SObject objectData : DATA_LIST_NEW) {
            String caseId = String.valueOf(objectData.get('ParentId'));
            if(caseMap.containsKey(caseId)) {
                caseMap.get(caseId).add(objectData);
            } else {
                caseMap.put(caseId, new Set<SObject>{objectData});
            }
        }
        if(!caseMap.isEmpty()) {
            List<Case> caseList = [SELECT Id, Status FROM Case WHERE Id IN : caseMap.keySet() AND Status = 'Closed'];
            
            if(caseList != null && !caseList.isEmpty()) {
                for(Case caseObject : caseList) {
                    for(SObject objData : caseMap.get(caseObject.Id)) {
                    	objData.addError('You can\'t add post or comment on closed case.');    
                    }
                }
            }
        }
    }
}