@isTest
public class FeedTriggerTest {

    @testSetup
    private static void setup() {

		UserRole userRoleOfficer = createUserRole('Officer2', null);
        User u = createUserwithRole('System Administrator', 'Officer2', userRoleOfficer.Id);

        System.runAs(u) {
            Account accountNewInsert = new Account(name ='Test Account',Organization_Code__c ='test') ;
            insert accountNewInsert;
            
            Contact con = new Contact(LastName ='testCon',ownerId=userinfo.getuserid(),AccountId = accountNewInsert.Id,email='test1211@tgd.com');       
            insert con; 
            
            createUser('AirAsia Community Login User', 'Officer', con.Id);
        }
    }
    
    private static UserRole createUserRole(String name, String parentRoleId) {
        UserRole userRoleObject = new UserRole();
        userRoleObject.DeveloperName = name.replace(' ', '_');
        userRoleObject.Name = name;
        userRoleObject.parentRoleId = parentRoleId;
        insert userRoleObject;
        return userRoleObject;
    }
    
    private static User createUserwithRole(String profileName, String lastName, Id roleId) {
        
        String ProfileId = [SELECT Id FROM Profile WHERE Name = : profileName].Id;
        
        User userObject = new User();
        userObject.ProfileId = ProfileId;
        userObject.LastName = lastName;
        userObject.Email = lastName + '@amamama.com';
        userObject.Username = lastName + '@amamama.com' + System.currentTimeMillis();
        userObject.CompanyName = 'TEST';
        userObject.Title = 'title';
        userObject.Alias = 'alias';
        userObject.TimeZoneSidKey = 'America/Los_Angeles';
        userObject.EmailEncodingKey = 'UTF-8';
        userObject.LanguageLocaleKey = 'en_US';
        userObject.LocaleSidKey = 'en_US';
        userObject.UserRoleId = roleId;
        insert userObject;
        
        return userObject;
    }
    
    @future
    private static void createUser(String profileName, String lastName, Id contactId) {
        
        Id roleId = createUserRole('Officer', null).Id;
        
        String ProfileId = [SELECT Id FROM Profile WHERE Name = : profileName].Id;
        
        User userObject = new User();
        userObject.ProfileId = ProfileId;
        userObject.LastName = lastName;
        userObject.Email = lastName + '@amamama.com';
        userObject.Username = lastName + '@amamama.com' + System.currentTimeMillis();
        userObject.CompanyName = 'TEST';
        userObject.Title = 'title';
        userObject.Alias = 'alias';
        userObject.TimeZoneSidKey = 'America/Los_Angeles';
        userObject.EmailEncodingKey = 'UTF-8';
        userObject.LanguageLocaleKey = 'en_US';
        userObject.LocaleSidKey = 'en_US';
        userObject.ContactId = contactId;
      //  userObject.UserRoleId = roleId;
        insert userObject;
    }
    
    @isTest
    private static void testFeedTrigger() {
        User userObj = [SELECT Id FROM User WHERE LastName = 'Officer' LIMIT 1];
        
        Test.startTest();
        System.runAs(userObj) {
            
            Case caseObj = new Case();
            caseObj.Subject = 'Test';
            insert caseObj;
            
            List<FeedItem> feedList = new List<FeedItem>();
            FeedItem feed = new FeedItem();
            feed.ParentId = caseObj.Id;
            feed.Body = 'Hello';
            feedList.add(feed);
            
            FeedItem feed1 = new FeedItem();
            feed1.ParentId = caseObj.Id;
            feed1.Body = 'Hello';
            feedList.add(feed1);
            insert feedList;
            
            caseObj = [SELECT Start_Subsequent_Milestone__c, New_Customer_Post_or_Comment__c FROM Case WHERE Id =:caseObj.Id LIMIT 1];
            System.assert(caseObj != null);
            System.assert(caseObj.Start_Subsequent_Milestone__c);
            System.assert(caseObj.New_Customer_Post_or_Comment__c);
            
            caseObj.Start_Subsequent_Milestone__c = false;
            caseObj.New_Customer_Post_or_Comment__c = false;
            update caseObj;
            
            FeedComment feedCommentObj = new FeedComment();
            //feedCommentObj.ParentId = caseObj.Id;
            feedCommentObj.FeedItemId = feed.Id;
            feedCommentObj.CommentBody = 'Hello';
            insert feedCommentObj;
            
            caseObj = [SELECT Start_Subsequent_Milestone__c, New_Customer_Post_or_Comment__c FROM Case WHERE Id =:caseObj.Id LIMIT 1];
            System.assert(caseObj != null);
            System.assert(caseObj.Start_Subsequent_Milestone__c);
            System.assert(caseObj.New_Customer_Post_or_Comment__c);
          	
            caseObj.Start_Subsequent_Milestone__c = false;
            caseObj.New_Customer_Post_or_Comment__c = false;
            caseObj.Status = 'Closed';
            caseObj.Type = 'Refund';
            caseObj.Sub_Category_1__c = 'Airport Tax';
            caseObj.Disposition_Level_1__c = 'Sales';  // CThompson 20 Aug 18
            update caseObj;
 
            feed1 = new FeedItem();
            feed1.ParentId = caseObj.Id;
            feed1.Body = 'Hello';
            feedList.add(feed1);
            
            try {
            	insert feed1;
                System.assert(false, 'exception expected for SObject ' + feed1);
            } catch(Exception exp) {
                Boolean expectedExceptionThrown =  exp.getMessage().contains('You can\'t add post or comment on closed case.') ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
            }
            
            caseObj = [SELECT Start_Subsequent_Milestone__c, New_Customer_Post_or_Comment__c FROM Case WHERE Id =:caseObj.Id LIMIT 1];
            System.assert(caseObj != null);
            System.assert(!caseObj.Start_Subsequent_Milestone__c);
            System.assert(!caseObj.New_Customer_Post_or_Comment__c);
        }
        Test.stopTest();
    }
}