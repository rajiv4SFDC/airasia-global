public without sharing class FileController {
    
    @AuraEnabled
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) { 
        try{
            if(parentId == null){
                Contact c;
                try{
                    c = [select Id from Contact where LastName =: 'TempForFileUpload'];
                }catch(Exception e){
                    c = new Contact(LastName='TempForFileUpload');
                    insert c;
                }
                parentId = c.Id;
            }
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            
            Attachment a = new Attachment();
            a.parentId = parentId;
            
            a.Body = EncodingUtil.base64Decode(base64Data);
            a.Name = fileName;
            a.ContentType = contentType;
            
            insert a;
            
            /*ContentVersion cv = new ContentVersion();
            cv.PathOnClient = fileName;
            cv.FirstPublishLocationId = parentId;
            cv.Title = fileName;
            cv.VersionData = EncodingUtil.base64Decode(base64Data);
            insert cv;*/
            
            return a.Id;
        }catch(Exception e){
            system.debug('##Exception##'+e.getMessage());
            return null;
        } 
    }
    
    @AuraEnabled
    public static Id saveTheChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) { 
        if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }
        
        return Id.valueOf(fileId);
    }
    
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id = :fileId
        ];
        
     	String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
        
        update a;
    }
}