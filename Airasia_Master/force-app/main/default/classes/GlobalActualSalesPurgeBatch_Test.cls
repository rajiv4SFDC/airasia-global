/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
12/OCT/2017     Sharan Desai         Created
10/Jan/2018		Charles Thompson     Updated to match changes to 
.                                    Schedular_GlobalActualSalesPurge
*******************************************************************/
@isTest
public class GlobalActualSalesPurgeBatch_Test {
    
    public static testMethod void testPrepBatch() {

        // set up
        DateTime now = System.now();
        
        // call the method to test
        GlobalActualSalesPurgeBatch gaspb = new GlobalActualSalesPurgeBatch();
        Test.StartTest();
            gaspb.prepBatch();
        Test.stopTest();
        
        // making sure startTime is correct
        System.assertEquals(now, gaspb.startTime);
    }
    
}