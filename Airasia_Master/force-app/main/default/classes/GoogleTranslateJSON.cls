/**
 * @File Name          : GoogleTranslateJSON.cls
 * @Description        : Decode JSON Response from Google Cloud Translation API
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 9/19/2019, 11:20:18 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/19/2019   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
public class GoogleTranslateJSON {

	public Data data;

	public class Data {
		public List<List<Detections>> detections;
	}

	public class Detections {
		public String language;
		public Boolean isReliable;
		public Double confidence;
	}

	
	public static GoogleTranslateJSON parse(String json) {
		return (GoogleTranslateJSON) System.JSON.deserialize(json, GoogleTranslateJSON.class);
	}
}