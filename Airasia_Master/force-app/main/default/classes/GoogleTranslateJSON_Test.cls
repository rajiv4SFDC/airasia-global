/**
 * @File Name          : GoogleTranslateJSON_Test.cls
 * @Description        : Mocked up test JSON Response from Google Cloud Translation API
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 9/19/2019, 11:20:18 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/19/2019   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/

@IsTest
public class GoogleTranslateJSON_Test {
	
	static testMethod void testParse() {
		String json = '{'+
		'\"data\": {'+
		'		\"detections\":'+
		'		['+
		'		  [{'+
		'		  	 \"language\": \"en\",'+
		'		    \"isReliable\": false,'+
		'		    \"confidence\": 0.9882'+
		'		  }],'+
		'		  [{'+
		'		    \"language\": \"pl\",'+
		'		    \"isReliable\": false,'+
		'		    \"confidence\": 0.5683'+
		'		  }]'+
		'		]'+
		'		}'+
		'}';
		GoogleTranslateJSON obj = GoogleTranslateJSON.parse(json);
		System.assert(obj != null);
	}
}