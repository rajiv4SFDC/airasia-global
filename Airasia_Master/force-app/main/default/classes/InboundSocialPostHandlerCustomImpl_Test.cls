@isTest
public class InboundSocialPostHandlerCustomImpl_Test {
    
    static Map<String, Object> sampleSocialData;
    static InboundSocialPostHandlerCustomImpl handler;
    
    static {
        handler = new InboundSocialPostHandlerCustomImpl();
        sampleSocialData = getSampleSocialData('1');
        System.debug('Testinit: SampleSocialData = ' + sampleSocialData);
    }
    
    static testMethod void verifyNewRecordCreation() {
        SocialPost post = getSocialPost(sampleSocialData);
        SocialPersona persona = getSocialPersona(sampleSocialData);
        System.debug('verifyNewRecordCreation.post = '+post);
        System.debug('verifyNewRecordCreation.persona = '+persona);
        
        test.startTest();
        handler.handleInboundSocialPost(post, persona, sampleSocialData);
        test.stopTest();
        
        SocialPost createdPost = [SELECT Id, PersonaId, ParentId, WhoId FROM SocialPost];
        SocialPersona createdPersona = [SELECT Id, ParentId FROM SocialPersona];
        //Contact createdContact = [SELECT Id FROM Contact];
        Case createdCase = [SELECT Id, ContactId FROM Case];
        
        System.assertEquals(createdPost.PersonaId, createdPersona.Id, 'Post is not linked to the Persona.');
    }
    
    static testMethod void matchSocialPostRecord() {
        SocialPost existingPost = getSocialPost(getSampleSocialData('2'));
        insert existingPost;
        
        SocialPost post = getSocialPost(sampleSocialData);
        post.R6PostId = existingPost.R6PostId;
        SocialPersona persona = getSocialPersona(sampleSocialData);
        
        test.startTest();
        handler.handleInboundSocialPost(post, persona, sampleSocialData);
        test.stopTest();
        
        System.assertEquals(1, [SELECT Id FROM SocialPost].size(), 'There should be only 1 post');
    }
    
    static testMethod void matchSocialPersonaRecord() {
        Contact existingContact = new Contact(LastName = 'LastName');
        insert existingContact;
        SocialPersona existingPersona = getSocialPersona(getSampleSocialData('2'));
        existingPersona.ParentId = existingContact.Id;
        insert existingPersona;
        
        SocialPost post = getSocialPost(sampleSocialData);
        SocialPersona persona = getSocialPersona(sampleSocialData);
        persona.ExternalId = existingPersona.ExternalId;
        
        test.startTest();
        handler.handleInboundSocialPost(post, persona, sampleSocialData);
        test.stopTest();
        
        SocialPost createdPost = [SELECT Id, PersonaId, ParentId, WhoId FROM SocialPost];
        SocialPersona createdPersona = [SELECT Id, ParentId FROM SocialPersona];
        Contact createdContact = [SELECT Id FROM Contact];
        Case createdCase = [SELECT Id, ContactId FROM Case];
        
        System.assertEquals(createdPost.PersonaId, createdPersona.Id, 'Post is not linked to the Persona.');
        //System.assertEquals(createdPost.WhoId, createdPersona.ParentId, 'Post is not linked to the Contact');
        System.assertEquals(createdPost.ParentId, createdCase.Id, 'Post is not linked to the Case.');
        //System.assertEquals(createdCase.ContactId, createdContact.Id, 'Contact is not linked to the Case.');
    }
    
    static testMethod void matchCaseRecord() {
        Contact existingContact = new Contact(LastName = 'LastName');
        insert existingContact;
        SocialPersona existingPersona = getSocialPersona(getSampleSocialData('2'));
        existingPersona.ParentId = existingContact.Id;
        insert existingPersona;
        Case existingCase = new Case(ContactId = existingContact.Id, Subject = 'Test Case');
        insert existingCase;
        SocialPost existingPost = getSocialPost(getSampleSocialData('2'));
        existingPost.ParentId = existingCase.Id;
        existingPost.WhoId = existingContact.Id;
        existingPost.PersonaId = existingPersona.Id;
        insert existingPost;
        
        SocialPost post = getSocialPost(sampleSocialData);
        post.responseContextExternalId = existingPost.ExternalPostId;
        
        test.startTest();
        handler.handleInboundSocialPost(post, existingPersona, sampleSocialData);
        test.stopTest();

        SocialPost createdPost = [SELECT Id, 
                                         PersonaId, 
                                         ParentId, 
                                         WhoId 
                                  FROM   SocialPost 
                                  WHERE  R6PostId = :post.R6PostId
                                 ];
        System.assertEquals(existingPersona.Id, createdPost.PersonaId, 'Post is not linked to the Persona.');
        //System.assertEquals(existingContact.Id, createdPost.WhoId, 'Post is not linked to the Contact');
        //System.assertEquals(existingCase.Id, createdPost.ParentId, 'Post is not linked to the Case.');
        System.assertEquals(1, [SELECT Id FROM Case].size(), 'There should be 1 Case.'); //Updated Behaviour in Handler
    }

    static testMethod void matchCaseRecord2() {
        // different post data
        Contact existingContact = new Contact(LastName = 'LastName');
        insert existingContact;
        SocialPersona existingPersona = getSocialPersona(getSampleSocialDataForFSurvey('3'));
        existingPersona.ParentId = existingContact.Id;
        insert existingPersona;
        Case existingCase = new Case(ContactId = existingContact.Id, Subject = 'Test Case');
        insert existingCase;
        SocialPost existingPost = getSocialPost(getSampleSocialData('2'));
        existingPost.ParentId = existingCase.Id;
        existingPost.WhoId = existingContact.Id;
        existingPost.PersonaId = existingPersona.Id;
        existingPost.Recipient = '344185473103079';
        insert existingPost;
                
        SocialPost post = getSocialPost(sampleSocialData);
        post.responseContextExternalId = existingPost.ExternalPostId;
        post.Recipient = '344185473103079';  // match an existing external facebook account
        
        test.startTest();
        handler.handleInboundSocialPost(post, existingPersona, sampleSocialData);
        test.stopTest();

        SocialPost createdPost = [SELECT Id, 
                                         PersonaId, 
                                         ParentId, 
                                         WhoId 
                                  FROM   SocialPost 
                                  WHERE  R6PostId = :post.R6PostId
                                 ];
        System.assertEquals(existingPersona.Id, createdPost.PersonaId, 'Post is not linked to the Persona.');
        //System.assertEquals(existingContact.Id, createdPost.WhoId, 'Post is not linked to the Contact');
        //System.assertEquals(existingCase.Id, createdPost.ParentId, 'Post is not linked to the Case.');
        System.assertEquals(1, [SELECT Id FROM Case].size(), 'There should be 1 Case.');
    }
    
    static testMethod void reopenClosedCase() {
        Contact existingContact = new Contact(LastName = 'LastName');
        existingContact.Email = 'test@test.com';
        insert existingContact;
        SocialPersona existingPersona = getSocialPersona(getSampleSocialData('2'));
        existingPersona.ParentId = existingContact.Id;
        insert existingPersona;
        //Case existingCase = new Case(ContactId = existingContact.Id, Subject = 'Test Case', Status = 'Closed');
        Case existingCase = new Case(ContactId = existingContact.Id, Subject = 'Test Case', 
                                     Status = 'Closed', Disposition_Level_1__c = 'Client Support', 
                                     Type = 'Complaint', Sub_Category_1__c = 'Baggage');
        insert existingCase;
        SocialPost existingPost = getSocialPost(getSampleSocialData('2'));
        existingPost.ParentId = existingCase.Id;
        existingPost.WhoId = existingContact.Id;
        existingPost.PersonaId = existingPersona.Id;
        insert existingPost;
        
        SocialPost post = getSocialPost(sampleSocialData);
        post.responseContextExternalId = existingPost.ExternalPostId;
        
        test.startTest();
        handler.handleInboundSocialPost(post, existingPersona, sampleSocialData);
        test.stopTest();
    }
    
    static SocialPost getSocialPost(Map<String, Object> socialData) {
        SocialPost post = new SocialPost();
        post.Name = String.valueOf(socialData.get('source'));
        post.Content = String.valueOf(socialData.get('content'));
        post.Posted = Date.valueOf(String.valueOf(socialData.get('postDate')));
        post.PostUrl = String.valueOf(socialData.get('postUrl'));
        post.Provider = String.valueOf(socialData.get('mediaProvider'));
        post.MessageType = String.valueOf(socialData.get('messageType'));
        post.ExternalPostId = String.valueOf(socialData.get('externalPostId'));
        post.R6PostId = String.valueOf(socialData.get('r6PostId'));
        post.Language = String.valueOf(socialData.get('Language'));
        post.TopicProfileName = String.valueOf(socialData.get('TopicProfileName'));
        return post;
    }
    
    static SocialPersona getSocialPersona(Map<String, Object> socialData) {
        SocialPersona persona = new SocialPersona();
        persona.Name = String.valueOf(socialData.get('author'));
        persona.RealName = String.valueOf(socialData.get('realName'));
        persona.Provider = String.valueOf(socialData.get('mediaProvider'));
        persona.MediaProvider = String.valueOf(socialData.get('mediaProvider'));
        persona.ExternalId = String.valueOf(socialData.get('externalUserId'));
        return persona;
    }
    
    static Map<String, Object> getSampleSocialData(String suffix) {
        Map <String, Object> socialData = new Map<String, Object>();
        socialData.put('r6PostId', 'R6PostId' + suffix);
        socialData.put('r6SourceId', 'R6SourceId' + suffix);
        socialData.put('postTags', null);
        socialData.put('externalPostId', 'ExternalPostId' + suffix);
        //socialData.put('content', 'Content' + suffix);
        socialData.put('content', 'Complaint Baggage Call Center Baggage Allowance' + suffix);
        socialData.put('postDate', '2015-01-12T12:12:12Z');
        socialData.put('mediaType', 'Twitter');
        socialData.put('author', 'Author');
        socialData.put('skipCreateCase', false);
        socialData.put('mediaProvider', 'TWITTER');
        socialData.put('externalUserId', 'ExternalUserId');
        socialData.put('postUrl', 'PostUrl' + suffix);
        socialData.put('messageType', 'Direct');
        socialData.put('source', 'Source' + suffix);
        socialData.put('replyToExternalPostId', null);
        socialData.put('realName', 'Real Name');
        socialData.put('Language', 'English');
        socialData.put('provider', 'Twitter');
        socialData.put('TopicProfileName','AirAsia BIG');
        return socialData;
    }
    
    static Map<String, Object> getSampleSocialDataForSurvey(String suffix) {
        Map<String, Object> socialData = new Map<String, Object>();
        socialData.put('r6PostId', 'R6PostId' + suffix);
        socialData.put('r6SourceId', 'R6SourceId' + suffix);
        socialData.put('postTags', null);
        socialData.put('externalPostId', 'ExternalPostId' + suffix);
        //socialData.put('content', 'Content' + suffix);
        socialData.put('content', 'Complaint Baggage Call Center Baggage Allowance' + suffix);
        socialData.put('postDate', '2015-01-12T12:12:12Z');
        socialData.put('mediaType', 'Twitter');
        socialData.put('author', 'Author');
        socialData.put('skipCreateCase', false);
        socialData.put('mediaProvider', 'Twitter');
        
        socialData.put('provider', 'Twitter');
        
        socialData.put('externalUserId', 'ExternalUserId');
        socialData.put('postUrl', 'PostUrl' + suffix);
        socialData.put('messageType', 'Direct');
        socialData.put('source', 'Source' + suffix);
        socialData.put('replyToExternalPostId', null);
        socialData.put('realName', 'Real Name');
        socialData.put('TopicProfileName','Ourshop');
        return socialData;
    }
    
    static Map<String, Object> getSampleSocialDataForFSurvey(String suffix) {
        Map<String, Object> socialData = new Map<String, Object>();
        socialData.put('r6PostId', 'R6PostId' + suffix);
        socialData.put('r6SourceId', 'R6SourceId' + suffix);
        socialData.put('postTags', null);
        socialData.put('externalPostId', 'ExternalPostId' + suffix);
        //socialData.put('content', 'Content' + suffix);
        socialData.put('content', 'Complaint Baggage Call Center Baggage Allowance' + suffix);
        socialData.put('postDate', '2015-01-12T12:12:12Z');
        socialData.put('mediaType', 'Facebook');
        socialData.put('author', 'Author');
        socialData.put('skipCreateCase', false);
        socialData.put('mediaProvider', 'Facebook');
        
        socialData.put('provider', 'Facebook');
        
        socialData.put('externalUserId', 'ExternalUserId');
        socialData.put('postUrl', 'PostUrl' + suffix);
        socialData.put('messageType', 'Private');
        socialData.put('source', 'Source' + suffix);
        socialData.put('replyToExternalPostId', null);
        socialData.put('realName', 'Real Name');
        socialData.put('TopicProfileName','Rokki');
        return socialData;
    }
    
    static testMethod void testOtherScenario() {
        test.startTest();
        Integer closedDays = handler.getMaxNumberOfDaysClosedToReopenCase();
        System.assertEquals(5, closedDays);
        handler.getPostLabelsThatCreateCase();
        String accountId = handler.getDefaultAccountId();
        System.assertEquals(null, accountId);
        
        SocialPost post = getSocialPost(sampleSocialData);
        //handler.getPostLabelsThatCreateCase(post);
        handler.getPostLabelsThatCreateCase();
        handler.getPostLabels(post);
        handler.findReplyToBasedOnReplyToId(post);
        handler.findReplyToBasedOnResponseContextExternalPostIdAndProvider(post);
        
        SocialPersona persona = getSocialPersona(sampleSocialData);
        Contact newContact = new Contact(FirstName = handler.getPersonaFirstName(persona), 
                                         LastName = handler.getPersonaLastName(persona));
        insert newContact;
        //handler.createPersonaParent(post, persona);
        handler.createPersonaParent(persona);
        
        persona.Name = 'Test';
        persona.Provider = 'Other';
        handler.matchPersona(persona);
        
        persona.ExternalId = null;
        persona.Provider = 'Other';
        handler.matchPersona(persona);
        
        Case existingCase = new Case(Subject = 'Test Case', Status = 'Closed', 
                                     Disposition_Level_1__c = 'Client Support', 
                                     Type = 'Complaint', Sub_Category_1__c = 'Baggage');
        insert existingCase;
        handler.reopenCase(existingCase);
        handler.caseShouldBeReopened(existingCase);
        
        persona.id = null;
        //handler.updatePersona(post, persona);
        handler.updatePersona(persona);
        test.stopTest();
    }
    
    static testMethod void verifyTSurvey() {
        Case newCase = new Case();
        insert newCase;
        
        scs_twtSurveys__Social_Feedback_Survey__c stsfsc = new scs_twtSurveys__Social_Feedback_Survey__c();
        stsfsc.scs_twtSurveys__Case__c = newCase.Id;
        stsfsc.scs_twtSurveys__Direct_Message_ID__c = 'test';
        stsfsc.scs_twtSurveys__Feedback_Card_Id__c = 'test123';
        insert stsfsc;
        sampleSocialData = getSampleSocialDataForSurvey('1');
        
        SocialPost post = getSocialPost(sampleSocialData);
        post.ExternalPostId = 'test';
        SocialPersona persona = getSocialPersona(sampleSocialData);
        
        test.startTest();
        // handler.handleInboundSocialPost(post, persona, sampleSocialData);
        test.stopTest();
    }
    
    
    static testMethod void verifyFSurvey() {
        
        //  ExternalSocialAccount esa = new ExternalSocialAccount();
        //  esa.ProviderUserId = 'User1';
        //  esa.IsActive = true;
        //  esa.IsAuthenticated = true;
        //  esa.Provider = 'Facebook';
        //  insert esa;
        
        sampleSocialData = getSampleSocialDataForFSurvey('1');
        SocialPost post = getSocialPost(sampleSocialData);
        // post.ExternalPostId='test';
        post.Recipient = 'User1';
        SocialPersona persona = getSocialPersona(sampleSocialData);
        
        test.startTest();
        handler.handleInboundSocialPost(post, persona, sampleSocialData);
        test.stopTest();
    }
    
}