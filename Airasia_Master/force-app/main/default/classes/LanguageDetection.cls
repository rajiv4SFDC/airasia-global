/**
 * @File Name          : LanguageDetection.cls
 * @Description        : This is used to detect send out the email body to Google Cloud Translation API for language detection
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 9/19/2019, 11:20:18 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/19/2019   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
public class LanguageDetection {
    public static final String DEFAULT_LANG = 'English';
    public static final String SETTING_KEY = 'Language_Routing_Error';
    private static System_Settings__mdt logLevelCMD;
    private static Datetime startTime = DateTime.now();
    private static List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();
    private static Map<String, String> elbQueueMap = new Map<String, String>();
    
    @future(callout=true)
    public static void detectEmailLanguage(Set<Id> emailIds) {
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey(SETTING_KEY));
        
        Web_Services_Credentials__mdt apiKey = getApiKey();      
        
        List<EmailMessage> emailToUpdate = new List<EmailMessage>();
        List<Case> casesToUpdate = new List<Case>();        
        List<String> emailBodies = new List<String>();      
        List<String> emailToAddress = new List<String>();
        List<Id> emailIdList = new List<Id>();
        List<Id> caseIdList = new List<Id>();
        Set<String> supportedEmailAddresses = new Set<String>();
            
        for(Email_Language_Based_Routing__mdt elb : EmailTriggerHandler.getRoutingMDT()) {
            elbQueueMap.put(elb.Email_Address__c + '-' + elb.Email_Language__c, elb.Queue__c);
			System.debug('key--> ' + elb.Email_Address__c + '-' + elb.Email_Language__c + ' : ' + elb.Queue__c);
            supportedEmailAddresses.add(elb.Email_Address__c);
        }
        
        for (EmailMessage msg : [SELECT Id,
                       		  TextBody,
                              Subject,
                              ParentId,
                              ToAddress
                       FROM EmailMessage 
                                 WHERE Id=: emailIds]) {
        	emailIdList.add(msg.Id);
        	caseIdList.add(msg.ParentId);
            //emailToAddress.add(msg.ToAddress); //To Remove unsupported email
            //Check for multiple receipients
            for (String toAddress : msg.toAddress.split(';')) {
                if (supportedEmailAddresses.contains(toAddress)) {
                    emailToAddress.add(toAddress);
                    break;
                }
            }
			emailBodies.add(destroySignature(msg.TextBody, msg.Subject));                                     
        }
        
        if(!emailBodies.isEmpty()) {   
            Http http = new Http();
            HttpRequest req = createRequest(emailBodies, apiKey);
            HttpResponse res = http.send(req);
            if(res.getStatusCode() == 200) {
                List<List<sObject>> objToUpdate = processResponse(res, emailIdList, emailToAddress, caseIdList);                
                emailToUpdate = objToUpdate[0];
                casesToUpdate = objToUpdate[1];
            }
            else {
                //Error log
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'LanguageDetection', 'detectEmailLanguage',
                                                                             null, 'HttpResponse Code - ' + res.getStatusCode(), res.getBody(), req.getBody(), 'Error Log', startTime, logLevelCMD, null));
            }
        }        
        
        try {
        	Database.SaveResult[] resEmails = Database.update(emailToUpdate, false);
        	Database.SaveResult[] resCases = Database.update(casesToUpdate, false);
        }
        catch (Exception e) {
                 //Error log
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'LanguageDetection', 'detectEmailLanguage',
                                                                             null, 'Database Insert - ' + e.getMessage(), '-', e.getMessage(), 'Error Log', startTime, logLevelCMD, e));
        }
        
        if(!appWrapperLogList.isEmpty()) {
            GlobalUtility.logMessage(appWrapperLogList);
        }
    }
    
    private static HttpRequest createRequest(List<String> emailBodies, Web_Services_Credentials__mdt apiKey) {
		HttpRequest req = new HttpRequest();
        req.setEndpoint(apiKey.URL__c + '?access_token=' + getAccessToken(apiKey));
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
		req.setTimeout(10000);
        
        String jsonBody = '{';
        for (String emailBody : emailBodies) {
            jsonBody += 'q: ' + JSON.serialize(emailBody) + ',';
        }
        jsonBody = jsonBody.substring(0, jsonBody.length()-1); //Remove last comma
        jsonBody += '}';
        req.setBody(jsonBody);

		return req;        
    }
    
    private static List<List<sObject>> processResponse(HttpResponse res, List<Id> emailIdList, List<String> emailToAddress, List<Id> emailParentIdList) {
        List<List<sObject>> objtoUpdate = new List<List<sObject>>();
        List<EmailMessage> emailLang = new List<EmailMessage>();
        List<Case> caseLang = new List<Case>();
        Map<String, String> supportedLangMap = getSupportedLanguageMap();  
        Map<String, String> supportedCaseLangMap = getSupportedCaseLanguageMap(); 
        
        String json = res.getBody();
        System.debug('json -> ' + json);
        //Parse JSON
        GoogleTranslateJSON obj = GoogleTranslateJSON.parse(json);
        //Create Email Message Objects for updating
        if(obj.data.detections.size() == emailIdList.size()) {
            for (Integer i = 0; i < emailIdList.size(); i++) {
                EmailMessage emailObj = new EmailMessage();
                emailObj.Id = emailIdList[i];
                String langValue = supportedLangMap.get(obj.data.detections[i][0].Language);
                
                if(String.isEmpty(langValue)) {
                	emailObj.Language__c = DEFAULT_LANG;                     
                }
                else {
                	emailObj.Language__c = langValue;                    
                }

                Case caseObj = new Case();
                caseObj.Id = emailParentIdList[i];
                String caseLangValue = supportedCaseLangMap.get(obj.data.detections[i][0].Language);
                
                if(String.isEmpty(caseLangValue)) {
                	caseObj.Case_Language__c = DEFAULT_LANG;                     
                }
                else {
                	caseObj.Case_Language__c = caseLangValue;                    
                }
                
                //Get Queue for Case
				String queueName = elbQueueMap.get(emailToAddress[i] + '-' + langValue);
                 try {
                     System.debug('Queue -> ' + queueName);
                    queueName = Utilities.getQueueIdByName(queueName);
                 }
                 catch (Exception e) {
                     System.debug('Queue Error');
                    queueName = Utilities.getQueueIdByName(Utilities.getAppSettingsMdtValueByKey('Email_Language_Routing_Error_Queue'));  
                     appWrapperLogList.add(Utilities.createApplicationLog('Error', 'LanguageDetection', 'processResponse',
                                                                             null, 'Queue Error - "' + queueName + '" ' + e.getMessage(), '-', e.getMessage(), 'Error Log', startTime, logLevelCMD, e));
        
                 }
                caseObj.OwnerId = queueName;
                
                caseLang.add(caseObj);
                emailLang.add(emailObj);
            }
        }
        
        objtoUpdate.add(emailLang);
        objtoUpdate.add(caseLang);
        return objtoUpdate;
    }
    
    //Based on Google API Return (ISO-639-1)
    private static Map<String, String> getSupportedLanguageMap() {
        Map<String, String> langCodeMap = new Map<String, String>();
        
        for(TranslateLanguageCode__mdt tlc : [SELECT Id, 
                                                    Language__c, 
                                                    LanguageCode__c,
                                              		Case_Language__c
                                              FROM TranslateLanguageCode__mdt]) {
           langCodeMap.put(tlc.LanguageCode__c, tlc.Language__c);
       }
        
        
        return langCodeMap;
    }
    private static Map<String, String> getSupportedCaseLanguageMap() {
        Map<String, String> langCodeMap = new Map<String, String>();
        
        for(TranslateLanguageCode__mdt tlc : [SELECT Id, 
                                                    Language__c, 
                                                    LanguageCode__c,
                                              		Case_Language__c
                                              FROM TranslateLanguageCode__mdt]) {
           langCodeMap.put(tlc.LanguageCode__c, tlc.Case_Language__c);
       }
        
        
        return langCodeMap;
    }
    
    private static Web_Services_Credentials__mdt  getAPIKey() {
        Web_Services_Credentials__mdt  apiKey = [SELECT Url__c,
                                                 		Username__c,
                                                 		Password__c,
                                              		  	Key__c,
                                                 		LongKey__c,
                                                 		Origin__c
                                               FROM Web_Services_Credentials__mdt 
                                               WHERE Web_Service_Name__c = 'GAPIKey'];
        
        return apiKey;
    }
    
    public static String getAccessToken(Web_Services_Credentials__mdt apiKey) {
		
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setEndpoint(apiKey.Password__c);
        req.setMethod('POST');
        
        req.setHeader('ContentType','application/x-www-form-urlencoded');
        
        String header = '{"alg":"RS256","typ":"JWT"}';
        String header_encoded = EncodingUtil.base64Encode(blob.valueof(header));
        
        String claim_set = '{"iss":"' + apiKey.Username__c + '"';
        claim_set += ',"scope":"' + apiKey.Origin__c + '"';
        claim_set += ',"aud":"' + apiKey.Password__c + '"';
        claim_set += ',"exp":"' + datetime.now().addHours(1).getTime()/1000;
        claim_set += '","iat":"' + datetime.now().getTime()/1000 + '"}';
        
        String claim_set_encoded = EncodingUtil.base64Encode(blob.valueof(claim_set));
        
        String signature_encoded = header_encoded + '.' + claim_set_encoded;
        
        String key = apiKey.LongKey__c;
        
        blob private_key = EncodingUtil.base64Decode(key);
        signature_encoded = signature_encoded.replaceAll('=','');
        String signature_encoded_url = EncodingUtil.urlEncode(signature_encoded,'UTF-8');
        blob signature_blob =   blob.valueof(signature_encoded_url);
        
        String signature_blob_string = EncodingUtil.base64Encode(Crypto.sign('RSA-SHA256', signature_blob, private_key));
        
        String JWT = signature_encoded + '.' + signature_blob_string;
        
        JWT = JWT.replaceAll('=','');
        
        String grant_string= 'urn:ietf:params:oauth:grant-type:jwt-bearer';
        req.setBody('grant_type=' + EncodingUtil.urlEncode(grant_string, 'UTF-8') + '&assertion=' + EncodingUtil.urlEncode(JWT, 'UTF-8'));
        res = h.send(req);
        String response_debug = res.getBody() +' '+ res.getStatusCode();
        System.debug('Response =' + response_debug );
        if(res.getStatusCode() == 200) {
            JSONParser parser = JSON.createParser(res.getBody());
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                    // Move to the value.
                    parser.nextToken();
                    // Return the access_token
                    return parser.getText();
                }
            }
        }
        return 'error';
    }
    
    public static String destroySignature(String emailBody, String subject) {        
        subject = subject != null ? subject : '';
        emailBody = emailBody != null ? emailBody : '';
        
        if (subject.toUpperCase().contains('[ REF:_') &&
           	emailBody.toUpperCase().contains('> ')) {
            emailBody = emailBody.left(emailBody.toUpperCase().indexOf('> '));
        }
        
        if (!subject.toUpperCase().contains('[ REF:_')) {
            emailBody = subject + ' ' + emailBody;
            System.debug('New Email Received');
        }
        
        if (emailBody.toUpperCase().contains('DISCLAIMER')) {
            emailBody = emailBody.left(emailBody.toUpperCase().indexOf('DISCLAIMER'));
        }
        else if (emailBody.toUpperCase().contains('REGARDS,')) {
            emailBody = emailBody.left(emailBody.toUpperCase().indexOf('REGARDS,'));
        }
        else if (emailBody.toUpperCase().contains('THANK YOU,')) {
            emailBody = emailBody.left(emailBody.toUpperCase().indexOf('THANK YOU,'));
        }
        else {
            emailBody = emailBody.left(Math.round(emailBody.length()*0.3));
        }
        
        return emailBody;
    }
}