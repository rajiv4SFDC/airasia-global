/**
 * @File Name          : LanguageDetectionMock.cls
 * @Description        : This is used to emulate the response from Google Cloud Translation API
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 9/19/2019, 11:20:18 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/19/2019   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
public class LanguageDetectionMock implements HttpCalloutMock {
    private String langCode = 'en';
    public LanguageDetectionMock(String langCode) {
        this.langCode = langCode;
    }
    
	public HttpResponse respond(HttpRequest rqst) {
        HttpResponse res = new HttpResponse();
        res.setBody('{' +
                     '"data": {' +
                      '"detections": [' +
                       '[' +
                        '{' +
                         '"confidence": 0.6492361426353455,' +
                         '"isReliable": false,' +
                         '"language": "' + langCode + '"' +
                        '}' +
                       ']' +
                      ']' +
                     '}' +
                    '} ');
        res.setStatusCode(200);
        
        return res;
    }
}