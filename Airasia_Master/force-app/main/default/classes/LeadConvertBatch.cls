/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
22/09/2017      Sharan Desai   		Created
*******************************************************************/
global class LeadConvertBatch implements Database.Batchable<sObject>,Database.Stateful {
    
    global String accountId;
    global String leadId;
    global String sObjectName;
    global String processLog ='';
    global List < ApplicationLogWrapper > appWrapperLogList = null;
    global DateTime startTime =null;
    global System_Settings__mdt logLevelCMD;
    global static boolean throwUnitTestExceptions = false;
    global static boolean throwMainUnitTestExceptions = false;
    
    global LeadConvertBatch(String accountId,String leadId,String sObjectName){
        this.accountId=accountId;
        this.leadId=leadId;
        this.sObjectName=sObjectName;
        processLog='';
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        startTime = DateTime.now();
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('LeadConvertBatch_Log_Level_CMD_NAME'));
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext cntxt){    
        
        String soqlQueryString = 'SELECT ID,Lead__c FROM '+sObjectName+' WHERE Lead__c = :leadId';   
        
        if(sObjectName.equalsIgnoreCase(LeadConvertConstants.OPPORTUNITY_OBJECT_NAME)){
            soqlQueryString = 'SELECT ID,Lead__c FROM '+sObjectName+' WHERE Lead__c = :leadId';   
        }else{            
            soqlQueryString = 'SELECT ID,Lead__c FROM '+sObjectName+' WHERE Lead__c = :leadId OR WHOId =:leadId';               
        }
        
        return Database.getQueryLocator(soqlQueryString);
    }    
    
    global void execute(Database.BatchableContext cntxt,List<sObject> scope){
        
        try{  
            
            //-- Determine the Object
            Schema.SObjectType sObjectType;
            String sObjectName ='';
            if(scope.size()>0){
                sObjectType =scope.get(0).getSObjectType();                
                if(sObjectType == Schema.getGlobalDescribe().get(LeadConvertConstants.OPPORTUNITY_OBJECT_NAME)){
                    sObjectName=LeadConvertConstants.OPPORTUNITY_OBJECT_NAME;
                }else if(sObjectType == Schema.getGlobalDescribe().get(LeadConvertConstants.TASK_OBJECT_NAME)){                    
                    sObjectName=LeadConvertConstants.TASK_OBJECT_NAME;
                }else if(sObjectType == Schema.getGlobalDescribe().get(LeadConvertConstants.EVENT_OBJECT_NAME)){                    
                    sObjectName=LeadConvertConstants.EVENT_OBJECT_NAME;
                }                
            }
            
            //-- Build the update criteria based on object type for Re-parenting with account
            for(sObject eachsObject : scope){                
                if(sObjectName.equals(LeadConvertConstants.OPPORTUNITY_OBJECT_NAME)){
                    eachsObject.put(LeadConvertConstants.ACCOUNTID_FIELD_API_NAME,accountId);
                    eachsObject.put(LeadConvertConstants.LEADID_FIELD_API_NAME,null);                
                }else{
                    
                    eachsObject.put(LeadConvertConstants.WHOID_FIELD_API_NAME,null);
                    eachsObject.put(LeadConvertConstants.WHATID_FIELD_API_NAME,accountId);
                    eachsObject.put(LeadConvertConstants.LEADID_FIELD_API_NAME,null);
                    
                    if(test.isRunningTest() && throwUnitTestExceptions){
                        eachsObject.put(LeadConvertConstants.WHATID_FIELD_API_NAME,'accountId');
                    }                    
                }
            }      
            
            if(test.isRunningTest() && throwMainUnitTestExceptions){
                scope = null;
            }
            
            //-- update records to re-parent with matching account
            LIST<Database.SaveResult> saveResultSet =  Database.update(scope, false);    
          
            //-- build log for failed records
            for(Integer k=0 ;k <saveResultSet.size();k++){                
                if(!saveResultSet[k].isSuccess()){                    
                    for(Database.Error err : saveResultSet[k].getErrors()){
                        processLog += 'Failed to re-parent '+sObjectName+' ::: '+scope[k]+' for Lead ::: '+leadId+'\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += sObjectName+' fields that affected this error: ' + err.getFields() + '\r\n';
                        appWrapperLogList.add(Utilities.createApplicationLog('Error', 'LeadConvertBatch', 'execute',
                                                                         null, 'LeadConvertBatch', processLog, null,'Error Log',  startTime, logLevelCMD, null));
                    
                    }
                }               
            }
        }catch(Exception reparentingException){
            //-- Perform Application logging
            processLog = 'Failed Processing LeadConvert Batch : \r\n';
            processLog += 'REASON :: '+reparentingException.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'LeadConvertBatch', 'execute',
                                                                 null, 'LeadConvertBatch', processLog, null,'Error Log',  startTime, logLevelCMD, reparentingException));
        }               
    }
    
     global void finish(Database.BatchableContext bc) {
         //-- Perform Logging of failed records if any
         if(appWrapperLogList.size()>0){
             GlobalUtility.logMessage(appWrapperLogList);             
         }        
    } // END - finish
    
}