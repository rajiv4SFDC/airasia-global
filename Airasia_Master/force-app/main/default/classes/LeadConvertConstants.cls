public class LeadConvertConstants {
        
    public static final  String OPPORTUNITY_OBJECT_NAME='Opportunity';
    
    public static final String TASK_OBJECT_NAME='Task';
    
    public static final String EVENT_OBJECT_NAME='Event';
    
    public static final String WHOID_FIELD_API_NAME='WhoId';
    
    public static final String WHATID_FIELD_API_NAME='whatID';
    
    public static final String ACCOUNTID_FIELD_API_NAME='AccountId';
    
    public static final String LEADID_FIELD_API_NAME='Lead__c';

}