/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/09/2017      Sharan Desai   		Created
*******************************************************************/

public with sharing class LeadTriggerHandler {
    
   
    
    /**
    * This method performs all the required operation on AfterInsert context
    * 
    **/
    public static void isAfterInsert(Lead[] leads){
        
        LeadTriggerHelper.reparentOpportunityAndActivity(leads);
    }
    
    /**
    * This method performs all the required operation on AfterInsert context
    * 
    **/
    public static void isAfterUpdate(Lead[] leads){
        LeadTriggerHelper.reparentOpportunityAndActivity(leads);
    }

}