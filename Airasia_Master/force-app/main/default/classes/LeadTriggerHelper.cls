/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/09/2017      Sharan Desai        Created
*******************************************************************/

public with sharing class LeadTriggerHelper {
    
   // @TestVisible static boolean throwUnitTestExceptions=false;
    @TestVisible static boolean throwMainUnitTestExceptions=false;
    
    /**
    * This method updates Booking Channel and Sub Channel based on SalesChannel
    * 
    **/
    public static void reparentOpportunityAndActivity(Lead[] leads){
        
        DateTime startTime = DateTime.now();
        List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
        System_Settings__mdt logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('LeadTrigger_Log_Level_CMD_NAME'));
        List<Opportunity> globalOpportunityList = new List<Opportunity>();        
        List<Task> globalTaskList = new List<Task>();
        List<Event> gloablEventList = new List<Event>();
        
        try{
            
            Map<String,String> orgCodeAndAccIdMap = new   Map<String,String>();   
            Map<String,String> leadIdAndOrgCodeMap = new   Map<String,String>();   
            for(Lead eachLead : leads){ 
                if(String.isNotBlank(eachLead.Organization_Code__c)){
                    leadIdAndOrgCodeMap.put(eachLead.ID,eachLead.Organization_Code__c);
                }
            }
                        
            if(leadIdAndOrgCodeMap.size()>0){
            
             //-- This block of code is for Unit Testing
                if(Test.isRunningTest() && throwMainUnitTestExceptions){
                    leadIdAndOrgCodeMap=null;                    
                }
            
                //-- build map of OrgCode and AccountId
                for(Account eachAccount : [SELECT ID,Organization_Code__c from Account WHERE Organization_Code__c IN :leadIdAndOrgCodeMap.values() ]){
                    orgCodeAndAccIdMap.put(eachAccount.Organization_Code__c,eachAccount.ID);
                }
                
                //-- If matching Account records exist in system
                if(orgCodeAndAccIdMap.size()>0){
                    
                    Map<String,Lead> contactExtIdAndLeadObjectMap = new Map<String,Lead>();                    
					List<String> leadIdList = new List<String>();
                    for(Lead eachLead : leads){
                        if(String.isNotBlank(eachLead.Organization_Code__c)){                            
                            String matchingAccountId =  orgCodeAndAccIdMap.containsKey(eachLead.Organization_Code__c)?orgCodeAndAccIdMap.get(eachLead.Organization_Code__c):'';
                            if(String.isNotBlank(matchingAccountId)){                                
                                Database.executeBatch(new LeadConvertBatch(matchingAccountId,eachLead.ID,LeadConvertConstants.OPPORTUNITY_OBJECT_NAME));
                                Database.executeBatch(new LeadConvertBatch(matchingAccountId,eachLead.ID,LeadConvertConstants.TASK_OBJECT_NAME));
                                Database.executeBatch(new LeadConvertBatch(matchingAccountId,eachLead.ID,LeadConvertConstants.EVENT_OBJECT_NAME));                       
                           	    
                                //-- build lead Id to update
                                leadIdList.add(eachLead.ID);                                
                           
                                String contactExtIdString ='';                                
                                if(String.isNotBlank(eachLead.Organization_Code__c)){
                                    contactExtIdString= eachLead.Organization_Code__c;
                                }
                                if(String.isNotBlank(eachLead.Organization_Code__c)){
                                    contactExtIdString = contactExtIdString+eachLead.LastName;
                                }
                                if(String.isNotBlank(eachLead.Organization_Code__c)){
                                    contactExtIdString = contactExtIdString+eachLead.FirstName;
                                }
                                if(String.isNotBlank(eachLead.Organization_Code__c)){
                                    contactExtIdString = contactExtIdString+eachLead.Email;
                                }  
                                
                                if(String.isNotBlank(contactExtIdString)){
                                     contactExtIdAndLeadObjectMap.put(contactExtIdString,eachLead);
                                }
                            }                            
                        }
                    }     
                    
                    if(leadIdList.size()>0){
                        
                        List<Contact> contactList = [SELECT ID,Contact_ExtId__c,FirstName,MiddleName,LastName,Title,Email,Phone,MobilePhone FROM CONTACT Where Contact_ExtId__c IN :contactExtIdAndLeadObjectMap.keySet() AND Is_Primary__c = false];
                        Map<String,Contact> contactMap = new Map<String,Contact>();
                        for(Contact eachContact : contactList){
                            contactMap.put(eachContact.Contact_ExtId__c, eachContact);
                        }                        
                        
                        List<Contact> upsertContactList = new List<Contact>();
                        for(String eachContExtId : contactExtIdAndLeadObjectMap.keySet()){
                            
                            Lead eachLead = contactExtIdAndLeadObjectMap.get(eachContExtId);    
                            
                            if(eachLead!=null){
                                Contact contactObj = null; 
                                if(contactMap!=null && contactMap.containsKey(eachContExtId)){
                                    contactObj = contactMap.get(eachContExtId);
                                }else{
                                    contactObj = new Contact();
                                    contactObj.AccountId=orgCodeAndAccIdMap.get(eachLead.Organization_Code__c);
                                    contactObj.Contact_ExtId__c=eachContExtId;
                                }
                                contactObj.FirstName=eachLead.FirstName;
                                contactObj.MiddleName=eachLead.MiddleName;
                                contactObj.LastName=eachLead.LastName;
                                contactObj.Title=eachLead.Title;
                                contactObj.Email=eachLead.Email;
                                contactObj.Phone=eachLead.Phone;
                                contactObj.Phone=eachLead.Phone;
                                contactObj.MobilePhone=eachLead.MobilePhone; 
                                upsertContactList.add(contactObj);
                            }
                        }
                        
                        if(upsertContactList.size()>0){
                            upsert upsertContactList;
                        }                                                
                        updateLeadStatus(leadIdList);
                    }                    
                }
            }
            
            
        }catch(Exception reparentOpportunityAndActivityException){
            //-- Perform Application logging
            String processLog = 'Lead Trigger Failed to initiate LeadConvert Batch for Processing Records: \r\n';
            processLog += 'REASON :: '+reparentOpportunityAndActivityException.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'LeadTriggerHelper', 'reparentOpportunityAndActivity',
                                                                 null, 'LeadTrigger', processLog, null,'Error Log',  startTime, logLevelCMD, reparentOpportunityAndActivityException));
        }finally{      
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }  

}
    
    @future
    public static void updateLeadStatus(List<String> leadIdList){
        
        //-- Update lead status to Closed Converted
        List<lead> updateList = new List<Lead>();
        for(String eachLeadId : leadIdList){
            Lead newLead = new Lead(Id=eachLeadId,Status='Closed Converted');
            updateList.add(newLead);
        }
        if(updateList.size()>0){
            update updateList;
        } 
        
        
        
        
    }
    
    
}