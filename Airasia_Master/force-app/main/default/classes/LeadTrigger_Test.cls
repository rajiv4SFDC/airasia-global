/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
22/09/2017      Sharan Desai   		Created
10/Oct/2019		Alvin Tayag			Updated Lead Object Creation to add Lookup to Account
*******************************************************************/
@IsTest(SeeAllData=false)
public class LeadTrigger_Test {
    
    private static testMethod void testReparentingOpportunityAndActivityBestCaseScenario() {
        
        Map<String,Object> accountFieldValueMap = new Map<String,Object>();
        accountFieldValueMap.put('Name','Test Account');
        accountFieldValueMap.put('Organization_Code__c','AB456');
        accountFieldValueMap.put('Type',ActualSalesTriggerConstants.ACCOUNTY_TYPE_CORPORATE_DIRECT);
        TestUtility.createAccount(accountFieldValueMap);        
        
        
        Test.startTest();
        
        //10 Oct 2019 - Added to relate lead to account as per Jessica's new field
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
        
        Map<String,Object> leadFieldValueMap = new Map<String,Object>();
        leadFieldValueMap.put('FirstName','FN');
        leadFieldValueMap.put('MiddleName','MN');
        leadFieldValueMap.put('LastName','LN');
        leadFieldValueMap.put('Company','Company');    
        leadFieldValueMap.put('phone','9663311740');  
        leadFieldValueMap.put('Organization_Code__c','AB456');   
        leadFieldValueMap.put('Organization_Code_1__c', accountId); //10 Oct 2019 - Added to relate lead to account as per Jessica's new field
        TestUtility.createLead(leadFieldValueMap);       
        
        update TestUtility.getLead();
        
        Map<String,Object> taskFieldValueMap = new Map<String,Object>();
        taskFieldValueMap.put('Priority','Normal');
        taskFieldValueMap.put('Status','Open');
        taskFieldValueMap.put('Subject','Test Task');
        taskFieldValueMap.put('Lead__c',TestUtility.getLead().Id);        
        TestUtility.createTask(taskFieldValueMap);
        
        Map<String,Object> eventFieldValueMap = new Map<String,Object>();
        eventFieldValueMap.put('Subject','Test Event');
        eventFieldValueMap.put('StartDateTime',Date.today());
        eventFieldValueMap.put('EndDateTime',Date.today());
        eventFieldValueMap.put('Lead__c',TestUtility.getLead().Id);        
        TestUtility.createEvent(eventFieldValueMap);
        
        Map<String,Object> opportunityFieldValueMap = new Map<String,Object>();
        opportunityFieldValueMap.put('Name','Sharan Opportunity');
        opportunityFieldValueMap.put('CloseDate',Date.today());
        opportunityFieldValueMap.put('POS__c','AK');
        opportunityFieldValueMap.put('Sales_Channel__c','Travel Fair');
        opportunityFieldValueMap.put('Lead__c',TestUtility.getLead().Id);
        opportunityFieldValueMap.put('StageName','Qualification');
        opportunityFieldValueMap.put('Sub_Channel__c','Travel Fair');
        TestUtility.createOpportunity(opportunityFieldValueMap);
        
        
        
        Test.stopTest();
        
        List<String> opportunityQueryFieldsList = new List<String>();
        opportunityQueryFieldsList.add('AccountId');
        Opportunity opportunityObj = (Opportunity)TestUtility.getOpportunity(opportunityQueryFieldsList);        
        
        List<String> taskQueryFieldsList = new List<String>();
        taskQueryFieldsList.add('AccountId');
        Task taskObj = (Task)TestUtility.getTask(taskQueryFieldsList);
        
        List<String> eventQueryFieldsList = new List<String>();
        eventQueryFieldsList.add('AccountId');
        Event eventObj = (Event)TestUtility.getEvent(eventQueryFieldsList);
        
        System.assertEquals(TestUtility.getAccount().Id, opportunityObj.AccountId);
        System.assertEquals(taskObj.AccountId, TestUtility.getAccount().Id);
        System.assertEquals(eventObj.AccountId, TestUtility.getAccount().Id);
    }
    
    private static testMethod void testTriggerFailureScenario() {
        
        Map<String,Object> accountFieldValueMap = new Map<String,Object>();
        accountFieldValueMap.put('Name','Test Account');
        accountFieldValueMap.put('Organization_Code__c','AB456');
        accountFieldValueMap.put('Type',ActualSalesTriggerConstants.ACCOUNTY_TYPE_CORPORATE_DIRECT);
        TestUtility.createAccount(accountFieldValueMap);        
        
        
        Test.startTest();
        
        LeadTriggerHelper.throwMainUnitTestExceptions=true;
        
        Map<String,Object> leadFieldValueMap = new Map<String,Object>();
        leadFieldValueMap.put('FirstName','FN');
        leadFieldValueMap.put('MiddleName','MN');
        leadFieldValueMap.put('LastName','LN');
        leadFieldValueMap.put('Company','Company');    
        leadFieldValueMap.put('phone','9663311740');  
        leadFieldValueMap.put('Organization_Code__c','AB456');  
        TestUtility.createLead(leadFieldValueMap);       
        
        update TestUtility.getLead();
        
        Map<String,Object> taskFieldValueMap = new Map<String,Object>();
        taskFieldValueMap.put('Priority','Normal');
        taskFieldValueMap.put('Status','Open');
        taskFieldValueMap.put('Subject','Test Task');
        taskFieldValueMap.put('Lead__c',TestUtility.getLead().Id);        
        TestUtility.createTask(taskFieldValueMap);
        
        Map<String,Object> eventFieldValueMap = new Map<String,Object>();
        eventFieldValueMap.put('Subject','Test Event');
        eventFieldValueMap.put('StartDateTime',Date.today());
        eventFieldValueMap.put('EndDateTime',Date.today());
        eventFieldValueMap.put('Lead__c',TestUtility.getLead().Id);        
        TestUtility.createEvent(eventFieldValueMap);
        
        Map<String,Object> opportunityFieldValueMap = new Map<String,Object>();
        opportunityFieldValueMap.put('Name','Sharan Opportunity');
        opportunityFieldValueMap.put('CloseDate',Date.today());
        opportunityFieldValueMap.put('POS__c','AK');
        opportunityFieldValueMap.put('Sales_Channel__c','Travel Fair');
        opportunityFieldValueMap.put('Lead__c',TestUtility.getLead().Id);
        opportunityFieldValueMap.put('StageName','Qualification');
        opportunityFieldValueMap.put('Sub_Channel__c','Travel Fair');
        TestUtility.createOpportunity(opportunityFieldValueMap);
        
        
        
        Test.stopTest();
        
        List<String> opportunityQueryFieldsList = new List<String>();
        opportunityQueryFieldsList.add('AccountId');
        Opportunity opportunityObj = (Opportunity)TestUtility.getOpportunity(opportunityQueryFieldsList);        
        
        List<String> taskQueryFieldsList = new List<String>();
        taskQueryFieldsList.add('AccountId');
        Task taskObj = (Task)TestUtility.getTask(taskQueryFieldsList);
        
        List<String> eventQueryFieldsList = new List<String>();
        eventQueryFieldsList.add('AccountId');
        Event eventObj = (Event)TestUtility.getEvent(eventQueryFieldsList);
        
        System.assertEquals(null, opportunityObj.AccountId);
        System.assertEquals(null, taskObj.AccountId);
        System.assertEquals(null, eventObj.AccountId);
    }
    
}