global class LightningForgotPasswordController {

    public LightningForgotPasswordController() {

    }

    @AuraEnabled
    public static String forgotPassword(String username) {
        String result = 'success';
        try {
            Site.forgotPassword(username);

            if(!Site.isValidUsername(username)) {
                result = Label.Site.invalid_email;
            } 
        }
        catch (Exception ex) {
            result = ex.getMessage();
        }
        return result;
    }

}