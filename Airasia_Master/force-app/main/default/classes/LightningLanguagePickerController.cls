public class LightningLanguagePickerController {

    private static final String AIR_ASIA_GUEST_USER = 'AirAsia Site Guest User';
    private static final Map<String, String> langMap = new Map<String, String>();
    
    static {
        langMap.put('English (UK)', 'en_GB');
        langMap.put('Bahasa Indonesia', 'in');
        langMap.put('日本語', 'ja');
        langMap.put('한국어', 'ko');
        langMap.put('Bahasa Malaysia', 'ms');
        langMap.put('ไทย', 'th');
        langMap.put('Tiếng Việt', 'vi');
        langMap.put('中文（简体）', 'zh_CN');
        langMap.put('中文（繁體）', 'zh_TW');
    }
    
    @AuraEnabled
    public static LanguageInfo getLanguageInfo(String lang) {
        
        LanguageInfo langInfo = null;
        List<User> userList = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() AND Name != :AIR_ASIA_GUEST_USER LIMIT 1];
        if(userList != null && !userList.isEmpty()) {
            langInfo = new LanguageInfo();
            langInfo.userLanguage = String.isNotBlank(lang) ?  getLanguage(lang) : getLanguage(UserInfo.getLanguage());
            langInfo.languageSet = langMap.keySet();
        }
        
        return langInfo;
    }
    
    private static String getLanguage(String userLang) {
        String lang = 'English (UK)';
        
        if(String.isNotBlank(userLang)) {
            for(String key : langMap.keySet()) {
                if(userLang.equals(langMap.get(key))) {
                    lang = key;
                    break;
                }
            }
        }
        return lang;
    }
    
    @AuraEnabled
    public static String getUserLanguage(String selectedLang) {
        return langMap.containsKey(selectedLang) ? langMap.get(selectedLang) : 'en_GB';
    }
    
    public class LanguageInfo {
        @AuraEnabled
        public String userLanguage{set;get;}
        
        @AuraEnabled
        public Set<String> languageSet{set;get;}
    }
}