@isTest
public class LightningLanguagePickerControllerTest {

    @testSetup
    private static void setup() {
        
        UserRole userRoleOfficer = createUserRole('Officer', null);
        createUser('System Administrator', 'Officer', userRoleOfficer.Id);
    }
    
    private static UserRole createUserRole(String name, String parentRoleId) {
        UserRole userRoleObject = new UserRole();
        userRoleObject.DeveloperName = name.replace(' ', '_');
        userRoleObject.Name = name;
        userRoleObject.parentRoleId = parentRoleId;
        insert userRoleObject;
        return userRoleObject;
    }
    
    @future
    private static void createUser(String profileName, String lastName, Id roleId) {
        
        String ProfileId = [SELECT Id FROM Profile WHERE Name = : profileName].Id;
        
        User userObject = new User();
        userObject.ProfileId = ProfileId;
        userObject.LastName = lastName;
        userObject.Email = lastName + '@amamama.com';
        userObject.Username = lastName + '@amamama.com' + System.currentTimeMillis();
        userObject.CompanyName = 'TEST';
        userObject.Title = 'title';
        userObject.Alias = 'alias';
        userObject.TimeZoneSidKey = 'America/Los_Angeles';
        userObject.EmailEncodingKey = 'UTF-8';
        userObject.LanguageLocaleKey = 'en_US';
        userObject.LocaleSidKey = 'en_US';
        userObject.UserRoleId = roleId;
        insert userObject;
    }
    
    @isTest
    private static void testLightningLanguagePickerController() {
        User u = [SELECT Id FROM User WHERE lastname = 'Officer' LIMIT 1];
        
        Test.startTest();
        System.runAs(u) {
            LightningLanguagePickerController.LanguageInfo langInfo = LightningLanguagePickerController.getLanguageInfo('ms');
            System.assert(langInfo != null);
            System.assert(langInfo.userLanguage != null);
            System.assert(langInfo.userLanguage == 'Bahasa Malaysia');
           	System.assert(LightningLanguagePickerController.getUserLanguage('Bahasa Malaysia') == 'ms');
            System.assert(!langInfo.languageSet.isEmpty());
        }
        Test.stopTest();
    }
}