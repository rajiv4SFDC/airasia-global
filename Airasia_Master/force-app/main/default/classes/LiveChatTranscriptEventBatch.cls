global class LiveChatTranscriptEventBatch implements Database.Batchable<sObject>, Database.stateful {

  global List<Id> transcriptIdList;
  public Boolean chainAnotherbatch = false;
  public static Datetime createdDate = Test.isRunningTest() ? System.now() : System.now().addHours(-1);
  public static final String QUERY_COUNT = 'SELECT LiveChatTranscriptId FROM LiveChatTranscriptEvent WHERE CreatedDate <= :createdDate AND Type = \'Enqueue\' GROUP BY LiveChatTranscriptId HAVING COUNT(Name) > 1 LIMIT 500';
  public static final String QUERY_EVENT = 'SELECT Id, LiveChatTranscriptId FROM LiveChatTranscriptEvent WHERE Type = \'Enqueue\' AND LiveChatTranscriptId IN :transcriptIdList ORDER BY Time';

  global LiveChatTranscriptEventBatch(List<Id> transcriptIdList) {
    this.transcriptIdList = transcriptIdList;
  }

  global Database.QueryLocator start(Database.BatchableContext BC) {
    return Database.getQueryLocator(QUERY_EVENT);
  }

  global void execute(Database.BatchableContext BC, List<LiveChatTranscriptEvent> liveChatTranscriptEventList) {
    if(liveChatTranscriptEventList != null && !liveChatTranscriptEventList.isEmpty()) {
      chainAnotherbatch = true;
      Set<Id> transcriptIdSet = new Set<Id>();
      List<LiveChatTranscriptEvent> deleteTranscriptList = new List<LiveChatTranscriptEvent>();

      for(LiveChatTranscriptEvent event: liveChatTranscriptEventList) {
        if(transcriptIdSet.contains(event.LiveChatTranscriptId)) {
          deleteTranscriptList.add(event);
        } else {
          transcriptIdSet.add(event.LiveChatTranscriptId);
        }
      }
      if(!deleteTranscriptList.isEmpty()) {
        Database.delete(deleteTranscriptList, false);
        Database.emptyRecycleBin(deleteTranscriptList);
      }
    }
  }

  global void finish(Database.BatchableContext BC) {
    if(chainAnotherbatch) {
      chainAnotherbatch = false;
      executeEventBatch();
    }
  }

  public static void executeEventBatch() {
    List<AggregateResult> query = Database.query(LiveChatTranscriptEventBatch.QUERY_COUNT);

		List<Id> transcriptIdList = new List<Id>();
		for(AggregateResult result : query) {
			transcriptIdList.add((Id) result.get('LiveChatTranscriptId'));
		}

		LiveChatTranscriptEventBatch batchObj = new LiveChatTranscriptEventBatch(transcriptIdList);
		Database.executeBatch(batchObj);
  }
}