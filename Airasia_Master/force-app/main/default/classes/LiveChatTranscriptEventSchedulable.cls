global class LiveChatTranscriptEventSchedulable implements Schedulable {
	@ReadOnly
	global void execute(SchedulableContext sc) {
		 	LiveChatTranscriptEventBatch.executeEventBatch();
		}
}