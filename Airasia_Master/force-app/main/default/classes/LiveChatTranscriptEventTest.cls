@isTest
private class LiveChatTranscriptEventTest {
	 @isTest
	 private static void testLiveChatTranscriptEvent() {
			 Case caseObj = new Case();
			 caseObj.subject = 'Test';
			 caseObj.Quality_of_Service__c = 'Yes';
			 caseObj.Service_Recommend__c = 'Yes';
			 insert caseObj;

			 LiveChatVisitor visitor = new LiveChatVisitor();
			 insert visitor;

			 LiveChatTranscript script = new LiveChatTranscript();
			 script.CaseId = caseObj.Id;
			 script.LiveChatVisitorId = visitor.Id;
			 insert script;

			 LiveChatTranscriptEvent le = new LiveChatTranscriptEvent();
			 le.LiveChatTranscriptId = script.id;
			 le.type = 'Enqueue';
			 le.time = System.now();
			 insert le;

			 le = new LiveChatTranscriptEvent();
			 le.LiveChatTranscriptId = script.id;
			 le.type = 'Enqueue';
			 le.time = System.now();
			 insert le;

			 le = new LiveChatTranscriptEvent();
			 le.LiveChatTranscriptId = script.id;
			 le.type = 'Enqueue';
			 le.time = System.now();
			 insert le;

			 Test.startTest();
				Database.executeBatch(new CaseLanguageUpdateBatch(), 1);
				LiveChatTranscriptEventSchedulable es = new LiveChatTranscriptEventSchedulable();
				es.execute(null);

			 Test.stopTest();

			 List<LiveChatTranscriptEvent> listEvent = [Select Id from LiveChatTranscriptEvent WHERE LiveChatTranscriptId =:script.id];
			 System.assert(listEvent != null);
			 System.assert(listEvent.size() == 1);
	 }
}