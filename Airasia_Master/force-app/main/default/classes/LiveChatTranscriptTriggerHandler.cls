public class LiveChatTranscriptTriggerHandler extends TriggerHandler {
    
    private LiveChatTranscriptTriggerHelper helper = new LiveChatTranscriptTriggerHelper();
    
    public override void beforeInsert() {
        // Update Survery Questions
        helper.updateSurveyQuestions(DATA_LIST_NEW);
    }
}