public class LiveChatTranscriptTriggerHelper {
    
    public void updateSurveyQuestions(List<LiveChatTranscript> chatTranscriptList) {
        Set<Id> caseId = new Set<Id>();
        if(chatTranscriptList != null && !chatTranscriptList.isEmpty()) {
            for(LiveChatTranscript chatTranscript : chatTranscriptList) {
                caseId.add(chatTranscript.caseId);
            }
            
            Map<Id, Case> caseMap = new Map<Id, Case>([SELECT Id, Quality_of_Service__c, Service_Recommend__c FROM Case WHERE Id IN : caseId]);
            
            if(caseMap != null && !caseMap.isEmpty()) {
                for(LiveChatTranscript chatTranscript : chatTranscriptList) {
                    if(caseMap.containsKey(chatTranscript.caseId)) {
                        
                        System.debug('START');
                        System.debug('Quality_of_Service__c '+caseMap.get(chatTranscript.caseId).Quality_of_Service__c);
                        System.debug('Service_Recommend__c '+caseMap.get(chatTranscript.caseId).Service_Recommend__c);
                        chatTranscript.Quality_of_Service__c = caseMap.get(chatTranscript.caseId).Quality_of_Service__c;
                        chatTranscript.Service_Recommend__c = caseMap.get(chatTranscript.caseId).Service_Recommend__c;
                        System.debug('END');
                        
                        
                        
                    }
                }
            }         
        }
    }
}