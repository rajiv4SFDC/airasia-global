@isTest
public class LiveChatTranscriptTriggerTest {
    
   @isTest
    private static void testLiveChatTranscriptTrigger() {
        Case caseObj = new Case();
        caseObj.subject = 'Test';
        caseObj.Quality_of_Service__c = 'Yes';
        caseObj.Service_Recommend__c = 'Yes';
        insert caseObj;
        
        LiveChatVisitor visitor = new LiveChatVisitor();

        insert visitor;
        
        Test.startTest();
        LiveChatTranscript script = new LiveChatTranscript();
        script.CaseId = caseObj.Id;
        script.LiveChatVisitorId = visitor.Id;
        insert script;
        
        script = [SELECT Id, Quality_of_Service__c, Service_Recommend__c FROM LiveChatTranscript WHERE Id =: script.Id LIMIT 1];
        System.assert(script != null);
        Test.stopTest();
    }
}