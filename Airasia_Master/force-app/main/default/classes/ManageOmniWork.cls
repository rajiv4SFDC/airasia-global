public class ManageOmniWork {
	@AuraEnabled 
    public static user fetchUser(Id userId) {
        userId = String.isNotEmpty(userId) ? userId : userInfo.getUserId();
     // query current user information  
      	User oUser = [SELECT Live_Chat_Capacity__c, 
                    		Social_Capacity__c
                        FROM User 
                        WHERE id =: userId];
        return oUser;
    }
    
	@AuraEnabled 
    public static String fetchChatName(Id recordId) {
        String name = [SELECT Name
                       FROM LiveChatTranscript
                       WHERE Id=:recordId].Name;
        
        return name;
    }
    
    
	@AuraEnabled 
    public static String hasCaseId(Id recordId) {
        
        /*Boolean ownerIsQueue = [SELECT Owner_Is_Queue__c 
                       FROM LiveChatTranscript
                       WHERE Id=:recordId].Owner_Is_Queue__c;
        
        return (ownerIsQueue) ? 'Y' : 'N';*/
        String caseId = [SELECT CaseId 
                       FROM LiveChatTranscript
                       WHERE Id=:recordId].CaseId;
        
        return caseId;
    }
    
	@AuraEnabled 
    public static String fetchCaseName(Id recordId) {
        String name = [SELECT CaseNumber
                       FROM Case
                       WHERE Id=:recordId].CaseNumber;
        
        return name;
    }
}