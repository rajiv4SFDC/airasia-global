@isTest
public class ManageOmniWork_Test {
    
    @testSetup
    public static void testSetup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User();
        u.Live_Chat_Capacity__c = 2;
        u.Social_Capacity__c = 1;
        u.LastName = 'User';
        u.FirstName = 'Test';
        u.Username = 'TestUser@airasia.com';
        u.Email = 'TestUser@airasia.com';
        u.Alias = 'uTest';
        u.ProfileId = p.Id;
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.TimeZoneSidKey = 'Asia/Kuala_Lumpur';
        u.EmailEncodingKey = 'UTF-8';
        insert u;
        
        LiveChatVisitor  con = new LiveChatVisitor();
        
        insert con;
        
        con = [SELECT id FROM LiveChatVisitor];
        
        Case c = new Case();
        insert c;
        
        LiveChatTranscript lc = new LiveChatTranscript();
        lc.LiveChatVisitorId = con.Id;
        insert lc;
    }
    
    public static testMethod void testCapacity() {
        User u = [SELECT Id
                  FROM User
                  WHERE Username = 'TestUser@airasia.com'];
        u = ManageOmniWork.fetchUser(u.Id);
        
        System.assertEquals(2, u.Live_Chat_Capacity__c);
        System.assertEquals(1, u.Social_Capacity__c);
    }
    
    public static testMethod void testGetCaseNumber() {
        Case c = [SELECT Id, 
                  		 CaseNumber 
                  FROM Case];
        
        String caseNumber = ManageOmniWork.fetchCaseName(c.Id);
        System.assertEquals(c.CaseNumber, caseNumber);
        
    }
    
    public static testMethod void testGetChatName() {        
        LiveChatTranscript lc = [SELECT Id, 
                  		 Name 
                  FROM LiveChatTranscript];
        
        String lcName = ManageOmniWork.fetchChatName(lc.Id);
        System.assertEquals(lc.Name, lcName);
    }
}