/********************************************************************************************************
NAME:			MockCallout_Get_CCR
AUTHOR:			Aik Meng (aik.seow@salesforce.com)
PURPOSE:		Implement the HttpCalloutMock interface to simulate various Callout response scenarios
                for the Test Class.

UPDATE HISTORY:
DATE            AUTHOR			COMMENTS
-------------------------------------------------------------
19/10/2018      Aik Meng		First version
********************************************************************************************************/

public class MockCallout_Get_CCR implements HttpCalloutMock {

    String testScenario;  // SET by constructor to simulate various callout response
    
    // CONSTRUCTOR
    public MockCallout_Get_CCR(String testScenario) {
        this.testScenario = testScenario;
    }
    
    public HttpResponse respond(HttpRequest rqst) {

        HttpResponse hResp = new HttpResponse();
        
        // TEST DATA: sample JSON with 2 records
        String JSONString = '['+
        						'{'+
            						'"Conversion_Rate__c":0.007705100000000001,'+
            						'"Effective_Date__c":"2018-10-18",'+
            						'"Source_Currency_Code__c":"IDR",'+
            						'"Target_Currency_Code__c":"JPY"'+
            					'},'+
        							'{"Conversion_Rate__c":0.000095000000000003,'+
            						'"Effective_Date__c":"2018-10-18",'+
            						'"Source_Currency_Code__c":"IDR",'+
            						'"Target_Currency_Code__c":"SGD"'+
            					'}'+
            				']';
        
        //system.debug('testScenario: ' + this.testScenario);
        
        // SET the HTTP response based on test scenario
        if (this.testScenario.equalsIgnoreCase('CalloutSuccess')) {
            hResp.setStatusCode(200);
            hResp.setBody(JSONString);
        } else if (this.testScenario.equalsIgnoreCase('CalloutFail')) {
            hResp.setStatusCode(400);
            hResp.setStatus('Bad request');
        } else {
            hResp.setStatusCode(200);
            hResp.setBody('[]');  // No JSON record    
        }
        
        // RETURN HTTP Response
        return hResp;
        
    } // End - respond()
}