/*
Class Name : MockHttpResponseGenerator
Description: This is a MockTest class and this class is used in ChatContollerTest.cls

*/

@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        
        String response = '{'+
        '   \"messages\":['+
        '      {'+
        '         \"type\":\"Settings\",'+
        '         \"message\":{'+
        '            \"prefixKey\":\"5a9b4cfd00f1e628a238db2a197f2ba0e6d9e380\",'+
        '            \"contentServerUrl\":\"https://278e.la1-c1cs-hnd.salesforceliveagent.com/content\",'+
        '            \"pingRate\":50000.0,'+
        '            \"buttons\":['+
        '               {'+
        '                  \"estimatedWaitTime\":114,'+
        '                  \"language\":\"en_US\",'+
        '                  \"type\":\"Standard\",'+
        '                  \"id\":\"5739D00000000Hv\",'+
        '                  \"isAvailable\":true'+
        '               }'+
        '            ]'+
        '         }'+
        '      }'+
        '   ]'+
        '}';
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(response);
        res.setStatusCode(200);
        return res;
    }
}