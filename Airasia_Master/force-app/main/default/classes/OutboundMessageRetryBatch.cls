/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   This Batch class performs the retry of all the failed WS from Outbound_Message__c object
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    11/07/2017      Sharan Desai	    Created
    *******************************************************************/

global class OutboundMessageRetryBatch implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts{
    
    //-- Instance variables
    @TestVisible global Integer totalNoOfErrorRecords=0;
    @TestVisible global Integer totalNoOfReachedRetryLimit=0;
    @TestVisible global Integer totalNoOfRecordsWithMissingInterfaceNameMetadataSetup=0;
    @TestVisible global Integer totalNoOfSuccessRecords=0;
    @TestVisible global Integer totalNoOfrecords=0;
    @TestVisible global Integer totalNoOfretryLimitRecordsFailedtoUpdatesStatus=0;
    global String OUTBOUND_MESSAGE_FAILED_STATUS=OutboundMessageRetryConstants.FAILED_STATUS;
    global String SEPARATOR_CONSTANT='#@#';
    global String processLog = '';
    global DateTime startTime = DateTime.now();
    global System_Settings__mdt sysSettingMD;
    global  boolean throwUnitTestExceptions {set;get;}
    global  boolean throwUnitTestExceptionForRecordUpdate{set;get;}
    
    global Map<String,String> interfaceNamesAndIdWithMissingMetadataSetupMap  = new Map<String,String>();
    global Map<String,Decimal> interfaceAndNoOfRetryMap = new Map<String,Decimal>();
    
    //-- Batch constructor
    global OutboundMessageRetryBatch(Map<String,Decimal> interfaceAndNoOfRetryMap,System_Settings__mdt sysSettingMD){
        this.interfaceAndNoOfRetryMap = interfaceAndNoOfRetryMap;
        this.sysSettingMD=sysSettingMD;
    }
    
    //-- Get Database cursor for processing the records
    global Database.QueryLocator start(Database.BatchableContext bc){        
        
        String queryStatusCriteria ='Require Retry';
        String soqlQuery = 'SELECT Id,CreatedDate,Parent_Id__c,Type__c,Class_Name__c,Number_Of_Retry__c,Interface_Name__c,Object_Name__c,Reference_Id__c,Status__c FROM Outbound_Message__c WHERE Status__c = :queryStatusCriteria AND Parent_id__c = null ORDER BY CreatedDate ASC';
        
        //-- Perform SOQL Query Execution to get recordslist
        return Database.getQueryLocator(soqlQuery);            
    }
    
    
    global void execute(Database.BatchableContext bc,List<Outbound_Message__c> outboundMessageRecordList){
        
        totalNoOfrecords = totalNoOfrecords+outboundMessageRecordList.size(); 
        
        LIST<Outbound_Message__c> recordsTobeUpdatedWithFailedStatus = new LIST<Outbound_Message__c>();
        
        //-- If there are records to be processed
        if(outboundMessageRecordList!=null && outboundMessageRecordList.size()>0){
            
            //-- Iterate over each record to perform the retry
            for(Outbound_Message__c eachOutboundMessageRecord :  outboundMessageRecordList){
                
                try{
                    
                    OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();                    
                    //-- Get the Outboundmessage record details to prevalidate and proces further
                    obMsgRetryDOObj.recordId = eachOutboundMessageRecord.Id;
                    obMsgRetryDOObj.className = eachOutboundMessageRecord.Class_Name__c;
                    obMsgRetryDOObj.noOfretryAlreadyPerformed = eachOutboundMessageRecord.Number_Of_Retry__c;
                    obMsgRetryDOObj.interfaceName = eachOutboundMessageRecord.Interface_Name__c;
                    obMsgRetryDOObj.sObjectName = eachOutboundMessageRecord.Object_Name__c;
                    obMsgRetryDOObj.sObjectRecordID = eachOutboundMessageRecord.Reference_Id__c;
                    obMsgRetryDOObj.status = eachOutboundMessageRecord.Status__c;  
                    obMsgRetryDOObj.parentId = eachOutboundMessageRecord.Parent_Id__c;  
                    obMsgRetryDOObj.type = eachOutboundMessageRecord.Type__c;  
                    
                    //-- IF the Metadata setup is available for this Interface
                    if(interfaceAndNoOfRetryMap.containsKey(obMsgRetryDOObj.interfaceName)){                        
                        
                        //-- This block of code is for Unit Testing
                        if(Test.isRunningTest() && throwUnitTestExceptions){
                            obMsgRetryDOObj=null;
                        }
                        
                        if(obMsgRetryDOObj.noOfretryAlreadyPerformed==null){
                            obMsgRetryDOObj.noOfretryAlreadyPerformed=0;
                        }
                        
                        //-- IF retry value is within the Configured limit
                        if(obMsgRetryDOObj.noOfretryAlreadyPerformed < interfaceAndNoOfRetryMap.get(obMsgRetryDOObj.interfaceName)){
                                                        
                            System.enqueueJob(new OutboundMessageRetryQueueable(obMsgRetryDOObj,sysSettingMD));
                            totalNoOfSuccessRecords++;
                            
                            
                        }else{
                            totalNoOfReachedRetryLimit++;                            
                            eachOutboundMessageRecord.Status__c=OUTBOUND_MESSAGE_FAILED_STATUS;
                            recordsTobeUpdatedWithFailedStatus.add(eachOutboundMessageRecord);
                        }
                    }else{
                        
                        //-- Track the Interface names and record Ids for which Metadata setup is missing 
                        
                        //-- increment the counter for no of records not processed as part of this batch
                        totalNoOfRecordsWithMissingInterfaceNameMetadataSetup++;
                        
                        //-- Build Map to get details of records for which Interface configuration was missing in the Metadata
                        String mapValueString = obMsgRetryDOObj.recordId;                        
                        if(interfaceNamesAndIdWithMissingMetadataSetupMap.containsKey(obMsgRetryDOObj.interfaceName)){
                            mapValueString = mapValueString+SEPARATOR_CONSTANT+interfaceNamesAndIdWithMissingMetadataSetupMap.get(obMsgRetryDOObj.interfaceName);
                        }
                        interfaceNamesAndIdWithMissingMetadataSetupMap.put(obMsgRetryDOObj.interfaceName, mapValueString);
                        
                    }   
                    
                }catch(Exception OutboundMsgRetryException){            
                    ProcessLog += OutboundMsgRetryException.getMessage() + '\r\n';
                    totalNoOfErrorRecords++;
                }
            }                
        }
        
        try{
            //-- Update the records with Status as Failed for the records whose retry limit has reached the metadata config value
            if(recordsTobeUpdatedWithFailedStatus.size()>0){
                
                //-- This block of code is for Unit Testing
                if(Test.isRunningTest() && throwUnitTestExceptionForRecordUpdate){
                    recordsTobeUpdatedWithFailedStatus.get(0).id = null;
                }                
                update recordsTobeUpdatedWithFailedStatus;
            }            
        }catch(Exception recordsTobeUpdatedWithFailedStatusException){
            ProcessLog += recordsTobeUpdatedWithFailedStatusException.getMessage() + '\r\n';
            totalNoOfretryLimitRecordsFailedtoUpdatesStatus = totalNoOfretryLimitRecordsFailedtoUpdatesStatus+recordsTobeUpdatedWithFailedStatus.size();
        }                
    }
    
    global void finish(Database.BatchableContext bc) {
        System.debug('finish() - START');
        ProcessLog = ProcessLog + '################################################\r\n' +
            'Total No. Of Processed Records: ' + totalNoOfrecords + '\r\n' +
            'Total No. of Sucess Records: ' + totalNoOfSuccessRecords + '\r\n' +
            'Total No of Error Records: ' + totalNoOfErrorRecords + '\r\n' +
            'Total No of Records Missing InterfaceName Setup: '+totalNoOfRecordsWithMissingInterfaceNameMetadataSetup+'\r\n'+
            'Total No of retry Limit Reached Records: '+totalNoOfReachedRetryLimit+'\r\n'+
            'Total No of Retry Limit Reached Records Failed to update : '+totalNoOfretryLimitRecordsFailedtoUpdatesStatus+'\r\n'+
            '################################################\r\n';
        
        // query job details
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = : bc.getJobId()];
        ProcessLog += 'Job Id: ' + job.Id + '; Job Status: ' + job.Status + '; Total Job Items: ' + job.TotalJobItems + '\r\n';
        
        // add application log
        boolean isError = false;
        if (totalNoOfErrorRecords > 0 || totalNoOfRecordsWithMissingInterfaceNameMetadataSetup>0 ||
            totalNoOfretryLimitRecordsFailedtoUpdatesStatus>0) {
                isError = true;
            }
        insertApplicationLog(bc.getJobId(), isError);
        
        System.debug('finish() - END');
        
    } // END - finish
    
    
    private void insertApplicationLog(String jobId, boolean isError) {
        
        ApplicationLogWrapper appLogWrapper = new ApplicationLogWrapper();
        appLogWrapper.logLevel = 'Info';
        
        if (isError)
            appLogWrapper.logLevel = 'Error';
        
        appLogWrapper.sourceClass = 'OutboundMessageRetryBatch';
        appLogWrapper.sourceFunction = 'finish(Database.BatchableContext bc)';
        appLogWrapper.referenceInfo = 'Apex Batch: Job Id: ' + jobId;
        appLogWrapper.logMessage = processLog;
        appLogWrapper.applicationName = 'OutboundMessageRetryBatch';
        appLogWrapper.exceptionThrown = null;
        appLogWrapper.startDate = StartTime;
        appLogWrapper.endDate = DateTime.now();
        appLogWrapper.type = 'Job Log';
        
        // Set Log Level
        appLogWrapper.isDebug = sysSettingMD.Debug__c;
        appLogWrapper.isInfo = sysSettingMD.Info__c;
        appLogWrapper.isError = sysSettingMD.Error__c;
        appLogWrapper.isWarning = sysSettingMD.Warning__c;
        
        GlobalUtility.logMessage(appLogWrapper);
    }
}