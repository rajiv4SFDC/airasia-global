/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    21/07/2017      Sharan Desai	    Created

    20/08/2018      Charles Thompson    Updated to fix inherent flaw in
                                        code logic - two outbound messages
                                        cannot be sent at the same time, or 
                                        you will encounter a governor limit
                                        stopping you from chaining more than
                                        one job at a time in the Apex queue
*******************************************************************/

@isTest(SeeAllData=false) public class OutboundMessageRetryBatch_Test {
    
   /**
	* Scenario 1 : When OutboundMessage record's Interface and ClassName are valid
    * 
    * */
    @isTest public static void testBestCaseScenario(){
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
     
/*************************
 * This test will not work for more than one outbound message to be retried
 * -- CThompson 20 Aug 2018
 * 
        // create outbound Message record 
        Map < String, Object > obMsgFieldValueMap2 = new Map < String, Object > ();
        obMsgFieldValueMap2.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap2.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap2.put('Name', 'RPSInterface2');
        obMsgFieldValueMap2.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap2);
******************************/
        
        Map<String,Decimal> interfaceAndNoOfRetryMap = new Map<String,Decimal>();
        
        //-- STEP 1 : Query Metadata to build a Map with Key as InterfaceName and value as NoOfRetry
        // which is required for the batch processing
        List<System_Settings__mdt> systemSettingsMetadataList = [SELECT Interface_Name__c,Number_Of_Retry__c FROM System_Settings__mdt WHERE IsOutboundMessageRetry__c=true];
        if(systemSettingsMetadataList!=null && systemSettingsMetadataList.size()>0){
            for(System_Settings__mdt eachMetaDataObj : systemSettingsMetadataList){
                interfaceAndNoOfRetryMap.put(eachMetaDataObj.Interface_Name__c,eachMetaDataObj.Number_Of_Retry__c);   
            }    
        } 
        
        // get System Setting details for Logging
        System_Settings__mdt logLevelCMD  = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
        
        Test.startTest();        
        //-- Initiate batch process
        OutboundMessageRetryBatch batchInstance = new OutboundMessageRetryBatch(interfaceAndNoOfRetryMap,logLevelCMD);    
        batchInstance.throwUnitTestExceptions=false;
        batchInstance.throwUnitTestExceptionForRecordUpdate=false;
        Database.executeBatch(batchInstance,25);        
        Test.stopTest();
        
        //-- Check the batch no of records processing
        //System.assertEquals(obMsgObjList.size(),batchInstance.totalNoOfrecords);
        
    }
    
    /**
	* Scenario 2 : When OutboundMessage records ClassName is not  found and Metadata has no defination 
    * 
    * */
    @isTest public static void testNoInterfaceMappinghandlerFound(){
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler2');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface2');
        obMsgFieldValueMap.put('Name', 'RPSInterface2');
        obMsgFieldValueMap.put('Number_Of_Retry__c',0);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
         // create outbound Message record
        Map < String, Object > obMsgFieldValueMap1 = new Map < String, Object > ();
        obMsgFieldValueMap1.put('Class_Name__c', 'RPSInterfaceRetryHandler2');
        obMsgFieldValueMap1.put('Interface_Name__c', 'RPSInterface2');
        obMsgFieldValueMap1.put('Name', 'RPSInterface2');
        obMsgFieldValueMap1.put('Number_Of_Retry__c',0);
        obMsgFieldValueMap1.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap1);
        
        
        Map<String,Decimal> interfaceAndNoOfRetryMap = new Map<String,Decimal>();
        
        // STEP 1 : Query Metadata to build a Map with Key as InterfaceName and value as NoOfRetry
        // which is required for the batch processing
        List<System_Settings__mdt> systemSettingsMetadataList = [SELECT Interface_Name__c,Number_Of_Retry__c FROM System_Settings__mdt WHERE IsOutboundMessageRetry__c=true];
        if(systemSettingsMetadataList!=null && systemSettingsMetadataList.size()>0){
            for(System_Settings__mdt eachMetaDataObj : systemSettingsMetadataList){
                interfaceAndNoOfRetryMap.put(eachMetaDataObj.Interface_Name__c,eachMetaDataObj.Number_Of_Retry__c);   
            }    
        } 
        
        // get System Setting details for Logging
        System_Settings__mdt logLevelCMD  = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
        
        Test.startTest();        
        //-- Initiate batch process
        OutboundMessageRetryBatch batchInstance = new OutboundMessageRetryBatch(interfaceAndNoOfRetryMap,logLevelCMD);            
        batchInstance.throwUnitTestExceptions=false;
       batchInstance.throwUnitTestExceptionForRecordUpdate=false;
        Database.executeBatch(batchInstance,25);        
        Test.stopTest();
        
    }
    
    /**
	* Scenario 3 : When run time exception occurs
    * 
    * */
    @isTest public static void testRunTimeExceptionCase(){
        
               
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);        
        
        Map<String,Decimal> interfaceAndNoOfRetryMap = new Map<String,Decimal>();
        
        //-- STEP 1 : Query Metadata to build a Map with Key as InterfaceName and value as NoOfRetry
        // which is required for the batch processing
        List<System_Settings__mdt> systemSettingsMetadataList = [SELECT Interface_Name__c,Number_Of_Retry__c FROM System_Settings__mdt WHERE IsOutboundMessageRetry__c=true];
        if(systemSettingsMetadataList!=null && systemSettingsMetadataList.size()>0){
            for(System_Settings__mdt eachMetaDataObj : systemSettingsMetadataList){
                interfaceAndNoOfRetryMap.put(eachMetaDataObj.Interface_Name__c,eachMetaDataObj.Number_Of_Retry__c);   
            }    
        } 
        
        // get System Setting details for Logging
        System_Settings__mdt logLevelCMD  = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
        
        Test.startTest();        
        //-- Initiate batch process
        OutboundMessageRetryBatch batchInstance = new OutboundMessageRetryBatch(interfaceAndNoOfRetryMap,logLevelCMD);    
        batchInstance.throwUnitTestExceptions=true;
        batchInstance.throwUnitTestExceptionForRecordUpdate=false;
        Database.executeBatch(batchInstance,25);        
        Test.stopTest();
                
        //-- Check the batch no of records processing
        //System.assertEquals(obMsgObjList.size(),batchInstance.totalNoOfrecords);
        
        
    }
    
      
     /**
	* Scenario 4 : Exception case while updating records with Failed sttaus whose Retry has crossed the configured Limit
    * 
    * */
    @isTest public static void testEcxeptionForRecordUpdateWithFailedStatus(){
           
                  
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        Map<String,Decimal> interfaceAndNoOfRetryMap = new Map<String,Decimal>();
        
        //-- STEP 1 : Query Metadata to build a Map with Key as InterfaceName and value as NoOfRetry
        // which is required for the batch processing
        List<System_Settings__mdt> systemSettingsMetadataList = [SELECT Interface_Name__c,Number_Of_Retry__c FROM System_Settings__mdt WHERE IsOutboundMessageRetry__c=true];
        if(systemSettingsMetadataList!=null && systemSettingsMetadataList.size()>0){
            for(System_Settings__mdt eachMetaDataObj : systemSettingsMetadataList){
                interfaceAndNoOfRetryMap.put(eachMetaDataObj.Interface_Name__c,eachMetaDataObj.Number_Of_Retry__c);   
            }    
        } 
        
        // get System Setting details for Logging
        System_Settings__mdt logLevelCMD  = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
        
        Test.startTest();        
        //-- Initiate batch process
        OutboundMessageRetryBatch batchInstance = new OutboundMessageRetryBatch(interfaceAndNoOfRetryMap,logLevelCMD);    
        batchInstance.throwUnitTestExceptions=false;
        batchInstance.throwUnitTestExceptionForRecordUpdate=true;
        Database.executeBatch(batchInstance,25);        
        Test.stopTest();
        
        
        //-- Check the batch no of records processing
        //System.assertEquals(obMsgObjList.size(),batchInstance.totalNoOfrecords);
                
        
    }
    
    
    
}