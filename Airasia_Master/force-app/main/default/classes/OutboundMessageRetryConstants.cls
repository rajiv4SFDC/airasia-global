/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    19/07/2017     Sharan Desai	    Created
*******************************************************************/

global class OutboundMessageRetryConstants {

    global static final String SUCCESS_STATUS ='Success';    
    global static final String FAILED_STATUS ='Failed';    
    global  static final String REQUIRE_RETRY_STATUS ='Require Retry';    
    global  static final String PAYLOAD_CHUNK_MAP_KEY_PREFIX ='Payload#';    
    global  static final String OB_TYPE_BY_PAYLOAD ='By Payload';    
    global  static final String OB_TYPE_BY_REFERENCE ='By Reference';
       
}