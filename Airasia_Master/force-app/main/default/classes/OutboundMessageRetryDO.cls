/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    19/07/2017      Sharan Desai	    Created
*******************************************************************/

global class OutboundMessageRetryDO {
    
    global String recordId{get;set;}
    
    global String className{get;set;}
    
    global Decimal noOfretryAlreadyPerformed{get;set;}
    
    global String interfaceName{get;set;}
    
    global String sObjectName{get;set;}
    
    global String sObjectRecordID{get;set;}
    
    global String status{get;set;}
    
    global String parentId{get;set;}
    
    global String type{get;set;}
    
    
}