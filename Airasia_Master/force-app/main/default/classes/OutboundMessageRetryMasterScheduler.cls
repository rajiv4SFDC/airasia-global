global class OutboundMessageRetryMasterScheduler implements schedulable {
    
    //-- Variable for Application Logging
    global static List < ApplicationLogWrapper > appWrapperLogList = null;
    global System_Settings__mdt logLevelCMD ;
    global static String processLog = '';
    global static DateTime startTime = DateTime.now();
    
    global void execute(SchedulableContext context){
        
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        processLog='';
        
        try{
            
            // get System Setting details for Logging
            logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
            
            //-- declare variables
            String cronExp ='';
            String days =''; 
            String hoursExp='';
            
            //-- Query the settings from Metadata
            Outbound_Message_Retry__mdt obmsgRetrySettingObj = [SELECT Frequency_In_Hour__c,Preferred_End_Time__c,Preferred_Start_Time__c,Monday__c,Tuesday__c,Wednesday__c,Thursday__c,Friday__c,Saturday__c,Sunday__c FROM Outbound_Message_Retry__mdt WHERE DeveloperName='Outbound_Message_Retry' LIMIT 1];
            
            //-- form the Cron Expression for Days
            days = obmsgRetrySettingObj.Sunday__c==true?'SUN':days;
            days = obmsgRetrySettingObj.Monday__c==true?days.length()>0?days+',MON':'MON':days;
            days = obmsgRetrySettingObj.Tuesday__c==true?days.length()>0?days+',TUE':'TUE':days;
            days = obmsgRetrySettingObj.Wednesday__c==true?days.length()>0?days+',WED':'WED':days;
            days = obmsgRetrySettingObj.Thursday__c==true?days.length()>0?days+',THU':'THU':days;
            days = obmsgRetrySettingObj.Friday__c==true?days.length()>0?days+',FRI':'FRI':days;
            days = obmsgRetrySettingObj.Saturday__c==true?days.length()>0?days+',SAT':'SAT':days;
            
            //-- form the Cron Expression for hours
            Integer frequency = Integer.valueOf(obmsgRetrySettingObj.Frequency_In_Hour__c);
            Integer startTimeInt =  Integer.valueOf(obmsgRetrySettingObj.Preferred_Start_Time__c);
            Integer endTimeInt =  Integer.valueOf(obmsgRetrySettingObj.Preferred_End_Time__c);
            
            if((endTimeInt-startTimeInt) >= frequency){
                hoursExp =String.valueOf(startTimeInt);
                Integer currentIndex =startTimeInt+frequency;
                while(currentIndex <= endTimeInt){
                    hoursExp = hoursExp+','+String.valueOf(currentIndex);                    
                    currentIndex=currentIndex+frequency;                    
                }                
                cronExp = '0 0 '+hoursExp+' ? * '+days+' *';
            }
            
            System.debug('cronExp'+cronExp);
            
            String jobId = '';          
            if(String.isNotEmpty(cronExp)){
                
                //-- Schedule the batch scheduler
                jobId =  System.schedule('Outbound Message Retry', cronExp, new OutboundMessageRetryScheduler());
            }else{
                
                //-- Perform Application logging
                processLog += 'Failed to start OutboundMessageRetryScheduler Batch \r\n';
                processLog += 'No Outbound Messgae retry MetaData Setup Found \r\n';
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'OutboundMessageRetryMasterScheduler', 'execute',
                                                                     '', 'OutBound Message Retry Master Scheduler', processLog, null, 'Error Log',  startTime, logLevelCMD, null));
                
            }
        }catch(Exception scheduleException){
            //-- Perform Application logging
            processLog += 'Failed to start OutboundMessageRetryScheduler Batch \r\n';
            processLog += scheduleException.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'OutboundMessageRetryMasterScheduler', 'execute',
                                                                 '', 'OutBound Message Retry Master Scheduler', processLog, null, 'Error Log',  startTime, logLevelCMD, scheduleException));
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
                
    }
    
}