/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    21/07/2017      Sharan Desai	    Created
*******************************************************************/
@isTest(SeeAllData=false)
public class OutboundMessageRetryMasterScheduler_Test {
    
    @isTest public static void testScheduler(){
        
        Test.startTest();
        
        //--cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextScheduleTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        OutboundMessageRetryMasterScheduler obMsgRetryScdlr = new OutboundMessageRetryMasterScheduler();
        String jobId =  System.schedule('OutboundMessageRetryMasterScheduler' + String.valueOf(Datetime.now()), nextScheduleTime, obMsgRetryScdlr);
      
       // Get the information from the CronTrigger API object
        CronTrigger crnTriggerObj = [SELECT Id,State,StartTime,EndTime,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE ID=:jobId];
 
		// Verify the expressions are the same        
        System.assertEquals(crnTriggerObj.CronExpression,nextScheduleTime);
         
        
        Test.stopTest();
        
    }

}