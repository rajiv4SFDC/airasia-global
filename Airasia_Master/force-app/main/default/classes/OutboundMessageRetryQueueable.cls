/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    19/07/2017      Sharan Desai	    Created
*******************************************************************/

global class OutboundMessageRetryQueueable implements Queueable,Database.AllowsCallouts{
    
    global OutboundMessageRetryDO OutboundMessageRetryDO=null; 
    global String processLog = '';
    global DateTime startTime = DateTime.now();   
    global System_Settings__mdt sysSettingMD;
    global String OUTBOUND_MESSAGE_RETRY_STATUS_SUCCESS ='Success';
    global String OUTBOUND_MESSAGE_RETRY_STATUS_REQUIRE_RETRY ='Require Retry';
    global List < ApplicationLogWrapper > appWrapperLogList  = new List < ApplicationLogWrapper > ();
    global  boolean throwUnitTestExceptions {set;get;}
    
    
    //-- Constructor
    global  OutboundMessageRetryQueueable(OutboundMessageRetryDO OutboundMessageRetryDO,System_Settings__mdt sysSettingMD){        
        this.OutboundMessageRetryDO = OutboundMessageRetryDO;
        this.sysSettingMD=sysSettingMD;
    }
    

    global void execute(QueueableContext context) {
        
        try{            
            String className = OutboundMessageRetryDO.className;
            
            //-- CORE LOGIC
            Type classInstance = Type.forName(className);
            //-- This block of code is for Unit Testing
            if(Test.isRunningTest() && throwUnitTestExceptions){
                classInstance=null;
            }
            
            OutboundmessageRetryHandlerInterface handler = (OutboundmessageRetryHandlerInterface)classInstance.newInstance();
            handler.performRetryLogic(OutboundMessageRetryDO);
            
        }catch(Exception exe){
            //-- Perform Application logging
                processLog += 'Failed to Dynamic Class invoking from OutboundMessageRetryQueueable Class \r\n';
                processLog += exe.getMessage()+'\r\n';
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'OutboundMessageRetryQueueable', 'execute',
                                                                     '', 'OutBound Message Retry Scheduler', processLog, null, 'Error Log',  startTime, sysSettingMD, exe));
                
        }finally{
             if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
        
    }
    
}