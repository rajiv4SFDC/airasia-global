/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    24/07/2017      Sharan Desai	    Created
*******************************************************************/
@isTest(SeeAllData=false) public class OutboundMessageRetryQueueable_Test {

      /**
	* Scenario 1 : When OutboundMessage record's Interface and ClassName are valid
    * 
    * */
    @isTest public static void testBestCaseScenario(){
        
         // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);

        Outbound_Message__c eachOutboundMessageRecord = (Outbound_Message__c)TestUtility.getOutboundMessage();
        
        OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();        
        //-- Get the Outboundmessage record details to prevalidate and proces further
        obMsgRetryDOObj.recordId = eachOutboundMessageRecord.Id;
        obMsgRetryDOObj.className = 'RPSInterfaceRetryHandler';
        obMsgRetryDOObj.noOfretryAlreadyPerformed = 7;
        obMsgRetryDOObj.interfaceName = 'RPSInterface';
        obMsgRetryDOObj.sObjectName = '';
        obMsgRetryDOObj.sObjectRecordID = '';
        obMsgRetryDOObj.status = 'Require Retry';
          
           // get System Setting details for Logging
        System_Settings__mdt sysSettingMD  = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
        OutboundMessageRetryQueueable obMsgretryqueinstance = new OutboundMessageRetryQueueable(obMsgRetryDOObj,sysSettingMD);
           obMsgretryqueinstance.throwUnitTestExceptions=false;
            
        Test.startTest();
        System.enqueueJob(obMsgretryqueinstance);
        Test.stopTest();
        
    }
    
       /**
	* Scenario 2 : Run Time EXCEPTION CASE
    * 
    * */
    @isTest public static void testExceptionCase(){
        
         // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);

        Outbound_Message__c eachOutboundMessageRecord = (Outbound_Message__c)TestUtility.getOutboundMessage();
        
        OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();        
        //-- Get the Outboundmessage record details to prevalidate and proces further
        obMsgRetryDOObj.recordId = eachOutboundMessageRecord.Id;
        obMsgRetryDOObj.className = 'RPSInterfaceRetryHandler';
        obMsgRetryDOObj.noOfretryAlreadyPerformed = 7;
        obMsgRetryDOObj.interfaceName = 'RPSInterface';
        obMsgRetryDOObj.sObjectName = '';
        obMsgRetryDOObj.sObjectRecordID = '';
        obMsgRetryDOObj.status = 'Require Retry';
          
        // get System Setting details for Logging
        System_Settings__mdt sysSettingMD  = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
        
        OutboundMessageRetryQueueable obMsgretryqueinstance = new OutboundMessageRetryQueueable(obMsgRetryDOObj,sysSettingMD);
           obMsgretryqueinstance.throwUnitTestExceptions=true;
            
        Test.startTest();
        System.enqueueJob(obMsgretryqueinstance);
        Test.stopTest();
        
    }
    
}