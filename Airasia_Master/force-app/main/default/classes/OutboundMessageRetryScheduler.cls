/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    18/07/2017      Sharan Desai	    Created
*******************************************************************/

global class OutboundMessageRetryScheduler implements Schedulable {
    
    //-- Variable for Application Logging
    global static List < ApplicationLogWrapper > appWrapperLogList = null;
    global System_Settings__mdt logLevelCMD ;
    global static String processLog = '';
    global static DateTime startTime = DateTime.now();
    global  boolean throwUnitTestExceptions {set;get;}
    global  boolean throwUnitTestException {set;get;}
    
    //-- Interface name retry configuration input map for retry Batch
    global Map<String,Decimal> interfaceAndNoOfRetryMap = new Map<String,Decimal>();
    
    global void execute(SchedulableContext ctx){
        
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        processLog='';
        try{
            
            // get System Setting details for Logging
            logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
            
            //-- STEP 1 : Query Metadata to build a Map with Key as InterfaceName and value as NoOfRetry
            // which is required for the batch processing
            List<System_Settings__mdt> systemSettingsMetadataList = [SELECT Interface_Name__c,Number_Of_Retry__c FROM System_Settings__mdt WHERE IsOutboundMessageRetry__c=true];
            if(systemSettingsMetadataList!=null && systemSettingsMetadataList.size()>0){
                for(System_Settings__mdt eachMetaDataObj : systemSettingsMetadataList){
                    interfaceAndNoOfRetryMap.put(eachMetaDataObj.Interface_Name__c,eachMetaDataObj.Number_Of_Retry__c);   
                }    
            } 
            
            //-- Code block for unit test case
            if (Test.isRunningtest() && throwUnitTestExceptions) {
                interfaceAndNoOfRetryMap=null;
            }   
            
            if(interfaceAndNoOfRetryMap!=null && interfaceAndNoOfRetryMap.size()>0){
                
                //Default Queue Size
                Integer configuredBatchSize=1;  
                /**List<System_Settings__mdt> batchSizeMetadataList = [SELECT Batch_Size__c FROM System_Settings__mdt WHERE MasterLabel='OutboundMessageRetryBatch'];
                
                System.debug('batchSizeMetadataList'+batchSizeMetadataList);
                
                //-- If no batch size configuration found then use default value
                if(batchSizeMetadataList!=null && batchSizeMetadataList.size()>0){
                    
                    //-- Code block for unit test case
                    if (Test.isRunningtest() && throwUnitTestException) {
                        batchSizeMetadataList=null;
                    }                    
                    configuredBatchSize = batchSizeMetadataList.get(0).Batch_Size__c!=null?batchSizeMetadataList.get(0).Batch_Size__c.intValue():0;
                }**/
                
                //-- Initiate batch process
                OutboundMessageRetryBatch batchInstance = new OutboundMessageRetryBatch(interfaceAndNoOfRetryMap,logLevelCMD);            
                
                if(Test.isRunningTest()){
                    configuredBatchSize=200;                    
                }
                Database.executeBatch(batchInstance,configuredBatchSize);
                
            }else{
                //-- Perform Application logging
                processLog += 'Failed to start OutboundMessageRetryScheduler Batch \r\n';
                processLog += 'No Outbound Messgae retry MetaData Setup Found \r\n';
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'OutboundMessageRetryScheduler', 'execute',
                                                                     '', 'OutBound Message Retry Scheduler', processLog, null, 'Error Log',  startTime, logLevelCMD, null));
                
                
            }
            
        }catch(Exception scheduleException){
            
             System.debug('scheduleException'+scheduleException);
            
            //-- Perform Application logging
            processLog += 'Failed to start OutboundMessageRetryScheduler Batch \r\n';
            processLog += scheduleException.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'OutboundMessageRetryScheduler', 'execute',
                                                                 '', 'OutBound Message Retry Scheduler', processLog, null, 'Error Log',  startTime, logLevelCMD, scheduleException));
           

            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
    }
    
    
}