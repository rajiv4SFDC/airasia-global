/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    21/07/2017      Sharan Desai	    Created
*******************************************************************/

@isTest(SeeAllData=false)
public class OutboundMessageRetryScheduler_Test {
    
     /**
     * Scenario 1 : When the class is scheduled succesfully
     * 
     * */
    @isTest public static void testScheduler(){
        
           // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
         // create outbound Message record
        Map < String, Object > obMsgFieldValueMap1 = new Map < String, Object > ();
        obMsgFieldValueMap1.put('Class_Name__c', 'RPSInterfaceRetryHandler2');
        obMsgFieldValueMap1.put('Interface_Name__c', 'RPSInterface2');
        obMsgFieldValueMap1.put('Name', 'RPSInterface2');
        obMsgFieldValueMap1.put('Number_Of_Retry__c',0);
        obMsgFieldValueMap1.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap1);
        
        Test.startTest();
        
        //--cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextScheduleTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        OutboundMessageRetryScheduler obMsgRetryScdlr = new OutboundMessageRetryScheduler();
        obMsgRetryScdlr.throwUnitTestExceptions=false;
        obMsgRetryScdlr.throwUnitTestException=false;
        String jobId =  System.schedule('Outbound Message retry' + String.valueOf(Datetime.now()), nextScheduleTime, obMsgRetryScdlr);
      
       // Get the information from the CronTrigger API object
        CronTrigger crnTriggerObj = [SELECT Id,State,StartTime,EndTime,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE ID=:jobId];
 
		// Verify the expressions are the same        
        System.assertEquals(crnTriggerObj.CronExpression,nextScheduleTime);
         
        
        Test.stopTest();
        
    }
    
    /**
     * Scenario 2 : When there is no Metadata setup found for atleast one Interface with its retry limit
     * 
     * */
    @isTest public static void testNoMetadataSetupFoundForInterfaceRetryConfiguration(){
        
         // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
         // create outbound Message record
        Map < String, Object > obMsgFieldValueMap1 = new Map < String, Object > ();
        obMsgFieldValueMap1.put('Class_Name__c', 'RPSInterfaceRetryHandler2');
        obMsgFieldValueMap1.put('Interface_Name__c', 'RPSInterface2');
        obMsgFieldValueMap1.put('Name', 'RPSInterface2');
        obMsgFieldValueMap1.put('Number_Of_Retry__c',0);
        obMsgFieldValueMap1.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap1);
        
        Test.startTest();
        
        
        //-- cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextScheduleTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        OutboundMessageRetryScheduler obMsgRetryScdlr = new OutboundMessageRetryScheduler();
        obMsgRetryScdlr.throwUnitTestExceptions=true;
        obMsgRetryScdlr.throwUnitTestException=false;
        String jobId =   System.schedule('Outbound Message retry' + String.valueOf(Datetime.now()), nextScheduleTime, obMsgRetryScdlr);
        
        // Get the information from the CronTrigger API object
        CronTrigger crnTriggerObj = [SELECT Id,State,StartTime,EndTime,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE ID=:jobId];
 
		// Verify the expressions are the same        
        System.assertEquals(crnTriggerObj.CronExpression,nextScheduleTime);
        
         System.assertEquals(0, crnTriggerObj.TimesTriggered);
        
        
        Test.stopTest();
        
    }
    
    /**
     * Scenario 3 : When there is any runtime exception during during the process
     * 
     * */
    @isTest public static void testRuntimeExceptionCase(){
        
          // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
         // create outbound Message record
        Map < String, Object > obMsgFieldValueMap1 = new Map < String, Object > ();
        obMsgFieldValueMap1.put('Class_Name__c', 'RPSInterfaceRetryHandler2');
        obMsgFieldValueMap1.put('Interface_Name__c', 'RPSInterface2');
        obMsgFieldValueMap1.put('Name', 'RPSInterface2');
        obMsgFieldValueMap1.put('Number_Of_Retry__c',0);
        obMsgFieldValueMap1.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap1);
        
        Test.startTest();
        
        Integer preLogCount = [SELECT COUNT() FROM Application_Log__c];
        System.debug('preLogCount-------'+preLogCount);
        
        //-- cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextScheduleTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        OutboundMessageRetryScheduler obMsgRetryScdlr = new OutboundMessageRetryScheduler();
        obMsgRetryScdlr.throwUnitTestExceptions=false;
        obMsgRetryScdlr.throwUnitTestException=true;
        String jobId = System.schedule('Outbound Message retry' + String.valueOf(Datetime.now()), nextScheduleTime, obMsgRetryScdlr);
        
        // Get the information from the CronTrigger API object
        CronTrigger crnTriggerObj = [SELECT Id,State,StartTime,EndTime,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE ID=:jobId];

		// Verify the expressions are the same        
        System.assertEquals(crnTriggerObj.CronExpression,nextScheduleTime);  
        
        System.assertEquals(0, crnTriggerObj.TimesTriggered);
        
         Integer postLogCount = [SELECT COUNT() FROM Application_Log__c];
        System.debug('postLogCount-------'+postLogCount);
        
        Test.stopTest();
        
    }
}