/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com
Description:   This Class holds all the utility methods required for the OutboundMessage Retry Framework

History:
Date            Author              Comments
-------------------------------------------------------------
19/07/2017      Sharan Desai	    Created
*******************************************************************/


global class OutboundMessageRetryUtility {    
    
    /**
* This method updates the Outcome status post the retry operation
**/ 
    global static void updateOutboundMessageretryStatus(OutboundMessageRetryDO OutboundMessageRetryDO,String Status){
        
        String processLog = '';
        DateTime startTime = DateTime.now();   
        System_Settings__mdt logLevelCMD;
        List < ApplicationLogWrapper > appWrapperLogList  = new List < ApplicationLogWrapper > ();
        try{
            
            // get System Setting details for Logging
            logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
            
            //-- update the record with retry outcome
            Outbound_Message__c parentOutboundMessageObj = new Outbound_Message__c(Id=OutboundMessageRetryDO.recordId);
            
            //-- change the status of child records as well
            LIST<Outbound_Message__c> outboundMsgList =  [SELECT ID FROM Outbound_Message__c WHERE Parent_Id__c=:OutboundMessageRetryDO.recordId];
            
            if(outboundMsgList==null || outboundMsgList.size()<=0){
                outboundMsgList = new LIST<Outbound_Message__c>();
            }
            outboundMsgList.add(parentOutboundMessageObj);
            
            
            //-- update the status for both parent and child records
            for(Outbound_Message__c outboundMessageObj : outboundMsgList){
                
                if(Status.equalsIgnoreCase(OutboundMessageRetryConstants.SUCCESS_STATUS)){
                    outboundMessageObj.Status__c=OutboundMessageRetryConstants.SUCCESS_STATUS;
                    outboundMessageObj.Number_Of_Retry__c=OutboundMessageRetryDO.noOfretryAlreadyPerformed+1;
                }else if(Status.equalsIgnoreCase(OutboundMessageRetryConstants.REQUIRE_RETRY_STATUS)){
                    outboundMessageObj.Status__c=OutboundMessageRetryConstants.REQUIRE_RETRY_STATUS;
                    outboundMessageObj.Number_Of_Retry__c=OutboundMessageRetryDO.noOfretryAlreadyPerformed+1;
                }else if(Status.equalsIgnoreCase(OutboundMessageRetryConstants.FAILED_STATUS)){
                    outboundMessageObj.Status__c=OutboundMessageRetryConstants.FAILED_STATUS;
                }
            }
            
            update outboundMsgList;
            
        }catch(Exception execption){
            //-- Perform Application logging
            processLog += 'Failed to update the Outbound Message retry operation of Interface :: '+OutboundMessageRetryDO.interfaceName+' \r\n';
            processLog += execption.getMessage()+'\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'OutboundMessageRetryUtility', 'updateOutboundMessageretryStatus',
                                                                 '', 'OutboundMessageRetryUtility', processLog, null, 'Error Log',  startTime, logLevelCMD, execption));
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
            
        }
    }
    
    global static Map < String, String > generatePayloadChunks(String payload){
        
        Integer LONG_TXT_AREA_MAX_SIZE = 131072;        
        Map < String, String > payloadMap = new Map < String, String > ();
        
        if(payload!=null && payload.length()>0){
            
            Integer remainder = Math.Mod(payload.length(), LONG_TXT_AREA_MAX_SIZE);
            Integer quotient = (payload.length()) / LONG_TXT_AREA_MAX_SIZE;
            
            if (quotient > 0) {               
                
                for (Integer i = 1; i <= quotient; i++) {
                    payloadMap.put(OutboundMessageRetryConstants.PAYLOAD_CHUNK_MAP_KEY_PREFIX+ i, payload.substring(LONG_TXT_AREA_MAX_SIZE *(i-1), LONG_TXT_AREA_MAX_SIZE * (i)));
                }
                if (remainder > 0) {
                    payloadMap.put(OutboundMessageRetryConstants.PAYLOAD_CHUNK_MAP_KEY_PREFIX+ (quotient+1), payload.substring(LONG_TXT_AREA_MAX_SIZE * quotient, (LONG_TXT_AREA_MAX_SIZE * quotient) + remainder));
                }
                
            }else{
                payloadMap.put(OutboundMessageRetryConstants.PAYLOAD_CHUNK_MAP_KEY_PREFIX+1,payload);
            }
        }else{
           payloadMap.put(OutboundMessageRetryConstants.PAYLOAD_CHUNK_MAP_KEY_PREFIX+1,payload);
            
        }
        
        return payloadMap;
    }
    
    global static String getCompletePayload(String recordId){
        
        String returnPaylod = '';
        LIST<Outbound_Message__c> outboundRecList =   [SELECT payload__c from Outbound_Message__c WHERE Id=:recordId OR parent_Id__c=:recordId ORDER BY Sequence__c ASC];
        
        if(outboundRecList!=null && outboundRecList.size()>0){
            for(Outbound_Message__c eachParentChildObj : outboundRecList){
                returnPaylod = returnPaylod+eachParentChildObj.Payload__c;                    
            }    
        }
        
        return returnPaylod;
    }
    
    /**
     * This utility method creates Outbound_Message__c records
     * 
     **/
    global static void createOutboundRecord(Map<String,String> recordIdAndPayloadMap, String sObjectName,String interfaceName,String className,String obType){
        
        String processLog = '';
        DateTime startTime = DateTime.now();   
        System_Settings__mdt logLevelCMD;
        List < ApplicationLogWrapper > appWrapperLogList  = new List < ApplicationLogWrapper > ();
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('OutboundMessage_Retry_Log_Level_CMD_NAME'));
        
        Map<String,Outbound_Message__c> parentInsertmap = new Map<String,Outbound_Message__c>();
        Map<String,List<Outbound_Message__c>> childInsertmap = new Map<String,List<Outbound_Message__c>>();
        Set<String> caseIds = recordIdAndPayloadMap.keySet();
        
        //-- check whether the records already exist in Outbound_Message__c object if YES then just reset the counter to zero
        Map<Id,Outbound_Message__c> existingOutboundRecords = new Map<Id,Outbound_Message__c> ([SELECT ID FROM Outbound_Message__c WHERE Status__c=:OutboundMessageRetryConstants.REQUIRE_RETRY_STATUS AND Interface_Name__c =:interfaceName AND Reference_Id__c IN :caseIds AND Parent_id__c = null]);
        
        for (String eachCaseId: caseIds) {
            
            if(!existingOutboundRecords.containsKey(eachCaseId)){
                
                String payload ='';
                
                //-- get Payload only if Type ='By Payload';
                if(obType.equals(OutboundMessageRetryConstants.OB_TYPE_BY_PAYLOAD)){                   
                    
                    payload = recordIdAndPayloadMap.get(eachCaseId);
                }
                
                //-- Call utility method to create payload chunks
                Map < String, String > payloadMap = OutboundMessageRetryUtility.generatePayloadChunks(payload); 
                
                //-- determine no of OB records to be created based on the Payload size
                Integer numberOfOutboundMsgToBeCreated = payloadMap.size();
                
                for (Integer i = 1; i <= numberOfOutboundMsgToBeCreated; i++) {
                    
                    Outbound_Message__c outBoundMsgRetry = new Outbound_Message__c();
                    outBoundMsgRetry.Name = interfaceName;
                    outBoundMsgRetry.Interface_Name__c = interfaceName;
                    outBoundMsgRetry.Object_Name__c = sObjectName;
                    outBoundMsgRetry.Reference_Id__c = eachCaseId;
                    outBoundMsgRetry.Class_Name__c = className;
                    outBoundMsgRetry.Status__c = OutboundMessageRetryConstants.REQUIRE_RETRY_STATUS;
                    outBoundMsgRetry.Number_Of_Retry__c=0;
                    if(obType.equals(OutboundMessageRetryConstants.OB_TYPE_BY_PAYLOAD)){
                        outBoundMsgRetry.Payload__c=payloadMap.containsKey(OutboundMessageRetryConstants.PAYLOAD_CHUNK_MAP_KEY_PREFIX+ i) ? payloadMap.get(OutboundMessageRetryConstants.PAYLOAD_CHUNK_MAP_KEY_PREFIX + i) : '';
                    }
                    outBoundMsgRetry.Type__c=OutboundMessageRetryConstants.OB_TYPE_BY_PAYLOAD;
                    outBoundMsgRetry.Sequence__c=i;
                    
                    if(i==1){
                        parentInsertmap.put(eachCaseId, outBoundMsgRetry);
                    }else{   
                        
                        List<Outbound_Message__c> childList = null;
                        if(childInsertmap.containsKey(eachCaseId)){
                            childList = childInsertmap.get(eachCaseId);                           
                        }else{
                            childList = new  List<Outbound_Message__c>();                                                  
                        }
                        childList.add(outBoundMsgRetry);
                        childInsertmap.put(eachCaseId, childList);
                    }                                        
                }                
            }else{
                
                Outbound_Message__c outBoundMsgRetry = existingOutboundRecords.get(eachCaseId);
                outBoundMsgRetry.Number_Of_Retry__c=0;
                parentInsertmap.put(eachCaseId, outBoundMsgRetry);
            }            
        }
        
        //insert outbounMsgList;
        if (!parentInsertmap.isEmpty()) {
            Database.UpsertResult[] insertURList = Database.upsert(parentInsertmap.values(), false);
            for (Integer i = 0; i < insertURList.size(); i++) {
                if (!insertURList[i].isSuccess()) {
                    for (Database.Error err: insertURList[i].getErrors()) {
                        processLog += '******* Unable to insert Parent Outbound_Message__c FOR Case ID: ' + parentInsertmap.values()[i].Reference_Id__c + '\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += 'Outbound_Message__c fields that affected this error: ' + err.getFields() + '\r\n';
                    }
                    
                    //-- remove the child records whose parent records failed while DML operation
                    if (!childInsertmap.isEmpty()) {
                        childInsertmap.remove(parentInsertmap.values()[i].Reference_Id__c);
                    }
                }
            }
        }
        
        //-- upsert child records if any
        if (!childInsertmap.isEmpty()) {
            
            LIST<Outbound_Message__c> childInsertList = new LIST<Outbound_Message__c>();
            for(String childKey : childInsertmap.keySet()){
                
                String parentOutboundmsgId = parentInsertmap.get(childKey).Id;
                if(parentOutboundmsgId!=null && parentOutboundmsgId.trim().length()>0){
                    
                    List<Outbound_Message__c> eachChildList =  childInsertmap.get(childKey);
                    for(Outbound_Message__c eachChild:eachChildList){
                        eachChild.Parent_Id__c=parentOutboundmsgId;
                        childInsertList.add(eachChild);
                    }
                }
            }            
            Database.UpsertResult[] childUpsertList = Database.upsert(childInsertList, false);
            for (Integer i = 0; i < childUpsertList.size(); i++) {
                if (!childUpsertList[i].isSuccess()) {
                    for (Database.Error err: childUpsertList[i].getErrors()) {
                        processLog += '******* Unable to insert Child Outbound_Message__c FOR Case ID: ' + childInsertList[i].Reference_Id__c + '\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += 'Outbound_Message__c fields that affected this error: ' + err.getFields() + '\r\n';
                    }                   
                }
            }            
        }
        
        if (!appWrapperLogList.isEmpty()){
            GlobalUtility.logMessage(appWrapperLogList);
        }
        
        //return null;
    }
    
    
}