/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    24/07/2017      Sharan Desai	    Created
*******************************************************************/

@isTest(SeeAlldata=false) public class OutboundMessageRetryUtility_Test {
    
    /**
* Scenario 1 : When OutboundMessage record is getting updated with Succes Status
* 
* */
    @isTest public static void testupdateSucessStatus(){
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        Outbound_Message__c eachOutboundMessageRecord = (Outbound_Message__c)TestUtility.getOutboundMessage();
        
        OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();        
        //-- Get the Outboundmessage record details to prevalidate and proces further
        obMsgRetryDOObj.recordId = eachOutboundMessageRecord.Id;
        obMsgRetryDOObj.className = 'RPSInterfaceRetryHandler';
        obMsgRetryDOObj.noOfretryAlreadyPerformed = 7;
        obMsgRetryDOObj.interfaceName = 'RPSInterface';
        obMsgRetryDOObj.sObjectName = '';
        obMsgRetryDOObj.sObjectRecordID = '';
        obMsgRetryDOObj.status = 'Require Retry';
        
        Test.startTest();
        
        
        
        Integer preLogCount = [SELECT COUNT() FROM Application_Log__c];
        OutboundMessageRetryUtility.updateOutboundMessageretryStatus(obMsgRetryDOObj,OutboundMessageRetryConstants.SUCCESS_STATUS);
        Integer postLogCount = [SELECT COUNT() FROM Application_Log__c];
        
        //-- Assert to check that On Exception case Application Log has been created
        System.assert(postLogCount==preLogCount);
        
        Test.stopTest();
        
    }
    
    /**
* Scenario 2 : When OutboundMessage record is getting updated with Failure Status
* 
* */
    @isTest public static void testupdateFaledStatus(){
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        Outbound_Message__c eachOutboundMessageRecord = (Outbound_Message__c)TestUtility.getOutboundMessage();
        
        OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();        
        //-- Get the Outboundmessage record details to prevalidate and proces further
        obMsgRetryDOObj.recordId = eachOutboundMessageRecord.Id;
        obMsgRetryDOObj.className = 'RPSInterfaceRetryHandler';
        obMsgRetryDOObj.noOfretryAlreadyPerformed = 7;
        obMsgRetryDOObj.interfaceName = 'RPSInterface';
        obMsgRetryDOObj.sObjectName = '';
        obMsgRetryDOObj.sObjectRecordID = '';
        obMsgRetryDOObj.status = 'Require Retry';
        
        Test.startTest();
        
        
        
        Integer preLogCount = [SELECT COUNT() FROM Application_Log__c];
        OutboundMessageRetryUtility.updateOutboundMessageretryStatus(obMsgRetryDOObj,OutboundMessageRetryConstants.FAILED_STATUS);
        Integer postLogCount = [SELECT COUNT() FROM Application_Log__c];
        
        //-- Assert to check that On Exception case Application Log has been created
        System.assert(postLogCount==preLogCount);
        
        
        Test.stopTest();
        
    }
    
    /**
* Scenario 3 : When OutboundMessage record is getting updated with REQUIRE_RETRY Status
* 
* */
    @isTest public static void testupdateRequireRetryStatus(){
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        Outbound_Message__c eachOutboundMessageRecord = (Outbound_Message__c)TestUtility.getOutboundMessage();
        
        OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();        
        //-- Get the Outboundmessage record details to prevalidate and proces further
        obMsgRetryDOObj.recordId = eachOutboundMessageRecord.Id;
        obMsgRetryDOObj.className = 'RPSInterfaceRetryHandler';
        obMsgRetryDOObj.noOfretryAlreadyPerformed = 7;
        obMsgRetryDOObj.interfaceName = 'RPSInterface';
        obMsgRetryDOObj.sObjectName = '';
        obMsgRetryDOObj.sObjectRecordID = '';
        obMsgRetryDOObj.status = 'Require Retry';
        
        Test.startTest();
        
        
        Integer preLogCount = [SELECT COUNT() FROM Application_Log__c];
        OutboundMessageRetryUtility.updateOutboundMessageretryStatus(obMsgRetryDOObj, OutboundMessageRetryConstants.REQUIRE_RETRY_STATUS);
        Integer postLogCount = [SELECT COUNT() FROM Application_Log__c];
        
        //-- Assert to check that On Exception case Application Log has been created
        System.assert(postLogCount==preLogCount);
        
        
        Test.stopTest();
        
    }
    
    /**
* Scenario 4 : exception case
* 
* */
    @isTest public static void testExceptionCase(){
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c',7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry'); 
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        Outbound_Message__c eachOutboundMessageRecord = (Outbound_Message__c)TestUtility.getOutboundMessage();
        
        OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();        
        //-- Get the Outboundmessage record details to prevalidate and proces further
        obMsgRetryDOObj.recordId = eachOutboundMessageRecord.Id;
        obMsgRetryDOObj.className = 'RPSInterfaceRetryHandler';
        obMsgRetryDOObj.noOfretryAlreadyPerformed = 7;
        obMsgRetryDOObj.interfaceName = 'RPSInterface';
        obMsgRetryDOObj.sObjectName = '';
        obMsgRetryDOObj.sObjectRecordID = '';
        obMsgRetryDOObj.status = 'Require Retry';
        
        Test.startTest();
        Integer preLogCount = [SELECT COUNT() FROM Application_Log__c];
        OutboundMessageRetryUtility.updateOutboundMessageretryStatus(obMsgRetryDOObj, null);
        Integer postLogCount = [SELECT COUNT() FROM Application_Log__c];
        
        //-- Assert to check that On Exception case Application Log has been created
        System.assert(postLogCount>preLogCount);     
        
        Test.stopTest();
        
    }
}