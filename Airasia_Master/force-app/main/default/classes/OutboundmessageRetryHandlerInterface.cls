/*******************************************************************
    Author:        Sharan Desai
    Email:         dsharan@crmit.com
    Description:   
    
    History:
    Date            Author              Comments
    -------------------------------------------------------------
    11/07/2017      Sharan Desai	    Created
*******************************************************************/

global interface OutboundmessageRetryHandlerInterface {
	
    //-- Interface retry Wrapper class to implement this method for their retry logic
    void performRetryLogic(OutboundMessageRetryDO OutboundMessageRetryDOLocal);
}