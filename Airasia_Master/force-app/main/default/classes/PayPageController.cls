/*
 * Description: WechatPayPageController
 * Details: Wechat payment page
 * Author: David
 * Company: Charket
 * Date: 2019/01/31
 */
public class PayPageController 
{   
    public PayPageController() 
    {
    }
    
    private static String code;
    private static final String appId = 'wxa435550227a7e95b';//wx243072c076a06ad9
    private static final String merchantId = '1223439201';// 1393939302

    public String OpenId { get; set; }

    @RemoteAction
    public static String pay(Decimal amount, String openId, String merchantOrderNumber)
    {
        Charket.WeChatPayment.ProductInfo productInfo = new Charket.WeChatPayment.ProductInfo('亚洲航空账单', null, null, null);

        Charket.WeChatPayment.PaymentRequest paymentRequest = new Charket.WeChatPayment.PaymentRequest();
        paymentRequest.OpenId = openId;
        paymentRequest.Amount = amount;
        paymentRequest.TradeType = Charket.WeChatPayment.TradeType.JSAPI;
        paymentRequest.PaymentRestriction = null;//Charket.WeChatPayment.PaymentRestriction.no_credit;
        paymentRequest.ProductInfo = productInfo;
        paymentRequest.MerchantOrderNumber = merchantOrderNumber;

        Charket.WeChatPayment wechatPayment = new Charket.WeChatPayment(appId, merchantId);
        if(test.isRunningtest())
        {
            return '';
        }
        Charket__WeChatPayment__c payment = WeChatPayment.createPayment(paymentRequest);

        return JSON.serialize(payment);
    }

    @RemoteAction
    public static String getJSApiConfig(String paymentId)
    {
        Charket.WeChatPayment wechatPayment = new Charket.WeChatPayment(appId, merchantId);
        return wechatPayment.generateJSApiConfig(paymentId);
    }

    @RemoteAction
    public static String queryPayment(String paymentId)
    {
        Charket.WeChatPayment wechatPayment = new Charket.WeChatPayment(appId, merchantId);
        if(test.isRunningtest())
        {
            return '';
        }
        Charket__WeChatPayment__c payment = WeChatPayment.queryPayment(paymentId);
        return payment.Charket__Status__c;
    }

    public PageReference init()
    {
        Charket__WeChatAccount__c wechatAccount = [select Id, Charket__SalesforceEndpoint__c
                 from Charket__WeChatAccount__c where Charket__AppId__c = :appId limit 1];                 
        code = ApexPages.currentPage().getParameters().get('code');
        System.debug('code: ' + code);
        if(String.isBlank(code))
        {
            String callback = wechatAccount.Charket__SalesforceEndpoint__c + ApexPages.currentPage().getUrl().replaceAll('(^/apex/)', '/');
            Charket.WeChatApiOAuth oauth = (new Charket.WeChatClient(wechatAccount.Id)).getOAuth();
            return oauth.initiate(callback, Charket.WeChatApiOAuth.Scope.base, '');
        }
        else
        {           
            OpenId = getOpenId(wechatAccount.Id);
            System.debug('OpenId: ' + OpenId);
        }
        return null;
    }

    public static String getOpenId(String weChatAccountId)
    {
        try
        {
            String wechatCode = ApexPages.currentPage().getParameters().get('code');
            if(String.isNotBlank(wechatCode) && String.isNotBlank(weChatAccountId))
            {
                Charket.WeChatClient client = new Charket.WeChatClient(weChatAccountId);
                Charket.WeChatApiOAuth oauth = client.getOAuth();               
                Charket.WeChatApiOAuth.AuthTokenResponse response = oauth.handleCallback(wechatCode, '');
                return response.OpenId;
            }
        }
        catch(Exception ex) {}
        return null;
    }
}