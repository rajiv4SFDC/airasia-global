@isTest
public class PayPageControllerTest
{
    @isTest
    static void test()
    {
        Charket__WeChatAccount__c wechatAccount = new Charket__WeChatAccount__c(
                Name = 'Test Account', Charket__Type__c = 'Service Account',
                Charket__WeChatOriginId__c = 'gh_e8c4857297b0', Charket__AppId__c = 'wxa435550227a7e95b'
            );
        insert wechatAccount;
         Charket__WeChatFollower__c follower = new Charket__WeChatFollower__c(Charket__OpenId__c = 'abcdefg123456',  Charket__WeChatAccount__c = wechatAccount.Id);
        insert follower; 
        Charket__WeChatMerchant__c merchant = new Charket__WeChatMerchant__c(Charket__CredentialName__c = 'Acme', Charket__Key__c = '122222', Charket__MerchantId__c = 'abcde12345');
        insert merchant;
        Charket__WeChatPayment__c payment = new Charket__WeChatPayment__c(Charket__Amount__c = 100, Charket__SettledAmount__c = 100, Charket__Status__c = 'New', Charket__WeChatFollower__c = follower.Id);
        insert payment;
        PayPageController crl = new PayPageController();
        PayPageController.pay(100, 'abcdefg123456', 'abcde123457');
        PayPageController.getJSApiConfig(payment.Id);
        PayPageController.queryPayment(payment.Id);
        PageReference testPage = new pageReference('/apex/test?code=122222');
        test.setCurrentPage(testPage);
        crl.init();
        PayPageController.getOpenId(wechatAccount.Id);
    }
}