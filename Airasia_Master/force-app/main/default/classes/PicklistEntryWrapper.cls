/*******************************************************************
Author:        Venkatesh Budi
Company:       Salesforce.com
Description:   Related to dependent picklist
Test Class:    

History:
Date            Author              Comments
-------------------------------------------------------------
08-06-2017      Venkatesh Budi        Created
*******************************************************************/
public class PicklistEntryWrapper{
    
    public PicklistEntryWrapper(){            
    }
    public String active {get;set;}
    public String defaultValue {get;set;}
    public String label {get;set;}
    public String value {get;set;}
    public String validFor {get;set;}
}