/*******************************************************************
Author:        Venkatesh Budi
Company:       Salesforce.com
Description:   Dependent picklist field values
Test Class:    

History:
Date            Author              Comments
-------------------------------------------------------------
08-06-2017      Venkatesh Budi        Created
*******************************************************************/
public class PicklistFieldController{    
    private static Map<String, Schema.SObjectType> globalDescribe;

    public static Map<String, Schema.SObjectType> getGlobalDescribe() {
        if(globalDescribe == null) {
            globalDescribe = Schema.getGlobalDescribe();
        }
        return globalDescribe;
    }
    
    @AuraEnabled  
    public static List<PicklistWrapper> getDependentOptionsImpl(string objApiName , string contrfieldApiName , string depfieldApiName){
        try {
            system.debug(objApiName + '##' + contrfieldApiName + '###' + depfieldApiName);
            
            String objectName = objApiName.toLowerCase();
            String controllingField = contrfieldApiName.toLowerCase();
            String dependentField = depfieldApiName.toLowerCase();
            
            List<PicklistWrapper> objResults = new List<PicklistWrapper>();
            //get the string to sobject global map
            Map<String,Schema.SObjectType> objGlobalMap = getGlobalDescribe();
            System.debug('objGlobalMap: '+objGlobalMap);
            System.debug('objectName: '+objectName);
            
            if (!getGlobalDescribe().containsKey(objectName)){
                System.debug('OBJNAME NOT FOUND --.> ' + objectName);
                return null;
            }
            
            Schema.SObjectType objType = getGlobalDescribe().get(objectName);
            System.debug('objType: '+objType);
            if (objType==null){
                return objResults;
            }
            Bitset bitSetObj = new Bitset();
            Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
            //Check if picklist values exist
            if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
                System.debug('FIELD NOT FOUND --.> ' + controllingField + ' OR ' + dependentField);
                return objResults;     
            }
            //System.debug(' objFieldMap.get(controllingField) is ' +  objFieldMap.get(controllingField));
            List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
            List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();
            PicklistWrapper picklistWrapperObj = null;
            List<Integer> controllingIndexes = new List<Integer>();
            for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){            
                Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
                picklistWrapperObj = new PicklistWrapper();
                picklistWrapperObj.label = ctrlentry.getLabel();
                picklistWrapperObj.value = ctrlentry.getValue();
                picklistWrapperObj.dependentList = new List<PicklistWrapper>();
                objResults.add(picklistWrapperObj);
                controllingIndexes.add(contrIndex);
            }
            List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
            List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper>();
            for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){            
                Schema.PicklistEntry depentry = depEntries[dependentIndex];
                objEntries.add(depentry);
            } 
            objJsonEntries = (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
            List<Integer> indexes;
            for (PicklistEntryWrapper objJson : objJsonEntries){
                if (objJson.validFor==null || objJson.validFor==''){
                    continue;
                }
                indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
    
                for (Integer idx : indexes){                
                    String contrLabel = contrEntries[idx].getLabel();
                    for(PicklistWrapper wrapper : objResults) {
                        if(wrapper.label.equals(contrLabel)) {
                            picklistWrapperObj = new PicklistWrapper();
                            picklistWrapperObj.label = objJson.label;
                            picklistWrapperObj.value = objJson.value;                       
                            wrapper.dependentList.add(picklistWrapperObj);
                            break;
                        }                    
                    }                
                }
            }
            objEntries = null;
            objJsonEntries = null;
            system.debug('objResults--->' + objResults);
            return objResults;
        } catch (Exception ex) {
            System.debug('Exception getDependentOptionsImpl: '+ex.getMessage());
            return new List<PicklistWrapper>();
        }
    }
    
    @AuraEnabled  
    public static List<PicklistWrapper> getPicklistVals(string objApiName , string fieldApiName){
        try {
            String objectName = objApiName.toLowerCase();
            String fieldName = fieldApiName.toLowerCase();
            
            List<PicklistWrapper> objResults = new List<PicklistWrapper>();
            //get the string to sobject global map
            Map<String,Schema.SObjectType> objGlobalMap = getGlobalDescribe();
            
            if (!getGlobalDescribe().containsKey(objectName)){
                System.debug('OBJNAME NOT FOUND --.> ' + objectName);
                return null;
            }
            
            Schema.SObjectType objType = getGlobalDescribe().get(objectName);
            if (objType==null){
                return objResults;
            }
            Bitset bitSetObj = new Bitset();
            Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
            //Check if picklist values exist
            if (!objFieldMap.containsKey(fieldName)){
                System.debug('FIELD NOT FOUND --.> ' + fieldName );
                return objResults;     
            }
            
            List<Schema.PicklistEntry> fldEntries = objFieldMap.get(fieldName).getDescribe().getPicklistValues();
            objFieldMap = null;
            List<Integer> controllingIndexes = new List<Integer>();
            PicklistWrapper picklistWrapperObj = null;
            for(Integer contrIndex=0; contrIndex<fldEntries.size(); contrIndex++){            
                Schema.PicklistEntry ctrlentry = fldEntries[contrIndex];
                picklistWrapperObj = new PicklistWrapper();
                picklistWrapperObj.label = ctrlentry.getLabel();
                picklistWrapperObj.value = ctrlentry.getValue();
                objResults.add(picklistWrapperObj);
            }
            system.debug('objResults--->' + objResults);
            return objResults;
        } catch (Exception ex) {
            System.debug('Exception getPicklistVals: '+ex.getMessage());
            return new List<PicklistWrapper>();
        }
    }
    
    public class PicklistWrapper {
        @AuraEnabled
        public String label {set;get;}
        @AuraEnabled
        public String value {set;get;}
        @AuraEnabled
        public List<PicklistWrapper> dependentList {set;get;}
    }
}