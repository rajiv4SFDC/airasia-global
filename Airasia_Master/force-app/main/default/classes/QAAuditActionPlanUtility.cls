/*********************************************************
 * Utility invocable method to update a QA Audit Action 
 * Plan based on the audit's non-compliant items
 * 
 * Called by flow "QA Audit Action Plan: Auto-create"
 * 
 * Sep 2019 - Charles Thompson - Salesforce Singapore
 * Initial release
 * 
 * ******************************************************/
public with sharing class QAAuditActionPlanUtility {
    
    @InvocableMethod (label='Update action items based on QA Audit outcomes'
                      description='Invoked by the flow, QA Audit Action Plan: Auto-create. Adds action plan item data based on the outcome of the related QA Audit')
    public static List<Task> setActionItems(List<Task> inActionPlans){
        List<Task> outActionPlans = new List<Task>();
        List<Id> inQAAuditIds = new List<Id>();
        
        // get all related QA Audits
        for (Task t: inActionPlans){
            inQAAuditIds.add(t.WhatId);
        }

        // get QA Audit data matching the above list
        Map<Id, QA_Audit__c> inQAAudits = new Map<Id, QA_Audit__c>
            ([SELECT Id,
                     Service_Agent__r.ManagerId,
                     Overall_Recommendations__c,
                     X1_1__c, S1_1_Non_compliant__c,
                     X1_2__c, S1_2_Non_compliant__c,
                     X1_3__c, S1_3_Non_compliant__c,
                     X1_4__c, S1_4_Non_compliant__c,
                     X1_5__c, S1_5_Non_compliant__c,
                     X2_1__c, S2_1_Non_compliant__c,
                     X2_2__c, S2_2_Non_compliant__c,
                     X2_3__c, S2_3_Non_compliant__c,
                     X2_4__c, S2_4_Non_compliant__c,
                     X2_5__c, S2_5_Non_compliant__c,
                     X2_6__c, S2_6_Non_compliant__c,
                     X2_7__c, S2_7_Non_compliant__c,
                     X2_8__c, S2_8_Non_compliant__c,
                     X2_9__c, S2_9_Non_compliant__c,
                     X2_10__c, S2_10_Non_compliant__c,
                     X3_1_SkySpeed__c, S3_1_Non_compliant__c,
                     X3_2_SkySpeed__c, S3_2_Non_compliant__c,
                     X3_3_SkySpeed__c, S3_3_Non_compliant__c,
                     X3_4_SkySpeed__c, S3_4_Non_compliant__c,
                     X3_5_Salesforce__c, S3_5_Non_compliant__c,
                     X3_6_Salesforce__c, S3_6_Non_compliant__c,
                     X3_7_Salesforce__c, S3_7_Non_compliant__c,
                     X3_8_Salesforce__c, S3_8_Non_compliant__c,
                     X3_9_Salesforce__c, S3_9_Non_compliant__c
              FROM   QA_Audit__c
              WHERE  Id in :inQAAuditIds
             ]);
                
        // For each action plan, get the non-compliant items
        // and populate the action item
        Integer i = 0; // to count the action items required
        String[] v = new String[24]; // 24 questions in the audit
        String comments;
        
        for (Task t: inActionPlans){
            QA_Audit__c qa = inQAAudits.get(t.WhatId); // the related audit
            
            if (!String.isBlank(qa.Overall_Recommendations__c)){
                comments = qa.Overall_Recommendations__c + 
                           '\r\n\r\n';                
            }
            comments += 'Please accomplish an action plan task for the ' +
                        'following non-compliant audit items:\r\n'; 
            
            // check each audit question for compliance
            // if non-compliant, record it in our arrays
            if (qa.S1_1_Non_compliant__c){v[i] = '1.1: ' + qa.X1_1__c; i++;}
            if (qa.S1_2_Non_compliant__c){v[i] = '1.2: ' + qa.X1_2__c; i++;}
            if (qa.S1_3_Non_compliant__c){v[i] = '1.3: ' + qa.X1_3__c; i++;}
            if (qa.S1_4_Non_compliant__c){v[i] = '1.4: ' + qa.X1_4__c; i++;}
            if (qa.S1_5_Non_compliant__c){v[i] = '1.5: ' + qa.X1_5__c; i++;}
            if (qa.S2_1_Non_compliant__c){v[i] = '2.1: ' + qa.X2_1__c; i++;}
            if (qa.S2_2_Non_compliant__c){v[i] = '2.2: ' + qa.X2_2__c; i++;}
            if (qa.S2_3_Non_compliant__c){v[i] = '2.3: ' + qa.X2_3__c; i++;}
            if (qa.S2_4_Non_compliant__c){v[i] = '2.4: ' + qa.X2_4__c; i++;}
            if (qa.S2_5_Non_compliant__c){v[i] = '2.5: ' + qa.X2_5__c; i++;}
            if (qa.S2_6_Non_compliant__c){v[i] = '2.6: ' + qa.X2_6__c; i++;}
            if (qa.S2_7_Non_compliant__c){v[i] = '2.7: ' + qa.X2_7__c; i++;}
            if (qa.S2_8_Non_compliant__c){v[i] = '2.8: ' + qa.X2_8__c; i++;}
            if (qa.S2_9_Non_compliant__c){v[i] = '2.9: ' + qa.X2_9__c; i++;}
            if (qa.S2_10_Non_compliant__c){v[i] = '2.10: ' + qa.X2_10__c; i++;}
            if (qa.S3_1_Non_compliant__c){v[i] = '3.1: ' + qa.X3_1_SkySpeed__c; i++;}
            if (qa.S3_2_Non_compliant__c){v[i] = '3.2: ' + qa.X3_2_SkySpeed__c; i++;}
            if (qa.S3_3_Non_compliant__c){v[i] = '3.3: ' + qa.X3_3_SkySpeed__c; i++;}
            if (qa.S3_4_Non_compliant__c){v[i] = '3.4: ' + qa.X3_4_SkySpeed__c; i++;}
            if (qa.S3_5_Non_compliant__c){v[i] = '3.5: ' + qa.X3_5_Salesforce__c; i++;}
            if (qa.S3_6_Non_compliant__c){v[i] = '3.6: ' + qa.X3_6_Salesforce__c; i++;}
            if (qa.S3_7_Non_compliant__c){v[i] = '3.7: ' + qa.X3_7_Salesforce__c; i++;}
            if (qa.S3_8_Non_compliant__c){v[i] = '3.8: ' + qa.X3_8_Salesforce__c; i++;}
            if (qa.S3_9_Non_compliant__c){v[i] = '3.9: ' + qa.X3_9_Salesforce__c; i++;}
            
            // the arrays now have only those questions that are non-compliant
            // 
            // for each of the first ten items in the array, create an action item
            for (i = 0; i <= 9; i++){
                switch on i {
                    when 0 {t.Question_1__c = v[i];}
                    when 1 {t.Question_2__c = v[i];}
                    when 2 {t.Question_3__c = v[i];}
                    when 3 {t.Question_4__c = v[i];}
                    when 4 {t.Question_5__c = v[i];}
                    when 5 {t.Question_6__c = v[i];}
                    when 6 {t.Question_7__c = v[i];}
                    when 7 {t.Question_8__c = v[i];}
                    when 8 {t.Question_9__c = v[i];}
                    when 9 {t.Question_10__c = v[i];}
                }
                if (!String.isBlank(v[i])){
                    comments += '- ' + v[i] + '\r\n';
                }
            }
            comments += '\r\nPlease take note of the due date for all action ' +
                        'tasks to be completed.';
            
            // assign the task to the manager
            t.OwnerId = qa.Service_Agent__r.ManagerId;
            
            // populate the task comments field
            t.Description = comments; 

            outActionPlans.add(t);
        } // inActionPlans loop
        
        update outActionPlans;
        return outActionPlans;
    } // method setActionItems
}