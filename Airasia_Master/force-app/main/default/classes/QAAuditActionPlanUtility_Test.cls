/*********************************************************
 * Test class for QAAuditActionPlanUtility invocable method to 
 * update a QA Audit Action Plan based on the audit's non-compliant items
 * 
 * Sep 2019 - Charles Thompson - Salesforce Singapore
 * Initial release
 * 
 * ******************************************************/
@isTest
public class QAAuditActionPlanUtility_Test {

    @isTest
    static void testIt(){
        // ------------   Sample Data  -----------------
        // Profile Ids
        Profile mgrProfile = [SELECT Id
                              FROM   Profile
                              WHERE  Name = 'AOC Customer Care Managers'
                             ];
        Profile saProfile  = [SELECT Id 
                              FROM   Profile
                              WHERE  Name = 'AOC Customer Care Officer'
                             ];
        
        // Action Plan Record Type Id
        RecordType apRecordType = [SELECT Id
                                   FROM   RecordType
                                   WHERE  Name = 'QA Audit Action Plan'
                                  ];

        // Manager
        User mgr = new User();
        mgr.FirstName = 'Bugs';
        mgr.LastName = 'Bunny';
        mgr.Username = 'bugs@bunny.com.aacsm';
        mgr.ProfileId = mgrProfile.Id;
        mgr.Email = 'test@test.com';
        mgr.Alias = 'bugs';
        mgr.TimeZoneSidKey = 'Asia/Kuala_Lumpur';
        mgr.LocaleSidKey = 'en_MY';
        mgr.EmailEncodingKey = 'UTF-8';
        mgr.LanguageLocaleKey = 'en_MY';
        insert mgr;
        
        // Service Agent
        User sa = new User();
        sa.FirstName = 'Daffy';
        sa.LastName = 'Duck';
        sa.ManagerId = mgr.Id;
        sa.Username = 'daffy@duck.com.aacsm';
        sa.ProfileId = saProfile.Id;
        sa.Email = 'test@test.com';
        sa.Alias = 'daffy';
        sa.TimeZoneSidKey = 'Asia/Kuala_Lumpur';
        sa.LocaleSidKey = 'en_MY';
        sa.EmailEncodingKey = 'UTF-8';
        sa.LanguageLocaleKey = 'en_MY';
        insert sa;
        
        // Auditor
        User aud = new User();
        aud.FirstName = 'Yosemite';
        aud.LastName = 'Sam';
        aud.Username = 'yosemite@sam.com.aacsm';
        aud.ProfileId = mgrProfile.Id;
        aud.Email = 'test@test.com';
        aud.Alias = 'sam';
        aud.TimeZoneSidKey = 'Asia/Kuala_Lumpur';
        aud.LocaleSidKey = 'en_MY';
        aud.EmailEncodingKey = 'UTF-8';
        aud.LanguageLocaleKey = 'en_MY';
        insert aud;
        
        // Case
        Map<String, Object> caseFieldValueMap = new Map<String, Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('OwnerId', sa.Id);
        caseFieldValueMap.put('Status', 'Closed');
        TestUtility.createCase(caseFieldValueMap);
        List<String> queryFieldList = new List<String>{'Case.Owner.Name'};
        List<Case> casesList = (List<Case>) TestUtility.getCases(queryFieldList);
        Case c = casesList[0];
        
        // QA Audit
        QA_Audit__c qa = new QA_Audit__c();
        qa.Service_Agent__c = sa.Id;
        qa.Auditor__c = aud.Id;
        qa.Audit_Date__c = System.today();
        qa.Case__c = c.Id;
        qa.Overall_Recommendations__c = 'This agent needs to go back to school.';
        qa.Status__c = 'Draft';
        qa.S1_1_Compliant__c = true;
        qa.S1_2_Non_compliant__c = true; // 1.2 non compliant
        qa.S1_3_NA__c = true;
        qa.S1_4_Non_compliant__c = true; // 1.4 non compliant
        qa.S1_5_Compliant__c = true;
        qa.S2_1_Compliant__c = true;
        qa.S2_2_NA__c = true;
        qa.S2_3_Non_compliant__c = true; // 2.3 non compliant
        qa.S2_4_Compliant__c = true;
        qa.S2_5_Compliant__c = true;
        qa.S2_6_Compliant__c = true;
        qa.S2_7_NA__c = true;
        qa.S2_8_Non_compliant__c = true; // 2.8 non compliant
        qa.S2_9_Compliant__c = true;
        qa.S2_10_NA__c = true;
        qa.S3_1_Compliant__c = true;
        qa.S3_2_Non_compliant__c = true; // 3.2 non compliant
        qa.S3_3_Compliant__c = true;
        qa.S3_4_Compliant__c = true;
        qa.S3_5_NA__c = true;
        qa.S3_6_Compliant__c = true;
        qa.S3_7_Compliant__c = true;
        qa.S3_8_NA__c = true;
        qa.S3_9_Compliant__c = true;
        insert qa;
        
        //----------   End Sample Data -------------------------
        
        // test the apex...
        // 
        qa.Status__c = 'In Progress with Manager';
        update qa;
        
        // Pretend we're the flow that calls the Apex class
        // (build a new action plan task)
        Task ap = new Task();
        ap.RecordTypeId = apRecordType.Id;
        ap.ActivityDate = System.today() + 14;
        ap.IsReminderSet = true;
        ap.ReminderDateTime = System.now() + 7;
        ap.Subject = 'QA Audit Action Plan for ' + sa.FirstName + ' ' + sa.LastName;
        ap.WhatId = qa.Id;
        insert ap;
        List<Task> actionPlans = new List<Task>();
        actionPlans.add(ap);
        
        // call the class being tested
        QAAuditActionPlanUtility.setActionItems(actionPlans); 
        
        // find the new action plan
        List<Task> a = [SELECT Id,
                               Question_1__c,
                               Question_2__c,
                               Question_3__c,
                               Question_4__c,
                               Question_5__c,
                               Question_6__c
                        FROM   Task
                        WHERE  WhatId = :qa.Id
                       ];
        
        // check results
        System.assert(a[0].Question_1__c.left(3).equals('1.2'), 'Question 1 not equal to 1.2');
        System.assert(a[0].Question_2__c.left(3).equals('1.4'), 'Question 2 not equal to 1.4');
        System.assert(a[0].Question_3__c.left(3).equals('2.3'), 'Question 3 not equal to 2.3');
        System.assert(a[0].Question_4__c.left(3).equals('2.8'), 'Question 4 not equal to 2.8');
        System.assert(a[0].Question_5__c.left(3).equals('3.2'), 'Question 5 not equal to 3.2');
        System.assert(String.isBlank(a[0].Question_6__c), 'Question 6 not null');
    }
}