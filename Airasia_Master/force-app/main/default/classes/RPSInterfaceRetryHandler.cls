/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
25/07/2017      Pawan Kumar	        Created
*******************************************************************/
public class RPSInterfaceRetryHandler implements OutboundmessageRetryHandlerInterface {
    static List < System_Settings__mdt > logLevelCMD;
    static DateTime startTime = DateTime.now();
    static String processLog = '';
    //static List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
    static Map < String, String > appSettings;
    
    static {
        appSettings = Utilities.getAppSettings();
    }
    
    public static void performRetryLogic(final OutboundMessageRetryDO outboundMsgDo) {
        
        List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
        logLevelCMD = Utilities.getLogSettings(appSettings.get('CASE_TRIGGER_SYS_SETTING_NAME'));
        System.debug('logLevelCMD:' + logLevelCMD);
        Set < String > caseIds;
        try {
            //-- Perform RPS WS Operations here
            if (String.isNotEmpty(outboundMsgDo.sObjectRecordID)) {
                caseIds = new Set < String > {
                    outboundMsgDo.sObjectRecordID
                        };
                            
                            // send to RPS
                            if (!caseIds.isEmpty()) {
                                RPSRestWsHandler.retryRPSRestWebService(caseIds,outboundMsgDo);
                            }
                
                
                
                //-- update  the ws operation outcome back to OutBoundMessage record 
                OutboundMessageRetryUtility.updateOutboundMessageretryStatus(outboundMsgDo, OutboundMessageRetryConstants.SUCCESS_STATUS);
            }
        } catch (Exception RPSInterfaceretryException) {
            System.debug(LoggingLevel.ERROR, RPSInterfaceretryException);
            
            processLog += 'Failed to RPS Retry for Case Id: \r\n';
            processLog += outboundMsgDo.sObjectRecordID + '\r\n';
            processLog += 'Root Cause: ' + RPSInterfaceretryException.getMessage() + '\r\n';
            
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'RPSInterfaceRetryHandler', 'performRetryLogic',
                                                                 '', appSettings.get('RPS_RETRY_APPLICATION_NAME'), processLog, null, 'Error Log',
                                                                 startTime, logLevelCMD[0], RPSInterfaceretryException));
            
            // Now check reached MAX retry
            String obRecordStatus=OutboundMessageRetryConstants.REQUIRE_RETRY_STATUS;
            Integer noOfMaxRetry = Integer.valueof(logLevelCMD[0].Number_Of_Retry__c);
            Integer noOfretryPerformedAlready = Integer.valueof(outboundMsgDo.noOfretryAlreadyPerformed);
            if (noOfMaxRetry == (noOfretryPerformedAlready + 1)) {
                if (!caseIds.isEmpty()) {
                    RPSRestWsHandler.updateCases(caseIds, appSettings.get('SEND_TO_RPS_FAILED'));
                    obRecordStatus=OutboundMessageRetryConstants.FAILED_STATUS;
                }
            }
            
            //-- update  the ws operation outcome back to OutBoundMessage record 
            OutboundMessageRetryUtility.updateOutboundMessageretryStatus(outboundMsgDo, obRecordStatus);
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
        } // finally
        
    } //performRetryLogic
    
} //RPSInterfaceRetryHandler