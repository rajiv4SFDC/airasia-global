/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author          Comments
-------------------------------------------------------------
25/07/2017      Pawan Kumar	    Created
25/10/2018		Aik Meng		Updated testRefundStatusValidated() to accommodate new Validation Rule
*******************************************************************/
@isTest(SeeAllData = false)
private class RPSInterfaceRetryHandler_Test {
    
    private static testMethod void testRPSRetryHandler() {
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'testRetry');
        
        // no trigger will be called
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c'
                };
                    
                    List < Case > createdCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, createdCaseList);
        System.assertEquals(1, createdCaseList.size());
        System.assertEquals('Test_Case_1', createdCaseList[0].Subject);
        System.assertEquals('testRetry', createdCaseList[0].Send_to_RPS__c);
        
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c', 1);
        obMsgFieldValueMap.put('Status__c', 'Require Retry');
        obMsgFieldValueMap.put('Reference_Id__c', createdCaseList[0].Id);
        
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        // query created case
        List < String > obMFieldList = new List < String > {
            'Class_Name__c',
                'Interface_Name__c',
                'Name',
                'Number_Of_Retry__c',
                'Status__c',
                'Reference_Id__c'
                };
                    
                    List < Outbound_Message__c > eachOutboundMessageRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();
        obMsgRetryDOObj.recordId = eachOutboundMessageRecordList[0].Id;
        obMsgRetryDOObj.className = eachOutboundMessageRecordList[0].Class_Name__c;
        obMsgRetryDOObj.noOfretryAlreadyPerformed = eachOutboundMessageRecordList[0].Number_Of_Retry__c;
        obMsgRetryDOObj.interfaceName = eachOutboundMessageRecordList[0].Interface_Name__c;
        obMsgRetryDOObj.sObjectRecordID = eachOutboundMessageRecordList[0].Reference_Id__c;
        obMsgRetryDOObj.status = eachOutboundMessageRecordList[0].Status__c;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSMockImpl());
        
        RPSInterfaceRetryHandler.performRetryLogic(obMsgRetryDOObj);
        
        Test.stopTest();
        
        // check Send_to_RPS__c updated to 'Success'
        List < Case > afterUpdateCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, afterUpdateCaseList);
        System.assertEquals(1, afterUpdateCaseList.size());
        System.assertEquals('Test_Case_1', afterUpdateCaseList[0].Subject);
        System.assertEquals('Success', afterUpdateCaseList[0].Send_to_RPS__c);
        
        // check status changed
        List < Outbound_Message__c > afterUpdateOBMRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        System.assertNotEquals(null, afterUpdateOBMRecordList);
        System.assertEquals('Success', afterUpdateOBMRecordList[0].Status__c);
        
        // check application log created
        List < Application_Log__c > appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null, appLogs);
        System.assertEquals(0, appLogs.size());
        
    }
    
    
    private static testMethod void testException() {
        
        // query record type
        RecordType refund = [Select Id, name From RecordType where sobjecttype = 'Case'
                             And Name = 'Refund Case'
                            ];
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'testRetry');
        caseFieldValueMap.put('RecordTypeId', refund.Id);
        
        // no trigger will be called
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c',
                'Refund_status__c'
                };
                    
                    List < Case > createdCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, createdCaseList);
        System.assertEquals(1, createdCaseList.size());
        System.assertEquals('Test_Case_1', createdCaseList[0].Subject);
        System.assertEquals('testRetry', createdCaseList[0].Send_to_RPS__c);
        
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        obMsgFieldValueMap.put('Number_Of_Retry__c', 7);
        obMsgFieldValueMap.put('Status__c', 'Require Retry');
        obMsgFieldValueMap.put('Reference_Id__c', createdCaseList[0].Id);
        
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        // query created case
        List < String > obMFieldList = new List < String > {
            'Class_Name__c',
                'Interface_Name__c',
                'Name',
                'Number_Of_Retry__c',
                'Status__c',
                'Reference_Id__c'
                };
                    
        List < Outbound_Message__c > eachOutboundMessageRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        OutboundMessageRetryDO obMsgRetryDOObj = new OutboundMessageRetryDO();
        obMsgRetryDOObj.recordId = eachOutboundMessageRecordList[0].Id;
        obMsgRetryDOObj.className = eachOutboundMessageRecordList[0].Class_Name__c;
        obMsgRetryDOObj.noOfretryAlreadyPerformed = eachOutboundMessageRecordList[0].Number_Of_Retry__c;
        obMsgRetryDOObj.interfaceName = eachOutboundMessageRecordList[0].Interface_Name__c;
        obMsgRetryDOObj.sObjectRecordID = eachOutboundMessageRecordList[0].Reference_Id__c;
        obMsgRetryDOObj.status = eachOutboundMessageRecordList[0].Status__c;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSFailureMockImpl());
        
        RPSInterfaceRetryHandler.performRetryLogic(obMsgRetryDOObj);
        
        Test.stopTest();
        
        // check Send_to_RPS__c updated to 'Success'
        List < Case > afterUpdateCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, afterUpdateCaseList);
        System.assertEquals(1, afterUpdateCaseList.size());
        System.assertEquals('Test_Case_1', afterUpdateCaseList[0].Subject);
        System.assertEquals('testRetry', afterUpdateCaseList[0].Send_to_RPS__c);
        
        // check status changed
        List < Outbound_Message__c > afterUpdateOBMRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        System.assertNotEquals(null, afterUpdateOBMRecordList);
        System.assertEquals('Require Retry', afterUpdateOBMRecordList[0].Status__c);
        System.assertEquals(8, afterUpdateOBMRecordList[0].Number_Of_Retry__c);
        
        // check application log created
        List < Application_Log__c > appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null, appLogs);
        System.assertEquals(1, appLogs.size());
    }
    
    private static testMethod void testMaxRetry() {
        // query record type
        RecordType refund = [Select Id, name From RecordType where sobjecttype = 'Case'
                             And Name = 'Refund Case'
                            ];
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'testRetry');
        caseFieldValueMap.put('RecordTypeId', refund.Id);
        
        // no trigger will be called
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c',
                'Refund_status__c',
                'OwnerId'
                };
                    
                    List < Case > createdCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, createdCaseList);
        System.assertEquals(1, createdCaseList.size());
        System.assertEquals('Test_Case_1', createdCaseList[0].Subject);
        System.assertEquals('testRetry', createdCaseList[0].Send_to_RPS__c);
        
        
        // get MAX try number
        String caseTriggerSysSettngName = Utilities.getAppSettingsMdtValueByKey('CASE_TRIGGER_SYS_SETTING_NAME');
        List < System_Settings__mdt > logLevelCMD = Utilities.getLogSettings(caseTriggerSysSettngName);
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
        obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
        obMsgFieldValueMap.put('Name', 'RPSInterface');
        //obMsgFieldValueMap.put('Number_Of_Retry__c', (logLevelCMD[0].Number_Of_Retry__c-1));
        obMsgFieldValueMap.put('Number_Of_Retry__c', 9);
        obMsgFieldValueMap.put('Status__c', 'Require Retry');
        obMsgFieldValueMap.put('Reference_Id__c', createdCaseList[0].Id);
        
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        // query created case
        List < String > obMFieldList = new List < String > {
            'Class_Name__c',
                'Interface_Name__c',
                'Name',
                'Number_Of_Retry__c',
                'Status__c',
                'Reference_Id__c'
                };
                    
                    List < Outbound_Message__c > eachOutboundMessageRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        System.assertEquals(1, eachOutboundMessageRecordList.size());
        
        System.debug('eachOutboundMessageRecordList:' + eachOutboundMessageRecordList);
        OutboundMessageRetryDO obMsgRetryDOObj1 = new OutboundMessageRetryDO();
        obMsgRetryDOObj1.recordId = eachOutboundMessageRecordList[0].Id;
        obMsgRetryDOObj1.className = eachOutboundMessageRecordList[0].Class_Name__c;
        //obMsgRetryDOObj1.noOfretryAlreadyPerformed = eachOutboundMessageRecordList[0].Number_Of_Retry__c;
        obMsgRetryDOObj1.noOfretryAlreadyPerformed = 9;
        obMsgRetryDOObj1.interfaceName = eachOutboundMessageRecordList[0].Interface_Name__c;
        obMsgRetryDOObj1.sObjectRecordID = eachOutboundMessageRecordList[0].Reference_Id__c;
        obMsgRetryDOObj1.status = eachOutboundMessageRecordList[0].Status__c;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSFailureMockImpl());
        
        RPSInterfaceRetryHandler.performRetryLogic(obMsgRetryDOObj1);
        
        Test.stopTest();
        
        // check Send_to_RPS__c updated to 'Success'
        List < Case > afterUpdateCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, afterUpdateCaseList);
        System.assertEquals(1, afterUpdateCaseList.size());
        System.assertEquals('Test_Case_1', afterUpdateCaseList[0].Subject);
        System.assertEquals('Failed', afterUpdateCaseList[0].Send_to_RPS__c);
        System.assertEquals('System Error', afterUpdateCaseList[0].Refund_status__c);
        
        String highQueueId = Utilities.getQueueIdByName('AGSS Refund - High');
        System.assertEquals(highQueueId, afterUpdateCaseList[0].OwnerId);
        
        // check status changed
        List < Outbound_Message__c > afterUpdateOBMRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        System.assertNotEquals(null, afterUpdateOBMRecordList);
        System.assertEquals('Failed', afterUpdateOBMRecordList[0].Status__c);
        
        // check application log created
        List < Application_Log__c > appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null, appLogs);
        System.assertEquals(1, appLogs.size());
    }
    
    private static testMethod void testRefundStatusValidated() {
        // query record type
        RecordType refund = [Select Id, name From RecordType where sobjecttype = 'Case'
                             And Name = 'Refund Case'
                            ];
        
        // query record type
        RecordType childRefundCase = [Select Id, name From RecordType where sobjecttype = 'Case'
                                      And Name = 'Child Refund Case'
                                     ];
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'testRetry');
        caseFieldValueMap.put('RecordTypeId', refund.Id);
        
        // no trigger will be called
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c',
                'Refund_status__c',
                'OwnerId',
                'ParentId',
                'RecordTypeId'
                };
                    
                    List < Case > createdCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, createdCaseList);
        System.assertEquals(1, createdCaseList.size());
        
        // no trigger will be called
        caseFieldValueMap.put('RecordTypeId', childRefundCase.Id);
        caseFieldValueMap.put('Refund_status__c', 'Validated');
        caseFieldValueMap.put('ParentId', createdCaseList[0].Id);
        caseFieldValueMap.put('Priority', 'High');

        // Added: 25Oct2018 - additional fields required due to new Validation Rule
        caseFieldValueMap.put('Airline_Code__c','AK');
        caseFieldValueMap.put('Airline_Code_Navitaire__c','AK');
        caseFieldValueMap.put('Booking_Number__c','ABC123');
        caseFieldValueMap.put('Booking_Payment__c','Credit Card');
        caseFieldValueMap.put('Write_Off_Currency_Code__c','MYR');
        caseFieldValueMap.put('Write_Off_Amount__c',100);
        caseFieldValueMap.put('Refund_Currency_Code__c','MYR');
        caseFieldValueMap.put('Refund_Amount__c',100);
        
        TestUtility.createCase(caseFieldValueMap);
        
        List < Case > childCreatedCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, childCreatedCaseList);
        System.assertEquals(2, childCreatedCaseList.size());
        
        // get MAX try number
        String caseTriggerSysSettngName = Utilities.getAppSettingsMdtValueByKey('CASE_TRIGGER_SYS_SETTING_NAME');
        List < System_Settings__mdt > logLevelCMD = Utilities.getLogSettings(caseTriggerSysSettngName);
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        for (Integer i=0; i < childCreatedCaseList.size(); i++) {
            if (String.isNotEmpty(childCreatedCaseList[i].ParentId)) {
                obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
                obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
                obMsgFieldValueMap.put('Name', 'RPSInterface');
                //obMsgFieldValueMap.put('Number_Of_Retry__c', (logLevelCMD[0].Number_Of_Retry__c-1));
                obMsgFieldValueMap.put('Number_Of_Retry__c', 9);
                obMsgFieldValueMap.put('Status__c', 'Require Retry');
                obMsgFieldValueMap.put('Reference_Id__c', childCreatedCaseList[i].Id);
            }
        }
        
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        // query created case
        List < String > obMFieldList = new List < String > {
            'Class_Name__c',
                'Interface_Name__c',
                'Name',
                'Number_Of_Retry__c',
                'Status__c',
                'Reference_Id__c'
                };
                    
                    List < Outbound_Message__c > eachOutboundMessageRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        System.assertEquals(1, eachOutboundMessageRecordList.size());
        
        OutboundMessageRetryDO obMsgRetryDOObj1 = new OutboundMessageRetryDO();
        
        obMsgRetryDOObj1.recordId = eachOutboundMessageRecordList[0].Id;
        obMsgRetryDOObj1.className = eachOutboundMessageRecordList[0].Class_Name__c;
        //obMsgRetryDOObj1.noOfretryAlreadyPerformed = eachOutboundMessageRecordList[0].Number_Of_Retry__c;
        obMsgRetryDOObj1.noOfretryAlreadyPerformed = 9;
        obMsgRetryDOObj1.interfaceName = eachOutboundMessageRecordList[0].Interface_Name__c;
        obMsgRetryDOObj1.sObjectRecordID = eachOutboundMessageRecordList[0].Reference_Id__c;
        obMsgRetryDOObj1.status = eachOutboundMessageRecordList[0].Status__c;
        
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSFailureMockImpl());
        
        RPSInterfaceRetryHandler.performRetryLogic(obMsgRetryDOObj1);
        
        Test.stopTest();
        
        // check Send_to_RPS__c updated to 'Success'
        List < Case > afterUpdateCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, afterUpdateCaseList);
        System.assertEquals(2, afterUpdateCaseList.size());
        
        String highQueueId = Utilities.getQueueIdByName('Cardworks Pending');
        for (integer i=0; i < afterUpdateCaseList.size(); i++) {
            if (String.isNotEmpty(afterUpdateCaseList[i].ParentId)) {
                System.assertEquals('Failed', afterUpdateCaseList[i].Send_to_RPS__c);
                System.assertEquals('System Error', afterUpdateCaseList[i].Refund_status__c);
                //System.assertEquals(highQueueId, afterUpdateCaseList[i].OwnerId);
            }
        }
        // check status changed
        List < Outbound_Message__c > afterUpdateOBMRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        System.assertNotEquals(null, afterUpdateOBMRecordList);
        System.assertEquals('Failed', afterUpdateOBMRecordList[0].Status__c);
        
        // check application log created
        List < Application_Log__c > appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null, appLogs);
        System.assertEquals(2, appLogs.size()); //Updated after AG Automation
    }
    
    private static testMethod void testRefundStatusFailedValidated() {
        // query record type
        RecordType refund = [Select Id, name From RecordType where sobjecttype = 'Case'
                             And Name = 'Refund Case'
                            ];
        
        // query record type
        RecordType childRefundCase = [Select Id, name From RecordType where sobjecttype = 'Case'
                                      And Name = 'Child Refund Case'
                                     ];
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Send_to_RPS__c', 'testRetry');
        caseFieldValueMap.put('RecordTypeId', refund.Id);
        
        
        // no trigger will be called
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'Subject',
                'Send_to_RPS__c',
                'Refund_status__c',
                'OwnerId',
                'ParentId',
                'RecordTypeId'
                };
                    
                    List < Case > createdCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, createdCaseList);
        System.assertEquals(1, createdCaseList.size());
        
        // no trigger will be called
        Map < String, Object > caseFieldValueMap1 = new Map < String, Object > ();
        caseFieldValueMap1.put('RecordTypeId', childRefundCase.Id);
        caseFieldValueMap1.put('Refund_status__c', 'Failed Validation');
        caseFieldValueMap1.put('ParentId', createdCaseList[0].Id);
        caseFieldValueMap1.put('Priority', 'Low');
        
        TestUtility.createCase(caseFieldValueMap1);
        
        List < Case > childCreatedCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, childCreatedCaseList);
        System.assertEquals(2, childCreatedCaseList.size());
        
        // get MAX try number
        String caseTriggerSysSettngName = Utilities.getAppSettingsMdtValueByKey('CASE_TRIGGER_SYS_SETTING_NAME');
        List < System_Settings__mdt > logLevelCMD = Utilities.getLogSettings(caseTriggerSysSettngName);
        
        // create outbound Message record
        Map < String, Object > obMsgFieldValueMap = new Map < String, Object > ();
        for (integer i=0; i < childCreatedCaseList.size(); i++) {
            if (String.isNotEmpty(childCreatedCaseList[i].ParentId)) {
                obMsgFieldValueMap.put('Class_Name__c', 'RPSInterfaceRetryHandler');
                obMsgFieldValueMap.put('Interface_Name__c', 'RPSInterface');
                obMsgFieldValueMap.put('Name', 'RPSInterface');
                //obMsgFieldValueMap.put('Number_Of_Retry__c', (logLevelCMD[0].Number_Of_Retry__c-1));
                obMsgFieldValueMap.put('Number_Of_Retry__c', 9);
                obMsgFieldValueMap.put('Status__c', 'Require Retry');
                obMsgFieldValueMap.put('Reference_Id__c', childCreatedCaseList[i].Id);
            }
        }
        TestUtility.createOutboundMessage(obMsgFieldValueMap);
        
        // query created case
        List < String > obMFieldList = new List < String > {
            'Class_Name__c',
                'Interface_Name__c',
                'Name',
                'Number_Of_Retry__c',
                'Status__c',
                'Reference_Id__c'
                };
                    
                    List < Outbound_Message__c > eachOutboundMessageRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        System.assertEquals(1, eachOutboundMessageRecordList.size());
        
        OutboundMessageRetryDO obMsgRetryDOObj1 = new OutboundMessageRetryDO();
        obMsgRetryDOObj1.recordId = eachOutboundMessageRecordList[0].Id;
        obMsgRetryDOObj1.className = eachOutboundMessageRecordList[0].Class_Name__c;
        //obMsgRetryDOObj1.noOfretryAlreadyPerformed = eachOutboundMessageRecordList[0].Number_Of_Retry__c;
        obMsgRetryDOObj1.noOfretryAlreadyPerformed = 9;
        obMsgRetryDOObj1.interfaceName = eachOutboundMessageRecordList[0].Interface_Name__c;
        obMsgRetryDOObj1.sObjectRecordID = eachOutboundMessageRecordList[0].Reference_Id__c;
        obMsgRetryDOObj1.status = eachOutboundMessageRecordList[0].Status__c;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RPSRestWSFailureMockImpl());
        
        RPSInterfaceRetryHandler.performRetryLogic(obMsgRetryDOObj1);
        
        Test.stopTest();
        
        // check Send_to_RPS__c updated to 'Success'
        List < Case > afterUpdateCaseList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, afterUpdateCaseList);
        System.assertEquals(2, afterUpdateCaseList.size());
        
        String lowQueueId = Utilities.getQueueIdByName('AGSS Refund');
        for (integer i=0; i < afterUpdateCaseList.size(); i++) {
            if (String.isNotEmpty(afterUpdateCaseList[i].ParentId)) {
                System.assertEquals('Failed', afterUpdateCaseList[i].Send_to_RPS__c);
                System.assertEquals('System Error', afterUpdateCaseList[i].Refund_status__c);
                //System.assertEquals(lowQueueId, afterUpdateCaseList[i].OwnerId);
            }
        }
        
        // check status changed
        List < Outbound_Message__c > afterUpdateOBMRecordList = (List < Outbound_Message__c > ) TestUtility.getOutboundMessages(obMFieldList);
        System.assertNotEquals(null, afterUpdateOBMRecordList);
        System.assertEquals('Failed', afterUpdateOBMRecordList[0].Status__c);
        
        // check application log created
        List < Application_Log__c > appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null, appLogs);
        System.assertEquals(1, appLogs.size());
    }
    
}