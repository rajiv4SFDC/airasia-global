/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
25/07/2017      Pawan Kumar         Created
*******************************************************************/
public without sharing class RPSJsonRequestGenerator {
    public static String generateJson(Set<String> caseIds, Boolean hasNull) {
        List < Case > cases = new RPSJsonRequestGenerator().fetchCaseForJsonGeneration(caseIds, hasNull);
                     
        RPSCaseContainer  container = new RPSCaseContainer();
        if (!cases.isEmpty()) {
            container.cases = cases;
        }
        
        String jsonText = JSON.serialize(container, hasNull);
        System.debug('jsonText =' + jsonText);
        return jsonText;
    }
    
    
    
     public List<Case> fetchCaseForJsonGeneration(Set<String> caseIds, Boolean hasNull) {
         List < Case > cases = [SELECT Id,CaseNumber,AccountId,ContactId,RecordTypeId,ParentId,Type,Origin,Priority,Alternate_Phone_Number__c,
                                Case_Language__c,Flight_Number__c,Subject,EntitlementId,Booking_Number__c,Airline_Code__c,Booking_Payment__c,
                                Sub_Category_1__c,Sub_Category_2__c,Sub_Category_3__c,Bank_Holder_Name__c,Bank_Name__c,Bank_Account_Number__c,Country_of_Bank__c,
                                Bank_Branch__c,Bank_Code__c,Province__c,City__c,Bank_Address__c,Bank_Holder_Name_in_Chinese_language__c,
                                Bank_Name_in_Chinese_language__c,Bank_Branch_in_Chinese_language__c,Province_in_Chinese_language__c,City_in_Chinese_language__c,
                                Bank_Address_Non_English__c,Bank_Account_Holder_Home_Address__c,
                                Bank_Account_Holder_Home_Address_Non_En__c,Bank_Account_Holder_Gender__c,Bank_Account_Holder_Identity_Number_Pass__c,
                                Bank_Account_Holder_Telephone_Number__c,Bank_Branch_Code__c,Account_Type__c,Case_Currency__c,CreatedDate,
                                Parent_Case_Number__c,BSB_Code__c,Iban_Code__c,IFSC_Code__c,Sort_Code__c,Swift_Code__c, Account_First_Name__c,Account_Last_Name__c,                              
                                Start_Refund_Milestone__c,Payment_Code__c,Refund_Status__c,Declined_Reason__c,Declined_Reason_Code__c,Departure_Date_and_Time__c,
                                Manual_Contact_First_Name__c,Manual_Contact_Last_Name__c,Comments__c, Flight_Sector_to_be_Refunded__c, 
                                Sector_List_to_be_Refunded__c, Passenger_to_be_Refunded__c, Passenger_List_to_be_Refunded__c, RBV_Hotel_Refund__c FROM Case where Id IN:caseIds];
         
                  
        return cases;
    }
}