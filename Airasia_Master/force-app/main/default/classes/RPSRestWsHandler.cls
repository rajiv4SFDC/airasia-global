/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
25/07/2017      Pawan Kumar         Created
04/07/2019		Alvin Tayag			Added Bypass on Case Status Change for AG Automation
*******************************************************************/
public without sharing class RPSRestWsHandler {
    private static List < System_Settings__mdt > logLevelCMD;
    private static DateTime startTime = DateTime.now();
    private static String processLog = '';
    //private static List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
    private static Set < String > failureCaseList = new Set < String > ();
    private static Map < String, String > appSettings;
    
    static {
        appSettings = Utilities.getAppSettings();
    }
    
    @future(callout = true)
    public static void callRPSRestWebService(Set < String > caseIds) {
        //callRPSRestWS(caseIds, false,includeChildAlso);
        callRPSRestWS(caseIds, false,null);
    }
    
    
   
    public static void retryRPSRestWebService(Set < String > caseIds,OutboundMessageRetryDO outboundMessageDO) {
        //callRPSRestWS(caseIds, true,true);
        callRPSRestWS(caseIds, true,outboundMessageDO);
    }
    
    public static void callRPSRestWS(Set < String > caseIds, Boolean isRetry,OutboundMessageRetryDO outboundMessageDO) {
        List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
        String referenceType = 'By Payload';
        
        String rpsRequestBody = '';
        
        try {
            // query log level settings
            logLevelCMD = Utilities.getLogSettings(appSettings.get('CASE_TRIGGER_SYS_SETTING_NAME'));
            
            // send data to RPS 
            //String rpsRequestBody = RPSJsonRequestGenerator.generateJson(caseIds, false,includeChildAlso);
            
            if(isRetry && outboundMessageDO.Type==referenceType){
               
                //-- get the Paylod from OutboundMessage record combining all the chunks
                rpsRequestBody = OutboundMessageRetryUtility.getCompletePayload(outboundMessageDO.recordId);
                
            }else{
                rpsRequestBody = RPSJsonRequestGenerator.generateJson(caseIds, false);
            }
            
            sendDataToRPSRestWs(rpsRequestBody);
            
            // Update Case.Send_to_RPS__c='Success'
            updateCases(caseIds, appSettings.get('SEND_TO_RPS_SUCCESS'));
            
        } catch (Exception sendDataToRPSEx) {
            System.debug(LoggingLevel.ERROR, sendDataToRPSEx);
            
            processLog += 'Failed to send data to RPS for the following Case Ids: \r\n';
            processLog += JSON.serializePretty(caseIds) + '\r\n';
            processLog += 'Root Cause: ' + sendDataToRPSEx.getMessage() + '\r\n';
            processLog += 'Request Body: ' + rpsRequestBody;
            
            // create OM for retry
            if (isRetry) {
                throw new CalloutException(processLog);
                
            }
            
            // no need to create OM and log as 
            // this will be done at Retry Handler
            if (failureCaseList.isEmpty()) {
                failureCaseList = caseIds;
            }
           
            createOutboundMessageRecords(failureCaseList);
            
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'RPSRestWsHandler', 'callRPSRestWebService',
                                                                 null, appSettings.get('CASE_TRIGGER_APPLICATION_NAME'), processLog, null, 'Error Log',
                                                                 startTime, logLevelCMD[0], sendDataToRPSEx));
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
        }
    }
    
    public static void sendDataToRPSRestWs(String requestBody) {
        
        
        Web_Services_Credentials__mdt rpsRestCMD = getRPSRestCredentials();
        String rpsRestWsUrl = rpsRestCMD.URL__c;
        String rpsRestWsUN = rpsRestCMD.Username__c;
        String rpsRestWsPWD = rpsRestCMD.Password__c;
        
        // set request
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(rpsRestWsUrl);
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(requestBody);
        System.debug('rpsRequestBody'+requestBody);
        
        
        //Execute web service call here     
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        //get Body
        String rpsResBody = res.getBody();
        System.debug('rpsResBody: '+ res.getStatusCode() + ':' +rpsResBody);
        
        if (res.getStatusCode() == 200) {
            
            //throw new CalloutException('Test ');
            // Update Case Status
        } else {
            throw new CalloutException('' + res.getStatusCode());
        }
        
    }
    
    public static void updateCases(Set < String > caseIds, String statusValue) {
        List < Case > updateCaseList = new List < Case > ();
        
        // query record type
        String childRefundCaseRecId = [Select Id, name From RecordType where sobjecttype = 'Case'
                                       And Name = 'Child Refund Case'].Id;
        String parentRefundCaseRecId = [Select Id, name From RecordType where sobjecttype = 'Case'
                                        And Name = 'Refund Case'].Id;
        String highQueueId = Utilities.getQueueIdByName(appSettings.get('RPS_FAILED_QNAME_HIGH'));
        String lowQueueId = Utilities.getQueueIdByName(appSettings.get('RPS_FAILED_QNAME_LOW'));
        String cardWorkPendingQueueId = Utilities.getQueueIdByName(appSettings.get('RPS_CARDWORK_PENDING_QNAME'));
        
        for (Case eachCase: [Select Id, Send_to_RPS__c, Refund_Status__c, OwnerId, Airline_Refund_RPS_Case_Update__c from Case where Id IN: caseIds]) {
            if (String.isNotEmpty(statusValue) &&
                statusValue.equalsIgnoreCase(appSettings.get('SEND_TO_RPS_FAILED'))) {
                    eachCase.Send_to_RPS__c = statusValue;
                    eachCase.Refund_Status__c = 'System Error';
                    if (String.isNotEmpty(eachCase.Priority) &&
                        ((eachCase.Priority).equalsIgnoreCase('High'))) {
                            eachCase.OwnerId = highQueueId;
                        } else {
                            eachCase.OwnerId = lowQueueId;
                        }
                    
                } else {
                    eachCase.Send_to_RPS__c = statusValue;
                    // 04 July 2019 - Added Bypass on Case Status Change for AG Automation
                    if(eachCase.Airline_Refund_RPS_Case_Update__c == false) {
                    	eachCase.Status = appSettings.get('CASE_STATUS_IN_PROGRESS');
                    }
                    else {
                        eachCase.Airline_Refund_RPS_Case_Update__c = false; //Reset the flag to false for future updates
                    }
                }
                
            /*
            if (String.isNotEmpty(eachCase.ParentId)){
                eachCase.RecordTypeId = childRefundCaseRecId;
                
            }else{
                eachCase.RecordTypeId = parentRefundCaseRecId;
            }*/
            
            updateCaseList.add(eachCase);
            
        }
        
        // update cases
        if (!updateCaseList.isEmpty()) {
            Database.SaveResult[] updateRList = Database.update(updateCaseList, false);
            for (Integer i = 0; i < updateRList.size(); i++) {
                if (!updateRList[i].isSuccess()) {
                    for (Database.Error err: updateRList[i].getErrors()) {
                        processLog += '******* Unable to Update Case FOR Case ID: ' + updateCaseList[i].Id + '\r\n';
                        processLog += 'Root Cause: ' + err.getStatusCode() + ': ' + err.getMessage() + '\r\n';
                        processLog += 'Case fields that affected this error: ' + err.getFields() + '\r\n';
                        failureCaseList.add(updateCaseList[i].Id);
                    }
                }
            }
        }

        // this error will be thrown for RPSRetryHandler    
        if (!failureCaseList.isEmpty()) {
            throw new DMLException('Unable to update case status to ' +
                                   'success for Case Ids: ' + failureCaseList + '\r\n' + processLog + '\r\n');
        }
    }
    
    
    public static void createOutboundMessageRecords(Set < String > caseIds) {
        
        //-- get the Jsox Payload of each records
        List < Case > caseList = new RPSJsonRequestGenerator().fetchCaseForJsonGeneration(caseIds, false);
        Map<String,String> recordIdAndPayloadMap = new Map<String,String>();
        String InterfaceName = appSettings.get('OB_RETRY_INTERFACE_NAME_FOR_RPS');
        
        for (Case eachCase: caseList) {
            
            String eachCaseId = eachCase.Id;
            
            //-- build Payload for each case record for the retry mechansim
            RPSCaseContainer  container = new RPSCaseContainer();
            container.cases.add(eachCase);                
            String payload = JSON.serialize(container, false);
            
            //-- add the recordID and the Payload to the map
            recordIdAndPayloadMap.put(eachCaseId, payload);            
        }
        
        //-- Create Out bound Message records
        if(recordIdAndPayloadMap!=null && recordIdAndPayloadMap.size()>0){
            OutboundMessageRetryUtility.createOutboundRecord(recordIdAndPayloadMap,'Case',appSettings.get('OB_RETRY_INTERFACE_NAME_FOR_RPS'),appSettings.get('OB_RETRY_CLASS_NAME_FOR_RPS'),OutboundMessageRetryConstants.OB_TYPE_BY_PAYLOAD);
        }
        
        // below code just to cover error block - delete
        /**if (Test.isRunningTest()) {
            Outbound_Message__c outBoundMsgRetry = new Outbound_Message__c();
            parentInsertmap.put(null, outBoundMsgRetry);
        }**/
        
    }
    
    private static Web_Services_Credentials__mdt getRPSRestCredentials() {
        Web_Services_Credentials__mdt credentialsStore = [Select Username__c, Password__c, URL__c from Web_Services_Credentials__mdt where Web_Service_Name__c = : appSettings.get('RPS_WEBSERVICE_NAME')];
        return credentialsStore;
    }
}