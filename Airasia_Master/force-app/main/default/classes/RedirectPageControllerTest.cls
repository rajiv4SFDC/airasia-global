@isTest
private class RedirectPageControllerTest {

	@isTest static void testRedirectPage() {
		Test.startTest();
			RedirectPageController controller = new RedirectPageController();
			PageReference ref = controller.redirect();
			System.assert(ref != null);
		Test.stopTest();
	}

}