/**********************************************************************************************************************
* Social Advanced Customer Service Package                                                                                
* Version         2.0                                                                                                 
* Author          Hao Dong <hdong@salesforce.com>                                                                     
*                 Darcy Young <darcy.young@salesforce.com>                                                            
*                                                                                                                     
* Usage           The package bundles commonly requested Social Customer Care features, such as                       
*                  - Scalable PostTag to Post/Case Field Solution                                                    
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        
*                  - Ready to drive case management process                                                           
*                  - (Optionally) Set case origin                                                                     
*                  - (Optionally) Trigger case assignment rule                                                        
*                  - Rollup Followers from Social Persona to Contact                                                  
*                                                                                                                     
* Component       SFPS_SCS_SocialPostAction Class - Actions related to Social Post                                    
*                  - Scablable PostTag to Post/Case Field Solution                                                    
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        
*                  - Ready to drive case management process                                                           
*                  - (Optionally) Set case origin                                                                     
*                  - (Optionally) Trigger case assignment rule                                                        
*                                                                                                                     
* Custom Settings SFPS_SCS_Settings__c                                                                                
*                  - AssignmentRuleOnNewCase                                                                          
*                  - AssignmentRuleOnExistingCase                                                                     
*                  - CaseOrigin  
*                                                                                     
* Change Log      v1.0  Hao Dong 
*                       - PostTag dictionary 
*                       - SCS Post attribute
*                       - SCS case attribute
*                       - SCS from detractor to promoter trending
*                       - Social Persona / Contact roll up
*                 v2.0  Hao Dong
*                       - Service level metrics 
*
* Last Modified   09/14/2015                                                                                          
**********************************************************************************************************************/
public with sharing class SFPS_SCS_SocialPersonaActions {

    public static void updateContactFollowersCount(List<SocialPersona> triggerPersonaList){
        Map<Id, Integer> personaParentIdFollowerMap = new Map<Id, Integer>();

        // for social persona records in trigger, initialize personaParentIdFollowerMap with 0
        for(SocialPersona sp : triggerPersonaList){
            personaParentIdFollowerMap.put(sp.ParentId, 0);
        }

        // get all contacts in a map social persona parent ids
        Map<Id, Contact> contactMap = new Map<Id, Contact>([Select Id, SFPS_SCS_Followers__c From Contact Where Id IN :personaParentIdFollowerMap.keySet()]);

        // get all social persona records where parent id is no contact Map
        List<SocialPersona> contactPersonaList = [Select Id, ParentId, Followers From SocialPersona Where ParentId IN :contactMap.keySet()];

        // iterate through contact person records and add all followers
        for(SocialPersona sp : contactPersonaList){
            if(personaParentIdFollowerMap.containsKey(sp.ParentId)){
                if(sp.Followers != null) personaParentIdFollowerMap.put(sp.ParentId, personaParentIdFollowerMap.get(sp.ParentId) + sp.Followers);
            }
        }

        // iterate through contact map keys and assign follower count to SFPS_SCS_Followers__c
        for(Id cId : contactMap.keySet()){
            if(personaParentIdFollowerMap.containsKey(cId)){
                contactMap.get(cId).SFPS_SCS_Followers__c = personaParentIdFollowerMap.get(cId);
            }
        }

        try{
            // update contact map values
            if (!contactMap.isEmpty())
                update contactMap.values();                
        } catch (Exception e){
            system.debug('*** could not update contact : ' + e);
        }
    }

}