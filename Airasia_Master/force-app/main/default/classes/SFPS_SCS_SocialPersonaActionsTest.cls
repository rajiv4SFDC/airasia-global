/**********************************************************************************************************************
* Social Advanced Customer Service Package                                                                                
* Version         2.0                                                                                                 
* Author          Hao Dong <hdong@salesforce.com>                                                                     
*                 Darcy Young <darcy.young@salesforce.com>                                                            
*                                                                                                                     
* Usage           The package bundles commonly requested Social Customer Care features, such as                       
*                  - Scalable PostTag to Post/Case Field Solution                                                    
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        
*                  - Ready to drive case management process                                                           
*                  - (Optionally) Set case origin                                                                     
*                  - (Optionally) Trigger case assignment rule                                                        
*                  - Rollup Followers from Social Persona to Contact                                                  
*                                                                                                                     
* Component       SFPS_SCS_SocialPostAction Class - Actions related to Social Post                                    
*                  - Scablable PostTag to Post/Case Field Solution                                                    
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        
*                  - Ready to drive case management process                                                           
*                  - (Optionally) Set case origin                                                                     
*                  - (Optionally) Trigger case assignment rule                                                        
*                                                                                                                     
* Custom Settings SFPS_SCS_Settings__c                                                                                
*                  - AssignmentRuleOnNewCase                                                                          
*                  - AssignmentRuleOnExistingCase                                                                     
*                  - CaseOrigin  
*                                                                                     
* Change Log      v1.0  Hao Dong 
*                       - PostTag dictionary 
*                       - SCS Post attribute
*                       - SCS case attribute
*                       - SCS from detractor to promoter trending
*                       - Social Persona / Contact roll up
*                 v2.0  Hao Dong
*                       - Service level metrics 
*
* Last Modified   09/14/2015                                                                                          
**********************************************************************************************************************/
@isTest
private class SFPS_SCS_SocialPersonaActionsTest {

	@isTest static void updateContactFollowersCount() {
        List<SocialPersona> spList = new List<SocialPersona>();

        Contact contact1 = new Contact(LastName='Contact1'); // will have 1 social persona
        Contact contact2 = new Contact(LastName='Contact2'); // will have 2 social persona
        Contact contact3 = new Contact(LastName='Contact3'); // will have 3 social persona
        insert contact1;
        insert contact2;
        insert contact3;
        SocialPersona sp11 = new SocialPersona(Name='SocialPersona11', ParentId=contact1.Id, Provider='Twitter', Followers=100);
        SocialPersona sp21 = new SocialPersona(Name='SocialPersona21', ParentId=contact2.Id, Provider='Twitter', Followers=100);
        SocialPersona sp22 = new SocialPersona(Name='SocialPersona22', ParentId=contact2.Id, Provider='Facebook', Followers=100);
        SocialPersona sp31 = new SocialPersona(Name='SocialPersona31', ParentId=contact3.Id, Provider='Twitter', Followers=100);
        SocialPersona sp32 = new SocialPersona(Name='SocialPersona32', ParentId=contact3.Id, Provider='Facebook', Followers=100);
        SocialPersona sp33 = new SocialPersona(Name='SocialPersona33', ParentId=contact3.Id, Provider='Twitter', Followers=100);

        insert sp21;
        insert sp31;
        insert sp32;

        spList.add(sp22);
        spList.add(sp33);

        Test.startTest();
        insert sp11;
        insert spList;
        Test.stopTest();

        List<Contact> contactCheckList = [Select Id, LastName, SFPS_SCS_Followers__c From Contact];
        
        for (Contact c : contactCheckList){
            system.debug(c.LastName + ' : ' + c.SFPS_SCS_Followers__c);
            if (c.LastName == contact1.LastName) system.assertEquals(100, c.SFPS_SCS_Followers__c);
            if (c.LastName == contact2.LastName) system.assertEquals(200, c.SFPS_SCS_Followers__c);
            if (c.LastName == contact3.LastName) system.assertEquals(300, c.SFPS_SCS_Followers__c);
        }
	}  
	
}