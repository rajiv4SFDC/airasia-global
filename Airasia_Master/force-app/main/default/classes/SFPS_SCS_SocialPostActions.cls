/**********************************************************************************************************************
* Social Advanced Customer Service Package                                                                                
* Version         2.0                                                                                                 
* Author          Hao Dong <hdong@salesforce.com>                                                                     
*                 Darcy Young <darcy.young@salesforce.com>                                                            
*                                                                                                                     
* Usage           The package bundles commonly requested Social Customer Care features, such as                       
*                  - Scalable PostTag to Post/Case Field Solution                                                    
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        
*                  - Ready to drive case management process                                                           
*                  - (Optionally) Set case origin                                                                     
*                  - (Optionally) Trigger case assignment rule                                                        
*                  - Rollup Followers from Social Persona to Contact                                                  
*                                                                                                                     
* Component       SFPS_SCS_SocialPostAction Class - Actions related to Social Post                                    
*                  - Scablable PostTag to Post/Case Field Solution                                                    
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        
*                  - Ready to drive case management process                                                           
*                  - (Optionally) Set case origin                                                                     
*                  - (Optionally) Trigger case assignment rule                                                        
*                                                                                                                     
* Custom Settings SFPS_SCS_Settings__c                                                                                
*                  - AssignmentRuleOnNewCase                                                                          
*                  - AssignmentRuleOnExistingCase                                                                     
*                  - CaseOrigin  
*                                                                                     
* Change Log      v1.0  Hao Dong 
*                       - PostTag dictionary 
*                       - SCS Post attribute
*                       - SCS case attribute
*                       - SCS from detractor to promoter trending
*                       - Social Persona / Contact roll up
*                 v2.0  Hao Dong
*                       - Service level metrics 
*
* Last Modified   09/14/2015                                                                                          
**********************************************************************************************************************/
public with sharing class SFPS_SCS_SocialPostActions {
    public static Boolean SCS_SETTING_ASSIGNMENT_RULE_NEW_CASE_ENABLED = false;
    public static Boolean SCS_SETTING_ASSIGNMENT_RULE_EXISTING_CASE_ENABLED = false;
    public static String SCS_SETTING_CASE_ORIGIN_VALUE = '';
    public static Boolean SCS_SETTING_CASE_ORIGIN_ENABLED = false;

    public static Database.DMLOptions dmlOpts = null;
    
	public static void getSettings() {
		try {
            system.debug('*** SFPS_SCS getSettings() starts');

    		SCS_SETTING_CASE_ORIGIN_ENABLED = SFPS_SCS_Settings__c.getInstance('CaseOrigin').Enabled__c;
    		SCS_SETTING_CASE_ORIGIN_VALUE = SFPS_SCS_Settings__c.getInstance('CaseOrigin').Value__c;
    		SCS_SETTING_ASSIGNMENT_RULE_NEW_CASE_ENABLED = SFPS_SCS_Settings__c.getInstance('AssignmentRuleOnNewCase').Enabled__c;
    		SCS_SETTING_ASSIGNMENT_RULE_EXISTING_CASE_ENABLED = SFPS_SCS_Settings__c.getInstance('AssignmentRuleOnExistingCase').Enabled__c;

			//get case assignment rules ID
			if ((SCS_SETTING_ASSIGNMENT_RULE_NEW_CASE_ENABLED || SCS_SETTING_ASSIGNMENT_RULE_EXISTING_CASE_ENABLED))// && (SCS_SETTING_ASSIGNMENT_RULE_NEW_CASE_VALUE != null && SCS_SETTING_ASSIGNMENT_RULE_NEW_CASE_VALUE != ''))
			{
		        AssignmentRule AR = new AssignmentRule();
		        //AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true And Name = :SCS_SETTING_ASSIGNMENT_RULE_NEW_CASE_VALUE];
				AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true Limit 1];

		        if (AR != null)
		        {
			        dmlOpts = new Database.DMLOptions();
			        dmlOpts.assignmentRuleHeader.assignmentRuleId = AR.Id;
		        }
		    }

        } catch (Exception e){
            system.debug('*** SFPS_SCS getSettings() cannot retrieve all custom settings from SFPS_SCS_Settings__c: ' + e);
        }
	}

    // Update Post Attributes by PostTags
    public static void updatePostAttributesByTags(List<SocialPost> postList) {

        Set<String> allPostTags = new Set<String>();
        Set<String> postTagClauses = new Set<String>();   
        Map<Id, String> postTagsByPostSet = new Map<Id, String>();

        for(SocialPost p : postList) { 

            if (p.IsOutbound == false && p.PostTags != null)
            {
            	allPostTags.addAll(p.PostTags.split(','));
            }
        }

        if (!allPostTags.isEmpty())
        {
	        //Check with Darcy on this Map<String, Object> directly from SOQL w/o loop thru
	        Map<Id, SFPS_SCS_Post_Tag__c> postTagMapById = new Map<Id, SFPS_SCS_Post_Tag__c>([Select Id, Name, Tag_Importance__c, Priority__c, Language__c, Influence__c, NPS__c, Attr1__c, Attr2__c, Attr3__c, Attr4__c, Attr5__c, Attr6__c from SFPS_SCS_Post_Tag__c where Name IN :allPostTags]);


            if (postTagMapById.size() >= 1)
            {
				Map<String, SFPS_SCS_Post_Tag__c> postTagMap = new Map<String, SFPS_SCS_Post_Tag__c>();

				for (ID idKey : postTagMapById.keyset()) {
				    SFPS_SCS_Post_Tag__c postTagRecord = postTagMapById.get(idKey);
				    postTagMap.put(postTagRecord.Name, postTagRecord);
				}

		        for(SocialPost p : postList) 
		        { 
		            if (p.IsOutbound == false && p.PostTags != null)
		            {
		            	List<String> tags = p.PostTags.split(',');	

		            	String sPriority = '';
		            	Integer nImportancePriority = -1;

		            	String sLanguage = '';
		            	Integer nImportanceLanguage = -1;

		            	String sInfluence = '';
		            	Integer nImportanceInfluence = -1;

		            	String sNPS = '';
		            	Integer nImportanceNPS = -1;
		            	
		            	String sAttr1 = '';
		            	Integer nImportanceAttr1 = -1;
		            	
		            	String sAttr2 = '';
		            	Integer nImportanceAttr2 = -1;
		            	
		            	String sAttr3 = '';
		            	Integer nImportanceAttr3 = -1;
		            	
		            	String sAttr4 = '';
		            	Integer nImportanceAttr4 = -1;
		            	
		            	String sAttr5 = '';
		            	Integer nImportanceAttr5 = -1;
		            	
		            	String sAttr6 = '';
		            	Integer nImportanceAttr6 = -1;

				        for(String tag : tags) 
				        {
				            SFPS_SCS_Post_Tag__c postTagRecord = postTagMap.get(tag);

				            if (postTagRecord != null && postTagRecord.Tag_Importance__c != null)
				            {

				            	Integer nNewImportance = postTagRecord.Tag_Importance__c.intValue();

				            	if (postTagRecord.Priority__c != null && nNewImportance > nImportancePriority)
				            	{
				            		sPriority = postTagRecord.Priority__c;
				            		nImportancePriority = nNewImportance;
				            	}
				            	if (postTagRecord.Language__c != null && nNewImportance > nImportanceLanguage)
				            	{
				            		sLanguage = postTagRecord.Language__c;
				            		nImportanceLanguage = nNewImportance;
				            	}
				            	if (postTagRecord.Influence__c != null && nNewImportance > nImportanceInfluence)
				            	{
				            		sInfluence = postTagRecord.Influence__c;
				            		nImportanceInfluence = nNewImportance;
				            	}
				            	if (postTagRecord.NPS__c != null && nNewImportance > nImportanceNPS)
				            	{
				            		sNPS = postTagRecord.NPS__c;
				            		nImportanceNPS = nNewImportance;
				            	}

				            	if (postTagRecord.Attr1__c != null && nNewImportance > nImportanceAttr1)
				            	{
				            		sAttr1 = postTagRecord.Attr1__c;
				            		nImportanceAttr1 = nNewImportance;
				            	}
				            	if (postTagRecord.Attr2__c != null && nNewImportance > nImportanceAttr2)
				            	{
				            		sAttr2 = postTagRecord.Attr2__c;
				            		nImportanceAttr2 = nNewImportance;
				            	}
				            	if (postTagRecord.Attr3__c != null && nNewImportance > nImportanceAttr3)
				            	{
				            		sAttr3 = postTagRecord.Attr3__c;
				            		nImportanceAttr3 = nNewImportance;
				            	}
				            	if (postTagRecord.Attr4__c != null && nNewImportance > nImportanceAttr4)
				            	{
				            		sAttr4 = postTagRecord.Attr4__c;
				            		nImportanceAttr4 = nNewImportance;
				            	}
				            	if (postTagRecord.Attr5__c != null && nNewImportance > nImportanceAttr5)
				            	{
				            		sAttr5 = postTagRecord.Attr5__c;
				            		nImportanceAttr5 = nNewImportance;
				            	}
				            	if (postTagRecord.Attr6__c != null && nNewImportance > nImportanceAttr6)
				            	{
				            		sAttr6 = postTagRecord.Attr6__c;
				            		nImportanceAttr6 = nNewImportance;
				            	}
				            }
				        }


            			try {

					        if (sPriority != '')
					        	p.SFPS_SCS_Post_Priority__c = sPriority;	
					        if (sLanguage != '')
					        	p.SFPS_SCS_Post_Language__c = sLanguage;	
					        if (sInfluence != '')
					        	p.SFPS_SCS_Post_Influence__c = sInfluence;			        	
					        if (sNPS != '')
					        	p.SFPS_SCS_Post_NPS__c = sNPS;
					        if (sAttr1 != '')
					        	p.SFPS_SCS_Post_Attr1__c = sAttr1;
					        if (sAttr2 != '')
					        	p.SFPS_SCS_Post_Attr2__c = sAttr2;
					        if (sAttr3 != '')
					        	p.SFPS_SCS_Post_Attr3__c = sAttr3;
					        if (sAttr4 != '')
					        	p.SFPS_SCS_Post_Attr4__c = sAttr4;
					        if (sAttr5 != '')
					        	p.SFPS_SCS_Post_Attr5__c = sAttr5;
					        if (sAttr6 != '')
					        	p.SFPS_SCS_Post_Attr6__c = sAttr6;

			            } catch (Exception e) {
			                system.debug('*** SFPS_SCS updatePostAttributesByTags() could not update post: ' + e);
			            }

		            }
		        }
		    }
        }
    }


    // Update Case Attributes by PostTags
    public static void updateNewParent(List<SocialPost> postList) {
        system.debug('*** SFPS_SCS updateNewParent()');

    	getSettings();

        // create map to pull relative post fields based on case
        Map<ID, SocialPost> caseIdPostMap = new Map<ID, SocialPost>();
        
        // query case to get fields to update
        List<SocialPost> postToCheckList = [Select Id, IsOutbound, ParentId,
                                    	PostTags,
                                    	Provider,
                                    	Content,
                                    	SFPS_SCS_Post_Priority__c,                                       
										SFPS_SCS_Post_Influence__c,
										SFPS_SCS_Post_Language__c,
										SFPS_SCS_Post_Attr1__c,
										SFPS_SCS_Post_Attr2__c,
										SFPS_SCS_Post_Attr3__c,
										SFPS_SCS_Post_Attr4__c,
										SFPS_SCS_Post_Attr5__c,
										SFPS_SCS_Post_Attr6__c,
                                        SFPS_SCS_Post_NPS__c,
                                        SFPS_SCS_Case_Data_Processed__c,
                                        Posted, CreatedDate
                                        From SocialPost Where Id IN :postList];
        
        List<SocialPost> postToUpdateList = new List<SocialPost>();        
        //system.debug('*** SFPS_SCS postToUpdateList : ' + postToUpdateList);
        
        // populate map - contact id from case to case object and set sync date and reset picklist for CCC_Beauty_Profile_Sync_Option__c
        for (SocialPost p : postToCheckList){
            caseIdPostMap.put(p.ParentId, p);
        }
        
        //system.debug('*** SFPS_SCS caseIdPostMap : ' + caseIdPostMap);
        
        // query to get case fields to update
        List<Case> caseToCheckList = [Select Id,
                Subject,
                Description,
        		SFPS_SCS_Post_Original__c,
        		SFPS_SCS_Channel__c,
        		SFPS_SCS_Priority__c,
				SFPS_SCS_Influence__c,
				SFPS_SCS_Language__c,
				SFPS_SCS_Attr1__c,
				SFPS_SCS_Attr2__c,
				SFPS_SCS_Attr3__c,
				SFPS_SCS_Attr4__c,
				SFPS_SCS_Attr5__c,
				SFPS_SCS_Attr6__c,
        		SFPS_SCS_NPS__c,
        		SFPS_SCS_NPS_Original__c,
        		SFPS_SCS_TimeStamp_NPS__c,
        		SFPS_SCS_TimeStamp_NPS_Original__c,

                SFPS_SCS_FirstInboundPostAt__c,
                SFPS_SCS_FirstOutboundPostAt__c,
                SFPS_SCS_LastInboundPostAt__c,
                SFPS_SCS_LastOutboundPostAt__c,
                SFPS_SCS_LastPostAt__c,
                SFPS_SCS_LastPostRecordCreatedAt__c,
                SFPS_SCS_New_Post__c,
                SFPS_SCS_NumberInboundPosts__c,
                SFPS_SCS_NumberOutboundPosts__c,
                SFPS_SCS_NumberPosts__c,

                SFPS_SCS_CumulativeResponseTime__c,
                SFPS_SCS_CumulativeUpdateTime__c,
                SFPS_SCS_NumberUpdatePosts__c

                From Case
                Where Id IN :caseIdPostMap.keySet()];

		//system.debug('*** SFPS_SCS caseToCheckList : ' + caseToCheckList);

        List<Case> caseToUpdateList = new List<Case>();

        //system.debug('*** SFPS_SCS caseToUpdateList : ' + caseToUpdateList);
        
        //only proceed if query returned cases
        if (!caseToCheckList.isEmpty()) {
            for (Case c : caseToCheckList) {

            	String sCaseAssignmentRule = '';
            	SocialPost p = caseIdPostMap.get(c.Id);

            	system.debug('*** p : ' + p);
            	system.debug('*** c.SFPS_SCS_Post_Original__c : ' + c.SFPS_SCS_Post_Original__c);

            	if (c.SFPS_SCS_NumberPosts__c == null) c.SFPS_SCS_NumberPosts__c = 0;
            	c.SFPS_SCS_NumberPosts__c ++;
            	c.SFPS_SCS_LastPostAt__c = p.Posted;
            	c.SFPS_SCS_LastPostRecordCreatedAt__c = p.CreatedDate;

            	if (p.IsOutbound == false)
            	{
            		//inbound post
	            	c.SFPS_SCS_New_Post__c = true;

            		if (c.SFPS_SCS_FirstInboundPostAt__c == null) c.SFPS_SCS_FirstInboundPostAt__c = p.Posted;
					c.SFPS_SCS_LastInboundPostAt__c = p.Posted;

					if (c.SFPS_SCS_NumberInboundPosts__c == null) c.SFPS_SCS_NumberInboundPosts__c = 0;
					c.SFPS_SCS_NumberInboundPosts__c ++;

	            	//customer follow up post count since agent first response
	            	if (c.SFPS_SCS_NumberOutboundPosts__c >= 1)
	            	{
						if (c.SFPS_SCS_NumberUpdatePosts__c == null) c.SFPS_SCS_NumberUpdatePosts__c = 0;
						c.SFPS_SCS_NumberUpdatePosts__c ++;

		            	if (c.SFPS_SCS_LastInboundPostAt__c != null && c.SFPS_SCS_LastOutboundPostAt__c != null)
		            	{
		            		if (c.SFPS_SCS_CumulativeUpdateTime__c == null) c.SFPS_SCS_CumulativeUpdateTime__c = 0;

		            		//add response time in minutes
		            		c.SFPS_SCS_CumulativeUpdateTime__c += ((c.SFPS_SCS_LastInboundPostAt__c.getTime() - c.SFPS_SCS_LastOutboundPostAt__c.getTime()) / 10.0 / 60.0).Round() / 100.0;  
		            	}
		            }

            		//first inbound post
	            	if (c.SFPS_SCS_Post_Original__c == null)
	            	{
	            		c.SFPS_SCS_Post_Original__c = p.Id;
	            		c.SFPS_SCS_Channel__c = p.Provider;
	            		c.SFPS_SCS_Priority__c = p.SFPS_SCS_Post_Priority__c;
						c.SFPS_SCS_Influence__c = p.SFPS_SCS_Post_Influence__c;
						c.SFPS_SCS_Language__c = p.SFPS_SCS_Post_Language__c;
						c.SFPS_SCS_Attr1__c = p.SFPS_SCS_Post_Attr1__c;
						c.SFPS_SCS_Attr2__c = p.SFPS_SCS_Post_Attr2__c;
						c.SFPS_SCS_Attr3__c = p.SFPS_SCS_Post_Attr3__c;
						c.SFPS_SCS_Attr4__c = p.SFPS_SCS_Post_Attr4__c;
						c.SFPS_SCS_Attr5__c = p.SFPS_SCS_Post_Attr5__c;
						c.SFPS_SCS_Attr6__c = p.SFPS_SCS_Post_Attr6__c;

	            		c.Subject = p.PostTags;
	            		c.Description = p.Content;

	            		if (SCS_SETTING_CASE_ORIGIN_ENABLED && SCS_SETTING_CASE_ORIGIN_VALUE != '')            		
	            			c.Origin = SCS_SETTING_CASE_ORIGIN_VALUE;      

	            		if (SCS_SETTING_ASSIGNMENT_RULE_NEW_CASE_ENABLED && dmlOpts != null)      
	            			c.setOptions(dmlOpts);
	            	}
	            	else
	            	{
	            	    if (SCS_SETTING_ASSIGNMENT_RULE_EXISTING_CASE_ENABLED && dmlOpts != null)  
	            			c.setOptions(dmlOpts);
	            	}

	            	system.debug('*** p.SFPS_SCS_Post_NPS__c != null : ' + p.SFPS_SCS_Post_NPS__c != null);
	            	if (p.SFPS_SCS_Post_NPS__c != null)
	            	{
	            		
		            	system.debug('*** c.SFPS_SCS_NPS__c == null : ' + c.SFPS_SCS_NPS__c);
	            		if (c.SFPS_SCS_NPS__c == null)
	            		{
	            			c.SFPS_SCS_NPS_Original__c = p.SFPS_SCS_Post_NPS__c;
	            			c.SFPS_SCS_Timestamp_NPS_Original__c = p.Posted;
	            			c.SFPS_SCS_NPS__c = p.SFPS_SCS_Post_NPS__c;
	            			c.SFPS_SCS_Timestamp_NPS__c = p.Posted;
	            		}
	            		else
	            		{
			            	system.debug('*** c.SFPS_SCS_NPS__c != p.SFPS_SCS_Post_NPS__c : ' + (c.SFPS_SCS_NPS__c != p.SFPS_SCS_Post_NPS__c));

	            			if (c.SFPS_SCS_NPS__c != p.SFPS_SCS_Post_NPS__c)
	            			{
		            			c.SFPS_SCS_NPS__c = p.SFPS_SCS_Post_NPS__c;
		            			c.SFPS_SCS_Timestamp_NPS__c = p.Posted;			
	            			}
	            		}
	            	}
	            }
	            else
	            {
            		//outbound post
	            	c.SFPS_SCS_New_Post__c = false;

					if (c.SFPS_SCS_FirstOutboundPostAt__c == null) c.SFPS_SCS_FirstOutboundPostAt__c = p.Posted;
					c.SFPS_SCS_LastOutboundPostAt__c = p.Posted;

            		if (c.SFPS_SCS_NumberOutboundPosts__c == null) c.SFPS_SCS_NumberOutboundPosts__c = 0;
            		c.SFPS_SCS_NumberOutboundPosts__c ++;

	           		//agent response
	            	if (c.SFPS_SCS_NumberInboundPosts__c >= 1)
	            	{
		            	if (c.SFPS_SCS_LastInboundPostAt__c != null && c.SFPS_SCS_LastOutboundPostAt__c != null)
		            	{
		            		if (c.SFPS_SCS_CumulativeResponseTime__c == null) c.SFPS_SCS_CumulativeResponseTime__c = 0;

		            		//add agent response time in minutes
		            		c.SFPS_SCS_CumulativeResponseTime__c += ((c.SFPS_SCS_LastOutboundPostAt__c.getTime() - c.SFPS_SCS_LastInboundPostAt__c.getTime()) / 10.0 / 60.0).Round() / 100.0; 
		            	}
		            }

	            }

            	caseToUpdateList.add(c);

            	if (p.IsOutbound == false)
            	{
            		p.SFPS_SCS_Case_Data_Processed__c = true;
            		postToUpdateList.add(p);
            	}
            }                       
        }

    	system.debug('*** postToUpdateList : ' + postToUpdateList);

        try{
        	if (!postToUpdateList.isEmpty())
        	    update postToUpdateList;

	            system.debug('*** postToUpdateList check : ' + [Select Id, SFPS_SCS_Case_Data_Processed__c From SocialPost Where Id IN :postToUpdateList]);

        	if (!caseToUpdateList.isEmpty())
            	update caseToUpdateList;

            system.debug('*** SFPS_SCS updateNewParent() updated');

        } catch (Exception e){
            system.debug('*** SFPS_SCS updateNewParent() could not update post and/or case : ' + e);
        }
    }
}