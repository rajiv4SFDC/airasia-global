/**********************************************************************************************************************
* Social Advanced Customer Service Package                                                                                
* Version         2.0                                                                                                 
* Author          Hao Dong <hdong@salesforce.com>                                                                     
*                 Darcy Young <darcy.young@salesforce.com>                                                            
*                                                                                                                     
* Usage           The package bundles commonly requested Social Customer Care features, such as                       
*                  - Scalable PostTag to Post/Case Field Solution                                                    
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        
*                  - Ready to drive case management process                                                           
*                  - (Optionally) Set case origin                                                                     
*                  - (Optionally) Trigger case assignment rule                                                        
*                  - Rollup Followers from Social Persona to Contact                                                  
*                                                                                                                     
* Component       SFPS_SCS_SocialPostAction Class - Actions related to Social Post                                    
*                  - Scablable PostTag to Post/Case Field Solution                                                    
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        
*                  - Ready to drive case management process                                                           
*                  - (Optionally) Set case origin                                                                     
*                  - (Optionally) Trigger case assignment rule                                                        
*                                                                                                                     
* Custom Settings SFPS_SCS_Settings__c                                                                                
*                  - AssignmentRuleOnNewCase                                                                          
*                  - AssignmentRuleOnExistingCase                                                                     
*                  - CaseOrigin  
*                                                                                     
* Change Log      v1.0  Hao Dong 
*                       - PostTag dictionary 
*                       - SCS Post attribute
*                       - SCS case attribute
*                       - SCS from detractor to promoter trending
*                       - Social Persona / Contact roll up
*                 v2.0  Hao Dong
*                       - Service level metrics 
*
* Last Modified   09/14/2015                                                                                          
**********************************************************************************************************************/
@isTest
private class SFPS_SCS_SocialPostActionsTest
{
        @isTest
        static void updatePostAttributesByTags()
        {               
                //Set up Tag Dictionary
                SFPS_SCS_Post_Tag__c tag1 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 1, Name = 'English', Language__c='English');
                SFPS_SCS_Post_Tag__c tag2 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 3, Name = 'VIP Influencer', Influence__c='VIP Influencer', Priority__c = 'High');
                SFPS_SCS_Post_Tag__c tag3 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 2, Name = 'Complaints', NPS__c='Detractors', Priority__c = 'Medium');
                SFPS_SCS_Post_Tag__c tag4 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 1, Name = 'Compliments', NPS__c='Promoters', Priority__c = 'Low');
                SFPS_SCS_Post_Tag__c tag5 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 1, Name = 'Attr1', Attr1__c='Attr1');
                SFPS_SCS_Post_Tag__c tag6 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 1, Name = 'Attr2', Attr2__c='Attr2');
                SFPS_SCS_Post_Tag__c tag7 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 1, Name = 'Attr3', Attr3__c='Attr3');
                SFPS_SCS_Post_Tag__c tag8 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 1, Name = 'Attr4', Attr4__c='Attr4');
                SFPS_SCS_Post_Tag__c tag9 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 1, Name = 'Attr5', Attr5__c='Attr5');
                SFPS_SCS_Post_Tag__c tag10 = new SFPS_SCS_Post_Tag__c(Tag_Importance__c = 1, Name = 'Attr6', Attr6__c='Attr6');
                
                insert tag1;
                insert tag2;
                insert tag3;
                insert tag4;
                insert tag5;
                insert tag6;
                insert tag7;
                insert tag8;
                insert tag9;
                insert tag10;

                List<SocialPost> spList = new List<SocialPost>();

                //Single transactions
                SocialPost sp101 = new SocialPost(Name='TWEET101', IsOutbound = true, PostTags='English,VIP Influencer');
                SocialPost sp111 = new SocialPost(Name='TWEET111', IsOutbound = false, PostTags='English,VIP Influencer');

                //Bulk transactions
                SocialPost sp211 = new SocialPost(Name='TWEET212', IsOutbound = false, PostTags='Attr1,Attr2,Attr3,Attr4,Attr5,Attr6');
                SocialPost sp212 = new SocialPost(Name='TWEET211', IsOutbound = false, PostTags='Compliments,Complaints');
                SocialPost sp213 = new SocialPost(Name='TWEET213', IsOutbound = false, PostTags='English,VIP Influencer');

                spList.add(sp211);
                spList.add(sp212);
                spList.add(sp213);

                Test.startTest();
                insert sp101;
                insert sp111;
                insert spList;
                Test.stopTest();


                List<SocialPost> spCheckList = [Select Id, SFPS_SCS_Post_Priority__c, SFPS_SCS_Post_Language__c, SFPS_SCS_Post_Influence__c, SFPS_SCS_Post_NPS__c, 
                                                        SFPS_SCS_Post_Attr1__c, SFPS_SCS_Post_Attr2__c, SFPS_SCS_Post_Attr3__c, 
                                                        SFPS_SCS_Post_Attr4__c, SFPS_SCS_Post_Attr5__c, SFPS_SCS_Post_Attr6__c
                                                         From SocialPost];

                for(SocialPost sp : spCheckList){
                        if(sp.Id == sp101.Id){
                                //Verify outbound post is not eligible
                                system.assertEquals(null, sp.SFPS_SCS_Post_Language__c);
                        }

                        //Verify single update works
                        if(sp.Id == sp111.Id){                                
                                system.assertEquals('English', sp.SFPS_SCS_Post_Language__c);
                                system.assertEquals('VIP Influencer', sp.SFPS_SCS_Post_Influence__c);
                                system.assertEquals('High', sp.SFPS_SCS_Post_Priority__c);
                        }

                        //Verify bulk update works
                        //Verify custom post attributes
                        if(sp.Id == sp211.Id){
                                system.assertEquals('Attr1', sp.SFPS_SCS_Post_Attr1__c);
                                system.assertEquals('Attr2', sp.SFPS_SCS_Post_Attr2__c);
                                system.assertEquals('Attr3', sp.SFPS_SCS_Post_Attr3__c);
                                system.assertEquals('Attr4', sp.SFPS_SCS_Post_Attr4__c);
                                system.assertEquals('Attr5', sp.SFPS_SCS_Post_Attr5__c);
                                system.assertEquals('Attr6', sp.SFPS_SCS_Post_Attr6__c);
                        }

                        //Verify tag importance/priority works
                        if(sp.Id == sp212.Id){
                                system.assertEquals('Detractors', sp.SFPS_SCS_Post_NPS__c);
                                system.assertEquals('Medium', sp.SFPS_SCS_Post_Priority__c);
                        }
                }
        }

        @isTest
        static void updateNewParent()
        {
                insert new SFPS_SCS_Settings__c(Name='CaseOrigin', Enabled__c=true, Value__c='Social Media');
                insert new SFPS_SCS_Settings__c(Name='AssignmentRuleOnNewCase', Enabled__c=true);
                insert new SFPS_SCS_Settings__c(Name='AssignmentRuleOnExistingCase', Enabled__c=true);

                Case case1 = new Case(Subject='Case1');
                insert case1;
                
                SocialPost sp101 = new SocialPost();                
                sp101.Name='TWEET101';
                sp101.IsOutbound = false;
                sp101.PostTags='English,VIP Influencer,Complaints';
                sp101.ParentId = case1.Id;
                sp101.Provider = 'Twitter';
                sp101.Content = 'content101';
                sp101.SFPS_SCS_Post_Priority__c = 'High';                                       
                sp101.SFPS_SCS_Post_Influence__c = 'VIP Influencer';
                sp101.SFPS_SCS_Post_Language__c = 'English';
                sp101.SFPS_SCS_Post_Attr1__c = 'Attr11';
                sp101.SFPS_SCS_Post_Attr2__c = 'Attr12';
                sp101.SFPS_SCS_Post_Attr3__c = 'Attr13';
                sp101.SFPS_SCS_Post_Attr4__c = 'Attr14';
                sp101.SFPS_SCS_Post_Attr5__c = 'Attr15';
                sp101.SFPS_SCS_Post_Attr6__c = 'Attr16';
                sp101.SFPS_SCS_Post_NPS__c = 'Detractors';
                sp101.SFPS_SCS_Case_Data_Processed__c = false;
                sp101.Posted = system.today().addDays(-1);

                SocialPost sp102 = new SocialPost();
                sp102.Name='TWEET102';
                sp102.IsOutbound = false;
                sp102.PostTags='English,VIP Influencer,Compliments';
                sp102.ParentId = case1.Id;
                sp102.Provider = 'Twitter';
                sp102.Content = 'content102';
                sp102.SFPS_SCS_Post_Priority__c = null;                                       
                sp102.SFPS_SCS_Post_Influence__c = null;
                sp102.SFPS_SCS_Post_Language__c = 'English';
                sp102.SFPS_SCS_Post_Attr1__c = 'Attr21';
                sp102.SFPS_SCS_Post_Attr2__c = 'Attr22';
                sp102.SFPS_SCS_Post_Attr3__c = 'Attr23';
                sp102.SFPS_SCS_Post_Attr4__c = 'Attr24';
                sp102.SFPS_SCS_Post_Attr5__c = 'Attr25';
                sp102.SFPS_SCS_Post_Attr6__c = 'Attr26';
                sp102.SFPS_SCS_Post_NPS__c = 'Promoters';
                sp102.SFPS_SCS_Case_Data_Processed__c = false;
                sp102.Posted = system.today();

                Test.startTest();
                insert sp101;    
                insert sp102;
                Test.stopTest();

                SocialPost sp101Check = [Select Id, SFPS_SCS_Case_Data_Processed__c From SocialPost Where Id = :sp101.Id];
                SocialPost sp102Check = [Select Id, SFPS_SCS_Case_Data_Processed__c From SocialPost Where Id = :sp102.Id];

                Case caseCheck = [Select Id,
                                        Subject,
                                        Description,
                                        Origin,
                                        SFPS_SCS_Post_Original__c,
                                        SFPS_SCS_Channel__c,
                                        SFPS_SCS_Priority__c,
                                        SFPS_SCS_Influence__c,
                                        SFPS_SCS_Language__c,
                                        SFPS_SCS_Attr1__c,
                                        SFPS_SCS_Attr2__c,
                                        SFPS_SCS_Attr3__c,
                                        SFPS_SCS_Attr4__c,
                                        SFPS_SCS_Attr5__c,
                                        SFPS_SCS_Attr6__c,
                                        SFPS_SCS_NPS__c,
                                        SFPS_SCS_NPS_Original__c,
                                        SFPS_SCS_TimeStamp_NPS__c,
                                        SFPS_SCS_TimeStamp_NPS_Original__c
                                        From Case where Id = :case1.Id];
                
                //Verify Posts have been marked as processed
                system.assertEquals(true, sp101Check.SFPS_SCS_Case_Data_Processed__c);
                system.assertEquals(true, sp102Check.SFPS_SCS_Case_Data_Processed__c);

                //Verify case attributes populated from opening social post
                system.assertEquals('Social Media', caseCheck.Origin);
                system.assertEquals(sp101.Id, caseCheck.SFPS_SCS_Post_Original__c);
                system.assertEquals(sp101.PostTags, caseCheck.Subject);
                system.assertEquals(sp101.Content, caseCheck.Description);
                system.assertEquals(sp101.Provider, caseCheck.SFPS_SCS_Channel__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Priority__c, caseCheck.SFPS_SCS_Priority__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Influence__c, caseCheck.SFPS_SCS_Influence__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Language__c, caseCheck.SFPS_SCS_Language__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Attr1__c, caseCheck.SFPS_SCS_Attr1__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Attr2__c, caseCheck.SFPS_SCS_Attr2__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Attr3__c, caseCheck.SFPS_SCS_Attr3__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Attr4__c, caseCheck.SFPS_SCS_Attr4__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Attr5__c, caseCheck.SFPS_SCS_Attr5__c);
                system.assertEquals(sp101.SFPS_SCS_Post_Attr6__c, caseCheck.SFPS_SCS_Attr6__c);

                //Verify case NPS attributes populated from original and current social post that attached to the same case
                system.assertEquals(sp101.SFPS_SCS_Post_NPS__c, caseCheck.SFPS_SCS_NPS_Original__c);
                system.assertEquals(sp101.Posted, caseCheck.SFPS_SCS_Timestamp_NPS_Original__c);
                system.assertEquals(sp102.SFPS_SCS_Post_NPS__c, caseCheck.SFPS_SCS_NPS__c);
                system.assertEquals(sp102.Posted, caseCheck.SFPS_SCS_Timestamp_NPS__c);
        }


        @isTest
        static void verifyServiceLevelMetrics()
        {
                insert new SFPS_SCS_Settings__c(Name='CaseOrigin', Enabled__c=true, Value__c='Social Media');
                insert new SFPS_SCS_Settings__c(Name='AssignmentRuleOnNewCase', Enabled__c=false);
                insert new SFPS_SCS_Settings__c(Name='AssignmentRuleOnExistingCase', Enabled__c=false);

                Case case1 = new Case(Subject='Case1');
                insert case1;

                DateTime tBaseTime = system.today().addDays(-7);
                
                SocialPost sp1C1 = new SocialPost();                
                sp1C1.Name='TWEET1C1';
                sp1C1.IsOutbound = false;
                sp1C1.ParentId = case1.Id;
                sp1C1.Provider = 'Twitter';
                sp1C1.Content = 'First Customer Request';
                sp1C1.SFPS_SCS_Case_Data_Processed__c = false;
                sp1C1.Posted = tBaseTime;

                SocialPost sp1A1 = new SocialPost();  
                sp1A1.Name='TWEET1A1';
                sp1A1.IsOutbound = true;
                sp1A1.ParentId = case1.Id;
                sp1A1.Provider = 'Twitter';
                sp1A1.Content = 'First Agent Response';
                sp1A1.SFPS_SCS_Case_Data_Processed__c = false;
                sp1A1.Posted = tBaseTime.addHours(1);

                SocialPost sp1C2 = new SocialPost();                
                sp1C2.Name='TWEET1C2';
                sp1C2.IsOutbound = false;
                sp1C2.ParentId = case1.Id;
                sp1C2.Provider = 'Twitter';
                sp1C2.Content = 'Second Customer Post';
                sp1C2.SFPS_SCS_Case_Data_Processed__c = false;
                sp1C2.Posted = tBaseTime.addHours(2);

                SocialPost sp1A2 = new SocialPost();  
                sp1A2.Name='TWEET1A2';
                sp1A2.IsOutbound = true;
                sp1A2.ParentId = case1.Id;
                sp1A2.Provider = 'Twitter';
                sp1A2.Content = 'Second Agent Response';
                sp1A2.SFPS_SCS_Case_Data_Processed__c = false;
                sp1A2.Posted = tBaseTime.addHours(3);

                SocialPost sp1C3 = new SocialPost();                
                sp1C3.Name='TWEET1C3';
                sp1C3.IsOutbound = false;
                sp1C3.ParentId = case1.Id;
                sp1C3.Provider = 'Twitter';
                sp1C3.Content = 'Second Customer Post';
                sp1C3.SFPS_SCS_Case_Data_Processed__c = false;
                sp1C3.Posted = tBaseTime.addHours(4);

                SocialPost sp1A3 = new SocialPost();  
                sp1A3.Name='TWEET1A3';
                sp1A3.IsOutbound = true;
                sp1A3.ParentId = case1.Id;
                sp1A3.Provider = 'Twitter';
                sp1A3.Content = 'Second Agent Response';
                sp1A3.SFPS_SCS_Case_Data_Processed__c = false;
                sp1A3.Posted = tBaseTime.addHours(5);

                Test.startTest();
                insert sp1C1;    
                Case caseCheck1 = [Select Id, SFPS_SCS_New_Post__c, SFPS_SCS_NumberPosts__c From Case where Id = :case1.Id];
                system.assertEquals(true, caseCheck1.SFPS_SCS_New_Post__c);
                system.assertEquals(1, caseCheck1.SFPS_SCS_NumberPosts__c);

                insert sp1A1;
                Case caseCheck2 = [Select Id, SFPS_SCS_New_Post__c, SFPS_SCS_NumberPosts__c From Case where Id = :case1.Id];
                system.assertEquals(false, caseCheck2.SFPS_SCS_New_Post__c);
                system.assertEquals(2, caseCheck2.SFPS_SCS_NumberPosts__c);

                insert sp1C2;    
                Case caseCheck3 = [Select Id, SFPS_SCS_New_Post__c, SFPS_SCS_NumberPosts__c From Case where Id = :case1.Id];
                system.assertEquals(true, caseCheck3.SFPS_SCS_New_Post__c);
                system.assertEquals(3, caseCheck3.SFPS_SCS_NumberPosts__c);

                insert sp1A2;
                Case caseCheck4 = [Select Id, SFPS_SCS_New_Post__c, SFPS_SCS_NumberPosts__c From Case where Id = :case1.Id];
                system.assertEquals(false, caseCheck4.SFPS_SCS_New_Post__c);
                system.assertEquals(4, caseCheck4.SFPS_SCS_NumberPosts__c);

                insert sp1C3;    
                Case caseCheck5 = [Select Id, SFPS_SCS_New_Post__c, SFPS_SCS_NumberPosts__c From Case where Id = :case1.Id];
                system.assertEquals(true, caseCheck5.SFPS_SCS_New_Post__c);
                system.assertEquals(5, caseCheck5.SFPS_SCS_NumberPosts__c);

                insert sp1A3;
                Case caseCheck6 = [Select Id, SFPS_SCS_New_Post__c, SFPS_SCS_NumberPosts__c From Case where Id = :case1.Id];
                system.assertEquals(false, caseCheck6.SFPS_SCS_New_Post__c);
                system.assertEquals(6, caseCheck6.SFPS_SCS_NumberPosts__c);

                Test.stopTest();

                SocialPost sp1C1Check = [Select Id, SFPS_SCS_Case_Data_Processed__c, CreatedDate From SocialPost Where Id = :sp1C1.Id];
                SocialPost sp1A1Check = [Select Id, SFPS_SCS_Case_Data_Processed__c, CreatedDate From SocialPost Where Id = :sp1A1.Id];
                SocialPost sp1C2Check = [Select Id, SFPS_SCS_Case_Data_Processed__c, CreatedDate From SocialPost Where Id = :sp1C2.Id];
                SocialPost sp1A2Check = [Select Id, SFPS_SCS_Case_Data_Processed__c, CreatedDate From SocialPost Where Id = :sp1A2.Id];
                SocialPost sp1C3Check = [Select Id, SFPS_SCS_Case_Data_Processed__c, CreatedDate From SocialPost Where Id = :sp1C3.Id];
                SocialPost sp1A3Check = [Select Id, SFPS_SCS_Case_Data_Processed__c, CreatedDate From SocialPost Where Id = :sp1A3.Id];

                Case caseCheck = [Select Id, Origin,
                                        SFPS_SCS_Post_Original__c,
                                        SFPS_SCS_AvgResponseTime__c,
                                        SFPS_SCS_AvgUpdateTime__c,
                                        SFPS_SCS_CumulativeUpdateTime__c,
                                        SFPS_SCS_CumulativeResponseTime__c,
                                        SFPS_SCS_FirstInboundPostAt__c,
                                        SFPS_SCS_FirstOutboundPostAt__c,
                                        SFPS_SCS_FirstResponseTime__c,
                                        SFPS_SCS_LastInboundPostAt__c,
                                        SFPS_SCS_LastOutboundPostAt__c,
                                        SFPS_SCS_LastPostAt__c,
                                        SFPS_SCS_LastPostRecordCreatedAt__c,
                                        SFPS_SCS_NumberInboundPosts__c,
                                        SFPS_SCS_NumberOutboundPosts__c,
                                        SFPS_SCS_NumberPosts__c,
                                        SFPS_SCS_NumberUpdatePosts__c
                                        From Case where Id = :case1.Id];
                
                //Verify Inbound Posts have been marked as processed
                system.assertEquals(true, sp1C1Check.SFPS_SCS_Case_Data_Processed__c);
                system.assertEquals(false, sp1A1Check.SFPS_SCS_Case_Data_Processed__c);
                system.assertEquals(true, sp1C2Check.SFPS_SCS_Case_Data_Processed__c);
                system.assertEquals(false, sp1A2Check.SFPS_SCS_Case_Data_Processed__c);
                system.assertEquals(true, sp1C2Check.SFPS_SCS_Case_Data_Processed__c);
                system.assertEquals(false, sp1A3Check.SFPS_SCS_Case_Data_Processed__c);

                //Verify case timestamp field and count field
                system.assertEquals('Social Media', caseCheck.Origin);
                system.assertEquals(sp1C1.Id, caseCheck.SFPS_SCS_Post_Original__c);
                system.assertEquals(sp1C1.Posted, caseCheck.SFPS_SCS_FirstInboundPostAt__c);
                system.assertEquals(sp1A1.Posted, caseCheck.SFPS_SCS_FirstOutboundPostAt__c);
                system.assertEquals(sp1C3.Posted, caseCheck.SFPS_SCS_LastInboundPostAt__c);
                system.assertEquals(sp1A3.Posted, caseCheck.SFPS_SCS_LastOutboundPostAt__c);
                system.assertEquals(sp1A3.Posted, caseCheck.SFPS_SCS_LastPostAt__c);
                system.assertEquals(sp1A3Check.CreatedDate, caseCheck.SFPS_SCS_LastPostRecordCreatedAt__c);
                system.assertEquals(3, caseCheck.SFPS_SCS_NumberInboundPosts__c);
                system.assertEquals(3, caseCheck.SFPS_SCS_NumberOutboundPosts__c);
                system.assertEquals(6, caseCheck.SFPS_SCS_NumberPosts__c);
                system.assertEquals(2, caseCheck.SFPS_SCS_NumberUpdatePosts__c);

                //Verify calculated field (converted in minutes)
                //3 hours total between each of the three agent responses sp1A1, sp1A2, sp1A3 from previous customer post
                Decimal expectedCumulativeResponseTime = 3 * 60;
                //2 hours total between each of the two update customer update sp1C2, sp1C3 from previous agent response
                Decimal expectedCumulativeUpdateTime = 2 * 60;

                //Assert with 5-minute range to address potential rounding error
                system.assert(expectedCumulativeResponseTime - 5.0 < caseCheck.SFPS_SCS_CumulativeResponseTime__c 
                                    && expectedCumulativeResponseTime + 5.0 > caseCheck.SFPS_SCS_CumulativeResponseTime__c,
                                    'Expected approximate value: ' + String.valueOf(expectedCumulativeResponseTime) 
                                    + '; Actual value: ' + String.valueOf(caseCheck.SFPS_SCS_CumulativeResponseTime__c));

                system.assert(expectedCumulativeUpdateTime - 5.0 < caseCheck.SFPS_SCS_CumulativeUpdateTime__c 
                                    && expectedCumulativeUpdateTime + 5.0 > caseCheck.SFPS_SCS_CumulativeUpdateTime__c,
                                    'Expected approximate value: ' + String.valueOf(expectedCumulativeUpdateTime) 
                                    + '; Actual value: ' + String.valueOf(caseCheck.SFPS_SCS_CumulativeUpdateTime__c));
                
                //Note direct formula field are exempted from test as they could be updated by customer with different time unit 
                //For example, # of hours instead of # of minutes
                //SFPS_SCS_FirstResponseTime__c
                //SFPS_SCS_AvgUpdateTime__c
                //SFPS_SCS_AvgResponseTime__c
                
        }
}