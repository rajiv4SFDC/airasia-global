/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created
*******************************************************************/
@isTest(SeeAllData = false)
private class Schedular_EmailAttachmentArchival_Test {
    
    static testMethod void testEmailAttachmentArchivalBatchScheduler() {
        
        //build cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        DateTime currentDateTime = Datetime.now();
        
        // calling scheduler
        Test.StartTest();
        Schedular_EmailAttachmentArchival emailAttachSchedular = new Schedular_EmailAttachmentArchival();
        System.schedule('Email Attachment Archival ' + String.valueOf(currentDateTime), nextFireTime, emailAttachSchedular);
        Test.stopTest();
        
        //making sure job scheduled
        List < CronTrigger > jobs = [
            SELECT Id, CronJobDetail.Name, State, NextFireTime
            FROM CronTrigger
            WHERE CronJobDetail.Name = : 'Email Attachment Archival ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
    }
}