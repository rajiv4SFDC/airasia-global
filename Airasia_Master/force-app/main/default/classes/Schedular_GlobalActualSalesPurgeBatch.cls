global class Schedular_GlobalActualSalesPurgeBatch implements Schedulable {
    // Scheduled job to delete old data from Actual_Sales__c

    // Update 08 Jan 2019 - Charles Thompson, Salesforce Singapore
    // Moved all the preparation logic to a new class GlobalActualSalesPurgeBatch
    // This is because the date used for the batch's query was not changing (because
    // the value of System.now() in a Schedulable class is the time it was scheduled,
    // not the run time)

    global void execute(SchedulableContext ctx) {
        
        // simply call a class to prep the batch job
        GlobalActualSalesPurgeBatch gb = new GlobalActualSalesPurgeBatch(); 
        gb.prepBatch();
        
    } // END - execute()
}