/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created
*******************************************************************/
@isTest
private class Schedular_GlobalAppLogPurgeBatch_Test {
    
    public static testMethod void testGlobalAppLogPurgeBatchScheduler() {
        
        //build cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // call scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        Schedular_GlobalAppLogPurgeBatch globalPurgeBatchSchedular = new Schedular_GlobalAppLogPurgeBatch();
        System.schedule('Global App Logs Purge ' + String.valueOf(currentDateTime), nextFireTime, globalPurgeBatchSchedular);
        Test.stopTest();
        
        // making sure job scheduled
        List < CronTrigger > jobs = [
            SELECT Id, CronJobDetail.Name, State, NextFireTime
            FROM CronTrigger
            WHERE CronJobDetail.Name = : 'Global App Logs Purge ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
    }
}