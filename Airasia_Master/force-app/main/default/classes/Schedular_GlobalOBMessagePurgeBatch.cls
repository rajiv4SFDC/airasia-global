/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created
09/01/2019      Charles Thompson    Fixed mistaken use of DateTime.now() by 
                                    moving this code to a new class
*******************************************************************/
global class Schedular_GlobalOBMessagePurgeBatch implements Schedulable {

    global void execute(SchedulableContext ctx) {
        // original code moved to new class GlobalOBMessagePurgeBatch
        GlobalOBMessagePurgeBatch gobmpb = new GlobalOBMessagePurgeBatch();
        gobmpb.prepQuery();
    } // END - execute()
}