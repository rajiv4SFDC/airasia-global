/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Sharan Desai         Created
*******************************************************************/
@isTest
public class Schedular_GlobalOBMessagePurgeBatch_Test {
    
    public static testMethod void testGlobalAppLogPurgeBatchScheduler() {
        
        //build cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // call scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        Schedular_GlobalOBMessagePurgeBatch globalPurgeBatchSchedular = new Schedular_GlobalOBMessagePurgeBatch();
        System.schedule('Global OBMessage Purge ' + String.valueOf(currentDateTime), nextFireTime, globalPurgeBatchSchedular);
        Test.stopTest();
        
        // making sure job scheduled
        List < CronTrigger > jobs = [
            SELECT Id, CronJobDetail.Name, State, NextFireTime
            FROM CronTrigger
            WHERE CronJobDetail.Name = : 'Global OBMessage Purge ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
    }

}