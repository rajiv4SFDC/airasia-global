/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
12/OCT/2017      Sharan Desai         Created
*******************************************************************/
@isTest
public class Schedular_GlobalTaskPurgeBatch_Test {
    

    
    public static testMethod void testGlobalTaskPurgeBatchScheduler() {
        
        //build cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // call scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        Schedular_GlobalTaskPurgeBatch globalPurgeBatchSchedular = new Schedular_GlobalTaskPurgeBatch();
        System.schedule('Global Task Archival ' + String.valueOf(currentDateTime), nextFireTime, globalPurgeBatchSchedular);
        Test.stopTest();
        
        // making sure job scheduled
        List < CronTrigger > jobs = [
            SELECT Id, CronJobDetail.Name, State, NextFireTime
            FROM CronTrigger
            WHERE CronJobDetail.Name = : 'Global Task Archival ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
    }
}