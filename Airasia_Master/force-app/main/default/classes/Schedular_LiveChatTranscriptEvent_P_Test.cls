/********************************************************************************************************
NAME:			Schedular_LiveChatTranscriptEvent_P_Test
DESCRIPTION:	This is the TEST class for Schedular_LiveChatTranscriptEvent_Purge.apxc

UPDATE HISTORY:
DATE            AUTHOR		COMMENTS
-------------------------------------------------------------
31/08/2018      Aik Meng	First version - cloned source code of Schedular_GlobalAppLogPurgeBatch_Test.apxc
********************************************************************************************************/

@isTest
private  class Schedular_LiveChatTranscriptEvent_P_Test {

    public static testMethod void testScheduler() {
        
        //build cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // call scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        Schedular_LiveChatTranscriptEvent_Purge globalPurgeBatchSchedular = new Schedular_LiveChatTranscriptEvent_Purge();
        System.schedule('Global Purge ' + String.valueOf(currentDateTime), nextFireTime, globalPurgeBatchSchedular);
        Test.stopTest();
        
        // making sure job scheduled
        List < CronTrigger > jobs = [
            SELECT Id, CronJobDetail.Name, State, NextFireTime
            FROM CronTrigger
            WHERE CronJobDetail.Name = : 'Global Purge ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
    }
}