/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created
*******************************************************************/
global class Schedular_RefundCaseUpdateBatch implements Schedulable {
    global String metadata_label_name = 'Refund Case Update Batch';
    global String sobj_api_name;
    global String additionalFilter;
    global Integer batchSize=200;
    global String requiredOperator=' <= ';
    global DateTime StartTime = DateTime.now();
    List < System_Settings__mdt > sysSettingMD;
    
    global void execute(SchedulableContext ctx) {
        // find record ages from setting
        sysSettingMD = [Select Id, Label,Date_Field_API_Name__c, Additional_Filter__c,Batch_Size__c,SObject_Api_Name__c,Debug__c,
                        Info__c,Warning__c,Error__c From System_Settings__mdt Where MasterLabel = : metadata_label_name limit 1];
        String date_field = '';
        String parentQuery = '';
        if (sysSettingMD.size() > 0) {
            date_field = sysSettingMD[0].Date_Field_API_Name__c;
            if(sysSettingMD[0].Batch_Size__c>0)
                batchSize=Integer.valueof(sysSettingMD[0].Batch_Size__c);
            sobj_api_name= sysSettingMD[0].SObject_Api_Name__c;
            
            DateTime rpsStartDateTime = StartTime;
            String rpsStartDateTimeStr = rpsStartDateTime.formatGMT('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
            System.debug('rpsStartDateTimeStr:'+rpsStartDateTimeStr);
            
            // get last run time to run it multiple time 
            String lastRunDateTime = getPCULastRunDateTime();
            if(String.isNotBlank(lastRunDateTime)){
                rpsStartDateTimeStr = lastRunDateTime;
                requiredOperator = ' >= ';
            }
            
            // build parent query
            //parentQuery = 'Select ParentId,COUNT(Id) From ' + sobj_api_name + ' Where ' + date_field + ' >= ' + rpsStartDateTimeStr + ' AND ' + date_field + ' <= ' + rpsEndDateTimeStr;
            parentQuery = 'Select ParentId,COUNT(Id) From ' + sobj_api_name + ' Where ' + date_field + requiredOperator + rpsStartDateTimeStr;
            System.debug('*** parentQuery = ' + parentQuery);
            
            // add any additional query.
            additionalFilter = sysSettingMD[0].Additional_Filter__c;
            if (String.isNotBlank(additionalFilter)) {
                parentQuery = parentQuery + ' AND ' + additionalFilter+ ' group by ParentId';
            }else{
                parentQuery = parentQuery + ' group by ParentId';
            }
        } else {
            parentQuery = '';
        }
        System.debug('*** parentQuery = ' + parentQuery);
        
        // finally call batch   
        ID BatchId = Database.executeBatch(new Batch_RefundCaseUpdate(parentQuery,sysSettingMD[0]),batchSize);
        System.debug('Batch ID:'+BatchId);
    } // END - execute()
    
    // this will provide last run time of batch
    public String getPCULastRunDateTime(){
        if(BATCH_REFUND_CASE__c.getInstance('LAST_RUN_DATE_TIME')!=null){
            DateTime lastRunDateTime = BATCH_REFUND_CASE__c.getInstance('LAST_RUN_DATE_TIME').Last_Run_Date_Time__c;
            if(lastRunDateTime!=null){
                return (lastRunDateTime).formatGMT('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
            }
        }
        return '';
    }
}