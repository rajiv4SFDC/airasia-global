/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar         Created
*******************************************************************/
@isTest
private class Schedular_RefundCaseUpdateBatch_Test {
    
    // created Custom setting
    @testSetup
    static void createBatchRefundCustomSettings(){
        BATCH_REFUND_CASE__c batchRefundCS = new BATCH_REFUND_CASE__c();
        batchRefundCS.Name='LAST_RUN_DATE_TIME';
        batchRefundCS.Last_Run_Date_Time__c = null;
        upsert batchRefundCS;
    }
    
    /** test Schedular_RefundCaseUpdateBatch is scheduling the batch**/
    public static testMethod void testRefundCaseUpdateBatchScheduler() {
        
        //build cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() - 1);
        String ss = String.valueOf(Datetime.now().second()-1);
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';

        // schedule
        Test.StartTest();
        DateTime currentDateTime = Datetime.now();
        Schedular_RefundCaseUpdateBatch refundCaseUpdateBatch = new Schedular_RefundCaseUpdateBatch();
        System.schedule('Refund Case Update Batch ' + String.valueOf(currentDateTime), nextFireTime, refundCaseUpdateBatch);
        Test.stopTest();
        
        // making sure job scheduled
        List < CronTrigger > jobs = [
            SELECT Id, CronJobDetail.Name, State, NextFireTime
            FROM CronTrigger
            WHERE CronJobDetail.Name = : 'Refund Case Update Batch ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
    }
}