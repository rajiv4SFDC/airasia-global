/********************************************************************************************************
NAME:			Scheduled_Get_CCR
AUTHOR:			Aik Meng (aik.seow@salesforce.com)
PURPOSE:		To retrieve currency conversion rates from an external source (Named Credential) and store
				the retrieved data in the custom object AA_Currency_Conversion. The solution includes this
				schedulable Apex class and PB. 
DESCRIPTION:	This Apex class will fetch the currency conversion rates via REST API Callout on regular 
				interval (daily) and inserts the retrieved data in the custom object AA_Currency_Conversion.
				The related PB (Check_Application_Log) will check the Application Log record inserts for
				matching criteria pertaining to an issue raised by this Apex class. When there's a match, 
				the PB will invoke the Email Alert to notify the targeted Users.

UPDATE HISTORY:
DATE            AUTHOR			COMMENTS
-------------------------------------------------------------
17/10/2018      Aik Meng		First version
17/06/2019      Alvin Tayag     Reschedule after 1 hour on failure
********************************************************************************************************/

global class Scheduled_Get_CCR implements Schedulable {

    private static String applicationName = 'AA_Get_CCR';  // used in Process Builder matching criteria
    private static String processLog 	  = '';
    private static DateTime startTime 	  = DateTime.now();
    private static List<System_Settings__mdt> logLevelCMD;
    private Boolean isRetry = false;
	private Integer retryCount = 0;
    global Scheduled_Get_CCR() {
        isRetry = false;
        retryCount= 0;
    }

    global Scheduled_Get_CCR(Boolean retry, Integer count) {
        isRetry = retry;
        retryCount = count;
    }

	global void execute(SchedulableContext ctx) {

        if(isRetry) {
            System.abortJob(ctx.getTriggerId()); //Clear Job
		    futureMethodCallout(isRetry, retryCount);  // WebService Callout from Scheduled Apex needs @future method
        }
        else {
            futureMethodCallout();
        }
        
	} // End - execute()    
    
    @future(callout=true)
    public static void futureMethodCallout(Boolean isRetry, Integer retryCount) {
        calloutProcess(isRetry, retryCount);
    } // End - futureMethodCallout()

    @future(callout=true)
    public static void futureMethodCallout() {
        calloutProcess(false, 0);
    } // End - futureMethodCallout()

    public static void calloutProcess(Boolean isRetry, Integer retryCount) {
        System.debug('Retry --> ' + isRetry);
        List<ApplicationLogWrapper> appWrapperLogList = new List<ApplicationLogWrapper>();
        // QUERY log level settings
        logLevelCMD = Utilities.getLogSettings(applicationName);

        try {
            // PREPARE the HTTP request
            HttpRequest request = new HttpRequest();
            request.setEndPoint('callout:AA_Currency_Conversion_Rates');  // using Named Credential
            request.setMethod('GET');
            request.setTimeout(20000);  // Set timeout to 20 seconds
            
            // EXECUTE the WebService Callout
            Http httpProtocol = new Http();
            HttpResponse response = httpProtocol.send(request);
            
            // RETRIEVE the HTTP response
            String JSONString = response.getBody();
    
            // CHECK the HTTP response status        
            if(response.getStatusCode() == 200) {  // SUCCESS
    
                // Deserialize JSON response body
                List<AA_Currency_Conversion__c> listConversionRecords = 
                    (List<AA_Currency_Conversion__c>) JSON.deserialize(JSONString, List<AA_Currency_Conversion__c>.class);
                Integer recordCount = listConversionRecords.size();
                
                // INSERT the records into object
                if(listConversionRecords.isEmpty()==FALSE && listConversionRecords.size()>0) {
                    insert listConversionRecords;

                    // LOG the success
                    processLog += 'SUCCESS: WebService Callout for retrieval of currency conversion rates\r\n';
                    processLog += 'Number of records inserted: ' + recordCount;
                    appWrapperLogList.add(Utilities.createApplicationLog('Info', 'Scheduled_Get_CCR', 'execute()',
                                                                         null, applicationName, processLog, null, 'Job Log',
                                                                         startTime, logLevelCMD[0], null));
                } else {  // No records

                    throw new CalloutException('No records returned from WS Callout');
                    
                } // End - if listConversionRecords.isEmpty()
                
            } else {  // FAILURE

                throw new CalloutException('HTTP STATUS=' + response.getStatusCode());

            } // End - if response.getStatusCode()
        
        } catch (Exception exceptionGetCCR) {

            System.debug(LoggingLevel.ERROR, exceptionGetCCR);

            // LOG the issue
            if(isRetry) {
                processLog += 'FAILURE: WebService Callout for retrieval of currency conversion rates\r\n';
            }
            else {
                processLog += 'FAILURE ON RETRY: WebService Callout for retrieval of currency conversion rates\r\n';
            }
            //processLog += 'FAILURE: WebService Callout for retrieval of currency conversion rates\r\n';
            processLog += 'Root Cause: ' + exceptionGetCCR.getMessage() + '\r\n';
			appWrapperLogList.add(Utilities.createApplicationLog('Error', 'Scheduled_Get_CCR', 'execute()',
                                                                 null, applicationName, processLog, null, 'Error Log',
                                                                 startTime, logLevelCMD[0], exceptionGetCCR));
            System.debug('Log Size --> ' + appWrapperLogList.size());
            //AT 17 June 2019 - Reschedule after 1 hour
            if(retryCount < 12) {
                Datetime dtResched = system.now();
                dtResched = dtResched.addMinutes(60);
                String day = string.valueOf(dtResched.day());
                String month = string.valueOf(dtResched.month());
                String hour = string.valueOf(dtResched.hour());
                String minute = string.valueOf(dtResched.minute());
                String second = string.valueOf(dtResched.second());
                String year = string.valueOf(dtResched.year());

                String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                retryCount = retryCount + 1;
                String strJobName = 'Retry: Scheduled_Get_CCR ' + strSchedule;
                System.schedule(strJobName, strSchedule, new Scheduled_Get_CCR(true, retryCount));
            }
        } finally {
            // CREATE the log
            if (appWrapperLogList!=null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
            

        } // End - try{} catch{} finally{}
        
    }
}