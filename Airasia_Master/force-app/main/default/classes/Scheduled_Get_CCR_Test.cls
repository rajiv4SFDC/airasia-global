/********************************************************************************************************
NAME:			Scheduled_Get_CCR_Test
AUTHOR:			Aik Meng (aik.seow@salesforce.com)
PURPOSE:		This is the Test class for Scheduled_Get_CCR class

UPDATE HISTORY:
DATE            AUTHOR			COMMENTS
-------------------------------------------------------------
18/10/2018      Aik Meng		First version
********************************************************************************************************/

@isTest(SeeAllData=false)
public class Scheduled_Get_CCR_Test {

     public static testMethod void testScheduler() {
         
        // BUILD cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // INVOKE scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest(); 
        Scheduled_Get_CCR scheduledJob = new Scheduled_Get_CCR();
        System.schedule('Scheduled Get CCR ' + String.valueOf(currentDateTime), nextFireTime, scheduledJob);
        Test.stopTest();
        
        // CHECK that job was scheduled
        List<CronTrigger> jobs = [
            SELECT Id, CronJobDetail.Name, State, NextFireTime
            FROM CronTrigger
            WHERE CronJobDetail.Name = : 'Scheduled Get CCR ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
         
    } // End - testScheduler()

    
    public static testMethod void testCalloutSuccess() {

        Test.startTest();

        // PREPARE Mock Callout
        Test.setMock(HttpCalloutMock.class, new MockCallout_Get_CCR('CalloutSuccess'));  // Mock Callout with HTTP response=200

        // INVOKE WebService Callout
        Scheduled_Get_CCR.futureMethodCallout();

        Test.stoptest();
        
        // CHECK result: 2 records inserted
        List<AA_Currency_Conversion__c> listCCR = [SELECT 
                                                       Id 
                                                   FROM 
                                                       AA_Currency_Conversion__c];
        System.assertEquals(2, listCCR.size());  // Expecting 2 records per the MockCallout class

        // CHECK specific record
        listCCR = [SELECT 
                       Id, 
                       Effective_Date__c, 
                       Source_Currency_Code__c, 
                       Conversion_Rate__c 
                   FROM 
                       AA_Currency_Conversion__c 
                   WHERE 
                       Target_Currency_Code__c='JPY'];
        System.assertEquals(1, listCCR.size());  // Expecting 1 record matching this criteria
        System.assertEquals(Date.newInstance(2018, 10, 18), listCCR[0].Effective_Date__c);  
        System.assertEquals('IDR', listCCR[0].Source_Currency_Code__c);  
        System.assertEquals(0.007705100000000001, listCCR[0].Conversion_Rate__c);  
        List<Application_Log__c> listAppLogs = [SELECT Id FROM Application_Log__c];
        System.assertEquals(1, listCCR.size());  // Expecting 1 record for success callout scenario
        
    } // End - testCalloutSuccess()


    public static testMethod void testCalloutFail() {

        Test.startTest();

        // PREPARE Mock Callout
        Test.setMock(HttpCalloutMock.class, new MockCallout_Get_CCR('CalloutFail'));  // Mock Callout with HTTP response=400

        // INVOKE WebService Callout
        Scheduled_Get_CCR.futureMethodCallout();

        Test.stoptest();
        
        // CHECK result: No records inserted
        List<AA_Currency_Conversion__c> listCCR = [SELECT Id FROM AA_Currency_Conversion__c];
        System.assertEquals(0, listCCR.size());  // Expecting NO records for failed callout scenario
        
    } // End - testCalloutFail



    public static testMethod void testCalloutRetryFail() {

        Test.startTest();

        // PREPARE Mock Callout
        Test.setMock(HttpCalloutMock.class, new MockCallout_Get_CCR('CalloutFail'));  // Mock Callout with HTTP response=400

        // INVOKE WebService Callout
        Scheduled_Get_CCR.futureMethodCallout(true, 0);

        Test.stoptest();
        
        // CHECK result: No records inserted
        List<AA_Currency_Conversion__c> listCCR = [SELECT Id FROM AA_Currency_Conversion__c];
        System.assertEquals(0, listCCR.size());  // Expecting NO records for failed callout scenario
        
    } // End - testCalloutFail
    
    public static testMethod void testCalloutNoRecords() {

        Test.startTest();

        // PREPARE Mock Callout
        Test.setMock(HttpCalloutMock.class, new MockCallout_Get_CCR('CalloutNoRecords'));  // Mock Callout with no records

        // INVOKE WebService Callout
        Scheduled_Get_CCR.futureMethodCallout();

        Test.stoptest();
        
        // CHECK result: No records inserted
        List<AA_Currency_Conversion__c> listCCR = [SELECT Id FROM AA_Currency_Conversion__c];
        System.assertEquals(0, listCCR.size());  // Expecting NO records for failed callout scenario
        
    } // End - testCalloutFail

}