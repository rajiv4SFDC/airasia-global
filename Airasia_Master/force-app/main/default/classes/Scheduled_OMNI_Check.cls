/********************************************************************************************************
NAME:			Scheduled_OMNI_Check
AUTHOR:			Aik Meng (aik.seow@salesforce.com)
PURPOSE:		To proactively monitor and notify AirAisa when OMNI pending requests count is getting 
				close (70K) to the limit (100K). The solution includes this schedulable Apex class and PB. 
DESCRIPTION:	This Apex class will query the OMNI pending requests count on regular interval
				(based on schedule) and	update the count in the AA_NOTIFY custom object. 
				On record update in AA_NOTIFY object, the PB will check if the count exceeds the configured
				threshold value set in custom metadata (Application Setting:AA_Notify:'OMNI Warning').
				if the count exceeds, the PB will invoke the Email Alert and also Post to Chatter feed of
				AA_NOTIFY object.

UPDATE HISTORY:
DATE            AUTHOR			COMMENTS
-------------------------------------------------------------
13/08/2018      Aik Meng		First version
********************************************************************************************************/

global class Scheduled_OMNI_Check implements Schedulable {

    global String metadata_label_name 	= 'AA NOTIFY';
    global String metadata_key_name 	= 'OMNI Warning';
    global List<Application_Setting__mdt> appSettingMD;
    
    global void execute(SchedulableContext ctx) {

		Integer omniWarning 	= 70000;  // Default
        Integer currentCount	= 0;
        
		appSettingMD = [
           SELECT 
               Id,
               Key__c,
               Value__c
           FROM
	           Application_Setting__mdt
           WHERE
	           MasterLabel=:metadata_label_name AND
			   Key__c=:metadata_key_name
           LIMIT 1
		];

        if (appSettingMD.size()>0) {
        	omniWarning = Integer.valueOf(appSettingMD[0].Value__c);
        }
        
        // QUERY the current count of OMNI Pending Routing Requests
        // Related SF article: https://help.salesforce.com/articleView?id=000233885&language=en_US&type=1
        List<AggregateResult> results = [
        	SELECT 
            	count(Id) currentCount
            FROM
            	Case 
            WHERE
            	OwnerId IN (SELECT Id FROM Group WHERE QueueRoutingConfigId != NULL)    
        ];
        
        if (results.size()>0) {
        	currentCount = (Integer) results[0].get('currentCount');
system.debug('OMNI_Check: currentCount='+currentCount);		
        }
        
        // QUERY AA_NOTIFY record
        List<AA_NOTIFY__c> theRecord = [
			SELECT
            	OMNI_Pending_Count__c,
	            OMNI_Warning_Value__c
			FROM
            	AA_NOTIFY__c
            LIMIT 1
        ];
		
        // IF record exists, UPDATE the record with queried count
        if (theRecord.size()>0) {
            theRecord[0].OMNI_Pending_Count__c = currentCount;
            theRecord[0].OMNI_Warning_Value__c = omniWarning;
            UPDATE theRecord; // On record create or update, the PB checks & acts if threshold value is exceeded
system.debug('OMNI_Check: theRecord is updated');		
            
        } else { // IF record does NOT exist, create it
            AA_NOTIFY__c newRecord = new AA_NOTIFY__c(OMNI_Pending_Count__c = currentCount, OMNI_Warning_Value__c = omniWarning);
            INSERT newRecord; // On record create or update, the PB checks & acts if threshold value is exceeded
system.debug('OMNI_Check: newRecord is inserted');		

        }
        
    } // End - execute()    

}