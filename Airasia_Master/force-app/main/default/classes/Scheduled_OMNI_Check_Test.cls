/********************************************************************************************************
NAME:			Scheduled_OMNI_Check_Test
AUTHOR:			Aik Meng (aik.seow@salesforce.com)
PURPOSE:		This is the Test class for Scheduled_OMNI_Check class. 

UPDATE HISTORY:
DATE            AUTHOR			COMMENTS
-------------------------------------------------------------
13/08/2018      Aik Meng		First version
********************************************************************************************************/

@isTest(SeeAllData=false)
public class Scheduled_OMNI_Check_Test {

    public static testMethod void testScheduler() {
        
        // Build cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // Invoke scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        Scheduled_OMNI_Check scheduledJob = new Scheduled_OMNI_Check();
        System.schedule('Scheduled OMNI Check: ' + String.valueOf(currentDateTime), nextFireTime, scheduledJob);
        Test.stopTest();
        
        // Check that job was scheduled
        List < CronTrigger > jobs = [
            SELECT 
            	Id, 
            	CronJobDetail.Name, 
            	State, 
            	NextFireTime
            FROM 
            	CronTrigger
            WHERE 
            	CronJobDetail.Name = : 'Scheduled OMNI Check: ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
        
    } // End - testScheduler()

    
    public static testMethod void testOmniCheck() {
        
        // Build cron expression
        String hour = String.valueOf(Datetime.now().hour());
        String min = String.valueOf(Datetime.now().minute() + 1);
        String ss = String.valueOf(Datetime.now().second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        
        // Create TEST data
        AA_NOTIFY__c newRecord = new AA_NOTIFY__c(OMNI_Pending_Count__c=999, OMNI_Warning_Value__c=0);
        INSERT newRecord;
        
        // Invoke scheduler
        DateTime currentDateTime = Datetime.now();
        Test.StartTest();
        Scheduled_OMNI_Check scheduledJob = new Scheduled_OMNI_Check();
        System.schedule('Scheduled OMNI Check: ' + String.valueOf(currentDateTime), nextFireTime, scheduledJob);
        Test.stopTest();
        
        // Check that job was scheduled
        List < CronTrigger > jobs = [
            SELECT 
            	Id, 
            	CronJobDetail.Name, 
            	State, 
            	NextFireTime
            FROM 
            	CronTrigger
            WHERE 
            	CronJobDetail.Name = : 'Scheduled OMNI Check: ' + String.valueOf(currentDateTime)
        ];
        System.assertNotEquals(null, jobs);
        System.assertEquals(1, jobs.size());
        
        // Verify that TEST record has been updated
        List<AA_NOTIFY__c> theRecord = [
			SELECT
            	OMNI_Pending_Count__c,
	            OMNI_Warning_Value__c
			FROM
            	AA_NOTIFY__c
        ];
        System.assertEquals(1, theRecord.size());   // ONLY 1 record
        Integer queueCount 	 = (Integer) theRecord[0].OMNI_Pending_Count__c;
        Integer warningLevel = (Integer) theRecord[0].OMNI_Warning_Value__c;
        System.assertNotEquals(999, queueCount);	// queueCount has been updated by Scheduled Apex 
        System.assertNotEquals(0, warningLevel);    // warningLevel should be non-zero value from MDT 
        
    } // End - testOmniCheck()    
}