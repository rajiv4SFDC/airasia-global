/*
* @Author: Steven M. Giangrasso
* @Description: APEX Server Side Controller: SearchArticleController 
* sets up the Knowledge Article and Voting data referenced by the client Side Controller
*/

public class SearchArticleController {
    
    @AuraEnabled
    public static String getUserType() {
        return UserInfo.getUserType();
    }
    
    /******************************
     * @Method Name: SearchArticles
     * @Description : This method is uded to search Article in salesforce
     * @Param1: This method accepts a String paramter which is used to search articles in salesforce 
     * @Return Type: This method returns the list of Sobject (KnowledgeArticleVersion) 
     * ***************************/    
    @AuraEnabled
    public static List<Record> SearchArticles(String searchKey) {
        return SearchArticlesLang(searchKey, 'en_GB');
    }
    
    @AuraEnabled
    public static List<Record> SearchArticlesLang(String searchKey, String language) {
        
        List<Record> searchResult = new List<Record>();
        Integer nbResult = 5;
        String status = 'Online';
        
        if(String.isNotBlank(searchKey) && searchKey.length() > 3) {
            String name = '%' + searchKey + '%';
            
            String objectType = 'KnowledgeArticleVersion';
            
            Search.SuggestionOption options = new Search.SuggestionOption();
            Search.KnowledgeSuggestionFilter filters = new Search.KnowledgeSuggestionFilter();
            
            filters.setLanguage(language);
            filters.setPublishStatus(status);
            // 16/7/18: Add data category into Map and List if it's not already in the list
            Map<String, String> catMap = new Map<String, String > ();
            List<String> cats = new List<String> ();
            List<FAQ__DataCategorySelection> FAQData = [SELECT DataCategoryName, 
                                                               DataCategoryGroupName
                                                        FROM   FAQ__DataCategorySelection 
                                                       ] ;
            for (FAQ__DataCategorySelection cat: FAQData ){ 
                if ((! Cats.contains(cat.DataCategoryName)) && 
                    (cat.DataCategoryGroupName == 'AirAsia')) 
                { 
                    catMap.put('AirAsia', cat.DataCategoryName);
                    cats.add(cat.DataCategoryName);
                    system.debug('Cat = '+cat.DataCategoryName);
                }
            }
            
            filters.setDataCategories(catMap);
            
            options.setFilter(filters);
            options.setLimit(nbResult);
            
            Search.SuggestionResults suggestionResults = Search.suggest(searchKey, objectType, options);
            
            for(Search.SuggestionResult result : suggestionResults.getSuggestionResults()) {
                searchResult.add(new Record(result.getSobject()));
                System.debug('id ' + result.getSobject());
            }
            
        } else {
            
            String query = 'SELECT Id, KnowledgeArticleId, Title, UrlName, Summary '+
                           'FROM   KnowledgeArticleVersion '+
                           'WHERE  PublishStatus =\'' +status+ '\' '+
               			   'AND    Id IN (SELECT ParentId FROM FAQ__DataCategorySelection)'+
                           'AND    Language  =\'' + language + '\' ';
            if(String.isNotBlank(searchKey)) {	
                query += ' AND Title like \'%' + searchKey +'%\'';
            }
            query += 'WITH DATA CATEGORY AirAsia__c BELOW All__c ';
            query += 'ORDER BY LastModifiedDate Desc LIMIT ' + nbResult;
            List<KnowledgeArticleVersion> kaList = Database.query(query);
            if(kaList != null && !kaList.isEmpty()) {
                for(KnowledgeArticleVersion kaObject : kaList) {
                    searchResult.add(new Record(kaObject));
                }
            }
        }
        return searchResult; 
    }
    
    /******************************
     * @Method Name: ArticleDetails
     * @Description : This method is uded to return articles with the FAQ__kav TYPE
     * @Param1: This method accepts the articledID as a String parameter
     * @Return Type: This method returns the list of Sobject (KnowledgeArticleVersion) 
     * ***************************/
    @AuraEnabled
    public static Record ArticleDetails(String articleid) {
        Record rec = new Record();
        FAQ__kav faq = [Select id, Title, Summary from FAQ__kav where id =: articleid];
        rec.Title = faq.Title;
        rec.Body = faq.Summary;
        List<Vote> lstVote = [SELECT Id,IsDeleted,Type FROM Vote where ParentId =: articleid and CreatedById =:userinfo.getUserId()];
        rec.Liked = false;
        if(!lstVote.isEmpty())
        {
            rec.Liked = true;
            rec.VoteType=lstVote[0].Type;
            rec.VoteId=lstVote[0].Id;
        }
        
        return rec;
    }
    
    /******************************
     * @Method Name: getArticleDetails
     * @Description : This method is used to return the detail of an article: Title, Body, Summary 
     *                from the Article. This method also returns the Voting data related to the 
     *                standard VOTE object.
     * @Param1: This method accepts the articledID as a String parameter
     * @Return Type: This method returns the list of Sobject (KnowledgeArticleVersion) 
     * ***************************/
    @AuraEnabled
    public static Record getArticleDetails(String articleid) {
        Record rec = new Record();
        KnowledgeArticleVersion objKnowledgeArticleVersion = 
            [SELECT ArticleNumber,
                    //ArticleType,   // No longer available in Lightning Knowledge
                    CreatedDate,
                    FirstPublishedDate,
                    Id,
                    IsLatestVersion,
                    IsVisibleInPrm,
                    KnowledgeArticleId,
                    Language,
                    OwnerId,
                    PublishStatus,
                    SourceId,
                    Summary,
                    Title,
                    UrlName,
                    VersionNumber 
             FROM   KnowledgeArticleVersion 
             WHERE Id = :articleid
            ];
        System.debug('objKnowledgeArticleVersion ' + objKnowledgeArticleVersion);
        FAQ__kav faq = [SELECT id, 
                               Title, 
                               Answer__c 
                        FROM   FAQ__kav 
                        WHERE  id = :articleid
                       ];
        
        rec.Title = objKnowledgeArticleVersion.Title;
        rec.Body = faq.Answer__c;
        rec.SourceArticleId=objKnowledgeArticleVersion.KnowledgeArticleId;
        rec.KnowldgeVersionId=articleid;
        
        //Query the Standard Vote object from the Knowledge article
        List<Vote> lstVote = [SELECT Id,
                                     IsDeleted,
                                     Type 
                              FROM   Vote 
                              WHERE  ParentId = :objKnowledgeArticleVersion.KnowledgeArticleId 
                              AND    CreatedById = :userinfo.getUserId()
                             ];
        rec.Liked = false;
        if(!lstVote.isEmpty())
        {
            rec.Liked = true;
            rec.VoteType=lstVote[0].Type;
            rec.VoteId=lstVote[0].Id;
        }
        
        return rec;
    }
    
    /*
*
*@Description: This method persists the users Vote selection to the Vote object
*/
    @auraEnabled
    public static record CreateVote(string jsonstring,string VoteType)
    {
        
        system.debug('jsonstring-->'+jsonstring);
        system.debug('VoteType-->'+VoteType);
        
        Record  objRecord = (record)JSON.deserialize(jsonstring, record.class);
        string articleid='';
        system.debug('objRecord-->'+objRecord);
        if(objRecord!=null)
        {
            articleid=objRecord.KnowldgeVersionId;
            if(objRecord.Liked==true)
            {
                
                vote objVote=new vote(id=objRecord.VoteId);
                delete objVote;
                
                
            }
            vote objVoteCreate=new vote(parentId= objRecord.SourceArticleId,type=VoteType);
            insert objVoteCreate;
        }
        record rec;
        try{
            rec=getArticleDetails(articleid);
        }catch(Exception e){}
        
        return rec;
        
    }
    
    public class Record {
        @AuraEnabled
        public string KAId { get; set; }
        
        @AuraEnabled
        public Boolean Liked { get; set; }
        
        @AuraEnabled
        public string VoteType{get;set;}
        
        @auraEnabled
        public string VoteId{get;set;}
        
        @AuraEnabled
        public string Title { get; set; }
        
        @AuraEnabled
        public string SourceArticleId{get;set;}
        
        @AuraEnabled
        public string KnowldgeVersionId{get;set;}
        
        @AuraEnabled
        public string Body { get; set; }
        public Record() {}
        public Record(SObject objRecord) {
            KAId = (String)objRecord.get('Id');
            Title = (String)objRecord.get('Title');
            Body = (String)objRecord.get('summary');
        }
    }
    
    /*
    @AuraEnabled
    public static List<Record> getTrendingArticles() {
        ConnectApi.KnowledgeArticleVersionCollection  KACollect  = 
	        ConnectApi.Knowledge.getTrendingArticles('', 5);
        return null;
	} */
    
}