@isTest
public class SearchArticleController_Test {
    public static testmethod void SearchArticleControllerMethod() {
        
        User u = [SELECT Id,
                         UserPermissionsKnowledgeUser,
                         UserPermissionsSupportUser 
                  FROM   User 
                  WHERE  Id = :UserInfo.getUserId() 
                  LIMIT 1
                 ];
        u.UserPermissionsKnowledgeUser = true;
        u.UserPermissionsSupportUser = true;
        update u;
        System.runAs(u) {
            
            // test a long search string
            List<SearchArticleController.Record> recs = SearchArticleController.SearchArticles('test23');
            System.assertEquals(recs.size(), 0); // there shouldn't be any records that match
            
            recs = SearchArticleController.SearchArticles('of'); // test a short search string
            System.assertEquals(recs.size(), 0); // there shouldn't be any records that match
            
            SearchArticleController.getUserType();

            //SearchArticleController.ArticleDetails('ka00l000000CaeqAAC');
            //SearchArticleController.getArticleDetails();
            FAQ__kav KAVersion = new FAQ__kav();
            KAVersion.Title = 'Test';
            KAVersion.UrlName = 'testarticleurl';
            KAVersion.Language='en_US';
            insert KAVersion;
            KAVersion = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Id = :KAVersion.Id];
            KbManagement.PublishingService.publishArticle(KAVersion.KnowledgeArticleId, true);
            
            system.debug('********* '+KAVersion);
            SearchArticleController.Record rec = SearchArticleController.ArticleDetails(KAVersion.Id);
            //KAVersion.SourceId
            SearchArticleController.getArticleDetails(KAVersion.Id);
            
            system.debug('----- '+rec);
            
            String Jstring ='{"Body":"test","KnowldgeVersionId":"'+KAVersion.KnowledgeArticleId+'","Liked":false,"SourceArticleId":"'+KAVersion.KnowledgeArticleId+'","Title":"Title","VoteId":"","VoteType":"Down"}';
            
            
            
            SearchArticleController.CreateVote(Jstring, 'Up');
            SearchArticleController.getArticleDetails(KAVersion.Id);
            SearchArticleController.ArticleDetails(KAVersion.Id);
        }
    }
    
    
}