public class SearchPersonAccountConsoleAppCtrl {
    
    //-- Variable for Application Logging
    private static List < ApplicationLogWrapper > appWrapperLogList = null;
    public  boolean throwUnitTestExceptions {set;get;}
    private System_Settings__mdt logLevelCMD{get;set;}
    private static String processLog = '';
    private static DateTime startTime = DateTime.now();
    private static String payLoad = '';
    
    //-- Variable for Find Person Interface returned Objects
    //AT 30 April 2019 - ACE to ACE-GCP Upgrade
    //private AsyncTempuriOrgPersn.FindPersonsResponse_elementFuture findPersonResponseFuture; 
    //public schemasDatacontractOrg200407AceEntPersn.FindPersonResponseData findPersonResponseData{get;set;}
    private AsyncACEPersonService.FindPersonsResponse_elementFuture findPersonResponseFuture; 
    public ACEPersonService.FindPersonResponseData findPersonResponseData{get;set;}
    
    //-- Variable for Class level fields
    public String searchEmailAddress{set;get;}
    public String redirectURL{set;get;}    
    public List<FindPersonDO> findPersonDOList{set;get;}
    public String personEmailAddress{get;set;}
   
    //-- variable for Person Account found in SF
    public String personAccountId{get;set;}
    public String personAccountContactId{get;set;}
    public String personAccountName{get;set;}
    public String personAccountPh{get;set;}
    
    public String errorMessage{get;set;}
    public boolean isErrorOccured{get;set;} 
    public boolean pageRenderedFlag{get;set;}
    public String caseURLPrefix{get;set;}
    public String caseURLSuffix{get;set;}
    
    public boolean noResultFound{get;set;}
    
    //-- Variable for Class level Contants
    public static final String NO_PERSON_DATA_FOUND ='No Result Found.'; 
    public static final String NO_PERSON_EMAIL_ADDRESS_FOUND ='Please Enter Valid Email address.'; 
    
    
    //-- class constructor
    public SearchPersonAccountConsoleAppCtrl(){
        
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('SearchPersonAccoount_Log_Level_CMD_NAME'));
        pageRenderedFlag=false;
        noResultFound=false;
        
        //-- SOQL Query to fetch the CASE URL
         List<External_URLS__mdt> externalURLObjList = [SELECT URL__c,MasterLabel FROM External_URLS__mdt WHERE MasterLabel IN ('CaseURLPrefix','CaseURLSuffix')];
         
        System.debug('externalURLObjList'+externalURLObjList);
        for(External_URLS__mdt externalURLObj : externalURLObjList){            
            if(externalURLObj.MasterLabel=='CaseURLPrefix'){                         
                caseURLPrefix=externalURLObj.URL__c;
            }else if(externalURLObj.MasterLabel=='CaseURLSuffix'){
                caseURLSuffix=externalURLObj.URL__c;
            }
        }        
    } 
    
    /**
    * This method is called on load of the Visulaforce page to get the ACE session token and initiated the
    * FindPerson WS call to get the history details using email address as input criteria.
    * Continuation framework is used to make an asynchronous call.
    **/
    public Object startAceFindPerson(){
        
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        processLog='';
        pageRenderedFlag = false;
        redirectURL='';
        errorMessage = '';
        isErrorOccured =false;
        personAccountId='';
        personAccountContactId='';
        personAccountName='';
        personAccountPh='';
        payLoad = '';
        try{
            
            //-- get the email address
            personEmailAddress = searchEmailAddress;   
            
            //-- check whether the email address is valid enough to perform web service operation
            String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
            Pattern emailPattern = Pattern.compile(emailRegex);
            Matcher emailPatternMatcher = emailPattern.matcher(personEmailAddress);

            
            //-- check if email address is authentic to proceed further operations
            if(personEmailAddress!=null && personEmailAddress.length()>0 && emailPatternMatcher.matches()){
                
                Continuation con = new Continuation(60);
                 //-- Code block for unit test case
                if (Test.isRunningtest() && throwUnitTestExceptions) {
                    con=null;
                }   
                con.continuationMethod='processAceFindPersonResponse';
                
                // get Ace Session ID
                String aceSessionID = Utilities.getAceSessionID();
                
                // Build Request for findPerson
                //AT 30 April 2019 - ACE to ACE-GCP Upgrade
    			//schemasDatacontractOrg200407AceEntPersn.FindPersonRequest findPersonInput = new schemasDatacontractOrg200407AceEntPersn.FindPersonRequest();
                ACEPersonService.FindPersonRequest findPersonInput = new ACEPersonService.FindPersonRequest();
                findPersonInput.EmailAddress=personEmailAddress;
                findPersonInput.PersonType='Customer';
                //AT 30 April 2019 - ACE to ACE-GCP Upgrade
                //findPersonInput.ActiveOnly='true';
                findPersonInput.ActiveOnly=true;
                
                //-- Logging Input Payload
                payLoad += 'Input SOAP Message : \r\n';
                payLoad += findPersonInput.toString() + '\r\n';
                
                // call FindPerson
                //AT 30 April 2019 - ACE to ACE-GCP Upgrade
                //AsyncTempuriOrgPersn.AsyncbasicHttpsPersonService asyncPort = new AsyncTempuriOrgPersn.AsyncbasicHttpsPersonService();
                AsyncACEPersonService.AsyncBasicHttpsBinding_IPersonService asyncPort = new AsyncACEPersonService.AsyncBasicHttpsBinding_IPersonService();
                findPersonResponseFuture = asyncPort.beginFindPersons(con,aceSessionID,findPersonInput);
                
                return con;
            }else{
                
                //-- flag this transaction as error
                searchEmailAddress='';
                isErrorOccured =true;
                errorMessage = NO_PERSON_EMAIL_ADDRESS_FOUND;
            }            
        }catch(Exception findPersonSessionResponseFailedEx){
            
            //-- flag this transaction as error
            isErrorOccured =true;
            errorMessage = findPersonSessionResponseFailedEx.getMessage();
            
            //-- Perform Application logging
            processLog += 'Failed to get ACE Session ID for Find Person Inteface: \r\n';
            processLog += findPersonSessionResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'SearchPersonAccountConsoleAppCtrl', 'startAceFindPerson',
                                                                 null, 'Find Person Interface Session ID Call', processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, findPersonSessionResponseFailedEx));
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }        
        return null;
    }
    
   /**
    * This method parses the WS response data and builds required Do for display on UI
    **/
    public Object processAceFindPersonResponse(){
        
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        processLog='';
        errorMessage = '';
        isErrorOccured =false;
        payLoad='';
        try{
            
            //-- get the WS response data
            findPersonResponseData = findPersonResponseFuture.getValue();
          
            System.debug('personEmailAddress'+personEmailAddress);
            
            //-- code to check whether person account with matching email address exists in the SF
            List<Account> personAccList = [SELECT ID,PersonContactId,Name,Phone FROM Account WHERE PERSONEMAIL=:personEmailAddress AND IsPERSONACCOUNT=true ORDER BY LASTMODIFIEDDATE DESC LIMIT 1];
            if(personAccList!=null && personAccList.size()>0){                
                personAccountId= personAccList.get(0).Id;
                personAccountContactId= personAccList.get(0).PersonContactId;
                personAccountName=personAccList.get(0).Name;
                personAccountPh=personAccList.get(0).Phone;                
            }
            
            System.debug('personAccList'+personAccList);
            
            //-- If the Response object is null or has no required object
            if(findPersonResponseData!=null && findPersonResponseData.FindPersonList!=null && findPersonResponseData.FindPersonList.FindPersonItem.size()>0){                
                
                //-- build log content
                payLoad += 'Output SOAP Message : \r\n';
                payLoad += findPersonResponseData.toString() + '\r\n';
                
                noResultFound = false;
                findPersonDOList = new List<FindPersonDO>();
                //AT 30 April 2019 - ACE to ACE-GCP Upgrade
                //for(schemasDatacontractOrg200407AceEntPersn.FindPersonItem eachFindPerson : findPersonResponseData.FindPersonList.FindPersonItem){
                for(ACEPersonService.FindPersonItem eachFindPerson : findPersonResponseData.FindPersonList.FindPersonItem){
                    //-- write json object to be populated on case creation page layout
                    JSONGenerator jsonObject =  JSON.createGenerator(true);
                    jsonObject.writeStartObject();
                    jsonObject.writeNumberField(WS_PersonConstants.PERSON_ID, eachFindPerson.PersonID);
                    jsonObject.writeStringField(WS_PersonConstants.PERSON_TYPE,eachFindPerson.PersonType);
                    jsonObject.writeStringField(WS_PersonConstants.PHONE_NUMBER,eachFindPerson.PhoneNumber);
                    jsonObject.writeStringField(WS_PersonConstants.STATUS,eachFindPerson.Status);
                    jsonObject.writeStringField(WS_PersonConstants.EMAIL,eachFindPerson.EMail);
                    
                    FindPersonDO eachFindPersonObj = new FindPersonDO();
                    //-- Get the Person basic details
                    eachFindPersonObj.customerNumber = eachFindPerson.CustomerNumber;
                    eachFindPersonObj.personID = eachFindPerson.PersonID;
                    eachFindPersonObj.personType = eachFindPerson.PersonType;
                    eachFindPersonObj.phoneNumber = eachFindPerson.PhoneNumber;
                    eachFindPersonObj.status = eachFindPerson.Status;
                    eachFindPersonObj.EMail = eachFindPerson.EMail;
                    
                    String customerName = '';
                    if(eachFindPerson.Name!=null){
                        customerName = eachFindPerson.Name.FirstName;
                        customerName = customerName.trim()+' '+eachFindPerson.Name.MiddleName;
                        customerName = customerName.trim()+' '+eachFindPerson.Name.LastName;
                        eachFindPersonObj.CustomerName=customerName.trim();
                        jsonObject.writeStringField(WS_PersonConstants.FIRST_NAME,eachFindPerson.Name.FirstName);
                        jsonObject.writeStringField(WS_PersonConstants.LAST_NAME,eachFindPerson.Name.LastName);
                        jsonObject.writeStringField(WS_PersonConstants.MIDDLE_NAME,eachFindPerson.Name.MiddleName);
                    }else{
                        jsonObject.writeStringField(WS_PersonConstants.FIRST_NAME,'');
                        jsonObject.writeStringField(WS_PersonConstants.LAST_NAME,'');
                        jsonObject.writeStringField(WS_PersonConstants.MIDDLE_NAME,'');
                    }
                    
                    //-- Get the Person Address Details
                    //AT 30 April 2019 - ACE to ACE-GCP Upgrade
                    //schemasDatacontractOrg200407AceEntPersn.Address addressDetails  = eachFindPerson.Address;                
                    ACEPersonService.Address addressDetails  = eachFindPerson.Address;                
                    if(addressDetails!=null){
                        
                        jsonObject.writeStringField(WS_PersonConstants.ADDRESS_LINE_1,addressDetails.AddressLine1);
                        jsonObject.writeStringField(WS_PersonConstants.ADDRESS_LINE_2,addressDetails.AddressLine2);
                        jsonObject.writeStringField(WS_PersonConstants.ADDRESS_LINE_3,addressDetails.AddressLine3);
                        jsonObject.writeStringField(WS_PersonConstants.CITY,addressDetails.City);
                        jsonObject.writeStringField(WS_PersonConstants.COUNTRY_CODE,addressDetails.CountryCode);
                        jsonObject.writeStringField(WS_PersonConstants.POSTAL_CODE,addressDetails.PostalCode);
                        jsonObject.writeStringField(WS_PersonConstants.PROVINCE_STATE,addressDetails.ProvinceState);
                        
                    }                        
                    jsonObject.writeEndObject();  
                    eachFindPersonObj.fieldSetJsonString=jsonObject.getAsString();       
                    
                    //-- form the case url
                    String newCaseURLPrefix = caseURLPrefix+caseURLSuffix.replace('CUSTOMERNUMBER',eachFindPerson.CustomerNumber);
                    newCaseURLPrefix = newCaseURLPrefix.replace('JSONSTRING',eachFindPersonObj.fieldSetJsonString);
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONACCID',personAccountId);
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONACCCONID',personAccountContactId);
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONEMAIL',personEmailAddress);
                    
                    eachFindPersonObj.caseURL=newCaseURLPrefix;
                    findPersonDOList.add(eachFindPersonObj);     
                    
                }                    
                searchEmailAddress='';                    
            }else{
                
                //-- block to display the SF Person Account when no record found from Navaitaire
                if(personAccountId!=null && personAccountId.length()>0){
                  
                  
                    findPersonDOList = new List<FindPersonDO>();
                    FindPersonDO eachFindPersonObj = new FindPersonDO();
                    //-- Get the Person basic details
                    eachFindPersonObj.customerNumber = '';
                    eachFindPersonObj.phoneNumber = personAccountPh;
                    eachFindPersonObj.EMail = personEmailAddress;
                    eachFindPersonObj.CustomerName=personAccountName;
                    
                    //-- form the case url
                    String newCaseURLPrefix = caseURLPrefix+caseURLSuffix.replace('CUSTOMERNUMBER','');
                    newCaseURLPrefix = newCaseURLPrefix.replace('JSONSTRING','');
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONACCID',personAccountId);
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONACCCONID',personAccountContactId);
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONEMAIL',personEmailAddress);
                    
                    eachFindPersonObj.caseURL=newCaseURLPrefix;
                    findPersonDOList.add(eachFindPersonObj);
                    noResultFound = false;  
                    System.debug('findPersonDOList'+findPersonDOList);                    
                    
                }else{
                    //&00N0l000000Mkr0=PERSONEMAIL
                    //-- form the case url
                    String newCaseURLPrefix = caseURLPrefix+caseURLSuffix.replace('CUSTOMERNUMBER','');
                    newCaseURLPrefix = newCaseURLPrefix.replace('JSONSTRING','');
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONACCID','');
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONACCCONID','');
                    newCaseURLPrefix = newCaseURLPrefix.replace('PERSONEMAIL',personEmailAddress);
                    caseURLPrefix=newCaseURLPrefix;
                    
                    noResultFound = true;                    
                    //-- get the exception message from WS response
                    errorMessage =  findPersonResponseData.ExceptionMessage;
                    if(errorMessage==null || errorMessage=='null' || errorMessage.length()<=0){
                        errorMessage = NO_PERSON_DATA_FOUND;     
                    }
                    
                    //-- Perform Application logging to note that there is no response data recieved
                    //appWrapperLogList.add(Utilities.createApplicationLog('Error', 'SearchPersonAccountConsoleAppCtrl', 'processAceFindPersonResponse',
                    //                                                     null, 'Search Person Account Find Person Interface Response Call', errorMessage, null, 'Error Log',  startTime, logLevelCMD, null));
                    
                }               
               
            } 
        }catch(Exception findPersonResponseFailedEx){
            
            isErrorOccured =true;
            errorMessage = findPersonResponseFailedEx.getMessage();
            
            //-- Perform Application logging
            processLog += 'Failed to get Find Person Response Data for Search Person Account Functionality: \r\n';
            processLog += findPersonResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'SearchPersonAccountConsoleAppCtrl', 'processAceFindPersonResponse',
                                                                 null, 'Search Person Account Find Person Interface Response Call', processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, findPersonResponseFailedEx));
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
        return null;
    }
}