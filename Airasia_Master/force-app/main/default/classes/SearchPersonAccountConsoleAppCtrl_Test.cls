@isTest(SeeAllData=false)
public class SearchPersonAccountConsoleAppCtrl_Test {
    
    /**
     * Scenario 1 : When Person Account with email found in Navitaire system 
     * 
     * */
    public static testmethod void testPersonAccountFoundInNavitaire() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();        
       
        SearchPersonAccountConsoleAppCtrl controller = new SearchPersonAccountConsoleAppCtrl();
        controller.searchEmailAddress='sharandesai.sd@gmail.com';
        controller.throwUnitTestExceptions=false;
        
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation) controller.startAceFindPerson();
        
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
       
         // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        //AT - 30 April 2019 - ACE to ACE-GCP Upgrade
        //String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>518</a:NSProcessTime><a:ProcessTime>521</a:ProcessTime><a:FindPersonList><a:FindPersonItem><a:Address><a:AddressLine1>CCC</a:AddressLine1><a:AddressLine2/><a:AddressLine3/><a:City>Sepang</a:City><a:CountryCode>MY</a:CountryCode><a:PostalCode>88888</a:PostalCode><a:ProvinceState>MY</a:ProvinceState></a:Address><a:CustomerNumber>2136815211</a:CustomerNumber><a:EMail>sharandesai.sd@gmail.com</a:EMail><a:Name><a:FirstName>Test User</a:FirstName><a:LastName>Tai</a:LastName><a:MiddleName/><a:Suffix/><a:Title>MS</a:Title></a:Name><a:PersonID>3607706</a:PersonID><a:PersonType>Customer</a:PersonType><a:PhoneNumber>601266778899</a:PhoneNumber><a:Status>Active</a:Status></a:FindPersonItem></a:FindPersonList><a:OtherServiceInformations i:nil="true"/></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
       String responseXML = '<s:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' +
                                '<s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult><NSProcessTime>878</NSProcessTime><ProcessTime>979</ProcessTime><FindPersonList>' +
                                '<FindPersonItem><PersonID>36123984</PersonID><PersonType>Customer</PersonType><Status>Active</Status><CustomerNumber>7380282365</CustomerNumber><Name>' +
                                '<Title>MS</Title><FirstName>Christy</FirstName><MiddleName/><LastName>Testing</LastName><Suffix/></Name><PhoneNumber>000000000</PhoneNumber>' +
                                '<EMail>hiketa@mail-list.top</EMail><Address><AddressLine1/><AddressLine2/><AddressLine3/><City/><ProvinceState/>' +
                                '<PostalCode/><CountryCode/></Address></FindPersonItem></FindPersonList></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
                
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(controller, conti);
        System.debug(controller);
       
        // result is the return value of the callback
        System.assertEquals(null, result);
        
        //-- Assert to state that Records found either Navitaire Or SF 
        System.assert(!controller.noResultFound);        
        
        //-- Assert to state that No error occured while processing the response
        System.assert(!controller.isErrorOccured);        
      
        Test.stopTest();
        
    }
    
     /**
     * Scenario 2 : When Person Account not found in both Navitaire system and SF
     * 
     * */
    public static testmethod void testPersonAccountNotFoundInNavitaireAndSF() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();  
        
        SearchPersonAccountConsoleAppCtrl controller = new SearchPersonAccountConsoleAppCtrl();
        controller.searchEmailAddress='sharandesai.sd@gmail.com';
        controller.throwUnitTestExceptions=false;
        
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation) controller.startAceFindPerson();
        
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
       
         // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        //AT - 30 April 2019 - ACE to ACE-GCP Upgrade
        //String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>436</a:NSProcessTime><a:ProcessTime>439</a:ProcessTime><a:FindPersonList i:nil="true"/><a:OtherServiceInformations i:nil="true"/></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        String responseXML='<s:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult><NSProcessTime>453</NSProcessTime><ProcessTime>463</ProcessTime></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
                
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(controller, conti);
        System.debug(controller);
       
        // result is the return value of the callback
        System.assertEquals(null, result);
        
      
        //-- Assert to state that No error occured while processing the response
        System.assert(!controller.isErrorOccured);        
        
        //-- Assert to state that No Records found from both Navitaire and SF 
         System.assert(controller.noResultFound);        
        
        Test.stopTest();
        
    }
    
     /**
     * Scenario 3 : When Search is done with Empty Email Address
     * 
     * */
    public static testmethod void testSearchPersonAccountWithEmptyEmailAddress() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();  
        
        SearchPersonAccountConsoleAppCtrl controller = new SearchPersonAccountConsoleAppCtrl();
        controller.searchEmailAddress='';
        
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation) controller.startAceFindPerson();
        
        //-- Assert to check that no WS request has been initiated
        system.assertEquals(null,conti);// (requests.size() <= 0);
       
        //-- Assert to state that request has been stated as Error since search is done with email address
        System.assert(controller.isErrorOccured);        
        
        Test.stopTest();
        
    }
    
     /**
     * Scenario 4 : When Exception occurs before initiating the Request
     * 
     * */
   public static testmethod void testExceptionScenario() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();  
       
       Integer preLogCount = [SELECT COUNT() FROM Application_Log__c];
        
        SearchPersonAccountConsoleAppCtrl controller = new SearchPersonAccountConsoleAppCtrl();
        controller.searchEmailAddress='sharandesai.sd@gmail.com';
        controller.throwUnitTestExceptions=true;
        
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation) controller.startAceFindPerson();
       
       Integer postLogCount = [SELECT COUNT() FROM Application_Log__c];
       
        //-- Assert to check that no WS request has been initiated
        system.assertEquals(null,conti);// (requests.size() <= 0);
       
        //-- Assert to state that request has been stated as Error since search is done with email address
        System.assert(controller.isErrorOccured);     
       
        //-- Assert to check that On Exception case Application Log has been created
        System.assert(postLogCount>preLogCount);     
        
        Test.stopTest();
        
    }
    
     /**
     * Scenario 5 : When Person Account not found in Navitaire system but exists in SF
     * 
     * */
    public static testmethod void testPersonAccountNotFoundInNavitaireButExistsInSF() {
        
        //-- Get record Type
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        
        // create test Person Account       
        Account newPersonAccount = new Account();
        newPersonAccount.FirstName = 'Sharan';
        newPersonAccount.LastName = 'Desai';
        newPersonAccount.PersonEmail = 'sharandesai.sd@gmail.com';
        newPersonAccount.RecordType = personAccountRecordType;
        insert newPersonAccount;
        
         // set mock implementation   
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();  
        
        SearchPersonAccountConsoleAppCtrl controller = new SearchPersonAccountConsoleAppCtrl();
        controller.searchEmailAddress='sharandesai.sd@gmail.com';
        controller.throwUnitTestExceptions=false;
        
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation) controller.startAceFindPerson();
        
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
       
         // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        //AT - 30 April 2019 - ACE - ACE-GCP Upgrade
        //String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>436</a:NSProcessTime><a:ProcessTime>439</a:ProcessTime><a:FindPersonList i:nil="true"/><a:OtherServiceInformations i:nil="true"/></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        String responseXML='<s:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult><NSProcessTime>453</NSProcessTime><ProcessTime>463</ProcessTime></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
                
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(controller, conti);
        System.debug(controller);
       
        // result is the return value of the callback
        System.assertEquals(null, result);
        
        //-- Assert to state that No error occured while processing the response
        System.assert(!controller.isErrorOccured);        
        
        //-- Assert to state that Records found Either from SF or Navitaire
         System.assert(!controller.noResultFound);        
        
         //-- Assert to state that Records found Either from SF or Navitaire
         System.assertEquals(1,controller.findPersonDOList.size());        
        
        Test.stopTest();
        
    }
    
     /**
     * Scenario 6 : When Person Account found in Navitaire system has no Name Element details
     * 
     * */
    public static testmethod void testPersonAccountFoundHasNoNameElementDetails() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();        
       
        SearchPersonAccountConsoleAppCtrl controller = new SearchPersonAccountConsoleAppCtrl();
        controller.searchEmailAddress='sharandesai.sd@gmail.com';
        controller.throwUnitTestExceptions=false;
        
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation) controller.startAceFindPerson();
        
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
       
         // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        //AT - 30 April 2019 - ACE to ACE-GCP Upgrade
        //String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>518</a:NSProcessTime><a:ProcessTime>521</a:ProcessTime><a:FindPersonList><a:FindPersonItem><a:Address><a:AddressLine1>CCC</a:AddressLine1><a:AddressLine2/><a:AddressLine3/><a:City>Sepang</a:City><a:CountryCode>MY</a:CountryCode><a:PostalCode>88888</a:PostalCode><a:ProvinceState>MY</a:ProvinceState></a:Address><a:CustomerNumber>2136815211</a:CustomerNumber><a:EMail>sharandesai.sd@gmail.com</a:EMail><a:PersonID>3607706</a:PersonID><a:PersonType>Customer</a:PersonType><a:PhoneNumber>601266778899</a:PhoneNumber><a:Status>Active</a:Status></a:FindPersonItem></a:FindPersonList><a:OtherServiceInformations i:nil="true"/></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        String responseXML = '<s:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' +
                                '<s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult><NSProcessTime>878</NSProcessTime><ProcessTime>979</ProcessTime><FindPersonList>' +
                                '<FindPersonItem><PersonID>36123984</PersonID><PersonType>Customer</PersonType><Status>Active</Status><CustomerNumber>7380282365</CustomerNumber><Name>' +
                                '<Title>MS</Title><FirstName>Christy</FirstName><MiddleName/><LastName>Testing</LastName><Suffix/></Name><PhoneNumber>000000000</PhoneNumber>' +
                                '<EMail>hiketa@mail-list.top</EMail><Address><AddressLine1/><AddressLine2/><AddressLine3/><City/><ProvinceState/>' +
                                '<PostalCode/><CountryCode/></Address></FindPersonItem></FindPersonList></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
                
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(controller, conti);
        System.debug(controller);
       
        // result is the return value of the callback
        System.assertEquals(null, result);
        
        //-- Assert to state that Records found either Navitaire Or SF 
        System.assert(!controller.noResultFound);        
        
        //-- Assert to state that No error occured while processing the response
        System.assert(!controller.isErrorOccured);        
      
        Test.stopTest();
        
    }
    
     /**
     * Scenario 7 : When Exception occurs while processing the request
     * 
     * */
    public static testmethod void testExceptionCase() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();        
       
        SearchPersonAccountConsoleAppCtrl controller = new SearchPersonAccountConsoleAppCtrl();
        controller.searchEmailAddress='sharandesai.sd@gmail.com';
        controller.throwUnitTestExceptions=false;
        
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation) controller.startAceFindPerson();
        
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
       
         // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>518</a:NSProcessTime><a:ProcessTime>521</a:ProcessTime><a:FindPersonLiCC</a:AddressLine1><a:AddressLine2/><a:AddressLine3/><a:City>Sepang</a:City><a:CountryCode>MY</a:CountryCode><a:PostalCode>88888</a:PostalCode><a:ProvinceState>MY</a:ProvinceState></a:Address><a:CustomerNumber>2136815211</a:CustomerNumber><a:EMail>sharandesai.sd@gmail.com</a:EMail><a:PersonID>3607706</a:PersonID><a:PersonType>Customer</a:PersonType><a:PhoneNumber>601266778899</a:PhoneNumber><a:Status>Active</a:Status></a:FindPersonItem></a:FindPersonList><a:OtherServiceInformations i:nil="true"/></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
                
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(controller, conti);
        System.debug(controller);
       
        // result is the return value of the callback
        System.assertEquals(null, result);
        
        //-- Assert to state that Records found either Navitaire Or SF 
        System.assert(!controller.noResultFound);        
        
        //-- Assert to state that error occured while processing the response
        System.assert(controller.isErrorOccured);        
      
        Test.stopTest();
        
    }
    
    @isTest public static void testCodeCoverageForRequest(){
        
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        //AT - 30 April 2019 - ACE to ACE-GCP Upgrade
        //tempuriOrgPersn.basicHttpsPersonService basicHttpsObj1 = new tempuriOrgPersn.basicHttpsPersonService();
        ACEPersonService.BasicHttpsBinding_IPersonService basicHttpsObj1 = new ACEPersonService.BasicHttpsBinding_IPersonService();
        //basicHttpsObj1.FindPersons('',new schemasDatacontractOrg200407AceEntPersn.FindPersonRequest());
        basicHttpsObj1.FindPersons('',new ACEPersonService.FindPersonRequest());
        Test.stopTest();
        
        
    }

   
}