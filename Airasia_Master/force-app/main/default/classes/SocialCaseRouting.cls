/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/06/2017      Pawan Kumar         Created
25/03/2019		Charles Thompson    Added support for RBV social cases
*******************************************************************/
public class SocialCaseRouting {
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    private static String processLog = '';
    private static List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
    private static final String DELIMITER = '#~#';
    @TestVisible
    private static boolean isInvalidPickTest = false;
    
    @InvocableMethod
    public static void assignCaseToRelevantQueue(List<Id> caseIds) {
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Routing_Log_Level_CMD_NAME'));
        
        try {
            
            List<Case> updateCaseList;
            
            List<Case> caseList = [SELECT Id, 
                                          Origin, 
                                          Priority, 
                                          Case_Language__c,
                                   		  RecordTypeId
                                   FROM   Case 
                                   WHERE  Id IN :caseIds
                                  ];
            
            if (!caseList.isEmpty()) {
                
                updateCaseList = new List<Case>();
                
                Map<String, String> socialCaseRoutingMap = new Map<String, String>();
                List<Social_Case_Routing__mdt> socialCaseRoutingList = [SELECT Id, 
                                                                               Case_Origin__c, 
                                                                               Case_Priority__c, 
                                                                               Case_Language__c, 
                                                                               Queue__c 
                                                                        FROM   Social_Case_Routing__mdt
                                                                       ];
                
                for (Social_Case_Routing__mdt eachRow: socialCaseRoutingList) {
                    
                    // RBV enhancement - 25/03/2019
                    // Indicate whether the queue name is for an RBV business
                    String queueType = 'Airline';
                    if (eachRow.Queue__c.containsIgnoreCase('RBV')){
                        queueType = 'RBV';
                    }
                    socialCaseRoutingMap.put(queueType + DELIMITER +
                                                        eachRow.Case_Origin__c + DELIMITER + 
                                                        eachRow.Case_Priority__c + DELIMITER + 
                                                        eachRow.Case_Language__c, 
                                             eachRow.Queue__c);
                    // end RBV code - 25/03/2019
                }
                //System.debug('socialCaseRoutingMap:'+JSON.serializePretty(socialCaseRoutingMap));
                
                for (Case caseToBeAssignedToQueue: caseList) {

                    // meta data query does not count against governor limit.
                    String caseId = caseToBeAssignedToQueue.id;
                    
                    /*****  Charles Thompson 25/03/2019
				     * RBV social cases also have origin = facebook or twitter
                     * We need to assign queue based on record type (which includes 
                     * the various RBV business names)
                     * ********************************/
                    Id caseRecTypeId = caseToBeAssignedToQueue.RecordTypeId;
                    String caseRecType = Schema.SObjectType.Case
                                             .getRecordTypeInfosById()
                                             .get(caseRecTypeId).getName();
                    String caseType = 'Airline';
                    if (caseRecType.containsIgnoreCase('loyalty') ||
                        caseRecType.containsIgnoreCase('rokki') ||
                        caseRecType.containsIgnoreCase('ourshop') ||
                        caseRecType.containsIgnoreCase('travel') ||
                        caseRecType.containsIgnoreCase('vidi') 
                       ){
                           caseType = 'RBV';
                       }
                    // end RBV code - 25/03/2019
                        
                    try {
                        
                        String criteriaKey = caseType + DELIMITER +   // RBV - 25/03/2019
                            				 caseToBeAssignedToQueue.Origin + DELIMITER + 
                                             caseToBeAssignedToQueue.Priority + DELIMITER + 
                                             caseToBeAssignedToQueue.Case_Language__c;
                        
                        if (socialCaseRoutingMap.containsKey(criteriaKey)) {
                            caseToBeAssignedToQueue.OwnerId = 
                                Utilities.getQueueIdByName(socialCaseRoutingMap.get(criteriaKey));
                        } else {
                            String soqlQuery = 'Select Id, Queue__c from Social_Case_Routing__mdt' +
                                ' where Case_Priority__c =' + caseToBeAssignedToQueue.Priority +
                                ' AND Case_Origin__c =' + caseToBeAssignedToQueue.Origin +
                                ' AND Case_Language__c =' + caseToBeAssignedToQueue.Case_Language__c+ '\r\n';
                            throw new QueryException(soqlQuery);
                        }
                    } catch (Exception queueQueryEx) {
                        System.debug(LoggingLevel.ERROR, queueQueryEx);
                        
                        if (!isLanguageNotInScope(caseToBeAssignedToQueue.Case_Language__c)) {
                            caseToBeAssignedToQueue.OwnerId = 
                                Utilities.getQueueIdByName(
                                    Utilities.getAppSettingsMdtValueByKey('Case_Routing_Error_Queue'));
                            processLog += 'Failed to get QUEUE NAME from routing table: \r\n';
                            processLog += 'Case_Language__c is not in Scope: \r\n';
                            processLog += queueQueryEx.getMessage() + '\r\n';
                            
                            appWrapperLogList.add(Utilities.createApplicationLog(
                                'Warning', 'SocialCaseRouting', 'assignCaseToRelevantQueue',
                                caseId, 'Social Case Routing', processLog, null, 'Error Log', 
                                startTime, logLevelCMD, queueQueryEx));
                        } else {
                            caseToBeAssignedToQueue.OwnerId = 
                                Utilities.getQueueIdByName(
                                    Utilities.getAppSettingsMdtValueByKey('Main_Social_Care_Queue'));
                        }
                    }
                    
                    updateCaseList.add(caseToBeAssignedToQueue);
                }
                
                // update Case With Case
                if (updateCaseList != null && !updateCaseList.isEmpty()) {
                    update updateCaseList;
                }
            }
            
        } catch (Exception caseAssignFailedEx) {
            System.debug(LoggingLevel.ERROR, caseAssignFailedEx);
            
            String firstCaseId = '';
            if (caseIds != null && !caseIds.isEmpty()) {
                firstCaseId = caseIds[0];
            }
            
            processLog += 'Failed to assign queue for the following Case Ids: \r\n';
            processLog += JSON.serializePretty(caseIds) + '\r\n';
            processLog += 'Root Cause: ' + caseAssignFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog(
                'Error', 'SocialCaseRouting', 'assignCaseToRelevantQueue',
                firstCaseId, 'Social Case Routing', processLog, null, 'Error Log', 
                startTime, logLevelCMD, caseAssignFailedEx));
            
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
        }
    }
    
    public static boolean isLanguageNotInScope(String language) {
        
        if(Test.isRunningTest() && isInvalidPickTest){
            return false;
        }   
        
        Set<String> uniqueLangForSCRList = new Set < String > ();
        if (String.isEmpty(language)) {
            return true;
        } else {
            // Query custom meta data to get Unique Language.
            // Custom meta data does not support Group By   
            List<Social_Case_Routing__mdt> socialCaseUniqueLangList = [SELECT Case_Language__c 
                                                                       FROM   Social_Case_Routing__mdt 
                                                                       ORDER BY Case_Language__c
                                                                      ];
            for (Social_Case_Routing__mdt eachSocialCaseRows: socialCaseUniqueLangList) {
                uniqueLangForSCRList.add(eachSocialCaseRows.Case_Language__c);
            }
        }
        System.debug('uniqueLangForSCRList:' + uniqueLangForSCRList);
        
        if (!uniqueLangForSCRList.contains(language)) {
            return true;
        }
        
        return false;
    }
}