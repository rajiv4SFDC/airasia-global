/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/06/2017      Pawan Kumar	        Created
26/03/2019      Charles Thompson    Updates for RBV Social Care
*******************************************************************/
@isTest(SeeAllData=false)
private class SocialCaseRouting_Test{
    
    private static testMethod void testPosSocialRouting() {
        Test.startTest();
        
        // create test case  
        Map<String, Object> caseFieldValueMap = new Map<String, Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Origin', 'Twitter');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'Japanese');
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List<String> queryFieldList = new List<String>{
            'OwnerId',
            'Origin',
            'Priority',
            'Case_Language__c'
            };
                    
        List<Case> casesList = (List<Case>) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_1', casesList[0].Subject);
        System.assertEquals('Twitter', casesList[0].Origin);
        System.assertEquals('Low', casesList[0].Priority);
        System.assertEquals('Japanese', casesList[0].Case_Language__c);
        
        // call our routing logic.
        SocialCaseRouting.assignCaseToRelevantQueue(new List<String>{
            										    casesList[0].Id
                										});
        
        // query updated case by routing class.
        List<Case> updatedCasesList = (List<Case>) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_1', updatedCasesList[0].Subject);
        System.assertEquals('Twitter', updatedCasesList[0].Origin);
        System.assertEquals('Low', updatedCasesList[0].Priority);
        System.assertEquals('Japanese', updatedCasesList[0].Case_Language__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        String queueId = TestUtility.getQueueIdByName('Japanese Social Care');
        // System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        Test.stopTest();
    }
    
    private static testMethod void testNegSocialRoutingWhenLangNull() {
        Test.startTest();
        
        // create test case  
        Map<String, Object> caseFieldValueMap = new Map<String, Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_2');
        caseFieldValueMap.put('Origin', 'Twitter');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', null);
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List<String> queryFieldList = new List<String>{
            'OwnerId',
            'Origin',
            'Priority',
            'Case_Language__c'
            };
                    
        List<Case> casesList = (List<Case>) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_2', casesList[0].Subject);
        System.assertEquals('Twitter', casesList[0].Origin);
        System.assertEquals('Low', casesList[0].Priority);
        System.assertEquals(null, casesList[0].Case_Language__c);
        
        // call our routing logic.
        SocialCaseRouting.assignCaseToRelevantQueue(new List<String>{
                                                        casesList[0].Id
                                                      });
        
        // query updated case by routing class.
        List<Case> updatedCasesList = (List<Case>) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_2', updatedCasesList[0].Subject);
        System.assertEquals('Twitter', updatedCasesList[0].Origin);
        System.assertEquals('Low', updatedCasesList[0].Priority);
        System.assertEquals(null, updatedCasesList[0].Case_Language__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        String queueId = TestUtility.getQueueIdByName('Main Social Care');
        System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(0,appLogs.size());
        
        Test.stopTest();
    }    
    
    private static testMethod void testNoCaseIdsRouting() {
        Test.startTest();
        
        // call our routing logic.
        SocialCaseRouting.assignCaseToRelevantQueue(null);
        
        // query updated case by routing class.
        List<Case> updatedCasesList = (List<Case>) TestUtility.getCases();
        System.assertNotEquals(null, updatedCasesList);
        System.assertEquals(0, updatedCasesList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
        
        // just for code coverage as we can not create record with
        // the value which is not in pick list.  
        SocialCaseRouting.isLanguageNotInScope('test');
        
        Test.stopTest();
    }

    private static testMethod void testRBVCase() {
        Test.startTest();
        
        // get record type ID
        Id recTypeId = Schema.SObjectType.Case
        	                 .getRecordTypeInfosByDeveloperName()
    	                     .get('RBV_R1_Support_Case_BIG_Loyalty')
                             .getRecordTypeId();
        
        // create test case  
        Map<String, Object> caseFieldValueMap = new Map<String, Object>();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Origin', 'Twitter');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('RecordTypeId', recTypeId);
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List<String> queryFieldList = new List<String>{
            'OwnerId',
            'Origin',
            'Priority',
            'Case_Language__c'
            };
                    
        List<Case> casesList = (List<Case>) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_1', casesList[0].Subject);
        System.assertEquals('Twitter', casesList[0].Origin);
        System.assertEquals('Low', casesList[0].Priority);
        System.assertEquals('English', casesList[0].Case_Language__c);
        
        // call our routing logic.
        SocialCaseRouting.assignCaseToRelevantQueue(new List<String>{
            										    casesList[0].Id
                										});
        
        // query updated case by routing class.
        List<Case> updatedCasesList = (List<Case>) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_1', updatedCasesList[0].Subject);
        System.assertEquals('Twitter', updatedCasesList[0].Origin);
        System.assertEquals('Low', updatedCasesList[0].Priority);
        System.assertEquals('English', updatedCasesList[0].Case_Language__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        String queueId = TestUtility.getQueueIdByName('Japanese Social Care');
        // System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        Test.stopTest();
    }
    
}