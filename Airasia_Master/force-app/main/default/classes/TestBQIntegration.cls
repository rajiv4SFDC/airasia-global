/*
* Author    : Tushar Arora
* Purpose   : Test class for BQ Integration functionality
* Date      Purpose             Created/Modified By
* 9/12/19   Created             Tushar Arora
*/
@isTest(SeeAllData=false)
public class TestBQIntegration {
    
    public static String tableId = 'testtableid';
    public static String projectId = 'testprojectid';
    public static String datasetId = 'testdatasetid';
    public static Integer sendJobBatchSize = 200;
    public static Integer deleteJobBatchSize = 200;
    public GoogleBigQuery google = new GoogleBigQuery(projectId, datasetId, tableId);
	public GoogleBigQuery.InsertAll insertAll = new GoogleBigQuery.InsertAll();
    
    @testSetup static void setupTestData() {
		
        //create test lead data
        List<sObject> insertList = new List<sObject>();
        List<Lead> leadRecInsert = new List<Lead>();
        for(Integer i=0;i<20;i++){
            
            Lead leadRec = new Lead();
            leadRec.FirstName = 'Ini'+i;
            leadRec.LastName = 'Tushar'+i;
            leadRec.Company = 'Mansarovar'+i;
            leadRec.Status = 'New';
            leadRecInsert.add(leadRec);
        }
        
        insertList.addAll(leadRecInsert);
        
        String childRefundCaseRecId = [SELECT Id, 
                                       Name 
                                FROM   RecordType 
                                WHERE  sobjecttype = 'Case' 
                                AND    Name = 'Child Refund Case'
                               ].Id;
        
        String parentRefundCaseRecId = [SELECT Id, 
                                        Name 
                                 FROM   RecordType 
                                 WHERE  sobjecttype = 'Case' 
                                 AND    Name = 'Refund Case'
                                ].Id;  
        
        
        // CREATE Parent Case record
        Map<String,Object> caseFieldValueMap = new Map <String,Object>();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Origin', 'Web');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c','Other');
    	caseFieldValueMap.put('Disposition_Level_1__c', null);  // Make sure this field is NULL
        caseFieldValueMap.put('Sub_Category_2__c', null);
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);
        caseFieldValueMap.put('ADA_Chatter_Token__c', 'Test');
        //caseFieldValueMap.put('IsClosed', 'true');
        caseFieldValueMap.put('Status', 'Closed');
        // trigger should not trigger on insert as Send_to_RPS__c!='New'
        //TestUtility.createCase(caseFieldValueMap);
        
        
        
        //Case caseRecord = createCase(1)[0];
        Case caseRecord = (Case)TestUtility.getSobjectWithFieldPopulated('Case', caseFieldValueMap);
        insertList.add(caseRecord); 
        insert insertList;
        
        LiveChatTranscript liveChatTranscriptRecord = TestUtility.createLiveChatTranscript(caseRecord.Id, 1)[0];
        insert liveChatTranscriptRecord;
        
        List<LiveChatTranscriptEvent> chattranscriptrecords = TestUtility.createLiveChatTranscriptEvent(Userinfo.getUserId(), liveChatTranscriptRecord.Id, 1);
        insert chattranscriptrecords;
        
        
	}
    
    static testMethod void testSendingAndDeletingDataExecutionOfChatEvents() {
    	
        
        //create test data to be sent
        //call the send batch to send data
        //make sure the data is received
        Boolean doNotDelete = false;
        String sendJobQueryStringLead = 'SELECT Id, FirstName, LastName, Company, Status, CreatedDate FROM Lead';
    	String sendJobQueryStringChatTranscriptEvent = 'SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent';
        List<LiveChatTranscriptEvent> chatEvents = [SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent];
        Test.setMock(HttpCalloutMock.class, new BQCalloutMock(true));
            
        Test.startTest();
        	new BQObjectBatch(tableId,sendJobQueryStringChatTranscriptEvent,projectId,datasetId,doNotDelete).execute(null, chatEvents);
        Test.stopTest();
        
        List<LiveChatTranscriptEvent> chatTranscriptEventAfterDeletion = [SELECT Id FROM LiveChatTranscriptEvent];
        System.assertEquals(0, chatTranscriptEventAfterDeletion.size());
    }    

    
    static testMethod void testSendSchedulerForLeads() {
    	
        Test.setMock(HttpCalloutMock.class, new BQCalloutMock(true));
        
        Test.startTest();
        	
            BQSendSchedule bs = new BQSendSchedule();
            bs.execute(null);
        
        Test.stopTest();
        
    }

	static testMethod void testSendingAndNotDeletingData() {
    	
        
        //create test data to be sent
        //call the send batch to send data
        //make sure the data is received
        Boolean doNotDelete = true;
        String sendJobQueryStringLead = 'SELECT Id, FirstName, LastName, Company, Status, CreatedDate FROM Lead';
    	String sendJobQueryStringChatTranscriptEvent = 'SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent';
        List<LiveChatTranscriptEvent> chatEvents = [SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent];
        Test.setMock(HttpCalloutMock.class, new BQCalloutMock(true));

        Test.startTest();
        	new BQObjectBatch(tableId,sendJobQueryStringChatTranscriptEvent,projectId,datasetId,doNotDelete).execute(null, chatEvents);
        	
        Test.stopTest();
        
        List<Lead> leadsLeftAfterDeletion = [SELECT Id FROM Lead];
        List<LiveChatTranscriptEvent> chatTranscriptEventAfterDeletion = [SELECT Id FROM LiveChatTranscriptEvent];
        System.assertNotEquals(0, leadsLeftAfterDeletion.size());
        System.assertNotEquals(0, chatTranscriptEventAfterDeletion.size());
    }    
	
    
	static testMethod void testDataSendFailure() {
    	
        
        Boolean doNotDelete = false;
        String sendJobQueryStringLead = 'SELECT Id, FirstName, LastName, Company, Status, CreatedDate FROM Lead';
    	String sendJobQueryStringChatTranscriptEvent = 'SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent';
        List<LiveChatTranscriptEvent> chatEvents = [SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent];
        //Set to make data send failure
        Test.setMock(HttpCalloutMock.class, new BQCalloutMock(false));
        
        Test.startTest();
        	
        	new BQObjectBatch(tableId,sendJobQueryStringChatTranscriptEvent,projectId,datasetId,doNotDelete).execute(null, chatEvents);
        
        Test.stopTest();
        
        List<LiveChatTranscriptEvent> chatTranscriptEventAfterDeletion = [SELECT Id FROM LiveChatTranscriptEvent];
        //Deletions do not happen
        System.assertNotEquals(0, chatTranscriptEventAfterDeletion.size());
        
        List<Application_Log__c> appLogs = [SELECT Id FROM Application_Log__c 
                                            WHERE Application_Name__c = :Constants.APPLICATION_NAME_BQ_INTEGRATION
                                           AND Debug_Level__c =:Constants.LOG_DEBUG_LEVEL_ERROR];
        
        System.assertNotEquals(0, appLogs.size());
    }    
	
    
    static testMethod void testDeletionJobCompleteProcess() {
    	
        
        //create test data to be sent
        //call the send batch to send data
        //make sure the data is received
        
        //Delete data
        List<sObject> deleteList = new List<sObject>();
        List<Lead> leadList = [select id from Lead];

        String responseString = TestUtility.createGoogleResponseOfIds(leadList);


        //List<LiveChatTranscriptEvent> liveChatTranscriptEventList = [SELECT id from LiveChatTranscriptEvent];
        deleteList.addAll(leadList);
        //deleteList.addAll(liveChatTranscriptEventList);
        
        delete deleteList;
        
    
        String deleteJobQueryStringLead = 'SELECT Id, Name FROM Lead WHERE isdeleted=true AND LastModifiedBy.Id =:bqIntegrationUserId All Rows';
        String bqIntegrationUserId = UserInfo.getUserId();
        Boolean unDeleteConfirmedSentRecords = false;
        Boolean hardDeleteRecords = true;
        Boolean unDeleteUnsentRecords = false;

    	//String sendJobQueryStringChatTranscriptEvent = 'SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent';
        Test.setMock(HttpCalloutMock.class, new BQCalloutMock(true, responseString));

        Test.startTest();
        	
            Database.executeBatch(new BQObjectDelete(tableId,deleteJobQueryStringLead,
                                    projectId,datasetId,bqIntegrationUserId,
                                    unDeleteConfirmedSentRecords, hardDeleteRecords, 
                                    unDeleteUnsentRecords,1),deleteJobBatchSize); 


            
        Test.stopTest();
        
        List<Application_Log__c> applicationLogs = [SELECT Id FROM Application_Log__c 
                                                    WHERE Application_Name__c=:Constants.APPLICATION_NAME_BQ_INTEGRATION];
        System.assertNotEquals(0, applicationLogs.size());
        
    }



    static testMethod void testDeletionJobExecutionWithAllRecordsSent() {
    	
        
        //create test data to be sent
        //call the send batch to send data
        //make sure the data is received
        
        //Delete data
        List<sObject> deleteList = new List<sObject>();
        List<Lead> leadList = [select id from Lead];

        String responseString = TestUtility.createGoogleResponseOfIds(leadList);


        //List<LiveChatTranscriptEvent> liveChatTranscriptEventList = [SELECT id from LiveChatTranscriptEvent];
        deleteList.addAll(leadList);
        //deleteList.addAll(liveChatTranscriptEventList);
        
        delete deleteList;
        String bqIntegrationUserId = UserInfo.getUserId();

        List<Lead> recordsToSendToBatchExecute = [SELECT Id, 
                                                Name, SystemModstamp 
                                                FROM Lead WHERE isdeleted=true 
                                                AND LastModifiedBy.Id =:bqIntegrationUserId 
                                                All Rows];
        
        String deleteJobQueryStringLead = 'SELECT Id, Name FROM Lead WHERE isdeleted=true AND LastModifiedBy.Id =:bqIntegrationUserId All Rows';
        Boolean unDeleteConfirmedSentRecords = false;
        Boolean hardDeleteRecords = true;
        Boolean unDeleteUnsentRecords = false;

    	//String sendJobQueryStringChatTranscriptEvent = 'SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent';
        Test.setMock(HttpCalloutMock.class, new BQCalloutMock(true, responseString));

        Test.startTest();
        	
            new BQObjectDelete(tableId,deleteJobQueryStringLead,
                                    projectId,datasetId,bqIntegrationUserId,
                                    unDeleteConfirmedSentRecords, hardDeleteRecords, 
                                    unDeleteUnsentRecords,4).execute(null, recordsToSendToBatchExecute);                        
        	//Database.executeBatch(new BQObjectBatch(tableId,sendJobQueryStringChatTranscriptEvent,projectId,datasetId,doNotDelete),sendJobBatchSize); 
        
        Test.stopTest();
        
        List<Application_Log__c> applicationLogsInfo = [SELECT Id, Description__c, Stack_Trace__c, Integration_Payload__c FROM Application_Log__c
                                                    WHERE Debug_Level__c =:Constants.LOG_DEBUG_LEVEL_INFO 
                                                    AND Application_Name__c=:Constants.APPLICATION_NAME_BQ_INTEGRATION];

        List<Application_Log__c> applicationLogsError = [SELECT Id, Description__c, Stack_Trace__c, Integration_Payload__c FROM Application_Log__c
                                                    WHERE Debug_Level__c =:Constants.LOG_DEBUG_LEVEL_ERROR 
                                                    AND Application_Name__c=:Constants.APPLICATION_NAME_BQ_INTEGRATION];

        System.assertNotEquals(0, applicationLogsInfo.size(),'Info logs='+JSON.serialize(applicationLogsInfo));
        System.assertEquals(0, applicationLogsError.size(),'Error logs='+JSON.serialize(applicationLogsError));
        
    }    
    
    static testMethod void testDeletionJobExecutionWithSomeRecordsNotSent() {
    	
        
        //create test data to be sent
        //call the send batch to send data
        //make sure the data is received
        
        //Delete data
        System.debug('ta:testDeletionJobExecutionWithSomeRecordsNotSent');
        List<sObject> deleteList = new List<sObject>();
        List<Lead> leadList = [select id from Lead];
        deleteList.addAll(leadList);
        leadList.remove(0);
        leadList.remove(1);
        String responseString = TestUtility.createGoogleResponseOfIds(leadList);

        //List<LiveChatTranscriptEvent> liveChatTranscriptEventList = [SELECT id from LiveChatTranscriptEvent];
        
        //deleteList.addAll(liveChatTranscriptEventList);
        
        delete deleteList;
        String bqIntegrationUserId = UserInfo.getUserId();

        List<Lead> recordsToSendToBatchExecute = [SELECT Id, Name, SystemModstamp 
                                                FROM Lead WHERE isdeleted=true 
                                                AND LastModifiedBy.Id =:bqIntegrationUserId 
                                                All Rows];
        
        String deleteJobQueryStringLead = 'SELECT Id, Name FROM Lead WHERE isdeleted=true AND LastModifiedBy.Id =:bqIntegrationUserId All Rows';
        Boolean unDeleteConfirmedSentRecords = false;
        Boolean hardDeleteRecords = true;
        Boolean unDeleteUnsentRecords = false;

    	//String sendJobQueryStringChatTranscriptEvent = 'SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent';
        
        Test.startTest();

            Test.setMock(HttpCalloutMock.class, new BQCalloutMock(true, responseString));
	
            new BQObjectDelete(tableId,deleteJobQueryStringLead,
                                    projectId,datasetId,bqIntegrationUserId,
                                    unDeleteConfirmedSentRecords, hardDeleteRecords, 
                                    unDeleteUnsentRecords,4).execute(null, recordsToSendToBatchExecute);                        
        	//Database.executeBatch(new BQObjectBatch(tableId,sendJobQueryStringChatTranscriptEvent,projectId,datasetId,doNotDelete),sendJobBatchSize); 
        
        Test.stopTest();
        
        List<Application_Log__c> applicationLogsInfo = [SELECT Id FROM Application_Log__c
                                                    WHERE Debug_Level__c =:Constants.LOG_DEBUG_LEVEL_INFO 
                                                    AND Application_Name__c=:Constants.APPLICATION_NAME_BQ_INTEGRATION];

        List<Application_Log__c> applicationLogsError = [SELECT Id FROM Application_Log__c
                                                    WHERE Debug_Level__c =:Constants.LOG_DEBUG_LEVEL_ERROR 
                                                    AND Application_Name__c=:Constants.APPLICATION_NAME_BQ_INTEGRATION];

        System.assertNotEquals(0, applicationLogsInfo.size());
        System.assertNotEquals(0, applicationLogsError.size());
        
    }

    static testMethod void testDeletionScheduler() {
    	
        //Delete data
        List<sObject> deleteList = new List<sObject>();
        List<Lead> leadList = [select id from Lead];

        String responseString = TestUtility.createGoogleResponseOfIds(leadList);

        deleteList.addAll(leadList);
        
        delete deleteList;
        
    
        String deleteJobQueryStringLead = 'SELECT Id, Name FROM Lead WHERE isdeleted=true AND LastModifiedBy.Id =:bqIntegrationUserId All Rows';
        String bqIntegrationUserId = UserInfo.getUserId();
        Boolean unDeleteConfirmedSentRecords = false;
        Boolean hardDeleteRecords = true;
        Boolean unDeleteUnsentRecords = false;

    	//String sendJobQueryStringChatTranscriptEvent = 'SELECT Id, AgentId, Agent.Name, Detail, LastReferencedDate, LastViewedDate, LiveChatTranscriptId, Name, Time, Type, CreatedById, CreatedDate, CurrencyIsoCode, LastModifiedById, LastModifiedDate, SystemModstamp FROM LiveChatTranscriptEvent';
        Test.setMock(HttpCalloutMock.class, new BQCalloutMock(true, responseString));

        Test.startTest();
        	
            BQDeleteSchedule bd = new BQDeleteSchedule();
            bd.execute(null);
            
        Test.stopTest();
        
            
    }
    
    
    public class BQCalloutMock implements HttpCalloutMock {
         // Implement this interface method
        
        public Boolean sendSuccessResponse;
        public String responseBody='';
        
        public BQCalloutMock(Boolean sendSuccessResponse){
            this.sendSuccessResponse = sendSuccessResponse;
        }
        
        public BQCalloutMock(Boolean sendSuccessResponse, String responseBody){
            this.sendSuccessResponse = sendSuccessResponse;
            this.responseBody = responseBody;
        }
         public HTTPResponse respond(HTTPRequest request) {
            
            // Create a fake response
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(responseBody);
            if(sendSuccessResponse){
                response.setStatusCode(200);    
            }
            else{
            	
                response.setStatusCode(201);
            } 
            
            return response; 
        }
	}
    
    
    
    
    


}