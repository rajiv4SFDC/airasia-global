/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
11/07/2017      Pawan Kumar	        Created
09/12/19        Tushar Arora        Added methods for creating test data. 
*******************************************************************/
@isTest(SeeAllData = false)
public class TestUtility {
    public static void createCase(Map < String, Object > caseFieldValueMap) {
        insert getSobjectWithFieldPopulated('Case', caseFieldValueMap);
    }
    
    public static void createAccount(Map < String, Object > caseFieldValueMap) {
        insert getSobjectWithFieldPopulated('Account', caseFieldValueMap);
    }
    
   
    public static void createCase() {
        insert getSobjectWithFieldPopulated('Case', null);
    }
    
    public static void createAccount() {
        insert getSobjectWithFieldPopulated('Account', null);
    }
    
    public static void createActualSales(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('Actual_Sales__c', fieldValueMap);
    }
    
     public static Sobject getActualSales() {
        return getSObjctWithRequiredFields('Actual_Sales__c', null, true)[0];
    }
    
     public static Sobject getActualSales(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Actual_Sales__c', sobjectFieldList, true)[0];
    }
    
    
    public static void createCorporateTarget(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('Corporate_Target__c', fieldValueMap);
    }
    
     public static Sobject getCorporateTarget() {
        return getSObjctWithRequiredFields('Corporate_Target__c', null, true)[0];
    }
    
     public static Sobject getCorporateTarget(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Corporate_Target__c', sobjectFieldList, true)[0];
    }
    
    public static void createPOSTarget(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('POS_Target__c', fieldValueMap);
    }
    
     public static Sobject getPOSTarget() {
        return getSObjctWithRequiredFields('POS_Target__c', null, true)[0];
    }
    
    public static Sobject getPOSTarget(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('POS_Target__c', sobjectFieldList, true)[0];
    }
    
     public static void createAccountTarget(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('Account_Target__c', fieldValueMap);
    }
    
      public static Sobject getAccountTarget() {
        return getSObjctWithRequiredFields('Account_Target__c', null, true)[0];
    }
    
      public static Sobject getAccountTarget(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Account_Target__c', sobjectFieldList, true)[0];
    }
    
     public static void createChannelTarget(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('Channel_Target__c', fieldValueMap);
    }

     public static Sobject getChannelTarget() {
            return getSObjctWithRequiredFields('Channel_Target__c', null, true)[0];
        }
    
     public static Sobject getChannelTarget(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Channel_Target__c', sobjectFieldList, true)[0];
    }
    
    public static void createLead(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('Lead', fieldValueMap);
    }
    
    public static Sobject getLead() {
        return getSObjctWithRequiredFields('Lead', null, true)[0];
    }
    
     public static void createOpportunity(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('Opportunity', fieldValueMap);
    }
    
    public static Sobject getOpportunity(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Opportunity', sobjectFieldList, true)[0];
    }
    
     public static void createTask(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('Task', fieldValueMap);
    }
    
    public static Sobject getTask(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Task', sobjectFieldList, true)[0];
    }
    
     public static void createEvent(Map < String, Object > fieldValueMap) {
        insert getSobjectWithFieldPopulated('Event', fieldValueMap);
    }
    
    public static Sobject getEvent(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Event', sobjectFieldList, true)[0];
    }
    
    public static void createCases(Map < String, Object > caseFieldValueMap, Integer noOfRecords) {
        insert getSobjectsWithFieldPopulated('Case', caseFieldValueMap, noOfRecords);
    }
    
    public static void createApplicationLogs(Map < String, Object > caseFieldValueMap, Integer noOfRecords) {
        insert getSobjectsWithFieldPopulated('Application_Log__c', caseFieldValueMap, noOfRecords);
    }
    
    public static void createOutboundMessage(Map < String, Object > caseFieldValueMap) {
        insert getSobjectWithFieldPopulated('Outbound_Message__c', caseFieldValueMap);
    }
    
    public static void createEmailMessages(Map < String, Object > caseFieldValueMap, Integer noOfRecords) {
        insert getSobjectsWithFieldPopulated('EmailMessage', caseFieldValueMap, noOfRecords);
    }
    
    public static void createAttachments(Map < String, Object > caseFieldValueMap, Integer noOfRecords) {
        insert getSobjectsWithFieldPopulated('Attachment', caseFieldValueMap, noOfRecords);
    }
    
    public static void createAccounts(Map < String, Object > caseFieldValueMap, Integer noOfRecords) {
        insert getSobjectsWithFieldPopulated('Account', caseFieldValueMap, noOfRecords);
    }
    
    public static void createCases(Integer noOfRecords) {
        insert getSobjectsWithFieldPopulated('Case', null, noOfRecords);
    }
    
    public static void createAccounts(Integer noOfRecords) {
        insert getSobjectsWithFieldPopulated('Account', null, noOfRecords);
    }
    
    public static Sobject getOutboundMessage() {
        return getSObjctWithRequiredFields('Outbound_Message__c', null, true)[0];
    }
    
    public static Sobject getCase() {
        return getSObjctWithRequiredFields('Case', null, true)[0];
    }
    
   
    
    public static String getQueueIdByName(String queueName) {
        return [select Id from Group where Name = : queueName and Type = 'Queue'
                Limit 1
               ].Id;
    }
    
    public static Sobject getAttachment() {
        return getSObjctWithRequiredFields('Attachment', null, true)[0];
    }
    
    public static Sobject getAccount() {
        return getSObjctWithRequiredFields('Account', null, true)[0];
    }
    
    public static Sobject getCase(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Case', sobjectFieldList, true)[0];
    }
    
    public static List < Sobject > getOutboundMessages(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Outbound_Message__c', sobjectFieldList, true);
    }
    
    
    public static Sobject getAccount(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Account', sobjectFieldList, true)[0];
    }
    
    public static List < Sobject > getCases() {
        return getSObjctWithRequiredFields('Case', null, false);
    }
    
    public static List < Sobject > getOutboundMessages() {
        return getSObjctWithRequiredFields('Outbound_Message__c', null, false);
    }
    
    
    public static List < Sobject > getAttachments() {
        return getSObjctWithRequiredFields('Attachment', null, false);
    }
    
    public static List < Sobject > getGroups() {
        return getSObjctWithRequiredFields('Group', null, false);
    }
    
    public static List < Sobject > getAccounts() {
        return getSObjctWithRequiredFields('Account', null, false);
    }
    
    public static List < Sobject > getApplicationLogs() {
        return getSObjctWithRequiredFields('Application_Log__c', null, false);
    }
    
    public static List < Sobject > getUsers() {
        return getSObjctWithRequiredFields('User', null, false);
    }
    
    public static List < Sobject > getCases(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Case', sobjectFieldList, false);
    }
    
    public static List < Sobject > getApplicationLogs(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Application_Log__c', sobjectFieldList, false);
    }
    
    public static List < Sobject > getAttachments(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Attachment', sobjectFieldList, false);
    }
    
    public static List < Sobject > getEmailMessages(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('EmailMessage', sobjectFieldList, false);
    }
    
    public static List < Sobject > getGroups(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Group', sobjectFieldList, false);
    }
    public static List < Sobject > getAccounts(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('Account', sobjectFieldList, false);
    }
    
    public static List < Sobject > getUsers(List < String > sobjectFieldList) {
        return getSObjctWithRequiredFields('User', sobjectFieldList, false);
    }
    
    public static SObject getNewSobject(String objectApiName) {
        Map < String, Schema.SObjectType > gd = Schema.getGlobalDescribe();
        Schema.SObjectType st = gd.get(objectApiName);
        System.assert(st != null, 'Type provided: "' + objectApiName + '" doesnt exist in this ORG.');
        Sobject newlyCreatedObject = st.newSobject();
        return newlyCreatedObject;
    }
    
    public static List < Object_Mandatory_Field__mdt > getMandatoryFields(String sobjectApiName) {
        List < Object_Mandatory_Field__mdt > mandatoryFieldsList = [Select SObjectField_Api_Name__c, SObjectField_Data_Type__c,
                                                                    Possible_Values__c from Object_Mandatory_Field__mdt where SObject_Api_Name__c = : sobjectApiName
                                                                   ];
        
        return mandatoryFieldsList;
    }
    
    public static SObject getSobjectWithFieldPopulated(String SObject_API_Name, Map < String, Object > caseFieldValueMap) {
        Sobject newSobject = getNewSobject(SObject_API_Name);
        Map < String, Object > newSobjectFieldValueMap = new Map < String, Object > ();
        List < Object_Mandatory_Field__mdt > mandFieldsList = getMandatoryFields(SObject_API_Name);
        if (caseFieldValueMap == null && !mandFieldsList.isEmpty()) {
            
            for (Object_Mandatory_Field__mdt eachMandFields: mandFieldsList) {
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Text') ||
                    (eachMandFields.SObjectField_Data_Type__c).equals('Text Area') ||
                    (eachMandFields.SObjectField_Data_Type__c).equals('Text Area (Long)'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 'Test_' + SObject_API_Name);
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Picklist'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 'Test' + (eachMandFields.Possible_Values__c).split('#')[0]);
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Date'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, (DateTime.now()).format('yyyy-MM-dd'));
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Date/Time'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, (DateTime.now()).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\''));
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Number'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 123);
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Email'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 'xyz@gmail.com');
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('URL'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 'https://airasiadev--aacsmdev.cs58.my.salesforce.com');
            }
        }
        
        if (caseFieldValueMap != null) {
            
            for (String fieldApiName: caseFieldValueMap.keySet()) {
                newSobject.put(fieldApiName, caseFieldValueMap.get(fieldApiName));
            }
        } else {
            for (String fieldApiName: newSobjectFieldValueMap.keySet()) {
                newSobject.put(fieldApiName, newSobjectFieldValueMap.get(fieldApiName));
            }
        }
        
        return newSobject;
    }
    
    public static List < SObject > getSobjectsWithFieldPopulated(String SObject_API_Name, Map < String, Object > caseFieldValueMap, Integer noOfRecords) {
        List < SObject > newSobjectList = new List < SObject > ();
        Map < String, Object > newSobjectFieldValueMap = new Map < String, Object > ();
        List < Object_Mandatory_Field__mdt > mandFieldsList = null;
        try {
            mandFieldsList = getMandatoryFields(SObject_API_Name);
        }
        Catch(Exception e) {
            System.debug(e);
        }
        if (caseFieldValueMap == null && !mandFieldsList.isEmpty()) {
            
            for (Object_Mandatory_Field__mdt eachMandFields: mandFieldsList) {
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Text') ||
                    (eachMandFields.SObjectField_Data_Type__c).equals('Text Area') ||
                    (eachMandFields.SObjectField_Data_Type__c).equals('Text Area (Long)'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 'Test_' + SObject_API_Name);
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Picklist'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 'Test' + (eachMandFields.Possible_Values__c).split('#')[0]);
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Date'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, (DateTime.now()).format('yyyy-MM-dd'));
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Date/Time'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, (DateTime.now()).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\''));
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Number'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 123);
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('Email'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 'xyz@gmail.com');
                
                if ((eachMandFields.SObjectField_Data_Type__c).equals('URL'))
                    newSobjectFieldValueMap.put(eachMandFields.SObjectField_Api_Name__c, 'https://airasiadev--aacsmdev.cs58.my.salesforce.com');
            }
        }
        
        if (caseFieldValueMap != null) {
            for (Integer i = 0; i < noOfRecords; i++) {
                Sobject newSobject = getNewSobject(SObject_API_Name);
                for (String fieldApiName: caseFieldValueMap.keySet()) {
                    newSobject.put(fieldApiName, caseFieldValueMap.get(fieldApiName));
                } // for inner
                
                newSobjectList.add(newSobject);
            } // for outer
        } else {
            
            for (Integer i = 0; i < noOfRecords; i++) {
                Sobject newSobject = getNewSobject(SObject_API_Name);
                
                for (String fieldApiName: newSobjectFieldValueMap.keySet()) {
                    newSobject.put(fieldApiName, newSobjectFieldValueMap.get(fieldApiName));
                } // for inner
                
                newSobjectList.add(newSobject);
            } // for outer
        }
        
        return newSobjectList;
    }
    
    public static List < Sobject > getSObjctWithRequiredFields(String SObject_API_Name, List < String > sobjectFieldList, Boolean isOnlyOne) {
        List < Object_Mandatory_Field__mdt > mandFieldsList = getMandatoryFields(SObject_API_Name);
        if (sobjectFieldList == null) {
            //    if (sobjectFieldList.isEmpty()) {
            sobjectFieldList = new List < String > ();
        }
        
        Set < String > fieldsSet = new Set < String > (sobjectFieldList);
        
        for (Object_Mandatory_Field__mdt eachManFields: mandFieldsList) {
            //sobjectFieldList.add(eachManFields.SObjectField_Api_Name__c);
            fieldsSet.add(eachManFields.SObjectField_Api_Name__c);
        }
        
        String query = 'Select Id';
        //  for (String eachField: sobjectFieldList) {
        for (String eachField: fieldsSet) {
            query += ', ' + eachField;
        }
        query += ' From ' + SObject_API_Name;
        
        // only one record
        if (isOnlyOne) {
            query += ' LIMIT 1';
        }
        
        return Database.query(query);
    }

    public static String createGoogleResponseOfIds(List<sObject> sobjectList){

        String googleResponse = '{';
        for(sobject obj : sobjectList){

            googleResponse += '"v":"'+obj.Id+'",';
        }
        googleResponse = googleResponse.substring(0, googleResponse.length()-1);
        googleResponse +='}';
        System.debug('Mock google response='+googleResponse);
        return googleResponse;
    }

    public static List<LiveChatTranscript> createLiveChatTranscript(Id caseId, Integer noOfRecord){
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        List<LiveChatTranscript> returnList = new List<LiveChatTranscript>();
        
        for(Integer i=0;i<noOfRecord;i++){
            
            LiveChatTranscript lc = new LiveChatTranscript();
            lc.Body = 'test';
            lc.CaseId = caseId;
            lc.LiveChatVisitorId = lcv.Id;
			returnList.add(lc);            
            
        }
        
        return returnList;
        
    }

    public static List<LiveChatTranscriptEvent> createLiveChatTranscriptEvent(Id userId, Id liveChatTranscriptId, Integer noOfRecord){
        
        List<LiveChatTranscriptEvent> returnList = new List<LiveChatTranscriptEvent>();
        for(Integer i=0;i<noOfRecord;i++){
            LiveChatTranscriptEvent record = new LiveChatTranscriptEvent();
            record.AgentId = userId;
            record.Detail = '';
            record.LiveChatTranscriptId=liveChatTranscriptId;
            record.Time = System.now();
            record.Type='Accept';
            record.CurrencyIsoCode = 'MYR';
            returnList.add(record);
        }
        
        return returnList;
    }
    
} //TestUtility