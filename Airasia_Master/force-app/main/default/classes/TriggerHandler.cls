public abstract class TriggerHandler {
    
    @TestVisible
    private TriggerContext context;
    
    protected List<SObject> DATA_LIST_OLD, DATA_LIST_NEW;
    protected Map<Id, SObject> DATA_MAP_OLD, DATA_MAP_NEW;
    
    public TriggerHandler() {
        DATA_LIST_OLD = trigger.Old;
        DATA_LIST_NEW = trigger.New;
        DATA_MAP_OLD = trigger.OldMap;
        DATA_MAP_NEW = trigger.NewMap;
    }
    
    public void run() {
        
        // dispatch to the correct handler method
        if(Trigger.isBefore && Trigger.isInsert) {
            this.beforeInsert();
        } else if(Trigger.isBefore && Trigger.isUpdate) {
            this.beforeUpdate();
        } else if(Trigger.isBefore && Trigger.isDelete) {
            this.beforeDelete();
        } else if(Trigger.isAfter && Trigger.isInsert) {
            this.afterInsert();
        } else if(Trigger.isAfter && Trigger.isUpdate) {
            this.afterUpdate();
        } else if(Trigger.isAfter && Trigger.isDelete) {
            this.afterDelete();
        } else if(Trigger.isAfter && Trigger.isUndelete) {
            this.afterUndelete();
        }        
    }
    
    // context-specific methods for override
    @TestVisible
    protected virtual void beforeInsert(){}
    
    @TestVisible
    protected virtual void beforeUpdate(){}
    
    @TestVisible
    protected virtual void beforeDelete(){}
    
    @TestVisible
    protected virtual void afterInsert(){}
    
    @TestVisible
    protected virtual void afterUpdate(){}
    
    @TestVisible
    protected virtual void afterDelete(){}
    
    @TestVisible
    protected virtual void afterUndelete(){}
}