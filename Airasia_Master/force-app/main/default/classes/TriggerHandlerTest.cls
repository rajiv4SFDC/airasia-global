@isTest
public class TriggerHandlerTest {
	@isTest
    private static void testLiveChatTranscriptTrigger() {
        TriggerHandler handler = new ChildTriggerHandler();
        handler.beforeInsert();
        handler.beforeUpdate();
        handler.beforeDelete();
        handler.afterInsert();
        handler.afterUpdate();
        handler.afterDelete();
        handler.afterUndelete();
        System.assert(handler instanceof ChildTriggerHandler);
    }
    
    public class ChildTriggerHandler extends TriggerHandler {}
}