public class UpdateChildCaseCount {
    public static void updateCRC(DateTime startDate, DateTime endDate) {
        List<Id> caseIds = new List<Id>();
        
        List<CaseChildReference__c> crcList = new List<CaseChildReference__c>();
        
		for (Case c : [SELECT Id, 
               IsClosed
               FROM Case 
               WHERE IsClosed = true 
               AND ClosedDate >=: startDate
               AND ClosedDate <=: endDate
               AND ParentId != '']) {
                   	caseIds.add(c.Id);
               }
        
        for (CaseChildReference__c crc : [SELECT Id, Closed__c FROM CaseChildReference__c WHERE Case_Number__c = : caseIds AND Closed__c = false]) {
            crc.Closed__c = true;
            crcList.add(crc);
        }
        
        if(crcList.isEmpty() == false) {
            update crcList;
        }
        
        
    }
}