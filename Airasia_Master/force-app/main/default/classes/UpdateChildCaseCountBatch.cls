/**
 * @File Name          : UpdateChildCaseCountBatch.cls
 * @Description        : One-Time batch to update the number of child cases under parents to transition
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 7/9/2019, 3:46:42 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                Modification
 *==============================================================================
 * 1.0    6/24/2019, 12:56:24 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
public class UpdateChildCaseCountBatch implements Database.Batchable<sObject>, Database.Stateful {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id, ParentId, IsClosed FROM Case WHERE ParentId!=NULL');
    }

    public void execute(Database.BatchableContext bc, List<Case> scope) {
        List<CaseChildReference__c> crcToCreate = new List<CaseChildReference__c>();
        List<Id> scopeIds = new List<Id>();
        List<Id> usedIds = new List<Id>();

        if (!Test.isRunningTest()) { //Only if running tests
            for(Case c : scope) {
                scopeIds.add(c.Id);
            }
            List<CaseChildReference__c> crcFound = [SELECT Case_Number__c FROM CaseChildReference__c WHERE Case_Number__c IN :scopeIds];
            for(CaseChildReference__c crc : crcFound) {
                usedIds.add(crc.Case_Number__c);
            }

        }

        for(Case c : scope) {
            if(usedIds.contains(c.Id) == False) { //Avoid Duplication
                CaseChildReference__c crc = new CaseChildReference__c();
                crc.ParentCase__c = c.ParentId;
                crc.Case_Number__c = c.Id;
                crc.Closed__c = c.IsClosed;
                crcToCreate.add(crc);
            }
        }
        System.debug('CRC Size --> '+crcToCreate.size());
        
        if(!crcToCreate.isEmpty()) {
            Database.insert(crcToCreate);
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        
    }
}