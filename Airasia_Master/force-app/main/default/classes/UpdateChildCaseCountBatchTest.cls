/**
 * @File Name          : UpdateChildCaseCountBatchTest.cls
 * @Description        : 
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 7/8/2019, 11:18:12 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    7/4/2019, 2:20:20 PM   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
@isTest(SeeAllData=false)
private class UpdateChildCaseCountBatchTest {

  @isTest
    static void testCoverage() {
        String childRefundCaseRecId = '';
        String parentRefundCaseRecId = '';

        childRefundCaseRecId = [SELECT Id, 
                                       Name 
                                FROM   RecordType 
                                WHERE  sobjecttype = 'Case' 
                                AND    Name = 'Child Refund Case'
                               ].Id;
        
        parentRefundCaseRecId = [SELECT Id, 
                                        Name 
                                 FROM   RecordType 
                                 WHERE  sobjecttype = 'Case' 
                                 AND    Name = 'Refund Case'
                                ].Id;   
        
        List<Case> testParentCases = new List<Case>();
        List<Case> testOpenChildCases = new List<Case>();
        List<Case> testClosedChildCases = new List<Case>();
        


        
        //parent case data
        Map<String,Object> caseFieldValueMap = new Map<String,Object>();
        caseFieldValueMap.put('Subject', 'Parent_Case');
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Status', 'New');
        caseFieldValueMap.put('Refund_Status__c', 'New');
        caseFieldValueMap.put('RecordTypeId', parentRefundCaseRecId);
        
        TestUtility.createCases(caseFieldValueMap, 1);
        
        List<Case> parentCaseCreatedList = (List<Case>) TestUtility.getCases();
        System.assertEquals(1, parentCaseCreatedList.size());
        
        // child case data
        for (Case eachParentCase: parentCaseCreatedList) {
            caseFieldValueMap = new Map<String,Object>();
            caseFieldValueMap.put('Subject', 'Child_Case');
            caseFieldValueMap.put('Status', 'New');
            caseFieldValueMap.put('Type', 'Refund');
            caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
            caseFieldValueMap.put('ParentId', eachParentCase.Id);
            caseFieldValueMap.put('Refund_Status__c', 'New');
            caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
            caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
            TestUtility.createCases(caseFieldValueMap, 2);
            List<Case> childCases = [SELECT Id FROM Case WHERE ParentId=:eachParentCase.Id];
        	System.assertEquals(2, childCases.size());
            
            caseFieldValueMap = new Map<String,Object>();
            caseFieldValueMap.put('Subject', 'Child_Case');
            caseFieldValueMap.put('Status', 'Closed');
            caseFieldValueMap.put('Type', 'Refund');
            caseFieldValueMap.put('Sub_Category_1__c', 'Airport Tax');
            caseFieldValueMap.put('ParentId', eachParentCase.Id);
            caseFieldValueMap.put('Refund_Status__c', 'New');
            caseFieldValueMap.put('RPS_Update_Date_Time__c', (DateTime.now() - 20));
            caseFieldValueMap.put('RecordTypeId', childRefundCaseRecId);
            TestUtility.createCases(caseFieldValueMap, 3);
            childCases = [SELECT Id FROM Case WHERE ParentId=:eachParentCase.Id];
        	System.assertEquals(5, childCases.size());
        }
        
        Test.startTest();
        UpdateChildCaseCountBatch childCount = new UpdateChildCaseCountBatch();
        Database.executeBatch(childCount);
        Test.stopTest();
        
        List<Case> cases = [SELECT Number_of_Child_Cases__c, Number_of_Closed_Child_Cases__c, ParentId FROM Case WHERE ParentId=NULL LIMIT 1];
        Decimal totalCases = cases[0].Number_of_Child_Cases__c;
        Decimal totalClosed = cases[0].Number_of_Closed_Child_Cases__c;
        System.assertEquals(10, totalCases); //Should be created twice (due to process builder and batch apex)
        System.assertEquals(6, totalClosed);  //Should be created twice (due to process builder and batch apex) 
    }
}