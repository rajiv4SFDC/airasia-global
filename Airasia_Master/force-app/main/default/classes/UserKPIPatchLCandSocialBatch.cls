/**
 * @File Name          : UserKPIPatchLCandSocialBatch.cls
 * @Description        : One-Time batch to create UserKPIs from past cases
 * @Author             : Charles Thompson (charles.thompson@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Charles Thompson (charles.thompson@salesforce.com)
 * @Last Modified On   : 05 JUL 2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date           Author            Modification
 *==============================================================================
 * 1.0    05 JUL 2019    Charles Thompson       Initial Version
**/

// What does it do?
// ----------------
// Intended to be used once, after deploying the associated Process Builder flow
// ("User KPIs: Live Chat + Social and Case Counter")
// The PB will add User KPI records for new case surveys received after deployment.
// This batch is to create these records retroactively for cases closed since
// the start of this year.
// 
// This batch is for Cases, since the Live Chat and Social KPI scores are stored 
// in fields on the Case record.  There is a related batch that does the same thing
// for Web cases, because their KPI scores are stored on SurveyResponse, not Case.
// 
// Note that User_KPI__c is a child of User.  Each record is a CSAT or FCR score
// for each Case that the user is the owner of at the time the survey is received.
// 
public class UserKPIPatchLCandSocialBatch 
				implements Database.Batchable<sObject>, 
                           Database.Stateful {
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        // Get ALL closed cases created since the start of this year
        String query = 'SELECT Id, ' +
                       '       Service_Recommend__c, ' +
                       '       Live_Chat_Used_Service_Recommend_Score__c, ' +
                       '       Quality_of_Service__c, ' +
                       '       Live_Chat_Used_Quality_of_Service_Score__c, ' +
                       '       scs_twtSurveys__Feedback_Score_Value__c, ' +
                       '       Social_Care_FCR__c, ' +
                       '       User_CSAT__c, ' +
                       '       User_FCR__c, ' +
                       '       OwnerId, ' +
                       '       LastModifiedDate ' +
                       'FROM   Case ' +
                       'WHERE  IsClosed = TRUE ' +
                       'AND    CreatedDate = THIS_YEAR ' +
                       'AND    OwnerIsQueue__c = FALSE ';
        if (Test.isRunningTest()){
            query +=   'AND    Subject = \'UserKPIPatchLCandSocialBatchTest\'';
        }
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Case> scope) {
        List<User_KPI__c> kpisToInsert = new List<User_KPI__c>();
        List<Case> casesToUpdate = new List<Case>();
        Boolean cUpdated = false;
        User_KPI__c newKPI;

        for(Case c : scope) {
            // Live Chat CSAT
            if (!String.isBlank(c.Service_Recommend__c) &&
                !c.User_CSAT__c)
            {
                // Create a new LC CSAT record
                newKPI = new User_KPI__c();
                newKPI.Case__c = c.Id;
                newKPI.Channel__c = 'Live Chat';
                newKPI.OwnerId = c.OwnerId;
                newKPI.Response_DateTime__c = c.LastModifiedDate;
                newKPI.Score__c = c.Live_Chat_Used_Service_Recommend_Score__c;
                newKPI.Survey_Type__c = 'CSAT';
                newKPI.User__c = c.OwnerId;
                kpisToInsert.add(newKPI);
                c.User_CSAT__c = true;
                cUpdated = true;
            }
            
            // Live Chat FCR
            if (!String.isBlank(c.Quality_of_Service__c) &&
                !c.User_FCR__c)
            {
                // Create a new LC FCR record
                newKPI = new User_KPI__c();
                newKPI.Case__c = c.Id;
                newKPI.Channel__c = 'Live Chat';
                newKPI.OwnerId = c.OwnerId;
                newKPI.Response_DateTime__c = c.LastModifiedDate;
                newKPI.Score__c = c.Live_Chat_Used_Service_Recommend_Score__c;
                newKPI.Survey_Type__c = 'FCR';
                newKPI.User__c = c.OwnerId;
                kpisToInsert.add(newKPI);
                c.User_FCR__c = true;
                cUpdated = true;
            }

            // Social CSAT
            if (!String.isBlank(c.scs_twtSurveys__Feedback_Score_Value__c) &&
                !c.User_CSAT__c)
            {
                // Create a new Social CSAT record
                newKPI = new User_KPI__c();
                newKPI.Case__c = c.Id;
                newKPI.Channel__c = 'Social';
                newKPI.OwnerId = c.OwnerId;
                newKPI.Response_DateTime__c = c.LastModifiedDate;
                if ((c.scs_twtSurveys__Feedback_Score_Value__c == 'VS') ||
                    (c.scs_twtSurveys__Feedback_Score_Value__c == 'SS'))
                {
                    newKPI.Score__c = 1;
                } else {
                    newKPI.Score__c = 0;
                }
                newKPI.Survey_Type__c = 'CSAT';
                newKPI.User__c = c.OwnerId;
                kpisToInsert.add(newKPI);
                c.User_CSAT__c = true;
                cUpdated = true;
                
                // Social FCR
                newKPI = new User_KPI__c();
                newKPI.Case__c = c.Id;
                newKPI.Channel__c = 'Social';
                newKPI.OwnerId = c.OwnerId;
                newKPI.Response_DateTime__c = c.LastModifiedDate;
                if (c.Social_Care_FCR__c) {
                    newKPI.Score__c = 1;
                } else {
                    newKPI.Score__c = 0;
                }
                newKPI.Survey_Type__c = 'FCR';
                newKPI.User__c = c.OwnerId;
                kpisToInsert.add(newKPI);
                c.User_FCR__c = true;
            }
            if (cUpdated){
                casesToUpdate.add(c);
            }
        }
        System.debug('UserKPIPatchLCandSocialBatch: kpisToInsert: ' + kpisToInsert.size());
        Database.insert(kpisToInsert);
        System.debug('UserKPIPatchLCandSocialBatch: casesToUpdate: ' + casesToUpdate.size());
        Database.update(casesToUpdate);
    }
    
    public void finish(Database.BatchableContext bc) {
        
    }
}