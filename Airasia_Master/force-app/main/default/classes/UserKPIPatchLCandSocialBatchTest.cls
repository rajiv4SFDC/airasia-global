/**
 * @File Name          : UserKPIPatchLCandSocialBatchTest.cls
 * @Description        : Test class for UserKPIPatchLCandSocialBatch, which loads User KPIs
 * @Author             : Charles Thompson (charles.thompson@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Charles Thompson (charles.thompson@salesforce.com)
 * @Last Modified On   : 6 Jul 2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date    Author      		    Modification
 *==============================================================================
 * 1.0    6 Jul 2019   Charles Thompson     Initial Version
**/
@isTest(SeeAllData=true)
private class UserKPIPatchLCandSocialBatchTest {
    @isTest
    static void testCoverage() {
        
        // add some cases with scores
        List<Case> cases = new List<Case>();
        
        // Live Chat CSAT & FCR
        Case c = new Case();
        c.Origin = 'Web';
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                get('Auto Case Creation Enquiry/Compliment/Complain').
                                getRecordTypeId();
        c.Subject = 'UserKPIPatchLCandSocialBatchTest';
		c.Status = 'Closed';
        c.Disposition_Level_1__c = 'No Disposition Required';
        c.Type = 'Compliment';
        c.Sub_Category_1__c = 'Others';
        c.Sub_Category_2__c = 'Advertising';
        c.Quality_of_Service__c = 'Yes';
        c.Service_Recommend__c = 'Yes';
        cases.add(c);
        
        // Live Chat CSAT & FCR
        c = new Case();
        c.Origin = 'Web';
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                get('Auto Case Creation Enquiry/Compliment/Complain').
                                getRecordTypeId();
        c.Subject = 'UserKPIPatchLCandSocialBatchTest';
		c.Status = 'Closed';
        c.Disposition_Level_1__c = 'No Disposition Required';
        c.Type = 'Compliment';
        c.Sub_Category_1__c = 'Others';
        c.Sub_Category_2__c = 'Advertising';
        c.Quality_of_Service__c = 'Yes';
        c.Service_Recommend__c = 'No';
        cases.add(c);
        
        // Social CSAT & FCR
        c = new Case();
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                get('Auto Case Creation Enquiry/Compliment/Complain').
                                getRecordTypeId();
        c.Origin = 'Twitter';
        c.Subject = 'UserKPIPatchLCandSocialBatchTest';
		c.Status = 'Closed';
        c.Disposition_Level_1__c = 'No Disposition Required';
        c.Type = 'Compliment';
        c.Sub_Category_1__c = 'Others';
        c.Sub_Category_2__c = 'Advertising';
        c.scs_twtSurveys__Feedback_Score_Value__c = 'VD';
        cases.add(c);
                
        // Social CSAT & FCR
        c = new Case();
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                get('Auto Case Creation Enquiry/Compliment/Complain').
                                getRecordTypeId();
        c.Origin = 'Twitter';
        c.Subject = 'UserKPIPatchLCandSocialBatchTest';
		c.Status = 'Closed';
        c.Disposition_Level_1__c = 'No Disposition Required';
        c.Type = 'Compliment';
        c.Sub_Category_1__c = 'Others';
        c.Sub_Category_2__c = 'Advertising';
        c.scs_twtSurveys__Feedback_Score_Value__c = 'VS';
        cases.add(c);
        
        for (Case myC: cases){
            System.debug('Case: ' + myC);
        }

        insert cases;

        // do the test
        Test.startTest();
            UserKPIPatchLCandSocialBatch theBatch = new UserKPIPatchLCandSocialBatch();
	    	Database.executeBatch(theBatch);
        Test.stopTest();
        
        // check the results
        cases = [SELECT Id,
                        User_CSAT__c,
                        User_FCR__c
                 FROM   Case
                 WHERE  Subject = 'UserKPIPatchLCandSocialBatchTest'
                ];
        
        List<User_KPI__c> kpis = [SELECT Id                                  FROM   User_KPI__c 
                                  WHERE  Case__c IN :cases
                                 ];
        System.assert(cases.size() == 4); // we created 4 cases, each with 2 KPIs
        System.assert(kpis.size() == 8);
        System.assert(cases[0].User_CSAT__c);
        System.assert(cases[1].User_FCR__c);
    }
}