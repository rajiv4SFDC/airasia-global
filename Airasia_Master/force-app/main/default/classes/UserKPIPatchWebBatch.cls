/**
 * @File Name          : UserKPIPatchWebBatch.cls
 * @Description        : One-Time batch to create UserKPIs from past Survey Responses
 * @Author             : Charles Thompson (charles.thompson@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Charles Thompson (charles.thompson@salesforce.com)
 * @Last Modified On   : 07 JUL 2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date           Author            Modification
 *==============================================================================
 * 1.0    07 JUL 2019    Charles Thompson       Initial Version
**/

// What does it do?
// ----------------
// Intended to be used once, after deploying the associated Process Builder flow
// ("SurveyForce: response master process")
// The PB will add User KPI records for new case surveys received after deployment.
// This batch is to create these records retroactively for cases closed since
// the start of this year.
// 
// This batch is for Survey Responses, since the Web scores are stored 
// in SurveyQuestionResponse__c records.  There is a related batch that does the same thing
// for Live Chat and Social cases, because their KPI scores are stored on Case.
// 
// Note that User_KPI__c is a child of User.  Each record is a CSAT or FCR score
// for each Case that the user is the owner of at the time the survey is received.
// 
public class UserKPIPatchWebBatch 
				implements Database.Batchable<sObject>, 
                           Database.Stateful 
{
    // Batch Start
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        // Get ALL Survey Responses created since the start of this year
        String query = 'SELECT Id, ' +
                       '       CreatedDate, ' +
                       '       Question__c, ' +
                       '       Response_Score__c, ' +
                       '       SurveyTaker__r.Case__c ' +
                       'FROM   SurveyQuestionResponse__c ' +
                       'WHERE  CreatedDate = THIS_YEAR ' +
                       'AND    SurveyTaker__r.Case__c != null ' +
                       'AND    SurveyTaker__r.Case__r.OwnerIsQueue__c = false ' +
                       'AND    Response_Score__c != null ';
        if (Test.isRunningTest()){
            query +=   'AND    SurveyTaker__r.Case__r.Subject = \'UserKPIPatchWebBatchTest\' ';
        }
        return Database.getQueryLocator(query);
    }

    // Batch execute
    public void execute(Database.BatchableContext bc, 
                        List<SurveyQuestionResponse__c> scope) 
    {
        List<User_KPI__c> kpisToInsert = new List<User_KPI__c>();
        List<Id> relatedCaseIds = new List<Id>();
        Map<Id, Case> casesToUpdate = new Map<Id, Case>();
        User_KPI__c newKPI;
        
        // get the case Ids from the responses
        for (SurveyQuestionResponse__c r: scope){
            relatedCaseIds.add(r.SurveyTaker__r.Case__c);
        }
        // get the cases from the list of Ids
        Map<Id, Case> casesMap = new Map<Id, Case>([SELECT Id,
                                                           OwnerId,
                                                           User_CSAT__c,
                                                           User_FCR__c
                                                    FROM   Case
                                                    WHERE  Id in :relatedCaseIds
                                                   ]);

        // process the responses
        for (SurveyQuestionResponse__c r : scope) {
            // find the case from our map
            Case c = casesMap.get(r.SurveyTaker__r.Case__c);

            // Note:  each response is either a CSAT or FCR,
            // so will result in a single User KPI record

            // Web CSAT or FCR
            newKPI = new User_KPI__c();
            newKPI.Case__c = c.Id;
            newKPI.Channel__c = 'Web';
            newKPI.OwnerId = c.OwnerId;
            newKPI.Response_DateTime__c = r.CreatedDate;
            newKPI.Score__c = r.Response_Score__c;
            // CSAT or FCR?  Only the question text can tell us.
            if (r.Question__c.containsIgnoreCase('friendly')){
                newKPI.Survey_Type__c = 'CSAT';
                c.User_CSAT__c = true;
            } else {
                newKPI.Survey_Type__c = 'FCR';
                c.User_FCR__c = true;
            }
            newKPI.User__c = c.OwnerId;
            
            kpisToInsert.add(newKPI);  // save new KPI
            
            // add or update the case in the map
            if (casesToUpdate.containsKey(c.Id)){
                casesToUpdate.get(c.Id).User_CSAT__c = 
                    casesToUpdate.get(c.Id).User_CSAT__c && c.User_CSAT__c; // boolean math!
                casesToUpdate.get(c.Id).User_FCR__c = 
                    casesToUpdate.get(c.Id).User_FCR__c && c.User_FCR__c;
            } else {
                casesToUpdate.put(c.Id, c); // and save the updated case
            }
        }
        
        System.debug('UserKPIPatchWebBatch: kpisToInsert: ' + kpisToInsert.size());
        if (kpisToInsert.size() > 0){
            Database.insert(kpisToInsert);
        }
        System.debug('UserKPIPatchWebBatch: casesToUpdate: ' + casesToUpdate.size());
        if (casesToUpdate.size() > 0){
            Database.update(casesToUpdate.values());
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        
    }
}