/**
 * @File Name          : UserKPIPatchWebBatchTest.cls
 * @Description        : Test class for UserKPIPatchWebBatch, which loads User KPIs
 * @Author             : Charles Thompson (charles.thompson@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Charles Thompson (charles.thompson@salesforce.com)
 * @Last Modified On   : 7 Jul 2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date    Author      		    Modification
 *==============================================================================
 * 1.0    7 Jul 2019   Charles Thompson     Initial Version
**/
@isTest(SeeAllData=true)
private class UserKPIPatchWebBatchTest {
    @isTest
    static void testCoverage() {

        // add some cases
        List<Case> cases = new List<Case>();
        
        Case c = new Case();
        c.Origin = 'Web';
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                get('Auto Case Creation Enquiry/Compliment/Complain').
                                getRecordTypeId();
        c.Subject = 'UserKPIPatchWebBatchTest';
		c.Status = 'Closed';
        c.Disposition_Level_1__c = 'No Disposition Required';
        c.Type = 'Compliment';
        c.Sub_Category_1__c = 'Others';
        c.Sub_Category_2__c = 'Advertising';
        cases.add(c);
        
        c = new Case();
        c.Origin = 'Web';
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                get('Auto Case Creation Enquiry/Compliment/Complain').
                                getRecordTypeId();
        c.Subject = 'UserKPIPatchWebBatchTest';
		c.Status = 'Closed';
        c.Disposition_Level_1__c = 'No Disposition Required';
        c.Type = 'Compliment';
        c.Sub_Category_1__c = 'Others';
        c.Sub_Category_2__c = 'Advertising';
        cases.add(c);
        
        c = new Case();
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                get('Auto Case Creation Enquiry/Compliment/Complain').
                                getRecordTypeId();
        c.Origin = 'Web';
        c.Subject = 'UserKPIPatchWebBatchTest';
		c.Status = 'Closed';
        c.Disposition_Level_1__c = 'No Disposition Required';
        c.Type = 'Compliment';
        c.Sub_Category_1__c = 'Others';
        c.Sub_Category_2__c = 'Advertising';
        cases.add(c);
                
        c = new Case();
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().
                                get('Auto Case Creation Enquiry/Compliment/Complain').
                                getRecordTypeId();
        c.Origin = 'Web';
        c.Subject = 'UserKPIPatchWebBatchTest';
		c.Status = 'Closed';
        c.Disposition_Level_1__c = 'No Disposition Required';
        c.Type = 'Compliment';
        c.Sub_Category_1__c = 'Others';
        c.Sub_Category_2__c = 'Advertising';
        cases.add(c);

        insert cases;
        
        // create the Survey we use
        Survey__c survey = new Survey__c();
        survey.Name = 'UserKPIPatchWebBatchTest';
        insert survey;
        
        // create two questions for the survey
        Survey_Question__c q1 = new Survey_Question__c();
        q1.Choices__c = 'Yes No';
        q1.Name = 'Do you find me friendly and warm?';
        q1.OrderNumber__c = 0.0;
        q1.Question__c = 'Do you find me friendly and warm?';
        q1.Required__c = true;
        q1.Survey__c = survey.Id;
        q1.Type__c = 'Single Select--Horizontal';
        
        Survey_Question__c q2 = new Survey_Question__c();
        q2.Choices__c = 'Yes No';
        q2.Name = 'Have I completely resolved your query?';
        q2.OrderNumber__c = 1.0;
        q2.Question__c = 'Have I completely resolved your query?';
        q2.Required__c = true;
        q2.Survey__c = survey.Id;
        q2.Type__c = 'Single Select--Horizontal';

        insert q1;
        insert q2;
        
        // Create a taker record for each case
        List<SurveyTaker__c> takers = new List<SurveyTaker__c>();
        for (Case eachC: cases){
            SurveyTaker__c t = new SurveyTaker__c();
            t.Case__c = eachC.Id;
            t.Taken__c = 'false'; // they are all false!
            t.Survey__c = survey.Id;
            takers.add(t);
        }
        insert takers;

        // Now create some Survey Responses, based on the takers
        List<SurveyQuestionResponse__c> responses = new List<SurveyQuestionResponse__c>();
        for (SurveyTaker__c eachT: takers){
            SurveyQuestionResponse__c r1 = new SurveyQuestionResponse__c();
            r1.of_Time_Question_was_Responded_to__c = 1;
            r1.Response__c = 'Yes';
            r1.Survey_Question__c = q1.Id;
            r1.SurveyTaker__c = eachT.Id;
            responses.add(r1);
            
            SurveyQuestionResponse__c r2 = new SurveyQuestionResponse__c();
            r2.of_Time_Question_was_Responded_to__c = 1;
            r2.Response__c = 'No';
            r2.Survey_Question__c = q2.Id;
            r2.SurveyTaker__c = eachT.Id;
            responses.add(r2);
        }
        insert responses;

        // do the test
        Test.startTest();
            UserKPIPatchWebBatch theBatch = new UserKPIPatchWebBatch();
	    	Database.executeBatch(theBatch);
        Test.stopTest();
        
        cases = [SELECT Id,
                        User_CSAT__c,
                        User_FCR__c
                 FROM   Case
                 WHERE  Subject = 'UserKPIPatchWebBatchTest'
                ];
        
        List<User_KPI__c> kpis = [SELECT Id                                  
                                  FROM   User_KPI__c 
                                  WHERE  Case__c IN :cases
                                 ];
        System.assert(cases.size() == 4); // we created 4 cases, each with 2 KPIs
        System.assert(kpis.size() == 8);
        System.assert(cases[0].User_CSAT__c);
        System.assert(cases[1].User_FCR__c);
    }
}