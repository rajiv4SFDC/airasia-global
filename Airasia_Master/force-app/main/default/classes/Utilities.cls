/**
 * Using without sharing as need to run query on Organization table
 */
public without sharing class Utilities {

 //Get instance from INSTANCE.visual.force.com page so we can build
 public Static String getInstance() {
   String instance = '';
   Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
   String orgType = o.OrganizationType;
   String insName = o.InstanceName;
   if (orgType == 'Developer Edition' || insName.startsWithIgnoreCase('cs')) {
    List < String > parts = ApexPages.currentPage().getHeaders().get('Host').split('\\.');
    instance = parts[parts.size() - 4] + '.';
   }
   return instance;
  }
  //Needed in cases if current org is sandbox
 public static String getSubdomainPrefix() {
  Organization o = [SELECT OrganizationType, InstanceName FROM Organization limit 1];
  String orgType = o.OrganizationType;
  String insName = o.InstanceName;
  if (insName.startsWithIgnoreCase('cs')) {
   return UserInfo.getUserName().substringAfterLast('.') + '-';
  }
  return '';
 }

 public static List < String > querySobjectPkList(String object_name, String field_name) {
  List < String > pickListValues = new List < String > ();
  Schema.sObjectType sobject_type = Schema.getGlobalDescribe().get(object_name);
  Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
  Map < String, Schema.SObjectField > field_map = sobject_describe.fields.getMap();
  List < Schema.PicklistEntry > pick_list_values = field_map.get(field_name).getDescribe().getPickListValues();
  for (Schema.PicklistEntry f: pick_list_values) {
   pickListValues.add(f.getValue());
  }
  return pickListValues;
 }

 public static String getQueueIdByName(String queueName) {
  // Query Queue Id 
  // improvement 1: Better to Use 'Platform Cache' for Queue Id
  String queueId = getQueueIdToOrgCache(queueName);
  if (queueId != null) {
   return queueId;
  } else {
   queueId = [select Id from Group where Name = : queueName AND Type = 'Queue'].Id;
   addQueueIdToOrgCache(queueName, queueId);
  }
  return queueId;
 }

/*
 public static String getCSApplicationConstantsValueByKey(String Key) {
  return (Application_Constants__c.getInstance(Key)).Value__c;
 }*/

 public static String getAppSettingsMdtValueByKey(String key) {
  return [Select Value__c from Application_Setting__mdt where Key__c = : key].Value__c;
 }


 public static Map < String, String > getAppSettings() {
  Map < String, String > appSettingsMap = new Map < String, String > ();
  for (Application_Setting__mdt eachAppSettings: [Select Key__c, Value__c from Application_Setting__mdt]) {
   appSettingsMap.put(eachAppSettings.Key__c, eachAppSettings.Value__c);
  }

  return appSettingsMap;
 }

 public static String getQueueIdToOrgCache(String queueName) {
  if (Cache.Org.contains('local.CaseRoutingQueue.QueueCacheMap')) {
   system.debug('## getting values from cache');
   Map < String, String > queueCacheMap = (Map < String, String > ) Cache.Org.get('local.CaseRoutingQueue.QueueCacheMap');
   if (queueCacheMap.containsKey(queueName)) {
    return queueCacheMap.get(queueName);
   }
  }
  return null;
 }

 public static void addQueueIdToOrgCache(String queueName, String queueId) {
  Map < String, String > queueCacheMap;
  if (Cache.Org.contains('local.CaseRoutingQueue.QueueCacheMap')) {
   system.debug('## adding value to cache');
   queueCacheMap = (Map < String, String > ) Cache.Org.get('local.CaseRoutingQueue.QueueCacheMap');
   queueCacheMap.put(queueName, queueId);
   Cache.Org.put('local.CaseRoutingQueue.QueueCacheMap', queueCacheMap);
  } else {
   system.debug('## adding first value to cache');
   queueCacheMap = new Map < String, String > ();
   queueCacheMap.put(queueName, queueId);
   Cache.Org.put('local.CaseRoutingQueue.QueueCacheMap', queueCacheMap);
  }
 }

 public static String getAceSessionID() {
  PageReference pdf = Page.WS_GetAceInterfaceSessionID;
  if (isCCUser())
   pdf = new PageReference(getSiteURL() + '/apex/WS_GetAceInterfaceSessionID');
  if (!test.isRunningTest()) {
   String aceLoginWSContents = (pdf.getContent()).toString();
   System.Debug(aceLoginWSContents);
   if (aceLoginWSContents.indexof('AceLoginSessionID##@@@##') != -1) {
    string[] aceSessionIDArray = aceLoginWSContents.Split('##@@@##');
    if (aceSessionIDArray.size() == 2) {
     System.Debug(aceSessionIDArray[1]);
     return aceSessionIDArray[1];
    } else {
     throw new CalloutException(aceLoginWSContents);
    }
   } else {
    throw new CalloutException(aceLoginWSContents);
   }
  }
  return ''; // should never reach here.
 }

 public static System_Settings__mdt getLogLevel(String metadata_label_name) {
  List < System_Settings__mdt > routingLogSysStngCMD = [Select Id, Label, Date_Field_API_Name__c, Additional_Filter__c, Batch_Size__c, SObject_Api_Name__c, Debug__c,
   Info__c, Warning__c, Error__c From System_Settings__mdt Where MasterLabel = : metadata_label_name limit 1
  ];
  if (!routingLogSysStngCMD.isEmpty())
   return routingLogSysStngCMD[0];
  return null;
 }

 public static ApplicationLogWrapper createApplicationLog(String logLevel, String sourceClass, String sourceFunction, String failedRecordId, String applicationName, String processLog, String payLoad, String type, DateTime startTime, System_Settings__mdt logLevelCMD, Exception thrownExeption) {
  ApplicationLogWrapper appLogWrapper = new ApplicationLogWrapper();
  appLogWrapper.logLevel = logLevel;
  appLogWrapper.sourceClass = sourceClass;
  appLogWrapper.sourceFunction = sourceFunction;
  appLogWrapper.logMessage = processLog;
  appLogWrapper.applicationName = applicationName;
  appLogWrapper.exceptionThrown = thrownExeption;
  appLogWrapper.startDate = startTime;
  appLogWrapper.endDate = DateTime.now();
  appLogWrapper.type = 'Error Log';

  if (String.isNotEmpty(type))
   appLogWrapper.type = type;

  if (String.isNotEmpty(payLoad))
   appLogWrapper.payLoad = payLoad;

  if (thrownExeption != null)
   appLogWrapper.logLevel = 'Error';

  if (String.isNotEmpty(failedRecordId))
   appLogWrapper.referenceId = failedRecordId;

  // Set Log Level
  if (logLevelCMD != null) {
   appLogWrapper.isDebug = logLevelCMD.Debug__c;
   appLogWrapper.isInfo = logLevelCMD.Info__c;
   appLogWrapper.isError = logLevelCMD.Error__c;
   appLogWrapper.isWarning = logLevelCMD.Warning__c;
  }

  System.debug('appLogWrapper' + appLogWrapper);

  return appLogWrapper;
 }

 public static String aceFindPerson(String personEmail) {
  PageReference findPersonInterfaceVf = Page.WS_FindPersonInterface;
  if (isCCUser())
   findPersonInterfaceVf = new PageReference(getSiteURL() + '/apex/WS_FindPersonInterface');

  findPersonInterfaceVf.getParameters().put(WS_PersonConstants.PERSON_EMAIL_REQUEST, personEmail);

  if (!test.isRunningTest()) {
   String findPersonContents = (findPersonInterfaceVf.getContent()).toString();
   System.Debug(findPersonContents);
   return findPersonContents;
  }

  if (test.isRunningTest()) {
   String testData = '{"isErrorOccured" : false,"isContactExistByEmailId":true,"CustomerNumber" : "6930037936","PersonID" : 8944706,"PersonType" : "Customer","PhoneNumber" : "60987654321","Status" : "Active","EMail" : "clickcard5@yahoo.com","AddressLine1" : "333 Jalan Redq","AddressLine3" : "","AddressLine3" : "","City" : "Aiti","CountryCode" : "JP","PostalCode" : "12345","ProvinceState" : "AA"}';

   if (personEmail.indexof('test') != -1) {
    testData = '{"isErrorOccured" : false,"isContactExistByEmailId":false,"CustomerNumber" : "16930037936","PersonID" : 8944706,"PersonType" : "Customer","PhoneNumber" : "60987654321","Status" : "Active","EMail" : "test_clickcard5@yahoo.com","AddressLine1" : "333 Jalan Redq","AddressLine3" : "","AddressLine3" : "","City" : "Aiti","CountryCode" : "JP","PostalCode" : "12345","ProvinceState" : "AA"}';
   }
   return testData;
  }

  return '';
 }


 public static String aceGetPerson(String customerNumber) {
  PageReference getPersonInterfaceVf = Page.WS_GetPersonInterface;
  if (isCCUser())
   getPersonInterfaceVf = new PageReference(getSiteURL() + '/apex/WS_GetPersonInterface');

  getPersonInterfaceVf.getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER, customerNumber);

  if (!test.isRunningTest()) {
   String getPersonContents = (getPersonInterfaceVf.getContent()).toString();
   System.Debug(getPersonContents);
   return getPersonContents;
  }

  if (test.isRunningTest()) {
   String testData = '{"isErrorOccured" : false,"Title" : "MS","FirstName" : "Five test","LastName" : "Ikinw","Nationality" : "JP","DOB" : "01/04/1993"}';
   return testData;
  }

  return '';
 }

 public static List < System_Settings__mdt > getLogSettings(String sysMetadataLableName) {
  return [Select Id, is_Trigger__c, Number_Of_Retry__c, Debug__c, Info__c, Warning__c, Error__c From System_Settings__mdt
   Where MasterLabel = : sysMetadataLableName limit 1
  ];

 }

 public static String getSiteURL() {
  Network myNetwork = [SELECT Id FROM Network WHERE Name = 'AirAsia'];
  ConnectApi.Community myCommunitySite = ConnectApi.Communities.getCommunity(myNetwork.id);
  System.debug('MyDebug : ' + myCommunitySite.siteUrl);
  return myCommunitySite.siteUrl;
 }

 public static boolean isCCUser() {
  List < User > ccUserList = [Select Id, Profile.Name from user where Id = : UserInfo.getUserId() AND(name = 'AirAsia Site Guest User'
   OR Profile.Name = 'AirAsia Community Login User')];
  if (ccUserList != null && !ccUserList.isEmpty()) {
   return true;
  }

  return false;
 }
}