@isTest
global class WS_ACELoginMockImpl implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest aceLoginWsReq) {
       String responseXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body>'+
       '<LogonResponse xmlns="http://tempuri.org/"><LogonResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" '+
       'xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>398</a:NSProcessTime>'+
       '<a:ProcessTime>400</a:ProcessTime><a:SessionID>PtJXi5uXgaM=|eab+3TZ1+2au24nzBquSfs+n/spFj+SXY2ElUbL6DAFE'+
       'foy1MAvaho6G9EKaHqWeSoJnylX/otcxpl2bwALfamkZcjMs8758XpFI0re8bKRMmpbGPGoZjC5F1ldyt+EyTf7Qzi3PbIo='+
       '</a:SessionID><a:Username>APISGSFORC</a:Username><a:SessionTimeoutInterval>20</a:SessionTimeoutInterval>'+
       '</LogonResult></LogonResponse></s:Body></s:Envelope>'; 
       
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('callout:ACE_Login_WS/ACE/SessionService.svc', aceLoginWsReq.getEndpoint());
        System.assertEquals('POST', aceLoginWsReq.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml; charset=utf-8');
        res.setBody(responseXML);
        res.setStatusCode(200);
        return res;
   }
}