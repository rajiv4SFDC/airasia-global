/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
10/07/2017      Pawan Kumar         Created
*******************************************************************/
public without sharing class WS_AsyncACELoginCtrl {
    private String returnedContinuationId;
    public String responseText {
        get;
        set;
    }
    
    private static String processLog = '';
    private static String payLoad = '';
    private DateTime startTime = DateTime.now();
    public WS_AsyncACELoginCtrl() {}
    
    public static HttpRequest getACELoginRequest() {
        
        Web_Services_Credentials__mdt aceCrednetials = getAceLoginCredentials();
        String inputSoapMsg = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
            'xmlns:tem="http://tempuri.org/" xmlns:ace="http://schemas.datacontract.org/2004/07/ACE.Entities"> ' +
            '<soapenv:Header/> <soapenv:Body> <tem:Logon> <tem:logonRequest> <ace:Username>' + aceCrednetials.Username__c + '</ace:Username>' +
            '<ace:Password>' + aceCrednetials.Password__c + '</ace:Password> </tem:logonRequest> </tem:Logon> </soapenv:Body> </soapenv:Envelope>';
        
        //Set HTTPRequest Method
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Accept-Encoding', 'gzip,deflate');
        req.setHeader('content-type', 'text/xml; charset=utf-8');
        req.setHeader('Content-Length', String.valueOf(inputSoapMsg.length()));
        req.setHeader('SOAPAction', 'http://tempuri.org/ISessionService/Logon');
        req.setEndpoint(aceCrednetials.URL__c);
        System.debug('Input Request SOAP Message:'+inputSoapMsg);
        req.setBody(inputSoapMsg);
        return req;
    }
    
    private String parse(String toParse) {
        DOM.Document doc = new DOM.Document();
        try {
            doc.load(toParse);
            DOM.XMLNode root = doc.getRootElement();
            return walkThrough(root);
            
        } catch (System.XMLException e) { 
            // invalid XML
            //return e.getMessage();
            throw e;
        }
    }
    
    private String walkThrough(DOM.XMLNode node) {
        String result = '';
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            System.debug('node.getName():' + node.getName());
            if (node.getName().equals('SessionID')) {
                if (node.getText().trim() != '') {
                    result = node.getText();
                }
            }
            
            // exception XML
            if (node.getName().equals('ExceptionMessage')) {
                if (node.getText().trim() != '') {
                    throw new CalloutException(node.getText());
                }
            }
            
            for (Dom.XMLNode child: node.getChildElements()) {
                result = walkThrough(child);
                if (result != null && result.length() > 0) {
                    break;
                }
            }
            return result;
        }
        return ''; //should never reach here 
    }
    
    public Object requestAceLogin() {
        Continuation con = new Continuation(60);
        con.continuationMethod = 'processAceResponse';
        HttpRequest httpReq = getACELoginRequest();
        returnedContinuationId = con.addHttpRequest(httpReq);
        return con;
    }
    
    public Object processAceResponse() {
        HttpResponse httpRes = Continuation.getResponse(returnedContinuationId);
        getAceSessionID(httpRes);
        return null;
    }
    
    public void getAceSessionID(HttpResponse httpRes) {
        try {
            //Helpful debug messages
            System.debug(httpRes.toString());
            System.debug('STATUS:' + httpRes.getStatus());
            System.debug('STATUS_CODE:' + httpRes.getStatusCode());
            
            String responseXML = httpRes.getBody();
            payLoad += 'Output SOAP Message : \r\n';
            payLoad += responseXML + '\r\n';
            if (httpRes.getStatusCode() == 200) {
                System.debug('responseXML:' + responseXML);
                responseText = 'AceLoginSessionID##@@@##' + parse(responseXML);
            } else {
                // any other network error
                responseText = 'AceLoginExceptionMsg:' + 'STATUS: ' + httpRes.getStatus() + ' : STATUS_CODE: ' + httpRes.getStatusCode();
                processLog += responseText + '\r\n';
                //throw new CalloutException(responseText);
            }
        } catch (Exception wsAceLoginEx) {
            //  catch (System.CalloutException wsAceLoginEx) {
            System.debug('wsAceLoginEx:' + wsAceLoginEx);
            responseText = 'AceLoginExceptionMsg:' + wsAceLoginEx.getMessage();
            
            // get System Setting details for Logging
            System_Settings__mdt logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Ws_Ace_Login_Log_Level_CMD_NAME'));
            processLog += wsAceLoginEx.getMessage() + '\r\n';
            GlobalUtility.logMessage(Utilities.createApplicationLog('Error', 'WS_AsyncACELoginCtrl', 'getAceSessionID',
                                                                    null, 'ACE Login Web Service', processLog, payLoad,'Integration Log', startTime, logLevelCMD, wsAceLoginEx));
        }
    }
    
    public static Web_Services_Credentials__mdt getAceLoginCredentials() {
        Web_Services_Credentials__mdt credentialsStore = [Select Username__c, Password__c, URL__c from Web_Services_Credentials__mdt where Web_Service_Name__c = 'ACE_LOGIN_WS'];
        return credentialsStore;
    }
}