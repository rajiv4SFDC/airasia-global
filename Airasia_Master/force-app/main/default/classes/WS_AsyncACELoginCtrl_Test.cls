/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
10/07/2017      Pawan Kumar         Created
*******************************************************************/
@isTest
public class WS_AsyncACELoginCtrl_Test {
    
    public static testmethod void testAsyncACELoginWS() {
        // set mock implementation     
        Test.setMock(HttpCalloutMock.class, new WS_ACELoginMockImpl());
        Test.startTest();
        
        // Invoke the continuation by calling the action method 
        WS_AsyncACELoginCtrl aceLoginCtrl = new WS_AsyncACELoginCtrl();
        Continuation conti = (Continuation) aceLoginCtrl.requestAceLogin();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        response.setBody('Mock response body');
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(aceLoginCtrl, conti);
        
        //get ace token
        
        WS_AsyncACELoginCtrl.getACELoginRequest();
        WS_AsyncACELoginCtrl.getAceLoginCredentials();
        
        // check response
        String responseXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body>' +
            '<LogonResponse xmlns="http://tempuri.org/"><LogonResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" ' +
            'xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>398</a:NSProcessTime>' +
            '<a:ProcessTime>400</a:ProcessTime><a:SessionID>PtJXi5uXgaM=|eab+3TZ1+2au24nzBquSfs+n/spFj+SXY2ElUbL6DAFE' +
            'foy1MAvaho6G9EKaHqWeSoJnylX/otcxpl2bwALfamkZcjMs8758XpFI0re8bKRMmpbGPGoZjC5F1ldyt+EyTf7Qzi3PbIo=' +
            '</a:SessionID><a:Username>APISGSFORC</a:Username><a:SessionTimeoutInterval>20</a:SessionTimeoutInterval>' +
            '</LogonResult></LogonResponse></s:Body></s:Envelope>';
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml; charset=utf-8');
        res.setBody(responseXML);
        res.setStatusCode(200);
        aceLoginCtrl.getAceSessionID(res);
        
        String expectedSesId='AceLoginSessionID##@@@##PtJXi5uXgaM=|eab+3TZ1+2au24nzBquSfs+n/spFj+SXY2ElUbL6DAFEfoy1MAvaho6G9EKaHqWeSoJnylX/otcxpl2bwALfamkZcjMs8758XpFI0re8bKRMmpbGPGoZjC5F1ldyt+EyTf7Qzi3PbIo=';
        System.assertEquals(expectedSesId,aceLoginCtrl.responseText );
        
        // assert application log created
        List<Application_Log__c> appLogList = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogList);
        System.assertEquals(0,appLogList.size());
        
        Test.stopTest();
    }
    
    
    public static testmethod void testOtherThan200Status() {
        // set mock implementation     
        Test.setMock(HttpCalloutMock.class, new WS_ACELoginMockImpl());
        Test.startTest();
        
        // Invoke the continuation by calling the action method 
        WS_AsyncACELoginCtrl aceLoginCtrl = new WS_AsyncACELoginCtrl();
        Continuation conti = (Continuation) aceLoginCtrl.requestAceLogin();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        response.setBody('Mock response body');
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(aceLoginCtrl, conti);
        
        //get ace token
        
        WS_AsyncACELoginCtrl.getACELoginRequest();
        WS_AsyncACELoginCtrl.getAceLoginCredentials();
        
        // check response
        String responseXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">';
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml; charset=utf-8');
        res.setBody(responseXML);
        res.setStatusCode(400);
        res.setStatus('BAD_REQUEST');
        aceLoginCtrl.getAceSessionID(res);
        String expectedSesId='AceLoginExceptionMsg:STATUS: BAD_REQUEST : STATUS_CODE: 400';
        
        System.assertEquals(expectedSesId,aceLoginCtrl.responseText);
        Test.stopTest();
    }
    
    public static testmethod void testInValidXMLResponse() {
        // set mock implementation     
        Test.setMock(HttpCalloutMock.class, new WS_ACELoginMockImpl());
        Test.startTest();
        
        // Invoke the continuation by calling the action method 
        WS_AsyncACELoginCtrl aceLoginCtrl = new WS_AsyncACELoginCtrl();
        Continuation conti = (Continuation) aceLoginCtrl.requestAceLogin();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        response.setBody('Mock response body');
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(aceLoginCtrl, conti);
        
        //get ace token
        
        WS_AsyncACELoginCtrl.getACELoginRequest();
        WS_AsyncACELoginCtrl.getAceLoginCredentials();
        
        // check response
        String responseXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">';
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml; charset=utf-8');
        res.setBody(responseXML);
        res.setStatusCode(200);
        res.setStatus('OK');
        aceLoginCtrl.getAceSessionID(res);
        
        // assert application log created
        List<Application_Log__c> appLogList = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogList);
        System.assertEquals(1,appLogList.size());
        
        Test.stopTest();
    }
    
    public static testmethod void testInvalidCredentials() {
        // set mock implementation     
        Test.setMock(HttpCalloutMock.class, new WS_ACELoginMockImpl());
        Test.startTest();
        
        // Invoke the continuation by calling the action method 
        WS_AsyncACELoginCtrl aceLoginCtrl = new WS_AsyncACELoginCtrl();
        Continuation conti = (Continuation) aceLoginCtrl.requestAceLogin();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        response.setBody('Mock response body');
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(aceLoginCtrl, conti);
        
        //get ace token
        
        WS_AsyncACELoginCtrl.getACELoginRequest();
        WS_AsyncACELoginCtrl.getAceLoginCredentials();
        
        // check response
        String responseXML = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">'+
            '<s:Body><LogonResponse xmlns="http://tempuri.org/">'+
            '<LogonResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'+
            '<a:ExceptionMessage>Unauthorized Access : APISGSFOR</a:ExceptionMessage><a:NSProcessTime>257</a:NSProcessTime>'+
            '<a:ProcessTime>257</a:ProcessTime><a:SessionID i:nil="true"/>'+
            '<a:Username i:nil="true"/><a:SessionTimeoutInterval>0</a:SessionTimeoutInterval>'+
            '</LogonResult></LogonResponse></s:Body></s:Envelope>';
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml; charset=utf-8');
        res.setBody(responseXML);
        res.setStatusCode(200);// Internal Server Error
        res.setStatus('OK');
        aceLoginCtrl.getAceSessionID(res);
        
        String expectedError='AceLoginExceptionMsg:Unauthorized Access : APISGSFOR';
        System.assertEquals(expectedError,aceLoginCtrl.responseText);
        
        // assert application log created
        List<Application_Log__c> appLogList = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogList);
        System.assertEquals(1,appLogList.size());
        
        Test.stopTest();
    }
    
}