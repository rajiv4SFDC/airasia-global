public without sharing class WS_AsyncAceFindPersonCtrl {
    
    //-- Variable for Application Logging
    private static List < ApplicationLogWrapper > appWrapperLogList = null;
    public  boolean throwUnitTestExceptions {set;get;}
    private System_Settings__mdt logLevelCMD{get;set;}
    private static String processLog = '';
    private static DateTime startTime = DateTime.now();
     private static String payLoad = '';
    
    //-- Variable for Class level fields
    //AT 30 April 2019 - ACE to ACE-GCP Upgrade
    //private AsyncTempuriOrgPersn.FindPersonsResponse_elementFuture findPersonResponseFuture;
    //public schemasDatacontractOrg200407AceEntPersn.FindPersonResponseData findPersonResponseData {get;set;} 
    private AsyncACEPersonService.FindPersonsResponse_elementFuture findPersonResponseFuture;
    public ACEPersonService.FindPersonResponseData findPersonResponseData {get;set;}   
    public String jsonRespStr {get;set;}   
    public String customerNumber {get;set;}
    public String personEmailAddress {get;set;}
    public String errorMessage {get;set;}
    public boolean isErrorOccured {get;set;}    
    public static final String NO_PERSON_EMAIL_ADDRESS_FOUND = 'No Person EmailAddress Found.';    
    
    public WS_AsyncAceFindPersonCtrl() {
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('FindPersonInterface_Log_Level_CMD_NAME'));
    }
    
    /**
    * This method is called on load of the Visulaforce page to get the ACE session token and initiated the
    * FindPerson WS call to get the Person details.
    * Continuation framework is used to make an asynchronous call.
    **/
    public Object startAceFindPerson() {
        JSONGenerator jsonResObj = JSON.createGenerator(true);
        jsonResObj.writeStartObject();
         payLoad = '';
        errorMessage = '';
        isErrorOccured = false;
        processLog='';
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        try {
            
            //-- Get the Url parameteres
            personEmailAddress = ApexPages.currentPage().getParameters().get(WS_PersonConstants.PERSON_EMAIL_REQUEST);
            customerNumber = ApexPages.currentPage().getParameters().get(WS_PersonConstants.CUSTOMER_NUMBER);
            
            if (personEmailAddress != null && personEmailAddress.length() > 0) {
                
                Continuation con = new Continuation(60);
                //-- Code block for unit test case
                if (Test.isRunningtest() && throwUnitTestExceptions) {
                    con=null;
                }   
                con.continuationMethod = 'processAceFindPersonResponse';
                
                // get Ace Session ID
                String aceSessionID = Utilities.getAceSessionID();
                // above commenetd by PAWAN
                //String aceSessionID = WS_ACELoginUtility.getACESecurityToken();
                
                // Build Request for findPerson
                //AT 30 April 2019 - ACE to ACE-GCP Upgrade
                //schemasDatacontractOrg200407AceEntPersn.FindPersonRequest findPersonInput = new schemasDatacontractOrg200407AceEntPersn.FindPersonRequest();
                ACEPersonService.FindPersonRequest findPersonInput = new ACEPersonService.FindPersonRequest();
                findPersonInput.EmailAddress = personEmailAddress;
                findPersonInput.PersonType = 'Customer';
                //AT 30 April 2019 - ACE to ACE-GCP Upgrade
                //findPersonInput.ActiveOnly = 'true';
                findPersonInput.ActiveOnly = true;
                
                //-- Logging Input Payload
                payLoad += 'Input SOAP Message : \r\n';
                payLoad += findPersonInput.toString() + '\r\n';
                
                // call FindPerson
                // AT 30 April 2019 - ACE to ACE-GCP Upgrade
                // AsyncTempuriOrgPersn.AsyncbasicHttpsPersonService asyncPort = new AsyncTempuriOrgPersn.AsyncbasicHttpsPersonService();
                AsyncACEPersonService.AsyncBasicHttpsBinding_IPersonService asyncPort = new AsyncACEPersonService.AsyncBasicHttpsBinding_IPersonService();
                findPersonResponseFuture = asyncPort.beginFindPersons(con, aceSessionID, findPersonInput);
                
                return con;
            } else {
                isErrorOccured = true;
                errorMessage = NO_PERSON_EMAIL_ADDRESS_FOUND;
                
                //-- build JSON Response
                jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED, isErrorOccured);
                jsonResObj.writeStringField(WS_PersonConstants.ERROR_MESSAGE, errorMessage);
            }
            
        } catch (Exception findPersonSessionResponseFailedEx) {
            isErrorOccured = true;
            errorMessage = findPersonSessionResponseFailedEx.getMessage();
            
            //-- build JSON Response
            jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED, isErrorOccured);
            jsonResObj.writeStringField(WS_PersonConstants.ERROR_MESSAGE, errorMessage);
            
            //-- Perform Application logging
            processLog += 'Failed to get ACE Session ID for Find Person Inteface: \r\n';
            processLog += findPersonSessionResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceFindPersonCtrl', 'startAceFindPerson',
                                                                 null, 'Find Person Interface Call', processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, findPersonSessionResponseFailedEx));
            
            
        } finally {
            jsonResObj.writeEndObject();
            jsonRespStr = jsonResObj.getAsString();
            
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
        
        return null;
    
    }
    
    /**
    * This method parses the WS response data and builds required Do
    **/
    public Object processAceFindPersonResponse() {
        
        //-- variables reset and declarations
        JSONGenerator jsonResObj = JSON.createGenerator(true);
        jsonResObj.writeStartObject();
        errorMessage = '';
        isErrorOccured = false;
        processLog='';
        appWrapperLogList = new List < ApplicationLogWrapper > ();
         payLoad = '';
        
        try {
            
            //-- get the WS Response data
            findPersonResponseData = findPersonResponseFuture.getValue();
            
            //-- check whether response data has required elements for processing
            if (findPersonResponseData != null && findPersonResponseData.FindPersonList != null) {
                
                //-- build log content
                payLoad += 'Output SOAP Message : \r\n';
                payLoad += findPersonResponseData.toString() + '\r\n';
                
                //AT 30 April 2019 - ACE to ACE-GCP Upgrade
                //for (schemasDatacontractOrg200407AceEntPersn.FindPersonItem eachFindPerson: findPersonResponseData.FindPersonList.FindPersonItem) {
                for (ACEPersonService.FindPersonItem eachFindPerson: findPersonResponseData.FindPersonList.FindPersonItem) {
                    jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED, isErrorOccured);
                    jsonResObj.writeBooleanField(WS_PersonConstants.IS_CONTACT_EXIST, true);
                    jsonResObj.writeStringField(WS_PersonConstants.CUSTOMER_NUMBER, eachFindPerson.CustomerNumber);
                    jsonResObj.writeNumberField(WS_PersonConstants.PERSON_ID, eachFindPerson.PersonID);
                    jsonResObj.writeStringField(WS_PersonConstants.PERSON_TYPE, eachFindPerson.PersonType);
                    jsonResObj.writeStringField(WS_PersonConstants.PHONE_NUMBER, eachFindPerson.PhoneNumber);
                    jsonResObj.writeStringField(WS_PersonConstants.STATUS, eachFindPerson.Status);
                    jsonResObj.writeStringField(WS_PersonConstants.EMAIL, personEmailAddress);
                    
                    //-- Get the Person Address Details
                    //AT 30 April 2019 - ACE to ACE-GCP Upgrade
                    //schemasDatacontractOrg200407AceEntPersn.Address addressDetails = eachFindPerson.Address;
                    ACEPersonService.Address addressDetails = eachFindPerson.Address;
                    if (addressDetails != null) {
                        jsonResObj.writeStringField(WS_PersonConstants.ADDRESS_LINE_1, addressDetails.AddressLine1);
                        jsonResObj.writeStringField(WS_PersonConstants.ADDRESS_LINE_3, addressDetails.AddressLine2);
                        jsonResObj.writeStringField(WS_PersonConstants.ADDRESS_LINE_3, addressDetails.AddressLine3);
                        jsonResObj.writeStringField(WS_PersonConstants.CITY, addressDetails.City);
                        jsonResObj.writeStringField(WS_PersonConstants.COUNTRY_CODE, addressDetails.CountryCode);
                        jsonResObj.writeStringField(WS_PersonConstants.POSTAL_CODE, addressDetails.PostalCode);
                        jsonResObj.writeStringField(WS_PersonConstants.PROVINCE_STATE, addressDetails.ProvinceState);
                    }
                }
            }else{
                if(findPersonResponseData!=null && String.isEmpty(findPersonResponseData.ExceptionMessage)){
                    //-- build JSON Response
                    jsonResObj.writeBooleanField(WS_PersonConstants.IS_CONTACT_EXIST, false);
                    jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED, false);
                    jsonResObj.writeStringField(WS_PersonConstants.ERROR_MESSAGE, '');
                }else{
                    jsonResObj.writeBooleanField(WS_PersonConstants.IS_CONTACT_EXIST, false);
                    jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED, true);
                    jsonResObj.writeStringField(WS_PersonConstants.ERROR_MESSAGE, findPersonResponseData.ExceptionMessage);
                }
                
            }} catch (Exception findPersonResponseFailedEx) {
                isErrorOccured = true;
                errorMessage = findPersonResponseFailedEx.getStackTraceString();
                
                //-- build JSON Response
                jsonResObj.writeBooleanField(WS_PersonConstants.IS_CONTACT_EXIST, false);
                jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED, isErrorOccured);
                jsonResObj.writeStringField(WS_PersonConstants.ERROR_MESSAGE, errorMessage);
                
                //-- Perform Application logging
                processLog += 'Failed while processing response for Find Person Interface r\n';
                processLog += findPersonResponseFailedEx.getMessage() + '\r\n';
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceFindPersonCtrl', 'processAceFindPersonResponse',
                                                                     null, 'Find Person Response Processing ', processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, findPersonResponseFailedEx));
                
                
            } finally {
                jsonResObj.writeEndObject();
                jsonRespStr = jsonResObj.getAsString();
                
                if (!appWrapperLogList.isEmpty())
                    GlobalUtility.logMessage(appWrapperLogList);
            }
        return null;
    }
}