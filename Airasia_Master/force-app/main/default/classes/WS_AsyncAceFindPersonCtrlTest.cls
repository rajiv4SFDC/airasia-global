@isTest public class WS_AsyncAceFindPersonCtrlTest {
    
    /**
     * Scenario 1 : When Find Person WS results vaid response data for a mataching emailAddress from Navitaire system 
     * 
     * */
    public static testmethod void testAceFindPerson(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_FindPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.PERSON_EMAIL_REQUEST,'test@gmail.com');
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceFindPersonCtrl asyncAceFindPersonCtrlObj = new WS_AsyncAceFindPersonCtrl();
        asyncAceFindPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) asyncAceFindPersonCtrlObj.startAceFindPerson();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        //AT - 30 April 2019 - ACE to ACE-GCP Upgrade (New Payload)
        //String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>518</a:NSProcessTime><a:ProcessTime>521</a:ProcessTime><a:FindPersonList><a:FindPersonItem><a:Address><a:AddressLine1>CCC</a:AddressLine1><a:AddressLine2/><a:AddressLine3/><a:City>Sepang</a:City><a:CountryCode>MY</a:CountryCode><a:PostalCode>88888</a:PostalCode><a:ProvinceState>MY</a:ProvinceState></a:Address><a:CustomerNumber>2136815211</a:CustomerNumber><a:EMail>christytai@airasia.com</a:EMail><a:Name><a:FirstName>Test User</a:FirstName><a:LastName>Tai</a:LastName><a:MiddleName/><a:Suffix/><a:Title>MS</a:Title></a:Name><a:PersonID>3607706</a:PersonID><a:PersonType>Customer</a:PersonType><a:PhoneNumber>601266778899</a:PhoneNumber><a:Status>Active</a:Status></a:FindPersonItem></a:FindPersonList><a:OtherServiceInformations i:nil="true"/></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        String responseXML = '<s:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' +
                                '<s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult><NSProcessTime>878</NSProcessTime><ProcessTime>979</ProcessTime><FindPersonList>' +
                                '<FindPersonItem><PersonID>36123984</PersonID><PersonType>Customer</PersonType><Status>Active</Status><CustomerNumber>7380282365</CustomerNumber><Name>' +
                                '<Title>MS</Title><FirstName>Christy</FirstName><MiddleName/><LastName>Testing</LastName><Suffix/></Name><PhoneNumber>000000000</PhoneNumber>' +
                                '<EMail>hiketa@mail-list.top</EMail><Address><AddressLine1/><AddressLine2/><AddressLine3/><City/><ProvinceState/>' +
                                '<PostalCode/><CountryCode/></Address></FindPersonItem></FindPersonList></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(asyncAceFindPersonCtrlObj, conti);
        
          // result is the return value of the callback
        System.assertEquals(null, result);
        
        //-- Assert to state that No error occured while processing the response
        System.assert(!asyncAceFindPersonCtrlObj.isErrorOccured);        
        
        Test.stopTest();
    }
    
      /**
     * Scenario 2 : When Find Person WS results invalid response data which results in exception while parsing
     * 
     * */
     public static testmethod void testAceFindPerson_InvalidResponse(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_FindPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.PERSON_EMAIL_REQUEST,'test@gmail.com');
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceFindPersonCtrl asyncAceFindPersonCtrlObj = new WS_AsyncAceFindPersonCtrl();
         asyncAceFindPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) asyncAceFindPersonCtrlObj.startAceFindPerson();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        String responseXML ='<s:Envelope xmlns:s="http://schemas.x';
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(asyncAceFindPersonCtrlObj, conti);
         
         // result is the return value of the callback
         System.assertEquals(null, result);
         
         //-- Assert to state that No error occured while processing the response
         System.assert(asyncAceFindPersonCtrlObj.isErrorOccured);     
        
        Test.stopTest();
    }
    
    
    /**
    * Scenario 3 : When Find Person WS results in no person records 
    * 
    * */
    public static testmethod void testAceFindPerson_NoPersonDataFound(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_FindPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.PERSON_EMAIL_REQUEST,'test@gmail.com');
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceFindPersonCtrl asyncAceFindPersonCtrlObj = new WS_AsyncAceFindPersonCtrl();
        asyncAceFindPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) asyncAceFindPersonCtrlObj.startAceFindPerson();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
       //AT - 30 April 2019 - ACE to ACE-GCP Upgrade (New Payload)
        //String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>436</a:NSProcessTime><a:ProcessTime>439</a:ProcessTime><a:FindPersonList i:nil="true"/><a:OtherServiceInformations i:nil="true"/></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
      	String responseXML='<s:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult><NSProcessTime>453</NSProcessTime><ProcessTime>463</ProcessTime></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(asyncAceFindPersonCtrlObj, conti);
        
            // result is the return value of the callback
        System.assertEquals(null, result);
        
        //-- Assert to state that No Person records found in Navitaire
        System.assertEquals(null,asyncAceFindPersonCtrlObj.findPersonResponseData.FindPersonList);
        
        
        Test.stopTest();
    }
    
    /**
    * Scenario 4 : When Find Person WS call made to search with an empty email address criteria 
    * 
    * */
     public static testmethod void testAceFindPerson_EmptyEmail(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_FindPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.PERSON_EMAIL_REQUEST,'');
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceFindPersonCtrl asyncAceFindPersonCtrlObj = new WS_AsyncAceFindPersonCtrl();
          asyncAceFindPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) asyncAceFindPersonCtrlObj.startAceFindPerson();
        
         // result is the return value of the callback
         System.assertEquals(null, conti);
         
          //-- Assert to state that No Person records found in Navitaire
        System.assert(asyncAceFindPersonCtrlObj.isErrorOccured);
     
        Test.stopTest();
    }
    
    
     /**
    * Scenario 5 : When Find Person WS results in an runtime exception
    * 
    * */
    public static testmethod void testFindPersonRunTimeException(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_FindPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.PERSON_EMAIL_REQUEST,'test@gmail.com');
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceFindPersonCtrl asyncAceFindPersonCtrlObj = new WS_AsyncAceFindPersonCtrl();
        asyncAceFindPersonCtrlObj.throwUnitTestExceptions=true;
        Continuation conti = (Continuation) asyncAceFindPersonCtrlObj.startAceFindPerson();
        
       
            // result is the return value of the callback
        System.assertEquals(null, conti);
        
        //-- Assert to state that Error occured while processing the response
        System.assert(asyncAceFindPersonCtrlObj.isErrorOccured);
        
        
        Test.stopTest();
    }
    
     /**
    * Scenario 6 : When Find Person WS results results response with a valid exception message
    * 
    * */
    public static testmethod void testFindPersonValidExceptionResponse(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_FindPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.PERSON_EMAIL_REQUEST,'test@gmail.com');
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceFindPersonCtrl asyncAceFindPersonCtrlObj = new WS_AsyncAceFindPersonCtrl();
        asyncAceFindPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) asyncAceFindPersonCtrlObj.startAceFindPerson();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
       String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><FindPersonsResponse xmlns="http://tempuri.org/"><FindPersonsResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage>Valid Exception message</a:ExceptionMessage><a:NSProcessTime>436</a:NSProcessTime><a:ProcessTime>439</a:ProcessTime><a:FindPersonList i:nil="true"/><a:OtherServiceInformations i:nil="true"/></FindPersonsResult></FindPersonsResponse></s:Body></s:Envelope>';
      
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(asyncAceFindPersonCtrlObj, conti);
        
            // result is the return value of the callback
        System.assertEquals(null, result);
        
        //-- Assert to state that Error occured while processing the response
       // System.assert(asyncAceFindPersonCtrlObj.isErrorOccured);
        
        
        Test.stopTest();
    }
}