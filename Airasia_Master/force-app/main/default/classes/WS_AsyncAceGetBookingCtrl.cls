public without sharing class WS_AsyncAceGetBookingCtrl {
    
    //-- Variable for Application Logging
    private static List < ApplicationLogWrapper > appWrapperLogList = null;
    private System_Settings__mdt logLevelCMD{get;set;}
    public  boolean throwUnitTestExceptions {set;get;}
    private static String processLog = '';
    private static DateTime startTime = DateTime.now();
     private static String payLoad = '';
    
    //-- Variable for Data Objects
    public List<BookingSegmentDO> segmentDOList{get;set;}
    public List<WS_GetBookingPassengerDo> passengerDOList{get;set;}
    public List<WS_GetBookingPaymentDO> paymentDOList{get;set;}
    public List<WS_GetBookingCommentsDO> commentsDOList{get;set;}    
    
    //-- Variable for Get Booking Interface returned Objects
    private String returnedContinuationId;    
    /* AT 2019 Apr 22 - Upgrade ACE to GCP-ACE
    private AsyncTempuriOrg.GetBookingResponse_elementFuture  getBookingResponseFuture; 
    public schemasDatacontractOrg200407AceEnt.Booking bookingDetails{get;set;}    
    public schemasDatacontractOrg200407AceEnt.ArrayOfBookingName bookingNameArray{get;set;}
    public schemasDatacontractOrg200407AceEnt.ArrayOfBookingContact bookingContactArray{get;set;}
    public schemasDatacontractOrg200407AceEnt.ArrayOfJourney journeyArray{get;set;}
    public schemasDatacontractOrg200407AceEnt.ArrayOfSegment segmentArray{get;set;}
    public schemasDatacontractOrg200407AceEnt.ArrayOfPassenger passengerArray{get;set;}
    public List<schemasDatacontractOrg200407AceEnt.Segment> segmentList{get;set;}
    public List<schemasDatacontractOrg200407AceEnt.Passenger> passengerList{get;set;}
    */
    
    // AT 2019 Apr 22 - Upgrade ACE to GCP-ACE
    private AsyncACEBookingService.GetBookingResponse_elementFuture  getBookingResponseFuture; 
    public ACEBookingService.Booking bookingDetails{get;set;}    
    public ACEBookingService.ArrayOfBookingName bookingNameArray{get;set;}
    public ACEBookingService.ArrayOfBookingContact bookingContactArray{get;set;}
    public ACEBookingService.ArrayOfJourney journeyArray{get;set;}
    public ACEBookingService.ArrayOfSegment segmentArray{get;set;}
    public ACEBookingService.ArrayOfPassenger passengerArray{get;set;}
    public List<ACEBookingService.Segment> segmentList{get;set;}
    public List<ACEBookingService.Passenger> passengerList{get;set;}

    public Map<String,List<BookingLegDO>> bookingLegMap{get;set;}
    public String balanceDueColorCode{get;set;}
    
    
    //-- Variable for class level fields
    public String bookinghistoryVFPageURL{get;set;}
    public Long bookingID{get;set;}
    public String recordLocator{get;set;}
    public String bookingStatus{get;set;}
    public String paymentStatus{get;set;}
    public Decimal totalCost{get;set;}
    public Decimal balancedue{get;set;}
    public String balancedueSign{get;set;}
    public String balancedueString{get;set;}
    public String owningCarrierCode{get;set;}
    public String createdDate{get;set;}
    public String originalAgent{get;set;}    
    public String title{get;set;}
    public String firstName{get;set;}
    public String lastName{get;set;}
    public String customerID{get;set;}
    public String eMail{get;set;}   
    public String phone{get;set;}
    public String other{get;set;}
    public String agency{get;set;}
    public String fax{get;set;}
    Public Map<Integer,String> ordinalMap{get;set;}
    public String currencyCode{get;set;}
    
    //-- Variable for Class level Contants
    public String errorMessage{get;set;}
    public boolean isErrorOccured{get;set;}
    public static final String NO_BOOKING_NUMBER_FOUND ='No Booking Number Found.';
    public static final String NO_BOOKING_DETAILS_FOUND = 'No Booking Details Found';
    public static final String BOOKING_NUMBER = 'bookingNumber';
    
    //-- Class constructor
    public WS_AsyncAceGetBookingCtrl(){
        balanceDueColorCode= 'black';
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('GetBooking_Log_Level_CMD_NAME'));
    }    
    
    /**
    * This method is called on load of the Visulaforce page to get the ACE session token and initiated the
    * GetBooking WS call to get the booking details using Bookingnumber as input criteria.
    * Continuation framework is used to make an asynchronous call.
    **/
    public Object startAceGetBooking(){
        
        balanceDueColorCode= 'black';
        errorMessage = '';
        balancedueSign='';
        isErrorOccured =false;
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        processLog='';
         payLoad = '';
        try{
            
            //-- Get BookingNumber as URL Parameter
            String bookingNumber = ApexPages.currentPage().getParameters().get(BOOKING_NUMBER);
            if(bookingNumber!=null){
                bookingNumber = bookingNumber.trim();    
            }            
            // create continuation object
            Continuation con = new Continuation(60);
            if(bookingNumber!=null && bookingNumber!='null' && bookingNumber.length() > 0){
                
                //-- Code block for unit test case
                if (Test.isRunningtest() && throwUnitTestExceptions) {
                    con=null;
                } 
                
                con.continuationMethod='processAceGetBookingResponse';
                // get Ace Session ID
                String aceSessionID = Utilities.getAceSessionID();
                
                // Build Request for GetBooking
                //AT - 2019 April 22 - Upgrade ACE to GCP-ACE
                //schemasDatacontractOrg200407AceEnt.GetByRecordLocator recordLocatorObj = new schemasDatacontractOrg200407AceEnt.GetByRecordLocator();
                ACEBookingService.GetByRecordLocator recordLocatorObj = new ACEBookingService.GetByRecordLocator();
                recordLocatorObj.RecordLocator= bookingNumber;            
                
                //AT - 2019 April 22 - Upgrade ACE to GCP-ACE
                //schemasDatacontractOrg200407AceEnt.GetBookingRequestData  bookingInput = new schemasDatacontractOrg200407AceEnt.GetBookingRequestData();
                ACEBookingService.GetBookingRequestData  bookingInput = new ACEBookingService.GetBookingRequestData();
                bookingInput.GetBookingBy='RecordLocator';
                bookingInput.GetByRecordLocator=recordLocatorObj;
                
                  //-- Logging Input Payload
                payLoad += 'Input SOAP Message : \r\n';
                payLoad += bookingInput.toString() + '\r\n';
                
                // call GetBooking
                //AT - 2019 April 22 - Upgrade ACE to GCP-ACE
                //AsyncTempuriOrg.AsyncbasicHttpsBookingService asyncPort= new AsyncTempuriOrg.AsyncbasicHttpsBookingService();
                AsyncACEBookingService.AsyncBasicHttpsBinding_IBookingService asyncPort= new AsyncACEBookingService.AsyncBasicHttpsBinding_IBookingService();
                getBookingResponseFuture = asyncPort.beginGetBooking(con,aceSessionID,bookingInput);
                
                return con;                
            }else{                
                isErrorOccured =true;
                errorMessage = NO_BOOKING_NUMBER_FOUND;           
            }
            
        }catch(Exception getBookingResponseFailedEx){
            
            //-- Perform Application logging
            processLog += 'Failed to get ACE Session ID for GetBooking Inteface: \r\n';
            processLog += getBookingResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetBookingCtrl', 'startAceGetBooking',
                                                                 null, 'Get Booking Interface Session ID Call',processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, getBookingResponseFailedEx));
            isErrorOccured =true;
            errorMessage = getBookingResponseFailedEx.getMessage();
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
        return null;
        
    }
    
    
    /**
    * This method is parses the Response recieved from the Ace system, forms the respective DO to display the data
    * on the visualforce UI 
    **/
    public Object processAceGetBookingResponse(){
        
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        processLog='';
         payLoad = '';
        try{
           
            //-- Get the Response data
            bookingDetails = getBookingResponseFuture.getValue();
            
            //-- get the recordlocator to confirm that details found
            recordLocator= bookingDetails.RecordLocator;            
            if(recordLocator!=null && recordLocator.length()>0){
                
                //-- build log content
                payLoad += 'Output SOAP Message : \r\n';
                payLoad += bookingDetails.toString() + '\r\n';
                
                bookingID = bookingDetails.BookingID;          
                bookinghistoryVFPageURL = '/apex/GetBookingHistory?bookingID='+bookingID;                
                ordinalMap = new Map<Integer,String>();
                ordinalMap = generateOrdinalMap();
                
                bookingStatus= bookingDetails.bookingInfo.BookingStatus;
                paymentStatus= bookingDetails.bookingInfo.PaidStatus;
                owningCarrierCode= bookingDetails.bookingInfo.OwningCarrierCode;
                createdDate= bookingDetails.bookingInfo.CreatedDate.format();
                
                totalCost= bookingDetails.bookingSum.TotalCost;
                currencyCode= bookingDetails.currencyCode;
                balancedue= bookingDetails.bookingSum.Balancedue;
                balancedueSign='';
                
                //-- Code to display balancedue in different color based on value
                if(balancedue>0){
                    balanceDueColorCode='red';
                    balancedueString = currencyCode+' '+String.valueOf(formatNumber(balancedue.format()));
                }else if(balancedue<0){                    
                    balancedue = balancedue*-1;                    
                    balanceDueColorCode='blue';
                    balancedueSign='-';
                    balancedueString = '('+currencyCode+' '+String.valueOf(formatNumber(balancedue.format()))+')';
                }else{
                    balanceDueColorCode= 'black';
                    balancedueString = currencyCode+' '+String.valueOf(formatNumber(balancedue.format()));
                }
                
                //-- format the number
                // balancedueString = balancedueSign+String.valueOf(formatNumber(balancedue.format()));
                
                
                
                originalAgent= bookingDetails.POS.AgentCode;                
                bookingContactArray=bookingDetails.BookingContacts;
                bookingNameArray=bookingContactArray.BookingContact[0].Names;    
                title=bookingNameArray.BookingName[0].Title;
                firstName=bookingNameArray.BookingName[0].FirstName;
                lastName=bookingNameArray.BookingName[0].LastName;
                customerID=bookingContactArray.BookingContact[0].CustomerNumber;
                eMail=bookingContactArray.BookingContact[0].EmailAddress;
                
                phone=bookingContactArray.BookingContact[0].HomePhone;
                other=bookingContactArray.BookingContact[0].OtherPhone;
                agency=bookingContactArray.BookingContact[0].WorkPhone;
                fax=bookingContactArray.BookingContact[0].Fax;
                
                journeyArray = bookingDetails.Journeys;
                passengerArray= bookingDetails.Passengers;
                passengerList=passengerArray.Passenger;                   
                
                Map<Integer,List<String>> passengerJourneyStatusMap = new Map<Integer,List<String>>();
                segmentDOList = new List<BookingSegmentDO>();                    
                 bookingLegMap = new Map<String,List<BookingLegDO>>();
                Integer legCounter=0;
                
                //-- Get the Flight Details
                //AT - 2019 April 22 - Upgrade ACE to GCP-ACE
                //for(schemasDatacontractOrg200407AceEnt.Journey eachJourney : journeyArray.Journey){
                if(journeyArray != null) {
                    for(ACEBookingService.Journey eachJourney : journeyArray.Journey){
                        segmentArray = eachJourney.Segments;
                        segmentList=segmentArray.Segment;       
                        
                        //for(schemasDatacontractOrg200407AceEnt.Segment eachSegment : segmentList){
                        for(ACEBookingService.Segment eachSegment : segmentList){
                            Integer PaxSegmentCounter=0;
                            //for(schemasDatacontractOrg200407AceEnt.PaxSegment eachPaxSegment : eachSegment.PaxSegments.PaxSegment){                
                            for(ACEBookingService.PaxSegment eachPaxSegment : eachSegment.PaxSegments.PaxSegment){                
                                List<String> passengerStatus = null;
                                if(passengerJourneyStatusMap.containsKey(PaxSegmentCounter)){
                                    passengerStatus =  passengerJourneyStatusMap.get(PaxSegmentCounter);
                                }else{
                                    passengerStatus = new List<String>();
                                }
                                passengerStatus.add(eachPaxSegment.LiftStatus);
                                passengerJourneyStatusMap.put(PaxSegmentCounter, passengerStatus);                
                                PaxSegmentCounter = PaxSegmentCounter+1;
                            }
                                                   
                            //schemasDatacontractOrg200407AceEnt.ArrayOfLeg legArray = eachSegment.Legs;
                            ACEBookingService.ArrayOfLeg legArray = eachSegment.Legs;
                            //List<schemasDatacontractOrg200407AceEnt.Leg> legList = legArray.Leg;
                            List<ACEBookingService.Leg> legList = legArray.Leg;
                            
                            //-- Build the Flight Segment details
                            if(legList!=null && legList.size()>0){
                                legCounter++;
                              
                                List<BookingLegDO> legDOList = new List<BookingLegDO>();
                                //for(schemasDatacontractOrg200407AceEnt.Leg eachLeg : legList){
                                for(ACEBookingService.Leg eachLeg : legList){
                                    
                                    BookingLegDO newBookingLegDO = new BookingLegDO();  
                                    newBookingLegDO.arrivalDate= eachLeg.STA.formatGMT('d MMM yyyy HH:mm') ;
                                    newBookingLegDO.arrivalStation=eachLeg.ArrivalStation;
                                    newBookingLegDO.carrierCode=eachLeg.FlightDesignator.CarrierCode;
                                    newBookingLegDO.departureDate=eachLeg.STD.formatGMT('d MMM yyyy HH:mm');
                                    newBookingLegDO.departureStation=eachLeg.DepartureStation;
                                    newBookingLegDO.flightNumber=eachLeg.FlightDesignator.FlightNumber;
                                    legDOList.add(newBookingLegDO);
                                }
                                bookingLegMap.put('Segment '+legCounter, legDOList);                            
                            }
    
                            
                           /** BookingSegmentDO eachSegmentDO = new BookingSegmentDO();                       
                            eachSegmentDO.arrivalDate=eachSegment.STA.format();
                            eachSegmentDO.arrivalStation=eachSegment.ArrivalStation;
                            eachSegmentDO.carrierCode=eachSegment.FlightDesignator.CarrierCode;
                            eachSegmentDO.departureDate=eachSegment.STD.format();
                            eachSegmentDO.departureStation=eachSegment.DepartureStation;
                            eachSegmentDO.flightNumber=eachSegment.FlightDesignator.FlightNumber;
                            segmentDOList.add(eachSegmentDO);  **/      
                        }
                    }
                   }
                //-- Get the Passenger details
                passengerDOList = new List<WS_GetBookingPassengerDo>();
                Integer passengerCounter=0;    
                //for(schemasDatacontractOrg200407AceEnt.Passenger eachPassenger : passengerList){            
                for(ACEBookingService.Passenger eachPassenger : passengerList){            
                    
                    
                    WS_GetBookingPassengerDo eachPassengerDO = new WS_GetBookingPassengerDo();
                    eachPassengerDO.passengerFeeDOList = new List<WS_GetBookingPassengerFeeDO>();
                    eachPassengerDO.firstName=eachPassenger.Names.BookingName[0].FirstName;
                    eachPassengerDO.lastName=eachPassenger.Names.BookingName[0].LastName;
                    eachPassengerDO.title=eachPassenger.Names.BookingName[0].Title;
                    eachPassengerDO.passengerBoardingStatusList=passengerJourneyStatusMap.get(passengerCounter);
                    
                    //-- Get the Passenger fee details
                    if(eachPassenger.PassengerFees!=null){                
                        //for( schemasDatacontractOrg200407AceEnt.PassengerFee eachPassengerFee : eachPassenger.PassengerFees.PassengerFee){
                        for( ACEBookingService.PassengerFee eachPassengerFee : eachPassenger.PassengerFees.PassengerFee){
                            
                            //for( schemasDatacontractOrg200407AceEnt.BookingServiceCharge eachBookingServiceCharge : eachPassengerFee.ServiceCharges.BookingServiceCharge){
                            for(ACEBookingService.BookingServiceCharge eachBookingServiceCharge : eachPassengerFee.ServiceCharges.BookingServiceCharge){
                                WS_GetBookingPassengerFeeDO passengerFeeDo = new WS_GetBookingPassengerFeeDO(); 
                                passengerFeeDo.feeCode=eachBookingServiceCharge.ChargeCode;
                                passengerFeeDo.dateAdded=eachPassengerFee.CreatedDate.format();
                                passengerFeeDo.currencyVal=eachBookingServiceCharge.CurrencyCode;
                                passengerFeeDo.total=eachBookingServiceCharge.Amount;
                                eachPassengerDO.passengerFeeDOList.add(passengerFeeDo);
                            }
                        }
                    }            
                    passengerDOList.add(eachPassengerDO);
                    
                    //-- Get the Payment details
                    //schemasDatacontractOrg200407AceEnt.ArrayOfPayment paymentArray = 	bookingDetails.Payments;
                    ACEBookingService.ArrayOfPayment paymentArray = 	bookingDetails.Payments;
                    if(paymentArray!=null){               
                        List<WS_GetBookingPaymentDO> paymentDOListTemp = new List<WS_GetBookingPaymentDO>();
                        //for(schemasDatacontractOrg200407AceEnt.Payment eachPayment : paymentArray.Payment){
                        for(ACEBookingService.Payment eachPayment : paymentArray.Payment){
                            WS_GetBookingPaymentDO newPaymentDO = new WS_GetBookingPaymentDO();
                            newPaymentDO.paymentMethodCode=eachPayment.PaymentMethodCode;
                            newPaymentDO.paymentMethodType=eachPayment.PaymentMethodType;
                            newPaymentDO.collectedCurrencyCode=eachPayment.CollectedCurrencyCode;
                            newPaymentDO.paymentAmount=eachPayment.paymentAmount;
                            newPaymentDO.createdDate=eachPayment.createdDate.format();   
                            paymentDOListTemp.add(newPaymentDO);
                        }
                        
                        //-- sort the data in descending order
                        if(paymentDOListTemp!=null && paymentDOListTemp.size()>0){
                            paymentDOList = new List<WS_GetBookingPaymentDO>();
                            for(Integer k=paymentDOListTemp.size()-1;k>=0;k--){
                                paymentDOList.add(paymentDOListTemp.get(k));
                            }                               
                        }                        
                    }
                    
                    //-- Get the comments details
                    //schemasDatacontractOrg200407AceEnt.ArrayOfBookingComment commentArray = bookingDetails.BookingComments;
                    ACEBookingService.ArrayOfBookingComment commentArray = bookingDetails.BookingComments;
                    if(commentArray!=null){               
                        List<WS_GetBookingCommentsDO> commentsDOListTemp = new List<WS_GetBookingCommentsDO>();
                        //for(schemasDatacontractOrg200407AceEnt.BookingComment eachComment : commentArray.BookingComment){
                        for(ACEBookingService.BookingComment eachComment : commentArray.BookingComment){
                            WS_GetBookingCommentsDO newCommentDO = new WS_GetBookingCommentsDO();
                            newCommentDO.type=eachComment.CommentType!=null && eachComment.CommentType!='Default'?eachComment.CommentType:'Reservation';
                            newCommentDO.description=eachComment.CommentText;
                            newCommentDO.agentCode=eachComment.PointOfSale.AgentCode;
                            newCommentDO.dateVal=eachComment.CreatedDate.format();   
                            commentsDOListTemp.add(newCommentDO);
                        }
                        
                        //-- sort the data in descending order
                        if(commentsDOListTemp!=null && commentsDOListTemp.size()>0){
                            commentsDOList = new List<WS_GetBookingCommentsDO>();
                            for(Integer k=commentsDOListTemp.size()-1;k>=0;k--){
                                commentsDOList.add(commentsDOListTemp.get(k));
                            }                               
                        }
                    }            
                }        
            }else{
                
                isErrorOccured =true;
                //-- get the exception message from WS response
                errorMessage =  bookingDetails.ExceptionMessage;
                if(errorMessage==null || errorMessage=='null' || errorMessage.length()<=0){
                    errorMessage = NO_BOOKING_DETAILS_FOUND;     
                }
                //-- Perform Application logging to note that there is no response data recieved
                //appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetBookingCtrl', 'processAceGetBookingResponse',
                  //                                                   null, 'Get Booking Interface Response Call', errorMessage, null, 'Error Log',  startTime, logLevelCMD, null));
                
            }
        }catch(Exception getBookingResponseFailedEx){
            System.debug('getBookingResponseFailedEx'+getBookingResponseFailedEx);
            isErrorOccured =true;
            errorMessage = getBookingResponseFailedEx.getMessage();
            
            //-- Perform Application logging
            processLog += 'Failed to get ACE Session ID for GetBooking Inteface: \r\n';
            processLog += getBookingResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetBookingCtrl', 'processAceGetBookingResponse',
                                                                 null, 'Get Booking Interface Response Call', processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, getBookingResponseFailedEx));
            
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }        
        return null;
    }
    
    
    public Map<Integer,String> generateOrdinalMap(){        
        Map<Integer,String> ordinalMapLocal = new Map<Integer,String>();        
        for(Integer k=1;k<=100;k++){            
            String[] numberOrdinals = new String[] { 'th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th' };
                Integer numberOrdinal = k;
            Integer reminder = math.mod(numberOrdinal, 100);// numberOrdinal/100;
            if(reminder==11 || reminder==12 || reminder == 13){
                
                ordinalMapLocal.put(k, numberOrdinal + 'th');
            }else{
                ordinalMapLocal.put(k,numberOrdinal + numberOrdinals[math.mod(numberOrdinal, 10)]);
            }     
        }       
        return ordinalMapLocal;
    }
    
    public static String formatNumber(String s) {
        
        if (!s.contains('.')) {
            s = s + '.00';
        } else {
            Integer dPos = s.indexOf('.');
            if (s.length() - dPos < 3) { s = s + '0'; }   
        }
        return s; 
    }
    
    
}