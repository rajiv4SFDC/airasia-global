public without sharing class WS_AsyncAceGetBookingHistoryCtrl {
    
    
    //-- Variable for Application Logging
    private static List < ApplicationLogWrapper > appWrapperLogList = null;
    private System_Settings__mdt logLevelCMD{get;set;}
    public  boolean throwUnitTestExceptions {set;get;}
    private static String processLog = '';
    private static DateTime startTime = DateTime.now();
    private static String payLoad = '';
    
    
    //-- Variable for Get Booking history Interface returned Objects
    // AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
    //private AsyncTempuriOrg.GetBookingResponse_elementFuture  getBookingResponseFuture; 
    private AsyncACEBookingService.GetBookingResponse_elementFuture  getBookingResponseFuture; 

    //public schemasDatacontractOrg200407AceEnt.Booking bookingDetails{get;set;}
    /* AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
    private AsyncTempuriOrg.GetBookingHistoryResponse_elementFuture  getBookingHistoryResponseFuture; 
    public schemasDatacontractOrg200407AceEnt.GetBookingHistoryResponseData bookingHistoryDetails{get;set;}    
    public schemasDatacontractOrg200407AceEnt.ArrayOfBookingHistory bookingHistoryArray{get;set;}
    */
    private AsyncACEBookingService.GetBookingHistoryResponse_elementFuture  getBookingHistoryResponseFuture; 
    public ACEBookingService.GetBookingHistoryResponseData bookingHistoryDetails{get;set;}    
    public ACEBookingService.ArrayOfBookingHistory bookingHistoryArray{get;set;}
       
    public List<WS_GetBookingHistoryDO> bookingHistoryDOList {get;set;}
    public List<WS_GetBookingHistoryDO> bookingHistoryDOListSorted {get;set;}
    public Map<String,List<WS_GetBookingHistoryDO>> bookingHistoryMap {get;set;}
    public List<String> historyMapKeyListSorted{get;set;}
    public Integer totalRecords{get;set;}
    public Integer totalNoOfHistoryRecords{get;set;}
    
    //-- Variable for Class level Contants
    public String errorMessage{get;set;}
    public boolean isErrorOccured{get;set;}
    public static final String NO_BOOKING_ID_FOUND ='No Booking ID Found.';
    public static final String NO_BOOKING_HISTORY_FOUND = 'No Booking History Found.';
    public static final String BOOKING_ID = 'bookingID';
    public Application_Setting__mdt applicationSettingmdtObj;
    
    //-- Class constructor
    public WS_AsyncAceGetBookingHistoryCtrl (){
        
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('GetBooking_Log_Level_CMD_NAME'));

        //-- Get the Booking History WS PagSize
        applicationSettingmdtObj = [SELECT Key__c,Value__c FROM Application_Setting__mdt WHERE Key__c='BookingHistoryPageSize'];
    }
    
    /**
* This method is called on load of the Visulaforce page to get the ACE session token and initiated the
* GetBookingHistory WS call to get the history details using BookingID as input criteria.
* Continuation framework is used to make an asynchronous call.
**/
    public Object startAceGetBookingHistory(){
        
        //-- variables
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        bookingHistoryMap = new Map<String,List<WS_GetBookingHistoryDO>>();
        processLog='';
        totalNoOfHistoryRecords=0;
        payLoad = '';
        try{
            
            //-- Get bookingId from URL Paramter
            String bookingID = ApexPages.currentPage().getParameters().get(BOOKING_ID);   
            
            //-- check if the BookingId is authentic to proceed further
            if(bookingID!=null && bookingID!='null' && bookingID != '0' && bookingID.length()>0){
                
                Long bookingIDLong = Long.valueOf(bookingID);
                
                // get Ace Session ID
                String aceSessionID = Utilities.getAceSessionID();                
                
                // create continuation object
                Continuation con = new Continuation(60);
                //-- Code block for unit test case
                if (Test.isRunningtest() && throwUnitTestExceptions) {
                    con=null;
                }   
                con.continuationMethod='processAceGetBookingHistoryResponse';
                
                // Build Request for GetBooking
                //AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
                //schemasDatacontractOrg200407AceEnt.GetBookingHistoryRequestData bookingHistroyRequest = new schemasDatacontractOrg200407AceEnt.GetBookingHistoryRequestData();
                ACEBookingService.GetBookingHistoryRequestData bookingHistoryRequest = new ACEBookingService.GetBookingHistoryRequestData();
                bookingHistoryRequest.BookingID= bookingIDLong;
                bookingHistoryRequest.PageSize= Integer.valueOf(applicationSettingmdtObj.Value__c);
                
                //-- Logging Input Payload
                payLoad += 'Input SOAP Message : \r\n';
                payLoad += bookingHistoryRequest.toString() + '\r\n';
                
                // call GetBooking
                //AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
                //AsyncTempuriOrg.AsyncbasicHttpsBookingService asyncPort= new AsyncTempuriOrg.AsyncbasicHttpsBookingService();
                AsyncACEBookingService.AsyncBasicHttpsBinding_IBookingService asyncPort= new AsyncACEBookingService.AsyncBasicHttpsBinding_IBookingService();
                getBookingHistoryResponseFuture= asyncPort.beginGetBookingHistory(con,aceSessionID,bookingHistoryRequest);
                
                return con;
            }else{
                //-- Flad this transaction as error
                isErrorOccured = true;
                errorMessage = NO_BOOKING_ID_FOUND;
                
                //-- Perform Application logging
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetBookingHistoryCtrl', 'startAceGetBookingHistory',
                                                                     null, 'Booking History Interface Session ID Call',  processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, null));
                
                
            }   
        }catch(Exception getBookingResponseFailedEx){
            
            isErrorOccured =true;
            errorMessage = getBookingResponseFailedEx.getMessage();
            
            //-- Perform Application logging
            processLog += 'Failed to get ACE Session ID for Booking History Inteface: \r\n';
            processLog += getBookingResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetBookingHistoryCtrl', 'startAceGetBookingHistory',
                                                                 null, 'Booking History Interface Session ID Call', processLog, null, 'Error Log',  startTime, logLevelCMD, getBookingResponseFailedEx));
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }        
        return null;        
    }
    
    /**
*  This process processes the Booking History Response details 
* 
**/    
    public Object processAceGetBookingHistoryResponse(){
        
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        processLog='';
        payLoad = '';
        
        try{
            
            //-- Get the Response data
            bookingHistoryDetails = getBookingHistoryResponseFuture.getValue();
            bookingHistoryDOList = new List<WS_GetBookingHistoryDO>();  
            bookingHistoryDOListSorted = new List<WS_GetBookingHistoryDO>();  
            bookingHistoryArray = bookingHistoryDetails.BookingHistories;
            
            if(bookingHistoryArray!=null){       
                
                //-- build log content
                payLoad += 'Output SOAP Message : \r\n';
                payLoad += bookingHistoryArray.toString() + '\r\n';
                
                //-- iterate the data
                //AT 26 Apr 2019 - Upgrade ACE to GCP-ACE ACEBookingService
                //for(schemasDatacontractOrg200407AceEnt.BookingHistory eachBookingDetail : bookingHistoryArray.BookingHistory){
                for(ACEBookingService.BookingHistory eachBookingDetail : bookingHistoryArray.BookingHistory){
                    WS_GetBookingHistoryDO eachBookingHistoryObj = new WS_GetBookingHistoryDO();
                    eachBookingHistoryObj.createdDate=eachBookingDetail.CreatedDate.format();
                    eachBookingHistoryObj.agentCode=eachBookingDetail.PointofSale.AgentCode;
                    eachBookingHistoryObj.details=eachBookingDetail.HistoryDetail;
                    bookingHistoryDOList.add(eachBookingHistoryObj);
                    totalNoOfHistoryRecords++;
                    
                    List<WS_GetBookingHistoryDO> historydataList = null;
                    if(bookingHistoryMap.containsKey(eachBookingHistoryObj.createdDate)){
                        historydataList =  bookingHistoryMap.get(eachBookingHistoryObj.createdDate);
                        
                    }else{
                        historydataList = new List<WS_GetBookingHistoryDO>();
                    }
                    historydataList.add(eachBookingHistoryObj);
                    bookingHistoryMap.put(eachBookingHistoryObj.createdDate, historydataList);   
                    
                } 
                
                /**if(bookingHistoryDOList!=null && bookingHistoryDOList.size()>0){
//-- Sort data based on createdDate
for(Integer k=bookingHistoryDOList.size()-1;k>=0;k--){                  
bookingHistoryDOListSorted.add(bookingHistoryDOList.get(k));                                                        
}   
}**/
                
                //-- Build history List in descending orer of created date
                historyMapKeyListSorted =  new List<String>();
                List<String> historyMapKeyList =  new List<String>(bookingHistoryMap.keySet());
                historyMapKeyList.sort();
                for(Integer k=historyMapKeyList.size()-1;k>=0;k--){
                    historyMapKeyListSorted.add(historyMapKeyList.get(k));                        
                }
                
                totalRecords = historyMapKeyListSorted.size();
                
            }else{
                
                isErrorOccured =true;
                //-- get the exception message from WS response
                errorMessage =  bookingHistoryDetails.ExceptionMessage;
                if(errorMessage==null || errorMessage=='null' || errorMessage.length()<=0){
                    errorMessage = NO_BOOKING_HISTORY_FOUND;     
                }                    
                //-- Perform Application logging to note that there is no response data recieved
                appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetBookingHistoryCtrl', 'processAceGetBookingHistoryResponse',
                                                                     null, 'Booking History Interface Response ID Call', errorMessage, null, 'Error Log',  startTime, logLevelCMD, null));
                
                
            }
        }catch(Exception getBookingResponseFailedEx){
            
            isErrorOccured =true;
            errorMessage = getBookingResponseFailedEx.getMessage();           
            //-- Perform Application logging
            processLog += 'Failed to get ACE Session ID for Booking History Inteface: \r\n';
            processLog += getBookingResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetBookingHistoryCtrl', 'processAceGetBookingHistoryResponse',
                                                                 null, 'Booking History Interface Response ID Call', processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, getBookingResponseFailedEx));
            
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
        return null;
    }
}