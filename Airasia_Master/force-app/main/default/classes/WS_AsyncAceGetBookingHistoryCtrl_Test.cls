@isTest
public class WS_AsyncAceGetBookingHistoryCtrl_Test{
    
     /**
     * Scenario 1 : When Booking history details found for BookingId
     * 
     * */
    public static testmethod void testBookingHistoryFound() {
        
        // set mock implementation     
        // AT -26 Apr 2019 ACE to GCP-ACE
        //Test.setMock(WebServiceMock.class, new tempuriOrgBookingHistoryResponseMock());
        Test.setMock(WebServiceMock.class, new tempuriOrgBookingHistoryResponseMock());
        Test.startTest();
        
        Test.setCurrentPage(new PageReference('Page.GetBookingHistory'));
		System.currentPageReference().getParameters().put('bookingID','205481875');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetBookingHistoryCtrl aceGetBookingHistoryCtrl = new WS_AsyncAceGetBookingHistoryCtrl();        
        aceGetBookingHistoryCtrl.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) aceGetBookingHistoryCtrl.startAceGetBookingHistory();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        // AT - 26 April 2019 - ACE to ACE-GCP upgrade
        //String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><GetBookingHistoryResponse xmlns="http://tempuri.org/"><GetBookingHistoryResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>542</a:NSProcessTime><a:ProcessTime>545</a:ProcessTime><a:BookingHistories><a:BookingHistory><a:CreatedDate>2017-07-04T07:33:40.277Z</a:CreatedDate><a:HistoryCode>IS</a:HistoryCode><a:HistoryDetail>IS Itinerary Emailed to madalyalee@gmail.com on 7/4/2017 at 5:33 PM</a:HistoryDetail><a:PointOfSale><a:State>Clean</a:State><a:AgentCode>PaymentMaster</a:AgentCode><a:OrganizationCode i:nil="true"/><a:DomainCode>SYS</a:DomainCode><a:LocationCode>SYS</a:LocationCode></a:PointOfSale><a:ReceivedBy>M, Ali</a:ReceivedBy><a:ReceivedByReference/><a:SourcePointOfSale><a:State>Clean</a:State><a:AgentCode>PaymentMaster</a:AgentCode><a:OrganizationCode i:nil="true"/><a:DomainCode>SYS</a:DomainCode><a:LocationCode>SYS</a:LocationCode></a:SourcePointOfSale></a:BookingHistory><a:BookingHistory><a:CreatedDate>2017-07-13T08:08:59.44Z</a:CreatedDate><a:HistoryCode>TC</a:HistoryCode><a:HistoryDetail>TC 31Aug17 D7 206 KUL Sep17 1540 New STA: 01Sep17 1540</a:HistoryDetail><a:PointOfSale><a:State>Clean</a:State><a:AgentCode>MAA5924B</a:AgentCode><a:OrganizationCode i:nil="true"/><a:DomainCode>DEF</a:DomainCode><a:LocationCode>KUL</a:LocationCode></a:PointOfSale><a:ReceivedBy/><a:ReceivedByReference/><a:SourcePointOfSale><a:State>Clean</a:State><a:AgentCode/><a:OrganizationCode i:nil="true"/><a:DomainCode/><a:LocationCode/></a:SourcePointOfSale></a:BookingHistory><a:BookingHistory><a:CreatedDate>2017-07-13T08:08:59.91Z</a:CreatedDate><a:HistoryCode>TC</a:HistoryCode><a:HistoryDetail>TC 01Sep17 1610</a:HistoryDetail><a:PointOfSale><a:State>Clean</a:State><a:AgentCode>MAA5924B</a:AgentCode><a:OrganizationCode i:nil="true"/><a:DomainCode>DEF</a:DomainCode><a:LocationCode>KUL</a:LocationCode></a:PointOfSale><a:ReceivedBy/><a:ReceivedByReference/><a:SourcePointOfSale><a:State>Clean</a:State><a:AgentCode/><a:OrganizationCode i:nil="true"/><a:DomainCode/><a:LocationCode/></a:SourcePointOfSale></a:BookingHistory></a:BookingHistories><a:LastID>2505434091</a:LastID><a:PageSize>10</a:PageSize><a:TotalCount>0</a:TotalCount></GetBookingHistoryResult></GetBookingHistoryResponse></s:Body></s:Envelope>';
		String responseXML = '<s:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' +
                                '<s:Body><GetBookingHistoryResponse xmlns="http://tempuri.org/"><GetBookingHistoryResult><NSProcessTime>708</NSProcessTime><ProcessTime>717</ProcessTime><BookingHistories>' + 
                            	'<BookingHistory><HistoryCode>IS</HistoryCode><HistoryDetail>IS Itinerary Emailed to newskies555@yahoo.com on 23Apr19 13:15</HistoryDetail><PointOfSale><State>Clean</State>' +
                    			'<AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></PointOfSale><SourcePointOfSale><State>Clean</State><AgentCode>GSS0493</AgentCode>' + 
            					'<DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></SourcePointOfSale><ReceivedBy>Testt, Gtest</ReceivedBy><ReceivedByReference/><CreatedDate>2019-04-23T03:15:33.103Z</CreatedDate>' +
            					'</BookingHistory><BookingHistory><HistoryCode>AS</HistoryCode><HistoryDetail>AS 23Apr19 KUL-KBR 6442 Y 31F Gtest Testt</HistoryDetail><PointOfSale><State>Clean</State>' +
            					'<AgentCode>TOPWCHCKINSTG</AgentCode><DomainCode>TOP</DomainCode><LocationCode>API3D</LocationCode></PointOfSale><SourcePointOfSale><State>Clean</State>' +
            					'<AgentCode>TOPWCHCKINSTG</AgentCode><DomainCode>TOP</DomainCode><LocationCode>API3D</LocationCode></SourcePointOfSale><ReceivedBy/><ReceivedByReference/>' +
            					'<CreatedDate>2019-04-23T03:26:50.057Z</CreatedDate></BookingHistory><BookingHistory><HistoryCode>SR</HistoryCode><HistoryDetail>SR Added SSR ASA 4/23/2019 KULKBR 6442 for Gtest Testt' +
            					'</HistoryDetail><PointOfSale><State>Clean</State><AgentCode>TOPWCHCKINSTG</AgentCode><DomainCode>TOP</DomainCode><LocationCode>API3D</LocationCode></PointOfSale><SourcePointOfSale>' +
            					'<State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></SourcePointOfSale><ReceivedBy>Seat Assignment Tool</ReceivedBy>' + 
            					'<ReceivedByReference/><CreatedDate>2019-04-23T03:26:50.7Z</CreatedDate></BookingHistory><BookingHistory><HistoryCode>CI</HistoryCode><HistoryDetail>CI 23Apr19 KULKBR 2040/2145 # AK6442  ' +
            					'Seat:31F Seq:1 Testt/Gtest</HistoryDetail><PointOfSale><State>Clean</State><AgentCode>TOPWCHCKINSTG</AgentCode><DomainCode>TOP</DomainCode><LocationCode>API3D</LocationCode></PointOfSale>' +
            					'<SourcePointOfSale><State>Clean</State><AgentCode>TOPWCHCKINSTG</AgentCode><DomainCode>TOP</DomainCode><LocationCode>API3D</LocationCode></SourcePointOfSale><ReceivedBy/><ReceivedByReference/>' +
            					'<CreatedDate>2019-04-23T03:29:19.02Z</CreatedDate></BookingHistory><BookingHistory><HistoryCode>SR</HistoryCode><HistoryDetail>SR Added SSR ASM1 4/23/2019 KULKBR 6442 for Gtest Testt' +
            					'</HistoryDetail><PointOfSale><State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></PointOfSale><SourcePointOfSale>' +
            					'<State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></SourcePointOfSale><ReceivedBy>newskies555@yahoo.com</ReceivedBy>' +
            					'<ReceivedByReference/><CreatedDate>2019-04-23T03:49:22.26Z</CreatedDate></BookingHistory><BookingHistory><HistoryCode>SF</HistoryCode><HistoryDetail>SF ASM1 0 Gtest Testt  8.90 MYR' +
            					'</HistoryDetail><PointOfSale><State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></PointOfSale><SourcePointOfSale>' +
            					'<State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></SourcePointOfSale><ReceivedBy>newskies555@yahoo.com</ReceivedBy>' +
            					'<ReceivedByReference/><CreatedDate>2019-04-23T03:49:22.26Z</CreatedDate></BookingHistory><BookingHistory><HistoryCode>SF</HistoryCode>' +
            					'<HistoryDetail>SF COND 1 Gtest Testt  4.49 MYR</HistoryDetail><PointOfSale><State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode>' +
            					'<LocationCode>KUL</LocationCode></PointOfSale><SourcePointOfSale><State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode>' +
            					'</SourcePointOfSale><ReceivedBy>newskies555@yahoo.com</ReceivedBy><ReceivedByReference/><CreatedDate>2019-04-23T03:49:22.26Z</CreatedDate></BookingHistory><BookingHistory>' +
            					'<HistoryCode>XF</HistoryCode><HistoryDetail>XF COND 1 Testt/Gtest  4.49 MYR</HistoryDetail><PointOfSale><State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode>' +
            					'<LocationCode>KUL</LocationCode></PointOfSale><SourcePointOfSale><State>Clean</State><AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode>' +
            					'</SourcePointOfSale><ReceivedBy>newskies555@yahoo.com</ReceivedBy><ReceivedByReference/><CreatedDate>2019-04-23T03:50:15.84Z</CreatedDate></BookingHistory><BookingHistory>' +
            					'<HistoryCode>BD</HistoryCode><HistoryDetail>BD 23Apr19 KULKBR 2040/2145 # AK6442  Seat:31F Seq:1 Testt/Gtest</HistoryDetail><PointOfSale><State>Clean</State>' +
            					'<AgentCode>GSS0493</AgentCode><DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></PointOfSale><SourcePointOfSale><State>Clean</State><AgentCode>GSS0493</AgentCode>' +
            					'<DomainCode>DEF</DomainCode><LocationCode>KUL</LocationCode></SourcePointOfSale><ReceivedBy/><ReceivedByReference/><CreatedDate>2019-04-23T03:52:44.27Z</CreatedDate></BookingHistory>' +
            					'</BookingHistories><LastID>10</LastID><PageSize>10</PageSize><TotalCount>0</TotalCount></GetBookingHistoryResult></GetBookingHistoryResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(aceGetBookingHistoryCtrl, conti);
                
        Test.stopTest();
        
         //-- Assert that request resulted in the history details data from WS
        System.assert(aceGetBookingHistoryCtrl.historyMapKeyListSorted.size()>0);
    }
    
    
     /**
     * Scenario 2 : When Booking history not found for a BookingId
     * 
     * */
    public static testmethod void testBookingHistoryNotFound() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new WS_AsyncAceGetBookingHistoryMock());
        Test.startTest();
        
        Test.setCurrentPage(new PageReference('Page.GetBookingHistory'));
		System.currentPageReference().getParameters().put('bookingID','205481875');
        
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetBookingHistoryCtrl aceGetBookingHistoryCtrl = new WS_AsyncAceGetBookingHistoryCtrl();
        aceGetBookingHistoryCtrl.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) aceGetBookingHistoryCtrl.startAceGetBookingHistory();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><GetBookingHistoryResponse xmlns="http://tempuri.org/"><GetBookingHistoryResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>387</a:NSProcessTime><a:ProcessTime>389</a:ProcessTime><a:BookingHistories i:nil="true"/><a:LastID>0</a:LastID><a:PageSize>0</a:PageSize><a:TotalCount>0</a:TotalCount></GetBookingHistoryResult></GetBookingHistoryResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(aceGetBookingHistoryCtrl, conti);
        
         //-- Assert that request resulted in the history details data from WS
        System.assertEquals(aceGetBookingHistoryCtrl.historyMapKeyListSorted,null);
        
        Test.stopTest();
        
        
    }
    
     /**
     * Scenario 3 : When Booking history data requested with an empty BookingId
     * 
     * */
    public static testmethod void testBookingHistoryForEmptyBookingId() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new WS_AsyncAceGetBookingHistoryMock());
        Test.startTest();
        
        Test.setCurrentPage(new PageReference('Page.GetBookingHistory'));
		System.currentPageReference().getParameters().put('bookingID','');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetBookingHistoryCtrl aceGetBookingHistoryCtrl = new WS_AsyncAceGetBookingHistoryCtrl();
        aceGetBookingHistoryCtrl.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) aceGetBookingHistoryCtrl.startAceGetBookingHistory();
        
        // result is the return value of the callback
        System.assertEquals(null, conti);
        
        //-- Assert to state that error occured while processing the response
        System.assert(aceGetBookingHistoryCtrl.isErrorOccured);   
        
        //-- Assert stating no history data returend
        System.assertEquals(aceGetBookingHistoryCtrl.historyMapKeyListSorted,null);
                 
        Test.stopTest();
        
         //System.assert(aceGetBookingHistoryCtrl.historyMapKeyListSorted.size()<=0);   

    }
    
     
     /**
     * Scenario 4 : When Runtime exception occurs
     *  
     **/
     public static testmethod void testRunTimeExceptionScenario() {
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new WS_AsyncAceGetBookingHistoryMock());
        Test.startTest();
        
        Test.setCurrentPage(new PageReference('Page.GetBookingHistory'));
		System.currentPageReference().getParameters().put('bookingID','123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetBookingHistoryCtrl aceGetBookingHistoryCtrl = new WS_AsyncAceGetBookingHistoryCtrl();
        aceGetBookingHistoryCtrl.throwUnitTestExceptions=true;
        Continuation conti = (Continuation) aceGetBookingHistoryCtrl.startAceGetBookingHistory();
        
        // result is the return value of the callback
        System.assertEquals(null, conti);
        
        //-- Assert to state that error occured while processing the response
        System.assert(aceGetBookingHistoryCtrl.isErrorOccured);   
         
          //-- Assert stating no history data returend
         //System.assert(aceGetBookingHistoryCtrl.bookingHistoryDOListSorted.size()<=0);   
        
        Test.stopTest();
    }
    
     /**
     * Scenario 5 : When exception occurs while processing the response data
     *  
     **/
     public static testmethod void testExceptionCase() {
        
         // set mock implementation     
           Test.setMock(WebServiceMock.class, new WS_AsyncAceGetBookingHistoryMock());
      
        Test.startTest();
         
         
        Test.setCurrentPage(new PageReference('Page.GetBookingHistory'));
		System.currentPageReference().getParameters().put('bookingID','205481875');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetBookingHistoryCtrl aceGetBookingHistoryCtrl = new WS_AsyncAceGetBookingHistoryCtrl();        
        aceGetBookingHistoryCtrl.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) aceGetBookingHistoryCtrl.startAceGetBookingHistory();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><GetBookyResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>542</a:NSProcessTime><a:ProcessTime>545</a:ProcessTime><a:BookingHistories><a:BookingHistory><a:CreatedDate>2017-07-04T07:33:40.277Z</a:CreatedDate><a:HistoryCode>IS</a:HistoryCode><a:HistoryDetail>IS Itinerary Emailed to madalyalee@gmail.com on 7/4/2017 at 5:33 PM</a:HistoryDetail><a:PointOfSale><a:State>Clean</a:State><a:AgentCode>PaymentMaster</a:AgentCode><a:OrganizationCode i:nil="true"/><a:DomainCode>SYS</a:DomainCode><a:LocationCode>SYS</a:LocationCode></a:PointOfSale><a:ReceivedBy>M, Ali</a:ReceivedBy><a:ReceivedByReference/><a:SourcePointOfSale><a:State>Clean</a:State><a:AgentCode>PaymentMaster</a:AgentCode><a:OrganizationCode i:nil="true"/><a:DomainCode>SYS</a:DomainCode><a:LocationCode>SYS</a:LocationCode></a:SourcePointOfSale></a:BookingHistory><a:BookingHistory><a:CreatedDate>2017-07-13T08:08:59.44Z</a:CreatedDate><a:HistoryCode>TC</a:HistoryCode><a:HistoryDetail>TC 31Aug17 D7 206 KUL Sep17 1540 New STA: 01Sep17 1540</a:HistoryDetail><a:PointOfSale><a:State>Clean</a:State><a:AgentCode>MAA5924B</a:AgentCode><a:OrganizationCode i:nil="true"/><a:DomainCode>DEF</a:DomainCode><a:LocationCode>KUL</a:LocationCode></a:PointOfSale><a:ReceivedBy/><a:ReceivedByReference/><a:SourcePointOfSale><a:State>Clean</a:State><a:AgentCode/><a:OrganizationCode i:nil="true"/><a:DomainCode/><a:LocationCode/></a:SourcePointOfSale></a:BookingHistory><a:BookingHistory><a:CreatedDate>2017-07-13T08:08:59.91Z</a:CreatedDate><a:HistoryCode>TC</a:HistoryCode><a:HistoryDetail>TC 01Sep17 1610</a:HistoryDetail><a:PointOfSale><a:State>Clean</a:State><a:AgentCode>MAA5924B</a:AgentCode><a:OrganizationCode i:nil="true"/><a:DomainCode>DEF</a:DomainCode><a:LocationCode>KUL</a:LocationCode></a:PointOfSale><a:ReceivedBy/><a:ReceivedByReference/><a:SourcePointOfSale><a:State>Clean</a:State><a:AgentCode/><a:OrganizationCode i:nil="true"/><a:DomainCode/><a:LocationCode/></a:SourcePointOfSale></a:BookingHistory></a:BookingHistories><a:LastID>2505434091</a:LastID><a:PageSize>10</a:PageSize><a:TotalCount>0</a:TotalCount></GetBookingHistoryResult></GetBookingHistoryResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(aceGetBookingHistoryCtrl, conti);
         
         // result is the return value of the callback
         System.assertEquals(null, result);
        
         //-- Assert to state that error occured while processing the response
        System.assert(aceGetBookingHistoryCtrl.isErrorOccured);   
         
         //-- Assert stating no history data returend
        // System.assert(aceGetBookingHistoryCtrl.bookingHistoryDOListSorted.size()<=0);   
         
         
                
        Test.stopTest();
    }
    
     @isTest public static void testGetBookingHistoryMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new WS_AsyncAceGetBookingHistoryMock());
        Test.startTest();
         //AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
          //tempuriOrg.basicHttpsBookingService basicHttpsObj1 = new tempuriOrg.basicHttpsBookingService();
        //basicHttpsObj1.GetBookingHistory('',new schemasDatacontractOrg200407AceEnt.GetBookingHistoryRequestData());
        ACEBookingService.BasicHttpsBinding_IBookingService basicHttpsObj1 = new ACEBookingService.BasicHttpsBinding_IBookingService();
        basicHttpsObj1.GetBookingHistory('',new ACEBookingService.GetBookingHistoryRequestData());
        Test.stopTest();
        
      
      }
   
   
}