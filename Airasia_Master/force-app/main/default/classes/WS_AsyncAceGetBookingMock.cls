public class WS_AsyncAceGetBookingMock implements WebServiceMock {
    public void doInvoke(
        Object stub,
        Object request,
        Map < String, Object > response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            
            
            response.put('response_x', new ACEBookingService.GetBookingResponse_element());
            
        }
}