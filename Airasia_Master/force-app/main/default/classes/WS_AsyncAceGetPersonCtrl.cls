public without sharing class WS_AsyncAceGetPersonCtrl {
    
    //-- Variable for Application Logging
    private static List < ApplicationLogWrapper > appWrapperLogList = null;
    public  boolean throwUnitTestExceptions {set;get;}
    private System_Settings__mdt logLevelCMD{get;set;}
    private static String processLog = '';
    private static DateTime startTime = DateTime.now();
    private static String payLoad = '';
    
    /* AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
    private AsyncTempuriOrgPersn.GetPersonResponse_elementFuture getPersonResponseFuture; 
    public schemasDatacontractOrg200407AceEntPersn.Person getPersonResponseData{get;set;}
    */
    private AsyncACEPersonService.GetPersonResponse_elementFuture getPersonResponseFuture; 
    public ACEPersonService.Person getPersonResponseData{get;set;}
    public String title{get;set;}
    public String firstName{get;set;}
    public String lastName{get;set;}
    public String nationality{get;set;}
    public String dOB{get;set;}
    public String jsonRespStr{get;set;}    
    public String errorMessage{get;set;}
    public boolean isErrorOccured{get;set;}   
    public static final String NO_PERSON_DATA_FOUND ='No Person Details Found.';
    
    public WS_AsyncAceGetPersonCtrl(){
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('GetPersonInterface_Log_Level_CMD_NAME'));
    } 
    
    /**
* This method is called on load of the Visulaforce page to get the ACE session token and initiated the
* GetPerson WS call to get the Person details.
* Continuation framework is used to make an asynchronous call.
**/
    public Object startAceGetPerson(){
        
        JSONGenerator jsonResObj = JSON.createGenerator(true);
        jsonResObj.writeStartObject();
        errorMessage = '';
        isErrorOccured =false;
        processLog='';
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        payLoad = '';
        try{
            
            //-- get the customer number
            String customerNumber = ApexPages.currentPage().getParameters().get(WS_PersonConstants.CUSTOMER_NUMBER);      
            
            Continuation con = new Continuation(60);
            //-- Code block for unit test case
            if (Test.isRunningtest() && throwUnitTestExceptions) {
                con=null;
            }   
            con.continuationMethod='processAceGetPersonResponse';
            // get Ace Session ID
            String aceSessionID = Utilities.getAceSessionID();

            //AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
            //schemasDatacontractOrg200407AceEntPersn.GetPersonByCustomerNumber getPersonByCustNo = new schemasDatacontractOrg200407AceEntPersn.GetPersonByCustomerNumber();
            ACEPersonService.GetPersonByCustomerNumber getPersonByCustNo = new ACEPersonService.GetPersonByCustomerNumber();
            getPersonByCustNo.CustomerNumber=customerNumber;//'2136815211';
            
            // Build Request for findPerson
            //AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
            //schemasDatacontractOrg200407AceEntPersn.GetPersonRequestData getPersonInput = new schemasDatacontractOrg200407AceEntPersn.GetPersonRequestData();
            ACEPersonService.GetPersonRequestData getPersonInput = new ACEPersonService.GetPersonRequestData();
            getPersonInput.GetPersonBy='GetPersonByCustomerNumber';
            getPersonInput.GetPersonByCustomerNumber=getPersonByCustNo;   
            
            //-- Logging Input Payload
            payLoad += 'Input SOAP Message : \r\n';
            payLoad += getPersonInput.toString() + '\r\n';
            
            // call FindPerson
            //AT 26 Apr 2019 - Upgrade ACE to GCP-ACE
            //AsyncTempuriOrgPersn.AsyncbasicHttpsPersonService asyncPort = new AsyncTempuriOrgPersn.AsyncbasicHttpsPersonService();
            AsyncACEPersonService.AsyncBasicHttpsBinding_IPersonService asyncPort = new AsyncACEPersonService.AsyncBasicHttpsBinding_IPersonService();
            getPersonResponseFuture = asyncPort.beginGetPerson(con,aceSessionID,getPersonInput);
            
            return con;
            
            
        }catch(Exception getPersonSessionResponseFailedEx){
            
            isErrorOccured =true;
            errorMessage = getPersonSessionResponseFailedEx.getMessage();
            
            //-- build JSON Response
            jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED,isErrorOccured);
            jsonResObj.writeStringField(WS_PersonConstants.ERROR_MESSAGE,errorMessage);
            
            //-- Perform Application logging
            processLog += 'Failed to get ACE Session ID/initiate the get Person WS Call: \r\n';
            processLog += getPersonSessionResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetPersonCtrl', 'startAceGetPerson',
                                                                 null, 'Get Person Interface Call',  processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, getPersonSessionResponseFailedEx));
            
        }finally{
            jsonResObj.writeEndObject();
            jsonRespStr =  jsonResObj.getAsString(); 
            
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
        
        return null;
    }
    
    /**
* This method parses the WS response data and builds required Do
**/
    public Object processAceGetPersonResponse(){
        
        
        JSONGenerator jsonResObj = JSON.createGenerator(true);                
        jsonResObj.writeStartObject();
        errorMessage = '';
        isErrorOccured =false;
        processLog='';
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        payLoad = '';
        
        try{
            
            //-- Get the response data
            getPersonResponseData = getPersonResponseFuture.getValue(); 
            
            //-- Perform Null check
            if(getPersonResponseData!=null && getPersonResponseData.PersonNameList!=null && getPersonResponseData.PersonNameList.PersonName !=null){
                
                //-- build log content
                payLoad += 'Output SOAP Message : \r\n';
                payLoad += getPersonResponseData.toString() + '\r\n';
                
                nationality = getPersonResponseData.nationality;                    
                if(getPersonResponseData.DOB!=null){
                    Date dOBDateTime=getPersonResponseData.DOB.date();
                    dob = dOBDateTime.format();
                }                    
                
                //if(getPersonResponseData.PersonNameList!=null && getPersonResponseData.PersonNameList.PersonName !=null){    
                    
                //AT 26 Apr 2019 - Upgrade ACE to GCP-ACE                    
                //for(schemasDatacontractOrg200407AceEntPersn.PersonName eachPersonName : getPersonResponseData.PersonNameList.PersonName){                            
                //    schemasDatacontractOrg200407AceEntPersn.Name eachPerson = eachPersonName.Name;
                for(ACEPersonService.PersonName eachPersonName : getPersonResponseData.PersonNameList.PersonName){                            
                    ACEPersonService.Name eachPerson = eachPersonName.Name;
                    title=eachPersonName.Name.Title;
                    System.debug('Title:'+eachPersonName.Name.Title);
                    firstName=eachPersonName.Name.FirstName;
                    lastName=eachPersonName.Name.lastName;
                    //bigmemberId=eachPersonName.Name.bigmemberId;                            
                } 
                //}      
                
                //-- build JSON Response               
                jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED,isErrorOccured);  
                jsonResObj.writeStringField(WS_PersonConstants.TITLE,title);
                jsonResObj.writeStringField(WS_PersonConstants.FIRST_NAME,firstName);
                jsonResObj.writeStringField(WS_PersonConstants.LAST_NAME, lastName);    
                jsonResObj.writeStringField(WS_PersonConstants.NATIONALITY, nationality);  
                jsonResObj.writeStringField(WS_PersonConstants.DOB, dOB);
                
            }else{
                isErrorOccured =true;
                errorMessage = NO_PERSON_DATA_FOUND;                
                
                //-- build JSON Response 
                jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED,isErrorOccured);
                jsonResObj.writeStringField(WS_PersonConstants.ERROR_MESSAGE,NO_PERSON_DATA_FOUND);
            }
        }catch(Exception getPersonResponseFailedEx){
            
            isErrorOccured =true;
            errorMessage = getPersonResponseFailedEx.getStackTraceString();
            
            //-- build JSON Response
            jsonResObj.writeBooleanField(WS_PersonConstants.IS_ERROR_OCCURED,isErrorOccured);
            jsonResObj.writeStringField(WS_PersonConstants.ERROR_MESSAGE,errorMessage);            
            
            //-- Perform Application logging
            processLog += 'Failed to process the GetPerson WS Response : \r\n';
            processLog += getPersonResponseFailedEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_AsyncAceGetPersonCtrl', 'processAceGetPersonResponse',
                                                                 null, 'Get Person Response Processing', processLog, payLoad, 'Integration Log',  startTime, logLevelCMD, getPersonResponseFailedEx));
            
            
            
        }finally{
            jsonResObj.writeEndObject();
            jsonRespStr =  jsonResObj.getAsString(); 
            
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
        return null;
    }
    
}