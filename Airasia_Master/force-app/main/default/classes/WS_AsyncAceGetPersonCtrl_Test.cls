@isTest public class WS_AsyncAceGetPersonCtrl_Test {

    
    
     /**
     * Scenario 1 : When Get Person WS results vaid response data for a mataching CustomerId from Navitaire system 
     * 
     * */
    public static testmethod void testAceGetPerson(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_GetPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetPersonCtrl wsAsyncAceGetPersonCtrlObj = new WS_AsyncAceGetPersonCtrl();
        wsAsyncAceGetPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) wsAsyncAceGetPersonCtrlObj.startAceGetPerson();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><GetPersonResponse xmlns="http://tempuri.org/"><GetPersonResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>451</a:NSProcessTime><a:ProcessTime>455</a:ProcessTime><a:State>Clean</a:State><a:PersonID>3607706</a:PersonID><a:PersonType>Customer</a:PersonType><a:PersonStatus>Active</a:PersonStatus><a:CultureCode>en-GB</a:CultureCode><a:DOB>1978-01-01T00:00:00</a:DOB><a:Gender>Female</a:Gender><a:NationalIDNumber/><a:TypeBAddress i:nil="true"/><a:CustomerNumber>2136815211</a:CustomerNumber><a:TrustLevel>1</a:TrustLevel><a:PaxType>ADT</a:PaxType><a:Nationality>MY</a:Nationality><a:ResidentCountry>  </a:ResidentCountry><a:NotificationPreference>None</a:NotificationPreference><a:PersonAddressList i:nil="true"/><a:PersonComments i:nil="true"/><a:PersonEMailList i:nil="true"/><a:PersonFOPList i:nil="true"/><a:PersonInfoList i:nil="true"/><a:PersonNameList><a:PersonName><a:State>Clean</a:State><a:CreatedAgentID>13</a:CreatedAgentID><a:CreatedDate>2009-05-11T12:00:00</a:CreatedDate><a:ModifiedAgentID>19641252</a:ModifiedAgentID><a:ModifiedDate>2017-06-30T06:44:27.017</a:ModifiedDate><a:Name><a:FirstName>Test User</a:FirstName><a:LastName>Tai</a:LastName><a:MiddleName/><a:Suffix/><a:Title>MS</a:Title></a:Name><a:NameType>True</a:NameType><a:PersonID>3607706</a:PersonID><a:PersonNameID>3604407</a:PersonNameID></a:PersonName></a:PersonNameList><a:PersonPhoneList i:nil="true"/><a:TravelDocs i:nil="true"/><a:PersonCustomerPrograms i:nil="true"/><a:PersonAttachments i:nil="true"/><a:PersonContactList i:nil="true"/><a:PersonUnitPreferences i:nil="true"/><a:PersonAffiliationList i:nil="true"/></GetPersonResult></GetPersonResponse></s:Body></s:Envelope>';        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(wsAsyncAceGetPersonCtrlObj, conti);
        
           // result is the return value of the callback
        System.assertEquals(null, result);
        
        //-- Assert to state that No error occured while processing the response
        //System.assert(!wsAsyncAceGetPersonCtrlObj.isErrorOccured);   
        
        Test.stopTest();
    }
    
     /**
     * Scenario 2 : When Get Person WS request is made for an EMpty Customer Id
     * 
     * */
    public static testmethod void testAceGetPerson_NoCustomerId(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_GetPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetPersonCtrl wsAsyncAceGetPersonCtrlObj = new WS_AsyncAceGetPersonCtrl();
        wsAsyncAceGetPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) wsAsyncAceGetPersonCtrlObj.startAceGetPerson();
        
           // result is the return value of the callback
        //System.assertEquals(null, conti);
        
        //-- Assert to state that error occured while processing the response
       // System.assert(wsAsyncAceGetPersonCtrlObj.isErrorOccured);   
        
        Test.stopTest();
    }
    
     /**
     * Scenario 3 : When Get Person WS responds with in invalid response data
     * 
     * */
    public static testmethod void testAceGetPerson_InvalidResponse(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_GetPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetPersonCtrl wsAsyncAceGetPersonCtrlObj = new WS_AsyncAceGetPersonCtrl();
        wsAsyncAceGetPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) wsAsyncAceGetPersonCtrlObj.startAceGetPerson();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        String responseXML ='<s:Envelope xmlns:s="http://schenvelope>';   
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(wsAsyncAceGetPersonCtrlObj, conti);
        
        // result is the return value of the callback
       // System.assertEquals(null, conti);
        
        //-- Assert to state that error occured while processing the response
        System.assert(wsAsyncAceGetPersonCtrlObj.isErrorOccured);   
        
        Test.stopTest();
    }
    
   /**
    * Scenario 4 : When Get Person WS responds with no person records
    * 
    * */
    public static testmethod void testAceGetPerson_NoPersonDataFound(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_GetPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetPersonCtrl wsAsyncAceGetPersonCtrlObj = new WS_AsyncAceGetPersonCtrl();
        wsAsyncAceGetPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) wsAsyncAceGetPersonCtrlObj.startAceGetPerson();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><s:Fault><faultcode xmlns:a="http://schemas.microsoft.com/net/2005/12/windowscommunicationfoundation/dispatcher">a:InternalServiceFault</faultcode><faultstring xml:lang="en-US">Object reference not set to an instance of an object.</faultstring><detail><ExceptionDetail xmlns="http://schemas.datacontract.org/2004/07/System.ServiceModel" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><HelpLink i:nil="true"/><InnerException i:nil="true"/><Message>Object reference not set to an instance of an object.</Message><StackTrace>   at ACE.Business.Pers22:34:52:000 USER_D</StackTrace><Type>System.NullReferenceException</Type></ExceptionDetail></detail></s:Fault></s:Body></s:Envelope>';   
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(wsAsyncAceGetPersonCtrlObj, conti);
        
        // result is the return value of the callback
       // System.assertEquals(null, conti);
        
        //-- Assert to state that error occured while processing the response
        System.assert(wsAsyncAceGetPersonCtrlObj.isErrorOccured);   
        
        Test.stopTest();
    }
    
    /**
    * Scenario 5 : When Get Person WS results in RunTime Exception
    * 
    * */
    public static testmethod void testAceGetPersonRunTimeException(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_GetPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetPersonCtrl wsAsyncAceGetPersonCtrlObj = new WS_AsyncAceGetPersonCtrl();
        wsAsyncAceGetPersonCtrlObj.throwUnitTestExceptions=true;
        Continuation conti = (Continuation) wsAsyncAceGetPersonCtrlObj.startAceGetPerson();
       
        // result is the return value of the callback
        System.assertEquals(null, conti);
        
        //-- Assert to state that error occured while processing the response
        System.assert(wsAsyncAceGetPersonCtrlObj.isErrorOccured);   
        
        Test.stopTest();
    }
    
     /**
    * Scenario 6 : When Person Detail elements not found
    * 
    * */
    public static testmethod void testAceFindPerson_Null(){
        
          // set mock implementation     
        Test.setMock(WebServiceMock.class, new SearchPersonAccountConsoleAppCtrlMock());
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.WS_GetPersonInterface'));        
        System.currentPageReference().getParameters().put(WS_PersonConstants.CUSTOMER_NUMBER,'123123123');
        
        
       
        // Invoke the continuation by calling the action method 
        WS_AsyncAceGetPersonCtrl wsAsyncAceGetPersonCtrlObj = new WS_AsyncAceGetPersonCtrl();
         wsAsyncAceGetPersonCtrlObj.throwUnitTestExceptions=false;
        Continuation conti = (Continuation) wsAsyncAceGetPersonCtrlObj.startAceGetPerson();
        
        // Verify that the continuation has the proper requests 
        Map < String, HttpRequest > requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock call out 
        // (i.e. skip the call out and call the callback method) 
        HttpResponse response = new HttpResponse();
        String responseXML ='<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><GetPersonResponse xmlns="http://tempuri.org/"><GetPersonResult xmlns:a="http://schemas.datacontract.org/2004/07/ACE.Entities" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><a:ExceptionMessage i:nil="true"/><a:NSProcessTime>451</a:NSProcessTime><a:ProcessTime>455</a:ProcessTime><a:State>Clean</a:State><a:PersonID>3607706</a:PersonID><a:PersonType>Customer</a:PersonType><a:PersonStatus>Active</a:PersonStatus><a:CultureCode>en-GB</a:CultureCode><a:DOB>1978-01-01T00:00:00</a:DOB><a:Gender>Female</a:Gender><a:NationalIDNumber/><a:TypeBAddress i:nil="true"/><a:CustomerNumber>2136815211</a:CustomerNumber><a:TrustLevel>1</a:TrustLevel><a:PaxType>ADT</a:PaxType><a:Nationality>MY</a:Nationality><a:ResidentCountry>  </a:ResidentCountry><a:NotificationPreference>None</a:NotificationPreference><a:PersonAddressList i:nil="true"/><a:PersonComments i:nil="true"/><a:PersonEMailList i:nil="true"/><a:PersonFOPList i:nil="true"/><a:PersonInfoList i:nil="true"/><a:PersonPhoneList i:nil="true"/><a:TravelDocs i:nil="true"/><a:PersonCustomerPrograms i:nil="true"/><a:PersonAttachments i:nil="true"/><a:PersonContactList i:nil="true"/><a:PersonUnitPreferences i:nil="true"/><a:PersonAffiliationList i:nil="true"/></GetPersonResult></GetPersonResponse></s:Body></s:Envelope>';
        response.setBody(responseXML);
        
        // Set the fake response for the continuation 
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method 
        Object result = Test.invokeContinuationMethod(wsAsyncAceGetPersonCtrlObj, conti);
        
         // result is the return value of the callback
        //System.assertEquals(null, conti);
        
        //-- Assert to state that error occured while processing the response
        System.assert(wsAsyncAceGetPersonCtrlObj.isErrorOccured);   
        
        Test.stopTest();
    }
}