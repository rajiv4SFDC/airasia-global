public class WS_BookingLauncherPageController {
    
    //-- Variable for Application Logging
    private static List < ApplicationLogWrapper > appWrapperLogList = null;
    private System_Settings__mdt logLevelCMD{get;set;}
    private static String processLog = '';
    private static DateTime startTime = DateTime.now();
    
    
    
    private final Case caseObj;
    public String caseId{get;set;}
    public LIST<External_URLS__mdt> externalURLObjList{get;set;}
    public String bookingNumber{get;set;}
    public String bookingDetailsVFPageURL{get;set;}
    public Integer externalURLObjListSize{get;set;}
    
    public WS_BookingLauncherPageController(ApexPages.StandardController stdController) {
        
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('GetBooking_Log_Level_CMD_NAME'));
        appWrapperLogList = new List < ApplicationLogWrapper > ();
        processLog='';
        externalURLObjListSize = 0;
        try{
            
            this.caseObj = (Case)stdController.getRecord();
            this.externalURLObjList = new LIST<External_URLS__mdt>();
            
            if(this.caseObj!=null && this.caseObj.Id!=null){
                
                caseId = this.caseObj.Id;
                
                Case caseObj = [SELECT Id,Booking_Number__c FROM CASE WHERE Id =:caseId];    
                
                bookingNumber = caseObj.Booking_Number__c;                
                bookingDetailsVFPageURL = '/apex/GetBookingDetails?bookingNumber='+bookingNumber;
                
            }
            
            if(Test.isRunningTest() && this.caseObj.Id == null){
                throw new DmlException('Case Id Null');                
            }
            
            //-- SOQL Query to fetch the external URLS from custom metadata
            externalURLObjList = [SELECT URL__c,Launch_Mode__c,MasterLabel FROM External_URLS__mdt WHERE Launch_Mode__c!=''];
            
            //-- get the size of number of external urls avilable
            if(externalURLObjList!=null && externalURLObjList.size()>0){
                externalURLObjListSize = externalURLObjList.size();
            }
            
        }catch(Exception getLaunchingPageEx){
            
            //-- Perform Application logging
            processLog += 'Failed while rendering Booking Launcher Page: \r\n';
            processLog += getLaunchingPageEx.getMessage() + '\r\n';
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WS_BookingLauncherPageController', 'WS_BookingLauncherPageController',
                                                                 null, 'Launch Navitaire GetBooking', processLog, null, 'Error Log',  startTime, logLevelCMD, getLaunchingPageEx));
            
        }finally{
            if (!appWrapperLogList.isEmpty())
                GlobalUtility.logMessage(appWrapperLogList);
        }
    }    
}