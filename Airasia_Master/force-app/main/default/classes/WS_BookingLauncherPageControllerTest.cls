@IsTest private class WS_BookingLauncherPageControllerTest {
    
    @IsTest private static void testLauncherPage(){
        
        Integer preLogCount = [SELECT COUNT() FROM Application_Log__c];
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Priority', 'Low');
        TestUtility.createCase(caseFieldValueMap);
        
        Case newCase = (Case)TestUtility.getCase();
        
        List<Case> caseList =  [SELECT Id,Booking_Number__c FROM CASE LIMIT 1];            
        if(caseList!=null && caseList.size()>0){
            
            Test.startTest();                
            PageReference pageRef = Page.WS_BookingLauncherPage;
            pageRef.getParameters().put('bookingNumber',caseList.get(0).Booking_Number__c);
            Test.setCurrentPage(pageRef);
            
            
            ApexPages.StandardController stdContrller = new ApexPages.StandardController(newCase);  
            WS_BookingLauncherPageController cntrlObj = new WS_BookingLauncherPageController(stdContrller);  
            
            Integer postLogCount = [SELECT COUNT() FROM Application_Log__c];
            
            //-- Assert to check that No Exception has occured
            System.assert(postLogCount==preLogCount);     
            
            Test.stopTest();
        } 
        
        
    }
    
     @IsTest private static void testLauncherPageExceptionCase(){
        
        Integer preLogCount = [SELECT COUNT() FROM Application_Log__c];
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Priority', 'Low');
        TestUtility.createCase(caseFieldValueMap);
        
        Case newCase = (Case)TestUtility.getCase();
        
        List<Case> caseList =  [SELECT Id,Booking_Number__c FROM CASE LIMIT 1];            
        if(caseList!=null && caseList.size()>0){
            
            Test.startTest();                
            PageReference pageRef = Page.WS_BookingLauncherPage;
            pageRef.getParameters().put('bookingNumber',caseList.get(0).Booking_Number__c);
            Test.setCurrentPage(pageRef);
            
            newCase.Id=null;
            ApexPages.StandardController stdContrller = new ApexPages.StandardController(newCase);  
            WS_BookingLauncherPageController cntrlObj = new WS_BookingLauncherPageController(stdContrller);  
            
            Integer postLogCount = [SELECT COUNT() FROM Application_Log__c];
            
            //-- Assert to check that No Exception has occured
           // System.assert(postLogCount>preLogCount);     
            
            Test.stopTest();
        }
    
    }
}