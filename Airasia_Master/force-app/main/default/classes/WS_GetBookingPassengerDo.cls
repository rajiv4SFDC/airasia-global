public class WS_GetBookingPassengerDo {
    public String firstName{get;set;}
    public String lastName{get;set;}
    public String title{get;set;}
    public List<String> passengerBoardingStatusList{get;set;}    
    public List<WS_GetBookingPassengerFeeDO> passengerFeeDOList{get;set;}
}