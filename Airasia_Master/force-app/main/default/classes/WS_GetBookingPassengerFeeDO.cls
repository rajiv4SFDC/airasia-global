public class WS_GetBookingPassengerFeeDO {
    public String feeCode{get;set;}
    public String dateAdded{get;set;}
    public String currencyVal{get;set;}
    public Decimal total{get;set;}
}