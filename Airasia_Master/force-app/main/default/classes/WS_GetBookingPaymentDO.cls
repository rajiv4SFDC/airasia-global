public class WS_GetBookingPaymentDO {
    
    public String paymentMethodCode{get;set;}
    public String paymentMethodType{get;set;}
    public String collectedCurrencyCode {get;set;}
    public Decimal paymentAmount{get;set;}    
    public String createdDate{get;set;}
    
}