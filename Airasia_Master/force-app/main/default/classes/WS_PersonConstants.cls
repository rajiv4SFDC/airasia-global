public class WS_PersonConstants {
    
    public static final String IS_ERROR_OCCURED ='isErrorOccured';
    
    public static final String TITLE ='Title';
    
    public static final String FIRST_NAME ='FirstName';
    
    public static final String LAST_NAME ='LastName';
    
    public static final String NATIONALITY ='Nationality';
    
    public static final String DOB ='DOB';
    
    public static final String ERROR_MESSAGE ='ErrorMessage';
    
    public static final String CUSTOMER_NUMBER ='CustomerNumber';
    
    public static final String PERSON_EMAIL_REQUEST ='PersonEmail';
    
    public static final String PERSON_ID ='PersonID';
    
    public static final String PERSON_TYPE ='PersonType';
    
    public static final String PHONE_NUMBER ='PhoneNumber';
    
    public static final String STATUS ='Status';
    
    public static final String EMAIL ='EMail';
    
    public static final String ADDRESS_LINE_1 ='AddressLine1';
    
    public static final String ADDRESS_LINE_2 ='AddressLine2';
    
    public static final String ADDRESS_LINE_3 ='AddressLine3';
    
    public static final String CITY ='City';
    
    public static final String COUNTRY_CODE ='CountryCode';
    
    public static final String POSTAL_CODE ='PostalCode';
    
    public static final String PROVINCE_STATE ='ProvinceState';
    
    
    public static final String MIDDLE_NAME ='MiddleName';
    
    public static final String IS_CONTACT_EXIST ='isContactExistByEmailId';
    
    
    
    
    
    
    
    
    
    
}