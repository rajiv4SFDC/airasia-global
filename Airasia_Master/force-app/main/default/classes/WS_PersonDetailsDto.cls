public without sharing class WS_PersonDetailsDto {
 public String customerNumber {
  get;
  set;
 }
 public String personEmailAddress {
  get;
  set;
 }
 public Long personID {
  get;
  set;
 }
 public String personType {
  get;
  set;
 }
 public String phoneNumber {
  get;
  set;
 }
 public String status {
  get;
  set;
 }
 public String addressLine1 {
  get;
  set;
 }
 public String addressLine2 {
  get;
  set;
 }
 public String addressLine3 {
  get;
  set;
 }
 public String city {
  get;
  set;
 }
 public String countryCode {
  get;
  set;
 }
 public String postalCode {
  get;
  set;
 }
 public String provinceState {
  get;
  set;
 }
 public String title {
  get;
  set;
 }
 public String firstName {
  get;
  set;
 }
 public String lastName {
  get;
  set;
 }
 public String nationality {
  get;
  set;
 }
 public String dOB {
  get;
  set;
 }
 public String bigMemberId {
  get;
  set;
 }
 public String errorMessage {
  get;
  set;
 }
 public boolean isErrorOccured {
  get {
   if (isErrorOccured == null)
    return false;
   return isErrorOccured;
  }

  set;
 }

 public boolean isContactExist {
  get;
  set;
 }

 public String webName {
  get;
  set;
 }
    public String personTitle {
        get;
        set;
    }
    
}