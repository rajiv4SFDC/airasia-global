@RestResource(urlMapping='/save-access-token/*')
global class WeChatAccessTokenRestResource
{
    @HttpPost
    global static void doPost()
    {
        String responseMessage = '';
        try
        {
            Pattern saveTokenPattern = Pattern.compile('/save-access-token(.*)');
            Matcher saveTokenMatch = saveTokenPattern.matcher(RestContext.request.requestURI);
            if(saveTokenMatch.matches() && RestContext.request.requestBody != null)
            {
                responseMessage = saveAccessToken(RestContext.request.requestBody.toString());
            }
            else
            {
                RestContext.response.statusCode = 400;
                responseMessage = 'no match';
            }
        }
        catch(Exception ex)
        {
            insert (new Charket__Log__c(Charket__ApexCodeName__c = 'WeChatAccessTokenRestResource', Charket__Message__c = ex.getMessage()));
            RestContext.response.statusCode = 400;
            responseMessage = ex.getMessage();
        }
        
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(responseMessage);
    }
    
    private static String saveAccessToken(String requestBody)
    {
        Map<String, Object> requestBodyMap = (Map<String, Object>)JSON.deserializeUntyped(requestBody);
        
        String wechatOriginId = (String)requestBodyMap.get('wechatOriginId');
        String accessToken = (String)requestBodyMap.get('accessToken');
        Integer expiresIn = (Integer)requestBodyMap.get('expiresIn');
        Charket.WeChatAccessToken.setAccessToken(wechatOriginId, accessToken, expiresIn);
        
        return 'success';
    }
}