/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/06/2017      Pawan Kumar         Created
21/11/2019		Alvin Tayag			Add Risk in Web Case Routing
*******************************************************************/
public without sharing class WebCaseRouting {
    private static System_Settings__mdt logLevelCMD;
    private static DateTime startTime = DateTime.now();
    private static String processLog = '';
    private static String restrictedCaseType = '';
    private static List < ApplicationLogWrapper > appWrapperLogList = new List < ApplicationLogWrapper > ();
    private static final String DELIMITER = '#~#';
    
    @InvocableMethod
    public static void assignCaseToRelevantQueue(List < Id > caseIds) {
        // get System Setting details for Logging
        logLevelCMD = Utilities.getLogLevel(Utilities.getAppSettingsMdtValueByKey('Routing_Log_Level_CMD_NAME'));
        //restrictedCaseType = Utilities.getAppSettingsMdtValueByKey('WCR_Restricted_Case_Type');
        
        try {
            // Query Case for extra details
            List < Case > updateCaseList = null;
            List < Case > caseList = [Select Id, Priority, Case_Language__c, Airline_Code__c, Type, Sub_Category_1__c,
                                      Sub_Category_2__c, Risk__c from Case where Id IN: caseIds
                                     ];
            
            if (!caseList.isEmpty()) {
                
                updateCaseList = new List < Case > ();
                
                Map < String, String > webCaseRoutingMap = new Map < String, String > ();
                List < Web_Case_Routing__mdt > webCaseRoutingList = [Select Id, Case_Priority__c, Case_Language__c, Airline_Code__c, 
                                                                     Case_Type__c, Case_Subtype_1__c, Case_Subtype_2__c, Queue__c, Risk__c from Web_Case_Routing__mdt];
                for (Web_Case_Routing__mdt eachWebRow: webCaseRoutingList) {
                    
                    // for case Type =='Refund'
                    //webCaseRoutingMap.put(eachWebRow.Case_Priority__c + DELIMITER + eachWebRow.Case_Language__c + DELIMITER +
                    //                      eachWebRow.Airline_Code__c + DELIMITER + eachWebRow.Case_Type__c + DELIMITER +
                    //                      eachWebRow.Case_Subtype_1__c + DELIMITER + eachWebRow.Case_Subtype_2__c, eachWebRow.Queue__c);
                    
                    // for case Type = !'Refund'
                    webCaseRoutingMap.put(eachWebRow.Case_Priority__c + DELIMITER + eachWebRow.Case_Language__c + DELIMITER +
                                          eachWebRow.Airline_Code__c + DELIMITER + eachWebRow.Case_Type__c + DELIMITER + eachWebRow.Risk__c, eachWebRow.Queue__c);
                    
                }
                //System.debug('webCaseRoutingMap:'+JSON.serializePretty(webCaseRoutingMap));
                
                for (Case caseToBeAssignedToQueue: caseList) {
                    String caseId = caseToBeAssignedToQueue.id;
                    try {
                        
                        String criteriaKey = '';
                        //if (restrictedCaseType.indexOf(caseToBeAssignedToQueue.Type) != -1) {
                            criteriaKey = caseToBeAssignedToQueue.Priority + DELIMITER + caseToBeAssignedToQueue.Case_Language__c + DELIMITER + 
                                caseToBeAssignedToQueue.Airline_Code__c + DELIMITER + caseToBeAssignedToQueue.Type + DELIMITER + caseToBeAssignedToQueue.Risk__c;
                        System.debug('Criteria Key: ' + criteriaKey);
                        /*} else {
                            criteriaKey = caseToBeAssignedToQueue.Priority + DELIMITER + caseToBeAssignedToQueue.Case_Language__c + DELIMITER + 
                                caseToBeAssignedToQueue.Airline_Code__c + DELIMITER + caseToBeAssignedToQueue.Type + DELIMITER + 
                                caseToBeAssignedToQueue.Sub_Category_1__c + DELIMITER + caseToBeAssignedToQueue.Sub_Category_2__c;
                            
                        }*/
                        
                        if (webCaseRoutingMap.containsKey(criteriaKey)) {
                            caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(webCaseRoutingMap.get(criteriaKey));
                        } else {
                            String soqlQuery = '';
                            //if (restrictedCaseType.indexOf(caseToBeAssignedToQueue.Type) != -1) {
                                soqlQuery = 'Select Id, Queue__c from Web_Case_Routing__mdt' +
                                    ' where Case_Priority__c =' + caseToBeAssignedToQueue.Priority +
                                    ' AND Case_Language__c =' + caseToBeAssignedToQueue.Case_Language__c +
                                    ' AND Airline_Code__c =' + caseToBeAssignedToQueue.Airline_Code__c +
                                    ' AND Case_Type__c =' + caseToBeAssignedToQueue.Type + '\r\n';
                            /*} else {
                                soqlQuery = 'Select Id, Queue__c from Web_Case_Routing__mdt' +
                                    ' where Case_Priority__c =' + caseToBeAssignedToQueue.Priority +
                                    ' AND Case_Language__c =' + caseToBeAssignedToQueue.Case_Language__c +
                                    ' AND Airline_Code__c =' + caseToBeAssignedToQueue.Airline_Code__c +
                                    ' AND Case_Type__c =' + caseToBeAssignedToQueue.Type +
                                    ' AND Case_Subtype_1__c =' + caseToBeAssignedToQueue.Sub_Category_1__c +
                                    ' AND Case_Subtype_2__c =' + caseToBeAssignedToQueue.Sub_Category_2__c + '\r\n';
                            }*/
                            throw new QueryException(soqlQuery);
                        }
                    } catch (Exception queueQueryEx) {
                        System.debug(LoggingLevel.ERROR, queueQueryEx);
                        
                        caseToBeAssignedToQueue.OwnerId = Utilities.getQueueIdByName(Utilities.getAppSettingsMdtValueByKey('Case_Routing_Error_Queue'));
                        processLog += 'Failed to get QUEUE NAME from routing table: \r\n';
                        processLog += queueQueryEx.getMessage() + '\r\n';
                        appWrapperLogList.add(Utilities.createApplicationLog('Warning', 'WebCaseRouting', 'assignCaseToRelevantQueue',
                                                                             caseId, 'Web Case Routing', processLog, null, 'Error Log', startTime, logLevelCMD, queueQueryEx));
                    }
                    
                    updateCaseList.add(caseToBeAssignedToQueue);
                }
                
                if (updateCaseList != null && !updateCaseList.isEmpty())
                    update updateCaseList;
            }
        } catch (Exception caseAssignFailedEx) {
            System.debug(LoggingLevel.ERROR, caseAssignFailedEx);
            
            String firstCaseId = '';
            if (caseIds != null && !caseIds.isEmpty()) {
                firstCaseId = caseIds[0];
            }
            
            processLog += 'Failed to assign queue for the following Case Ids: \r\n';
            processLog += JSON.serializePretty(caseIds) + '\r\n';
            processLog += 'Root Cause: ' + caseAssignFailedEx.getMessage() + '\r\n';
            
            appWrapperLogList.add(Utilities.createApplicationLog('Error', 'WebCaseRouting', 'assignCaseToRelevantQueue',
                                                                 firstCaseId, 'Web Case Routing', processLog, null, 'Error Log', startTime, logLevelCMD, caseAssignFailedEx));
            
        } finally {
            if (appWrapperLogList != null && !appWrapperLogList.isEmpty()) {
                GlobalUtility.logMessage(appWrapperLogList);
            }
        } // end finally
        
    } // end assignCaseToRelevantQueue
    
} // WebCaseRouting