/*******************************************************************
Author:        Pawan Kumar
Email:         kpawan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/06/2017      Pawan Kumar	        Created
*******************************************************************/
@isTest(SeeAllData=false)
private class WebCaseRouting_Test{
    private static testMethod void testPosWCRouting() {
        Test.startTest();
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Enquiry/Request');
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'OwnerId',
                'Origin',
                'Priority',
                'Case_Language__c',
                'Airline_Code__c',
                'Type',
                'Sub_Category_1__c',
                'Sub_Category_2__c'
                };
                    
                    List < Case > casesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_1', casesList[0].Subject);
        System.assertEquals('Low', casesList[0].Priority);
        System.assertEquals('English', casesList[0].Case_Language__c);
        System.assertEquals(null, casesList[0].Airline_Code__c);
        System.assertEquals('Enquiry/Request', casesList[0].Type);
        System.assertEquals(null, casesList[0].Sub_Category_1__c);
        System.assertEquals(null, casesList[0].Sub_Category_2__c);
        
        // call our routing logic.
        WebCaseRouting.assignCaseToRelevantQueue(new List < String > {
            casesList[0].Id
                });
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_1', updatedCasesList[0].Subject);
        System.assertEquals('Low', updatedCasesList[0].Priority);
        System.assertEquals('English', updatedCasesList[0].Case_Language__c);
        System.assertEquals(null, updatedCasesList[0].Airline_Code__c);
        System.assertEquals('Enquiry/Request', updatedCasesList[0].Type);
        System.assertEquals(null, updatedCasesList[0].Sub_Category_1__c);
        System.assertEquals(null, updatedCasesList[0].Sub_Category_2__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        String queueId = TestUtility.getQueueIdByName('MAA Customer Care');
        System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        Test.stopTest();
    }
    
    private static testMethod void testPos1WCRouting() {
        Test.startTest();
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_1');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Refund');
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        List < String > queryFieldList = new List < String > {
            'OwnerId',
                'Origin',
                'Priority',
                'Case_Language__c',
                'Airline_Code__c',
                'Type',
                'Sub_Category_1__c',
                'Sub_Category_2__c'
                };
                    
                    List < Case > casesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_1', casesList[0].Subject);
        System.assertEquals('Low', casesList[0].Priority);
        System.assertEquals('English', casesList[0].Case_Language__c);
        System.assertEquals(null, casesList[0].Airline_Code__c);
        System.assertEquals('Refund', casesList[0].Type);
        System.assertEquals(null, casesList[0].Sub_Category_1__c);
        System.assertEquals(null, casesList[0].Sub_Category_2__c);
        
        // call our routing logic.
        WebCaseRouting.assignCaseToRelevantQueue(new List < String > {
            casesList[0].Id
                });
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_1', updatedCasesList[0].Subject);
        System.assertEquals('Low', updatedCasesList[0].Priority);
        System.assertEquals('English', updatedCasesList[0].Case_Language__c);
        System.assertEquals(null, updatedCasesList[0].Airline_Code__c);
        System.assertEquals('Refund', updatedCasesList[0].Type);
        System.assertEquals(null, updatedCasesList[0].Sub_Category_1__c);
        System.assertEquals(null, updatedCasesList[0].Sub_Category_2__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        String queueId = TestUtility.getQueueIdByName('MAA Customer Care');
        //System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        Test.stopTest();
    }
    
    private static testMethod void testNegWCRouting() {
        Test.startTest();
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_2');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', 'English');
        caseFieldValueMap.put('Airline_Code__c', null);
        //aseFieldValueMap.put('Type', 'Enquiry/Request');  
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        // query created case
        List < String > queryFieldList = new List < String > {
            'OwnerId',
                'Origin',
                'Priority',
                'Case_Language__c',
                'Airline_Code__c',
                'Type',
                'Sub_Category_1__c',
                'Sub_Category_2__c'
                };
                    
                    List < Case > casesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_2', casesList[0].Subject);
        System.assertEquals('Low', casesList[0].Priority);
        System.assertEquals('English', casesList[0].Case_Language__c);
        System.assertEquals(null, casesList[0].Airline_Code__c);
        //System.assertEquals('Enquiry/Request', casesList[0].Type);
        System.assertEquals(null, casesList[0].Sub_Category_1__c);
        System.assertEquals(null, casesList[0].Sub_Category_2__c);
        
        // call our routing logic.
        WebCaseRouting.assignCaseToRelevantQueue(new List < String > {
            casesList[0].Id
                });
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_2', updatedCasesList[0].Subject);
        System.assertEquals('Low', updatedCasesList[0].Priority);
        System.assertEquals('English', updatedCasesList[0].Case_Language__c);
        System.assertEquals(null, updatedCasesList[0].Airline_Code__c);
        //System.assertEquals('Enquiry/Request', updatedCasesList[0].Type);
        System.assertEquals(null, updatedCasesList[0].Sub_Category_1__c);
        System.assertEquals(null, updatedCasesList[0].Sub_Category_2__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        String queueId = TestUtility.getQueueIdByName('Error Handling Queue');
        System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
        
        Test.stopTest();
    }
    
    private static testMethod void testNeg1WCRouting() {
        Test.startTest();
        
        // create test case  
        Map < String, Object > caseFieldValueMap = new Map < String, Object > ();
        caseFieldValueMap.put('Subject', 'Test_Case_2');
        caseFieldValueMap.put('Priority', 'Low');
        caseFieldValueMap.put('Case_Language__c', null);
        caseFieldValueMap.put('Airline_Code__c', null);
        caseFieldValueMap.put('Type', 'Enquiry/Request');  
        caseFieldValueMap.put('Sub_Category_1__c',null);
        caseFieldValueMap.put('Sub_Category_2__c', null);
        TestUtility.createCase(caseFieldValueMap);
        
        // query created case
        // query created case
        List < String > queryFieldList = new List < String > {
            'OwnerId',
                'Origin',
                'Priority',
                'Case_Language__c',
                'Airline_Code__c',
                'Type',
                'Sub_Category_1__c',
                'Sub_Category_2__c'
                };
                    
                    List < Case > casesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, casesList);
        System.assertEquals(1, casesList.size());
        System.assertEquals('Test_Case_2', casesList[0].Subject);
        System.assertEquals('Low', casesList[0].Priority);
        System.assertEquals(null, casesList[0].Case_Language__c);
        System.assertEquals(null, casesList[0].Airline_Code__c);
        //System.assertEquals('Enquiry/Request', casesList[0].Type);
        System.assertEquals(null, casesList[0].Sub_Category_1__c);
        System.assertEquals(null, casesList[0].Sub_Category_2__c);
        
        // call our routing logic.
        WebCaseRouting.assignCaseToRelevantQueue(new List < String > {
            casesList[0].Id
                });
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases(queryFieldList);
        System.assertnotEquals(null, updatedCasesList);
        System.assertEquals(1, updatedCasesList.size());
        System.assertEquals('Test_Case_2', updatedCasesList[0].Subject);
        System.assertEquals('Low', updatedCasesList[0].Priority);
        System.assertEquals(null, updatedCasesList[0].Case_Language__c);
        System.assertEquals(null, updatedCasesList[0].Airline_Code__c);
        //System.assertEquals('Enquiry/Request', updatedCasesList[0].Type);
        System.assertEquals(null, updatedCasesList[0].Sub_Category_1__c);
        System.assertEquals(null, updatedCasesList[0].Sub_Category_2__c);
        System.assertNotEquals(null, (updatedCasesList[0]).OwnerId);
        
        // assert queue with id case owner id
        //String queueId = TestUtility.getQueueIdByName('Error Handling Queue');
        //MAA Customer Care - 2019 Sept 11 - Changed resulting queue
        String queueId = TestUtility.getQueueIdByName('MAA Customer Care');
        System.assertEquals(queueId, updatedCasesList[0].OwnerId);
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        //System.assertEquals(1,appLogs.size());
        //MAA Customer Care - 2019 Sept 11 - Changed resulting queue
        System.assertEquals(0,appLogs.size());
        
        Test.stopTest();
    }
    
    private static testMethod void testNoCaseIdsRouting() {
        Test.startTest();
        
        // call our routing logic.
        WebCaseRouting.assignCaseToRelevantQueue(null);
        
        // query updated case by routing class.
        List < Case > updatedCasesList = (List < Case > ) TestUtility.getCases();
        System.assertNotEquals(null, updatedCasesList);
        System.assertEquals(0, updatedCasesList.size());
        
        // check application log created
        List<Application_Log__c> appLogs = TestUtility.getApplicationLogs();
        System.assertNotEquals(null,appLogs);
        System.assertEquals(1,appLogs.size());
        
        Test.stopTest();
    }
}