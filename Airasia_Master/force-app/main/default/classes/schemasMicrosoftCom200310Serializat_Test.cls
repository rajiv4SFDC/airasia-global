@isTest public class schemasMicrosoftCom200310Serializat_Test {
    
    /***
     * This is a formal Test class for code coverage of WsdlToApex class generated
     * 
     * 
     **/
    
    @isTest
    public static void testCodeCoverage(){
        
        schemasMicrosoftCom200310Serializat schemObj = new schemasMicrosoftCom200310Serializat(); 
        
        schemasMicrosoftCom200310Serializat.ArrayOfstring arrOFStr = new schemasMicrosoftCom200310Serializat.ArrayOfstring();
        arrOFStr.string_x=null;        
        arrOFStr.string_x_type_info =null;
        arrOFStr.apex_schema_type_info=null;
        arrOFStr.field_order_type_info=null;
        
        schemasMicrosoftCom200310Serializat.ArrayOfint arrOFInt = new schemasMicrosoftCom200310Serializat.ArrayOfint();
        arrOFInt.int_x=new Integer[]{1};
        arrOFInt.int_x_type_info =new String[]{'int','http://schemas.microsoft.com/2003/10/Serialization/Arrays',null,'0','-1','false'};
        arrOFInt.apex_schema_type_info=new String[]{'int_x'};
        arrOFInt.field_order_type_info = new String[]{'int_x'};
            
         schemasMicrosoftCom200310Serializat.ArrayOflong arrOFlong = new schemasMicrosoftCom200310Serializat.ArrayOflong();   
        arrOFlong.long_x=new Long[]{1};
        arrOFlong.long_x_type_info = new String[]{'long'};
        arrOFlong.apex_schema_type_info = new String[]{'long'};
        arrOFlong.field_order_type_info = new String[]{'long'};
                        
                       
        schemasMicrosoftCom200310Serializat.ArrayOfunsignedInt aryOfunsignedInt = new schemasMicrosoftCom200310Serializat.ArrayOfunsignedInt();       
        aryOfunsignedInt.int_x = new Integer[]{1};
        aryOfunsignedInt.int_x_type_info = new String[]{'int','http://schemas.microsoft.com/2003/10/Serialization/Arrays',null,'0','-1','false'};
        aryOfunsignedInt.apex_schema_type_info = new String[]{'http://schemas.microsoft.com/2003/10/Serialization/Arrays','true','false'};
        aryOfunsignedInt.field_order_type_info = new String[]{'int_x'};

            
            
        
        
    }

}