@isTest public class tempuriOrgPersn_Test {

    
    
    @isTest public static void testCodeCoverage(){
        
        
        tempuriOrgPersn tempuriOrgPersnObj = new tempuriOrgPersn();
        tempuriOrgPersn.FindPersonsResponse_element findPersonsResponseElObj= new tempuriOrgPersn.FindPersonsResponse_element();
        findPersonsResponseElObj.FindPersonsResult_type_info = new String[]{'FindPersonsResult','http://tempuri.org/',null,'0','1','true'};
        findPersonsResponseElObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        findPersonsResponseElObj.field_order_type_info = new String[]{'FindPersonsResult'};
            
            
            
        tempuriOrgPersn.FindPersons_element FindPersons_elementObj = new tempuriOrgPersn.FindPersons_element();
        FindPersons_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        FindPersons_elementObj.objFindPersonRequestData_type_info = new String[]{'objFindPersonRequestData','http://tempuri.org/',null,'0','1','true'};
        FindPersons_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
		FindPersons_elementObj.field_order_type_info = new String[]{'strSessionID','objFindPersonRequestData'};
                        
            
            
        tempuriOrgPersn.GetPerson_element GetPerson_elementobj = new tempuriOrgPersn.GetPerson_element();
        GetPerson_elementobj.strSessionID='';
        GetPerson_elementobj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        GetPerson_elementobj.objGetPersonRequestData_type_info = new String[]{'objGetPersonRequestData','http://tempuri.org/',null,'0','1','true'};
        GetPerson_elementobj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        GetPerson_elementobj.field_order_type_info = new String[]{'strSessionID','objGetPersonRequestData'};
    
        tempuriOrgPersn.GetPersonResponse_element GetPersonResponse_elementObj = new tempuriOrgPersn.GetPersonResponse_element();

        GetPersonResponse_elementObj.GetPersonResult=new schemasDatacontractOrg200407AceEntPersn.Person();
        //schemasDatacontractOrg200407AceEntPersn.Person = new schemasDatacontractOrg200407AceEntPersn.Person();
        GetPersonResponse_elementObj.GetPersonResult_type_info = new String[]{'GetPersonResult','http://tempuri.org/',null,'0','1','true'};
        GetPersonResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        GetPersonResponse_elementObj.field_order_type_info = new String[]{'GetPersonResult'};
    
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new tempuriOrgPersnMock());
        Test.startTest();
        tempuriOrgPersn.basicHttpsPersonService basicHttpsObj = new tempuriOrgPersn.basicHttpsPersonService();
        basicHttpsObj.GetPerson('',new schemasDatacontractOrg200407AceEntPersn.GetPersonRequestData());
        Test.stopTest();
        
        
               
        
    }
    
    
    
     @isTest public static void testCodeCoverageForRequest(){
        
        
        // set mock implementation     
        Test.setMock(WebServiceMock.class, new tempuriOrgPersnFindMock());
        Test.startTest();
        tempuriOrgPersn.basicHttpsPersonService basicHttpsObj1 = new tempuriOrgPersn.basicHttpsPersonService();
        basicHttpsObj1.FindPersons('',new schemasDatacontractOrg200407AceEntPersn.FindPersonRequest());
        Test.stopTest();
        
        
    }
}