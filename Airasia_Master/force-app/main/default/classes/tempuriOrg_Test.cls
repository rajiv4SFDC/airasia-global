@isTest public class tempuriOrg_Test {

    
    
    @isTest public static void testCodeCoverage(){
        
        
        tempuriOrg.UpdateContactsResponse_element UpdateContactsResponse_elementObj = new tempuriOrg.UpdateContactsResponse_element();
        UpdateContactsResponse_elementObj.UpdateContactsResult_type_info = new String[]{'UpdateContactsResult','http://tempuri.org/',null,'0','1','true'};
        UpdateContactsResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        UpdateContactsResponse_elementObj.field_order_type_info = new String[]{'UpdateContactsResult'};
        
            
       tempuriOrg.OverrideFeeResponse_element OverrideFeeResponse_elementObj = new tempuriOrg.OverrideFeeResponse_element();
       OverrideFeeResponse_elementObj.OverrideFeeResult = new schemasDatacontractOrg200407AceEnt.Booking();
       OverrideFeeResponse_elementObj.OverrideFeeResult_type_info = new String[]{'OverrideFeeResult','http://tempuri.org/',null,'0','1','true'};
      OverrideFeeResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
      OverrideFeeResponse_elementObj.field_order_type_info = new String[]{'OverrideFeeResult'};
  
            tempuriOrg.UnassignSeats_element UnassignSeats_elementObj = new tempuriOrg.UnassignSeats_element();
        UnassignSeats_elementObj.strSessionID='';
       // public schemasDatacontractOrg200407AceEnt.SeatSellRequest objSeatSellRequest;
       UnassignSeats_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
      UnassignSeats_elementObj.objSeatSellRequest_type_info = new String[]{'objSeatSellRequest','http://tempuri.org/',null,'0','1','true'};
       UnassignSeats_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
      UnassignSeats_elementObj.field_order_type_info = new String[]{'strSessionID','objSeatSellRequest'};
 
   tempuriOrg.ResellSSRResponse_element ResellSSRResponse_elementObj = new tempuriOrg.ResellSSRResponse_element();
      //  public schemasDatacontractOrg200407AceEnt.BookingUpdateResponseData ResellSSRResult;
       ResellSSRResponse_elementObj.ResellSSRResult_type_info = new String[]{'ResellSSRResult','http://tempuri.org/',null,'0','1','true'};
       ResellSSRResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       ResellSSRResponse_elementObj.field_order_type_info = new String[]{'ResellSSRResult'};
  
    tempuriOrg.GetBookingHistoryResponse_element GetBookingHistoryResponse_elementObj = new tempuriOrg.GetBookingHistoryResponse_element();
        GetBookingHistoryResponse_elementObj.GetBookingHistoryResult = new schemasDatacontractOrg200407AceEnt.GetBookingHistoryResponseData();
        GetBookingHistoryResponse_elementObj.GetBookingHistoryResult_type_info = new String[]{'GetBookingHistoryResult','http://tempuri.org/',null,'0','1','true'};
        GetBookingHistoryResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetBookingHistoryResponse_elementObj.field_order_type_info = new String[]{'GetBookingHistoryResult'};
    
  tempuriOrg.FindBookingResponse_element FindBookingResponse_elementObj = new tempuriOrg.FindBookingResponse_element();
      //  public schemasDatacontractOrg200407AceEnt.FindBookingResponseData FindBookingResult;
        FindBookingResponse_elementObj.FindBookingResult_type_info = new String[]{'FindBookingResult','http://tempuri.org/',null,'0','1','true'};
            FindBookingResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
                FindBookingResponse_elementObj.field_order_type_info = new String[]{'FindBookingResult'};
                    
                    tempuriOrg.Cancel_element Cancel_elementObj = new tempuriOrg.Cancel_element();
       Cancel_elementObj.strSessionID='';
      Cancel_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
         Cancel_elementObj.objCancelRequestData_type_info = new String[]{'objCancelRequestData','http://tempuri.org/',null,'0','1','true'};
         Cancel_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
          Cancel_elementObj.field_order_type_info = new String[]{'strSessionID','objCancelRequestData'};

    tempuriOrg.GetBookingPayments_element GetBookingPayments_elementObj = new tempuriOrg.GetBookingPayments_element();
       GetBookingPayments_elementObj.strSessionID='';
     GetBookingPayments_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
         GetBookingPayments_elementObj.objGetBookingPaymentsRequestData_type_info = new String[]{'objGetBookingPaymentsRequestData','http://tempuri.org/',null,'0','1','true'};
          GetBookingPayments_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
            GetBookingPayments_elementObj.field_order_type_info = new String[]{'strSessionID','objGetBookingPaymentsRequestData'};
    
    tempuriOrg.UpdateSSR_element UpdateSSR_elementObj = new tempuriOrg.UpdateSSR_element();
       UpdateSSR_elementObj.strSessionID='';
      UpdateSSR_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
      UpdateSSR_elementObj.objSSRRequest_type_info = new String[]{'objSSRRequest','http://tempuri.org/',null,'0','1','true'};
        UpdateSSR_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        UpdateSSR_elementObj.field_order_type_info = new String[]{'strSessionID','objSSRRequest'};
    
     tempuriOrg.GetSSRAvailability_element GetSSRAvailability_elementObj = new tempuriOrg.GetSSRAvailability_element();
        GetSSRAvailability_elementObj.strSessionID='';
       GetSSRAvailability_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        GetSSRAvailability_elementObj.objSSRAvailabilityRequest_type_info = new String[]{'objSSRAvailabilityRequest','http://tempuri.org/',null,'0','1','true'};
       GetSSRAvailability_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetSSRAvailability_elementObj.field_order_type_info = new String[]{'strSessionID','objSSRAvailabilityRequest'};
    
		tempuriOrg.GetBookingFromStateFiltered_element GetBookingFromStateFiltered_elementObj = new tempuriOrg.GetBookingFromStateFiltered_element();
       GetBookingFromStateFiltered_elementObj.strSessionID='';
      GetBookingFromStateFiltered_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
       GetBookingFromStateFiltered_elementObj.objGetBookingFromStateFilteredRequestData_type_info = new String[]{'objGetBookingFromStateFilteredRequestData','http://tempuri.org/',null,'0','1','true'};
       GetBookingFromStateFiltered_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetBookingFromStateFiltered_elementObj.field_order_type_info = new String[]{'strSessionID','objGetBookingFromStateFilteredRequestData'};
    
		tempuriOrg.CancelInProcessPaymentResponse_element CancelInProcessPaymentResponse_elementObj = new tempuriOrg.CancelInProcessPaymentResponse_element();
        CancelInProcessPaymentResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        CancelInProcessPaymentResponse_elementObj.field_order_type_info = new String[]{};
    
  tempuriOrg.FareOverride_element FareOverride_elementObj = new tempuriOrg.FareOverride_element();
        FareOverride_elementObj.strSessionID='';
      FareOverride_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        FareOverride_elementObj.objFareOverrideRequestData_type_info = new String[]{'objFareOverrideRequestData','http://tempuri.org/',null,'0','1','true'};
        FareOverride_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       FareOverride_elementObj.field_order_type_info = new String[]{'strSessionID','objFareOverrideRequestData'};
    
    tempuriOrg.ClearResponse_element ClearResponse_elementObj = new tempuriOrg.ClearResponse_element();
        ClearResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        ClearResponse_elementObj.field_order_type_info = new String[]{};
    
              tempuriOrg.SendItinerary_element SendItinerary_elementObj = new tempuriOrg.SendItinerary_element();
        SendItinerary_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
       SendItinerary_elementObj.strRecordLocator_type_info = new String[]{'strRecordLocator','http://tempuri.org/',null,'0','1','true'};
       SendItinerary_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       SendItinerary_elementObj.field_order_type_info = new String[]{'strSessionID','strRecordLocator'};
    
   tempuriOrg.AddInProcessPaymentToBooking_element AddInProcessPaymentToBooking_elementObj = new tempuriOrg.AddInProcessPaymentToBooking_element();
        AddInProcessPaymentToBooking_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
      AddInProcessPaymentToBooking_elementObj.objPayment_type_info = new String[]{'objPayment','http://tempuri.org/',null,'0','1','true'};
     AddInProcessPaymentToBooking_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       AddInProcessPaymentToBooking_elementObj.field_order_type_info = new String[]{'strSessionID','objPayment'};
    
    tempuriOrg.GetBookingHistory_element GetBookingHistory_elementObj = new tempuriOrg.GetBookingHistory_element();
       GetBookingHistory_elementObj.strSessionID='';
       // GetBookingHistory_elementObj.GetBookingHistoryRequestData= new schemasDatacontractOrg200407AceEnt.GetBookingHistoryRequestData();
       GetBookingHistory_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        GetBookingHistory_elementObj.objGetBookingHistoryRequestData_type_info = new String[]{'objGetBookingHistoryRequestData','http://tempuri.org/',null,'0','1','true'};
        GetBookingHistory_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        GetBookingHistory_elementObj.field_order_type_info = new String[]{'strSessionID','objGetBookingHistoryRequestData'};
    
               tempuriOrg.SendItineraryResponse_element SendItineraryResponse_elementObj = new tempuriOrg.SendItineraryResponse_element();
        SendItineraryResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        SendItineraryResponse_elementObj.field_order_type_info = new String[]{};
    
	
	  tempuriOrg.Clear_element Clear_elementObj = new tempuriOrg.Clear_element();
        Clear_elementObj.strSessionID='';
        Clear_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        Clear_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        Clear_elementObj.field_order_type_info = new String[]{'strSessionID'};
    
	
	
	   tempuriOrg.GetBookingFromStateResponse_element GetBookingFromStateResponse_elementObj = new tempuriOrg.GetBookingFromStateResponse_element();
       // public schemasDatacontractOrg200407AceEnt.Booking GetBookingFromStateResult;
        GetBookingFromStateResponse_elementObj.GetBookingFromStateResult_type_info = new String[]{'GetBookingFromStateResult','http://tempuri.org/',null,'0','1','true'};
       GetBookingFromStateResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetBookingFromStateResponse_elementObj.field_order_type_info = new String[]{'GetBookingFromStateResult'};
    
	
	    tempuriOrg.CancelInProcessPayment_element CancelInProcessPayment_elementObj = new tempuriOrg.CancelInProcessPayment_element();
        CancelInProcessPayment_elementObj.strSessionID='';
        CancelInProcessPayment_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
        CancelInProcessPayment_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        CancelInProcessPayment_elementObj.field_order_type_info = new String[]{'strSessionID'};
    
	
	    tempuriOrg.GetBookingResponse_element GetBookingResponse_elementObj = new tempuriOrg.GetBookingResponse_element();
       // public schemasDatacontractOrg200407AceEnt.Booking GetBookingResult;
       GetBookingResponse_elementObj.GetBookingResult_type_info = new String[]{'GetBookingResult','http://tempuri.org/',null,'0','1','true'};
       GetBookingResponse_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
       GetBookingResponse_elementObj.field_order_type_info = new String[]{'GetBookingResult'};
    
    tempuriOrg.GetBookingFromState_element GetBookingFromState_elementObj = new tempuriOrg.GetBookingFromState_element();
       GetBookingFromState_elementObj.strSessionID='';
       GetBookingFromState_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
      GetBookingFromState_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
      GetBookingFromState_elementObj.field_order_type_info = new String[]{'strSessionID'};
    
	
	 tempuriOrg.GetBooking_element GetBooking_elementObj = new tempuriOrg.GetBooking_element();
       GetBooking_elementObj.strSessionID='';
      //  public schemasDatacontractOrg200407AceEnt.GetBookingRequestData objGetBookingRequestData;
        GetBooking_elementObj.strSessionID_type_info = new String[]{'strSessionID','http://tempuri.org/',null,'0','1','true'};
       GetBooking_elementObj.objGetBookingRequestData_type_info = new String[]{'objGetBookingRequestData','http://tempuri.org/',null,'0','1','true'};
      GetBooking_elementObj.apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
      GetBooking_elementObj.field_order_type_info = new String[]{'strSessionID','objGetBookingRequestData'};
    
            
    }
    
    
      @isTest public static void testGetBookingHistoryMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new tempuriOrgBookingHistoryResponseMock());
        Test.startTest();
        tempuriOrg.basicHttpsBookingService basicHttpsObj1 = new tempuriOrg.basicHttpsBookingService();
        basicHttpsObj1.GetBookingHistory('',new schemasDatacontractOrg200407AceEnt.GetBookingHistoryRequestData());
        Test.stopTest();
        
      
      }
    
    
      @isTest public static void testClearSessionMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new tempuriOrgBookingClearSessionMock());
        Test.startTest();
        tempuriOrg.basicHttpsBookingService basicHttpsObj1 = new tempuriOrg.basicHttpsBookingService();
        basicHttpsObj1.Clear('');
        Test.stopTest();
        
      
      }
    
     @isTest public static void testGetBookingMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new tempuriOrgBookingMock());
        Test.startTest();
        tempuriOrg.basicHttpsBookingService basicHttpsObj1 = new tempuriOrg.basicHttpsBookingService();
        basicHttpsObj1.GetBooking('',new schemasDatacontractOrg200407AceEnt.GetBookingRequestData());
        Test.stopTest();
        
      
      }
    
      @isTest public static void testSendItenariryMockMock(){
      
            // set mock implementation     
        Test.setMock(WebServiceMock.class, new tempuriOrgSendItenariryMock());
        Test.startTest();
        tempuriOrg.basicHttpsBookingService basicHttpsObj1 = new tempuriOrg.basicHttpsBookingService();
        basicHttpsObj1.SendItinerary('','');
        Test.stopTest();
        
      
      }
    
    
    
    
    
}