<apex:page id="changePassword" showHeader="false" controller="ChangePasswordController" title="{!$Label.site.change_password}" >
    <style>
        @import url("https://fonts.googleapis.com/css?family=Roboto");
        *{
        font-family: Roboto,san-serif !important;
        font-size: 14px;
        }
        .defaultColor {
        color: #696969;
        }
        .defaultBorder {
        border: #bfc5cf thin solid;
        border-radius: 5px;
        padding: 20px;
        }
        .defaultMargin {
        margin-top: 10px;
        margin-bottom: 10px;
        }
        .defaultSize {
        height: 70px !important
        }        
        .fontLarge {
        font-size: 1.5em;
        }
        .loginHeader {
        margin: 10px;
        font-size: 1.5em;
        color: #333;
        font-weight: 700;
        line-height: 1.4;
        word-wrap: break-word;
        }
        .marginBottom {
        margin-bottom: 10px;
        }
        .maxWidth input,
        button.maxWidth {
        width: 100%;
        margin-left : 0px;
        }
        .maxWidth input {
        border: #bfc5cf thin solid;
        }
        .maxWidth label {
        margin-bottom: 0px;    
        }
        .maxWidth abbr {
        display: none;
        }
        .maxWidth .slds-form-element__label {
        margin-bottom: 0px;
        }
        .defaultButton {
        border: #ff3333 1px solid !important;
        color: #ffffff !important;
        background-color: #ff3333 !important;
        font-size: 1em !important;
        cursor: pointer !important;
        }
        .defaultButton:disabled {
        cursor: default !important;
        background-color: #D8DDE6 !important;
        border: #D8DDE6 1px solid !important;
        }
        .defaultButton:hover {
        color: #ff3333 !important;
        background-color: #ffffff !important;
        }
        .defaultButton:disabled:hover {
        cursor: default !important;
        background-color: #D8DDE6 !important;
        color: #ffffff !important;
        }
        .defaultLink {
        color: #ff3333;
        font-weight: 700;
        }
        .marginTop {
        margin-top: 5%;
        }
        .errMsg {
        color: #ff3333;
        text-align: center;
        }
        .password-wrapper {
        position: relative;
        background: #FFF;
        }
        
        input.answer-input:focus,
        input.password-input:focus {
        background-color: #ffffff;
        border: 1px solid #1589ee;	
        outline: 0;
        }
        
        input.password-input.password-weak,
        input.password-input.password-weak:focus {
        border:1px solid #C23934;
        }
        
        input.password-input.password-good,
        input.password-input.password-good:focus {
        border:1px solid #4bca81;
        }
        
        .password-good-color {
        color: green;
        }
        
        input.password-input.input {
        border: 1px solid #D8DDE6;
        border-radius: 4px; 
        border-image-source: initial;
        border-image-slice: initial;
        border-image-width: initial;
        border-image-outset: initial;
        border-image-repeat: initial;
        background-color: #fff;
        font-family: SFS, Arial, sans-serif;
        box-sizing: border-box;
        -webkit-appearance: none;
        font-size: 14px;
        transition: all 0.1s;
        }
        
        .input {
        padding: 12px;
        }
        
        .input:focus { 
        background-color: #ffffff;
        border: 1px solid #1589ee;
        outline: 0;
        box-shadow: 0 0 3px #0070D2;
        }
        
        input.twofactor {
        background-image: url('/img/keys.png');
        background-repeat: no-repeat;
        background-position: 12px, 12px;
        background-size: 14px 18px;
        padding: 12px 40px;
        }
        
        input.error {
        border: 1px solid #C23934;
        }
        
        .wide {
        width: 100%;
        }
        
        .mt8 {
        margin-top: 8px;
        }
        
        .mb16 {
        margin-bottom: 16px;
        }
        
        .password-message {
        display: block;
        line-height: 45px;
        font-size:10px;
        padding: 0 10px;
        pointer-events: none;
        position: absolute;
        right: 0;
        top: 0;
        }
        
        html[dir="rtl"] .password-message {
        right: auto;
        left: 0;
        }
        
        .password-none-color {
        color: #16325c;
        }
        
        .password-weak-color {
        color: #CF4F43;
        }
        .passwordicon {
        width: 16px;
        margin-left: 10px;
        display: none;
        }
    </style>
    <script type='text/javascript'>
    var elementClassList = ['', 'password-weak', 'password-good'];
    var elementMsgClassList = ['password-none-color', 'password-weak-color', 'password-weak-color'];
    function checkPassword(element, msgElement) {
        msgElement = document.getElementById(msgElement);
        if(element.value === '') {
            includeClassList(element, elementClassList, '');
            includeClassList(msgElement, elementMsgClassList, 'password-none-color');
            msgElement.innerHTML = '';
            visibleElement(element, /^(?=.*[a-zA-Z])/, 'lettericon');
            visibleElement(element, /^(?=.*[0-9])/, 'numbericon');
            visibleElement(element, /^(?=.{8,20})/, '8charicon');
        } else {
            visibleElement(element, /^(?=.*[a-zA-Z])/, 'lettericon');
            visibleElement(element, /^(?=.*[0-9])/, 'numbericon');
            visibleElement(element, /^(?=.{8,20})/, '8charicon');
            
            if(validateData(element, /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.{8,20})/)){
                includeClassList(element, elementClassList, 'password-good');
                includeClassList(msgElement, elementMsgClassList, 'password-good-color');
                msgElement.innerHTML = 'Good';
            } else {
                includeClassList(element, elementClassList, 'password-weak');
                includeClassList(msgElement, elementMsgClassList, 'password-weak-color');
                msgElement.innerHTML = 'Too Week';
            }
        }
        verifyPassword();
    }
    function validateData(element, regexPattern) {
        return regexPattern.test(element.value);
    }
    function visibleElement(element, regexPattern, visibleElement) {
        if(validateData(element, regexPattern)) {
            document.getElementById(visibleElement).style.display = "inline-block";
        } else {
            document.getElementById(visibleElement).style.display = "none";
        }
    }
    function verifyPassword() {
        passwordElement = document.getElementById('{!$Component.changePasswordForm.psw}');
        verifyPasswordElement = document.getElementById('{!$Component.changePasswordForm.vpsw}');
        verifyPasswordMsg = document.getElementById('verifypassword-message');
        document.getElementById('{!$Component.changePasswordForm.cpwbtn}').disabled = true;
        if(verifyPasswordElement.value === '' || verifyPasswordElement.value == null) {
            verifyPasswordMsg.innerHTML = '';
            includeClassList(verifyPasswordElement, elementClassList, '');
            includeClassList(verifyPasswordMsg, elementMsgClassList, 'password-none-color');
        } else {
            if(validateData(passwordElement, /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.{8,20})/) && passwordElement.value == verifyPasswordElement.value) {
                includeClassList(verifyPasswordElement, elementClassList, 'password-good');
                includeClassList(verifyPasswordMsg, elementMsgClassList, 'password-good-color');
                verifyPasswordMsg.innerHTML = 'Match';
                document.getElementById('{!$Component.changePasswordForm.cpwbtn}').disabled = false;
            } else if(passwordElement.value.indexOf(verifyPasswordElement.value) === 0) {
                verifyPasswordMsg.innerHTML = 'Good so far';
                includeClassList(verifyPasswordElement, elementClassList, '');
                includeClassList(verifyPasswordMsg, elementMsgClassList, 'password-none-color');
            } else {
                verifyPasswordMsg.innerHTML = 'Password don\'t match';
                includeClassList(verifyPasswordElement, elementClassList, 'password-weak');
                includeClassList(verifyPasswordMsg, elementMsgClassList, 'password-weak-color');
            }
        }
    }
    function includeClassList(element, classList, keepClass) {
        for (i = 0; i < classList.length; i++) {
            if(classList[i] !== '') {
                element.classList.remove(classList[i]);
            }
        }
        if(keepClass !== '') {
            element.classList.add(keepClass);
        }
    }
    function validateOldPassword() {
        var valid = true;
        try {
            var oldPasswordElement = document.getElementById('{!$Component.changePasswordForm.oldpsw}');
            if(oldPasswordElement && oldPasswordElement != null) {
                if(validateData(oldPasswordElement, /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.{8,20})/) === false) {
                    alert('Please Enter Valid Old Password');
                    valid = false;
                }
            }
        }catch(err) {
            console.error(err);
        }
        return valid;
    }
    window.onload = function() { 
   	 document.getElementById('{!$Component.changePasswordForm.cpwbtn}').disabled = true;
    } 
    </script>
    <apex:slds />
    <apex:define name="body">  
        <div class="slds-grid slds-wrap slds-grid_pull-padded">
            <div class="slds-p-horizontal_small slds-size_1-of-1 slds-medium-size_1-of-6 slds-large-size_3-of-12"></div>
            <div class="slds-p-horizontal_small slds-size_1-of-1 slds-medium-size_4-of-6 slds-large-size_6-of-12">
                <div class="slds-grid slds-wrap slds-grid_pull-padded marginTop">
                    <div class="slds-p-horizontal_small slds-size_2-of-12"></div>
                    <div class="slds-p-horizontal_small slds-size_8-of-12">
                        <div class="slds-grid slds-wrap slds-grid_pull-padded slds-text-align_center cLoginHeader">
                            <div class="slds-p-horizontal_small slds-size_1-of-1">
                                <a href="/s?language=en_GB" target="_self">
                                    <apex:image styleClass="defaultSize" url="{!URLFOR($Resource.CC_Resources, '/images/airasialogo.png')}" />
                                </a>
                            </div>
                            <div class="slds-p-horizontal_small slds-size_1-of-1 loginHeader">{!$Label.AA_Case_View_My_Cases}</div>
                        </div>
                        <div class="slds-grid slds-wrap slds-grid_pull-padded defaultBorder">
                            <div class="slds-p-horizontal_small slds-size_1-of-1 defaultColor fontLarge">
                                {!$Label.AA_Case_Change_Password}
                            </div>
                            <div class="slds-p-horizontal_small slds-size_1-of-1 defaultMargin defaultColor">
                                <apex:outputText escape="false" value="{!message1}"/>
                            </div>
                            <div style="width:100%;">
                                <apex:form id="changePasswordForm">
                                    <apex:pageMessages id="error"/>
                                    <apex:outputLabel rendered="{! !$Site.IsPasswordExpired}" >
                                        <div class="slds-p-horizontal_small slds-size_1-of-1 defaultMargin">
                                            <div class="slds-form-element maxWidth is-required lightningInput">
                                                <label class="slds-form-element__label slds-no-flex">
                                                    <abbr class="slds-required" title="required">*</abbr>
                                                    <span>{!$Label.site.old_password}</span>
                                                </label>
                                                <div class="slds-form-element__control slds-grow">
                                                    <div class="password-wrapper">
                                                        <apex:inputSecret styleClass="password-input input wide mt8 mb16" required="true" id="oldpsw" value="{!oldPassword}"/>
                                                        <span id="oldpassword-message" aria-live="polite" class="password-message password-none-color"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </apex:outputLabel>
                                    <div class="slds-p-horizontal_small slds-size_1-of-1 defaultMargin">
                                        <div class="slds-form-element maxWidth is-required lightningInput">
                                            <label class="slds-form-element__label slds-no-flex">
                                                <abbr class="slds-required" title="required">*</abbr>
                                                <span>{!$Label.AA_Case_Change_Password_New}</span>
                                            </label>
                                            <div class="slds-form-element__control slds-grow">
                                                <div class="password-wrapper">
                                                    <apex:inputSecret styleClass="password-input input wide mt8 mb16" required="true" id="psw" value="{!newPassword}" onkeyup="checkPassword(this, 'newpassword-message');"/>
                                                    <span id="newpassword-message" aria-live="polite" class="password-message password-none-color"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slds-p-horizontal_small slds-size_1-of-1 defaultMargin">
                                        <div class="slds-form-element maxWidth is-required lightningInput">
                                            <label class="slds-form-element__label slds-no-flex">
                                                <abbr class="slds-required" title="required">*</abbr>
                                                <span>{!$Label.AA_Case_Change_Password_Confirm_New}</span>
                                            </label>
                                            <div class="slds-form-element__control slds-grow">
                                                <div class="password-wrapper">
                                                    <apex:inputSecret styleClass="password-input input wide mt8 mb16" required="true" id="vpsw" value="{!verifyNewPassword}" onkeyup="verifyPassword();"/>
                                                    <span id="verifypassword-message" aria-live="polite" class="password-message password-none-color"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slds-p-horizontal_small slds-size_1-of-1 defaultMargin maxWidth marginTop">
                                        <apex:commandButton styleClass="slds-button slds-button--neutral defaultButton" id="cpwbtn" action="{!changePassword}" value="{!$Label.site.change_password}" />
                                    </div>
                                    <div class="slds-p-horizontal_small slds-size_1-of-1 defaultColor">
                                        {!message2}
                                    </div>
                                </apex:form>   
                            </div>
                        </div>
                    </div>
                    <div class="slds-p-horizontal_small slds-size_2-of-12"></div>
                </div>
            </div>
            <div class="slds-p-horizontal_small slds-size_1-of-1 slds-medium-size_1-of-6 slds-large-size_3-of-12"></div>
        </div>
    </apex:define>
</apex:page>