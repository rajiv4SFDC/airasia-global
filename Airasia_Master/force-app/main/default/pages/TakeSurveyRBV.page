<apex:page standardcontroller="Survey__c" extensions="ViewSurveyController" cache="false" sidebar="false" showheader="false">
    <apex:includeScript value="{! $Resource.SurveyForce_jquery}"/>
    <!-- convertCheckBoxToLDS() is in surveyforce.js -->
    <apex:includeScript value="{! $Resource.SurveyForce + '/surveyforce.js'}"/>
    <apex:stylesheet value="{! $Resource.SurveyForce + '/surveyforce.css'}"/>
    <apex:stylesheet value="{! $Resource.SurveyForce + '/surveyforce_pagemessage_override.css'}"/>
    <apex:stylesheet value="{! $Resource.SurveyForce_SLDS + '/assets/styles/salesforce-lightning-design-system-vf.min.css'}"/>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    
    
    <script>
    $(document).ready(function(){
        //Convert any elements with "convertToLDS" style to SLDS styles
        //Used in certain places where it's not easy to use SLDS for those elements like "apex:selectCheckboxes"
        convertCheckBoxToLDS();
    });
    </script>
    <!-- Custom CSS added by survey admin -->
    <style>
        <apex:outputText value="{!HTMLENCODE(surveyContainerCss)}" escape="false"/>
    </style>
    
<!--   <c:ChatHeaderCmp date="" header="AirAsia Customer Service - Case Survey" /> -->
    <body style="padding:0 0 0 0; margin:0 auto;-webkit-text-size-adjust:none;-ms-text-size-adjust: 100%; font-family:Arial, Helvetica, sans-serif; background-color:#f3f3f3; font-size:14px;">
        <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F3F3F3" class="emailwrapto100pc" style="-webkit-text-size-adjust:none;-ms-text-size-adjust: 100%;">
            <tr>
                <td align="center"><table class="emailwrapto100pc" style="border:#dddddd thin solid; " border="0" cellspacing="0" cellpadding="0" width="640" align="center" bgcolor="#ffffff">
                    <tr>
                        <td align="center" bgcolor="#ffffff" style="padding-top: 10px;">
                            <table width="85%" border="0" cellspacing="0" cellpadding="0"  class="emailwrapto100pc">
                                <tr>
                                    <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:10px;line-height:17px">
                                        <div id="survey_container" class="surveyforce">
                                            <apex:form id="theForm"   >
                                                
                                                <apex:outputPanel id="seeSurvey" rendered="false" ><!-- {! If((Survey__c.Hide_Survey_Name__c == false || Survey__c.Survey_Header__c != ''), True, False )}-->
                                                    <div class="slds-box slds-theme--shade">
                                                        <div class="slds-text-heading--large"><apex:outputText value="{!Survey__c.Survey_Header__c}" escape="false" /> </div>
                                                        <apex:outputPanel rendered="{!Survey__c.Hide_Survey_Name__c == false}">
                                                            <div class="slds-text-heading--medium">{!Survey__c.Name}</div>
                                                        </apex:outputPanel>
                                                    </div>
                                                </apex:outputPanel>
                                                
                                                
                                                <script>
                                                $(document).ready(function(){
                                                    overridePageMessages();
                                                });
                                                </script>
                                                
                                                
                                                <apex:pageMessages />
                                                
                                                <c:uiMessage severity="success" message="{!surveyThankYouText}" renderMe="{!thankYouRendered == true}" />
                                                
                                                <apex:outputPanel rendered="{!thankYouRendered == false}">
                                                    <div class="slds-form--stacked">
                                                        <apex:repeat value="{!allQuestions}" var="qPreview" id="aQPreview">
                                                            <div class="slds-box slds-theme--default" style="border: none;">
                                                                <div class="slds-form-element">
                                                                    <label class="slds-form-element__label">
                                                                        {!qPreview.orderNumber + ': ' + qPreview.question}
                                                                        <apex:outputText rendered="{! qPreview.required}" value="({!$Label.LABS_SF_Required})" />
                                                                    </label>
                                                                </div>
                                                                <apex:outputPanel rendered="{!qPreview.renderSelectRadio}">
                                                                    <apex:selectRadio styleClass="convertToLDS" layout="pageDirection" rendered="{!qPreview.renderSelectRadio}" value="{!qPreview.selectedOption}" >
                                                                        <apex:selectOptions value="{!qPreview.singleOptions}"/>
                                                                    </apex:selectRadio>
                                                                </apex:outputPanel>
                                                                
                                                                <apex:outputPanel rendered="{!qPreview.renderSelectCheckboxes}">
                                                                    <apex:selectCheckboxes styleClass="convertToLDS" layout="pageDirection" rendered="{!qPreview.renderSelectCheckboxes}" value="{!qPreview.selectedOptions}" >
                                                                        <apex:selectOptions value="{!qPreview.multiOptions}"/>
                                                                    </apex:selectCheckboxes>
                                                                </apex:outputPanel>
                                                                <apex:outputPanel rendered="{!qPreview.renderFreeText}">
                                                                    <div class="slds-form-element">
                                                                        <div class="slds-form-element__control" >
                                                                            <apex:inputTextArea styleClass="slds-textarea" cols="" rows="{!qPreview.noOfRowsForTextArea}" rendered="{!qPreview.renderFreeText}" value="{!qPreview.choices}"/>
                                                                        </div>
                                                                    </div>
                                                                </apex:outputPanel>
                                                                <apex:outputPanel rendered="{!qPreview.renderSelectRow}">
                                                                    <apex:selectRadio styleClass="convertToLDS" rendered="{!qPreview.renderSelectRow}" value="{!qPreview.selectedOption}">
                                                                        <apex:selectOptions value="{!qPreview.rowOptions}"/>
                                                                    </apex:selectRadio>
                                                                </apex:outputPanel>
                                                            </div>
                                                        </apex:repeat>
                                                    </div>
                                                </apex:outputPanel>
                                                <apex:outputPanel rendered="{!thankYouRendered == false}">
                                                    <div class="slds-box slds-theme--default" style="border: none;">
                                                        <apex:outputPanel rendered="{!isInternal}" >
                                                            <span class="slds-text-body--regular">{!$Label.LABS_SF_Answer_as}:</span>
                                                            <apex:selectRadio styleClass="convertToLDS" value="{!anonymousAnswer}">
                                                                <apex:selectOptions value="{!anonymousOrUser}" />
                                                                <apex:actionSupport event="onchange" rerender="hiddenAnonymousAnswer"/>
                                                            </apex:selectRadio>
                                                            <apex:inputHidden value="{!anonymousAnswer}" id="hiddenAnonymousAnswer"/>
                                                            <br />
                                                        </apex:outputPanel>
                                                        <apex:commandButton styleClass="slds-button slds-button--brand" style="background-color: #ff3333;font-size: 14px;" action="{!submitResults}" value="{!$Label.LABS_SF_SubmitSurvey}" rerender="theForm,seeSurvey" />
                                                    </div>
                                                </apex:outputPanel>
                                            </apex:form>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table style="display:none;" width="80%" border="0" cellspacing="0" cellpadding="0"  class="emailwrapto100pc">
                                <tr>
                                    <td height="40">&nbsp;</td>
                                </tr>
                            </table>
                            <table class="emailwrapto100pc" width="640" border="0" cellspacing="0" cellpadding="0">
                            </table>
                        </td>
                    </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>        
<!--    <c:EmailTemplateFooterCmp /> -->
</apex:page>