/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
14/09/2017      Sharan Desai   		Created
*******************************************************************/

trigger ActualSalesTrigger on Actual_Sales__c (before insert,before update) {
        
    //-- For Before Context
    if(trigger.isBefore){
        
        //-- Call Handler method to perform Before Insert Context operations
        if(Trigger.IsInsert){            
            ActualSalesTriggerHandler.isBeforeInsert(Trigger.NEW);
        }
        
        //-- Call Handler method to perform Before Update Context operations
        if(Trigger.IsUpdate){
            ActualSalesTriggerHandler.isBeforeUpdate(Trigger.NEW);            
        }
        
    }
    
}