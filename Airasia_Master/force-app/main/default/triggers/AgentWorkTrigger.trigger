trigger AgentWorkTrigger on AgentWork (before insert, after update) {
    /**Before Insert context has been enabled for this Trigger only for the Code Coverage of the Test Class
       Since Work Record creation/updation always requires Agent to be Live , so to over come this issue changes are made
    **/
    //-- After Update 
    if (Trigger.IsUpdate && Trigger.IsAfter) {
        AgentWorkTriggerHandler.UpdateCaseStatus(trigger.New);
    }
}