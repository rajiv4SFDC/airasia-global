trigger CaseTrigger on Case(after insert, after update,before update,before insert) {
    
    
    if (Trigger.IsInsert && Trigger.IsAfter) {
        CaseTriggerHandler.sendDataToRPSOnInsert(Trigger.new);
    }
    
    if (Trigger.IsInsert && Trigger.IsAfter && !System.isFuture() && !System.isBatch()) {
        CaseTriggerHandler.retrieveAVATranscript(Trigger.new);
        CaseTriggerHandler.retrieveAVAAttachment(Trigger.new);
    }
    
    if (Trigger.IsInsert && Trigger.IsBefore) {
        CaseTriggerHandler.populateAdaGlassCase(Trigger.new);
        CaseTriggerHandler.sendDataToRPSOnBeforeInsert(Trigger.new);
    }
    
    if (Trigger.IsUpdate && Trigger.IsAfter && !System.isFuture()) {
//    if (Trigger.IsUpdate && Trigger.IsAfter) {
        CaseTriggerHandler.sendDataToRPSOnUpdate(Trigger.new, Trigger.OldMap);
        CaseTriggerHandler.sendDataToCaseRouting(Trigger.new,Trigger.OldMap); // ADDED 12Sep2018: Interim solution
        CaseTriggerHandler.updateCRC(Trigger.new,Trigger.OldMap); // ADDED 04 July 2019 - Added for ChildReference
        CaseTriggerHandler.closeParentCases(Trigger.new,Trigger.OldMap); // ADDED 15Sep2018
    }
    
     if (Trigger.IsUpdate && Trigger.IsBefore && !System.isFuture()) {
//     if (Trigger.IsUpdate && Trigger.IsBefore) {
        CaseTriggerHandler.sendDataToRPSOnBeforeUpdate(Trigger.new,Trigger.OldMap);
        CaseTriggerHandler.matchSocialCases(Trigger.new); //To Fix Account Retrieval
        CAACResponseHandler.sendResponseToCAAC(Trigger.new); //AT - 14 June 2019 - Added for CAAC Response automation
    }
    
}//CaseTrigger