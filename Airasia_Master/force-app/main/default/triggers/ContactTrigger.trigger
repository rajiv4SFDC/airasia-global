/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
09/OCT/2017      Sharan Desai   		Created
*******************************************************************/
trigger ContactTrigger on Contact (before insert,before update) {

     
    //-- For Before Context
    if(trigger.isBefore){
        
       //-- Call Handler method to perform before Insert Context operations
        if(Trigger.IsInsert && ContactTriggerHandler.runOnce()){            
            ContactTriggerHandler.CheckPrimaryContact(Trigger.NEW,true);
        }
        
         //-- Call Handler method to perform before Insert Context operations
        if(Trigger.IsUpdate  && ContactTriggerHandler.runOnce()){            
           ContactTriggerHandler.CheckPrimaryContact(Trigger.NEW,false);
        }
        
    }
    
}