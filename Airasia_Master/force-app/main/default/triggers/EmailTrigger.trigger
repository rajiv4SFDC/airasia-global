/**
 * @File Name          : EmailTriggerHandler.cls
 * @Description        : Email trigger for Detecting case Language
 * @Author             : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Group              : 
 * @Last Modified By   : Mark Alvin Tayag (mtayag@salesforce.com)
 * @Last Modified On   : 9/19/2019, 11:20:18 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/19/2019   Mark Alvin Tayag (mtayag@salesforce.com)     Initial Version
**/
trigger EmailTrigger on EmailMessage (after insert) {
	EmailTriggerHandler.detectEmailLanguage(Trigger.new);
}