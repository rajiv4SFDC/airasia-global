trigger FeedCommentTrigger on FeedComment (before insert, after insert) {
    FeedTriggerHandler handler = new FeedTriggerHandler();
    handler.isFeedItem = false;
    handler.run();
}