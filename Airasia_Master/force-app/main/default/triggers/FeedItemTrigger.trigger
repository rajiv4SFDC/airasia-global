trigger FeedItemTrigger on FeedItem (before insert, after insert) {
	FeedTriggerHandler handler = new FeedTriggerHandler();
    handler.isFeedItem = true;
    handler.run();
}