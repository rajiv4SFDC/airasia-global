/*******************************************************************
Author:        Sharan Desai
Email:         dsharan@crmit.com

History:
Date            Author              Comments
-------------------------------------------------------------
21/09/2017      Sharan Desai        Created
*******************************************************************/

trigger LeadTrigger on Lead (after insert,after update) {
    
    //-- For Before Context
    if(trigger.isAfter && !System.isFuture()){
        
        //-- Call Handler method to perform After Insert Context operations
        if(Trigger.IsInsert){            
            LeadTriggerHandler.isAfterInsert(Trigger.NEW);
        }
        
        //-- Call Handler method to perform After Update Context operations
        if(Trigger.IsUpdate){
            LeadTriggerHandler.isAfterUpdate(Trigger.NEW);            
        }
        
    }
}