trigger LiveChatTranscriptTrigger on LiveChatTranscript (before insert) {
    TriggerHandler handler = new LiveChatTranscriptTriggerHandler();
    handler.run();
}