/**********************************************************************************************************************
* Social Customer Service Power Bundle                                                                                *
* Version         1.0                                                                                                 *
* Author          Hao Dong <hdong@salesforce.com>                                                                     *
*                 Darcy Young <darcy.young@salesforce.com>                                                            *
*                                                                                                                     *
* Usage           The package bundles commonly requested Social Customer Care features, such as                       *
*                  - Scablable PostTag to Post/Case Field Solution                                                    *
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        *
*                  - Ready to drive case management process                                                           *
*                  - (Optionally) Set case origin                                                                     *
*                  - (Optionally) Trigger case assignment rule                                                        *
*                  - Rollup Followers from Social Persona to Contact                                                  *
*                                                                                                                     *
* Component       SFPS_SCS_SocialPersonaAction Class - Actions related to Social Persona                              *
*                  - Rollup Followers from Social Persona to Contact                                                  *
*                                                                                                                     *
*                                                                                                                     *
* Last Modified   02/23/2015                                                                                          *
**********************************************************************************************************************/
trigger SFPS_SCS_SocialPersonaTrigger on SocialPersona (after insert, after update) {

	List<SocialPersona> socialPersonasWithParent = new List<SocialPersona>(); 

	for (SocialPersona p : trigger.new)
	{
        if (Trigger.IsInsert || Trigger.IsUpdate)
        {
        	if (Trigger.IsAfter)
        	{
	        	if(p.ParentId != null){
	        		if(p.Followers != null || p.Followers != 0){
	        			socialPersonasWithParent.add(p);
	        		}
	        	}
	        }
        }
	}

    if(!socialPersonasWithParent.isEmpty()) SFPS_SCS_SocialPersonaActions.updateContactFollowersCount(socialPersonasWithParent);


}