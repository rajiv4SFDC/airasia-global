/**********************************************************************************************************************
* Social Customer Service Power Bundle                                                                                *
* Version         1.0                                                                                                 *
* Author          Hao Dong <hdong@salesforce.com>                                                                     *
*                 Darcy Young <darcy.young@salesforce.com>                                                            *
*                                                                                                                     *
* Usage           The package bundles commonly requested Social Customer Care features, such as                       *
*                  - Scablable PostTag to Post/Case Field Solution                                                    *
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        *
*                  - Ready to drive case management process                                                           *
*                  - (Optionally) Set case origin                                                                     *
*                  - (Optionally) Trigger case assignment rule                                                        *
*                  - Rollup Followers from Social Persona to Contact                                                  *
*                                                                                                                     *
* Component       SFPS_SCS_SocialPostAction Class - Actions related to Social Post                                    *
*                  - Scablable PostTag to Post/Case Field Solution                                                    *
*                  - PostTag Dictionary including fields such as language influence, NPS and custom attributes        *
*                  - Ready to drive case management process                                                           *
*                  - (Optionally) Set case origin                                                                     *
*                  - (Optionally) Trigger case assignment rule                                                        *
*                                                                                                                     *
* Custom Settings SFPS_SCS_Settings__c                                                                                *
*                  - AssignmentRuleOnNewCase                                                                          *
*                  - AssignmentRuleOnExistingCase                                                                     *
*                  - CaseOrigin                                                                                       *
*                                                                                                                     *
* Last Modified   02/23/2015                                                                                          *
**********************************************************************************************************************/
trigger SFPS_SCS_SocialPostTrigger on SocialPost (before insert, before update, after insert, after update) {

	List<SocialPost> postsWithNewParent = new List<SocialPost>(); 

    if (Trigger.isBefore){

        if (Trigger.isInsert){
            SFPS_SCS_SocialPostActions.updatePostAttributesByTags(trigger.new);
        }

    }

    if (Trigger.isAfter){

        if(Trigger.isInsert || Trigger.isUpdate){

	        for (SocialPost p : trigger.new) 
	        {
	            if(p.ParentId != null && ((p.IsOutbound == false && p.SFPS_SCS_Case_Data_Processed__c == false) || (p.IsOutbound == true && Trigger.isInsert))
                    && (trigger.oldMap == null || trigger.oldMap.get(p.Id) == null || trigger.oldMap.get(p.Id).ParentId == null))
	                postsWithNewParent.add(p);	        
            }

        }
    }

    if(!postsWithNewParent.isEmpty()) {
        SFPS_SCS_SocialPostActions.updateNewParent(postsWithNewParent);
    }
}